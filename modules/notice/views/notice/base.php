<?php foreach($notifications as $type => $notification): ?>
	
	<?php if ( ! empty($notification)): ?>

	<?php foreach ($notification as $notice): ?>
		
		<?php if ($notice['message'] !== NULL): ?>

			<div class="message-<?php echo $type ?>">
		
				<?php echo $notice['message'] ?>
	
			</div>
		
		<?php endif; ?>

		<?php if ( ! empty($notice['items'])): ?>
		
			<?php foreach($notice['items'] as $item): ?>

			<div class="message-<?php echo $type ?>">
			
				<?php echo __($item) ?>
			
			</div>
			
			<?php endforeach; ?>
			
		<?php endif; ?>
		
	<?php endforeach; ?>

	<?php endif; ?>
	
<?php endforeach; ?>
