<div class="pagination">
	<ul>
		<?php for ($i = 1; $i <= $total_pages; $i++): ?>

		<?php if ($i == $current_page): ?>
		<li class="current"><span><?php echo $i; ?></span></li>
		<?php else: ?>
		<li><a href="<?php echo HTML::chars($page->url($i)) ?>"><?php echo $i; ?></a></li>
		<?php endif; ?>

		<?php endfor; ?>
	</ul>
	<div class="clear"></div>
</div>