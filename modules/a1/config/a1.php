<?php

return array(

	'driver'     => 'ORM', // orm/jelly/mango/sprig
	'user_model' => 'admin',
	'cost'       => 12,    // Bcrypt Cost - any number between 4 and 31 -> higher = stronger hash

	'cookie'     => array(
		'key'         => 'hoterip_ycs_login',
		'lifetime'    => 1209600, // two weeks
	),

	'columns'   => array(
		'username'    => 'username',
		'password'    => 'password',
		'token'       => 'token',
		'last_login'	=> 'last_login', // (optional)
		'logins'			=> 'logins'      // (optional)
	),

	'session'  => array(
		'type'        => 'database' // native or database
	)
);
