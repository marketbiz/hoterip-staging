<?php defined('SYSPATH') or die('No direct script access.');

// Autoloading for MPDF
require Kohana::find_file('vendor', 'mpdf/mpdf');