<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Email message building and sending.
 *
 * @package    mPDF
 * @category   PDF
 * @author     kohei okuda
 */
class Kompdf {

	/**
	 * @var  string
	 * 
	 *	utf-8: DEFAULT
	 */
	protected $_mode = 'utf-8';

	/**
	 * @var  string
	 *
	 *	A4: DEFAULT
	 */
	protected $_format = 'A4';

	protected $_font_size = 0;
	protected $_default_font = '';

	protected $_margin_left = 5;
	protected $_margin_right = 5;
	protected $_margin_top = 10;
	protected $_margin_bottom = 10;
	protected $_margin_header = 0;
	protected $_margin_footer = 0;

	/**
	 * @var  string
	 * 
	 *	P: DEFAULT Portrait
	 *	L: Landscape
	 */
	protected $_orientation = 'P';

	/**
	 * @var  string
	 */
	protected $_html;
	protected $_filename;

	/**
	 * Create a new kompdf
	 *
	 * @param   string  mode
	 * @param   string  format
	 * @return  kompdf
	 */
	public static function factory($mode = NULL, $format = NULL)
	{
		$kompdf = new Kompdf;

		if ($format)
		{
			$kompdf->_format = $format;
		}

		if ($mode)
		{
			$kompdf->_mode = $mode;
		}

		return $kompdf;
	}

	protected function _set()
	{
		$kospdf = new mpdf(
			$this->_mode,
			$this->_format,
			$this->_font_size,
			$this->_default_font,
			$this->_margin_left,
			$this->_margin_right,
			$this->_margin_top,
			$this->_margin_bottom,
			$this->_margin_header,
			$this->_margin_footer,
			$this->_orientation
		);

		return $kospdf;
	}

	/**
	 * Adds HTML
	 *
	 * @param		string
	 * @return		Kompdf
	 */
	public function set_html($html)
	{
		$this->_html = $html;

		return $this;
	}

	// Default: send the file inline to the browser.
	// The plug-in is used if available.
	// The name given by filename is used when one selects the "Save as" option
	// on the link generating the PDF.
	public function send_browser()
	{
		$kospdf = $this->_set();
		$kospdf->WriteHTML($this->_html);
		$kospdf->Output();

		exit;
	}

	// Download: send to the browser and force a file download with the name given by filename.
	public function send_file($filename)
	{
		$kospdf = $this->_set();
		$kospdf->WriteHTML($this->_html);
		$kospdf->Output($filename, 'D');

		exit;
	}

	//File: save to a local file with the name given by filename (may include a path).
	public function save($path, $filename = NULL)
	{
		if ($filename)
		{
			$path = $path.$filename;
		}
		$kospdf = $this->_set();
		$kospdf->WriteHTML($this->_html);
		$kospdf->Output($path, 'F');
	}

	// String: return the document as a string. filename is ignored.
	public function render()
	{
		$kospdf = $this->_set();
		$kospdf->WriteHTML($this->_html);

		return $kospdf->Output('', 'S');
	}

}