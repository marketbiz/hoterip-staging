<?php defined('SYSPATH') or die('No direct access allowed.');

class Kohana_CE_Oerfree extends CE {

	public function convert($amount, $from, $to)
	{
		// define rates writing
		$from = strtoupper($from);
		$to   = strtoupper($to);
		$owncurr = strtoupper($from);
		// Set Base Currency
		$base = "USD";
		// Get the api key
		$app_id = $this->_config['oer_app_id'];
		// Get the rate
		$rate_json = Request::factory("http://openexchangerates.org/api/latest.json?app_id=$app_id&base=$base")
			->method(Request::GET)
			->execute()
			->body();

		$rate_json = json_decode($rate_json);
		// echo '<pre>';
		// print_r($rate_json);
		// exit;
		if (!$rate_json->timestamp) {
			return false;
		} else {
			if ($from == $to)
			{
				$value = 1;
			}
			elseif ($from == 'USD')
			{
				$rates = $rate_json->rates;
				$value = $amount * $rates->$to;
			}
			else
			{
				$rates = $rate_json->rates;
				$value = $amount * ($rates->$to/$rates->$owncurr);
			}
		}

		return $value;
	}

}