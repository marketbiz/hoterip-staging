<?php defined('SYSPATH') or die('No direct access allowed.');

class Kohana_CE_Oer extends CE {

	public function convert($amount, $from, $to)
	{
		// Get the api key
		$app_id = $this->_config['oer_app_id'];
		// Get the rate
		$rate_json = Request::factory("http://openexchangerates.org/api/latest.json?app_id=$app_id&base=$from")
			->method(Request::GET)
			->execute()
			->body();

		$rate_json = json_decode($rate_json);
		if (!$rate_json->timestamp) {
			return false;
		} else {
			$rates = $rate_json->rates;
			$to = strtoupper($to);
			$value = $amount * $rates->$to;
		}

		return $value;
	}

}