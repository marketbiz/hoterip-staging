<?php defined('SYSPATH') or die('No direct access allowed.');

class Kohana_CE_Xe extends CE {

	public function convert($amount, $from, $to)
	{
		$cache = Cache::instance();
		
		/// Write log to check function execute time
		$status = 0;

		if ( ! $xml = $cache->get('xefeed_'.$from))
		{
			/// Write log to check function execute time
			$status = 1;

			// Get feed url
			$feed_url = $this->_config->feed_url[$from];

			// Initialize curl
			$ch = curl_init($feed_url);

			// Set not to output 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

			// Execute curl and get the response
			$xml = curl_exec($ch);

			// Close the curl
			curl_close($ch);
			
			// Cache the output for 24 hours.
			$cache->set('xefeed_'.$from, $xml, 86400);
			Log::instance()->add(Log::NOTICE, date("Y-m-d H:i:s").". Trying to fetch latest datafeed from XE: {$feed_url}.");
		}

		// Create new dom document
		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		$doc->loadXML($xml);

		// Get the currencies node list
		$currencies = $doc->getElementsByTagName('currency');

		// Set default rate
		$rate = 0;

		foreach ($currencies as $currency)
		{
			$values = array();
			foreach ($currency->childNodes as $node)
			{
				$values[$node->nodeName] = $node->nodeValue;
			}

			if ($values['csymbol'] == $to)
			{
				$rate = $values['crate'];
				break;
			}
		}

		/// Write log to check function execute time
		Log::instance()->add(Log::NOTICE, date("Y-m-d H:i:s")." from ".$from." to ".$to." rate ".$rate." amount ".$amount." status : XE");
		
		return $amount * $rate;
	}

}
