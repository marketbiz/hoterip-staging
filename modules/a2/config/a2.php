<?php

return array(

	/*
	 * The Authentication library to use
	 * Make sure that the library supports:
	 * 1) A get_user method that returns FALSE when no user is logged in
	 *	  and a user object that implements Acl_Role_Interface when a user is logged in
	 * 2) A static instance method to instantiate a Authentication object
	 *
	 * array(CLASS_NAME,array $arguments)
	 */
	'lib' => array(
		'class'  => 'A1', // (or AUTH)
		'params' => array('a1')
	),

	/**
	 * Throws an a2_exception when authentication fails
	 */
	'exception' => FALSE,

	/*
	 * The ACL Roles (String IDs are fine, use of ACL_Role_Interface objects also possible)
	 * Use: ROLE => PARENT(S) (make sure parent is defined as role itself before you use it as a parent)
	 */
	'roles' => array
	(
		// ADD YOUR OWN ROLES HERE
		'user'	=>	'guest'
	),

	/*
	 * The name of the guest role 
	 * Used when no user is logged in.
	 */
	'guest_role' => 'guest',

	/*
	 * The ACL Resources (String IDs are fine, use of ACL_Resource_Interface objects also possible)
	 * Use: ROLE => PARENT (make sure parent is defined as resource itself before you use it as a parent)
	 */
	'resources' => array
	(
		// ADD YOUR OWN RESOURCES HERE
		//'blog'	=>	NULL
//		'hotel' => NULL,
//		'booking' => NULL,
//		'room' => NULL,
//		'item' => NULL,
//		'campaign' => NULL,
//		'earlybird' => NULL,
//		'lastminute' => NULL,
//		'compulsory' => NULL,
//		'hotel_photo' => NULL,
//		'hotel_video' => NULL,
//    'user' => NULL,
//		'review' => NULL,
//		'currency' => NULL,
	),

	/*
	 * The ACL Rules (Again, string IDs are fine, use of ACL_Role/Resource_Interface objects also possible)
	 * Split in allow rules and deny rules, one sub-array per rule:
	     array( ROLES, RESOURCES, PRIVILEGES, ASSERTION)
	 *
	 * Assertions are defined as follows :
			array(CLASS_NAME,$argument) // (only assertion objects that support (at most) 1 argument are supported
			                            //  if you need to give your assertion object several arguments, use an array)
	 */
	'rules' => array
	(
		'allow' => array
		(
			/**
			 * Admin role
			 */
			array(
				'role' => 'admin',
				'resource' => 'all',
				'privilege' => array('hotel', 'hoterip'),
			),
      array(
        'role' => 'admin',
        'resource' => 'email',
        'privilege' => array('receive'),
      ),
			array(
				'role' => 'admin',
				'resource' => 'hotel',
				'privilege' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Hotel_Assertion'),
			),
      array(
				'role' => 'admin',
				'resource' => 'hotel_photo',
				'privilege' => array('manage', 'add', 'main'),
			),
			array(
				'role' => 'admin',
				'resource' => 'hotel_photo',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Photo_Assertion'),
			),
      array(
				'role' => 'admin',
				'resource' => 'booking',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'admin',
				'resource' => 'booking',
				'privilage' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Booking_Assertion'),
			),
			array(
				'role' => 'admin',
				'resource' => 'room',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'admin',
				'resource' => 'room',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Room_Assertion'),
			),
      array(
				'role' => 'admin',
				'resource' => 'cancellation',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'admin',
				'resource' => 'cancellation',
				'privilege' => array('edit', 'delete'),
				'assertion' => array('Acl_Admin_Cancellation_Assertion'),
			),
      array(
				'role' => 'admin',
				'resource' => 'item',
				'privilege' => array('manage'),
			),
      array(
				'role' => 'admin',
				'resource' => 'itemb2b',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'admin',
				'resource' => 'item',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
				'role' => 'admin',
				'resource' => 'itemb2b',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
 				'role' => 'admin',
				'resource' => 'benefit',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'promotion',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'admin',
				'resource' => 'promotion_search',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'admin',
				'resource' => 'campaign',
				'privilege' => array('manage', 'add'),
			),
      array(
				'role' => 'admin',
				'resource' => 'surcharge',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'admin',
				'resource' => 'restriction',
				'privilege' => array('manage'),
			),
      array(
				'role' => 'admin',
				'resource' => 'log',
				'privilege' => array('hotel', 'all'),
			),
      
      array(
				'role' => 'admin',
				'resource' => 'report',
				'privilege' => array('finance', 'finance_all', 'booking_all'),
			),
			array(
				'role' => 'admin',
				'resource' => 'admin',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'user',
				'privilege' => array('manage', 'edit', 'delete', 'unsubscribe'),
			),
      array(
				'role' => 'admin',
				'resource' => 'subscribe',
				'privilege' => array('manage', 'edit', 'delete', 'unsubscribe', 'subscribe'),
			),
			array(
				'role' => 'admin',
				'resource' => 'hotel',
				'privilege' => array('manage', 'add', 'delete', 'assign'),
			),
      array(
				'role' => 'admin',
				'resource' => 'banner',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'feature',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'deal',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'deal_hotel',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'destination',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'admin',
				'resource' => 'review',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'district',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'admin',
				'resource' => 'city',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'admin',
				'resource' => 'country',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'admin',
				'resource' => 'page',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'admin',
				'resource' => 'announcement',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'admin',
				'resource' => 'email',
				'privilege' => array('send'),
			),
      array(
				'role' => 'admin',
				'resource' => 'item',
				'privilege' => array('export'),
			),
      array(
				'role' => 'admin',
				'resource' => 'itemb2b',
				'privilege' => array('export'),
			),
      array(
        'role' => 'admin',
        'resource' => 'setting',
        'privilege' => array('edit'),
      ),
      array(
        'role' => 'admin',
        'resource' => 'csv',
        'privilege' => array('manage', 'add', 'edit', 'delete'),
      ),
			array(
				'role' => 'admin',
				'resource' => 'api',
				'privilege' => array('manage', 'add', 'edit','delete'),
			),
			array(
				'role' => 'admin',
				'resource' => 'promotionpage',
				'privilege' => array('manage', 'add', 'edit','delete'),
			),

      /**
			 * Extranet role
			 */
			array(
				'role' => 'extranet',
				'resource' => 'all',
				'privilege' => array('hotel'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'hotel',
				'privilege' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Hotel_Assertion'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'hotel_photo',
				'privilege' => array('manage', 'add', 'main'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'hotel_photo',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Photo_Assertion'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'booking',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'booking',
				'privilage' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Booking_Assertion'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'room',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'room',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Room_Assertion'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'cancellation',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'cancellation',
				'privilege' => array('edit', 'delete'),
				'assertion' => array('Acl_Admin_Cancellation_Assertion'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'item',
				'privilege' => array('manage'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'itemb2b',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'item',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'itemb2b',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'promotion',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'campaign',
				'privilege' => array('manage', 'add'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'surcharge',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'restriction',
				'privilege' => array('manage'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'report',
				'privilege' => array('finance'),
			),
			array(
				'role' => 'extranet',
				'resource' => 'announcement',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'extranet',
				'resource' => 'log',
				'privilege' => array('hotel'),
			),
      
      /**
			 * Hoterip staff role
			 */
			array(
				'role' => 'hoterip-staff',
				'resource' => 'all',
				'privilege' => array('hotel', 'hoterip'),
			),
      array(
        'role' => 'hoterip-staff',
        'resource' => 'email',
        'privilege' => array('receive'),
      ),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'hotel',
				'privilege' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Hotel_Assertion'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'hotel_photo',
				'privilege' => array('manage', 'add', 'main'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'hotel_photo',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Photo_Assertion'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'booking',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'booking',
				'privilage' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Booking_Assertion'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'room',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'room',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Room_Assertion'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'night',
        'privilege' => array('manage', 'add'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'night',
				'privilege' => array('edit', 'delete'),
				'assertion' => array('Acl_Admin_Night_Assertion'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'cancellation',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'cancellation',
				'privilege' => array('edit', 'delete'),
				'assertion' => array('Acl_Admin_Cancellation_Assertion'),
			),  
      array(
				'role' => 'hoterip-staff',
				'resource' => 'item',
				'privilege' => array('manage'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'itemb2b',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'item',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'itemb2b',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
 				'role' => 'hoterip-staff',
				'resource' => 'benefit',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'promotion',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'promotion_search',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'campaign',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'campaign',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Campaign_Assertion'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'surcharge',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'restriction',
				'privilege' => array('manage'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'log',
				'privilege' => array('hotel', 'all'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'report',
				'privilege' => array('finance', 'finance_all', 'booking_all'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'user',
				'privilege' => array('manage', 'edit', 'unsubscribe'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'subscribe',
				'privilege' => array('manage', 'edit', 'delete', 'unsubscribe', 'subscribe'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'hotel',
				'privilege' => array('manage', 'add', 'delete', 'assign'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'banner',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'review',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'district',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'city',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'announcement',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'country',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'page',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'hoterip-staff',
				'resource' => 'email',
				'privilege' => array('send'),
			),
			array(
				'role' => 'hoterip-staff',
				'resource' => 'api',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),

			/**
			 * QA role
			 */
			array(
				'role' => 'quality-assurance',
				'resource' => 'all',
				'privilege' => array('hotel'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'hotel',
				'privilege' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Hotel_Assertion'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'hotel_photo',
				'privilege' => array('manage', 'add', 'main'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'hotel_photo',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Photo_Assertion'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'booking',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'booking',
				'privilage' => array('read', 'edit'),
				'assertion' => array('Acl_Admin_Booking_Assertion'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'room',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'room',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Room_Assertion'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'cancellation',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'cancellation',
				'privilege' => array('edit', 'delete'),
				'assertion' => array('Acl_Admin_Cancellation_Assertion'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'item',
				'privilege' => array('manage'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'itemb2b',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'item',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'itemb2b',
				'privilege' => array('edit'),
				'assertion' => array('Acl_Admin_Item_Assertion'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'benefit',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'promotion',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'promotion_search',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'campaign',
				'privilege' => array('manage', 'add'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'campaign',
				'privilege' => array('read', 'edit', 'delete'),
				'assertion' => array('Acl_Admin_Campaign_Assertion'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'surcharge',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'restriction',
				'privilege' => array('manage'),
			),
			array(
				'role' => 'quality-assurance',
				'resource' => 'announcement',
				'privilege' => array('manage', 'add', 'edit', 'delete'),
			),
      array(
				'role' => 'quality-assurance',
				'resource' => 'log',
				'privilege' => array('hotel'),
			),

			/**
			 * API role
			 */
			array(
				'role' => 'api',
				'resource' => 'api',
				'privilege' => array('edit'),
			),

			/*
			 * ADD YOUR OWN ALLOW RULES HERE 
			 *
			'ruleName1' => array(
				'role'      => 'guest',
				'resource'  => 'blog',
				'privilege' => 'read'
			),
			'ruleName2' => array(
				'role'      => 'admin'
			),
			'ruleName3' => array(
				'role'      => array('user','manager'),
				'resource'  => 'blog',
				'privilege' => array('delete','edit')
			)
			 */
		),
		'deny' => array
		(
			// ADD YOUR OWN DENY RULES HERE
		)
	)
);