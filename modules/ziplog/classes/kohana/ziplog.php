<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Zip Log
 *
 * @package	   ZipLog
 * @author	   Sam Fajar <samfajar1206@gmail.com>
 * @license    Hoterip.com
 */

class Kohana_ZipLog
{

	protected $_config;
	protected $_year;
	protected $_month;
	protected $_days;
	protected $_files;

	public function __construct($files) {
		try {
			
			$this->_config =  Kohana::$config->load('ziplog');
			$this->_files = $files;

		} catch (Exception $e) {
			$error = $e->getTrace();
			Log::instance()->add(Log::INFO, print_r($error[0], true));   
			exit();				
		}
	}

	public function getYearAndMonth($date_interval = NULL)
	{
		try {
			$date = strtotime(date('Y-m-d'));
			if(!empty($date_interval))
				$date = date('Y-m-d', strtotime("-$date_interval days", $date));
			else	
				$date = date('Y-m-d', strtotime("-1 month", $date));
			$date = explode('-', $date);

			if(is_dir($this->_config['dir_file']['vacation'].DIRECTORY_SEPARATOR.$date[0])){
				if(is_dir($this->_config['dir_file']['vacation'].DIRECTORY_SEPARATOR.$date[0].DIRECTORY_SEPARATOR.$date[1])){

					$this->_year = $date[0];
					$this->_month = $date[1];
					$this->_days = $date[2];

				}else{
					throw new Exception("Error Processing Request", 1);
				}
			}else{
				throw new Exception("Error Processing Request", 1);
			}
		} catch (Exception $e) {
			$error = $e->getTrace();
			Log::instance()->add(Log::INFO, print_r($error[0], true));   
			exit();	
		}	
	}

	public static function factory($files)
	{
		return new ZipLog($files);
	}

	/**
	*@return Directory location file log
	*@param String year
	*@param String Month
	**/
	public function getLocationFileLog()
	{
		try {
			return $this->_config['dir_file']['vacation'].DIRECTORY_SEPARATOR.$this->_year.DIRECTORY_SEPARATOR.$this->_month.DIRECTORY_SEPARATOR;
		} 
		/**
		*ERROR HANDLE
		*/
		catch (Exception $e) {
			$error = $e->getTrace();
			Log::instance()->add(Log::INFO, print_r($error[0], true));   
			exit();			
		}		
	}

	/**
	*@param Directory file log
	**/
	public function deleteLog($file_log, $perfile = FALSE)
	{
		try {

			if($perfile == TRUE){
				unlink(realpath($file_log) . DIRECTORY_SEPARATOR . $this->_days.'.php');
			}
			else{
				//scan dir
			$files = array_diff(scandir($file_log), array('.', '..'));

				// loog per file log
			foreach ($files as $file)
			{
					//delete
				unlink(realpath($file_log) . DIRECTORY_SEPARATOR . $file);
			}

				//delete dir month
			rmdir($file_log);
			}
		}
			/**
			*ERROR HANDLE
			*/
			catch (Exception $e) {
				$error = $e->getTrace();
				Log::instance()->add(Log::INFO, print_r($error[0], true));    					
				exit();
			}	
		}


		public function create()
		{
			Log::instance()->add(Log::INFO, 'Start Zipping File');

			$this->getYearAndMonth();
			try {
				foreach ($this->_files as $file) {


					//get dir_logs
					$file_log = $this->getLocationFileLog();

					//scan dir
					$files = array_diff(scandir($file_log), array('.', '..'));

					// if(empty($files)){
					// 	throw new Exception("Empty Files Logs", 1);
					// }

					//chdir
					chdir($file_log);

					set_time_limit(0);

					$zip = new ZipArchive();

					if ($zip->open($this->_month.'.zip', ZipArchive::CREATE)!==TRUE) {
						throw new Exception("Cannot Open File After Create", 1);
					}

					//add all zip
					$zip->addGlob('*.php');
					$zip->close();

					//move zip
					rename($this->_month.'.zip', $this->_config['dir_result_zip']['vacation'].DIRECTORY_SEPARATOR.$this->_year.DIRECTORY_SEPARATOR.$this->_month.'.zip');

					$this->deleteLog($file_log);

				}
				Log::instance()->add(Log::INFO, 'Finished Zipping File');
			} 
			/**
			*ERROR HANDLE
			*/
			catch (Exception $e) {
				$error = $e->getTrace();
				Log::instance()->add(Log::INFO, print_r($error[0], true));    					
				exit();
			}	
		}

		/**
		*@return Result Zip per date interval
		*@param Date Interval
		**/
		public function createPerFileLog($date_interval)
		{
			Log::instance()->add(Log::INFO, 'Start Zipping File Whit Date Interval');

			// set date interval
			$this->getYearAndMonth($date_interval);

			try {
				foreach ($this->_files as $file) {

					//get dir_logs
					$file_log = $this->getLocationFileLog();

					//scan dir
					$files = array_diff(scandir($file_log), array('.', '..'));

					// if(empty($files)){
					// 	throw new Exception("Empty Files Logs", 1);
					// }

					//chdir
					chdir($file_log);

					set_time_limit(0);

					$zip = new ZipArchive();

					if ($zip->open($this->_days.'.zip', ZipArchive::CREATE)!==TRUE) {
						throw new Exception("Cannot Open File After Create", 1);
					}

					//add all zip
					$zip->addGlob($this->_days.'.php');
					$zip->close();

					//move zip
					// rename($this->_month.DIRECTORY_SEPARATOR.$this->_days.'.zip', $this->_config['dir_result_zip']['vacation'].DIRECTORY_SEPARATOR.$this->_year.DIRECTORY_SEPARATOR.$this->_month.DIRECTORY_SEPARATOR.$this->_days.'.zip');

					$this->deleteLog($file_log, TRUE);

				}
				Log::instance()->add(Log::INFO, 'Finished Zipping File Whit Date Interval');
			} 
			/**
			*ERROR HANDLE
			*/
			catch (Exception $e) {
				$error = $e->getTrace();
				Log::instance()->add(Log::INFO, print_r($error[0], true));    					
				exit();
			}				
		}		
	}