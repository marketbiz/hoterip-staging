<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Costum Log
 *
 * @package	   Costum Log kohana
 * @author	   Sam Fajar <samfajar1206@gmail.com>
 * @license    Hoterip.com
 */
class Kohana_CustomLog {

    protected $_timestamp;
    protected $_config;

    public function __construct() {
        $this->_config = Kohana::$config->load('customlog');
        $this->_timestamp = 'Y-m-d H:i:s';
    }

    public static function factory() {
        return new CustomLog();
    }

    public function getDirectory() {
        return realpath($this->_config['dir']) . DIRECTORY_SEPARATOR;
    }

    public function makeDir($directory) {
        try {
            // Create the yearly directory
            mkdir($directory, 02777);

            // Set permissions (must be manually set to fix umask issues)
            chmod($directory, 02777);
        } catch (Exception $e) {
            Log::instance()->add(Log::INFO, 'Error : Failed make Dir ' . $directory);

            //add log last tarce
            $error = $e->getTrace();
            Log::instance()->add(Log::INFO, print_r($error[0], true));
        }
    }

    public function makeFile($filename) {
        try {
            // Create the log file
            file_put_contents($filename, Kohana::FILE_SECURITY . ' ?>' . PHP_EOL);

            // Allow anyone to write to log files
            chmod($filename, 0666);
        } catch (Exception $e) {

            Log::instance()->add(Log::INFO, 'ERROR : Failed make file ' . $filename);

            //add log last tarce
            $error = $e->getTrace();
            Log::instance()->add(Log::INFO, print_r($error[0], true));
        }
    }

    public function add($dir, $level, $message) {
        try {
            $messages = array(
                'level' => $level,
                'time' => date($this->_timestamp),
                'body' => $message,
                'dir' => $this->_config['type'][$dir],
            );

            $this->writeLog($messages);
        } catch (Exception $e) {
            Log::instance()->add(Log::INFO, 'ERROR : Failed build message ');

            //add log last tarce
            $error = $e->getTrace();
            Log::instance()->add(Log::INFO, print_r($error[0], true));
        }
    }

    public function writeLog($messages) {
        try {
            $directory = $this->getDirectory();

            $directory = $directory . $messages['dir'] . DIRECTORY_SEPARATOR;
            if (!is_dir($directory)) {
                $this->makeDir($directory);
            }

            $directory = $directory . date('Y');
            if (!is_dir($directory)) {
                $this->makeDir($directory);
            }

            $directory .= DIRECTORY_SEPARATOR . date('m');
            if (!is_dir($directory)) {
                $this->makeDir($directory);
            }

            $filename = $directory . DIRECTORY_SEPARATOR . date('d') . EXT;
            if (!file_exists($filename)) {
                $this->makeFile($filename);
            }

            // Write each message into the log file
            // Format: time --- level: body
            file_put_contents($filename, PHP_EOL . $messages['time'] . ' --- ' . $messages['level'] . ': ' . $messages['body'], FILE_APPEND);
        } catch (Exception $e) {

            // add log
            Log::instance()->add(Log::INFO, 'Failed write Costum Log');

            //add log last tarce
            $error = $e->getTrace();
            Log::instance()->add(Log::INFO, print_r($error[0], true));
        }
    }

}
