<?php
/**
 * LOJAI 帮助文件
 */
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');


if ((DEBUG_MODE & 2) != 2)
{
  $smarty->caching = true;
}
	
   /* 缓存编号 */
$cur=$_COOKIE['ECS']['currency'] ;//PHPOSS 201106
$cache_id = sprintf('%X', hash('crc32b',$_SESSION['email'].'-'.$_SESSION['user_rank'] . '-' . $_CFG['lang'].'-'.$cur));

   if (!$smarty->is_cached('contact_help.dwt', $cache_id))
   {
   		assign_template();
	        $smarty->assign('best_goods',      get_recommend_goods('best'));    // 推荐商品
		$smarty->assign('new_goods',       get_recommend_goods('new'));     // 最新商品
		$smarty->assign('hot_goods',       get_recommend_goods('hot'));     // 热点文章
		$smarty->assign('helps',           get_shop_help());       // 网店帮助
                 $smarty->assign('article_cat',    get_article_sub());   // 子分类及文章
                $smarty->assign('new_articles',    get_relevant_articles());   // 相关文章
		$smarty->assign('articles_size', count(get_relevant_articles())/2);
   }
   $smarty->display('contact_help.dwt', $cache_id);
/**
 * 获得相关的文章列表。
 *
 * @access  private
 * @return  array
 */
function get_relevant_articles(){
     $sql = 'SELECT a.article_id, a.title, ac.cat_name, a.add_time, a.file_url, a.open_type, ac.cat_id, ac.cat_name ' .
            ' FROM ' . $GLOBALS['ecs']->table('article') . ' AS a, ' .
                $GLOBALS['ecs']->table('article_cat') . ' AS ac' .
            ' WHERE a.is_open = 1 AND a.cat_id = ac.cat_id AND ac.cat_name LIKE "Contact Help"' .
            ' ORDER BY a.article_type DESC, a.add_time DESC ';
    $res = $GLOBALS['db']->getAll($sql);

    $arr = array();
    foreach ($res AS $idx => $row)
    {
        $arr[$idx]['id']          = $row['article_id'];
        $arr[$idx]['title']       = $row['title'];
        $arr[$idx]['short_title'] = $GLOBALS['_CFG']['article_title_length'] > 0 ?
                                        sub_str($row['title'], $GLOBALS['_CFG']['article_title_length']) : $row['title'];
        $arr[$idx]['cat_name']    = $row['cat_name'];
        $arr[$idx]['add_time']    = local_date($GLOBALS['_CFG']['date_format'], $row['add_time']);
        $arr[$idx]['url']         = $row['open_type'] != 1 ?
                                        build_uri('article', array('aid' => $row['article_id']), $row['title']) : trim($row['file_url']);
        $arr[$idx]['cat_url']     = build_uri('article_cat', array('acid' => $row['cat_id']), $row['cat_name']);
    }

    return $arr;
}
function get_article_sub(){
     $sql = 'SELECT cat_id,cat_name ' .
                'FROM ' . $GLOBALS['ecs']->table('article_cat') .
                '  WHERE parent_id = 135';
     $res = $GLOBALS['db']->getAll($sql);
     $arr = array();
     foreach ($res AS $idx => $row){
          $sql = 'SELECT a.article_id, a.title, ac.cat_name, a.add_time, a.file_url, a.open_type, ac.cat_id, ac.cat_name ' .
            ' FROM ' . $GLOBALS['ecs']->table('article') . ' AS a, ' .
                $GLOBALS['ecs']->table('article_cat') . ' AS ac' .
            ' WHERE a.is_open = 1 AND a.cat_id = ac.cat_id AND ac.cat_id= '.$row['cat_id'] .
            ' ORDER BY a.article_type DESC, a.add_time DESC ';
          $ress = $GLOBALS['db']->getAll($sql);
          $arrs = array();
          $arr[$idx]['id']=$row['cat_id'];
          $arr[$idx]['title']= $row['cat_name'];
          foreach ($ress AS $idxs => $rows){
                $arrs[$idxs]['id']          = $rows['article_id'];
                $arrs[$idxs]['title']       = $rows['title'];
                $arrs[$idxs]['short_title'] = $GLOBALS['_CFG']['article_title_length'] > 0 ?
                                                sub_str($rows['title'], $GLOBALS['_CFG']['article_title_length']) : $rows['title'];
                $arrs[$idxs]['cat_name']    = $rows['cat_name'];
                $arrs[$idxs]['add_time']    = local_date($GLOBALS['_CFG']['date_format'], $rows['add_time']);
                $arrs[$idxs]['url']         = $rows['open_type'] != 1 ?
                                                build_uri('article', array('aid' => $rows['article_id']), $rows['title']) : trim($rows['file_url']);
                $arrs[$idxs]['cat_url']     = build_uri('article_cat', array('acid' => $rows['cat_id']), $rows['cat_name']);
          }
	  $arr[$idx]['arr']=$arrs;
     }
      return $arr;
}   
    
?>