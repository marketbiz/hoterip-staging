<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/kohana/core' . EXT;

if (is_file(APPPATH . 'classes/kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Asia/Kuala_Lumpur');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
// Kohana::$environment = Kohana::DEVELOPMENT;
Kohana::$environment = Kohana::PRODUCTION;

if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
    'base_url' => '/',
    'index_file' => FALSE,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
//    'auth'      => MODPATH.'auth',      // Basic authentication
    'cache' => MODPATH . 'cache', // Caching with multiple backends
//    'codebench' => MODPATH.'codebench', // Benchmarking tool
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'orm' => MODPATH . 'orm', // Object Relationship Mapping
//    'unittest'  => MODPATH.'unittest',  // Unit testing
//    'userguide' => MODPATH.'userguide', // User guide and API documentation
    'a1' => MODPATH . 'a1', // Authentication
    'a2' => MODPATH . 'a2', // Authorization
    'acl' => MODPATH . 'acl', // Access Control List
    'notice' => MODPATH . 'notice', // Notice     
    'kostache' => MODPATH . 'kostache', // Mustache template
    'email' => MODPATH . 'email', // Swift Mailer
    'csv' => MODPATH . 'csv', // CSV Reader and writer
    'pagination' => MODPATH . 'pagination', // Pagination 
    'ce' => MODPATH . 'ce', // Currency Exchange
    'uuid' => MODPATH . 'uuid', // UUID
    'kompdf' => MODPATH . 'kompdf', // kompdf
    'customlog' => MODPATH . 'customlog', // logtest
    'cron' => MODPATH . 'cron', //cron
    'ziplog' => MODPATH . 'ziplog', //zip
));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
// Authentication shortcuts
Route::set('auth', '<action>', array(
            'action' => '(login|logout|forgot|reset)'
        ))
        ->defaults(array(
            'controller' => 'auth'
        ));

// Api shortcuts
Route::set('api', '<directory>(/<controller>(/<action>))', array(
            'controller' => '(dropbox|request|channel|requesta)',
            'action' => '(csv|xml|request|get|rates|download)',
        ))
        ->defaults(array(
            'directory' => 'api',
        ));
// Trivago shortcuts
Route::set('trivago', '<directory>(/<controller>(/<action>))', array(
            'controller' => '(dropbox|availability)',
            'action' => '(csv|request)',
        ))
        ->defaults(array(
            'directory' => 'trivago',
        ));

// // staah shortcuts
// Route::set('staah', '<directory>(/<controller>(/<action>))',
//     array(
//         'controller' => '(api|reservation)',
//         'action'     => '(request)',
//     ))
//     ->defaults(array(
//         'directory' => 'staah',
//     ));
// Trivagov2 shortcuts
Route::set('trivago', 'api(/<directory>(/<controller>(/<action>)))', array(
            'directory' => '(trivagov2)',
            'controller' => '(availability)',
            'action' => '(request)',
        ))
        ->defaults(array(
            'directory' => 'trivagov2',
        ));

// ApiV2 shortcuts
Route::set('vacation', 'apiv2(/<directory>(/<controller>(/<action>)))', array(
            'directory' => '(vacation)',
            'controller' => '(availability|reservation|cancel|info)',
            'action' => '(request)',
        ))
        ->defaults(array(
            'directory' => 'vacation',
        ));

// ApiV2 shortcuts
Route::set('siteminder', 'apiv2(/<directory>(/<controller>(/<action>)))', array(
            'directory' => '(siteminder|staah)',
            'controller' => '(api|availability)',
            'action' => '(request)',
        ))
        ->defaults(array(
            'directory' => 'siteminder',
        ));

// Account shortcuts
Route::set('account', 'account/<action>', array(
            'action' => '(profile|password)'
        ))
        ->defaults(array(
            'controller' => 'account'
        ));

// Error route
Route::set('error', 'error/<action>(/<message>)', array('action' => '[0-9]++', 'message' => '.+'))
        ->defaults(array(
            'controller' => 'error',
        ));

// Default routes
Route::set('default', '(<controller>(/<action>(/<id>)))')
        ->defaults(array(
            'controller' => 'home',
            'action' => 'dashboard',
        ));


//husen tes

Route::set('husen', '<controller>/<action>')
        ->defaults(array(
            'directory' => 'folder',
            'controller' => 'husen',
            'action' => 'index',
        ));
Route::set('cron', '<controller>/<action>')
        ->defaults(array(
            'directory' => 'folder',
            'controller' => 'cron',
            'action' => 'index',
        ));
/**
 * Set default session driver
 */
Session::$default = 'database';

/**
 * Set default cookie salt
 */
Cookie::$salt = 'a1e5d2e6f43cfc6ffb9f427714121960';

/**
 * Set upload default directory
 */
//Upload::$default_directory = 'media/upload';
