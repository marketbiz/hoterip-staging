<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'commission_rate_too_low' => 'Your commission rate is too low.',
	'commission_rate_too_high' => 'Your commission rate is too high.',
  'commission_rate_changed' => 'Your commisssion rate has been changed. Commission rate can drastically improve your hotel rank.',
  /// Minimum rate commision
	'commission_rate_minimum_too_low' => 'Your minimum commission rate is too low.',
	'commission_rate_minimum_too_high' => 'Your minimum commission rate is too high.',
  'commission_rate_minimum_changed' => 'Minimum commisssion rate has been changed.',
   /// Hotel Active
  'used_by_features_deals' => 'This Hotel is used by Landing Page, Featured Hotels, or Hot Deals Banners. Please, Contact our Administrator.',
);