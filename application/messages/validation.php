<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'Upload::not_empty' => ':field must not be empty',
	'Upload::valid'			=> ':field is not valid',
	'Upload::type'			=> ':field type must :param2'
);
