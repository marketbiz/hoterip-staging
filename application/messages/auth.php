<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'login_failed' => 'Please check your username and password.',
  'forgot_success' => 'Instruction to change your password has been sent to your email address.',
  'email_not_found' => 'Email is not registed.',
  'reset_success' => 'Password successfully changed.',
  'reset_failed' => 'Failed to change password. Your key is not valid.'
);