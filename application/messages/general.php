<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'save_success' => 'Save success.',
	'save_success_with_link' => 'Save success. :link',
	'save_failed' => 'Save failed. Please check these following errors.',
	'delete_success' => 'Delete success.',
	'delete_failed' => 'Delete failed. :message',
	'not_authorized' => 'You are not authorized to perform this action.',
	'announcements' => 'Announcements title can not be empty.',
	'selected_hotel_changed' => 'Your action is not saved because selected hotel has been changed. If you still want to continue please submit the form again.',
	'room_booked' => 'This room has booking record.'
);