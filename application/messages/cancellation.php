<?php defined('SYSPATH') or die('No direct script access.');

return array(
  'no_room_selected' => 'You must select at least one room.',
  'room_cancellation_not_exist' => ':room_name cancellation policy does not exist. You must add cancellation policy for this room.',
  'date_overlap' => 'Start date may not exceed the end date.',
  'cancellation_overlap' => 'Your cancellation policy date is overlapping another cancellation policy.',
);