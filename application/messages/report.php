<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'refund_success' => 'Email notification has been sent to guest and this booking point has been refunded.',
  'refund_failed' => 'Failed to refund this booking. Booking must already canceled and not refunded yet.',
);