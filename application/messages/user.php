<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'unsubscribe_success' => 'User has been unsubscribed from newsletter',
  'unsubscribe_failed' => 'Failed to unsubsribe user from newsletter',
);