<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'no_assigned_hotels' => 'You do not have any assigned hotels.',
);