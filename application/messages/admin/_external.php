<?php defined('SYSPATH') or die('No direct script access.');

return array(
  'Model_Admin::username_available' => 'Username has already used by another admin.',
  'Model_Admin::email_available' => 'Email has already used by another admin.',
);