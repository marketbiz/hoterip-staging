<?php defined('SYSPATH') or die('No direct script access.');

return array(
	/*
	 * Vacation
	 */
	'vacation_rateplan_notif_save_success' => 'API Rateplan Notification succeed.',
	'vacation_rateplan_notif_save_failure' => 'API Rateplan Notification failed.',
	'vacation_rateplan_amount_save_success' => 'API Hotel Rate Amount Notification succeed.',
	'vacation_rateplan_amount_notif_save_failure' => 'API Hotel Rate Amount Notification failed.',
	'vacation_availability_vacation_save_success' => 'API Hotel Availability Notification succeed.',
	'vacation_availability_vacation_save_failure' => 'API Hotel Availability Notification failed.',
		/*
	  * Rategain
	  */
	'rategain_canceled_success' => 'Rategain Cancellation Notification succees.',
	'rategain_canceled_failed' => 'Rategain Cancellation Notification succees.',
		/*
	  * Siteminder
	  */
	'siteminder_canceled_success' => 'Siteminder Cancellation Notification succees.',
	'siteminder_canceled_failed' => 'Siteminder Cancellation Notification succees.',
);