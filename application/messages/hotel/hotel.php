<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'permalink_available' => 'Permalink is already used by another hotel. Please choose another permalink.',
);