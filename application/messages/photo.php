<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'manage_photos' => 'Back to manage photos',
  'add_photo' => 'Add another photo',
);