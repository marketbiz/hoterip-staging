<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'locked_by' => 'This booking is currently locked by :username (:name). You need to unlock this booking and lock it again with your user before you can edit this booking.',
	'lock_needed' => 'You need to lock this booking before do this action.',
	'lock' => 'Locked this booking',
	'unlock' => 'Unlocked this booking',
  'no_show_cancel_failed' => 'You can not cancel this booking and set as no show because today has not passed guest check-in date.'
);