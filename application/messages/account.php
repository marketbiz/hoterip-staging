<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'password_incorrect' => 'Your current password is incorrect. Please try again.',
);