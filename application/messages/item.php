<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'import_success' => 'Import Success',
	'import_failed' => 'Import Failed',
	'room_not_valid' => 'Room not valid',
  'no_room' => 'You must have at least one room.',
  'no_cancellation_policy' => 'You must add cancellation policy for this room before adding stocks and prices.',
  'Model_Cancellation::valid_date' => 'Cancellation policy for this period does not exist.',
  'Model_Item::valid_extranet_price' => 'You have promotion that have discount amount same or more than entered price.',
  'import_success' => 'Import Success',
  'net_price' => 'Net price must be lower than price',
  'cancelation_date' => 'Cancelations only available until ',
  'promotion_date' => 'Promotions only available until ',
  'minimum_night' => 'Minimum Night must more than 0 ',
  'price_rule' => 'One or more item price did not meet minimum or maximum price margin.'
);