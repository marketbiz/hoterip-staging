<?php defined('SYSPATH') or die('No direct script access.');

return array(
  'no_benefit' => '\'benefit\' cannot be less or equal to 0 while \'other benefit\' is empty.',
	'no_campaigns' => 'You must have at least one campaign.',
  'no_room_selected' => 'You must select at least one room.',
  'no_language_selected' => 'You must select at least one store front language.',
  'booking_date_overlap' => 'Booking start date may not exceed the booking end date.',
  'stay_date_overlap' => 'Stay start date may not exceed the stay end date.',
  'no_cancellation' => ':room_name does not have cancellation policy for entered stay date. Please add cancellation policy first.',
  'discount_rate_too_big' => 'Your discount rate should be less than 100',
  'discount_amount_too_big' => 'You have room price same or below discount amount in this stay period.',
  'free_night_too_big' => 'Free night must be less than Minimum number of nights.',
	'days_in_advance_empty' => 'Days in advance cannot be zero or less than zero',
	'within_days_of_arrival_empty' => 'Within days of arrival cannot be zero or less than zero',
  'booking_start_date_less_than_now' => 'Booking start date must more than or equal today',
  'stay_start_date_less_than_now' => 'Stay start date must more than or equal today',

);