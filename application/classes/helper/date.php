<?php

defined('SYSPATH') or die('No direct script access.');

class Helper_Date {

	public static function create_date($date)
	{
		if (is_numeric($date))
		{
			$d = new DateTime();
			$d->setTimestamp($date);
		}
		elseif (is_string($date))
		{
			$d = new DateTime($date);
		}
		
		return $d;
	}

}
