<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Photo_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(hotel_photos.id)'), 'count')
			)
			->from('hotel_photos')
			->join('hotels')->on('hotels.id', '=', 'hotel_photos.hotel_id')
			->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('hotel_photos.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}