<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Cancellation_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(cancellations.id)'), 'count')
			)
			->from('cancellations')
			->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('admins_hotels')->on('hotels.id', '=', 'admins_hotels.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('cancellations.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}