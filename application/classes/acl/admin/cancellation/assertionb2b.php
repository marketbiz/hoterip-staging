<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Cancellation_Assertionb2b implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(cancellations.id)'), 'count')
			)
			->from('cancellations')
			->join('campaignsb2b_cancellations')->on('campaigns_cancellationsb2b.cancellation_id', '=', 'cancellations.id')
      ->join('campaignb2bs')->on('campaignb2bs.id', '=', 'campaignsb2b_cancellations.campaignb2b_id')
      ->join('campaignsb2b_rooms')->on('campaignsb2b_rooms.campaignb2b_id', '=', 'campaignb2bs.id')
      ->join('rooms')->on('rooms.id', '=', 'campaignsb2b_rooms.room_id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('admins_hotels')->on('hotels.id', '=', 'admins_hotels.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('cancellations.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}