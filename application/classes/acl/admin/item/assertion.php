<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Item_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(items.id)'), 'count')
			)
			->from('items')
			->join('rooms')->on('rooms.id', '=', 'items.room_id')
			->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'rooms.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('items.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}