<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Campaign_Assertionb2b implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(campaignb2bs.id)'), 'count')
			)
			->from('campaignb2bs')
      ->join('campaignsb2b_rooms')->on('campaignsb2b_rooms.campaignb2b_id', '=', 'campaignb2bs.id')
			->join('rooms')->on('rooms.id', '=', 'campaignsb2b_rooms.room_id')
			->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'rooms.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('campaignb2bs.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}