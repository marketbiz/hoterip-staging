<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Campaign_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(campaigns.id)'), 'count')
			)
			->from('campaigns')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
			->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
			->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'rooms.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('campaigns.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}