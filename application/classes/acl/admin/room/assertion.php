<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Room_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(rooms.id)'), 'count')
			)
			->from('rooms')
			->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('admins_hotels')->on('hotels.id', '=', 'admins_hotels.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('rooms.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}