<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Hotel_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(hotel_id)'), 'count')
			)
			->from('admins_hotels')
			->where('admin_id', '=', $role->id)
			->where('hotel_id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}