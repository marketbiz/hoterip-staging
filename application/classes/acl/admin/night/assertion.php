<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Night_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{
		return DB::select(
				array(DB::expr('COUNT(nights.id)'), 'count')
			)
			->from('nights')
      ->join('nights_rooms')->on('nights_rooms.night_id', '=', 'nights.id')
			->join('rooms')->on('rooms.id', '=', 'nights_rooms.room_id')
			->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'rooms.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('nights.id', '=', $resource->id)
			->execute()
			->get('count') > 0;
	}
	
}