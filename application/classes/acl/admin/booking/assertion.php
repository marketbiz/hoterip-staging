<?php defined('SYSPATH') or die('No direct script access.');

class Acl_Admin_Booking_Assertion implements Acl_Assert_Interface {
	
	public function assert(Acl $acl, $role = null, $resource = null, $privilege = null)
	{

		/// Manage booking. add exception for booking privilage
		$booking_privilage = DB::select(
				array(DB::expr('COUNT(bookings.id)'), 'count')
			)
			->from('bookings')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
			->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'rooms.hotel_id')
			->where('admins_hotels.admin_id', '=', $role->id)
			->where('bookings.id', '=', $resource->id)
			->execute()
			->get('count') > 0;

		if($booking_privilage == FALSE)
		{
			$booking_privilage = DB::select(
					array(DB::expr('COUNT(bookings.id)'), 'count')
				)
				->from('bookings')
      	->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
				->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'room_deleted.hotel_id')
				->where('admins_hotels.admin_id', '=', $role->id)
				->where('bookings.id', '=', $resource->id)
				->execute()
				->get('count') > 0;
		}

		return $booking_privilage;
	}
	
}