<?php defined('SYSPATH') or die('No direct script access.');

class View_Room_Add extends Kostache {
	
	public function room_texts()
	{
		$room_texts = array();
		
		foreach ($this->languages as $language)
		{
      $room_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}"),
			);  
		}
		
		return $room_texts;
	}
  
  public function hotel_photos()
  {
    $hotel_photos = array();
		
		foreach ($this->hotel_photos as $hotel_photo)
		{			
			$hotel_photos[] = $hotel_photo->as_array() + array(
        'url' => Kohana::config('application.hotel_images_url').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->basename,
        'selected' => Arr::get($this->values, 'hotel_photo_id') == $hotel_photo->id,
      );
		}
		
		return $hotel_photos;
  }
  
  public function room_capacities()
	{
		$room_capacities = array();
    
		$index = 0;
		for ($i = 1; $i <= 9; $i++)
		{
			for ($j = 0; $j <= 6; $j++)
			{
        $extrabeds = array();
    
        for ($number = 0; $number <= 10; $number++)
        {
          $extrabeds[] = array(
            'number' => $number,
            'selected' => Arr::path($this->values, "number_of_extrabeds.$index") == $number,
          );
        }
        
				$room_capacities[] = array(
					'number_of_adults' => $i,
					'number_of_children' => $j,
          'index' => $index,
          'checked' => (bool) Arr::path($this->values, "indexs.$index"),
          'extrabeds' => $extrabeds,
				);
				
				$index++;
			}
		}
		
		return $room_capacities;
	}
	
}