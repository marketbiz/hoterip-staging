<?php defined('SYSPATH') or die('No direct script access.');

class View_Room_Edit extends Kostache {
	
  protected $_room;
  protected $_room_facility;

  public function room()
  {
    if ( ! $this->_room)
    {
      $this->_room = Arr::overwrite($this->room->as_array(), $this->values);
    }
    
    return $this->_room;
  }
  
  public function room_facility()
  {
    if ( ! $this->_room_facility)
    {
      $this->_room_facility = Arr::overwrite($this->room_facility->as_array(), $this->values);
    }
    
    return $this->_room_facility;
  }
  
	public function room_texts()
	{
		$room_texts = array();
		
		foreach ($this->languages as $language)
		{			
			$room_text = Arr::get($this->room_texts, $language->id);
			
			if ( ! $room_text)
			{
				$room_text = ORM::factory('room_text');
				$room_text->name = '';
			}
			
			$room_texts[] = array(
				'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}", $room_text->name),
			);
		}
		
		return $room_texts;
	}
  
  public function hotel_photos()
  {
    $hotel_photos = array();
		
		foreach ($this->hotel_photos as $hotel_photo)
		{			
			$hotel_photos[] = $hotel_photo->as_array() + array(
        'url' => Kohana::config('application.hotel_images_url').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->basename,
        'selected' => Arr::get($this->values, 'hotel_photo_id', $this->room->hotel_photo_id) == $hotel_photo->id,
      );
		}
		
		return $hotel_photos;
  }
  
  public function room_capacities()
	{
		$room_capacities = array();
		
		$index = 0;
		
		for ($i = 1; $i <= 9; $i++)
		{
			for ($j = 0; $j <= 6; $j++)
			{
				$room_capacity = Arr::path($this->room_capacities, "$i.$j");
				
				if ( ! $room_capacity)
				{
					$room_capacity = ORM::factory('room_capacity');
          $room_capacity->number_of_extrabeds = 0;
				}
        
        $extrabeds = array();
    
        for ($number = 0; $number <= 10; $number++)
        {
          $extrabeds[] = array(
            'number' => $number,
            'selected' => Arr::path($this->values, "number_of_extrabeds.$index", $room_capacity->number_of_extrabeds) == $number,
          );
        }
					
				$room_capacities[] = array(
          'index' => $index,
					'number_of_adults' => $i,
					'number_of_children' => $j,
          'number_of_extrabeds' => $room_capacity->number_of_extrabeds,
          'checked' => (bool) $room_capacity->id,
          'extrabeds' => $extrabeds,
				);
				
				$index++;
			}
		}
		
		return $room_capacities;
	}
	
}