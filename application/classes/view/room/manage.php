<?php defined('SYSPATH') or die('No direct script access.');

class View_Room_Manage extends Kostache {

  public function rooms()
	{
		$rooms = array();
		
		foreach ($this->rooms as $room)
		{
      $rooms[] = Arr::merge($room->as_array(), array(
        'publish_price' => Num::display($room->publish_price),
        'adult_breakfast_price' => Num::display($room->adult_breakfast_price),
        'child_breakfast_price' => Num::display($room->child_breakfast_price),
        'extrabed_price' => Num::display($room->extrabed_price),
      ));
		}
		
		return $rooms;
	}
  
}