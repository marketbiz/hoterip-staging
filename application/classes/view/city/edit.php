<?php defined('SYSPATH') or die('No direct script access.');

class View_City_Edit extends Kostache {
	
  public function countries()
  {
    $countries = array();
    foreach ($this->countries as $country)
    {
      $countries[] = Arr::merge($country->as_array(), array(
        'selected' => Arr::get($this->values, 'country_id', $this->city->country_id) == $country->id,
      ));
    }
    
    return $countries;
  }
  
	public function city_texts()
	{
		$city_texts = array();
		
		foreach ($this->languages as $language)
		{
      if ( ! $city_text = Arr::get($this->city_texts, $language->id))
			{
				$city_text = ORM::factory('city_text');
				$city_text->name = '';
			}
      
			$city_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}", $city_text->name),
			);
		}
		
		return $city_texts;
	}
	
}