<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Send extends Kostache {
  
  public function tos()
  {
    $names = array(
      __('Subscribed Users'), 
      __('All Users'), 
      __('All Admins'),
      __('User Email'),
    );
    
    $tos = array();
    foreach ($names as $id => $name)
    {
      $tos[] = array(
        'id' => $id,
        'name' => $name,
        'selected' => Arr::get($this->values, 'to') == $id,
      );
    }
    
    return $tos;
  }
  
}