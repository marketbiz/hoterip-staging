<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Booking_Fail extends Kostache {

	public function booking_pending()
	{
		$booking_pending = clone $this->booking_pending;
		$booking_pending->created = date(__('M j, Y'), $booking_pending->created);
		$booking_pending->nationality = Model::factory('nationality')->get_nationality_name($booking_pending->nationality_id);

		return $booking_pending;
	}

  public function booking_data()
  {
    $booking_data = $this->booking_data;
    
    $booking_data['check_in'] = date(__('M j, Y'), strtotime($booking_data['check_in']));
    $booking_data['check_out'] = date(__('M j, Y'), strtotime($booking_data['check_out']));
    
    return $booking_data;
  }
 
}