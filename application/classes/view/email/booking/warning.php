<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Booking_Warning extends Kostache {

	public function created()
	{
		return date(__('M j, Y'), $this->created);
	}

  public function booking_data()
  {
    $booking_data = $this->booking_data;
    
    $booking_data['check_in'] = date(__('M j, Y'), strtotime($booking_data['check_in']));
    $booking_data['check_out'] = date(__('M j, Y'), strtotime($booking_data['check_out']));
    
    return $booking_data;
  }
  

}