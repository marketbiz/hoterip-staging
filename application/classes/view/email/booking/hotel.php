<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Booking_Hotel extends Kostache {

	public function booking_pending()
	{
		$booking_pending = clone $this->booking_pending;
		$booking_pending->created = date(__('M j, Y'), $this->booking_pending->created);
		$booking_pending->nationality = Model::factory('nationality')->get_nationality_name($this->booking_pending->nationality_id);

		return $booking_pending;
	}
  
  public function booking_data()
  {
    $booking_data = $this->booking_data;

    $booking_data['check_in'] = date(__('M j, Y'), strtotime($booking_data['check_in']));
    $booking_data['check_out'] = date(__('M j, Y'), strtotime($booking_data['check_out']));

    return $booking_data;
  }
  
  public function items()
  {
    foreach ($this->items as $item)
    {
      $item->display_date = date('M j, Y', strtotime($item->date));
      $item->stock_before = $item->stock + $this->booking_data['number_of_rooms'];
    }

    return $this->items;
  }
  public function render()
  {
    $this->set(array(
      'text_1' => I18n::get('Hello ', $this->language->code),
      'text_2' => I18n::get('Guest has booked your hotel room.', $this->language->code),
      'text_3' => I18n::get('You can check the booking details from link below.', $this->language->code),
      
      'text_4' => I18n::get('Remaining Stock for ', $this->language->code),
      'text_5' => I18n::get('Number of Sold Rooms:', $this->language->code),
      'text_6' => I18n::get(' Rooms', $this->language->code),

      'text_7' => I18n::get(' -> ', $this->language->code),
      'text_8' => I18n::get(' Sold', $this->language->code),
      'text_9' => I18n::get('Here is guest information.', $this->language->code),

      'text_booking_date' => I18n::get('Booking Date', $this->language->code),
      'text_booking_id' => I18n::get('Booking ID', $this->language->code),
      'text_hotel_name' => I18n::get('Hotel Name', $this->language->code),
      'text_hotel_address' => I18n::get('Hotel Address', $this->language->code),
      'text_hotel_telephone' => I18n::get('Telephone', $this->language->code),
      'text_hotel_first_name' => I18n::get('Fist Name', $this->language->code),
      'text_hotel_last_name' => I18n::get('Last Name', $this->language->code),
      'text_hotel_nationality' => I18n::get('Nationality', $this->language->code),
      'text_check_in_date' => I18n::get('Check-in Date', $this->language->code),
      'text_check_out_date' => I18n::get('Check-out Date', $this->language->code),
      'text_room_name' => I18n::get('Room Name', $this->language->code),

      'text_number_of_nights' => I18n::get('Number of Nights', $this->language->code),
      'text_breakfats_included' => I18n::get('Included', $this->language->code),
      'text_breakfats_not_included' => I18n::get('Not Included', $this->language->code),
      'text_cancellation_policy' => I18n::get('Cancellation Policy', $this->language->code),
      'text_number_of_rooms' => I18n::get('Number of Rooms', $this->language->code),
      'text_number_of_adults' => I18n::get('Number of Adults', $this->language->code),
      'text_number_of_child' => I18n::get('Number of Children', $this->language->code),
      'text_number_of_extrabed' => I18n::get('Number of Extra Beds', $this->language->code),
      'text_breakfast' => I18n::get('Breakfast', $this->language->code),

      'base_url_ssl' => 'http://',
      'url' => 'http://my.hoterip.com/booking/manage?id=',

      'hoterip_1' => 'Hoterip.com by H.I.S.',
      'hoterip_2' => 'Smart Hotel Search',
      'hoterip_3' => 'Jl. By Pass Ngurah Rai No.732 Pesanggaran, Denpasar',
      'hoterip_4' => 'Indonesia',
      'hoterip_5' => 'Tel: +62 361 721064',
      'hoterip_6' => 'support@hoterip.com',
    ));
    
    return parent::render();
  }
}