<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Booking_Hotel extends Kostache {

	public function booking_pending()
	{
		$booking_pending = clone $this->booking_pending;
		$booking_pending->created = date(__('M j, Y'), $this->booking_pending->created);
		$booking_pending->nationality = Model::factory('nationality')->get_nationality_name($this->booking_pending->nationality_id);

		return $booking_pending;
	}
  
  public function booking_data()
  {
    $booking_data = $this->booking_data;

    $booking_data['check_in'] = date(__('M j, Y'), strtotime($booking_data['check_in']));
    $booking_data['check_out'] = date(__('M j, Y'), strtotime($booking_data['check_out']));

    return $booking_data;
  }
  
  public function items()
  {
    foreach ($this->items as $item)
    {
      $item->display_date = date('M j, Y', strtotime($item->date));
      $item->stock_before = $item->stock + $this->booking_data['number_of_rooms'];
    }

    return $this->items;
  }

}