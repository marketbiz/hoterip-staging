<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Conversation extends Kostache {
	
  public function render()
  {
    $this->set(array(
      'text_1' => strtr(I18n::get('Hello :first_name :last_name,', $this->language->code), array(':first_name' => $this->booking->first_name, ':last_name' => $this->booking->last_name)),
      'text_2' => strtr(I18n::get(':hotel_name has sent you message regarding booking ID #:booking_id.', $this->language->code), array(':hotel_name' => $this->selected_hotel->name, ':booking_id' => $this->booking->id)),
      'text_3' => I18n::get('The following is the contents of the message.', $this->language->code),
      'text_4' => I18n::get('To reply this message, please click link below.', $this->language->code),
      'text_5' => I18n::get('Note: Please write using English or language where the hotel is located.', $this->language->code),
      'text_6' => I18n::get('This email is an automated notification email. Do not reply to this email.', $this->language->code),
    ));
    
    return parent::render();
  }
  
}