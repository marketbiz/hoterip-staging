<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Refund extends Kostache {
  
  public function booking()
  {
    return Arr::merge($this->booking->as_array(), array(
      'check_in_date' => date(I18n::get('M j, Y', $this->language->code), strtotime($this->booking->check_in_date)),
      'check_out_date' => date(I18n::get('M j, Y', $this->language->code), strtotime($this->booking->check_out_date)),
    ));
  }
  
  public function hotel()
  {
    $hotel = unserialize($this->booking->hotel);
    $hotel_texts = unserialize($this->booking->hotel_texts);
    $hotel_text = Arr::get($hotel_texts, $this->language->id);
    
    $hotel->name = $hotel_text->name;
    
    return $hotel;
  }
  
  public function room()
  {
    $room = unserialize($this->booking->room);
    $room_texts = unserialize($this->booking->room_texts);
    $room_text = Arr::get($room_texts, $this->language->id);
    
    $room->name = $room_text->name;
    
    return $room;
  }
  
  public function cancellation_rules()
  {
    $cancellation = unserialize($this->booking->cancellation);
    
    return implode('. ', array(
      __($cancellation->rule_1_name),
      __($cancellation->rule_2_name),
      __($cancellation->rule_3_name),
    ));
  }
  public function guest_currency()
  {
    $guest_currency = unserialize($this->booking->guest_currency);
    return $guest_currency->code;
  }
  
  public function transaction_currency()
  {
    $transaction_currency = unserialize($this->booking->transaction_currency);
    return $transaction_currency->code;
  }
  
  public function amount_guest_paid_guest_currency()
  {
    // Get hotel to guest exchange rate
    $hotel_to_guest_exchange = unserialize($this->booking->hotel_to_guest_exchange);
    return Num::format_decimal(round($this->prices['amount_paid_by_guest'] / $hotel_to_guest_exchange->amount * $hotel_to_guest_exchange->rate, 2));
  }
  
  public function amount_guest_paid_transaction_currency()
  {
    // Get hotel to transaction exchange rate
    $hotel_to_transaction_exchange = unserialize($this->booking->hotel_to_transaction_exchange);
    return Num::format_decimal(round($this->prices['amount_paid_by_guest'] / $hotel_to_transaction_exchange->amount * $hotel_to_transaction_exchange->rate, 2));
  }
  
  public function guest_currency_same_with_transaction_currency()
  {
    return $this->guest_currency() == $this->transaction_currency();
  }
  
  public function amount_money_refunded_transaction_currency()
  {
    // Get hotel to transaction exchange rate
    $hotel_to_transaction_exchange = unserialize($this->booking->hotel_to_transaction_exchange);
    return round($this->prices['amount_money_refunded_to_guest'] / $hotel_to_transaction_exchange->amount * $hotel_to_transaction_exchange->rate, 2);
  }
  
  public function amount_point_refunded()
  {
    return $this->prices['amount_point_refunded_to_guest'];
  }
  
  public function render()
  {
    $this->set(array(
      'text_1' => strtr(I18n::get('Hello :first_name :last_name,', $this->language->code), array(':first_name' => $this->booking->first_name, ':last_name' => $this->booking->last_name)),
      'text_2' => I18n::get('We have refunded money or/and points to your Hoterip account as below', $this->language->code),
      'text_3' => I18n::get('It may take up to 7 days to be done the process.', $this->language->code),
      'text_4' => I18n::get('Please contact Hoterip Support if you do not receive the above amount after 7 days.', $this->language->code),
      
      'text_booking_id' => I18n::get('Booking ID', $this->language->code),
      'text_credit_card' => I18n::get('Credit Card Number', $this->language->code),
      'text_hotel_name' => I18n::get('Hotel Name', $this->language->code),
      'text_check_in_date' => I18n::get('Check-in Date', $this->language->code),
      'text_check_out_date' => I18n::get('Check-out Date', $this->language->code),
      'text_cancellation_policy' => I18n::get('Cancellation Policy', $this->language->code),
      'text_paid' => I18n::get('Paid', $this->language->code),
      'text_used_point' => I18n::get('Used Point', $this->language->code),
      'text_refunded' => I18n::get('Refunded', $this->language->code),
      'text_refunded_point' => I18n::get('Refunded Point', $this->language->code),
      'text_point' => I18n::get('Point', $this->language->code),
    ));
    
    return parent::render();
  }
  
}