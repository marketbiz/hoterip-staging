<?php defined('SYSPATH') or die('No direct script access.');

class View_Email_Cancel extends Kostache {
  
  public function booking()
  {
    return Arr::merge($this->booking->as_array(), array(
      'check_in_date' => date(I18n::get('M j, Y', $this->language->code), strtotime($this->booking->check_in_date)),
      'check_out_date' => date(I18n::get('M j, Y', $this->language->code), strtotime($this->booking->check_out_date)),
    ));
  }
  
  public function hotel()
  {
    $hotel = unserialize($this->booking->hotel);
    $hotel_texts = unserialize($this->booking->hotel_texts);
    $hotel_text = Arr::get($hotel_texts, $this->language->id);
    
    $hotel->name = $hotel_text->name;
    
    return $hotel;
  }
  
  public function room()
  {
    $room = unserialize($this->booking->room);
    $room_texts = unserialize($this->booking->room_texts);
    $room_text = Arr::get($room_texts, $this->language->id);
    
    $room->name = $room_text->name;
    
    return $room;
  }
  
  public function cancellation_rules()
  {
    $cancellation = unserialize($this->booking->cancellation);
    
    return implode('. ', array(
      __($cancellation->rule_1_name),
      __($cancellation->rule_2_name),
      __($cancellation->rule_3_name),
    ));
  }
  
  public function render()
  {
    $this->set(array(
      'text_1' => strtr(I18n::get('Hello :first_name :last_name,', $this->language->code), array(':first_name' => $this->booking->first_name, ':last_name' => $this->booking->last_name)),
      'text_2' => I18n::get('Your booking has been canceled.', $this->language->code),
      'text_3' => I18n::get('The following are your canceled booking details.', $this->language->code),
      
      'text_booking_id' => I18n::get('Booking ID', $this->language->code),
      'text_hotel_name' => I18n::get('Hotel Name', $this->language->code),
      'text_room_name' => I18n::get('Room Name', $this->language->code),
      'text_check_in_date' => I18n::get('Check-in Date', $this->language->code),
      'text_check_out_date' => I18n::get('Check-out Date', $this->language->code),
      'text_cancellation_policy' => I18n::get('Cancellation Policy', $this->language->code),
    ));
    
    return parent::render();
  }
  
}