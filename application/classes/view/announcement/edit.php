<?php defined('SYSPATH') or die('No direct script access.');

class View_Announcement_Edit extends Kostache {
	
	public function Announcement_texts()
	{
		$announcement_texts = array();
		
		foreach ($this->languages as $language)
		{
      if ( ! $announcement_text = Arr::get($this->announcement_texts, $language->id))
			{
				$announcement_text = ORM::factory('announcement_text');
				$announcement_text->name = '';
			}
      
			$announcement_texts[] = array(
        'language' => $language,
				'description' => Arr::path($this->values, "descriptions.{$language->id}", $announcement_text->description),
			);
		}

		return $announcement_texts;
	}
	
}