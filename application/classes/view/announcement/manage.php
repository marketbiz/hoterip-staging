<?php defined('SYSPATH') or die('No direct script access.');

class View_Announcement_Manage extends Kostache {

  public function announcements_data()
	{
		$announcement_texts = array();
		
		foreach ($this->announcements as $announcement)
		{
			$announcement_texts[] = array(
        'title' => $announcement->title,
        'id' => $announcement->announcement_id,
        'is_show' => $announcement->announcement ? TRUE:FALSE
			);
		}

		return $announcement_texts;
	}
}