<?php defined('SYSPATH') or die('No direct script access.');

class View_Announcement_Add extends Kostache {
  
  public function announcement_texts()
	{
		$announcement_texts = array();
		
		foreach ($this->languages as $language)
		{
			$announcement_texts[] = array(
        'language' => $language,
				'description' => Arr::path($this->values, "descriptions.{$language->id}"),
			);
		}
		
		return $announcement_texts;
	}
	
}