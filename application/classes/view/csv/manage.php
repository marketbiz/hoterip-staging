<?php defined('SYSPATH') or die('No direct script access.');

class View_Csv_Manage extends Kostache {

	public function csv_data()
	{
		$csv_data = array();

		foreach ($this->datas as $key => $data) {
			$csv_data[] = array(
				'name' =>$data
				);
		}

		return $csv_data;
	}
}