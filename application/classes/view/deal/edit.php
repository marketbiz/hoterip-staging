<?php defined('SYSPATH') or die('No direct script access.');

class View_Deal_Edit extends Kostache {
  
  public function deal()
  {
    return Arr::merge($this->deal->as_array(), $this->values);
  }
  
  public function languages()
  {
    $languages = array();
    
    foreach ($this->languages as $language)
    {
      $languages[] = Arr::merge($language->as_array(), array(
        'selected' => Arr::get($this->values, 'language_id', $this->deal->language_id) == $language->id,
      ));
    }
    
    return $languages;
  }
  
}