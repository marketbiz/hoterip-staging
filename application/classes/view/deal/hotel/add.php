<?php defined('SYSPATH') or die('No direct script access.');

class View_Deal_Hotel_Add extends Kostache {
  
  public function hotels()
  {
    $hotels = array();
    
    foreach ($this->hotels as $hotel)
    {
      $hotels[] = Arr::merge($hotel->as_array(), array(
        'selected' => Arr::get($this->values, 'hotel_id') == $hotel->id,
      ));
    }
    
    return $hotels;
  }
  
  public function currencies()
  {
    $currencies = array();
    
    foreach ($this->currencies as $currency)
    {
      $currencies[] = Arr::merge($currency->as_array(), array(
        'selected' => Arr::get($this->values, 'currency_id') == $currency->id,
      ));
    }
    
    return $currencies;
  }
  
}