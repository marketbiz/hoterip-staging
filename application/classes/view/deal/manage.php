<?php defined('SYSPATH') or die('No direct script access.');

class View_Deal_Manage extends Kostache {
  
  private $hotels;
  
  public function deal_add_url()
  {
    return Route::url('default', array('controller' => 'deal', 'action' => 'add'));
  }
  
  public function deals()
  {
    $deals = array();
    
    foreach ($this->deals as $deal)
    {
      $deal_row = $deal->as_array();
      $deal_row['hotel_add_url'] = Route::url('default', array('controller' => 'deal_hotel', 'action' => 'add', 'id' => $deal->id));
      $deal_row['edit_url'] = Route::url('default', array('controller' => 'deal', 'action' => 'edit', 'id' => $deal->id));
      $deal_row['delete_url'] = Route::url('default', array('controller' => 'deal', 'action' => 'delete', 'id' => $deal->id));
      
      $this->hotels = $deal->hotels
        ->select(
          array('deals_hotels.price', 'price'),
          array('currencies.code', 'currency_code'),
          array('hotel_texts.name', 'name')
        )
        ->join('currencies')->on('currencies.id', '=', 'deals_hotels.currency_id')
        ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
        ->where('hotel_texts.language_id', '=', $this->selected_language->id)
        ->order_by('order')
        ->find_all();
      
      foreach ($this->hotels as $hotel)
      {
        $hotel_row = $hotel->as_array();
        $hotel_row['deal_id'] = $deal->id;
        $hotel_row['price'] = number_format($hotel->price);
        $hotel_row['image_url'] = Kohana::config('application.hotel_images_url').'/'.$hotel->url_segment.'/'.$hotel->basename;
        $hotel_row['delete_url'] = Route::url('default', array('controller' => 'deal_hotel', 'action' => 'delete', 'id' => $deal->id.'-'.$hotel->id));
        
        $deal_row['hotels'][] = $hotel_row;
      }
      
      $deals[] = $deal_row;
    }
    
    return $deals;
  }
	
}