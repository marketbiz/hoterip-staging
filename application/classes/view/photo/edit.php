<?php defined('SYSPATH') or die('No direct script access.');

class View_Photo_Edit extends Kostache {
	
	public function hotel_photo_texts()
	{
		$hotel_photo_texts = array();
		
		foreach ($this->languages as $language)
		{
			$hotel_photo_text = Arr::get($this->hotel_photo_texts, $language->id);
			
			if ( ! $hotel_photo_text)
			{
				$hotel_photo_text = ORM::factory('hotel_photo_text');
				$hotel_photo_text->name = '';
			}
			
			$hotel_photo_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}", $hotel_photo_text->name),
			);
		}
		
		return $hotel_photo_texts;
	}

	public function render()
	{
		$this->set(array(
			'photo_url' => Kohana::config('application.hotel_images_url').'/'.$this->selected_hotel->url_segment.'/'.$this->hotel_photo->basename,
		));
		
		return parent::render();
	}
	
}