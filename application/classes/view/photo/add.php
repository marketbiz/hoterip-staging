<?php defined('SYSPATH') or die('No direct script access.');

class View_Photo_Add extends Kostache {
	
	public function hotel_photo_texts()
	{
		$hotel_photo_texts = array();
		
		foreach ($this->languages as $language)
		{
			$hotel_photo_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}"),
			);
		}
		
		return $hotel_photo_texts;
	}
	
}