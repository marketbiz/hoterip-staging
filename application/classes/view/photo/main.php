<?php defined('SYSPATH') or die('No direct script access.');

class View_Photo_Main extends Kostache {

	public function hotel_photos()
  {
    $hotel_photos = array();
		
		foreach ($this->hotel_photos as $hotel_photo)
		{			
			$hotel_photos[] = $hotel_photo->as_array() + array(
        'url' => Kohana::config('application.hotel_images_url').'/'.$this->hotel->url_segment.'/'.$hotel_photo->basename,
        'selected' => Arr::get($this->values, 'hotel_photo_id', $this->hotel->hotel_photo_id) == $hotel_photo->id,
      );
		}
		
		return $hotel_photos;
  }
	
}