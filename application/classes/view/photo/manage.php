<?php defined('SYSPATH') or die('No direct script access.');

class View_Photo_Manage extends Kostache {
	
	public function hotel_photos()
	{
		$hotel_photos = array();
		
		foreach ($this->hotel_photos as $hotel_photo)
		{
			$hotel_photos[] = $hotel_photo->as_array() + array(
				'url' => Kohana::config('application.hotel_images_url').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->basename,
				'edit_url' => Route::url('default', array('controller' => 'photo', 'action' => 'edit', 'id' => $hotel_photo->id)),
				'delete_url' => Route::url('default', array('controller' => 'photo', 'action' => 'delete', 'id' => $hotel_photo->id)),
			);
		}
		
		return $hotel_photos;
	}

	public function render()
	{
		$this->set(array(
      'main_photo_exist' => $this->hotel->hotel_photo_id != NULL,
      'main_photo_url' => Kohana::config('application.hotel_images_url').'/'.$this->hotel->url_segment.'/'.$this->hotel->photo_basename,
      'main_photo_edit_url' => Route::url('default', array('controller' => 'photo', 'action' => 'main')),
			'photo_add_url' => Route::url('default', array('controller' => 'photo', 'action' => 'add')),
		));
		
		return parent::render();
	}
	
}