<?php defined('SYSPATH') or die('No direct script access.');

class View_User_Manage extends Kostache {
	
  public function fields()
  {
    $names = array(
      1 => __('Email'),
      2 => __('First Name'),
      3 => __('Last Name'),
    );
    
    $fields = array();
    
    foreach ($names as $key => $name)
    {
      $fields[] = array(
        'id' => $key,
        'name' => $name,
        'selected' => Arr::get($this->filters, 'field') == $key,
      );
    }
    
    return $fields;
  }
}