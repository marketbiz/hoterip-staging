<?php defined('SYSPATH') or die('No direct script access.');

class View_Restriction_Manage extends Kostache {
  
	public function restrictions()
	{
		$restrictions = array();
		
		foreach ($this->dates as $date)
		{
      $restrictions[] = array(
        'day' => __(date('D', strtotime($date))),
        'date' => $date,
        'display_date' => date(__('M j, Y'), strtotime($date)),
        'is_no_arrival' => (bool) Arr::get($this->no_arrivals, $date),
        'is_no_departure' => (bool) Arr::get($this->no_departures, $date),
      );
		}
    
		return $restrictions;
	}

}