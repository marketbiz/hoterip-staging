<?php

defined('SYSPATH') or die('No direct script access.');

class View_Layout_Admin extends Kostache {

    public function new_bookings() {
        $new_bookings = array();

        foreach ($this->new_bookings as $new_booking) {
            $new_bookings[] = Arr::merge($new_booking->as_array(), array(
                        'check_in_date' => date(__('M j, Y'), strtotime($new_booking->check_in_date)),
                        'check_out_date' => date(__('M j, Y'), strtotime($new_booking->check_out_date)),
            ));
        }

        return $new_bookings;
    }

    public function hotelier_menu() {
        return $this->hotel_edit OR
                $this->photo_manage OR
                $this->booking_manage OR
                $this->room_manage OR
                $this->night_manage OR
                $this->cancellation_manage OR
                $this->item_manage OR
                $this->promotion_manage OR
                $this->promotionpage_manage OR
                $this->surcharge_manage OR
                $this->announcement_manage OR
                $this->restriction_manage OR
                $this->report_finance OR
                $this->photo_less OR
                $this->room_less OR
                $this->policies_less OR
                $this->stock_less OR
                $this->campaigns_less OR
                $this->admin_id OR
                $this->home_dashboard OR
                $this->api_access OR
                $this->report_csv;
    }

    public function system_menu() {
        return $this->report_booking_all OR
                $this->report_finance_all OR
                $this->admin_manage OR
                $this->user_manage OR
                $this->subscribe_manage OR
                $this->hotel_manage OR
                $this->hotel_assign OR
                $this->banner_manage OR
                $this->itemb2b_manage OR
                $this->review_manage OR
                $this->district_manage OR
                $this->benefit_manage OR
                $this->city_manage OR
                $this->country_manage OR
                $this->page_manage OR
                $this->email_send OR
                $this->setting_edit OR
                $this->api_manage;
        ;
    }

    public function render() {
        $this->set(array(
            // URL
            'url_account_profile' => Route::url('account', array('action' => 'profile')),
            'url_account_change_password' => Route::url('account', array('action' => 'password')),
            'url_logout' => Route::url('auth', array('action' => 'logout')),
        ));

        return parent::render();
    }

}
