<?php defined('SYSPATH') or die('No direct script access.');

class View_Review_Manage extends Kostache {
	
	public function reviews()
	{
		$reviews = array();
		
		foreach ($this->reviews as $review)
		{
      $reviews[] = Arr::merge($review->as_array(), array(
        'submit_date' => date(__('M j, Y'), $review->created + Date::offset(A1::instance()->get_user()->timezone))
      ));
		}
		
		return $reviews;
	}
	
}