<?php defined('SYSPATH') or die('No direct script access.');

class View_Review_Edit extends Kostache {
	
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Edit Review'),
			'text_description' => __('Edit hotel review.'),

			'text_information' => __('Information'),
			'text_hotel_name' => __('Hotel Name'),
			'text_room_name' => __('Room Name'),
			'text_username' => __('Username'),
			'text_review_title' => __('Title'),
			'text_review_title_description' => __('Enter review title.'),
			'text_content' => __('Content'),
			'text_content_description' => __('Enter review content.'),
			'text_value_for_money_score' => __('Value for Money'),
			'text_value_for_money_score_description' => __('Select value for money rating.'),
			'text_select_area_score' => __('Select Area'),
			'text_select_area_score_description' => __('Select select area rating.'),
			'text_staff_performance_score' => __('Staff Performance'),
			'text_staff_performance_score_description' => __('Select staff performance rating.'),
			'text_hotel_condition_score' => __('Hotel Condition'),
			'text_hotel_condition_score_description' => __('Select hotel condition rating.'),
			'text_room_comfort_score' => __('Room comfort'),
			'text_room_comfort_score_description' => __('Select room comfort rating.'),
			'text_food_dining_score' => __('Food Dining'),
			'text_food_dining_score_description' => __('Select food dining rating.'),
			'text_language' => __('Language'),
			'text_language_description' => __('Select language for this review.'),
			'text_required' => __('Required'),
			
			// Form
			'form_open' => Form::open(Request::current()),
			'form_title' => Form::input('title', Arr::get($this->values, 'title', $this->review->title), array('size' => 50)),
			'form_content' => Form::textarea('content', Arr::get($this->values, 'content', $this->review->content), array('rows' => 6)),
			'form_value_for_money_score' => Form::select('value_for_money_score', Arr::range(1, 10), Arr::get($this->values, 'value_for_money_score', $this->review->value_for_money_score)),
			'form_select_area_score' => Form::select('select_area_score', Arr::range(1, 10), Arr::get($this->values, 'select_area_score', $this->review->select_area_score)),
			'form_staff_performance_score' => Form::select('staff_performance_score', Arr::range(1, 10), Arr::get($this->values, 'staff_performance_score', $this->review->staff_performance_score)),
			'form_hotel_condition_score' => Form::select('hotel_condition_score', Arr::range(1, 10), Arr::get($this->values, 'hotel_condition_score', $this->review->hotel_condition_score)),
			'form_room_comfort_score' => Form::select('room_comfort_score', Arr::range(1, 10), Arr::get($this->values, 'room_comfort_score', $this->review->room_comfort_score)),
			'form_food_dining_score' => Form::select('food_dining_score', Arr::range(1, 10), Arr::get($this->values, 'food_dining_score', $this->review->food_dining_score)),
			'form_language_id' => Form::select('language_id', $this->language_options, Arr::get($this->values, 'language_id', $this->review->language_id)),
			'form_submit' => Form::submit('submit', __('Submit')),
			'form_close' => Form::close(),
			
			'hotel_name' => $this->review->hotel_name,
			'room_name' => $this->review->room_name,
			'user_username' => $this->review->user_username,
		));
		
		return parent::render();
	}
	
}