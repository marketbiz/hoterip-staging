<?php defined('SYSPATH') or die('No direct script access.');

class View_Feature_Edit extends Kostache {
  
  public function feature()
  {
    return Arr::merge($this->feature->as_array(), $this->values);
  }
  
  public function hotels()
  {
    $hotels = array();
    
    foreach ($this->hotels as $hotel)
    {
      $hotels[] = Arr::merge($hotel->as_array(), array(
        'selected' => Arr::get($this->values, 'hotel_id', $this->feature->hotel_id) == $hotel->id,
      ));
    }
    
    return $hotels;
  }
  
  public function currencies()
  {
    $currencies = array();
    
    foreach ($this->currencies as $currency)
    {
      $currencies[] = Arr::merge($currency->as_array(), array(
        'selected' => Arr::get($this->values, 'currency_id', $this->feature->currency_id) == $currency->id,
      ));
    }
    
    return $currencies;
  }
  
  public function languages()
  {
    $languages = array();
    
    foreach ($this->languages as $language)
    {
      $languages[] = Arr::merge($language->as_array(), array(
        'selected' => Arr::get($this->values, 'language_id', $this->feature->language_id) == $language->id,
      ));
    }
    
    return $languages;
  }
  
}