<?php defined('SYSPATH') or die('No direct script access.');

class View_Feature_Manage extends Kostache {
  
  public function feature_add_url()
  {
    return Route::url('default', array('controller' => 'feature', 'action' => 'add'));
  }
  
  public function features()
  {
    $features = array();
    
    foreach ($this->features as $feature)
    {
      $features[] = Arr::merge($feature->as_array(), array(
        'edit_url' => Route::url('default', array('controller' => 'feature', 'action' => 'edit', 'id' => $feature->id)),
        'delete_url' => Route::url('default', array('controller' => 'feature', 'action' => 'delete', 'id' => $feature->id)),
        'price' => number_format($feature->price),
        'image_url' => Kohana::config('application.hotel_images_url').'/'.$feature->hotel_url_segment.'/'.$feature->hotel_basename,
      ));
    }
    
    return $features;
  }
	
}