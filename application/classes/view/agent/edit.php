<?php defined('SYSPATH') or die('No direct script access.');

class View_Agent_Edit extends Kostache {

  public function admin()
  {
    return Arr::overwrite($this->agent->as_array(), $this->values);
  }
  
  public function timezones()
  {
    $timezones = array();
    
    foreach ($this->timezones as $timezone)
    {
      $timezones[] = Arr::merge($timezone->as_array(), array(
        'selected' => Arr::get($this->values, 'timezone', $this->agent->timezone) == $timezone->identifier,
      ));
    }
    
    return $timezones;
  }
  
}