<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class View_Agent_Assign extends Kostache {

    public function assign_hotel() {
        
        $admin_hotels = $this->admin_hotels2;
        $values = $this->values;
        $currency = $this->admin;
        
        foreach ($admin_hotels as $name) {
            
            $order = $name->order;
            $stats = !is_null($values['recommend'][$name->id]) ? 1 : $name->recommend;
            
            $persen = !is_null($values['persen'][$name->id]) ? 1 : $name->persen;
            
            if(!is_null($values['order'][$name->id])){
                $order = ($values['order'][$name->id] < 11 ) ? $values['order'][$name->id] : $order;
            }
            
            $markup = ($name->markup == 0) ? 0 : $name->markup;
            
            $fields[] = array(
                'id' => $name->id,
                'name' => $name->name,
                'country_name' => $name->country_name,
                'city_name' => $name->city_name,
                'recommend' =>  !empty($stats) ? 1 : 0,
                'order' => !empty($order) ? $order : '',
                'persen' => !empty($persen) ? 1 : 0,
                'idr' => !empty($persen) ? 0 : 1,
                'currency' => $currency[0]['currency'],
                'markup' => $markup,
                'markup_text' => number_format($markup),
                'maxLength' => !empty($persen) ? 1 : 0
            );
        }
        
        return $fields;
    }

}
