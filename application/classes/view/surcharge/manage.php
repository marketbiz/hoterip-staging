<?php defined('SYSPATH') or die('No direct script access.');

class View_Surcharge_Manage extends Kostache {

  public function surcharges()
	{
		$surcharges = array();
		
		foreach ($this->dates as $date)
		{
			$surcharge = Arr::get($this->surcharges, $date);
      
			if ( ! $surcharge)
			{
				$surcharge = ORM::factory('surcharge');
        $surcharge->surcharge_description_id = 0;
				$surcharge->date = $date;
        $surcharge->adult_price = NULL;
        $surcharge->child_price = NULL;
			}
      
      $surcharge_descriptions = array();
      foreach ($this->surcharge_descriptions as $surcharge_description)
      {
        $surcharge_descriptions[] = Arr::merge($surcharge_description->as_array(), array(
          'selected' => $surcharge_description->id == $surcharge->surcharge_description_id,
          ));
      }
      
			$surcharges[] = Arr::merge($surcharge->as_array(), array(
        'day' => __(date('D', strtotime($surcharge->date))),
        'display_date' => date(__('M j, Y'), strtotime($surcharge->date)),
        'surcharge_descriptions' => $surcharge_descriptions,
			));
		}
    
		return $surcharges;
	}
	
}