<?php defined('SYSPATH') or die('No direct script access.');

class View_Country_Edit extends Kostache {
	
	public function country_texts()
	{
		$country_texts = array();
		
		foreach ($this->languages as $language)
		{
      if ( ! $country_text = Arr::get($this->country_texts, $language->id))
			{
				$country_text = ORM::factory('country_text');
				$country_text->name = '';
			}
      
			$country_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}", $country_text->name),
			);
		}
		
		return $country_texts;
	}
	
}