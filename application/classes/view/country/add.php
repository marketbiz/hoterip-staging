<?php defined('SYSPATH') or die('No direct script access.');

class View_Country_Add extends Kostache {
	
	public function country_texts()
	{
		$country_texts = array();
		
		foreach ($this->languages as $language)
		{
			$country_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}"),
			);
		}
		
		return $country_texts;
	}
	
}