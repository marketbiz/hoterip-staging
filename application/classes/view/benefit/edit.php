<?php defined('SYSPATH') or die('No direct script access.');

class View_Benefit_Edit extends Kostache {
	
	public function benefit_texts()
	{
		$benefit_texts = array();
		
		foreach ($this->languages as $language)
		{
      if ( ! $benefit_text = Arr::get($this->benefit_texts, $language->id))
			{
				$benefit_text = ORM::factory('benefit_text');
				$benefit_text->name = '';
			}
      
			$benefit_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}", $benefit_text->name),
			);
		}
		
		return $benefit_texts;
	}

	public function currencies_text()
	{
		$currencies_text = array();

		foreach ($this->currencies as $currency)
		{
			$currencies_text[] = array(
        'id' => $currency->id,
				'code' => $currency->code.'-'.$currency->name,
				'selected' => Arr::path($this->values, "currency", $this->benefit->currency) == $currency->id,
			);
		}

		return $currencies_text;
	}
}