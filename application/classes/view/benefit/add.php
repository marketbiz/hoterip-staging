<?php defined('SYSPATH') or die('No direct script access.');

class View_Benefit_Add extends Kostache {
	
	public function benefit_texts()
	{
		$benefit_texts = array();
		
		foreach ($this->languages as $language)
		{
			$benefit_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}"),
			);
		}
		
		return $benefit_texts;
	}

	public function currencies_text()
	{
		$currencies_text = array();

		foreach ($this->currencies as $currency)
		{
			$currencies_text[] = array(
        'id' => $currency->id,
				'code' => $currency->code.'-'.$currency->name
			);
		}

		return $currencies_text;
	}
}