<?php defined('SYSPATH') or die('No direct script access.');

class View_Search_Promotion extends Kostache {
  
  public function types(){
    $promotion_types = array(
      1 => __('Sale'),
      2 => __('Early Bird'),
      3 => __('Last Minute'),
      4 => __('Non refundable'),
      5 => __('Special Deal'),
      );

    $types = array();
    foreach ($promotion_types as $key => $promotion_type)
    {
      $types[] = array(
        'name' => ucfirst($promotion_type),
        'id' => $key,
        'selected' => Arr::get($this->filters, 'type') == $key
      );
    }
    return $types;
  }

  public function cities()
  {
    $cities = array();
    foreach ($this->cities as $key => $city)
    {
      $cities[] = array(
        'name' => ucfirst($city->url_segment),
        'id' => $city->id,
        'selected' => Arr::get($this->filters, 'city') == $city->id
      );
    }
    return $cities;
  }
  
  public function district_texts()
  {
    $district_texts = array();
    
    foreach ($this->districts as $key => $district)
    {
      $district_texts[] = array(
        'name' => ucfirst($district->url_segment),
        'id' => $district->id,
        'selected' => Arr::get($this->filters, 'district') == $district->id
      );
    }
    
    return $district_texts;
  }

  public function benefit_texts()
  {
    $benefit_texts = array();
    
    foreach ($this->other_benefits as $value => $other_benefit)
    {
      /// Benefit name
      $benefit_texts[] = array(
        'name' => substr(ucfirst($other_benefit['benefit_name']), 0, 30 ),
        'id' => $other_benefit['benefit_id'],
        'selected' => Arr::get($this->filters, 'other_benefit') == $other_benefit['benefit_id']
      );
    }
    
    return $benefit_texts;
  }
}