<?php defined('SYSPATH') or die('No direct script access.');

class View_Auth_Login extends Kostache {

  public function render()
  {
    $this->set(array(
      'forgot_url' => Route::url('auth', array('action' => 'forgot')),
    ));
    
    return parent::render();
  }
}