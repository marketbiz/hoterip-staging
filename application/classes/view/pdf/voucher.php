<?php defined('SYSPATH') or die('No direct script access.');

class View_Pdf_Voucher extends Kostache {

	public function booking_pending()
	{
		$booking_pending = clone $this->booking_pending;
		$booking_pending->created = date(__('M j, Y'), $booking_pending->created);
		$booking_pending->nationality = DB::select('nationalities.name')->from('nationalities')->where('id', '=', $booking_pending->nationality_id)
									                  ->execute()->get('name');;
		return $booking_pending;
	}

  public function booking_data()
  {
    $booking_data = $this->booking_data;

		$booking_data['check_in'] = Date::formatted_time($booking_data['check_in'], __('M j, Y'));
		$booking_data['check_out'] = Date::formatted_time($booking_data['check_out'], __('M j, Y'));
		
		$booking_data['invoice_transaction']['amount'] = number_format($booking_data['invoice_transaction']['amount']);

		$booking_data['additional_benefit_exist'] = !empty($booking_data['additional_benefit']) ? TRUE : FALSE;

    return $booking_data;
  }

	public function additional_benefit()
	{
		$booking_data = $this->booking_data;

		$benefits = array();

		if($booking_data['additional_benefit'] != NULL)
		{
				foreach ($booking_data['additional_benefit'] as $key => $campaign_benefits) {
				$benefits[] = array(
					'value' => $campaign_benefits['value'],
					'name' => ucfirst($campaign_benefits['name']),
					'benefit_for' => $campaign_benefits['value'] ? TRUE : FALSE
					);
			}
		}
		return $benefits;
	}

	public function base_url_load()
	{
		$protocol = (Kohana::$environment == Kohana::DEVELOPMENT) ? 'http' : 'https';

		$_SERVER['HTTPS'] = empty($_SERVER['HTTPS']) ? 0 : $_SERVER['HTTPS'];

		return URL::base($_SERVER['HTTPS'] ? $protocol : 'http');
	}
/*  
  public function credit_card()
  {
	return $this->payment_method_id == 1;
  }
*/
}