<?php defined('SYSPATH') or die('No direct script access.');

class View_City_Add extends Kostache {
  
  public function countries()
  {
    $countries = array();
    foreach ($this->countries as $country)
    {
      $countries[] = Arr::merge($country->as_array(), array(
        'selected' => Arr::get($this->values, 'country_id')
      ));
    }
    
    return $countries;
  }
  
  public function city_texts()
	{
		$city_texts = array();
		
		foreach ($this->languages as $language)
		{
			$city_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}"),
			);
		}
		
		return $city_texts;
	}
	
}