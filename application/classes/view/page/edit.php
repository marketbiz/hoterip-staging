<?php defined('SYSPATH') or die('No direct script access.');

class View_Page_Edit extends Kostache {

	public function description_texts()
	{
		$description_texts = array();
		$name = NULL;

		$descriptions = $this->descriptions;

		foreach ($this->languages as $language)
		{
			if(!empty($descriptions))
			{
				foreach ($descriptions as $description) {
					if($description->language_id == $language)
					{
						$name = $description->description_name;
					}
				}
			}

			$description_texts[] = array(
        'language' => $language,
				'name' => $name ? $name : ''
			);
		}

		return $description_texts;
	}

	public function location_hotels()
	{
		$hotels = $this->hotels;
		$descriptions = $this->descriptions;
		
		$special_promotion_hotel= array();
		$area_district_hotel= array();
		$location_hotels =array();

		if(count($descriptions) > 0)
		{
			foreach ($hotels as $key => $hotel)
			{
				foreach ($descriptions as $description) {
					if(!empty($description->special_promotions) || $description->special_promotions != '')
					{
				    $special_promotions = unserialize($description->special_promotions);

				    if($special_promotions)
				    {
					    foreach ($special_promotions as $special_promotion) {

					    	if($hotel->id == $special_promotion['id'])
					    	{
									$special_promotion_hotel[$key] = array(
										'id' => $hotel->id,
										'name' => $hotel->name,
										);
					    		break;
					    	}
					    }
				  	}

				    $area_hotels = unserialize($description->area_hotels);

				    if($area_hotels)
				    {
					    foreach ($area_hotels as $area_hotel) {
						    if($hotel->id == $area_hotel['id'])
						    {
									$area_district_hotel[$key] = array(
										'id' => $hotel->id,
										'name' => $hotel->name,
										);
						    	break;
						    }
				    	}
				    }
			  	}
				}
			}

			$special_promotion_hotel = array_values($special_promotion_hotel);
			$area_district_hotel = array_values($area_district_hotel);

			$location_hotels['special_promotions'] = $special_promotion_hotel;
			$location_hotels['area_district_hotel'] = $area_district_hotel;
		}

		return $location_hotels;
	}
}