<?php defined('SYSPATH') or die('No direct script access.');

class View_Page_Manage extends Kostache {

  public function fields()
  {
    $names = array(
      1 => __('City'),
      2 => __('Country'),
    );
    
    $fields = array();
    
    foreach ($names as $key => $name)
    {
      $fields[] = array(
        'id' => $key,
        'name' => $name,
        'selected' => Arr::get($this->filters, 'field') == $key,
      );
    }
    
    return $fields;
  }
}