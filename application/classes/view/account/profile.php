<?php defined('SYSPATH') or die('No direct script access.');

class View_Account_Profile extends Kostache {
  
  public function timezones()
  {
    $timezones = array();
    
    foreach ($this->timezones as $timezone)
    {
      $timezones[] = Arr::merge($timezone->as_array(), array(
        'selected' => $this->logged_in_admin->timezone == $timezone->identifier,
      ));
    }
    
    return $timezones;
  }
  
  public function admin()
  {
    return $this->logged_in_admin->as_array();
  }
  
	public function render()
	{
		$this->set(array(
      'time' => time(),
		));
		
		return parent::render();
	}
	
}