<?php defined('SYSPATH') or die('No direct script access.');

class View_Account_Password extends Kostache {

	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Change Password'),
			'text_description' => __('Change your password here.'),
			'text_current_password' => __('Current Password'),
			'text_current_password_description' => __('Enter your current password'),
			'text_password' => __('New Password'),
			'text_password_description' => __('Enter your new password'),
			'text_password_confirm' => __('New Password Confirmation'),
			'text_password_confirm_description' => __('Enter your new password once again'),
			
			// Form
			'form_open' => Form::open(Request::current()->uri()),
			'form_current_password' => Form::password('current_password'),
			'form_password' => Form::password('password'),
			'form_password_confirm' => Form::password('password_confirm'),
			'form_submit' => Form::submit('submit', __('Submit')),
			'form_close' => Form::close(),
		));
		
		return parent::render();
	}
	
}