<?php defined('SYSPATH') or die('No direct script access.');

class View_Cancellation_Add extends Kostache {
	
	public function rooms()
  {
    $rooms = array();
    
    foreach ($this->rooms as $room)
    {
      $rooms[] = Arr::merge($room->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "room_ids.$room->id"),
      ));
    }
    
    return $rooms;
  }
  
  public function cancellation_level_1_rules()
  {
    $cancellation_level_1_rules = array();
    
    foreach ($this->cancellation_level_1_rules as $cancellation_level_1_rule)
    {
      $cancellation_level_1_rules[] = Arr::merge($cancellation_level_1_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_1_cancellation_rule_id', 4) == $cancellation_level_1_rule->id,
      ));
    }
    
    return $cancellation_level_1_rules;
  }
  
  public function cancellation_level_2_rules()
  {
    $cancellation_level_2_rules = array();
    
    foreach ($this->cancellation_level_2_rules as $cancellation_level_2_rule)
    {
      $cancellation_level_2_rules[] = Arr::merge($cancellation_level_2_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_2_cancellation_rule_id') == $cancellation_level_2_rule->id,
      ));
    }
    
    return $cancellation_level_2_rules;
  }
  
  public function cancellation_level_3_rules()
  {
    $cancellation_level_3_rules = array();
    
    foreach ($this->cancellation_level_3_rules as $cancellation_level_3_rule)
    {
      $cancellation_level_3_rules[] = Arr::merge($cancellation_level_3_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_3_cancellation_rule_id') == $cancellation_level_3_rule->id,
      ));
    }
    
    return $cancellation_level_3_rules;
  }
  
  public function values()
  {
    return Arr::merge($this->values, array(
      'start_date' => Arr::get($this->values, 'start_date', date('Y-m-d')),
      'end_date' => Arr::get($this->values, 'end_date', date('Y-m-d', strtotime('+1 months'))),
    ));
  }
	
}