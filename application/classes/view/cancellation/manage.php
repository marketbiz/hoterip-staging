<?php defined('SYSPATH') or die('No direct script access.');

class View_Cancellation_Manage extends Kostache {

  public function cancellations()
  {
    $cancellations = array();
    
    foreach ($this->cancellations as $cancellation)
    {
      $room_names = ORM::factory('room')
        ->select(
           array('room_texts.name', 'name')
        )
        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
        ->join('campaigns_rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
        ->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
        ->join('campaigns_cancellations')->on('campaigns_cancellations.campaign_id', '=', 'campaigns.id')
        ->join('cancellations')->on('cancellations.id', '=', 'campaigns_cancellations.cancellation_id')
        ->where('cancellations.id', '=', $cancellation->id)
        ->where('room_texts.language_id', '=', $this->selected_language->id)
        ->find_all()
        ->as_array('id', 'name');
      
      $cancellations[] = Arr::merge($cancellation->as_array(), array(
        'is_effective' => strtotime('now') + Date::offset($this->selected_hotel->timezone) <= strtotime($cancellation->end_date),
        'start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
        'end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
        'room_names' => $this->selected_hotel->rooms->count_all() == count($room_names) ? __('All Rooms') : implode(', ', $room_names),
      ));
    }
    
    return $cancellations;
  }
}