<?php defined('SYSPATH') or die('No direct script access.');

class View_Cancellation_Edit extends Kostache {
	
  public $_cancellation;
  
  public function cancellation()
  {
    if ( ! $this->_cancellation)
    {
      $this->_cancellation = Arr::overwrite($this->cancellation->as_array(), $this->values);
    }
    
    return $this->_cancellation;
  }
  
	public function rooms()
  {
    $rooms = array();
    
    $selected_rooms = ORM::factory('room')
      ->join('campaigns_rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
      ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('campaigns_cancellations')->on('campaigns_cancellations.campaign_id', '=', 'campaigns.id')
      ->join('cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->where('cancellations.id', '=', $this->cancellation->id)
      ->where('campaigns.is_default', '=', 1)
      ->find_all()
      ->as_array('id');
    
    foreach ($this->rooms as $room)
    {
      $rooms[] = Arr::merge($room->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "room_ids.$room->id", (bool) Arr::get($selected_rooms, $room->id)),
      ));
    }
    
    return $rooms;
  }
  
  public function cancellation_level_1_rules()
  {
    $cancellation_level_1_rules = array();
    
    foreach ($this->cancellation_level_1_rules as $cancellation_level_1_rule)
    {
      $cancellation_level_1_rules[] = Arr::merge($cancellation_level_1_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_1_cancellation_rule_id', $this->cancellation->level_1_cancellation_rule_id) == $cancellation_level_1_rule->id,
      ));
    }
    
    return $cancellation_level_1_rules;
  }
  
  public function cancellation_level_2_rules()
  {
    $cancellation_level_2_rules = array();
    
    foreach ($this->cancellation_level_2_rules as $cancellation_level_2_rule)
    {
      $cancellation_level_2_rules[] = Arr::merge($cancellation_level_2_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_2_cancellation_rule_id', $this->cancellation->level_2_cancellation_rule_id) == $cancellation_level_2_rule->id,
      ));
    }
    
    return $cancellation_level_2_rules;
  }
  
  public function cancellation_level_3_rules()
  {
    $cancellation_level_3_rules = array();
    
    foreach ($this->cancellation_level_3_rules as $cancellation_level_3_rule)
    {
      $cancellation_level_3_rules[] = Arr::merge($cancellation_level_3_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_3_cancellation_rule_id', $this->cancellation->level_3_cancellation_rule_id) == $cancellation_level_3_rule->id,
      ));
    }
    
    return $cancellation_level_3_rules;
  }
  
}