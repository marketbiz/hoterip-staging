<?php defined('SYSPATH') or die('No direct script access.');

class View_Night_Add extends Kostache {
	
  public function rooms()
  {
    $rooms = array();
    
    foreach ($this->rooms as $room)
    {
      $rooms[] = Arr::merge($room->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "room_ids.$room->id"),
      ));
    }
    
    return $rooms;
  }
  
  public function values()
  {
    return Arr::merge($this->values, array(
      'start_date' => Arr::get($this->values, 'start_date', date('Y-m-d')),
      'end_date' => Arr::get($this->values, 'end_date', date('Y-m-d', strtotime('+7 days'))),
      'minimum_number_of_nights' => Arr::get($this->values, 'minimum_number_of_nights', 3),
    ));
  }
  
}