<?php defined('SYSPATH') or die('No direct script access.');

class View_Night_Manage extends Kostache {

  public function nights()
  {
    $nights = array();
    
    foreach ($this->nights as $night)
    {
      $room_names = ORM::factory('room')
        ->select(
           array('room_texts.name', 'name')
        )
        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
        ->join('nights_rooms')->on('nights_rooms.room_id', '=', 'rooms.id')
        ->join('nights')->on('nights.id', '=', 'nights_rooms.night_id')
        ->where('nights.id', '=', $night->id)
        ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
        ->where('room_texts.language_id', '=', $this->selected_language->id)
        ->find_all()
        ->as_array('id', 'name');
      
      $nights[] = Arr::merge($night->as_array(), array(
        'start_date' => date(__('M j, Y'), strtotime($night->start_date)),
        'end_date' => date(__('M j, Y'), strtotime($night->end_date)),
        'room_names' => $this->selected_hotel->rooms->count_all() == count($room_names) ? __('All Rooms') : implode(', ', $room_names),
      ));
    }
    
    return $nights;
  }
  
}