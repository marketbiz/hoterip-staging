<?php defined('SYSPATH') or die('No direct script access.');

class View_Night_Edit extends Kostache {
	
  protected $_night;
  
  public function night()
  {
    if ( ! $this->_night)
    {
      $this->_night = Arr::overwrite($this->night->as_array(), $this->values);
    }
    
    return $this->_night;
  }
  
  public function rooms()
  {
    $rooms = array();
    
    $selected_rooms = $this->night
      ->rooms
      ->find_all()
      ->as_array('id');
    
    foreach ($this->rooms as $room)
    {
      $rooms[] = Arr::merge($room->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "room_ids.$room->id", (bool) Arr::get($selected_rooms, $room->id)),
      ));
    }
    
    return $rooms;
  }
  
}