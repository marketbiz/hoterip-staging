<?php defined('SYSPATH') or die('No direct script access.');

class View_State_Edit extends Kostache {
	
	public function state_texts()
	{
		$state_texts = array();
		
		foreach ($this->languages as $language)
		{			
			$state_text = Arr::get($this->state_texts, $language->id);
			
			if ( ! $state_text)
			{
				$state_text = ORM::factory('state_text');
				$state_text->name = '';
			}
			
			$state_texts[] = array(
				'form_state_text_name' => Form::input("names[{$language->id}]", Arr::path($this->values, "names.{$language->id}", $state_text->name), array('size' => 30)),
				'state_text_language_name' => $language->name,
				'state_text_language_code' => $language->code,
			);
		}
		
		return $state_texts;
	}
	
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Edit State'),
			'text_description' => __('Edit current state.'),
			
			'text_country' => __('Country'),
			'text_country_name' => __('Country Name'),
			'text_country_name_description' => __('Select country for this state.'),
			
			'text_information' => __('Information'),
			'text_state_name' => __('State Name'),
			'text_state_name_description' => __('Enter state name. Enter state name in capital letters on each first letter in the word. For example: East Java.'),
			'text_required' => __('Required'),
			
			// Form
			'form_open' => Form::open(Request::current()->uri()),
			'form_country_id' => Form::select('country_id', $this->country_options, Arr::get($this->values, 'country_id')),
			'form_submit' => Form::submit('submit', __('Submit')),
			'form_close' => Form::close(),
			
			// Base url
			'url_base' => URL::base(),
		));
		
		return parent::render();
	}
	
}