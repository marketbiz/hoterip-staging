<?php defined('SYSPATH') or die('No direct script access.');

class View_State_Manage extends Kostache {
	
	public function states()
	{
		$states = array();
		
		foreach ($this->states as $state)
		{
			$states[] = array(
				'form_state_id' => Form::checkbox("ids[{$state->id}]", $state->id),
				'state_name' => $state->name,
				'state_country_name' => $state->country_name,
				'state_total_hotels' => $state->total_hotels,
				'state_edit_link' => Route::url('default', array('controller' => 'state', 'action' => 'edit', 'id' => $state->id)),
				'state_delete_link' => Route::url('default', array('controller' => 'state', 'action' => 'delete', 'id' => $state->id)),
			);
		}
		
		return $states;
	}
	
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Manage States'),
			'text_description' => __('Add, edit or delete state.'),
			'text_add' => __('Add new state'),
			'text_edit' => __('Edit'),
			'text_delete' => __('Delete'),
			'text_state_name' => __('State Name'),
			'text_country_name' => __('Country Name'),
			'text_total_hotels' => __('Total Hotels'),
			'text_action' => __('Action'),
			
			// Link
			'state_add_link' => Route::url('default', array('controller' => 'state', 'action' => 'add')),
			
			// Form
			'form_open' => Form::open(Request::current()->uri()),
			'form_check_all' => Form::checkbox('check_all', 1),
			'form_delete_checked' => Form::submit('delete_checked', __('Delete Checked')),
			'form_close' => Form::close(),
			
			// Base url
			'url_base' => URL::base(),
		));
		
		return parent::render();
	}
	
}