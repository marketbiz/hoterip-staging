<?php defined('SYSPATH') or die('No direct script access.');

class View_Log_Hotel extends Kostache {

  public function logs()
  {
    $logs = array();
    
    foreach ($this->logs as $log)
    {
      if ($log->room_texts)
      {
        // Get room texts
        $room_texts = unserialize($log->room_texts);
        // Get selected language room text
        $room_text = Arr::get($room_texts, $this->selected_language->id, 1);
      }
      else
      {
        $room_text = array(
          'name' => '',
        );
      }
      
      if ($log->data)
      {
        $data = unserialize($log->data);
        $text = strtr($log->text, $data);
      }
      else
      {
        $text = $log->text;
      }
      
      $logs[] = Arr::merge($log->as_array(), array(
        'room_name' => $room_text['name'],
        'text' => $text,
        'time' => date(__('M j, Y H:i'), $log->created + Date::offset($this->selected_hotel->timezone))
      ));
    }
    
    return $logs;
  }
  
}