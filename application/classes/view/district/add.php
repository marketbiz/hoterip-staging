<?php defined('SYSPATH') or die('No direct script access.');

class View_District_Add extends Kostache {
  
  public function cities()
  {
    $cities = array();
    foreach ($this->cities as $city)
    {
      $cities[] = Arr::merge($city->as_array(), array(
        'selected' => Arr::get($this->values, 'cities_id')
      ));
    }
    
    return $cities;
  }
  
  public function district_texts()
	{
		$district_texts = array();
		
		foreach ($this->languages as $language)
		{
			$district_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}"),
			);
		}
		
		return $district_texts;
	}
	
}