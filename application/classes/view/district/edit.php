<?php defined('SYSPATH') or die('No direct script access.');

class View_District_Edit extends Kostache {
	
  public function cities()
  {
    $cities = array();
    foreach ($this->cities as $city)
    {
      $cities[] = Arr::merge($city->as_array(), array(
        'selected' => Arr::get($this->values, 'city_id', $this->district->city_id) == $city->id,
      ));
    }
    
    return $cities;
  }
  
	public function district_texts()
	{
		$district_texts = array();
		
		foreach ($this->languages as $language)
		{
      if ( ! $district_text = Arr::get($this->district_texts, $language->id))
			{
				$district_text = ORM::factory('district_text');
				$district_text->name = '';
			}
      
			$district_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}", $district_text->name),
			);
		}
		
		return $district_texts;
	}
	
}