<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Add3 extends Kostache {
	
  public function items()
  {
    $items = array();
    
    $item_adult_prices = Arr::get($this->values, 'item_adult_prices', array());
    $item_child_prices = Arr::get($this->values, 'item_child_prices', array());
    $item_infant_prices = Arr::get($this->values, 'item_infant_prices', array());
    
    foreach ($this->dates as $key => $date)
    {
      $items[] = array(
        'item_date' => date('M j, Y', strtotime($date)),
        'item_form_item_dates_value' => $date,
        'item_form_item_adult_prices_value' => Arr::get($item_adult_prices, $date),
        'item_form_item_child_prices_value' => Arr::get($item_child_prices, $date),
        'item_form_item_infant_prices_value' => Arr::get($item_infant_prices, $date),
      );
    }
    
    return $items;
  }
  
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Add package'),
			'text_description' => __('Add new package.'),

      'text_information' => __('Information'),
      'text_schedules' => __('Schedules'),
      'text_prices' => __('Prices'),
      'text_photos' => __('Photos'),
      'text_confirmation' => __('Confirmation'),
      
      'text_fill_all' => __('Fill All'),
			'text_date' => __('Date'),
      'text_adult_price' => __('Adult Price'),
      'text_child_price' => __('Child Price'),
      'text_infant_price' => __('Infant Price'),
      'text_next_step' => __('Next Step'),
			'text_required' => __('Required'),
      
      // URL
      'url_base' => URL::base(),
		));
    
		return parent::render();
	}
	
}