<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Add2 extends Kostache {
	
  public function schedules()
  {
    $schedules = array();
    
    $schedule_descriptions = Arr::get($this->values, 'schedule_descriptions', array());
    $schedule_places = Arr::get($this->values, 'schedule_places', array());
    $schedule_is_breakfasts = Arr::get($this->values, 'schedule_is_breakfasts', array());
    $schedule_is_lunchs  = Arr::get($this->values, 'schedule_is_lunchs', array());
    $schedule_is_dinners = Arr::get($this->values, 'schedule_is_dinners', array());
      
    for ($i = 0; $i < $this->duration_days; $i++)
    {
      $schedules[] = array(
        'schedule_day' => $i + 1,
        'schedule_form_schedule_descriptions_value' => Arr::get($schedule_descriptions, $i + 1),
        'schedule_form_schedule_places_value' => Arr::get($schedule_places, $i + 1),
        'schedule_form_schedule_is_breakfasts_checked' => (bool) Arr::get($schedule_is_breakfasts, $i + 1),
        'schedule_form_schedule_is_lunchs_checked' => (bool) Arr::get($schedule_is_lunchs, $i + 1),
        'schedule_form_schedule_is_dinners_checked' => (bool) Arr::get($schedule_is_dinners, $i + 1),
      );
    }
    
    return $schedules;
  }
  
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Add package'),
			'text_description' => __('Add new package for your hotel. For example: Staying 3 days will receive 20% off. Note: You must also create normal price package.'),

      'text_information' => __('Information'),
      'text_schedules' => __('Schedules'),
      'text_prices' => __('Prices'),
      'text_photos' => __('Photos'),
      'text_confirmation' => __('Confirmation'),
			'text_day' => __('Day'),
      'text_stay_place' => __('Stay City'),
      'text_schedule_description' => __('Description'),
      'text_meal' => __('Meal'),
      'text_breakfast' => __('Breakfast'),
      'text_lunch' => __('Lunch'),
      'text_dinner' => __('Dinner'),
      'text_required' => __('Required'),
      'text_next_step' => __('Next Step'),
      
      // URL
      'url_base' => URL::base(),
		));
    
		return parent::render();
	}
	
}