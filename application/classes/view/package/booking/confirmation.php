<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Booking_Confirmation extends Kostache {
	
  public function total_prices()
  {
    return number_format($this->package_booking->adult_count * $this->package_booking->adult_price + 
      $this->package_booking->child_count * $this->package_booking->child_price + 
      $this->package_booking->infant_count * $this->package_booking->infant_price + 
      $this->package_booking->additional_price);
  }
  
	public function render()
	{
		$this->set(array(
			// Text
      'text_package_name' => I18n::get('Package Name', $this->language->code),
      'text_adults' => I18n::get('Adults', $this->language->code),
      'text_children' => I18n::get('Children', $this->language->code),
      'text_infants' => I18n::get('Infants', $this->language->code),
      'text_total_prices' => I18n::get('Total Prices', $this->language->code),
      
			'text_1' => I18n::get('Thank you for booking using Hoterip.', $this->language->code),
			'text_2' => I18n::get('We have confirmed your booking and is awaiting approval from you.', $this->language->code),
			'text_3' => I18n::get('The following are the details of your booking.', $this->language->code),
			'text_4' => I18n::get('To continue with this order, please click the link below and fill out the provided form.', $this->language->code),
      
      // URL
      'url_pakage_booking_confirmation' => 'http://hoterip.com/package/confirmation?package_booking_id='.$this->package_booking->id,
      
      // Currency
      'currency_symbol' => $this->currency->symbol,
      
      'package_name' => $this->package->name,
      'adult_count' => $this->package_booking->adult_count,
      'child_count' => $this->package_booking->child_count,
      'infant_count' => $this->package_booking->infant_count,
      
//      'adults' => strtr(I18n::get(':number '.Inflector::plural('person', $this->package_booking->adult_count), $this->language->code), array(':number' => $this->package_booking->adult_count)),
//      'children' => strtr(I18n::get(':number '.Inflector::plural('person', $this->package_booking->adult_count), $this->language->code), array(':number' => $this->package_booking->child_count)),
//      'infants' => strtr(I18n::get(':number '.Inflector::plural('person', $this->package_booking->adult_count), $this->language->code), array(':number' => $this->package_booking->other_count)),     
		));
		
		return parent::render();
	}
	
}