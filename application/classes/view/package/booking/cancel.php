<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Booking_Cancel extends Kostache {
	
	public function render()
	{
		$this->set(array(
			// Text
			'text_package_booking_information' => I18n::get('Package Booking Information', $this->language->code),
			'text_package_booking_id' => I18n::get('Package Booking ID', $this->language->code),
			'text_package_name' => I18n::get('Package Name', $this->language->code),
      'text_duration' => I18n::get('Duration', $this->language->code),
      'text_number_of_people' => I18n::get('Number of people', $this->language->code),
      'text_days' => __('Days'),
      'text_nights' => __('Nights'),
      'text_adults' => __('Adults'),
      'text_children' => __('Children'),
      'text_infants' => __('Infants'),
			'text_my_bookings_explanation' => I18n::get('You can see the details of your booking from the link below.', $this->language->code),
			'text_1' => I18n::get('Your booking has been canceled. The following are the details of the canceled booking.', $this->language->code),
			'text_2' => I18n::get('If you have paid for this booking our staff will contact you to refund you according to cancellation policy.', $this->language->code),
      
      // URL
      'url_account_bookings' => 'http://hoterip.com/account/bookings/',
      
			// Booking
			'package_booking_id' => $this->package_booking->id,
			'package_name' => $this->package->name,
      'package_duration_days' => $this->package->duration_days,
      'package_duration_nights' => $this->package->duration_nights,
      'package_booking_adult_count' => $this->package_booking->adult_count,
      'package_booking_child_count' => $this->package_booking->child_count,
      'package_booking_infant_count' => $this->package_booking->infant_count,
		));
		
		return parent::render();
	}
	
}