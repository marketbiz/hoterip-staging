<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Booking_Detail extends Kostache {
  
  public function package_booking_point()
  {
    $package_price = $this->package_booking->adult_count * $this->package_booking->adult_price +
        $this->package_booking->child_count * $this->package_booking->child_price +
        $this->package_booking->infant_count * $this->package_booking->infant_price +
        $this->package_booking->additional_price;
    
    return floor(($package_price / $this->package->currency_exchange_rate) * ($this->package_booking->point_percentage / 100));
  }
  
  public function package_booking_contacts()
  {
    $package_booking_contacts = array();
    
    foreach ($this->package_booking_contacts as $package_booking_contact)
    {
      $package_booking_contacts[] = array(
        'package_booking_contact_first_name' => $package_booking_contact->first_name,
        'package_booking_contact_last_name' => $package_booking_contact->last_name,
      );
    }
    
    return $package_booking_contacts;
  }
  
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Booking Detail'),
			'text_description' => __('Guest booking detail.'),
			
			'text_request_mark' => __('Booking marked as request'),
			'text_paid_mark' => __('Booking paid'),
			'text_cancelled_mark' => __('Booking cancelled'),
      
      'text_send_confirmation_email' => __('Send Confirmation'),
      'text_send_payment_request_email' => __('Send Payment Request'),
      'text_mark_as_paid' => __('Mark this booking as paid'),
      'text_mark_as_unpaid' => __('Mark this booking as unpaid'),
      'text_cancel_this_booking' => __('Cancel this booking'),
      'text_print_this_page' => __('Print this page'),
      
      'text_booking_information' => __('Booking Information'),
      'text_booking_id' => __('Booking ID'),
      'text_number_of_people' => __('Number of People'),
      'text_departure_date' => __('Departure Date'),
      'text_arrival_date' => __('Arrival Date'),

      'text_package_information' => __('Package Information'),
      'text_name' => __('Name'),
      'text_package_description' => __('Description'),
      'text_duration' => __('Duration'),
      'text_language' => __('Language'),
      'text_cancellation_policy' => __('Cancellation Policy'),
      
			'text_account_information' => __('Account Information'),
			'text_username' => __('Username'),
			'text_email' => __('Email'),
			
			'text_guest_information' => __('Guest Information'),
			'text_first_name' => __('First Name'),
			'text_last_name' => __('Last Name'),
			'text_telephone' => __('Telephone'),
			
			'text_price_information' => __('Price Information'),
      'text_edit_price' => __('Edit Price'),
      'text_amount' => __('Amount'),
      'text_adults' => __('Adults'),
      'text_children' => __('Children'),
      'text_infant' => __('Infant'),
      'text_additional' => __('Additional'),
      'text_information' => __('Information'),
      'text_price' => __('Price'),
      'text_subtotal' => __('Subtotal'),
      'text_total' => __('Total'),
			'text_point' => __('Hoterip Point'),
			
			'text_payment_information' => __('Payment Information'),
			'text_payment_method' => __('Payment Method'),
			'text_message' => __('Message'),

      'text_send_booking_completed' => __('Send Booking Completed'),
      
			'text_cancel_confirmation' => __('Are you sure want to cancel this booking? You can not undo this action.'),
			'text_send_payment_request_email_confirmation' => __('Are you sure want to send payment request email to guest?'),
      'text_send_confirmation_email_confirmation' => __('Are you sure want to send confirmation email to guest?'),
      'text_send_booking_completed_email_confirmation' => __('Are you sure want to send booking has been completed email to guest?'),
      
			// URL
      'url_base' => URL::base(),
			'url_edit_price' => Route::url('default', array('controller' => 'package_booking', 'action' => 'edit', 'id' => $this->package_booking->id)),
      
      // Currency
      'currency_name' => $this->currency->name,
			
			// Package Booking
			'package_booking_id' => $this->package_booking->id,
      'package_booking_paid' => (bool) $this->package_booking->is_paid,
      'package_booking_on_request' => (bool) $this->package_booking->is_on_request,
      'package_booking_cancelled' => (bool) $this->package_booking->is_cancelled,
      'package_booking_number_of_people' => __(':adults Adults, :children Children, :infants Infants', array(':adults' => $this->package_booking->adult_count, ':children' => $this->package_booking->child_count, ':infants' => $this->package_booking->infant_count)),
      'package_booking_departure_date' => date(__('M j, Y'), strtotime($this->package_booking->departure_date)),
      'package_booking_arrival_date' => date(__('M j, Y'), strtotime($this->package_booking->arrival_date)),
      'package_booking_contact_first_name' => $this->package_booking->contact_first_name,
      'package_booking_contact_last_name' => $this->package_booking->contact_last_name,
      'package_booking_contact_telephone' => $this->package_booking->contact_telephone,
      'package_booking_message' => Text::auto_p($this->package_booking->request),
      'package_booking_adult_count' => $this->package_booking->adult_count,
      'package_booking_child_count' => $this->package_booking->child_count,
      'package_booking_infant_count' => $this->package_booking->infant_count,
      'package_booking_adult_price' => number_format($this->package_booking->adult_price),
      'package_booking_child_price' => number_format($this->package_booking->child_price),
      'package_booking_infant_price' => number_format($this->package_booking->infant_price),
      'package_booking_total_adult_prices' => number_format($this->package_booking->adult_count * $this->package_booking->adult_price),
      'package_booking_total_child_prices' => number_format($this->package_booking->child_count * $this->package_booking->child_price),
      'package_booking_total_infant_prices' => number_format($this->package_booking->infant_count * $this->package_booking->infant_price),
      'package_booking_additional_price' => number_format($this->package_booking->additional_price),
      'package_booking_total_prices' => number_format($this->package_booking->adult_count * $this->package_booking->adult_price +
        $this->package_booking->child_count * $this->package_booking->child_price +
        $this->package_booking->infant_count * $this->package_booking->infant_price +
        $this->package_booking->additional_price),
      
      'show_child_count' => $this->package_booking->child_count > 0,
      'show_infant_count' => $this->package_booking->infant_count > 0,
      
      // Package
      'package_name' => $this->package->name,
      'package_description' => Text::auto_p($this->package->description),
      'package_language_name' => $this->package_language->name,
      'package_duration' => __(':days Days :nights Nights', array(':days' => $this->package->duration_days, ':nights' => $this->package->duration_nights)),
      'package_cancellation_policy' => Text::auto_p($this->package->cancellation_policy),
      
      // Payment type
      'payment_type_name' => $this->payment_type ? $this->payment_type->name : NULL,
      
			// Account
			'user_username' => $this->user->username,
			'user_email' => $this->user->email,
      'user_telephone' => $this->user->telephone,
		));
		
		return parent::render();
	}
	
}