<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Booking_Manage extends Kostache {
  
  public function package_bookings()
  {
    $package_bookings = array();
    
    foreach ($this->package_bookings as $package_booking)
    {
     $package_booking_classes = array();
     
      if ($package_booking->is_cancelled)
			{
				$package_booking_classes[] = 'red-bg';
			}
			elseif ($package_booking->is_paid)
			{
				$package_booking_classes[] = 'green-bg';
			}
			elseif ($package_booking->is_on_request)
			{
				$package_booking_classes[] = 'yellow-bg';
			}
      
      $package = unserialize($package_booking->package);
      
      $package_booking_comments = array();
      
      foreach ($package_booking->comments as $package_booking_comment)
			{
				$package_booking_comment_content = $package_booking_comment->content;
				
				if ($package_booking_comment->is_translate)
				{
					$package_booking_comment_content = __($package_booking_comment->content, unserialize($package_booking_comment->translate_data));
				}
				
				$package_booking_comments[] = array(
					'booking_comment_admin_name' => $package_booking_comment->admin_name,
					'booking_comment_content' => nl2br($package_booking_comment_content),
					'booking_time' => date(__('M j, Y H:i'), $package_booking_comment->created),
				);
			}
      
      $package_bookings[] = array(
        'package_booking_class' => implode(' ', $package_booking_classes),
        'package_booking_locked' => $package_booking->locker_id > 0,
        'package_booking_locker_username' => $package_booking->locker_username,
        'package_booking_lock_tooltip' => $package_booking->locker_id > 0 ? __('Locked by :name', array(':name' => $package_booking->locker_name)) : __('Unlocked'),
        'package_booking_id' => $package_booking->id,
        'package_booking_name' => $package->name,
        'package_booking_duration' => __(':days Days :nights Nights', array(':days' => $package->duration_days, ':nights' => $package->duration_nights)),
        'package_booking_user_username' => $package_booking->user_username,
        'package_booking_contact_name' => $package_booking->contact_first_name.' '.$package_booking->contact_last_name,
        'package_booking_number_of_people' => __(':adults Adults, :children Children, :infants Infants', array(':adults' => $package_booking->adult_count, ':children' => $package_booking->child_count, ':infants' => $package_booking->infant_count)),
        'package_booking_departure_date' => date(__('M j, Y'), strtotime($package_booking->departure_date)),
        'pacakge_booking_adult_count' => $package_booking->adult_count,
        'pacakge_booking_child_count' => $package_booking->child_count,
        'pacakge_booking_infant_count' => $package_booking->infant_count,
        'package_booking_paid' => (bool) $package_booking->is_paid,
        'package_booking_done' => (bool) $package_booking->is_done,
        
        'package_booking_lock_url' => Route::url('default', array('controller' => 'package_booking', 'action' => 'toggle', 'id' => $package_booking->id)).URL::query(array('field' => 'lock', 'value' => (int) ! ($package_booking->locker_id > 0)), FALSE),
				'package_booking_done_url' => Route::url('default', array('controller' => 'package_booking', 'action' => 'toggle', 'id' => $package_booking->id)).URL::query(array('field' => 'done'), FALSE),
        'package_booking_comment_url' => Route::url('default', array('controller' => 'package_booking', 'action' => 'toggle', 'id' => $package_booking->id)).URL::query(array('field' => 'read_comment'), FALSE),
        'package_booking_detail_link' => Route::url('default', array('controller' => 'package_booking', 'action' => 'detail', 'id' => $package_booking->id)),
        
        'package_booking_total_comments' => __(':total '.Inflector::plural('Comment', count($package_booking->comments)), array(':total' => count($package_booking->comments))),
				'package_booking_unread_comment' => $package_booking->unread_comment,
				'package_booking_comments' => $package_booking_comments,
      );
    }
    
    return $package_bookings;
  }
  
  public function render()
  {
    $this->set(array(
			// Text
			'text_title' => __('Manage Bookings'),
			'text_description' => __('Green bar means guest has completed the payment. Yellow bar means guest booking is marked as request. Red bar means booking is cancelled.'),
      
      'text_lock' => __('Lock'),
      'text_done' => __('Done'),
      'text_id' => __('ID'),
      'text_name' => __('Name'),
      'text_duration' => __('Duration'),
      'text_username' => __('Username'),
      'text_guest_name' => __('Guest Name'),
      'text_departure_date' => __('Departure Date'),
      'text_number_of_people' => __('Number of People'),
      'text_created' => __('Created'),
      'text_paid' => __('Paid'),
      'text_comment' => __('Comment'),
      'text_detail' => __('Detail'),
      'text_lock_confirmation' => __('Do you want to lock this booking?'),
			'text_unlock_confirmation' => __('This booking is locked by :username. Are you sure want to unlock this booking?'),
      'text_your_comment' => __('Your comment'),
      'text_submit' => __('Submit'),
      
      // URL
      'url_base' => URL::base(),
      
      // Logged in admin
      'logged_in_admin_username' => $this->logged_in_admin->username,
    ));
        
    return parent::render();
  }
  
}