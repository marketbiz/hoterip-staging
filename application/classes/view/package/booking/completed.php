<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Booking_Completed extends Kostache {
	
	public function render()
	{
		$this->set(array(
			// Text
      'text_1' => I18n::get('Thank you for the payment and your booking has been confirmed.', $this->language->code),
      'text_2' => I18n::get('You can see the details of your booking from the link below.', $this->language->code),
      'text_thank_you' => I18n::get('Thank you', $this->language->code),
      
      // URL
      'url_account_bookings' => 'http://hoterip.com/account/package_booking/'.$this->package_booking->id,
		));
		
		return parent::render();
	}
	
}