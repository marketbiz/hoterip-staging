<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Booking_Edit extends Kostache {
	
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Edit Bookings'),
			'text_description' => __('Edit booking price.'),
      'text_price' => __('Price'),
			'text_adult_price' => __('Adult Price'),
			'text_child_price' => __('Child Price'),
			'text_infant_price' => __('Infant Price'),
      'text_additional_price' => __('Additional Price'),
      'text_submit' => __('Submit'),
			
			// Form
      'form_adult_price_value' => Arr::get($this->values, 'adult_price', $this->package_booking->adult_price),
      'form_child_price_value' => Arr::get($this->values, 'child_price', $this->package_booking->child_price),
      'form_infant_price_value' => Arr::get($this->values, 'infant_price', $this->package_booking->infant_price),
      'form_additional_price_value' => Arr::get($this->values, 'additional_price', $this->package_booking->additional_price),
      
      // Currency
      'currency_name' => $this->currency->name,
		));
		
		return parent::render();
	}
	
}