<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Booking_Payment extends Kostache {
	
	public function total_prices()
  {
    return number_format($this->package_booking->adult_count * $this->package_booking->adult_price + 
      $this->package_booking->child_count * $this->package_booking->child_price + 
      $this->package_booking->infant_count * $this->package_booking->infant_price + 
      $this->package_booking->additional_price);
  }
	
	public function render()
	{
		$this->set(array(
			// Text
      'text_package_name' => __('Package Name'),
      'text_total_to_be_paid' => __('Total to be paid'),
      'text_thank_you' => __('Thank you'),
      
			'text_1' => I18n::get('Thank you for booking using Hoterip.', $this->language->code),
			'text_2' => I18n::get('To complete your payment please complete the following transactions.', $this->language->code),
			'text_3' => I18n::get('If you have not paid within 2 days then your order will be canceled automatically.', $this->language->code),
			'text_4' => I18n::get('Please click the link below to let us know after you make payment.', $this->language->code),
      
      'url_payment_confirm' => 'http://hoterip.com/package/payment_confirm?booking_id='.$this->package_booking->id,
      
      'eol' => PHP_EOL,
      
      'package_name' => $this->package->name,
      'currency_symbol' => $this->currency->symbol,
      'payment_type_description' => $this->payment_type->description,
		));
		
		return parent::render();
	}
	
}