<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Edit2 extends Kostache {
	
  public function schedules()
  {
    $schedules = array();
    
    $schedule_descriptions = Arr::get($this->values, 'schedule_descriptions', array());
    $schedule_places = Arr::get($this->values, 'schedule_places', array());
    $schedule_is_breakfasts = Arr::get($this->values, 'schedule_is_breakfasts', array());
    $schedule_is_lunchs  = Arr::get($this->values, 'schedule_is_lunchs', array());
    $schedule_is_dinners = Arr::get($this->values, 'schedule_is_dinners', array());
      
    for ($i = 1; $i <= $this->duration_days; $i++)
    {
      if ( ! $package_schedule = Arr::get($this->package_schedules, $i))
      {
        $package_schedule = new stdClass;
        $package_schedule->day = $i;
        $package_schedule->description = '';
        $package_schedule->place = '';
        $package_schedule->is_breakfast = 0;
        $package_schedule->is_lunch = 0;
        $package_schedule->is_dinner = 0;
      }
      
      $schedules[] = array(
        'schedule_day' => $i,
        'schedule_form_schedule_descriptions_value' => Arr::get($schedule_descriptions, $i, $package_schedule->description),
        'schedule_form_schedule_places_value' => Arr::get($schedule_places, $i, $package_schedule->place),
        'schedule_form_schedule_is_breakfasts_checked' => (bool) Arr::get($schedule_is_breakfasts, $i, $package_schedule->is_breakfast),
        'schedule_form_schedule_is_lunchs_checked' => (bool) Arr::get($schedule_is_lunchs, $i, $package_schedule->is_lunch),
        'schedule_form_schedule_is_dinners_checked' => (bool) Arr::get($schedule_is_dinners, $i, $package_schedule->is_dinner),
      );
    }
    
    return $schedules;
  }
  
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Edit package'),
			'text_description' => __('Edit package.'),

      'text_information' => __('Information'),
      'text_schedules' => __('Schedules'),
      'text_prices' => __('Prices'),
      'text_photos' => __('Photos'),
      'text_confirmation' => __('Confirmation'),
			'text_day' => __('Day'),
      'text_stay_place' => __('Stay City'),
      'text_schedule_description' => __('Description'),
      'text_meal' => __('Meal'),
      'text_breakfast' => __('Breakfast'),
      'text_lunch' => __('Lunch'),
      'text_dinner' => __('Dinner'),
      'text_required' => __('Required'),
      'text_next_step' => __('Next Step'),
      
      // URL
      'url_base' => URL::base(),
		));
    
		return parent::render();
	}
	
}