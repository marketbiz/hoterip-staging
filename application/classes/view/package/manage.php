<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Manage extends Kostache {
	
  public function packages()
  {
    $packages = array();
    
    foreach ($this->packages as $package)
    {
      $participants = array();
    
      if ($package->is_adult_can_participate)
      {
        $participants[] = __('Adult');
      }

      if ($package->is_child_can_participate)
      {
        $participants[] = __('Child');
      }

      if ($package->is_infant_can_participate)
      {
        $participants[] = __('Infant');
      }
      
      $package_classes = array();
      
      if (strtotime($package->booking_end_date) < strtotime(date('Y-m-d')))
			{
				$package_classes[] = 'red-bg';
			}
			elseif ($package->is_show)
			{
				$package_classes[] = 'green-bg';
			}
			else
			{
				$package_classes[] = 'yellow-bg';
			}
      
      if ($package->is_flight AND $package->is_hotel AND $package->is_activity)
      {
        $package_type = __('All Inclusive');
      }
      elseif ($package->is_flight AND $package->is_hotel)
      {
        $package_type = __('Free and Easy');
      }
      elseif ($package->is_hotel AND $package->is_activity)
      {
        $package_type = __('Land Package');
      }
      else
      {
        $package_type = '';
      }
    
      $packages[] = array(
        'package_class' => implode(' ', $package_classes),
        'package_type' => $package_type,
        'package_order' => $package->order,
        'package_id' => $package->id,
        'package_name' => $package->name,
        'package_description' => nl2br($package->description),
        'package_is_flight' => $package->is_flight,
        'package_is_hotel' => $package->is_hotel,
        'package_is_activity' => $package->is_activity,
        'package_is_transport' => $package->is_transport,
        'package_duration' => __(':days Days :nights Nights', array(':days' => $package->duration_days, ':nights' => $package->duration_nights)),
        'package_language_name' => $package->language_name,
        'package_participants' => implode(', ', $participants),
        'package_minimum_adults' => $package->minimum_adults,
        'package_booking_start_date' => date('j M, Y', strtotime($package->booking_start_date)),
        'package_booking_end_date' => date('j M, Y', strtotime($package->booking_end_date)),
        'package_stay_start_date' => date('j M, Y', strtotime($package->stay_start_date)),
        'package_stay_end_date' => date('j M, Y', strtotime($package->stay_end_date)),
        'package_cancellation_policy' => nl2br($package->cancellation_policy),
        'package_edit_url' => Route::url('default', array('controller' => 'package', 'action' => 'edit', 'id' => $package->id)),
				'package_delete_url' => Route::url('default', array('controller' => 'package', 'action' => 'delete', 'id' => $package->id)),
      );
    }
    
    return $packages;
  }
  
	public function render()
	{
    $this->set(array(
      // Text
      'text_title' => __('Manage Packages'),
      'text_description' => __('Add, edit or delete packages. Green bar means package is currenty running and shown to the guests. Yellow bar means package is not running and hidden from the guests. Red bar means the package is not effective anymore because the booking date has passed today.'),
      'text_add' => __('Add new package'),
      'text_delete_checked' => __('Delete checked'),
      'text_show' => __('Show'),
      'text_hide' => __('Hide'),
      
      'text_type' => __('Type'),
      'text_order' => __('Order'),
      'text_name' => __('Name'),
      'text_package_description' => __('Description'),
      'text_duration' => __('Duration'),
      'text_language' => __('Language'),
      'text_booking_date' => __('Booking Date'),
      'text_stay_date' => __('Stay Date'),
      'text_detail' => __('Detail'),
      'text_participants' => __('Participants'),
      'text_minimum_adults' => __('Minimum Adults'),
      'text_cancellation_policy' => __('Cancellation Policy'),
      'text_action' => __('Action'),
      'text_edit' => __('Edit'),
      'text_delete' => __('Delete'),
      
      'text_flight_included' => __('Flight Included'),
      'text_hotel_included' => __('Hotel Included'),
      'text_activity_included' => __('Activity Included'),
      'text_transportation_included' => __('Transportation Included'),
      
      // URL
      'url_base' => URL::base(),
			'url_package_add' => Route::url('default', array('controller' => 'package', 'action' => 'add')),
    ));
    
		return parent::render();
	}
	
}