<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Edit5 extends Kostache {
	
  public function participate()
  {
    $participate = array();
    
    if (Arr::get($this->values, 'is_adult_can_participate'))
    {
      $participate[] = __('Adult');
    }
    
    if (Arr::get($this->values, 'is_child_can_participate'))
    {
      $participate[] = __('Child');
    }
    
    if (Arr::get($this->values, 'is_infant_can_participate'))
    {
      $participate[] = __('Infant');
    }
    
    return implode(', ', $participate);
  }
  
  public function schedules()
  {
    $schedules = array();
    
    $schedule_days = Arr::get($this->values, 'schedule_days', array());
    $schedule_descriptions = Arr::get($this->values, 'schedule_descriptions', array());
    $schedule_places = Arr::get($this->values, 'schedule_places', array());
    $schedule_is_breakfasts = Arr::get($this->values, 'schedule_is_breakfasts', array());
    $schedule_is_lunchs = Arr::get($this->values, 'schedule_is_lunchs', array());
    $schedule_is_dinners = Arr::get($this->values, 'schedule_is_dinners', array());
    
    foreach ($schedule_days as $key => $schedule_day)
    {
      $meals = array();
      
      if (Arr::get($schedule_is_breakfasts, $key))
      {
        $meals[] = __('Breakfast');
      }
      
      if (Arr::get($schedule_is_lunchs, $key))
      {
        $meals[] = __('Lunch');
      }
      
      if (Arr::get($schedule_is_dinners, $key))
      {
        $meals[] = __('Dinner');
      }
    
      $schedules[] = array(
        'schedule_day' => $schedule_day,
        'schedule_description' => Text::auto_p(Arr::get($schedule_descriptions, $key)),
        'schedule_place' => Arr::get($schedule_places, $key),
        'schedule_meal' => implode(', ', $meals),
      );
    }
    
    return $schedules;
  }
  
  public function items()
  {
    $items = array();
    
    $item_dates = Arr::get($this->values, 'item_dates', array());
    $item_adult_prices = Arr::get($this->values, 'item_adult_prices', array());
    $item_child_prices = Arr::get($this->values, 'item_child_prices', array());
    $item_infant_prices = Arr::get($this->values, 'item_infant_prices', array());
    
    foreach ($item_dates as $key => $item_date)
    {
      $items[] = array(
        'item_date' => date('M j, Y', strtotime($item_date)),
        'item_adult_price' => number_format(Arr::get($item_adult_prices, $key)),
        'item_child_price' => number_format(Arr::get($item_child_prices, $key)),
        'item_infant_price' => number_format(Arr::get($item_infant_prices, $key)),
      );
    }
    
    return $items;
  }
  
  public function photos()
  {
    $photos = array();
    
    $photo_filenames = Arr::get($this->values, 'photo_filenames');
    $photo_names = Arr::get($this->values, 'photo_names');
    
    foreach ($photo_filenames as $key => $photo_filename)
    {
      $photos[] = array(
        'photo_filename' => Arr::get($photo_filenames, $key),
        'photo_url' => URL::site('media/upload/'.Arr::get($photo_filenames, $key)),
        'photo_name' => Arr::get($photo_names, $key),
      );
    }
    
    return $photos;
  }
  
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Add package'),
			'text_description' => __('Add new package for your hotel. For example: Staying 3 days will receive 20% off. Note: You must also create normal price package.'),

      'text_information' => __('Information'),
      'text_schedules' => __('Schedules'),
      'text_prices' => __('Prices'),
      'text_photos' => __('Photos'),
      'text_confirmation' => __('Confirmation'),
			'text_package_title' => __('Title'),
			'text_package_title_description' => __('Enter package name. Enter package name in capital letters on each first letter in the word. For example: Deluxe 3 Nights Special.'),
			'text_package_description' => __('Description'),
			'text_package_description_description' => __('Enter package description.'),
      'text_page_availability' => __('Page Availability'),
      'text_page_availability_description' => __('Select language page where this campaign will appear.'),
      'text_duration' => __('Duration'),
      'text_duration_description' => __('Enter duration for this package.'),
      'text_adult' => __('Adult'),
      'text_child' => __('Child'),
      'text_infant' => __('Infant'),
      'text_participate' => __('Participate'),
      'text_participate_description' => __('Select who can participate in this package.'),
      'text_minimum_adults' => __('Minimum Adults'),
      'text_minimum_adults_description' => __('Enter minimum adults for this package.'),
      'text_booking_date' => __('Booking Date'),
      'text_booking_date_description' => __('Select booking date for this package. For example: If you set the booking date from 2011-07-01 to 2011-07-05, then the guests can only see the package when today is between 2011-07-01 to 2011-07-05.'),
      'text_stay_date' => __('Stay Date'),
      'text_stay_date_description' => __('Select stay date for this package. For example: If you set the stay date from 2011-07-10 to 2011-07-20, then the guests can only stay between 2011-07-10 to 2011-07-20.'),
      'text_cancellation_policy' => __('Cancellation Policy'),
      'text_cancellation_policy_description' => __('Enter cancellation policy for this package.'),
      'text_flight' => __('Flight'),
      'text_places' => __('Places'),
      'text_places_description' => __('Enter flight place for this package. To add place use the plus button, and to remove place use the minus button. For example if the package from Jakarta to Singapore to Kuala Lumpur and back again to Jakarta, you need to enter three places, which is "Jakarta", "Singapore" and "Kuala Lumpur".'),
      'text_airline' => __('Airline'),
      'text_airline_description' => __('Enter flight airline company. For example: Garuda Indonesia. If the package using multiple airlines, separate it with commas, for example: "Garuda Indonesia, ANA". If the package airline is optional or not decided yet, leave blank, or enter possible airline using OR words, for example: "Garuda Indonesia or ANA"'),
      'text_hotel' => __('Hotel'),
      'text_hotel_names' => __('Names'),
      'text_hotel_names_description' => __('Enter hotel name for this package. For example: Conrad Bali. If the package using multiple hotels, separate it with commas, for example: "Conrad Bali, Aston Kuta". '),
      'text_activity' => __('Activity'),
      'text_activity_description' => __('Check the checkbox if the activity has been determined. For example, a tour can be categorized as activity because every day schedule has been determined.'),
      'text_calendar_caption' => __('Calendar Caption'),
      'text_cancellation_policy' => __('Cancellation Policy'),
      'text_days' => __('Days'),
      'text_nights' => __('Nights'),
      
      'text_day' => __('Day'),
      'text_stay_place' => __('Stay City'),
      'text_schedule_description' => __('Description'),
      'text_meal' => __('Meal'),
      'text_breakfast' => __('Breakfast'),
      'text_lunch' => __('Lunch'),
      'text_dinner' => __('Dinner'),
      
      'text_price_from' => __('Price From'),
      'text_date' => __('Date'),
      'text_adult_price' => __('Adult Price'),
      'text_child_price' => __('Child Price'),
      'text_infant_price' => __('Infant Price'),
      
      'text_package' => __('Package'),
      'text_show' => __('Show'),
      'text_show_description' => __('Show package on the website after submit.'),
      'text_order' => __('Order'),
      'text_order_description' => __('Enter sorting order. The higher number being entered, the more it will be displayed at the top in package list.'),
      
      'text_required' => __('Required'),
      'text_submit' => __('Submit'),
      
      // URL
      'url_base' => URL::base(),
      
      // Form
      'form_is_show_checked' => (bool) Arr::get($this->values, 'is_show', $this->package->is_show),
      'form_order_value' => Arr::get($this->values, 'order', $this->package->order),
      
      // Package
      'title' => Arr::get($this->values, 'title'),
      'description' => Text::auto_p(Arr::get($this->values, 'description')),
      'language_name' => $this->language->name,
      'duration_days' => Arr::get($this->values, 'duration_days'),
      'duration_nights' => Arr::get($this->values, 'duration_nights'),
      'minimum_adults' => Arr::get($this->values, 'minimum_adults'),
      'booking_start_date' => date('M j, Y', strtotime(Arr::get($this->values, 'booking_start_date'))),
      'booking_end_date' => date('M j, Y', strtotime(Arr::get($this->values, 'booking_end_date'))),
      'stay_start_date' => date('M j, Y', strtotime(Arr::get($this->values, 'stay_start_date'))),
      'stay_end_date' => date('M j, Y', strtotime(Arr::get($this->values, 'stay_end_date'))),
      'cancellation_policy' => Text::auto_p(Arr::get($this->values, 'cancellation_policy')),
      'is_flight' => (bool) Arr::get($this->values, 'is_flight'),
      'flight_places' => Arr::get($this->values, 'flight_places'),
      'flight_airline' => Arr::get($this->values, 'flight_airline'),
      'is_hotel' => (bool) Arr::get($this->values, 'is_hotel'),
      'hotel_names' => Arr::get($this->values, 'hotel_names'),
      'is_activity' => (bool) Arr::get($this->values, 'is_activity'),
      
      'price_from' => number_format(Arr::get($this->values, 'price_from')),
      'currency_name' => $this->currency->name,
		));
    
		return parent::render();
	}
	
}