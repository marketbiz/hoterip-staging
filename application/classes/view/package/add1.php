<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Add1 extends Kostache {
	
  public function languages()
  {
    $languages = array();
    
    foreach ($this->languages as $language)
    {
      $languages[] = array(
        'language_id' => $language->id,
        'language_name' => $language->name,
        'form_language_id_selected' => Arr::get($this->values, 'language_id') == $language->id,
      );
    }
    
    return $languages;
  }
  
	public function package_texts()
	{
		$package_texts = array();
		
		foreach ($this->languages as $language)
		{			
			$package_texts[] = array(
				'form_package_text_name' => Form::input("names[{$language->id}]", Arr::path($this->values, "names.{$language->id}"), array('size' => 30)),
				'form_package_text_description' => Form::textarea("descriptions[{$language->id}]", Arr::path($this->values, "descriptions.{$language->id}"), array('rows' => 6)),
				'form_package_text_cancellation_policy' => Form::textarea("cancellation_policies[{$language->id}]", Arr::path($this->values, "cancellation_policies.{$language->id}"), array('rows' => 6)),
				'package_text_language_name' => $language->name,
				'package_text_language_file' => URL::site("media/images/languages/$language->code.png")
			);
		}
		
		return $package_texts;
	}
  
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Add package'),
			'text_description' => __('Add new package for your hotel. For example: Staying 3 days will receive 20% off. Note: You must also create normal price package.'),

      'text_information' => __('Information'),
      'text_schedules' => __('Schedules'),
      'text_prices' => __('Prices'),
      'text_photos' => __('Photos'),
      'text_confirmation' => __('Confirmation'),
			'text_package_name' => __('Name'),
			'text_package_name_description' => __('Enter package name. Enter package name in capital letters on each first letter in the word. For example: Deluxe 3 Nights Special.'),
			'text_package_description' => __('Description'),
			'text_package_description_description' => __('Enter package description.'),
      'text_page_availability' => __('Page Availability'),
      'text_page_availability_description' => __('Select language page where this campaign will appear.'),
      'text_duration' => __('Duration'),
      'text_duration_description' => __('Enter duration for this package.'),
      'text_adult' => __('Adult'),
      'text_child' => __('Child'),
      'text_infant' => __('Infant'),
      'text_participate' => __('Participate'),
      'text_participate_description' => __('Select who can participate in this package.'),
      'text_minimum_adults' => __('Minimum Adults'),
      'text_minimum_adults_description' => __('Enter minimum adults for this package.'),
      'text_booking_date' => __('Booking Date'),
      'text_booking_date_description' => __('Select booking date for this package. For example: If you set the booking date from 2011-07-01 to 2011-07-05, then the guests can only see the package when today is between 2011-07-01 to 2011-07-05.'),
      'text_stay_date' => __('Stay Date'),
      'text_stay_date_description' => __('Select stay date for this package. For example: If you set the stay date from 2011-07-10 to 2011-07-20, then the guests can only stay between 2011-07-10 to 2011-07-20.'),
      'text_cancellation_policy' => __('Cancellation Policy'),
      'text_cancellation_policy_description' => __('Enter cancellation policy for this package.'),
      'text_flight' => __('Flight'),
      'text_places' => __('Places'),
      'text_places_description' => __('Enter flight place for this package. To add place use the plus button, and to remove place use the minus button. For example if the package from Jakarta to Singapore to Kuala Lumpur and back again to Jakarta, you need to enter three places, which is "Jakarta", "Singapore" and "Kuala Lumpur".'),
      'text_airline' => __('Airline'),
      'text_airline_description' => __('Enter flight airline company. For example: Garuda Indonesia. If the package using multiple airlines, separate it with commas, for example: "Garuda Indonesia, ANA". If the package airline is optional or not decided yet, leave blank, or enter possible airline using OR words, for example: "Garuda Indonesia or ANA"'),
      'text_hotel' => __('Hotel'),
      'text_hotel_names' => __('Names'),
      'text_hotel_names_description' => __('Enter hotel name for this package. For example: Conrad Bali. If the package using multiple hotels, separate it with commas, for example: "Conrad Bali, Aston Kuta". '),
      'text_activity' => __('Activity'),
      'text_activity_description' => __('Check the checkbox if the activity has been determined. For example, a tour can be categorized as activity because every day schedule has been determined.'),
      'text_calendar_caption' => __('Calendar Caption'),
      'text_cancellation_policy' => __('Cancellation Policy'),
      'text_days' => __('Days'),
      'text_nights' => __('Nights'),
      'text_next_step' => __('Next Step'),
			'text_required' => __('Required'),
			
			// Form
      'form_title_value' => Arr::get($this->values, 'title'),
      'form_description' => Arr::get($this->values, 'description'),
      'form_duration_days_value' => Arr::get($this->values, 'duration_days', 3),
      'form_duration_nights_value' => Arr::get($this->values, 'duration_nights', 2),
      'form_is_adult_can_participate_checked' => (bool) Arr::get($this->values, 'is_adult_can_participate', 1),
      'form_is_child_can_participate_checked' => (bool) Arr::get($this->values, 'is_child_can_participate'),
      'form_is_infant_can_participate_checked' => (bool) Arr::get($this->values, 'is_infant_can_participate'),
      'form_minimum_adults_value' => Arr::get($this->values, 'minimum_adults', 1),
      'form_booking_start_date_value' => Arr::get($this->values, 'booking_start_date', date('Y-m-d')),
      'form_booking_end_date_value' => Arr::get($this->values, 'booking_end_date', date('Y-m-d', strtotime('+2 days'))),
      'form_stay_start_date_value' => Arr::get($this->values, 'stay_start_date', date('Y-m-d')),
      'form_stay_end_date_value' => Arr::get($this->values, 'stay_end_date', date('Y-m-d', strtotime('+2 days'))),
      'form_cancellation_policy' => Arr::get($this->values, 'cancellation_policy'),
      'form_is_flight_checked' => (bool) Arr::get($this->values, 'is_flight'),
      'form_flight_places' => Arr::get($this->values, 'flight_places'),
      'form_flight_airline_value' => Arr::get($this->values, 'flight_airline'),
      'form_is_hotel_checked' => (bool) Arr::get($this->values, 'is_hotel'),
      'form_hotel_names' => Arr::get($this->values, 'hotel_names'),
      'form_is_activity_checked' => (bool) Arr::get($this->values, 'is_activity'),
      'form_calendar_caption_value' => Arr::get($this->values, 'calendar_caption'),
      'form_cancellation_policy_value' => Arr::get($this->values, 'cancellation_policy'),
      
      // URL
      'url_base' => URL::base(),
		));
    
		return parent::render();
	}
	
}