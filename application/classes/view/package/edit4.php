<?php defined('SYSPATH') or die('No direct script access.');

class View_Package_Edit4 extends Kostache {
	
  public function photos()
  {
    $photos = array();
    
    $photo_names = Arr::get($this->values, 'photo_names');
    
    for ($i = 0; $i < 4; $i++)
    {
      if ( ! $package_photo = Arr::get($this->package_photos, $i))
      {
        $package_photo = new stdClass;
        $package_photo->package_id = 0;
        $package_photo->name = '';
        $package_photo->filename = '';
      }
      
      $photos[] = array(
        'photo_index' => $i,
        'photo_form_photo_names_value' => Arr::get($photo_names, $i, $package_photo->name),
        'photo_url' => Kohana::config('application.package_url').'/package_'.$package_photo->package_id.'/'.$package_photo->filename,
        'photo_show_photo' => $package_photo->package_id > 0,
      );
    }
    
    return $photos;
  }
  
	public function render()
	{
		$this->set(array(
			// Text
			'text_title' => __('Add package'),
			'text_description' => __('Add new package for your hotel. For example: Staying 3 days will receive 20% off. Note: You must also create normal price package.'),

      'text_information' => __('Information'),
      'text_schedules' => __('Schedules'),
      'text_prices' => __('Prices'),
      'text_photos' => __('Photos'),
      'text_confirmation' => __('Confirmation'),
      'text_name' => __('Name'),
      'text_name_description' => __('Enter name for this photo.'),
      'text_photo' => __('Photo'),
			'text_photo_description' => __('Select photo for this package.'),
      'text_required' => __('Required'),
      'text_next_step' => __('Next Step'),
      
      // URL
      'url_base' => URL::base(),
		));
    
		return parent::render();
	}
	
}