<?php defined('SYSPATH') or die('No direct script access.');

class View_Home_Dashboard extends Kostache {

  public function top_city_hotels()
  {
    $top_city_hotels = array();
    
    foreach ($this->top_city_hotels as $key => $hotel)
    {
      $top_city_hotels[] = Arr::merge($hotel->as_array(), array(
        'number' => $key + 1,
        'is_selected_hotel' => $hotel->id == $this->selected_hotel->id,
//        'show_up' => $hotel->rank > $hotel->old_rank,
//        'show_down' => $hotel->rank < $hotel->old_rank,
      ));
    }
    
    return $top_city_hotels;
  }
  
  public function top_district_hotels()
  {
    $top_district_hotels = array();
    
    foreach ($this->top_district_hotels as $key => $hotel)
    {
      $top_district_hotels[] = Arr::merge($hotel->as_array(), array(
        'number' => $key + 1,
        'is_selected_hotel' => $hotel->id == $this->selected_hotel->id,
//        'show_up' => $hotel->rank > $hotel->old_rank,
//        'show_down' => $hotel->rank < $hotel->old_rank,
      ));
    }
    
    return $top_district_hotels;
  }
  
  public function show_rank_position_in_cities()
  {
    return $this->rank_position_in_cities > 10;
  }
  
  public function show_rank_position_in_districts()
  {
    return $this->rank_position_in_districts > 10;
  }
  
  public function campaigns()
  {
    foreach ($this->campaigns as $key => $campaign)
    {
      if ($campaign->is_default)
      {
        $benefit = '';
      }
      else
      {
        $benefit = '';
        
        if ($campaign->discount_rate > 0)
        {
          $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
            ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
            ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
            ':discount_rate' => $campaign->discount_rate,
          ));
        }
        elseif ($campaign->discount_amount > 0)
        {
          $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
            ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
            ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
            ':currency_code' => $this->hotel_currency->code,
            ':discount_amount' => $campaign->discount_amount,
          ));
        }
        elseif ($campaign->number_of_free_nights)
        {
          $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
            ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
            ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
            ':number_of_free_nights' => $campaign->number_of_free_nights,
          ));
        }

        if ($campaign->type == 2)
        {
          $benefit .= ' '.__('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
        }
        elseif ($campaign->type == 3)
        {
          $benefit .= ' '.__('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
        }
        elseif ($campaign->type == 4)
        {
          $benefit .= ' '.__('Non refundable.');
        }
        elseif ($campaign->type == 5)
        {
          $benefit .= ' '.__('Flash Deal.');
        }
      }
      
      $campaign->benefit = $benefit;
      $campaign->number = $key + 1;
    }
    
    return $this->campaigns;
  }
  
}