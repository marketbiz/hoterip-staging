<?php defined('SYSPATH') or die('No direct script access.');

class View_Item_Qa_Manage extends Kostache {
	
	public function items()
	{
		$items = array();
		
		foreach ($this->changed_items as $date => $changed_item)
		{
			$changed_prev_item = Arr::get($this->changed_prev_items, $date);
			
			$items[] = array_merge((array) $changed_item, array(
				'prev_stock' => $changed_prev_item->stock ? $changed_prev_item->stock : 'None',
				'prev_price' => $changed_prev_item->price ? $changed_prev_item->price : 'None',
				'prev_minimum_night' => $changed_prev_item->minimum_night ? $changed_prev_item->minimum_night : 'None',
				'prev_is_blackout' => $changed_prev_item->is_blackout ? 'Yes' : 'No',
				'prev_is_campaign_blackout' => $changed_prev_item->is_campaign_blackout ? 'Yes' : 'No',
				
				'day' => date('D', strtotime($changed_item->date)),
				'display_date' => date('M j, Y', strtotime($changed_item->date)),
				'minimum_night' => $changed_item->minimum_night,
				'is_blackout' => $changed_item->is_blackout ? 'Yes' : 'No',
				'is_campaign_blackout' => $changed_item->is_campaign_blackout ? 'Yes' : 'No',
			));
		}
		
		return $items;
	}
  
}