<?php defined('SYSPATH') or die('No direct script access.');

class View_Item_Manage extends Kostache {
  
  public function rooms()
  {
    $rooms = array();
    
    foreach ($this->rooms as $room)
    {
      $rooms[] = $room->as_array() + array(
        'selected' => $room->id == Arr::get($this->values, 'room_id'),
      );
    }
    
    return $rooms;
  }
	
  public function campaigns()
  {
    $campaigns = array();
    
    foreach ($this->campaigns as $campaign)
    {
      $benefit = '';

      /// Additional Benefit promotion name
      $room_names = $campaign->rooms
        ->select(array('room_texts.name', 'name'))
        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
        ->where('room_texts.language_id', '=', $this->selected_language->id)
        ->find_all()
        ->as_array('id', 'name');

      if ($campaign->discount_rate > 0)
      {
        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':discount_rate' => $campaign->discount_rate,
        ));
      }
      elseif ($campaign->discount_amount > 0)
      {
        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':currency_code' => $this->currency->code,
          ':discount_amount' => $campaign->discount_amount,
        ));
      }
      elseif ($campaign->number_of_free_nights)
      {
        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':number_of_free_nights' => $campaign->number_of_free_nights,
        ));
      }
      /// Additional Benefit promotion name
      else
      {
        $other_benefits = DB::select(
            'campaigns_benefits.*',
            array('benefit_texts.name', 'name')
          )
          ->from('campaigns_benefits')
          ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
          ->where('benefit_texts.language_id', '=', $this->selected_language->id)
          ->where('campaigns_benefits.campaign_id', '=', $campaign->id)
          ->execute();

        if(count($other_benefits))
        {
          $room_campaigns = count($this->rooms) == count($room_names) ? __('All Rooms') : implode(', ', $room_names);

          $benefit = $room_campaigns;
          $benefit .= __(' get ');

          foreach ($other_benefits as $count =>$other_benefit)
          {
            $benefit .= $other_benefit['name'];
            if($other_benefit['value'])
            {
              $benefit .= ' for '.$other_benefit['value'];
            }

            $benefit .=  count($other_benefits) == ($count + 1) ? '.' : ', ';
          }
        }
      }

      if ($campaign->type == 2)
      {
        $benefit .= ' '.__('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
      }
      elseif ($campaign->type == 3)
      {
        $benefit .= ' '.__('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
      }
      elseif ($campaign->type == 4)
      {
        $benefit .= ' '.__('Non refundable.');
      }
      elseif ($campaign->type == 5)
      {
        $benefit .= ' '.__('Flash Deal.');
      }
      elseif ($campaign->is_default)
      {
        //$benefit .= ' '.__('Stay at least 1 nights and book 1 or more rooms. No refundable');
        $benefit .= ' '.__('BAR.');
        $campaign->id = 0;
      }
      
      $campaigns[] = $campaign->as_array() + array(
        'selected' => $campaign->id == Arr::get($this->values, 'campaign_id'),
        'name' => $benefit ,
      );
    }
    
    return $campaigns;
  }

  public function items()
  {
    $items = array();
    $current = DB::select('code')
      ->from('currencies')
      ->where('id', '=', $this->selected_hotel->currency_id)
      ->execute()
      ->as_array();
    
    $codeCurrent = $current[0]['code'];
    foreach ($this->dates as $date)
    {
      $item = Arr::get($this->items, $date);
      /// Hoterip campaign
      if ( ! $item)
      {
        $item = ORM::factory('item');
        $item->date = $date;
        $item->stock = NULL;
        $item->price = NULL;
        $item->additional_pax_price = 0;
        $item->extrabed_item_price = 0;
        $item->net_price = NULL;
        $item->minimum_night = 1;
        $item->sold = 0;
        $item->is_blackout = 0;
        $item->is_campaign_blackout = 0;
      }

      $item->stock = Arr::path($this->values, "stocks.$date", $item->stock);
      $item->price = Arr::path($item->price, "prices.$date", $item->price);
      $item->additional_pax_price = Arr::path($this->values, "additional_pax_price.$date", $item->additional_pax_price);
      $item->extrabed_item_price = $this->room->extrabed_price;
      $item->net_price = Arr::path($this->values, "net_prices.$date", $item->net_price);
      $item->minimum_night = Arr::path($this->values, "minimum_nights.$date", $item->minimum_night);
      $item->is_blackout = Arr::path($this->values, "is_blackouts.$date", $item->is_blackout);
      $item->is_campaign_blackout = Arr::path($this->values, "is_campaign_blackouts.$date", $item->is_campaign_blackout);

      $items[] = Arr::merge($item->as_array(), array(
        'day' => __(date('D', strtotime($item->date))),
        'display_date' => date(__('M j, Y'), strtotime($item->date)),
        'dow' => date('w', strtotime($item->date)),
        'revenue' => preg_replace('/(\d)(?=(\d\d\d)+(?!\d))/', '$1,', $item->price - ($item->price * $this->selected_hotel->commission_rate / 100)),
        'currency' => $codeCurrent,
      ));
    }

    return $items;
  }
}