<?php defined('SYSPATH') or die('No direct script access.');

class View_Item_Export extends Kostache {
  
  public function years()
  {
    $values = Date::years();
    $years = array();
    
    foreach ($values as $value)
    {
      $years[] = array(
        'value' => $value,
        'selected' => Arr::get($this->values, 'year', date('Y')) == $value,
      );
    }
    
    return $years;
  }

}