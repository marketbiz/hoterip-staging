<?php defined('SYSPATH') or die('No direct script access.');

class View_Hotel_Edit extends Kostache {
  
  protected $_hotel;
  protected $_hotel_facility;
  protected $_hotel_service;

  public function hotel()
  {
    if ( ! $this->_hotel)
    {
      $this->_hotel = Arr::overwrite($this->hotel->as_array(), $this->values);
      
      if ( ! $this->_hotel['coordinate'])
      {
        $this->_hotel['coordinate'] = '0,0';
      }
    }
    
    return $this->_hotel;
  }
  
  public function hotel_facility()
  {
    if ( ! $this->_hotel_facility)
    {
      $this->_hotel_facility = Arr::overwrite($this->hotel_facility->as_array(), $this->values);
    }
    
    return $this->_hotel_facility;
  }
  
  public function hotel_service()
  {
    if ( ! $this->_hotel_service)
    {
      $this->_hotel_service = Arr::overwrite($this->hotel_service->as_array(), $this->values);
    }
    
    return $this->_hotel_service;
  }
  
  public function cities()
  {
    foreach ($this->cities as $city)
    {
      $city->selected = $city->id == Arr::get($this->values, 'city_id', $this->hotel->city_id);
    }
    
    return $this->cities;
  }
  
  public function districts()
  {
    foreach ($this->districts as $district)
    {
      $district->selected = $district->id == Arr::get($this->values, 'district_id', $this->hotel->district_id);
    }
    
    return $this->districts;
  }
  
  public function districts_lookup()
  {
    $district_lookup = array();
    
    foreach ($this->districts as $district)
    {
      $districts_lookup[$district->city_id][] = array(
        'id' => $district->id,
        'name' => $district->name,
      );
    }
    
    return json_encode($districts_lookup);
  }
  
  public function currencies()
  {
    foreach ($this->currencies as $currency)
    {
      $currency->selected = $currency->id == Arr::get($this->values, 'currency_id', $this->hotel->currency_id);
    }
    
    return $this->currencies;
  }
  
  public function stars()
  {
    $stars = array();
    $numbers = array(
      0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5
    );
    
    foreach ($numbers as $number)
    {
      $stars[] = array(
        'number' => $number,
        'selected' => $number == Arr::get($this->values, 'number', $this->hotel->star),
      );
    }
    
    return $stars;
  }
  
  public function hotel_types()
  {
    $hotel_types = array();
    
    foreach ($this->hotel_types as $hotel_type)
    {
      $hotel_types[] = $hotel_type->as_array() + array(
        'selected' => $hotel_type->id == Arr::get($this->values, 'hotel_type_id', $this->hotel->hotel_type_id)
      );
    }
    
    return $hotel_types;
  }
  
  public function child_ages()
  {
    $child_ages = array();
    $numbers = array(
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17
    );
    
    foreach ($numbers as $number)
    {
      $child_ages[] = array(
        'number' => $number,
        'selected_from' => $number == Arr::get($this->values, 'child_age_from', $this->hotel->child_age_from),
        'selected_until' => $number == Arr::get($this->values, 'child_age_until', $this->hotel->child_age_until),
      );
    }
    
    return $child_ages;
  }
  
  public function timezones()
  {
    $timezones = array();
    
    foreach ($this->timezones as $timezone)
    {
      $timezones[] = $timezone->as_array() + array(
        'selected' => $timezone->identifier == Arr::get($this->values, 'timezone', $this->hotel->timezone)
      );
    }
    
    return $timezones;
  }
  
  public function hotel_texts()
	{
		$hotel_texts = array();
		
		foreach ($this->languages as $language)
		{
      if ( ! $hotel_text = Arr::get($this->hotel_texts, $language->id))
			{
				$hotel_text = ORM::factory('hotel_text');
				$hotel_text->name = '';
			}
      
			$hotel_texts[] = array(
        'language' => $language,
				'name' => Arr::path($this->values, "names.{$language->id}", $hotel_text->name),
        'description' => Arr::path($this->values, "descriptions.{$language->id}", $hotel_text->description),
        'english' => $language->id == 1 ? 1 : 0,
			);
		}
		
		return $hotel_texts;
	}
  
}