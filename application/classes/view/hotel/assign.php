<?php defined('SYSPATH') or die('No direct script access.');

class View_Hotel_Assign extends Kostache {
  
  public function admins()
  {
    $admins = array();
		
		foreach ($this->admins as $admin)
		{
			$admins[] = $admin->as_array() + array(
        'selected' => $admin->id == Arr::get($this->values, 'admin_id')
			);
		}
		
		return $admins;
  }
  
  public function hotels()
  {
    $hotels = array();
		
		foreach ($this->hotels as $hotel)
		{
			$hotels[] = $hotel->as_array() + array(
        'selected' => $hotel->id == Arr::get($this->values, 'hotel_id')
			);
		}
		
		return $hotels;
  }
  
//	public function admin_hotels()
//	{
//		$admin_hotels = array();
//		
//		foreach ($this->admin_hotels as $admin_hotel)
//		{
//			$admin_hotels[] = array(
//				'form_admin_hotel_id' => Form::checkbox("ids[{$admin_hotel->id}]", $admin_hotel->id),
//				'admin_hotel_name' => $admin_hotel->name,
//				'admin_hotel_city_name' => $admin_hotel->city_name,
//				'admin_hotel_unassign_link' => Route::url('default', array('controller' => 'hotel', 'action' => 'unassign', 'id' => $admin_hotel->id)),
//			);
//		}
//		
//		return $admin_hotels;
//	}
	
}