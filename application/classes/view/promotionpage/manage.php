<?php defined('SYSPATH') or die('No direct script access.');

class View_PromotionPage_Manage extends Kostache {

  public function datas()
  {
    $datas = $this->datas;

    foreach ($datas['earlybirds'] as $datak => $data) {
      $campaign = $data['campaign'];
      $datas['earlybirds'][$datak]['campaign']['name'] = $this->promotionName($campaign);
      $datas['earlybirds'][$datak]['campaign']['expired'] = $this->getExpiredCampaign($campaign);
    }

    foreach ($datas['lastminutes'] as $datak => $data) {
      $campaign = $data['campaign'];
      $datas['lastminutes'][$datak]['campaign']['name'] = $this->promotionName($campaign);
      $datas['lastminutes'][$datak]['campaign']['expired'] = $this->getExpiredCampaign($campaign);
    }

    foreach ($datas['superdeal'] as $datak => $data) {
      
      $campaign = $data['campaign'];
      $datas['superdeal'][$datak]['campaign']['name'] = $this->promotionName($campaign);
      $datas['superdeal'][$datak]['campaign']['expired'] = $this->getExpiredCampaign($campaign);
    }    

    return $datas;
  }

  private function promotionName($campaign)
  {

    if ($campaign['discount_rate'] > 0)
    {
      $benefit = __(
        'Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', 
        array(
        ':minimum_number_of_nights' => $campaign['minimum_number_of_nights'],
        ':minimum_number_of_rooms' => $campaign['minimum_number_of_rooms'],
        ':discount_rate' => $campaign['discount_rate'],
        ));

    }
    elseif ($campaign['discount_amount'] > 0)
    {
      $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
        ':minimum_number_of_nights' => $campaign['minimum_number_of_nights'],
        ':minimum_number_of_rooms' => $campaign['minimum_number_of_rooms'],
        ':currency_code' => $this->currencycode,
        ':discount_amount' => $campaign['discount_amount'],
        ));
    }
    elseif ($campaign->number_of_free_nights)
    {
      $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
        ':minimum_number_of_nights' => $campaign['minimum_number_of_nights'],
        ':minimum_number_of_rooms' => $campaign['minimum_number_of_rooms'],
        ':number_of_free_nights' => $campaign['number_of_free_nights'],
        ));
    }
    
    if ($campaign['type'] == 2)
    {
      $benefit .= ' '.__('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign['days_in_advance']));
    }
    elseif ($campaign['type'] == 3)
    {
      $benefit .= ' '.__('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign['within_days_of_arrival']));
    }
    elseif ($campaign['type'] == 5)
    {
      $benefit .= ' '.__('Flash Deal.');
    }      

    return $benefit;
  }

  //call in method datas()
  //for know campaign is expired or not
  private function getExpiredCampaign($campaign)
  {
    // echo "<pre>";
    // print_r($campaign);
    // die();



    if(($campaign['booking_end_date'] < date('Y-m-d')) || ($campaign['stay_end_date'] < date('Y-m-d'))){
      return true;
    }
    else{
      return false;
    }
  }

}
