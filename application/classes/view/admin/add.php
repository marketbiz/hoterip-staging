<?php defined('SYSPATH') or die('No direct script access.');

class View_Admin_Add extends Kostache {
  
  public function roles()
  {
    $roles = array();
    
    foreach ($this->roles as $role)
    {
      $roles[] = Arr::merge($role->as_array(), array(
        'selected' => Arr::get($this->values, 'role') == $role->name,
      ));
    }
    
    return $roles;
  }
  
  public function timezones()
  {
    $timezones = array();
    
    foreach ($this->timezones as $timezone)
    {
      $timezones[] = Arr::merge($timezone->as_array(), array(
        'selected' => Arr::get($this->values, 'timezone') == $timezone->identifier,
      ));
    }
    
    return $timezones;
  }
	
}