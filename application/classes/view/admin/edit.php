<?php defined('SYSPATH') or die('No direct script access.');

class View_Admin_Edit extends Kostache {

  public function admin()
  {
    return Arr::overwrite($this->admin->as_array(), $this->values);
  }
  
  public function roles()
  {
    $roles = array();
    
    foreach ($this->roles as $role)
    {
      $roles[] = Arr::merge($role->as_array(), array(
        'selected' => Arr::get($this->values, 'role', $this->admin->role) == $role->value,
      ));
    }
    
    return $roles;
  }
  
  public function timezones()
  {
    $timezones = array();
    
    foreach ($this->timezones as $timezone)
    {
      $timezones[] = Arr::merge($timezone->as_array(), array(
        'selected' => Arr::get($this->values, 'timezone', $this->admin->timezone) == $timezone->identifier,
      ));
    }
    
    return $timezones;
  }
  
}