<?php defined('SYSPATH') or die('No direct script access.');

class View_Promotion_Add extends Kostache {
  
  public function rooms()
  {
    $rooms = array();
    
    foreach ($this->rooms as $room)
    {
      $rooms[] = Arr::merge($room->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "room_ids.$room->id"),
      ));
    }
    
    return $rooms;
  }
  
  public function languages()
  {
    $languages = array();
    
    foreach ($this->languages as $language)
    {
      $languages[] = Arr::merge($language->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "language_ids.$language->id"),
      ));
    }
    
    return $languages;
  }
  
  public function types()
  {
    $types = array();
    $names = array(
      1 => __('Standard Promotion'),
      2 => __('Early Bird'),
      3 => __('Last Minute'),
      4 => __('Non Refundable'),
      5 => __('Flash Deal (50% Discount)'),
    );
    
    foreach ($names as $number => $name)
    {
      $types[] = array(
        'number' => $number,
        'name' => $name,
        'selected' => Arr::get($this->values, 'type') == $number,
      );
    }
    
    return $types;
  }
  
  public function benefits()
  {
    $benefits = array();
    $names = array(
      0 => __('% Discount (Per Night)'),
      1 => __('Discount (Per Night)'),
      2 => __('Free Nights'),
    );
    
    foreach ($names as $number => $name)
    {
      $benefits[] = array(
        'number' => $number,
        'name' => $name,
        'selected' => Arr::get($this->values, 'benefit') == $number,
      );
    }
    
    return $benefits;
  }

  public function other_benefits()
  {
    $other_benefits = array();

    if(!empty($this->data_benefits))
    {
      foreach ($this->data_benefits as $number => $benefit_text)
      {
        $other_benefits_single[] = array(
          'number' => $benefit_text['name'],
          'name' => substr($benefit_text['name'], 0, 30),
          'id' => $benefit_text['benefit_id']
        );
      }
    }
    
    return $other_benefits;
  }

  public function other_benefits_live()
  {
    $other_benefits_live = array();

    foreach ($this->data_benefits as $number => $benefit_text)
    {
      $other_benefits_live[] = array(
        'number' => $benefit_text['name'],
        'name' => addslashes(substr($benefit_text['name'], 0, 30)),
        'id' => $benefit_text['benefit_id']
      );
    }
    
    return $other_benefits_live;
  }
  
  public function cancellation_types()
  {
    $cancellation_types = array();
    
    $names = array(
      0 => __('Default (Use Room Cancellation Policy)'),
      1 => __('Custom Cancellation Policy'),
    );
    
    foreach ($names as $number => $name)
    {
      $cancellation_types[] = array(
        'number' => $number,
        'name' => $name,
        'selected' => Arr::get($this->values, 'is_default_cancellation') == $number,
      );
    }
    
    return $cancellation_types;
  }
  
  public function cancellation_level_1_rules()
  {
    $cancellation_level_1_rules = array();
    
    foreach ($this->cancellation_level_1_rules as $cancellation_level_1_rule)
    {
      $cancellation_level_1_rules[] = Arr::merge($cancellation_level_1_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_1_cancellation_rule_id', 4) == $cancellation_level_1_rule->id,
      ));
    }
    
    return $cancellation_level_1_rules;
  }
  
  public function cancellation_level_2_rules()
  {
    $cancellation_level_2_rules = array();
    
    foreach ($this->cancellation_level_2_rules as $cancellation_level_2_rule)
    {
      $cancellation_level_2_rules[] = Arr::merge($cancellation_level_2_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_2_cancellation_rule_id') == $cancellation_level_2_rule->id,
      ));
    }
    
    return $cancellation_level_2_rules;
  }
  
  public function cancellation_level_3_rules()
  {
    $cancellation_level_3_rules = array();
    
    foreach ($this->cancellation_level_3_rules as $cancellation_level_3_rule)
    {
      $cancellation_level_3_rules[] = Arr::merge($cancellation_level_3_rule->as_array(), array(
        'selected' => Arr::path($this->values, 'level_3_cancellation_rule_id') == $cancellation_level_3_rule->id,
      ));
    }
    
    return $cancellation_level_3_rules;
  }
  
  public function status()
  {
      $status = array();
      $names = array(
          'c'=>__('Bisnis to Customer (B2C)'),
          'b'=>__('Bisnis to Bisnis (B2B)')
      );
      foreach ($names as $number => $name)
      {
          $status[] = array(
              'number' => $number,
              'name' => $name,
              'selected' => Arr::get($this->values, 'status') == $number
          );
      }
      return $status;
  }
  
  public function values()
  {
    return Arr::merge($this->values, array(
      'days_in_advance' => Arr::get($this->values, 'days_in_advance', 30),
      'within_days_of_arrival' => Arr::get($this->values, 'within_days_of_arrival', 3),
      'booking_start_date' => Arr::get($this->values, 'booking_start_date', date('Y-m-d')),
      'booking_end_date' => Arr::get($this->values, 'booking_end_date', date('Y-m-d', strtotime('+1 month'))),
      'stay_start_date' => Arr::get($this->values, 'stay_start_date', date('Y-m-d')),
      'stay_end_date' => Arr::get($this->values, 'stay_end_date', date('Y-m-d', strtotime('+1 month'))),
      'is_stay_sunday_active' => Arr::get($this->values, 'is_stay_sunday_active', 1),
      'is_stay_monday_active' => Arr::get($this->values, 'is_stay_monday_active', 1),
      'is_stay_tuesday_active' => Arr::get($this->values, 'is_stay_tuesday_active', 1),
      'is_stay_wednesday_active' => Arr::get($this->values, 'is_stay_wednesday_active', 1),
      'is_stay_thursday_active' => Arr::get($this->values, 'is_stay_thursday_active', 1),
      'is_stay_friday_active' => Arr::get($this->values, 'is_stay_friday_active', 1),
      'is_stay_saturday_active' => Arr::get($this->values, 'is_stay_saturday_active', 1),
      'minimum_number_of_nights' => Arr::get($this->values, 'minimum_number_of_nights', 1),
      'minimum_number_of_rooms' => Arr::get($this->values, 'minimum_number_of_rooms', 1),
      'discount_rate' => Arr::get($this->values, 'discount_rate', 20),
      'discount_amount' => Arr::get($this->values, 'discount_amount', 0),
      'number_of_free_nights' => Arr::get($this->values, 'number_of_free_nights', 1),
    ));
  }
  
}