<?php defined('SYSPATH') or die('No direct script access.');

class View_Promotion_Manage extends Kostache {
  
  public function campaigns()
  {
    $campaigns = array();
    foreach ($this->campaigns as $campaign)
    {
      $benefit = '';

      // Additional Benefit promotion name
      $room_names = $campaign->rooms
        ->select(array('room_texts.name', 'name'))
        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
        ->where('room_texts.language_id', '=', $this->selected_language->id)
        ->find_all()
        ->as_array('id', 'name');
      
      if ($campaign->discount_rate > 0)
      {
        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':discount_rate' => $campaign->discount_rate,
        ));
      }
      elseif ($campaign->discount_amount > 0)
      {
        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':currency_code' => $this->currency->code,
          ':discount_amount' => $campaign->discount_amount,
        ));
      }
      elseif ($campaign->number_of_free_nights)
      {
        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':number_of_free_nights' => $campaign->number_of_free_nights,
        ));
      }
      else
      {
        $other_benefits = DB::select(
            'campaigns_benefits.*',
            array('benefit_texts.name', 'name')
          )
          ->from('campaigns_benefits')
          ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
          ->where('benefit_texts.language_id', '=', $this->selected_language->id)
          ->where('campaigns_benefits.campaign_id', '=', $campaign->id)
          ->execute();
        
        if(count($other_benefits))
        {
          $room_campaigns = count($this->rooms) == count($room_names) ? __('All Rooms') : implode(', ', $room_names);

          $benefit = $room_campaigns;
          $benefit .= __(' get ');

          foreach ($other_benefits as $count =>$other_benefit)
          {
            $benefit .= $other_benefit['name'];
            if($other_benefit['value'])
            {
              $benefit .= ' for '.$other_benefit['value'];
            }

            $benefit .=  count($other_benefits) == ($count + 1) ? '.' : ', ';
          }
        }
      }
      
      if ($campaign->type == 2)
      {
        $benefit .= ' '.__('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
      }
      elseif ($campaign->type == 3)
      {
        $benefit .= ' '.__('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
      }
      elseif ($campaign->type == 4)
      {
        $benefit .= ' '.__('Non refundable.');
      }
      elseif ($campaign->type == 5)
      {
        $benefit .= ' '.__('Flash Deal.');
      }
      
      if($campaign->status == 'b')
      {
          $status = 'B2B';
      }
      else
      {
          $status = 'B2C';
      }
      
      $room_names = $campaign->rooms
        ->select(array('room_texts.name', 'name'))
        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
        ->where('room_texts.language_id', '=', $this->selected_language->id)
        ->find_all()
        ->as_array('id', 'name');
      
      // Is hoterip campaign exist
      $hoterip_campaign = $campaign->rooms
        ->where('rooms.hoterip_campaign', '=', 1)
        ->count_all() > 0;

      $cancellations = $campaign->cancellations
        ->select(
          array('rule_1.name', 'rule_1_name'),
          array('rule_2.name', 'rule_2_name'),
          array('rule_3.name', 'rule_3_name')
        )
        ->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'level_1_cancellation_rule_id')
        ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'level_2_cancellation_rule_id')
        ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'level_3_cancellation_rule_id')
        ->find_all();
      
      // Check expired campaign
      $is_expired = FALSE;
      $is_expired = date('Y-m-d') > $campaign->booking_end_date ? TRUE : FALSE;
      
      $is_status = ($campaign->status == 'b') ? TRUE : FALSE;
      
      $campaigns[] = Arr::merge($campaign->as_array(), array(
        'benefit' => $benefit,
        'room_names' => count($this->rooms) == count($room_names) ? __('All Rooms') : implode(', ', $room_names),
        'booking_start_date' => date(__('M j, Y'), strtotime($campaign->booking_start_date)),
        'booking_end_date' => date(__('M j, Y'), strtotime($campaign->booking_end_date)),
        'stay_start_date' => date(__('M j, Y'), strtotime($campaign->stay_start_date)),
        'stay_end_date' => date(__('M j, Y'), strtotime($campaign->stay_end_date)),
        'cancellations' => $cancellations,
        'show_cancellation' => count($cancellations) > 0,
        'is_expired' => $is_expired,
        'hoterip_campaign' => $hoterip_campaign,
        'status' => $status,
        'is_status' => $is_status,
      ));
    }
    
    return $campaigns;
  }
	
}
