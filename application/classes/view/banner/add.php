<?php defined('SYSPATH') or die('No direct script access.');

class View_Banner_Add extends Kostache {
	
  public function languages()
  {
    $languages = array();
    foreach ($this->languages as $language)
    {
      $languages[] = Arr::merge($language->as_array(), array(
        'selected' => Arr::get($this->values, 'language_id') == $language->id,
      ));
    }
    
    return $languages;
  }
	
}