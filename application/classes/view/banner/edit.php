<?php defined('SYSPATH') or die('No direct script access.');

class View_Banner_Edit extends Kostache {
  
  public $_banner;
  
  public function banner()
  {
    if ( ! $this->_banner)
    {
      $this->_banner = Arr::overwrite($this->banner->as_array(), $this->values);
    }
    
    return $this->_banner;
  }
  
  public function languages()
  {
    $languages = array();
    foreach ($this->languages as $language)
    {
      $languages[] = Arr::merge($language->as_array(), array(
        'selected' => Arr::get($this->values, 'language_id', $this->banner->language_id) == $language->id,
      ));
    }
    
    return $languages;
  }
	
}