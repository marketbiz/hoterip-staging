<?php defined('SYSPATH') or die('No direct script access.');

class View_Booking_Manage extends Kostache {
  
  public function bookings()
	{
		$bookings = array();
      
		foreach ($this->bookings as $booking)
		{
      /**
       * Rooms
       */
      $booking_rooms = ORM::factory('booking_room')
        ->where('booking_rooms.booking_id', '=', $booking->id)
        ->find_all();
      
      $rooms = array();
      foreach ($booking_rooms as $key => $booking_room)
      {
        $rooms[] = Arr::merge($booking_room->as_array(), array(
          'number' => $key + 1,
          ));
      }
      
      /**
       * Conversation
       */
      $booking_conversations = ORM::factory('booking_conversation')
        ->select(array('admins.name', 'admin_name'))
        ->join('admins', 'left')->on('admins.id', '=', 'booking_conversations.admin_id')
        ->where('booking_conversations.booking_id', '=', $booking->id)
        ->order_by('booking_conversations.created', 'ASC')
        ->find_all();
      
      $conversations = array();
      foreach ($booking_conversations as $booking_conversation)
      {
        $conversations[] = Arr::merge($booking_conversation->as_array(), array(
          'content' => nl2br($booking_conversation->content),
          'datetime' => date(__('M j, Y H:i'), $booking_conversation->created + Date::offset($this->logged_in_admin->timezone)),
          ));
      }
      
      $conversation_unread = DB::select(
          array(DB::expr('COUNT(*)'), 'count')
        )
        ->from('booking_conversation_notifications')
        ->where('booking_id', '=', $booking->id)
        ->where('admin_id', '=', $this->logged_in_admin->id)
        ->execute()
        ->get('count') > 0;
      
      /**
       * Comments
       */
      $booking_comments = ORM::factory('booking_comment')
        ->select(array('admins.name', 'admin_name'))
        ->join('admins')->on('admins.id', '=', 'booking_comments.admin_id')
        ->where('booking_comments.booking_id', '=', $booking->id)
        ->order_by('booking_comments.created', 'ASC')
        ->find_all();
      
      $comments = array();
      foreach ($booking_comments as $booking_comment)
      {
        $comments[] = Arr::merge($booking_comment->as_array(), array(
          'content' => nl2br($booking_comment->content),
          'datetime' => date(__('M j, Y H:i'), $booking_comment->created + Date::offset($this->logged_in_admin->timezone)),
          ));
      }
      
      $comment_unread = DB::select(
          array(DB::expr('COUNT(*)'), 'count')
        )
        ->from('booking_comment_notifications')
        ->where('booking_id', '=', $booking->id)
        ->where('admin_id', '=', $this->logged_in_admin->id)
        ->execute()
        ->get('count') > 0;
      
      /**
       * Bookings
       */

      //set title 
      if($booking->title_id==0){
        $title_guest = __('Ms');
      }
      elseif ($booking->title_id==1){
        $title_guest = __('Mr');
      }
      else{
        $title_guest = __('Mr/Ms');
      }

      $bookings[] = Arr::merge($booking->as_array(), array(
        'locked' => $booking->locker_id > 0,
        'title' => $title_guest,
        'check_in_date' => date(__('M j, Y'), strtotime($booking->check_in_date)),
        'check_out_date' => date(__('M j, Y'), strtotime($booking->check_out_date)),
        'booking_date' => date(__('M j, Y'), $booking->created + Date::offset($this->logged_in_admin->timezone)),
        'rooms' => $rooms,
        'has_special_request' => $booking->is_non_smoking_room_request OR 
          $booking->is_late_check_in_request OR
          $booking->is_early_check_in_request	OR
          $booking->is_high_floor_request OR
          $booking->is_large_bed_request OR
          $booking->is_twin_beds_request OR
          $booking->is_airport_transfer_request OR
          $booking->request_note,
        'request_note' => nl2br($booking->request_note),
        'conversations' => $conversations,
        'conversation_unread' => $conversation_unread,
        'number_of_comments' => count($comments),
        'comments' => $comments,
        'comment_unread' => $comment_unread,
      ));
		}
    
    /// Add deleted room data
    $bookings_values = array();

    foreach ($this->bookings_values as $bookings_value)
    {
      // Check room from deleted room is already restored
      $rooms = DB::select(
        array(DB::expr('COUNT(room_texts.id)'), 'count')
      )
      ->from('room_texts')
      ->where('room_texts.room_id', '!=', $bookings_value->room_id)
      ->where('room_texts.name', '=', $bookings_value->room_name)
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->execute();
      if(count($rooms)){continue;}
      
      /**
       * Rooms
       */
      $booking_rooms = ORM::factory('booking_room')
        ->where('booking_rooms.booking_id', '=', $bookings_value->id)
        ->find_all();

      $rooms = array();
      foreach ($booking_rooms as $key => $booking_room)
      {
        $rooms[] = Arr::merge($booking_room->as_array(), array(
          'number' => $key + 1,
          ));
      }
      
      /**
       * Conversation
       */
      $booking_conversations = ORM::factory('booking_conversation')
        ->select(array('admins.name', 'admin_name'))
        ->join('admins', 'left')->on('admins.id', '=', 'booking_conversations.admin_id')
        ->where('booking_conversations.booking_id', '=', $bookings_value->id)
        ->order_by('booking_conversations.created', 'ASC')
        ->find_all();
      
      $conversations = array();
      foreach ($booking_conversations as $booking_conversation)
      {
        $conversations[] = Arr::merge($booking_conversation->as_array(), array(
          'content' => nl2br($booking_conversation->content),
          'datetime' => date(__('M j, Y H:i'), $booking_conversation->created + Date::offset($this->logged_in_admin->timezone)),
          ));
      }
      
      $conversation_unread = DB::select(
          array(DB::expr('COUNT(*)'), 'count')
        )
        ->from('booking_conversation_notifications')
        ->where('booking_id', '=', $bookings_value->id)
        ->where('admin_id', '=', $this->logged_in_admin->id)
        ->execute()
        ->get('count') > 0;
      
      /**
       * Comments
       */
      $booking_comments = ORM::factory('booking_comment')
        ->select(array('admins.name', 'admin_name'))
        ->join('admins')->on('admins.id', '=', 'booking_comments.admin_id')
        ->where('booking_comments.booking_id', '=', $bookings_value->id)
        ->order_by('booking_comments.created', 'ASC')
        ->find_all();
      
      $comments = array();
      foreach ($booking_comments as $booking_comment)
      {
        $comments[] = Arr::merge($booking_comment->as_array(), array(
          'content' => nl2br($booking_comment->content),
          'datetime' => date(__('M j, Y H:i'), $booking_comment->created + Date::offset($this->logged_in_admin->timezone)),
          ));
      }
      
      $comment_unread = DB::select(
          array(DB::expr('COUNT(*)'), 'count')
        )
        ->from('booking_comment_notifications')
        ->where('booking_id', '=', $bookings_value->id)
        ->where('admin_id', '=', $this->logged_in_admin->id)
        ->execute()
        ->get('count') > 0;
      
      /**
       * Bookings
       */
      $bookings_values[] = Arr::merge($bookings_value->as_array(), array(
        'locked' => $bookings_value->locker_id > 0,
        'title' => $bookings_value->title_id ? __('Mr') : __('Ms'),
        'check_in_date' => date(__('M j, Y'), strtotime($bookings_value->check_in_date)),
        'check_out_date' => date(__('M j, Y'), strtotime($bookings_value->check_out_date)),
        'booking_date' => date(__('M j, Y'), $bookings_value->created + Date::offset($this->logged_in_admin->timezone)),
        'rooms' => $rooms,
        'has_special_request' => $bookings_value->is_non_smoking_room_request OR 
          $bookings_value->is_late_check_in_request OR
          $bookings_value->is_early_check_in_request OR
          $bookings_value->is_high_floor_request OR
          $bookings_value->is_large_bed_request OR
          $bookings_value->is_twin_beds_request OR
          $bookings_value->is_airport_transfer_request OR
          $bookings_value->request_note,
        'request_note' => nl2br($bookings_value->request_note),
        'conversations' => $conversations,
        'conversation_unread' => $conversation_unread,
        'number_of_comments' => count($comments),
        'comments' => $comments,
        'comment_unread' => $comment_unread,
      ));
    }

    // Combine Bookings
    $bookings = array_merge($bookings, $bookings_values);

    // Sort Booking
    array_multisort($bookings, SORT_DESC, $bookings);

    return $bookings;
  }

}