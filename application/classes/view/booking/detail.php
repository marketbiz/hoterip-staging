<?php defined('SYSPATH') or die('No direct script access.');

class View_Booking_Detail extends Kostache {
  
  public $_booking;
  
  public function booking()
  {
    if ( ! $this->_booking)
    {
      // Get hotel data
      $hotel = unserialize($this->booking->hotel);
      
      // Get hotel currency
      $hotel_currency = ORM::factory('currency')
        ->where('id', '=', $hotel->currency_id)
        ->find();
      
      // Get hotel texts data
      $hotel_texts = unserialize($this->booking->hotel_texts);
      
      if ( ! $hotel_text = Arr::get($hotel_texts, $this->selected_language->id))
      {
        // Use english text
        $hotel_text = Arr::get($hotel_texts, 1);
      }
      
      // Get room data
      $room = unserialize($this->booking->room);
      
      // Get room texts data
      $room_texts = unserialize($this->booking->room_texts);
      
      if ( ! $room_text = Arr::get($room_texts, $this->selected_language->id))
      {
        // Use english text
        $room_text = Arr::get($room_texts, 1);
      }
      
      // Get campaign data
      $campaign = unserialize($this->booking->campaign);
      
      $promotion_name = '';
      if ($campaign->discount_rate > 0)
      {
        $promotion_name = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':discount_rate' => $campaign->discount_rate,
        ));
      }
      elseif ($campaign->discount_amount > 0)
      {
        $promotion_name = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':currency_code' => $hotel_currency->code,
          ':discount_amount' => $campaign->discount_amount,
        ));
      }
      elseif ($campaign->number_of_free_nights)
      {
        $promotion_name = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
          ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
          ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
          ':number_of_free_nights' => $campaign->number_of_free_nights,
        ));
      }
      
      if ($campaign->type == 2)
      {
        $promotion_name .= ' '.__('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
      }
      elseif ($campaign->type == 3)
      {
        $promotion_name .= ' '.__('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
      }
      elseif ($campaign->type == 4)
      {
        $promotion_name .= ' '.__('Non refundable.');
      }
      elseif ($campaign->type == 5)
      {
        $promotion_name .= ' '.__('Flash Deal.');
      }
      
      // Get items data
      $items = unserialize($this->booking->items);
      
      // Get cancellation data
      $cancellation = unserialize($this->booking->cancellation);
      
      $level_1_cancellation_rule = ORM::factory('cancellation_rule')
        ->where('id', '=', $cancellation->level_1_cancellation_rule_id)
        ->find();
      
      $level_2_cancellation_rule = ORM::factory('cancellation_rule')
        ->where('id', '=', $cancellation->level_2_cancellation_rule_id)
        ->find();
      
      $level_3_cancellation_rule = ORM::factory('cancellation_rule')
        ->where('id', '=', $cancellation->level_3_cancellation_rule_id)
        ->find();
      
      // If booking is canceled
      if ($this->booking->is_canceled)
      {
        $cancellation_timestamp  = $this->booking->canceled_timestamp + Date::offset($hotel->timezone);
        $check_in_date_timestamp = strtotime($this->booking->check_in_date);

        // Get the within days
        $within_days = (int) ceil(($check_in_date_timestamp - $cancellation_timestamp) / Date::DAY);
        
        // Check cancellation rules
        if ($within_days <= $level_1_cancellation_rule->within_days)
        {
          $applied_cancellation_policy = __($level_1_cancellation_rule->name);
        }
        elseif ($within_days <= $level_2_cancellation_rule->within_days)
        {
          $applied_cancellation_policy = __($level_2_cancellation_rule->name);
        }
        elseif ($within_days <= $level_3_cancellation_rule->within_days)
        {
          $applied_cancellation_policy = __($level_3_cancellation_rule->name);
        }
        else
        {
          $applied_cancellation_policy = __('No Charge');
        }
      }
      else
      {
        $applied_cancellation_policy = '';
      }
      
      // Get items data
//      $booking_items = unserialize($this->booking->items);
      
      $prices = Model_Booking::calculate_prices($this->booking->id);
      
      foreach ($prices['rooms'] as & $price_room)
      {
        foreach ($price_room['items'] as & $item)
        {
          $item['date'] = date(__('M j, Y'), strtotime($item['date']));
          $item['hotel_revenue'] = Num::format_decimal($item['hotel_revenue']);
          $item['price'] = Num::format_decimal($item['price']);
          $item['extrabed_price'] = Num::format_decimal($item['extrabed_price']);
          $item['surcharge_price'] = Num::format_decimal($item['surcharge_price']);
          $item['subtotal_hotel_revenues'] = Num::format_decimal($item['subtotal_hotel_revenues']);
        }
      }
      
      $hotel_to_guest_exchange = unserialize($this->booking->hotel_to_guest_exchange);
      $hotel_to_transaction_exchange = unserialize($this->booking->hotel_to_transaction_exchange);

      $points = $this->booking->number_of_points_used;

      // Get selected hotel currency
      if($hotel_to_guest_exchange->from_currency_id != 1){
        $exchanges = DB::select()
          ->from('exchanges')
          ->where('from_currency_id', '=', 1)
          ->where('to_currency_id', '=', $hotel_to_guest_exchange->to_currency_id)
          ->as_object()
          ->execute()
          ->current();
        $points = $this->booking->number_of_points_used / $prices['point_to_hotel_exchange']->amount * $prices['point_to_hotel_exchange']->rate;
      }

      $netselling = $prices['amount_paid_by_guest'] + round($points) + $prices['bni_discount'];
      $amount_paid_by_guest_transaction = $prices['amount_paid_by_guest'] / $hotel_to_transaction_exchange->amount * $hotel_to_transaction_exchange->rate;

      /// bni discount
      $prices['bni_discount_percentage'] = (($prices['bni_discount']  / $prices['total_invoice']) * 100) > 6 ? 
      Num::format_decimal((($prices['bni_discount']  / $prices['total_invoice'])) * 100) : 5;
      $prices['bni_discount'] = Num::format_decimal($prices['bni_discount']);

      $prices['exchange_rate_exist'] = $hotel_to_transaction_exchange->to_currency_id == $hotel_currency->id ? FALSE: TRUE;
      $prices['exchange_rate'] = Num::format_decimal(round($hotel_to_guest_exchange->rate, 2));
      $prices['netselling'] = Num::format_decimal($netselling);
      $prices['hotel_revenue'] = Num::format_decimal($prices['hotel_revenue']);
      $prices['ota_revenue'] = Num::format_decimal($prices['ota_revenue']);
      $prices['amount_paid_by_guest_guest'] = Num::format_decimal(round($prices['amount_paid_by_guest'] / $hotel_to_guest_exchange->amount * $hotel_to_guest_exchange->rate, 2));
      $prices['amount_paid_by_guest'] = Num::format_decimal(round($prices['amount_paid_by_guest'],2));
      $prices['amount_paid_by_guest_transaction'] = Num::format_decimal(round($amount_paid_by_guest_transaction, 2));
      $prices['amount_money_refunded_to_guest'] = Num::format_decimal($prices['amount_money_refunded_to_guest']);
      
      $additional_benefits = unserialize($this->booking->additional_benefit);
      $additional_benefit_text = '';
      $is_additional_benefit = FALSE;

      if(count($additional_benefits) > 1)
      {
        foreach ($additional_benefits as $key => $additional_benefit)
        {
          $for_text = !$additional_benefit['value'] ? '':' for '.$additional_benefit['value']; 
          $additional_benefit_text .= $additional_benefit['name'].$for_text.', ';
        }
        $is_additional_benefit = TRUE;
        $additional_benefit_text = rtrim($additional_benefit_text, ', ').'. ';
      }
      
      $agent = $this->booking->as_array();
      
      $is_agent = DB::select()->from('users')->where('role','=','agent')->where('email','=',$agent['user_email'])->execute()->as_array();

      $this->_booking = Arr::merge($this->booking->as_array(), $prices + array(
        'hotel' => $hotel,
        'room' => $room,
        'campaign' => $campaign,
        'items' => $items,
        'cancellation' => $cancellation,
        'hotel_currency' => $hotel_currency,
        'hotel_name' => $hotel_text->name,
        'room_name' => $room_text->name,
        'promotion_name' => $promotion_name,
        'b2b' => (isset($is_agent) && !empty($is_agent)) ? 1 : 0, 
        'admin_b2b' => (($this->logged_in_admin->role == 'admin' && isset($agent['agent_id']) && !empty($agent['agent_id'])) || empty($agent['agent_id']) ) ? 0 : 1,  
        'additional_benefit' => $additional_benefit_text,
        'is_additional_benefit' => $is_additional_benefit,
        'guest_currency' => unserialize($this->booking->guest_currency),
        'transaction_currency' => unserialize($this->booking->transaction_currency),
        'booking_date' => date(__('M j, Y'), $this->booking->created + Date::offset($this->logged_in_admin->timezone)),
        'canceled_time' => date(__('M j, Y H:i'), $this->booking->canceled_timestamp + Date::offset($hotel->timezone)),
        'check_in_date' => date(__('M j, Y'), strtotime($this->booking->check_in_date)),
        'check_out_date' => date(__('M j, Y'), strtotime($this->booking->check_out_date)),
        'level_1_cancellation_rule_name' => $level_1_cancellation_rule->name,
        'level_2_cancellation_rule_name' => $level_2_cancellation_rule->name,
        'level_3_cancellation_rule_name' => $level_3_cancellation_rule->name,
        'has_special_request' => $this->booking->is_non_smoking_room_request OR 
          $this->booking->is_late_check_in_request OR
          $this->booking->is_early_check_in_request OR
          $this->booking->is_high_floor_request OR
          $this->booking->is_large_bed_request OR
          $this->booking->is_twin_beds_request OR
          $this->booking->is_airport_transfer_request OR
          $this->booking->request_note,
        'applied_cancellation_policy' => $applied_cancellation_policy,
      ));
    }
    
    return $this->_booking;
  }
  
}