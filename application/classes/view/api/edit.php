<?php defined('SYSPATH') or die('No direct script access.');

class View_Api_Edit extends Kostache {

  public function timezones()
  {
    $timezones = array();

    foreach ($this->timezones as $timezone)
    {
      $timezones[] = Arr::merge($timezone->as_array(), array(
        'selected' => 
        Arr::get($this->values, 'timezone', $this->api_access['timezone']) == $timezone->identifier,
      ));
    }
    
    return $timezones;
  }

}