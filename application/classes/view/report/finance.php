<?php defined('SYSPATH') or die('No direct script access.');

class View_Report_Finance extends Kostache {

  public $months;
  public $total_hotel_revenues = 0;
  public $total_ota_revenues = 0;
  
  public function months()
  {
    $this->months = Date::months(Date::MONTHS_LONG);
    
    $months = array();
    foreach ($this->months as $index => $name)
    {
      $months[] = array(
        'index' => $index,
        'name' => $name,
        'selected' => Arr::get($this->filters, 'month', date('n')) == $index,
      );
    }
    return $months;
  }
  
  public function bookings()
  {
    $bookings = array();
    $bookings_values = array();

    $total_hotel_revenues = 0;
    $total_ota_revenues = 0;
    $total_hotel_booking = 0;
    $total_ota_booking = 0;
    $total_hotel_values = 0;
    $total_ota_values = 0;

    foreach ($this->bookings as $booking)
    {
      $prices = Model_Booking::calculate_prices($booking->id);
      
      $bookings[] = Arr::merge($booking->as_array(), array(
        'booking_date' => date(__('M j, Y'), $booking->created + Date::offset($this->selected_hotel->timezone)),
        'check_in_date' => date(__('M j, Y'), strtotime($booking->check_in_date)),
        'check_out_date' => date(__('M j, Y'), strtotime($booking->check_out_date)),
        'hotel_revenue' => Num::display($prices['hotel_revenue']),
        'ota_revenue' => Num::display($prices['ota_revenue']),
      ));
      
      $total_hotel_booking += $prices['hotel_revenue'];
      $total_ota_booking += $prices['ota_revenue'];
    }

    /// Manage Booking
    foreach ($this->bookings_values as $bookings_value)
    {
      // Check room from deleted room is already restored
      $rooms = DB::select(
        array(DB::expr('COUNT(room_texts.id)'), 'count')
      )
      ->from('room_texts')
      ->where('room_texts.room_id', '!=', $bookings_value->room_id)
      ->where('room_texts.name', '=', $bookings_value->room_name)
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->execute();
      if(count($rooms)){continue;}
      
      $prices = Model_Booking::calculate_prices($bookings_value->id);
      
      $bookings_values[] = Arr::merge($bookings_value->as_array(), array(
        'booking_date' => date(__('M j, Y'), $bookings_value->created + Date::offset($this->selected_hotel->timezone)),
        'check_in_date' => date(__('M j, Y'), strtotime($bookings_value->check_in_date)),
        'check_out_date' => date(__('M j, Y'), strtotime($bookings_value->check_out_date)),
        'hotel_revenue' => Num::display($prices['hotel_revenue']),
        'ota_revenue' => Num::display($prices['ota_revenue']),
      ));
      
      $total_hotel_values += $prices['hotel_revenue'];
      $total_ota_values += $prices['ota_revenue'];
    }

    $bookings = array_merge($bookings, $bookings_values);
    $this->total_hotel_revenues = Num::display($total_hotel_booking + $total_hotel_values);
    $this->total_ota_revenues = Num::display($total_ota_booking + $total_ota_values);
      
    return $bookings;
  }
}