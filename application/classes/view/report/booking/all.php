<?php defined('SYSPATH') or die('No direct script access.');

class View_Report_Booking_All extends Kostache {
  
  public function hotels()
  {
    $hotels = array();
    
    foreach ($this->hotels as $hotel)
    {
      $hotels[] = Arr::merge($hotel->as_array(), array(
        'selected' => Arr::get($this->filters, 'hotel_id', 0) == $hotel->id,
      ));
    }
    
    return $hotels;
  }
  
  public function bookings()
  {
    $bookings = array();

    foreach ($this->bookings as $booking)
    {
      $number_of_nights = Date::span(strtotime($booking->check_in_date), strtotime($booking->check_out_date), 'days');
      
      $agent = DB::select()->from('users')->where('role','=','agent')->where('id','=',$booking->user_id)->execute()->as_array();

      $bookings[$booking->id] = Arr::merge($booking->as_array(), array(
        'title' => $booking->title_id ? __('Mr') : __('Ms'),
        'name_booking' => (isset($agent) && !empty($agent)) ? 'B' : '',
        'check_in_date' => date(__('M j, Y'), strtotime($booking->check_in_date)),
        'check_out_date' => date(__('M j, Y'), strtotime($booking->check_out_date)),
        'number_of_nights' => $number_of_nights,
        'booking_date' => date(__('M j, Y'), $booking->created + Date::offset($this->logged_in_admin->timezone)),
      ));
    }

    // Deleted Room data
    $bookings_values = array();

    foreach ($this->bookings_values as $bookings_value)
    {
      $number_of_nights = Date::span(strtotime($bookings_value->check_in_date), strtotime($bookings_value->check_out_date), 'days');
      
      $agent = DB::select()->from('users')->where('role','=','agent')->where('id','=',$bookings_value->user_id)->execute()->as_array();

      $bookings_values[$bookings_value->id] = Arr::merge($bookings_value->as_array(), array(
        'title' => $bookings_value->title_id ? __('Mr') : __('Ms'),
        'name_booking' => (isset($agent) && !empty($agent)) ? 'B' : '',
        'check_in_date' => date(__('M j, Y'), strtotime($bookings_value->check_in_date)),
        'check_out_date' => date(__('M j, Y'), strtotime($bookings_value->check_out_date)),
        'number_of_nights' => $number_of_nights,
        'booking_date' => date(__('M j, Y'), $bookings_value->created + Date::offset($this->logged_in_admin->timezone)),
      ));
    }

    // Combine Bookings
    $bookings = array_merge($bookings, $bookings_values);

    // Sort Booking
    array_multisort($bookings, SORT_DESC, $bookings);

    return $bookings;
  }

  public function total_bookings()
  {

    // Declare variable
    $total_number_of_bookings = 0;
    $total_number_of_rooms = 0;
    $total_number_of_nights = 0;
    $total_number_of_room_nights = 0;
    $total_number_of_adults = 0;
    $total_number_of_children = 0;
    $deleted_total_number_of_bookings = 0;
    $deleted_total_number_of_rooms = 0;
    $deleted_total_number_of_nights = 0;
    $deleted_total_number_of_room_nights = 0;
    $deleted_total_number_of_adults = 0;
    $deleted_total_number_of_children = 0;

    $total = array();

    foreach ($this->total_bookings as $booking)
    {
      $number_of_nights = Date::span(strtotime($booking->check_in_date), strtotime($booking->check_out_date), 'days');
     
      $total_number_of_bookings = count($this->total_bookings);
      $total_number_of_rooms += $booking->number_of_rooms;
      $total_number_of_nights += $number_of_nights;
      $total_number_of_room_nights += $booking->number_of_rooms * $number_of_nights;
      $total_number_of_adults += $booking->number_of_adults;
      $total_number_of_children += $booking->number_of_children;
    }

    // Count Total bookings
    foreach ($this->bookings_values as $bookings_value)
    {
      $number_of_nights = Date::span(strtotime($bookings_value->check_in_date), strtotime($bookings_value->check_out_date), 'days');
     
      $deleted_total_number_of_bookings = count($this->bookings_values);
      $deleted_total_number_of_rooms += $bookings_value->number_of_rooms;
      $deleted_total_number_of_nights += $number_of_nights;
      $deleted_total_number_of_room_nights += $bookings_value->number_of_rooms * $number_of_nights;
      $deleted_total_number_of_adults += $bookings_value->number_of_adults;
      $deleted_total_number_of_children += $bookings_value->number_of_children;
    }

    $this->total_number_of_bookings = $total_number_of_bookings + $deleted_total_number_of_bookings;
    $this->total_number_of_rooms = $total_number_of_rooms + $deleted_total_number_of_rooms;
    $this->total_number_of_nights = $total_number_of_nights + $deleted_total_number_of_nights;
    $this->total_number_of_room_nights = $total_number_of_room_nights + $deleted_total_number_of_room_nights;
    $this->total_number_of_adults = $total_number_of_adults + $deleted_total_number_of_adults;
    $this->total_number_of_children = $total_number_of_children + $deleted_total_number_of_children;

  }

  public function roles()
  {
    $role_data = array(
      '' => __('All Roles'),
      'admin' => __('Admin'),
      'user' => __('User'),
      'agent' => __('Agent')  
      );

    foreach ($role_data as $value => $role)
    {
      $roles[] = array(
        'value' => $value,
        'name' => $role,
        'selected' => Arr::get($this->filters, 'roles', '') == $value,
      );
    }

    return $roles;
  }
}