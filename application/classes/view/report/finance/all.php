<?php defined('SYSPATH') or die('No direct script access.');

class View_Report_Finance_All extends Kostache {

  public function hotels()
  {
    $hotels = array();
    
    foreach ($this->hotels as $hotel)
    {
      $hotels[] = Arr::merge($hotel->as_array(), array(
        'selected' => Arr::get($this->filters, 'hotel_id', 0) == $hotel->id,
      ));
    }
    
    return $hotels;
  }

  public function tracking_type()
  {
      //tracking_id
      $tracking_type = array(
        (object) array('value' => '0', 'name'  => 'Finnet (Old)'),
        (object) array('value' => '1', 'id' => 'finnet_order_id', 'name'  => 'Veritrans (Order ID)'),
        (object) array('value' => '2', 'id' => 'wego_tracking_id', 'name'  => 'Wego (Transaction ID)'),
      );
      $default = 0;

    foreach ($tracking_type as $track_type)
    {
      if ($track_type->value == Arr::get($_POST, 'tracking_type', $default))
      {
        $track_type->checked = 'checked';
      }
    }

    return $tracking_type;
  }
  
  public function filtered_hotels()
  {
    $filtered_hotels = array();
    
    foreach ($this->filtered_hotels as $filtered_hotel)
    {
      $total_hotel_revenues = 0;
      $total_ota_revenues = 0;
      $total_amount_paid_by_guest = 0;
      $total_amount_money_refunded_to_guest = 0;
      $total_amount_point_refunded_to_guest = 0;
      $total_number_of_points_used = 0;
      $total_reward_points = 0;
      $total_benefit_cost = 0;
      
      foreach ($filtered_hotel->bookings as $booking)
      {
        $prices = Model_Booking::calculate_prices($booking->id);
        
        $hotel_to_guest_exchange = unserialize($booking->hotel_to_guest_exchange);
        $hotel_to_transaction_exchange = unserialize($booking->hotel_to_transaction_exchange);
        
        $guest_currency = unserialize($booking->guest_currency);
        $transaction_currency = unserialize($booking->transaction_currency);
        
        // Benefit Cost
        $benefit_cost = $booking->benefit_price ? 
         $booking->benefit_price : 0;

        $booking->benefit_cost = $benefit_cost ? 
          Num::format_decimal(round($benefit_cost),2) : 0;

        $booking->check_in_date = date(__('M j, Y'), strtotime($booking->check_in_date));
        $booking->check_out_date = date(__('M j, Y'), strtotime($booking->check_out_date));
        $booking->booking_date = date(__('M j, Y'), $booking->created + Date::offset($this->logged_in_admin->timezone));
        $booking->hotel_revenue =  Num::format_decimal($prices['hotel_revenue']);
        $booking->ota_revenue =  Num::format_decimal($prices['ota_revenue']);
        
        // OTA revenue
        $ota_revenue = $prices['ota_revenue'] - $booking->benefit_price;
        $booking->ota_revenue =  Num::format_decimal(round($ota_revenue),2);

        $booking->amount_paid_by_guest_guest = Num::format_decimal(round($prices['amount_paid_by_guest'] / $hotel_to_guest_exchange->amount * $hotel_to_guest_exchange->rate));
        $booking->amount_paid_by_guest_transaction = Num::format_decimal(round($prices['amount_paid_by_guest'] / $hotel_to_transaction_exchange->amount * $hotel_to_transaction_exchange->rate));
        $booking->amount_paid_by_guest =  Num::format_decimal($prices['amount_paid_by_guest']);
        $booking->amount_money_refunded_to_guest = Num::format_decimal($prices['amount_money_refunded_to_guest']);
        $booking->amount_point_refunded_to_guest = $prices['amount_point_refunded_to_guest'];
        $booking->reward_point = $prices['reward_point'];

        $booking->guest_currency_code = $guest_currency->code;
        $booking->transaction_currency_code = $transaction_currency->code;
        
        $total_hotel_revenues += $prices['hotel_revenue'];      
        $total_ota_revenues += $ota_revenue;
        $total_benefit_cost += $benefit_cost;
        $total_amount_paid_by_guest += $prices['amount_paid_by_guest'];
        $total_amount_money_refunded_to_guest += $prices['amount_money_refunded_to_guest'];
        $total_amount_point_refunded_to_guest += $prices['amount_point_refunded_to_guest'];
        $total_reward_points += $prices['reward_point'];
        $total_number_of_points_used += $booking->number_of_points_used;
        
        $booking->can_refund = ($booking->is_canceled AND ! $booking->is_refunded);
      }
      
      $filtered_hotel->total_hotel_revenues = Num::format_decimal($total_hotel_revenues);
      $filtered_hotel->total_ota_revenues = Num::format_decimal($total_ota_revenues);
      $filtered_hotel->total_benefit_cost = Num::format_decimal($total_benefit_cost);
      $filtered_hotel->total_amount_paid_by_guest = Num::format_decimal($total_amount_paid_by_guest);
      $filtered_hotel->total_amount_money_refunded_to_guest = Num::format_decimal($total_amount_money_refunded_to_guest);
      $filtered_hotel->total_amount_point_refunded_to_guest = $total_amount_point_refunded_to_guest;
      $filtered_hotel->total_reward_points = $total_reward_points;
      $filtered_hotel->total_number_of_points_used = Num::format_decimal($total_number_of_points_used);
      
      $filtered_hotels[] = $filtered_hotel;
    }
    
    return $filtered_hotels;
  }
	
}