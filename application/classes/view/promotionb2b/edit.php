<?php defined('SYSPATH') or die('No direct script access.');

class View_Promotionb2b_Edit extends Kostache {
  
  public $_campaign;
  
  public function campaign()
  {
    if ( ! $this->_campaign)
    {
      $this->_campaign = Arr::overwrite($this->campaign->as_array(), $this->values);
    }
    
    return $this->_campaign;
  }
  
  public function rooms()
  {
    $rooms = array();
    
    $selected_rooms = $this->campaign
      ->rooms
      ->find_all()
      ->as_array('id');
    
    foreach ($this->rooms as $room)
    {
      $rooms[] = Arr::merge($room->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "room_ids.$room->id", (bool) Arr::get($selected_rooms, $room->id)),
      ));
    }
    
    return $rooms;
  }
  
  public function languages()
  {
    $languages = array();
    
    $selected_languages = $this->campaign
      ->languages
      ->find_all()
      ->as_array('id');
    
    foreach ($this->languages as $language)
    {
      $languages[] = Arr::merge($language->as_array(), array(
        'checked' => (bool) Arr::path($this->values, "language_ids.$language->id", (bool) Arr::get($selected_languages, $language->id)),
      ));
    }
    
    return $languages;
  }
  
  public function types()
  {
    $types = array();
    $names = array(
      1 => __('Standard Promotion'),
      2 => __('Early Bird'),
      3 => __('Last Minute'),
      4 => __('Non Refundable'),
      5 => __('Flash Deal (50% Discount)'),
    );
    
    foreach ($names as $number => $name)
    {
      $types[] = array(
        'number' => $number,
        'name' => $name,
        'selected' => Arr::get($this->values, 'type', $this->campaign->type) == $number,
      );
    }
    
    return $types;
  }
  
  public function benefits()
  {
    $benefits = array();
    $names = array(
      0 => __('% Discount (Per Night)'),
      1 => __('Discount (Per Night)'),
      2 => __('Free Nights'),
    );
    
    $benefit = 0;
    if ($this->campaign->discount_rate > 0)
    {
      $benefit = 0;
    }
    elseif ($this->campaign->discount_amount > 0)
    {
      $benefit = 1;
    }
    elseif ($this->campaign->number_of_free_nights > 0)
    {
      $benefit = 2;
    }
    
    foreach ($names as $number => $name)
    {
      $benefits[] = array(
        'number' => $number,
        'name' => $name,
        'selected' => Arr::get($this->values, 'benefit', $benefit) == $number,
      );
    }
    
    return $benefits;
  }

  public function other_benefits()
  {
    $other_benefits = array();
    $other_benefits_single = array();
    $other_benefits_datas = '';

    foreach ($this->other_benefits as $number_datas => $other_benefit)
    {
      $other_benefits_single[] = array(
        'number' => $other_benefit['benefit_id'],
        'name' => substr($other_benefit['name'], 0, 30),
      );

      $other_benefits[] = array(
        'count' => $number_datas,
        'value' => $other_benefit['value'],
        'other_benefits_single' => $other_benefits_single
      );

      unset($other_benefits_single);
    }

    return $other_benefits;
  }

  public function benefit_texts()
  {
    $other_benefit_texts = '';

    foreach ($this->data_benefits as $number => $benefit_text)
    { 
      $other_benefit_texts[$benefit_text->benefit_id] = $benefit_text->name;
    }

    return $other_benefit_texts;
  }

  public function other_benefits_live()
  {
    $other_benefits_live = array();

    foreach ($this->data_benefits as $number => $benefit_text)
    {
      $other_benefits_live[] = array(
        'number' => $benefit_text['name'],
        'name' => addslashes(substr($benefit_text['name'], 0, 30)),
        'id' => $benefit_text['benefit_id']
      );
    }

    return $other_benefits_live;
  }
  
  public function cancellation_types()
  {
    $cancellation_types = array();
    
    $names = array(
      0 => __('Default (Use Room Cancellation Policy)'),
      1 => __('Custom Cancellation Policy'),
    );
    
    $is_default_cancellation = count($this->cancellations) == 0 ? 0 : 1;
    
    foreach ($names as $number => $name)
    {
      $cancellation_types[] = array(
        'number' => $number,
        'name' => $name,
        'selected' => Arr::get($this->values, 'is_default_cancellation', $is_default_cancellation) == $number,
      );
    }
    
    return $cancellation_types;
  }
  
  public function cancellation_level_1_rules()
  {
    $cancellation_level_1_rules = array();
    
    foreach ($this->cancellation_level_1_rules as $cancellation_level_1_rule)
    {
      $cancellation_level_1_rules[] = Arr::merge($cancellation_level_1_rule->as_array(), array(
        'selected' => Arr::get($this->values, 'level_1_cancellation_rule_id', $this->cancellation->level_1_cancellation_rule_id) == $cancellation_level_1_rule->id,
      ));
    }
    
    return $cancellation_level_1_rules;
  }
  
  public function cancellation_level_2_rules()
  {
    $cancellation_level_2_rules = array();
    
    foreach ($this->cancellation_level_2_rules as $cancellation_level_2_rule)
    {
      $cancellation_level_2_rules[] = Arr::merge($cancellation_level_2_rule->as_array(), array(
        'selected' => Arr::get($this->values, 'level_2_cancellation_rule_id', $this->cancellation->level_2_cancellation_rule_id) == $cancellation_level_2_rule->id,
      ));
    }
    
    return $cancellation_level_2_rules;
  }
  
  public function cancellation_level_3_rules()
  {
    $cancellation_level_3_rules = array();
    
    foreach ($this->cancellation_level_3_rules as $cancellation_level_3_rule)
    {
      $cancellation_level_3_rules[] = Arr::merge($cancellation_level_3_rule->as_array(), array(
        'selected' => Arr::get($this->values, 'level_3_cancellation_rule_id', $this->cancellation->level_3_cancellation_rule_id) == $cancellation_level_3_rule->id,
      ));
    }
    
    return $cancellation_level_3_rules;
  }
  
}