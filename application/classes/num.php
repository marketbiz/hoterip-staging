<?php

class Num extends Kohana_Num {
  
  /**
	 * Format number for displaying
	 *
	 * @param   float    number to format
	 * @return  string
   * 
   */
	public static function display($number)
	{
    return preg_replace('/(\d)(?=(\d\d\d)+(?!\d))/', '$1,', $number);
	}
  
  public static function format_decimal($number)
  {
    $formatter = NumberFormatter::create(I18n::lang(), NumberFormatter::DECIMAL);
    return $formatter->format($number);
  }
  
}