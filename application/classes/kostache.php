<?php defined('SYSPATH') or die('No direct script access.');

abstract class Kostache extends Kohana_Kostache {
  
  public function stash($template, $view, $partials)
  {
    return $this->_stash($template, $view, $partials);
  }

  public function __()
  {
    return function ($string) 
    {
      return __($string);
    };
  }
  
  public function auto_p()
  {
    $view = $this;

    return function($string) use ($view)
    {
      return Text::auto_p($view->stash($string, $view, array()));
    };
  }
  
  public function nl2br()
  {
    $view = $this;

    return function($string) use ($view)
    {
      return nl2br($view->stash($string, $view, array()));
    };
  }
  
  public function plural()
  {
    $view = $this;

    return function($string) use ($view)
    {
      $rendered = $view->stash($string, $view, array())->render();
      list($str, $count) = explode(',', $rendered);
      return Inflector::plural($str, $count);
    };
  }
  
  public function url()
  {
    return function ($string) 
    {
      $route = explode('/', $string);
      
      $controller = Arr::get($route, 0);
      $action = Arr::get($route, 1);
      $id = Arr::get($route, 2);
      
      return Route::url('default', array('controller' => $controller, 'action' => $action, 'id' => $id));
    };
  }
  
  public function render()
  {
    $this->set('base', URL::base());
    
    return parent::render();
  }
  
  
}