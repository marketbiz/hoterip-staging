<?php

class Arr extends Kohana_Arr {
  
  /**
	 * Rotates a 2D array clockwise.
	 * Example, turns a 2x3 array into a 3x2 array.
   * Very useful for validating multiple file input.
	 *
	 * @param   array    array to rotate
	 * @param   boolean  keep the keys in the final rotated array. the sub arrays of the source array need to have the same key values.
	 *                   if your subkeys might not match, you need to pass FALSE here!
	 * @return  array
	 */
	public static function rotate($source_array, $keep_keys = TRUE)
	{
		$new_array = array();
		foreach ($source_array as $key => $value)
		{
			$value = ($keep_keys === TRUE) ? $value : array_values($value);
			foreach ($value as $k => $v)
			{
				$new_array[$k][$key] = $v;
			}
		}

		return $new_array;
	}
  
  /**
	 * Retrieves multiple keys from an array. If the key does not exist in the
	 * array, it will be ignored.
	 *
	 * @param   array   array to filter
	 * @param   array   list of allowed key names
	 * @return  array
	 */
  public static function filter($array, array $allowed_keys)
  {
    return array_intersect_key($array, array_flip($allowed_keys));
  }
  
  /**
	 * Fill an array with a range of numbers.
	 *
	 *     // Fill an array with values 1,3,5
	 *     $values = Arr::range2(1, 5, 2);
	 *
	 * @param   integer  starting number
	 * @param   integer  ending number
   * @param   integer  stepping
	 * @return  array
	 */
  public static function range2($start = 1, $end = 10, $step = 1)
	{
    $array = array();
    for ($i = $start; $i <= $end; $i += $step)
    {
      $array[$i] = $i;
    }
    return $array;
	}
  
}