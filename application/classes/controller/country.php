<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Country extends Controller_Layout_Admin {
	
	public function before()
	{	
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('country', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			if (A2::instance()->allowed('country', 'delete'))
			{
				try
				{
					// Get countries id
					$country_ids = $this->request->post('ids');
			
					// Delete countries
					DB::delete('countries')
						->where('id', 'IN', $country_ids)
						->execute();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			}
		}
		
		$countries = ORM::factory('country')
			->select(
				array('country_texts.name', 'name'),
				array(DB::expr('COUNT(hotels.id)'), 'total_hotels')
			)
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
			->join('cities', 'left')->on('cities.country_id', '=', 'countries.id')
			->join('hotels', 'left')->on('hotels.city_id', '=', 'cities.id')
			->where('country_texts.language_id', '=', $this->selected_language->id)
			->group_by('countries.id')
			->order_by('country_texts.name', 'ASC')
			->find_all();
		
		$this->template->main = Kostache::factory('country/manage')
			->set('notice', Notice::render())
			->set('countries', $countries);
	}
	
	public function action_add()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('country', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage countries
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'country', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Create country
				$country = ORM::factory('country')
					->create_country($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'country', 'action' => 'add')), __('Add another country'))));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'country', 'action' => 'edit', 'id' => $country->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('country'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('country/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('languages', $languages);
	}
	
	public function action_edit()
	{
		// Get country id
		$country_id = (int) $this->request->param('id');
		
		// Get country
		$country = ORM::factory('country')
			->where('id', '=', $country_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed('country', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage countries
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'country', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Update country
				ORM::factory('country')
					->where('id', '=', $country_id)
					->find()
					->update_country($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('country'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$country = ORM::factory('country')
			->where('id', '=', $country_id)
			->find();
		
		// Get country texts
		$country_texts = ORM::factory('country_text')
			->where('country_id', '=', $country->id)
			->find_all()
			->as_array('language_id');
		
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('country/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('country', $country)
			->set('country_texts', $country_texts)
			->set('languages', $languages);
	}

	public function action_delete()
	{
		// Get country id
		$country_id = $this->request->param('id');
		
		if (A2::instance()->allowed('country', 'delete'))
		{
			try
			{
				// Delete country
				DB::delete('countries')
					->where('id', '=', $country_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}