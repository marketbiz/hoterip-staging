<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Subscribe extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('subscribe', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    $field = $this->request->query('field');
    $keyword = $this->request->query('keyword');
    
    $page = Arr::get($this->request->query(), 'page', 1);
		$items_per_page = 30;
		$offset = ($page - 1) * $items_per_page;
    
    // Get subscribers
		$subscribers = ORM::factory('subscriber')
      ->select(
        array('email', 'email'),
        array('languages.name', 'language_name')
      )
      ->join('languages')->on('languages.id', '=', 'subscribers.language_id');

    if ($keyword)
    {
      if ($field == 1)
      {
        $subscribers->where('email', '=', $keyword);
      }
    }
    
    // Get total users
    $total_users = $subscribers
      ->reset(FALSE)
      ->count_all();
    
    // Get subscribers
    $subscribers = $subscribers
      ->offset($offset)
      ->limit($items_per_page)
      ->find_all();

		// Create pagination
		$pagination = Pagination::factory(array(
			'items_per_page' => $items_per_page,
			'total_items' => $total_users,
		));

		$this->template->main = Kostache::factory('subscribe/manage')
			->set('notice', Notice::render())
      ->set('filters', $this->request->query())
			->set('subscribers', $subscribers)
      ->set('pagination', $pagination);
	}
  
  public function action_delete()
  {
    $subscribe_id = $this->request->param('id');
    
    if (A2::instance()->allowed('subscribe', 'delete'))
		{
			try
			{
				// Delete newsletter
				DB::delete('subscribers')
					->where('id', '=', $subscribe_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
  }
  
  public function action_unsubscribe() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('subscribe', 'unsubscribe'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    $subscribe_id = $this->request->param('id');

    try
    {
      // Unsubscribe newsletter
      ORM::factory('subscriber')
        ->where('id', '=', $subscribe_id)
        ->find()
        ->values(array(
          'is_subscribe_newsletter' => 0,
        ))
        ->update();
      
      Notice::add(Notice::SUCCESS, Kohana::message('user', 'unsubscribe_success'));
    }
    catch (Exception $e)
    {
      Notice::add(Notice::ERROR, $e->getMessage());
    }
    
    $this->request->redirect($this->request->referrer());
  }

  public function action_subscribe() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('subscribe', 'subscribe'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    $subscribe_id = $this->request->param('id');

    try
    {
      // Unsubscribe newsletter
      ORM::factory('subscriber')
        ->where('id', '=', $subscribe_id)
        ->find()
        ->values(array(
          'is_subscribe_newsletter' => 1,
        ))
        ->update();
      
      Notice::add(Notice::SUCCESS, Kohana::message('user', 'subscribe_success'));
    }
    catch (Exception $e)
    {
      Notice::add(Notice::ERROR, $e->getMessage());
    }
    
    $this->request->redirect($this->request->referrer());
  }
}