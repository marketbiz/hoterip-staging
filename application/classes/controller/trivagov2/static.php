<?php defined('SYSPATH') or die('No direct script access.');


class Controller_TrivagoV2_Static extends Controller {


  public static function selectedCurrency($code)
  {
    // Set default currency
    $selected_currency = ORM::factory('currency')->get_currency_by_code($code);

    return $selected_currency;
  }

  public static function auth($auth)
  {
    //Check Direct API
    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
    $ip = getenv("HTTP_CLIENT_IP");
    else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
    $ip = getenv("HTTP_X_FORWARDED_FOR");
    else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
    $ip = getenv("REMOTE_ADDR");
    else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
    $ip = $_SERVER['REMOTE_ADDR'];
    else
    $ip = "unknown";


    // Get admins
    $api_access = DB::select(
        array('api_access.api_key', 'api_key'),
        array('api_access.ip', 'ip')     
      )
      ->from('admins')
      ->join('roles')->on('roles.value', '=', 'admins.role')
      ->join('api_access')->on('api_access.admin_id', '=', 'admins.id')
      ->where('admins.role', '=', 'api')
      ->where('admins.name', '=', $auth['source'])
      ->where_open()
      ->where('api_access.ip', '=', $ip)
      ->or_where('api_access.api_key', '=', $auth['key'])
      ->where_close()
      ->limit(1)
      ->as_object()
      ->execute()
      ->current()
      ;

    if (A1::instance()->login($auth['User'], $auth['Pwd'], (bool) False))
    {
      die('1');
      return TRUE;
    }
    if(!empty($api_access))
    {
      if($api_access->api_key == $auth['key'])
      {
        return TRUE;
      }
      elseif($api_access->ip == $ip)
      {
        return TRUE;
      }
    }
    
    return FALSE;
  }

  public static function config()
  {
    $config = Kohana::config('application');
    
    return $config;
  }

  public static function default_language()
  {
    // Set default language id using english
    $default_language_id = ORM::factory('language')
      ->where('code', '=', 'en-us')
      ->find()
      ->id;
    
    return $default_language_id;
  }

  public static function errors($errors, $node_head)
  {
    // Logout account
    A1::instance()->logout();

    $list_error_type = array(
      '1' =>'Unknown',

      '00' =>'Authentication',
      '1' =>'Unknown',
      '2' =>'No implementation',
      '3' =>'Biz rule',
      '4' =>'Authentication',
      '5' =>'Authentication timeout',
      '6' =>'Authorization',
      '7' =>'Protocol violation',
      '8' =>'Transaction model',
      '9' =>'Authentication model',
      '10' =>'Required field missing',
      '11'=> 'Version',

      // '00' =>'Welcome',
      );
    
    $list_error_code = array(
      '15' =>'Invalid Date',
      '61' =>'Invalid currency code',
      '77' => 'Invalid Transaction',
      '79' => 'Invalid Name Space',
      '55' => 'Invalid Version',
      '69' =>'Minimum stay criteria not fulfilled',
      '70' =>'Maximum stay criteria not fulfilled',
      '87' =>'Booking reference invalid',
      '95' =>'Booking already cancelled',
      '97' =>'Booking reference not found',
      '111' =>'Booking invalid',
      '127' =>'Reservation already exists',
      '146' =>'Service requested incorrect',
      '147' =>'Taxes incorrect',
      '161' =>'Search criteria invalid',
      '245' =>'Invalid confirmation number',
      '249' =>'Invalid rate code',
      '251' =>'Last name and customer number do not match',
      '321' =>'Required field missing',
      '364' =>'Error rate range',
      '394' =>'Invalid item',
      '397' =>'Invalid number of adults',
      '400' =>'Invalid property code',
      '402' =>'Invalid room type',
      '450' =>'Unable to process',
      '459' =>'Invalid request code',

      '00' =>'Welcome',
      '9900' =>'Invalid Username or Password',
      );

    //Generate Error XML
    $doc = new DOMDocument('1.0', 'UTF-8');
    $doc->formatOutput = TRUE;

    $RS = $doc->appendChild($doc->createElement(substr($node_head['head'], 0, -2).'RS'));
    $RS->setAttribute('xmlns', $node_head['xmlns']);
    $RS->setAttribute('TimeStamp', $node_head['TimeStamp']);
    $RS->setAttribute('Target', $node_head['Target']);
    $RS->setAttribute('Version', $node_head['Version']);

    $errors_node = $RS->appendChild($doc->createElement('Errors'));

    foreach ($errors as $key =>$error)
    {
      $error_descriptions = str_replace("_", " ", $key).', '.$list_error_code[$error['code']].'.';

      $error_node = $errors_node->appendChild($doc->createElement('Error',$error_descriptions));
      $error_node->setAttribute('Language', 'en-us');
      $error_node->setAttribute('Type', $error['type']);
      $error_node->setAttribute('ShortText', 4000);
      $error_node->setAttribute('Code', $error['code']);
    }

    $response = $doc->saveXML();

    return $response;
  }
}