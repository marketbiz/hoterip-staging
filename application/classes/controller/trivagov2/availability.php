<?php defined('SYSPATH') or die('No direct script access.');


class Controller_TrivagoV2_Availability extends Controller {


  public function checker($node_head, $xmlns, $head, $body)
  {

    //check namespace
    if($xmlns['soap'] != 'http://schemas.xmlsoap.org/soap/envelope/' 
      || $xmlns['api'] != 'http://api.trivago.com/')
    {
      $error['Name_Space']['type'] = '3';
      $error['Name_Space']['code'] = '79';
    }

    //auth API
    $auth = array(
      'usr' => (string)$head->children()->Username,
      'pwd' => (string)$head->children()->Password,
      'key' => '898276cf8055f5d04dedaea6a8c6f477',
      'source' => 'trivago',
      );

    // $xml_body = 
    if(!Controller_TrivagoV2_Static::auth($auth))
    {
      $error['Welcome']['type'] = '00';
      $error['Welcome']['code'] = '9900';
    }

    //check transaction
    if((string)$body->attributes()->Transaction != 'HotelAvailRQ')
    {
      $error['Transaction']['type'] = '8';
      $error['Transaction']['code'] = '77';
    }


    //check Target Enenvironment
    $vacation_environment = strtolower((string)$body->attributes()->Target)  == 'production' ? Kohana::PRODUCTION : Kohana::DEVELOPMENT;

    if(Kohana::$environment != $vacation_environment)
    {
      $error['Authorization']['type'] = '6';
      $error['Authorization']['code'] = '146';
    }

    $body = $body->children('api', true)->findAvailabilities->children();
    $version = (string)$body->attributes()->version;

    //check version
    if($version != '2.0')
    {
      $error['Authorization']['type'] = '2';
      $error['Authorization']['code'] = '55';
    }

    $hotelIdentifiers = (array)$body->children()->hotelIdentifiers;
    $data_rq['hotelIdentifiers'] = $hotelIdentifiers['hotelId'];

    $data_rq['check_in'] = (string)$body->children()->arrivalDate;
    $data_rq['check_out'] = (string)$body->children()->departureDate;

    //check valid date
    if(($data_rq['check_in'] >= $data_rq['check_out']) || $data_rq['check_in'] < date('Y-m-d'))
    { 
      $error['Invalid_date']['type'] = '2';
      $error['Invalid_date']['code'] = '15';
    }

    $data_rq['stay_night'] = Date::span(strtotime($data_rq['check_in']), strtotime($data_rq['check_out']), 'days');
    $data_rq['rooms'] = array();

    $rooms = (array)$body->children()->rooms;
    $roomOccupancies = $rooms['roomOccupancy'];

    foreach ($roomOccupancies as $occupancies => $occupancy) 
    {
      if($roomOccupancies->adults)
      {
        $data_rq['rooms'][0]['adult'] = (string)$roomOccupancies->adults;

        //check value node adult
        if(!$data_rq['rooms'][0]['adult'])
        {
          $error['Required_field_missing']['type'] = '10';
          $error['Required_field_missing']['code'] = '450';
        }

        $data_rq['rooms'][0]['child'] = (array)$roomOccupancies->children()->children;

        //check check value node adult
        foreach ($data_rq['rooms'][0]['child'] as $age) 
        {
          if(is_array($age))
          {
            foreach ($age as $value) 
            {
              if(empty($value))
              {
                $error['Required_field_missing']['type'] = '10';
                $error['Required_field_missing']['code'] = '450';
              }
            }
          }
          else
          {
            if(empty($age))
            {
              $error['Required_field_missing']['type'] = '10';
              $error['Required_field_missing']['code'] = '450';
            }
          }
        }
      }
      else
      {
        $data_rq['rooms'][$occupancies]['adult'] = (string)$occupancy->adults;

        //check node  adult
        if(!$data_rq['rooms'][$occupancies]['adult'])
        {
          $error['Required_field_missing']['type'] = '10';
          $error['Required_field_missing']['code'] = '450';
        }

        $data_rq['rooms'][$occupancies]['child'] = (array)$occupancy->children;

        //check check value node adult
        foreach ($data_rq['rooms'][$occupancies]['child'] as $age) 
        {
          if(is_array($age))
          {
            foreach ($age as $value) 
            {
              if(empty($value))
              {
                $error['Required_field_missing']['type'] = '10';
                $error['Required_field_missing']['code'] = '450';
              }
            }
          }
          else
          {
            if(empty($age))
            {
              $error['Required_field_missing']['type'] = '10';
              $error['Required_field_missing']['code'] = '450';
            }
          }
        }
      }
    }

    $data_rq['currency'] = (string)$body->children()->currency;

    //check currency code
    $data_rq['currency'] = Controller_TrivagoV2_Static::selectedCurrency($data_rq['currency']);

    if(empty($data_rq['currency']))
    {
      $error['Currency']['type'] = '2';
      $error['Currency']['code'] = '61';
    }

    $data_rq['language'] = Controller_TrivagoV2_Static::default_language();


    if(empty($error))
    {
      $this->process($data_rq, $node_head, $xmlns);
    }
    else
    {
      $errors = Controller_TrivagoV2_Static::errors($error, $node_head);

      $this->output($errors);
    }
  }

  public function process($data_rq, $node_head, $xmlns)
  {  

    $error = array();

    // Get hotel
    $hotel = ORM::factory('hotel')
      ->where('id', '=', $data['HotelCode'])
      ->find();

    //if rq hotel id = 1
    if(count($data_rq['hotelIdentifiers'])==1)
    {
      //get hotel only 1 id
      $hotels = ORM::factory('hotel')
      ->where('id', '=', $data_rq['hotelIdentifiers'])
      ->find_all();
    }
    //else get hotel any id
    else
    {
      //get hotel
      $hotels = ORM::factory('hotel')
      ->where('id', 'IN', $data_rq['hotelIdentifiers'])
      ->find_all()
      ;
    }

    //get capasities
    foreach ($data_rq['rooms'] as $roomsk => $value) 
    {
      $hotel_query['capasities'][$roomsk]['adult'] = $value['adult'];
      $hotel_query['capasities'][$roomsk]['child'] = count($value['child']['age']);
    }

    if(count($hotels))
    {
      foreach ($hotels as $hotel)
      {
        //get rooms
        $rooms = Model::factory('room')->get_by_hotel_id($data_rq['language'], $hotel->id);

        foreach ($rooms as $roomk => $room)
        {
          // Room data and Capacities
          $hotel_query['hotel_id'] =  $hotel->id;
          $hotel_query['check_in'] =  $data_rq['check_in'];
          // $hotel_query['check_out'] =  date('Y-m-d', strtotime($data_rq['check_in']."-1 day"));
          $hotel_query['number_of_rooms'] = count($data_rq['rooms']);
          $hotel_query['check_out'] =  $data_rq['check_out'];
          $hotel_query['language_id'] = $data_rq['language'];
          $hotel_query['now'] =  Date::formatted_time('now', 'Y-m-d', $hotel->timezone);
          $hotel_query['RatePlanCode'] =  '*';
          $hotel_query['InvTypeCode'] =  $room->room_id;

          //get campaigns
          $campaigns = Model::factory('hotel')->get_campaigns($hotel->id, $hotel_query, FALSE);

          if(count($campaigns))
          {
            foreach ($campaigns as &$campaign)
            {
              // Assign varibale room
              $campaign['quantity'] = $hotel_query['Quantity'] ? $hotel_query['Quantity'] : 1;
              $campaign['capacities'] = $hotel_query['GuestCounts'];

              $campaign['room_capacity'] = Model::factory('room_capacity')->check_room_capacity($campaign['room_id'],
                 $hotel_query['capasities']);

              //$campaign['room_capacities'] = Model::factory('room_capacity')->get_room_capacities($campaign['room_id']);

              $campaign['nights'] = Date::span(Helper_Date::create_date($hotel_query['check_in'])->getTimestamp(),
              Helper_Date::create_date($hotel_query['check_out'])->getTimestamp(), 'days');
              $campaign['night_count'] = 0;
              $campaign['total_price'] = 0;
              $campaign['average_price'] = NULL;

              // Redeclare room
              $hotel_query['InvTypeCode'] = $campaign['room_id'];
              $hotel_query['campaign_default'] = $campaign['campaign_default'];
              $campaign['items'] = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);

              if(empty($campaign['items']))
              {
                $campaign['items'] = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query, TRUE);
              }

              // Get campaign min stock
              $min_stock = isset($campaign['items'][$hotel_query['check_in']]) ? min(Arr::pluck($campaign['items'], 'stock')) : 99999;

              $remaining_stock = Kohana::$config->load('hoterip')->get('remaining_stock');

              // If min stock less then remaining stock
              if($min_stock <= $remaining_stock)
              {
                $campaign['remaining_flag'] = $min_stock;
              }

              // Determine if guest stay night more than minimum night
              $campaign['minimum_night_available'] = $campaign['max_minimum_night'];
            }

            foreach ($campaigns as $campaignk => &$campaign)
            {
              $campaign['default'] = 0;
              if($campaign['type'] == 3)
              {
                $campaign['last_days_date'] = Date::formatted_time("+{$campaign['last_days']} days", __('M j, Y'), $campaign['hotel_timezone']);
                
                $last_days =$campaign['last_days_date'];
                $last_days = Date::formatted_time("$last_days", 'Y-m-d', $campaign['hotel_timezone']);

                if ($hotel_query['check_in'] <= $last_days AND $hotel_query['check_out'] <= $last_days) {
                  $campaign['default'] = 0;
                }
                elseif ($hotel_query['check_in'] < $last_days AND $hotel_query['check_out'] >= $last_days) {
                  $campaign['default'] = 0;
                }
                else
                {
                  $campaign['default'] = 1;
                  unset($campaigns[$campaignk]);
                }
              }

              if($campaign['type'] == 2)
              {

                $early_days = $campaign['early_days'] -1;
                $campaign['early_days_date'] = Date::formatted_time("now + $early_days days", 'Y-m-d');

                $early_days =$campaign['early_days_date'];

                if($hotel_query['check_in']<=$early_days)
                {
                  unset($campaigns[$campaignk]);
                }
                else
                {
                  if($hotel_query['check_in'] > $early_days AND $hotel_query['check_out'] <= $early_days)
                  {
                    $campaign['default'] = 1;
                  }
                  elseif ($hotel_query['check_in'] < $early_days AND $hotel_query['check_out'] >= $early_days) {
                    $campaign['default'] = 0;
                  }
                  elseif ($hotel_query['check_in'] > $early_days AND $hotel_query['check_out'] >= $early_days) {
                    $campaign['default'] = 0;
                  }
                }
              }

              foreach ($campaign['items'] as $itemk => &$item)
              {
                if (isset($item['date']))
                {
                  // Show flag true
                  $item['remaining_flag'] = FALSE;

                  // If room capacity is true
                  if ($campaign['room_capacity'])
                  {
                    // If not blackout or not weekout
                    if (!($item['blackout'] OR $item['weekout']))
                    {
                      //If there is stock
                      if ($item['stock'] >= $hotel_query['number_of_rooms'])
                      {
                        // Out flag ture
                        $item['price_flag'] = TRUE;
                        $campaign['night_count']++;

                        // If campaign blackout
                        if ($item['campaign_blackout'])
                        {
                          $item['price'] = $item['price'];
                        }
                        
                        /// Campaign Prices. Is campaign prices
                        // elseif ($item['is_campaign_prices'])
                        // {
                        //   //application control
                        //   if($week_item['application_control'] != NULL)
                        //   {
                        //     $application_controls =  unserialize($week_item['application_control']);
                        //     if (count($application_controls) > 0) 
                        //     {
                        //       foreach ($application_controls as $key_application_control => $application_control) {
                        //         if (date('w', strtotime($week_item['date'])) == $key_application_control AND $application_control) 
                        //         {
                        //           $day['price'] = $week_item['price'];
                        //         }
                        //       }
                        //     }
                        //   }
                        // }

                        else
                        {
                          // If promotion
                          if ($item['promotion'])
                          {
                            // If free night
                            if ($item['free_night'])
                            {
                              $item['price'] = 0;
                              $item['free_flag'] = TRUE;
                            }
                            else
                            {
                              $item['price'] = ($item['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                            }
                          }
                          else
                          {
                            $item['price'] = $item['price'];
                          }
                          $item['remaining_flag'] = TRUE;
                        }
                      }
                    }
                  }
                }
              }
            }

            // unset $campaign
            unset($campaign);

            $room_is_valid = true;
            $prev_hotel_data = array();
            foreach ($campaigns as $campaignk => $campaign)
            {
              $campaign_is_valid = true;
              foreach ($campaign['items'] as $kcampaign => $icampaign)
              {
                if(!($icampaign['remaining_flag'] AND ($icampaign['price'] || $campaign['free_night'])))
                {
                  $campaign_is_valid = false;
                  break;
                }
              }

              if (!$campaign_is_valid) 
              {
                continue;
              }
              $hotel_data = array();

              //get surcharge
                //get surcharge
                $surcharge_all = Model::factory('surcharge')->get_surcharges($hotel->id, $data_rq['check_in'], $data_rq['check_out']);            
                $surcharge_date = array();

                //change key array surcharge
                if(count($surcharge_all))
                {
                  foreach ($surcharge_all as $surchargek => $value) {
                      $surcharge_date[$value['date']] = $value ;
                  }

                  //combine array surcharge to campaign items
                  foreach ($surcharge_date as $surchargek => $value) {
                    $campaign['items'][$surchargek]['surcharge']= $value;
                  }
                }

                $max_adult = DB::select('number_of_adults')
                ->from('room_capacities')
                ->where('room_id', '=', $campaign['room_id'])
                ->order_by('number_of_adults', 'desc')
                ->execute()
                ->current();

                $campaign['occupancies'] = $max_adult['number_of_adults'];

                $occupancies = array();
                //calculate ectrabed and selection age child
                foreach ($data_rq['rooms'] as $occupanciesk => $occupancie)
                {
                  foreach ($occupancie['child'] as $agek => $age)
                  {
                    if(is_array($age))
                    {
                      foreach ($age as $agek => $value) 
                      {
                       if(!($hotel->child_age_from <= $value AND $hotel->child_age_until >= $value))
                        {
                          unset($data_rq['rooms'][$occupanciesk]['child']['age'][$agek]);
                        }
                        if(!count($occupancie['child']))
                        {
                          $data_rq['rooms'][$occupanciesk]['child'] = array();
                        }
                      }

                    }
                    else
                    {
                      if(!($hotel->child_age_from <= $age AND $hotel->child_age_until >= $age))
                      {
                        $data_rq['rooms'][$occupanciesk]['child'] = array();
                      }
                    }
                  }

                  $occupancies = DB::select(
                    'room_capacities.*'
                    )
                    ->from('room_capacities')
                    ->where('room_id', '=', $campaign['room_id'])
                    ->where('number_of_adults', '=', $occupancie['adult'])
                    ->where('number_of_children', '=', count($occupancie['child']['age']))
                    ->execute()
                    ->current();

                  $extrabed = $occupancies['number_of_extrabeds'];
                  $campaign['extrabed'] += $extrabed;
                }

              if(!empty($campaign['campaign_id']))
              {

                $hotel_data['promotion_exist'] = 1;
                $hotel_data['on_requested'] = 1;
                $hotel_data['total_price'] = 0;
                $hotel_data['average_price'] = 0;

                foreach ($campaign['items'] as &$item)
                {
                  $hotel_data['total_price'] += $item['price'];
                }

                if ($campaign['night_count'] == $campaign['nights'])
                {
                  $hotel_data['average_price'] = $hotel_data['total_price'] / $campaign['nights'];
                }

                //calculated amount of extrabeds
                // $campaign['amount_extrabed'] = 0;
                // if($campaign['extrabed'] > 0)
                // {
                //   $campaign['amount_extrabed'] = ($campaign['extrabed'] * $campaign['extrabed_price']) * $data_rq['stay_night'];
                // }

                //calculated amount of surcharge
                $total_surcharge = 0;

                if(!empty($surcharge_all))
                {
                  foreach ($surcharge_all as $surchargek => $surcharge) 
                  {
                    $amount['adult'] = 0;
                    $amount['child'] = 0;
                    foreach ($data_rq['rooms'] as $occupanciesk => $occupancie)
                    {
                      $amount['adult'] += $occupancie['adult'] * $surcharge['adult_price'];
                      $amount['child'] += count($occupancie['child']['age']) * $surcharge['child_price']; 
                    }
                  }

                  $total_surcharge = $amount['adult'] + $amount['child'];
                }
                else
                {
                  $total_surcharge = 0;
                }

                //total_item_price
                $total_item_price = ($hotel_data['total_price'] * count($data_rq['rooms']));

                // total surcharge
                $campaign['total_surcharge'] = $total_surcharge;

                // // Get extrabed price
                if($campaign['extrabed']){
                  $campaign['total_extrabed_price'] = $campaign['extrabed_price'] * $campaign['nights'];
                }else{
                  $campaign['total_extrabed_price'] = 0 ;
                }
                

                // // Get invoice_amount
                $campaign['invoice_amount'] = $total_item_price + $campaign['total_surcharge'] + $campaign['total_extrabed_price'];


                $hotel_tax_charge = $hotel->service_charge_rate + $hotel->tax_rate;
                $campaign['total_price_without_tax'] = $total_item_price / (($hotel_tax_charge + 100) / 100);
                $campaign['total_price_tax'] = $total_item_price - $campaign['total_price_without_tax'];
                $campaign['total_price_exclude_tax'] = $campaign['total_price_without_tax'] + $campaign['total_extrabed_price'] + $campaign['total_surcharge'];


                //exchange to selected currency
                //define exchane
                $exchange = Model::factory('exchange');

                $campaign['total_price_without_tax'] = round($campaign['total_price_without_tax'] * $exchange->to($campaign['currency_id'], $data_rq['currency']->id));
                $campaign['total_price_tax'] = round($campaign['total_price_tax'] * $exchange->to($campaign['currency_id'], $data_rq['currency']->id));
                $campaign['total_surcharge'] = round($campaign['total_surcharge'] * $exchange->to($campaign['currency_id'], $data_rq['currency']->id));
                $campaign['total_extrabed_price'] = round($campaign['total_extrabed_price'] * $exchange->to($campaign['currency_id'], $data_rq['currency']->id));
                $campaign['invoice_amount'] = round($campaign['invoice_amount'] * $exchange->to($campaign['currency_id'], $data_rq['currency']->id));

                // TODO campaignに入れなくてもOKかな。
                // Get no arrival
                $hotel_no_arrival = DB::select()
                  ->from('hotel_no_arrivals')
                  ->where('hotel_id', '=', $campaign['hotel_id'])
                  ->where('date', '=', $hotel_query['check_in'])
                  ->execute()
                  ->get('date');

                // TODO campaignに入れなくてもOKかな。
                // Get no departure
                $hotel_no_departure = DB::select()
                  ->from('hotel_no_departures')
                  ->where('hotel_id', '=', $campaign['hotel_id'])
                  ->where('date', '=', $hotel_query['check_out'])
                  ->execute()
                  ->get('date');

                // Determine if guest stay night more than minimum night
                $minimum_night_available = $campaign['max_minimum_night'];
                if (! $hotel_no_arrival AND ! $hotel_no_departure AND $minimum_night_available)
                {
                  if(($data_rq['stay_night'] >= $minimum_night_available) && ($data_rq['stay_night'] >= $campaign['min_nights']) &&($hotel_query['number_of_rooms'] >= $campaign['min_rooms']))
                  {
                    $hotel_data['on_requested'] = 0;
                  }
                  else
                  {
                    continue;
                  }

                  if(($data_rq['stay_night'] < $minimum_night_available) && ($data_rq['stay_night'] >= $campaign['min_nights']) &&($hotel_query['number_of_rooms'] >= $campaign['min_rooms']))
                  {
                    $hotel_data['promotion_exist'] = 0;
                  }
                }

                // Promotions Descriptions
                if($hotel_data['promotion_exist'])
                {
                  $hotel_query['hotel_currency'] = $campaign['currency_id'];
                  $hotel_query['currency_code'] = $data_rq['currency']->id;
                  $hotel_data['descriptions'] = Controller_Vacation_Static::promotion_descriptions($hotel_query, $campaign, $hotel_data['promotion_exist']);
                }
                else
                {
                  $hotel_data['descriptions'] = 'Default Promotions';
                }

                if($hotel_data['on_requested'] == 0) 
                {
                  $hotel_data = array_merge($campaign, $hotel_data);
                  //break;
                }
                else
                {                
                  // Redeclare hotel_data
                  $hotel_data = array();
                  continue;
                }
              }
              else
              {
                continue;
              }

              //get chapest campaign
              if(empty($cheap_hotel_data))
              {
                $cheap_hotel_data = $hotel_data;
              }
              elseif($hotel_data['invoice_amount'] < $cheap_hotel_data['invoice_amount'])
              {
                $cheap_hotel_data = $hotel_data;
              }

            }//loop campaigns 
            if(!empty($cheap_hotel_data));
            {
              if(count($cheap_hotel_data))
              {
                $rooms_valid[$hotel->id][$cheap_hotel_data['room_id']][$cheap_hotel_data['campaign_id']] = $cheap_hotel_data;  
              }  
            }
            $cheap_hotel_data = array();  

          }//if counts campaigns

        }//loop rooms

      }//loop hotel
    }
    else
    {
      $error['Hotel_not_found']['type'] = '2';
      $error['Hotel_not_found']['code'] = '161';
    }
    if(!count($rooms_valid))
    {
      $error['Invalid_Item']['type'] = '2';
      $error['Invalid_Item']['code'] = '394';
    }

    if(!empty($error))
    {
      $errors = Controller_TrivagoV2_Static::errors($error, $node_head);

      $this->output($errors);
    }
    else
    {
      $this->xmlBuild($data_rq, $rooms_valid, $xmlns, $node_head);
    }
  }

  public function xmlBuild($data_rq, $rooms, $xmlns, $node_head)
  { 

    try
    {
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $node_envelope = $doc->appendChild($doc->createElement('soap:Envelope'));
      $node_envelope->setAttribute('xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
      $node_envelope->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      $node_envelope->setAttribute('xmlns', $xmlns['api']);
      $node_envelope->setAttribute('xmlns:soap', $xmlns['soap']);

      $node_body = $node_envelope->appendChild($doc->createElement('soap:Body'));
      $node_body->setAttribute('TimeStamp', date('c'));
      $node_body->setAttribute('Target', $node_head['Target']);
      $node_body->setAttribute('Transaction', 'HotelAvailRQ');
      $node_availabilityResponse = $node_body->appendChild($doc->createElement('availabilityResponse'));
      $node_availabilityResponse->setAttribute('Version', '2.0');

      foreach ($rooms as $hotel_id => $hotel)
      {
        $node_hotel = $node_availabilityResponse->appendChild($doc->createElement('hotel'));
        $node_hotel->setAttribute('id', $hotel_id);
        $node_rates = $node_hotel->appendChild($doc->createElement('rates')); 
        $node_rates->setAttribute('currency', $data_rq['currency']->code);
        foreach ($hotel as $room_id => $room) 
        {
          foreach ($room as $campaign_id => $campaign)
          {
            $total_amount += $campaign['invoice_amount'];
            $node_rates->setAttribute('total', $total_amount);
    
            $node_rate = $node_rates->appendChild($doc->createElement('rate'));
            $node_rate->setAttribute('payment', 'Credit Card');
            $campaign['free_cancellation'] == TRUE ? $node_rate->setAttribute('freeCancellation', 'TRUE') : $node_rate->setAttribute('freeCancellation', 'FALSE');  

            $node_rate->setAttribute('description', $campaign['descriptions']);
            $node_rate->setAttribute('left', $campaign['stock']);
            $node_meal = $node_rate->appendChild($doc->createElement('meal'));
            $campaign['is_breakfast_included'] == TRUE ? $node_meal->setAttribute('code', 'BF') : $node_meal->setAttribute('code', 'RO');

            $node_room = $node_rate->appendChild($doc->createElement('room'));
            $node_room->setAttribute('privateUse', 'TRUE');
            $node_room->setAttribute('occupancy', $campaign['occupancies']);
            $node_room->setAttribute('name', $campaign['room_name']);

            $node_price = $node_rate->appendChild($doc->createElement('price'));
            $node_price->setAttribute('total', $campaign['invoice_amount']);
            $node_price->setAttribute('currency', $data_rq['currency']->code);
            $node_netprice = $node_price->appendChild($doc->createElement('netPrice', $campaign['total_price_without_tax']));
            $node_tax = $node_price->appendChild($doc->createElement('taxPrice', $campaign['total_price_tax']));
            //if extrabed is true
            $node_extrabed = $node_price->appendChild($doc->createElement('extrabedPrice', $campaign['total_extrabed_price']));
            $node_extrabed = $node_price->appendChild($doc->createElement('surchargesPrice', $campaign['total_surcharge']));
          }
        }
        $total_amount = 0;
      }

      $response = $doc->saveXML();

    }
    catch (Kohana_Exception $e)
    {
      $error['XML']['type'] = '1';
      $error['XML']['code'] = '450';
    }

    if(!empty($error))
    {
      $errors = Controller_TrivagoV2_Static::errors($error,$node_head);

      $this->output($errors);
    }
    else
    {
      $this->output($response);
    }
  }

  public function output($response)
  {
    CustomLog::factory()->add(6,'DEBUG', "Trivago Response: Hotel Availability");
     CustomLog::factory()->add(6,'DEBUG', $response);

    $this->response
      ->headers('Content-Type', 'text/xml')
      // ->headers('Status:', '100')
      ->body($response)
      ->status(200)
      ;
  }

  public function action_request()
  {
    // Set time limit
    set_time_limit (60);

     $xml_location = file_get_contents('php://input');
     CustomLog::factory()->add(6,'DEBUG', "Trivago Request: Hotel Availability");
     CustomLog::factory()->add(6,'DEBUG', $xml_location);
    // <api:AuthenticationHeader>
    //   <Username>trivago</Username>
    //   <Password>1234567890</Password>
    // </api:AuthenticationHeader>

    $node_head = array(
    'head' => 'ResponseRQ',
    'xmlns' => 'http://schemas.xmlsoap.org/soap/envelope/',
    'TimeStamp' => date('c'),
    'EchoToken' => '000',
    'Target' => 'Test',
    'Version' => '1.00',
    'PrimaryLangID' => 'En',
    );

    if(!$xml_location)
    {
      $error['Welcome']['type'] = '2';
      $error['Welcome']['code'] = '450';

      $errors = Controller_TrivagoV2_Static::errors($error,$node_head);

      $this->output($errors);
    }
    else
    {
      $xml = new SimpleXMLElement($xml_location);

      $namespace = $xml->getDocNamespaces(TRUE);
      // foreach($namespace as $html_element => $value)
      // {
      //   $xmlns = $value;
      // }


      $head =   $xml->children('soap', true)->Header->children('api', true)->AuthenticationHeader;
      $body =   $xml->children('soap', true)->Body;

      $this->checker($node_head, $namespace, $head, $body);
    }
  }
}