<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Account extends Controller_Layout_Admin {
	
	public function action_password() 
	{
		// If submit button clicked
		if ($this->request->post('submit'))
		{
			$logged_in_admin = A1::instance()->get_user();
			
			// Check if current password correct
			if (A1::instance()->check_password($logged_in_admin, $this->request->post('current_password')))
			{
				try
				{
					$admin = ORM::factory('admin')
						->where('id', '=', $logged_in_admin->id)
						->find();
					
					// Edit password
					$admin->password = $this->request->post('password');
					
					// Extra validation
					$extra_validation = Validation::factory($this->request->post())
						->rule('password', 'min_length', array(':value', 4))
						->rule('password_confirm', 'matches', array(':validation', ':field', 'password'))
						->label('password', 'New Password')
						->label('password_confirm', 'New Password Confirmation');
					
					// Save
					$admin->update($extra_validation);
					
					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
				}
				catch (ORM_Validation_Exception $e)
				{
					Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('account')));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('account', 'password_incorrect'));
			}
		}
		
		$this->template->main = Kostache::factory('account/password')
			->set('notice', Notice::render());
	}
	
	public function action_profile() 
	{
		if ($this->request->post('submit'))
		{
			$admin = A1::instance()->get_user();
      
      // Start database transaction
      Database::instance()->begin();
      
			try
			{
				$admin->name = $this->request->post('name');
        $admin->timezone = $this->request->post('timezone');
        $admin->is_use_auto_save = $this->request->post('is_use_auto_save') ? 1 : 0;
				$admin->save();
        
        if (Upload::not_empty($_FILES['picture']))
        {
          // Save picture
          $filename = Upload::save($_FILES['picture'], $admin->username.'.jpg', 'media/images/admins');

          // Check Mime Type
					$fileinfo = mime_content_type($filename);
						switch ($fileinfo) {
							case 'image/jpeg':
								break;
							case 'image/png':
								break;
							case 'image/gif':
								break;
							
							default:

								// Rollback transaction
								Database::instance()->rollback();
								
								unlink($filename);
								
								throw new Exception('Invalid Image Type');

								break;
						}

          // Resize picture
          Image::factory($filename)
            ->resize(50, 50, Image::INVERSE)
            ->crop(50, 50)
            ->save();
        }
        
        // Commit transaction
        Database::instance()->commit();
				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
        // Rollback transaction
        Database::instance()->rollback();
        // Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('account'));
			}
		}
    
    $timezones = ORM::factory('timezone')
      ->find_all();
		
		$this->template->main = Kostache::factory('account/profile')
			->set('logged_in_admin', A1::instance()->get_user())
			->set('notice', Notice::render())
      ->set('timezones', $timezones);
	}
	
}