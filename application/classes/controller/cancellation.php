<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cancellation extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
    
    if ($this->auto_render)
    {
      // Calculate number of rooms
      $number_of_rooms = ORM::factory('room')
        ->where('hotel_id', '=', $this->selected_hotel->id)
        ->count_all();

      // If there is no rooms
      if ($number_of_rooms <= 0)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('item', 'no_room'));
        // Redirect to manage campaign
        $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
      }
    }
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('cancellation', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			$cancellation_ids = Arr::get($this->request->post(), 'ids', array());
			
			foreach ($cancellation_ids as $cancellation_id)
			{
				$cancellation = ORM::factory('cancellation')
					->where('id', '=', $cancellation_id)
					->find();
				
				if (A2::instance()->allowed($cancellation, 'delete'))
				{
					// Get cancellation again to refresh rules value
					$cancellation = ORM::factory('cancellation')
						->select(
							array('rules_1.name', 'rule_1_name'),
							array('rules_2.name', 'rule_2_name'),
							array('rules_3.name', 'rule_3_name')
						)
						->join(array('cancellation_rules', 'rules_1'), 'left')->on('level_1_cancellation_rule_id', '=', 'rules_1.id')
						->join(array('cancellation_rules', 'rules_2'), 'left')->on('level_2_cancellation_rule_id', '=', 'rules_2.id')
						->join(array('cancellation_rules', 'rules_3'), 'left')->on('level_3_cancellation_rule_id', '=', 'rules_3.id')
						->where('cancellations.id', '=', $cancellation_id)
						->find();

					// Get rooms
					$rooms = ORM::factory('room')
						->join('campaigns_rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
						->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
						->join('campaigns_cancellations')->on('campaigns_cancellations.campaign_id', '=', 'campaigns.id')
						->where('campaigns.is_default', '=', 1)
						->where('campaigns_cancellations.cancellation_id', '=', $cancellation->id)
						->find_all();

					foreach ($rooms as $room)
					{
						// Write log
						ORM::factory('log')
							->values(array(
								'admin_id' => A1::instance()->get_user()->id,
								'hotel_id' => $room->hotel_id,
								'action' => 'Deleted Cancellation Policy',
								'room_texts' => serialize(DB::select()
									->from('room_texts')
									->where('room_id', '=', $room->id)
									->execute()
									->as_array('language_id')),
								'text' => 'Date: :start_date - :end_date. '
									.'Cancellation Policy: :cancellation_policy.',
								'data' => serialize(array(
									':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
									':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
									':cancellation_policy' => implode('. ', array_filter(array(
										$cancellation->rule_1_name,
										$cancellation->rule_2_name,
										$cancellation->rule_3_name,
									))))),
								'created' => time(),
							))
							->create();

						// Get QA admins emails
						$qa_emails = ORM::factory('admin')
							->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
							->where('admins.role', '=', 'quality-assurance')
							->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
							->find_all()
							->as_array('email', 'name');

						// If there is QA admins
						if (count($qa_emails) > 0)
						{
							// Send email to QA admins
							Email::factory(strtr(':hotel_name - Deleted Cancellation Policy', array(
									':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
								)), strtr(
								"Action: Deleted Cancellation Policy.\n"
								."By: :admin_username (:admin_name).\n"
								."Room Name: :room_name.\n"
								."Date: :start_date - :end_date.\n"
								."Cancellation Policy: :cancellation_policy.",
								array(
									':admin_username' => A1::instance()->get_user()->username,
									':admin_name' => A1::instance()->get_user()->name,
									':room_name' => ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name,
									':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
									':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
									':cancellation_policy' => implode('. ', array_filter(array(
										$cancellation->rule_1_name,
										$cancellation->rule_2_name,
										$cancellation->rule_3_name,
									)))
								)))
								->from(Kohana::config('application.email'), Kohana::config('application.name'))
								->to($qa_emails)
								->send();
						}
					}
			
					// Delete cancellation
					$cancellation->delete();
				}
			}
		}
    
    $rooms = ORM::factory('room')
      ->select(array('room_texts.name', 'name'))
      ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
      ->where('hotel_id', '=', $this->selected_hotel->id)
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->order_by('room_texts.name')
      ->find_all();
    
    foreach ($rooms as $room)
    {
      $cancellation_available = ORM::factory('cancellation')
        ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
        ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
        ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
        ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
        ->where('campaigns.is_default', '=', 1)
        ->where('rooms.id', '=', $room->id)
        ->count_all() > 0;
      
      if ( ! $cancellation_available)
      {
        Notice::add(Notice::WARNING, Kohana::message('cancellation', 'room_cancellation_not_exist'), array(':room_name' => $room->name));
      }
    }
    
    $cancellations = ORM::factory('cancellation')
      ->select(
        array('rule_1.name', 'rule_1_name'),
        array('rule_2.name', 'rule_2_name'),
        array('rule_3.name', 'rule_3_name')
      )
      ->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'cancellations.level_1_cancellation_rule_id')
      ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'cancellations.level_2_cancellation_rule_id')
      ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'cancellations.level_3_cancellation_rule_id')
      ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where('campaigns.is_default', '=', 1)
      ->order_by('cancellations.start_date', 'ASC')
      ->group_by('cancellations.id')
      ->find_all();
		
		$this->template->main = Kostache::factory('cancellation/manage')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('selected_hotel', $this->selected_hotel)
      ->set('selected_language', $this->selected_language)
			->set('cancellations', $cancellations);
	}
	
	public function action_add()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('cancellation', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage rooms
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'cancellation', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
      // Start transaction
      Database::instance()->begin();
      
			try
			{
        // Get values from post
				$values = $this->request->post();

        $start_date = $this->request->post('start_date');
        $end_date = $this->request->post('end_date');
        $room_ids = Arr::get($this->request->post(), 'room_ids', array());
        
        if (strtotime($start_date) > strtotime($end_date))
          throw new Kohana_Exception(Kohana::message('cancellation', 'date_overlap'));
        
        if (count($room_ids) == 0)
          throw new Kohana_Exception(Kohana::message('cancellation', 'no_room_selected'));
        
        if ( ! Model_Cancellation::valid_cancellation_date($start_date, $end_date, $room_ids))
          throw new Kohana_Exception(Kohana::message('cancellation', 'cancellation_overlap'));
        
        // Create cancellation policy
        $cancellation = ORM::factory('cancellation')
          ->create_cancellation($values);
        
        foreach ($room_ids as $room_id)
        {
          // Get room
          $room = ORM::factory('room')
            ->where('id', '=', $room_id)
            ->find();
          
          // Is Authorized ?
          if ( ! A2::instance()->allowed($room, 'edit'))
          {
            throw new Kohana_Exception(Kohana::message('general', 'not_authorized'));
          }
          
          // Get room default campaigns
          $campaigns = ORM::factory('campaign')
            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
            ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
            ->where('campaigns.is_default', '=', 1)
            ->where('rooms.id', '=', $room->id)
            ->find_all();

          // Link campaign with cancellation policy
          foreach ($campaigns as $campaign)
          {
            $campaign->add('cancellations', $cancellation);
          }
          
					// Get cancellation once again to refresh rule names
          $cancellation = ORM::factory('cancellation')
            ->select(
              array('rules_1.name', 'rule_1_name'),
              array('rules_2.name', 'rule_2_name'),
              array('rules_3.name', 'rule_3_name')
            )
            ->join(array('cancellation_rules', 'rules_1'), 'left')->on('level_1_cancellation_rule_id', '=', 'rules_1.id')
            ->join(array('cancellation_rules', 'rules_2'), 'left')->on('level_2_cancellation_rule_id', '=', 'rules_2.id')
            ->join(array('cancellation_rules', 'rules_3'), 'left')->on('level_3_cancellation_rule_id', '=', 'rules_3.id')
            ->where('cancellations.id', '=', $cancellation->id)
            ->find();
          
					// Write log
          ORM::factory('log')
            ->values(array(
              'admin_id' => A1::instance()->get_user()->id,
              'hotel_id' => $room->hotel_id,
              'action' => 'Added Cancellation Policy',
              'room_texts' => serialize(DB::select()
                ->from('room_texts')
                ->where('room_id', '=', $room->id)
                ->execute()
                ->as_array('language_id')),
              'text' => 'Date: :start_date - :end_date. Cancellation Policy: :cancellation_policy.',
              'data' => serialize(array(
                ':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
                ':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
                ':cancellation_policy' => implode('. ', array_filter(array(
                  $cancellation->rule_1_name,
                  $cancellation->rule_2_name,
                  $cancellation->rule_3_name,
                ))),
              )),
              'created' => time(),
            ))
            ->create();
					
					// Get QA admins emails
					$qa_emails = ORM::factory('admin')
						->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
						->where('admins.role', '=', 'quality-assurance')
						->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
						->find_all()
						->as_array('email', 'name');

					// If there is QA admins
					if (count($qa_emails) > 0)
					{
						// Send email to QA admins
						Email::factory(strtr(':hotel_name - Added Cancellation Policy', array(
								':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
							)), strtr(
							"Action: Added Cancellation Policy.\n"
							."By: :admin_username (:admin_name).\n"
							."Room Name: :room_name.\n"
							."Date: :start_date - :end_date.\n"
							."Cancellation Policy: :cancellation_policy.",
							array(
								':admin_username' => A1::instance()->get_user()->username,
								':admin_name' => A1::instance()->get_user()->name,
								':room_name' => ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name,
								':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
								':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
								':cancellation_policy' => implode('. ', array_filter(array(
									$cancellation->rule_1_name,
									$cancellation->rule_2_name,
									$cancellation->rule_3_name,
								)))
							)))
							->from(Kohana::config('application.email'), Kohana::config('application.name'))
							->to($qa_emails)
							->send();
						}
        }
				
        // Commit transaction
        Database::instance()->commit();
				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'cancellation', 'action' => 'add')), __('Add another cancellation policy'))));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'cancellation', 'action' => 'edit', 'id' => $cancellation->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('room'));
			}
			catch (Exception $e)
			{
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
    
    $rooms = ORM::factory('room')
			->select(
				'rooms.*',
				array('room_texts.name', 'name')
			)
			->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
			->where('rooms.hotel_id', '=', $this->selected_hotel->id)
			->where('room_texts.language_id', '=', $this->selected_language->id)
			->order_by('rooms.publish_price', 'ASC')
			->find_all();
    
    $cancellation_level_1_rules = ORM::factory('cancellation_rule')
      ->where('level', '=', 1)
      ->find_all();
    
    $cancellation_level_2_rules = ORM::factory('cancellation_rule')
      ->where('level', '=', 2)
      ->find_all();
    
    $cancellation_level_3_rules = ORM::factory('cancellation_rule')
      ->where('level', '=', 2)
      ->find_all();
		
		$this->template->main = Kostache::factory('cancellation/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
      ->set('rooms', $rooms)
      ->set('cancellation_level_1_rules', $cancellation_level_1_rules)
      ->set('cancellation_level_2_rules', $cancellation_level_2_rules)
      ->set('cancellation_level_3_rules', $cancellation_level_3_rules);
	}
	
	public function action_edit()
	{
		// Get cancellation id
		$cancellation_id = (int) $this->request->param('id');
		
		// Get cancellation
		$cancellation = ORM::factory('cancellation')
			->where('id', '=', $cancellation_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed($cancellation, 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage rooms
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'cancellation', 'action' => 'manage')));
		}
		
		// If submit button clicked
		if ($this->request->post('submit'))
		{
      // Start transaction
      Database::instance()->begin();
      
			try
			{
				// Get values from post
				$values = $this->request->post();
        
        $start_date = $this->request->post('start_date');
        $end_date = $this->request->post('end_date');
        $room_ids = Arr::get($this->request->post(), 'room_ids', array());
        
        if (strtotime($start_date) > strtotime($end_date))
          throw new Kohana_Exception(Kohana::message('cancellation', 'date_overlap'));
        
        if (count($room_ids) == 0)
          throw new Kohana_Exception(Kohana::message('cancellation', 'no_room_selected'));
        
        if ( ! Model_Cancellation::valid_cancellation_date($start_date, $end_date, $room_ids, $cancellation_id))
          throw new Kohana_Exception(Kohana::message('cancellation', 'cancellation_overlap'));
        
        // Find cancellation policy
        $cancellation = ORM::factory('cancellation')
          ->select(
            array('rules_1.name', 'rule_1_name'),
            array('rules_2.name', 'rule_2_name'),
            array('rules_3.name', 'rule_3_name')
          )
          ->join(array('cancellation_rules', 'rules_1'), 'left')->on('level_1_cancellation_rule_id', '=', 'rules_1.id')
          ->join(array('cancellation_rules', 'rules_2'), 'left')->on('level_2_cancellation_rule_id', '=', 'rules_2.id')
          ->join(array('cancellation_rules', 'rules_3'), 'left')->on('level_3_cancellation_rule_id', '=', 'rules_3.id')
          ->where('cancellations.id', '=', $cancellation_id)
          ->find();
        
        // Clone cancellation for writing log
        $prev_cancellation = clone $cancellation;
        
        // Update cancellation policy
        $cancellation->update_cancellation($values);
        
        // Get cancellation again to refresh rules value
        $cancellation = ORM::factory('cancellation')
          ->select(
            array('rules_1.name', 'rule_1_name'),
            array('rules_2.name', 'rule_2_name'),
            array('rules_3.name', 'rule_3_name')
          )
          ->join(array('cancellation_rules', 'rules_1'), 'left')->on('level_1_cancellation_rule_id', '=', 'rules_1.id')
          ->join(array('cancellation_rules', 'rules_2'), 'left')->on('level_2_cancellation_rule_id', '=', 'rules_2.id')
          ->join(array('cancellation_rules', 'rules_3'), 'left')->on('level_3_cancellation_rule_id', '=', 'rules_3.id')
          ->where('cancellations.id', '=', $cancellation_id)
          ->find();
        
        // Delete campaign cancellation policy link
        DB::delete('campaigns_cancellations')
          ->where('cancellation_id', '=', $cancellation->id)
          ->execute();
        
        foreach ($room_ids as $room_id)
        {
          // Get room
          $room = ORM::factory('room')
            ->where('id', '=', $room_id)
            ->find();
          
          // Is Authorized ?
          if ( ! A2::instance()->allowed($room, 'edit'))
          {
            throw new Kohana_Exception(Kohana::message('general', 'not_authorized'));
          }
          
          // Get room default campaign
          $campaigns = ORM::factory('campaign')
            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
            ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
            ->where('campaigns.is_default', '=', 1)
            ->where('rooms.id', '=', $room->id)
            ->find_all();
          
          // Link campaign with cancellation policy
          foreach ($campaigns as $campaign)
          {
            $campaign->add('cancellations', $cancellation);
          }
          
          // Write log
          ORM::factory('log')
            ->values(array(
              'admin_id' => A1::instance()->get_user()->id,
              'hotel_id' => $room->hotel_id,
              'action' => 'Edited Cancellation Policy',
              'room_texts' => serialize(DB::select()
                ->from('room_texts')
                ->where('room_id', '=', $room->id)
                ->execute()
                ->as_array('language_id')),
              'text' => 'Date: :prev_start_date - :prev_end_date -> :start_date - :end_date. Cancellation Policy: :prev_cancellation_policy -> :cancellation_policy.',
              'data' => serialize(array(
                ':prev_start_date' => date(__('M j, Y'), strtotime($prev_cancellation->start_date)),
                ':prev_end_date' => date(__('M j, Y'), strtotime($prev_cancellation->end_date)),
                ':prev_cancellation_policy' => implode('. ', array_filter(array(
                  $prev_cancellation->rule_1_name,
                  $prev_cancellation->rule_2_name,
                  $prev_cancellation->rule_3_name,
                ))),
                
                ':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
                ':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
                ':cancellation_policy' => implode('. ', array_filter(array(
                  $cancellation->rule_1_name,
                  $cancellation->rule_2_name,
                  $cancellation->rule_3_name,
                ))),
              )),
              'created' => time(),
            ))
            ->create();
					
					// Get QA admins emails
					$qa_emails = ORM::factory('admin')
						->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
						->where('admins.role', '=', 'quality-assurance')
						->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
						->find_all()
						->as_array('email', 'name');

					// If there is QA admins
					if (count($qa_emails) > 0)
					{
						// Send email to QA admins
						Email::factory(strtr(':hotel_name - Edited Cancellation Policy', array(
								':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
							)), strtr(
							"Action: Edited Cancellation Policy.\n"
							."By: :admin_username (:admin_name).\n"
							."Room Name: :room_name.\n"
							."Date: :prev_start_date - :prev_end_date -> :start_date - :end_date.\n"
							."Cancellation Policy: :prev_cancellation_policy -> :cancellation_policy.",
							array(
								':admin_username' => A1::instance()->get_user()->username,
								':admin_name' => A1::instance()->get_user()->name,
								':room_name' => ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name,
								':prev_start_date' => date(__('M j, Y'), strtotime($prev_cancellation->start_date)),
								':prev_end_date' => date(__('M j, Y'), strtotime($prev_cancellation->end_date)),
								':prev_cancellation_policy' => implode('. ', array_filter(array(
									$prev_cancellation->rule_1_name,
									$prev_cancellation->rule_2_name,
									$prev_cancellation->rule_3_name,
								))),

								':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
								':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
								':cancellation_policy' => implode('. ', array_filter(array(
									$cancellation->rule_1_name,
									$cancellation->rule_2_name,
									$cancellation->rule_3_name,
								)))
							)))
							->from(Kohana::config('application.email'), Kohana::config('application.name'))
							->to($qa_emails)
							->send();
					}
        }
				
        // Commit transaction
        Database::instance()->commit();
				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('room'));
			}
			catch (Exception $e)
			{
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
    
    $rooms = ORM::factory('room')
			->select(
				'rooms.*',
				array('room_texts.name', 'name')
			)
			->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
			->where('rooms.hotel_id', '=', $this->selected_hotel->id)
			->where('room_texts.language_id', '=', $this->selected_language->id)
			->order_by('rooms.publish_price', 'ASC')
			->find_all();
    
    $cancellation_level_1_rules = ORM::factory('cancellation_rule')
      ->where('level', '=', 1)
      ->find_all();
    
    $cancellation_level_2_rules = ORM::factory('cancellation_rule')
      ->where('level', '=', 2)
      ->find_all();
    
    $cancellation_level_3_rules = ORM::factory('cancellation_rule')
      ->where('level', '=', 2)
      ->find_all();
		
		$this->template->main = Kostache::factory('cancellation/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
      ->set('cancellation', $cancellation)
      ->set('rooms', $rooms)
      ->set('cancellation_level_1_rules', $cancellation_level_1_rules)
      ->set('cancellation_level_2_rules', $cancellation_level_2_rules)
      ->set('cancellation_level_3_rules', $cancellation_level_3_rules);
	}
	
	public function action_delete()
	{
		// Get cancellation id
		$cancellation_id = $this->request->param('id');
		
		$cancellation = ORM::factory('cancellation')
			->where('id', '=', $cancellation_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed($cancellation, 'delete'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage rooms
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'cancellation', 'action' => 'manage')));
		}
		
		try
		{
			// Get cancellation again to refresh rules value
			$cancellation = ORM::factory('cancellation')
				->select(
					array('rules_1.name', 'rule_1_name'),
					array('rules_2.name', 'rule_2_name'),
					array('rules_3.name', 'rule_3_name')
				)
				->join(array('cancellation_rules', 'rules_1'), 'left')->on('level_1_cancellation_rule_id', '=', 'rules_1.id')
				->join(array('cancellation_rules', 'rules_2'), 'left')->on('level_2_cancellation_rule_id', '=', 'rules_2.id')
				->join(array('cancellation_rules', 'rules_3'), 'left')->on('level_3_cancellation_rule_id', '=', 'rules_3.id')
				->where('cancellations.id', '=', $cancellation_id)
				->find();
			
			// Get rooms
			$rooms = ORM::factory('room')
				->join('campaigns_rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
				->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
				->join('campaigns_cancellations')->on('campaigns_cancellations.campaign_id', '=', 'campaigns.id')
				->where('campaigns.is_default', '=', 1)
				->where('campaigns_cancellations.cancellation_id', '=', $cancellation->id)
				->find_all();
			
			foreach ($rooms as $room)
			{
				// Write log
				ORM::factory('log')
					->values(array(
						'admin_id' => A1::instance()->get_user()->id,
						'hotel_id' => $room->hotel_id,
						'action' => 'Deleted Cancellation Policy',
						'room_texts' => serialize(DB::select()
							->from('room_texts')
							->where('room_id', '=', $room->id)
							->execute()
							->as_array('language_id')),
						'text' => 'Date: :start_date - :end_date. '
							.'Cancellation Policy: :cancellation_policy.',
						'data' => serialize(array(
							':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
							':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
							':cancellation_policy' => implode('. ', array_filter(array(
								$cancellation->rule_1_name,
								$cancellation->rule_2_name,
								$cancellation->rule_3_name,
							))))),
						'created' => time(),
					))
					->create();
				
				// Get QA admins emails
				$qa_emails = ORM::factory('admin')
					->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
					->where('admins.role', '=', 'quality-assurance')
					->where('admins_hotels.hotel_id', '=', $room->hotel_id)
					->find_all()
					->as_array('email', 'name');

				// If there is QA admins
				if (count($qa_emails) > 0)
				{
					// Send email to QA admins
					Email::factory(strtr(':hotel_name - Deleted Cancellation Policy', array(
							':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $room->hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
						)), strtr(
						"Action: Deleted Cancellation Policy.\n"
						."By: :admin_username (:admin_name).\n"
						."Room Name: :room_name.\n"
						."Date: :start_date - :end_date.\n"
						."Cancellation Policy: :cancellation_policy.",
						array(
							':admin_username' => A1::instance()->get_user()->username,
							':admin_name' => A1::instance()->get_user()->name,
							':room_name' => ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name,
							':start_date' => date(__('M j, Y'), strtotime($cancellation->start_date)),
							':end_date' => date(__('M j, Y'), strtotime($cancellation->end_date)),
							':cancellation_policy' => implode('. ', array_filter(array(
								$cancellation->rule_1_name,
								$cancellation->rule_2_name,
								$cancellation->rule_3_name,
							)))
						)))
						->from(Kohana::config('application.email'), Kohana::config('application.name'))
						->to($qa_emails)
						->send();
				}
			}
				
			// Delete cancellation
			$cancellation->delete();

			// Add success notice
			Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
		}
		catch (Exception $e)
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}