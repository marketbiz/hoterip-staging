<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Search extends Controller_Layout_Admin {
    
  public function action_promotion()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('promotion_search', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}

    /*
     * Get filters
     */
    $districts_query = $this->request->query('district');
    $cities = $this->request->query('city');
    $promotion_type = $this->request->query('type');
    $benefit_of_price = $this->request->query('benefits');
    $benefit_of_others = $this->request->query('other_benefit');
    $booking_start_date = $this->request->query('booking_start_date');
    $booking_end_date = $this->request->query('booking_end_date');
    $stay_start_date = $this->request->query('stay_start_date');
    $stay_end_date = $this->request->query('stay_end_date');

    //District
    $districts = ORM::factory('district')
      ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
      ->where('district_texts.language_id', '=', $this->selected_language->id)
      ->order_by('districts.id', 'ASC');

    if($districts_query){
        $districts = $districts->where('districts.city_id','=',$cities);
    }

    //Other Benefits
    $other_benefits = DB::select(
      array('benefits.id', 'benefit_id'),
      array('benefit_texts.name', 'benefit_name')
      )
      ->from('benefits')
      ->join('benefit_texts')->on('benefit_texts.benefit_id', '=', 'benefits.id')
      ->where('.benefit_texts.language_id', '=', $this->selected_language->id)
      ->execute()
      ->as_array();

    //Pagination
    $pagination = '';
    $total_hotels = 0;
    $page = Arr::get($this->request->query(), 'page', 1);
    $items_per_page = 75;
    $offset = ($page - 1) * $items_per_page;

    // Get logged in admin
    $logged_in_admin = A1::instance()->get_user();

    // If ajax request
    if ($this->request->is_ajax())
    {
      $districts = $districts->where('districts.city_id','=',$_POST['idcity'])
                  ->find_all();
        echo '<option value="0">All District</option>';
      foreach ($districts as $key => $district) {
        echo '<option value='.$district->id.'>'.ucfirst($district->url_segment).'</option>';
        }  
      exit;
    }
    /*
     * Form submitted
     */
    elseif ($this->request->query('search'))
    {
      // Get all hotels
      $hotels = ORM::factory('hotel')
        ->select(
          array('hotels.id', 'hotel_id'),
          array('hotel_texts.name', 'name'),
          array('campaigns.id', 'campaign_id'),
          array('campaigns.booking_start_date', 'booking_start_date'),
          array('campaigns.booking_end_date', 'booking_end_date'),
          array('campaigns.stay_start_date', 'stay_start_date'),
          array('campaigns.stay_end_date', 'stay_end_date'),
          array('campaigns.discount_rate', 'discount_rate'),
          array('campaigns.discount_amount', 'discount_amount'),
          array('campaigns.number_of_free_nights', 'number_of_free_nights'),
          array('campaigns.minimum_number_of_nights', 'minimum_number_of_nights'),
          array('campaigns.minimum_number_of_rooms', 'minimum_number_of_rooms'),
          array('campaigns.minimum_number_of_rooms', 'minimum_number_of_rooms'),
          array('campaigns.days_in_advance', 'days_in_advance'),
          array('campaigns.within_days_of_arrival', 'within_days_of_arrival'),
          array('campaigns.type', 'type'),
          array('campaigns_benefits.benefit_id', 'benefit_id'),
          array('rooms.id', 'room_id'),
          array('room_texts.name', 'room_name')
        )
        ->from('hotels')
        ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
        ->join('cities')->on('cities.id', '=', 'hotels.city_id')
        ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
        ->join('districts')->on('districts.id', '=', 'hotels.district_id')
        ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
        ->join('countries')->on('countries.id', '=', 'cities.country_id')
        ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
        ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
        ->join('room_texts')->on('rooms.id', '=', 'room_texts.room_id')
        ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
        ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
        ->join('campaigns_benefits', 'left')->on('campaigns.id', '=' ,'campaigns_benefits.campaign_id')
        ->where('hotel_texts.language_id', '=', $this->selected_language->id)
        ->where('district_texts.language_id', '=', $this->selected_language->id)
        ->where('city_texts.language_id', '=', $this->selected_language->id)
        ->where('country_texts.language_id', '=', $this->selected_language->id)
        ->where('room_texts.language_id', '=', $this->selected_language->id)
        ->where('campaigns.booking_start_date', '!=', '0000-01-01')
        ->where('campaigns.booking_end_date', '!=', '9999-12-31')
        ->where('campaigns.stay_start_date', '!=', '0000-01-01')
        ->where('campaigns.stay_end_date', '!=', '9999-12-31')
        ->order_by('campaigns.id', 'DESC');

      if ($districts_query)
      {
        $hotels->where('districts.id', '=', $districts_query);
      }

      if ($cities)
      {
        $hotels->where('cities.id', '=', $cities);
      }

      if ($promotion_type)
      {
        $hotels->where('campaigns.type', '=', $promotion_type);
      }

      if ($benefit_of_price)
      {
        $hotels
          ->where_open()
          ->where('campaigns.discount_rate', '>=', $benefit_of_price)
          ->and_where('campaigns.discount_rate', '<', 100)
          ->where_close();
      }

      if ($benefit_of_others)
      {
        $hotels->where('campaigns_benefits.benefit_id', '=', $benefit_of_others);
      }

      if ($booking_start_date AND $booking_end_date)
      {
        $booking_start_date = date('Y-m-d', strtotime($booking_start_date));
        $booking_end_date = date('Y-m-d', strtotime($booking_end_date));
        
        $hotels
          ->where_open()
          ->where('campaigns.booking_start_date', '<=', $booking_start_date)
          ->and_where('campaigns.booking_end_date', '>=', $booking_end_date)
          ->where_close()
          ;
      }

      if ($stay_start_date AND $stay_end_date)
      {
        $stay_start_date = date('Y-m-d', strtotime($stay_start_date));
        $stay_end_date = date('Y-m-d', strtotime($stay_end_date));
        
        $hotels
          ->where_open()
          ->where('campaigns.stay_start_date', '<=', $stay_start_date)
          ->and_where('campaigns.stay_end_date', '>=', $stay_end_date)
          ->where_close()
          ;
      }

      // Get total users
      $total_hotels = $hotels
        ->reset(FALSE)
        ->count_all();

      $hotels = $hotels
        ->group_by('hotel_id')
        ->group_by('campaign_id')
        ->group_by('room_id')
        ->offset($offset)
        ->limit($items_per_page)
        ->find_all();

      $promotions = array();

      foreach ($hotels as $key => $hotel)
      {
        /*
         * Search Promotions additional benefits text
         */ 
        // Get Additional benefit
        $other_benefits_list = DB::select(
          'campaigns_benefits.*',
          array('benefit_texts.name', 'name')
        )
        ->from('campaigns_benefits')
        ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
        ->where('benefit_texts.language_id', '=', $this->selected_language->id)
        ->where('campaigns_benefits.campaign_id', '=', $hotel->campaign_id)
        ->group_by('campaigns_benefits.benefit_id')
        ->execute();

        $benefit_check = array();
        if(!empty($other_benefits_list))
        {
          foreach ($other_benefits_list as $key => $other_benefit)
          {
            $benefit_check[] = $other_benefit['name'];
          }

          $benefit_check = implode(', ', $benefit_check).'.';
          $benefit_check = $benefit_check != '.' ? $benefit_check : '-';
        }

        // Benefit
        $benefit = '';
        
        if ($hotel->discount_rate > 0)
        {
          $benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
            ':minimum_number_of_nights' => $hotel->minimum_number_of_nights,
            ':minimum_number_of_rooms' => $hotel->minimum_number_of_rooms,
            ':discount_rate' => $hotel->discount_rate,
          ));
        }
        elseif ($hotel->discount_amount > 0)
        {
          $benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
            ':minimum_number_of_nights' => $hotel->minimum_number_of_nights,
            ':minimum_number_of_rooms' => $hotel->minimum_number_of_rooms,
            ':discount_amount' => $hotel->discount_amount,
          ));
        }
        elseif ($hotel->number_of_free_nights)
        {
          $benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
            ':minimum_number_of_nights' => $hotel->minimum_number_of_nights,
            ':minimum_number_of_rooms' => $hotel->minimum_number_of_rooms,
            ':number_of_free_nights' => $hotel->number_of_free_nights,
          ));
        }

        $booking_period = date('M j,Y', strtotime($hotel->booking_start_date)).' - '.date('M d,Y', strtotime($hotel->booking_end_date));
        $stay_period = date('M j,Y', strtotime($hotel->stay_start_date)).' - '.date('M d,Y', strtotime($hotel->stay_end_date));

        if ($hotel->type == 2)
        {
          $benefit .= ' '.strtr('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $hotel->days_in_advance));
        }
        elseif ($hotel->type == 3)
        {
          $benefit .= ' '.strtr('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $hotel->within_days_of_arrival));
        }
        elseif ($hotel->type == 4)
        {
          $benefit .= ' Non refundable.';
        }
        elseif ($hotel->type == 5)
        {
          $benefit .= ' Flash Deal.';
        }

        $promotions[] = array(
          'hotel_name' => $hotel->name,
          'room_name' => $hotel->room_name,
          'campaign_id' => $hotel->campaign_id,
          'promotion_name' => $benefit,
          'booking_period' => $booking_period,
          'additional_benefit' => $benefit_check,
          'stay_period' => $stay_period,
          );
      }

      // Create pagination
      $pagination = Pagination::factory(array(
        'items_per_page' => $items_per_page,
        'total_items' => $total_hotels,
      ));
    }
    else
    {
      $promotions = array();
    }

    if($cities)
    {
      $districts = $districts->where('city_id', '=', $cities);
    }

    $districts = $districts->find_all();

    //Cities
    $cities = ORM::factory('city')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->order_by('cities.id', 'ASC')
      ->find_all();

		$this->template->main = Kostache::factory('search/promotion')
			->set('notice', Notice::render())
			->set('filters', $this->request->query())
      ->set('logged_in_admin', $logged_in_admin)
      ->set('promotions', $promotions)
      ->set('districts', $districts)
      ->set('cities', $cities)
      ->set('other_benefits', $other_benefits)
      ->set('total_promotions', count($promotions))
      ->set('pagination', $pagination);;
  }
  
}