<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Room extends Controller_Layout_Admin {

    public function before() {
        if ($this->request->action() == 'delete') {
            $this->auto_render = FALSE;
        }

        parent::before();
    }

    public function action_manage() {
        // Is Authorized ?
        if (!A2::instance()->allowed('room', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        if ($this->request->post('delete_checked')) {
            $room_ids = Arr::get($this->request->post(), 'ids', array());

            foreach ($room_ids as $room_id) {
                // Get room
                $room = ORM::factory('room')
                        ->where('id', '=', $room_id)
                        ->find();

                // If is allowed
                if (A2::instance()->allowed($room, 'delete')) {
                    $this->action_delete($room_id);
                }
            }
        }

        $rooms = ORM::factory('room')
                ->select(
                        'rooms.*', 'room_texts.name'
                )
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
                ->order_by('rooms.publish_price', 'ASC')
                ->find_all();

        $currency = ORM::factory('currency')
                ->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        $this->template->main = Kostache::factory('room/manage')
                ->set('notice', Notice::render())
                ->set('rooms', $rooms)
                ->set('currency', $currency);
    }

    public function action_add() {
        // Is Authorized ?
        if (!A2::instance()->allowed('room', 'add')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage rooms
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
        }

        if ($this->request->post('submit')) {
            // Start transaction
            Database::instance()->begin();

            try {
                $values = $this->request->post();
                $values['hotel_id'] = $this->selected_hotel->id;
                $values['is_breakfast_included'] = Arr::get($this->request->post(), 'is_breakfast_included') ? 1 : 0;
                $values['is_extrabed_get_breakfast'] = Arr::get($this->request->post(), 'is_extrabed_get_breakfast') ? 1 : 0;
                $values['hoterip_campaign'] = Arr::get($this->request->post(), 'hoterip_campaign') ? 1 : 0;

                // Create room
                $room = ORM::factory('room')
                        ->create_room($values);

                // Create default campaign for room
                $campaign = ORM::factory('campaign')->create_campaign(array(
                    'is_show' => 1,
                    'is_default' => 1,
                    'type' => 1,
                    'booking_start_date' => '0000-01-01',
                    'booking_end_date' => '9999-12-31',
                    'stay_start_date' => '0000-01-01',
                    'stay_end_date' => '9999-12-31',
                    'is_stay_sunday_active' => 1,
                    'is_stay_monday_active' => 1,
                    'is_stay_tuesday_active' => 1,
                    'is_stay_wednesday_active' => 1,
                    'is_stay_thursday_active' => 1,
                    'is_stay_friday_active' => 1,
                    'is_stay_saturday_active' => 1,
                    'discount_rate' => 0,
                    'discount_amount' => 0,
                    'minimum_number_of_nights' => 1,
                    'minimum_number_of_rooms' => 1,
                    'number_of_free_nights' => 0,
                    'days_in_advance' => 0,
                    'within_days_of_arrival' => 0,
                    'stock_limit' => NULL,
                ));

                // Link campaign to room
                $campaign->add('rooms', $room);

                /* Show campaign to all languages */

                // Get all languages
                $languages = ORM::factory('language')
                        ->find_all();

                foreach ($languages as $language) {
                    $campaign->add('languages', $language);
                }

                // Get room capacities for writing log
                $room_capacities = ORM::factory('room_capacity')
                        ->where('room_id', '=', $room->id)
                        ->find_all();

                $room_capacities_strings = array();
                foreach ($room_capacities as $room_capacity) {
                    $build_string = '';
                    $build_string .= $room_capacity->number_of_adults > 0 ? $room_capacity->number_of_adults . 'A ' : '';
                    $build_string .= $room_capacity->number_of_children > 0 ? $room_capacity->number_of_children . 'C ' : '';
                    $build_string .= $room_capacity->number_of_extrabeds > 0 ? $room_capacity->number_of_extrabeds . 'E ' : '';
                    $build_string = trim($build_string);
                    $room_capacities_strings[] = $build_string;
                }
                $room_capacities_string = implode(', ', $room_capacities_strings);

                // Write Log
                ORM::factory('log')
                        ->values(array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $room->hotel_id,
                            'action' => 'Added Room',
                            'room_texts' => serialize(DB::select()
                                    ->from('room_texts')
                                    ->where('room_id', '=', $room->id)
                                    ->execute()
                                    ->as_array('language_id')),
                            'text' => 'Room Size: :size. '
                            . 'Number of Beds: :number_of_beds. '
                            . 'Publish Price: :publish_price. '
                            . 'Adult Breakfast Price: :adult_breakfast_price. '
                            . 'Child Breakfast Price: :child_breakfast_price. '
                            . 'Breakfast Included: :is_breakfast_included. '
                            . 'Number of Person Breakfast Included: :number_of_persons_breakfast_included. '
                            . 'Extra Bed Price: :extrabed_price. '
                            . 'Max Number of Extrabeds: :maximum_number_of_extrabeds. '
                            . 'Extrabed Get Breakfast: :is_extrabed_get_breakfast. '
                            . 'Room Capacities: :room_capacities.',
                            'data' => serialize(array(
                                ':size' => $room->size,
                                ':number_of_beds' => $room->number_of_beds,
                                ':publish_price' => Num::display($room->publish_price),
                                ':adult_breakfast_price' => Num::display($room->adult_breakfast_price),
                                ':child_breakfast_price' => Num::display($room->child_breakfast_price),
                                ':is_breakfast_included' => $room->is_breakfast_included ? 'Yes' : 'No',
                                ':number_of_persons_breakfast_included' => $room->number_of_persons_breakfast_included,
                                ':extrabed_price' => Num::display($room->extrabed_price),
                                ':maximum_number_of_extrabeds' => $room->maximum_number_of_extrabeds,
                                ':is_extrabed_get_breakfast' => $room->is_extrabed_get_breakfast ? 'Yes' : 'No',
                                ':room_capacities' => $room_capacities_string,
                            )),
                            'created' => time(),
                        ))
                        ->create();

                // Get QA admins emails
                $qa_emails = ORM::factory('admin')
                        ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                        ->where('admins.role', '=', 'quality-assurance')
                        ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                        ->find_all()
                        ->as_array('email', 'name');

                // If there is QA admins
                if (count($qa_emails) > 0) {
                    // Send email to QA admins
                    Email::factory(strtr(':hotel_name - Added Room', array(
                                ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                    )), strtr(
                                            "Action: Added Room.\n"
                                            . "By: :admin_username (:admin_name).\n"
                                            . "Room Name: :room_name.\n"
                                            . "Room Size: :size.\n"
                                            . "Number of Beds: :number_of_beds.\n"
                                            . "Publish Price: :publish_price.\n"
                                            . "Adult Breakfast Price: :adult_breakfast_price.\n"
                                            . "Child Breakfast Price: :child_breakfast_price.\n"
                                            . "Breakfast Included: :is_breakfast_included.\n"
                                            . "Number of Person Breakfast Included: :number_of_persons_breakfast_included.\n"
                                            . "Extra Bed Price: :extrabed_price.\n"
                                            . "Max Number of Extrabeds: :maximum_number_of_extrabeds.\n"
                                            . "Extrabed Get Breakfast: :is_extrabed_get_breakfast.\n"
                                            . "Room Capacities: :room_capacities.", array(
                                ':admin_username' => A1::instance()->get_user()->username,
                                ':admin_name' => A1::instance()->get_user()->name,
                                ':room_name' => ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name,
                                ':size' => $room->size,
                                ':number_of_beds' => $room->number_of_beds,
                                ':publish_price' => Num::display($room->publish_price),
                                ':adult_breakfast_price' => Num::display($room->adult_breakfast_price),
                                ':child_breakfast_price' => Num::display($room->child_breakfast_price),
                                ':is_breakfast_included' => $room->is_breakfast_included ? 'Yes' : 'No',
                                ':number_of_persons_breakfast_included' => $room->number_of_persons_breakfast_included,
                                ':extrabed_price' => Num::display($room->extrabed_price),
                                ':maximum_number_of_extrabeds' => $room->maximum_number_of_extrabeds,
                                ':is_extrabed_get_breakfast' => $room->is_extrabed_get_breakfast ? 'Yes' : 'No',
                                ':room_capacities' => $room_capacities_string,
                            )))
                            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                            ->to($qa_emails)
                            ->send();
                }

                // Commit transaction
                Database::instance()->commit();

                //vacation_triiger
                $vacation_triger = array(
                    'hotel_id' => $this->selected_hotel->id,
                    'room_id' => $room->id,
                    'campaign_id' => NULL,
                    // 'start_date' =>$booking_data['check_in'], 
                    // 'end_date' =>$booking_data['check_out'],
                    'item_id' => NULL,
                    'start_date' => NULL,
                    'end_date' => NULL,
                    'type_trigger' => 4,
                    'status' => 0
                );

                Controller_Vacation_Trigger::insertData($vacation_triger);

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'room', 'action' => 'add')), __('Add another room'))));
                // Redirect to edit
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'edit', 'id' => $room->id)));
            } catch (ORM_Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('room'));
            } catch (Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        $hotel_photos = ORM::factory('hotel_photo')->select(array('hotel_photo_texts.name', 'name'))
                ->join('hotel_photo_texts')->on('hotel_photo_texts.hotel_photo_id', '=', 'hotel_photos.id')
                ->where('hotel_photos.hotel_id', '=', $this->selected_hotel->id)
                ->where('hotel_photo_texts.language_id', '=', $this->selected_language->id)
                ->order_by('hotel_photo_texts.name')
                ->find_all();

        $languages = ORM::factory('language')->find_all();

        $currency = ORM::factory('currency')->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        $this->template->main = Kostache::factory('room/add')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                ->set('selected_hotel', $this->selected_hotel)
                ->set('hotel_photos', $hotel_photos)
                ->set('languages', $languages)
                ->set('currency', $currency);
    }

    public function action_edit() {
        // Get room id
        $room_id = (int) $this->request->param('id');

        // Get room
        $room = ORM::factory('room')
                ->where('id', '=', $room_id)
                ->find();

        // Is Authorized ?
        if (!A2::instance()->allowed($room, 'edit')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage rooms
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
        }

        // If submit button clicked
        if ($this->request->post('submit')) {
            // Start transaction
            Database::instance()->begin();

            try {
                $values = $this->request->post();
                $values['is_breakfast_included'] = Arr::get($this->request->post(), 'is_breakfast_included') ? 1 : 0;
                $values['is_extrabed_get_breakfast'] = Arr::get($this->request->post(), 'is_extrabed_get_breakfast') ? 1 : 0;
                $values['hoterip_campaign'] = Arr::get($this->request->post(), 'hoterip_campaign') ? 1 : 0;

                // Check if hoterip campaign net is exist
                if (!$values['hoterip_campaign']) {
                    $net_price['net_price'] = NULL;
                    DB::update('items')
                            ->set($net_price)
                            ->where('room_id', '=', $room_id)
                            ->where('date', '>=', date('Y-m-d'))
                            ->execute();
                }

                // Find room
                $room = ORM::factory('room')
                        ->where('id', '=', $room_id)
                        ->find();

                // Clone room for writing log
                $prev_room = clone $room;

                // Get room capacities for writing log
                $prev_room_capacities = ORM::factory('room_capacity')
                        ->where('room_id', '=', $prev_room->id)
                        ->find_all();

                $prev_room_capacities_strings = array();
                foreach ($prev_room_capacities as $room_capacity) {
                    $build_string = '';
                    $build_string .= $room_capacity->number_of_adults > 0 ? $room_capacity->number_of_adults . 'A ' : '';
                    $build_string .= $room_capacity->number_of_children > 0 ? $room_capacity->number_of_children . 'C ' : '';
                    $build_string .= $room_capacity->number_of_extrabeds > 0 ? $room_capacity->number_of_extrabeds . 'E ' : '';
                    $build_string = trim($build_string);
                    $prev_room_capacities_strings[] = $build_string;
                }
                $prev_room_capacities_string = implode(', ', $prev_room_capacities_strings);

                // Update room
                $room->update_room($values);

                // Get room capacities for writing log
                $room_capacities = ORM::factory('room_capacity')
                        ->where('room_id', '=', $room->id)
                        ->find_all();

                $room_capacities_strings = array();
                foreach ($room_capacities as $room_capacity) {
                    $build_string = '';
                    $build_string .= $room_capacity->number_of_adults > 0 ? $room_capacity->number_of_adults . 'A ' : '';
                    $build_string .= $room_capacity->number_of_children > 0 ? $room_capacity->number_of_children . 'C ' : '';
                    $build_string .= $room_capacity->number_of_extrabeds > 0 ? $room_capacity->number_of_extrabeds . 'E ' : '';
                    $build_string = trim($build_string);
                    $room_capacities_strings[] = $build_string;
                }
                $room_capacities_string = implode(', ', $room_capacities_strings);

                // Write Log
                ORM::factory('log')
                        ->values(array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $room->hotel_id,
                            'action' => 'Edited Room',
                            'room_texts' => serialize(DB::select()
                                    ->from('room_texts')
                                    ->where('room_id', '=', $room->id)
                                    ->execute()
                                    ->as_array('language_id')),
                            'text' => 'Room Size: :prev_size -> :size. '
                            . 'Number of Beds: :prev_number_of_beds -> :number_of_beds. '
                            . 'Publish Price: :prev_publish_price -> :publish_price. '
                            . 'Adult Breakfast Price: :prev_adult_breakfast_price -> :adult_breakfast_price. '
                            . 'Child Breakfast Price: :prev_child_breakfast_price -> :child_breakfast_price. '
                            . 'Breakfast Included: :prev_is_breakfast_included -> :is_breakfast_included. '
                            . 'Number of Person Breakfast Included: :prev_number_of_persons_breakfast_included -> :number_of_persons_breakfast_included. '
                            . 'Extra Bed Price: :prev_extrabed_price -> :extrabed_price. '
                            . 'Max Number of Extrabeds: :prev_maximum_number_of_extrabeds -> :maximum_number_of_extrabeds. '
                            . 'Extrabed Get Breakfast: :prev_is_extrabed_get_breakfast -> :is_extrabed_get_breakfast. '
                            . 'Room Capacities: :prev_room_capacities -> :room_capacities.',
                            'data' => serialize(array(
                                ':prev_size' => $prev_room->size,
                                ':prev_number_of_beds' => $prev_room->number_of_beds,
                                ':prev_publish_price' => Num::display($prev_room->publish_price),
                                ':prev_adult_breakfast_price' => Num::display($prev_room->adult_breakfast_price),
                                ':prev_child_breakfast_price' => Num::display($prev_room->child_breakfast_price),
                                ':prev_is_breakfast_included' => $prev_room->is_breakfast_included ? 'Yes' : 'No',
                                ':prev_number_of_persons_breakfast_included' => $prev_room->number_of_persons_breakfast_included,
                                ':prev_extrabed_price' => Num::display($prev_room->extrabed_price),
                                ':prev_maximum_number_of_extrabeds' => $prev_room->maximum_number_of_extrabeds,
                                ':prev_is_extrabed_get_breakfast' => $prev_room->is_extrabed_get_breakfast ? 'Yes' : 'No',
                                ':prev_room_capacities' => $prev_room_capacities_string,
                                ':size' => $room->size,
                                ':number_of_beds' => $room->number_of_beds,
                                ':publish_price' => Num::display($room->publish_price),
                                ':adult_breakfast_price' => Num::display($room->adult_breakfast_price),
                                ':child_breakfast_price' => Num::display($room->child_breakfast_price),
                                ':is_breakfast_included' => $room->is_breakfast_included ? 'Yes' : 'No',
                                ':number_of_persons_breakfast_included' => $room->number_of_persons_breakfast_included,
                                ':extrabed_price' => Num::display($room->extrabed_price),
                                ':maximum_number_of_extrabeds' => $room->maximum_number_of_extrabeds,
                                ':is_extrabed_get_breakfast' => $room->is_extrabed_get_breakfast ? 'Yes' : 'No',
                                ':room_capacities' => $room_capacities_string,
                            )),
                            'created' => time(),
                        ))
                        ->create();

                // Get QA admins emails
                $qa_emails = ORM::factory('admin')
                        ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                        ->where('admins.role', '=', 'quality-assurance')
                        ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                        ->find_all()
                        ->as_array('email', 'name');

                // If there is QA admins
                if (count($qa_emails) > 0) {
                    // Send email
                    Email::factory(strtr(':hotel_name - Edited Room', array(
                                ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                    )), strtr(
                                            "Action: Edited Room.\n"
                                            . "By: :admin_username (:admin_name).\n"
                                            . "Room Name: :room_name.\n"
                                            . "Room Size: :prev_size -> :size.\n"
                                            . "Number of Beds: :prev_number_of_beds -> :number_of_beds.\n"
                                            . "Publish Price: :prev_publish_price -> :publish_price.\n"
                                            . "Adult Breakfast Price: :prev_adult_breakfast_price -> :adult_breakfast_price.\n"
                                            . "Child Breakfast Price: :prev_child_breakfast_price -> :child_breakfast_price.\n"
                                            . "Breakfast Included: :prev_is_breakfast_included -> :is_breakfast_included.\n"
                                            . "Number of Person Breakfast Included: :prev_number_of_persons_breakfast_included -> :number_of_persons_breakfast_included.\n"
                                            . "Extra Bed Price: :prev_extrabed_price -> :extrabed_price.\n"
                                            . "Max Number of Extrabeds: :prev_maximum_number_of_extrabeds -> :maximum_number_of_extrabeds.\n"
                                            . "Extrabed Get Breakfast: :prev_is_extrabed_get_breakfast -> :is_extrabed_get_breakfast.\n"
                                            . "Room Capacities: :prev_room_capacities -> :room_capacities.", array(
                                ':admin_username' => A1::instance()->get_user()->username,
                                ':admin_name' => A1::instance()->get_user()->name,
                                ':room_name' => ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name,
                                ':prev_size' => $prev_room->size,
                                ':prev_number_of_beds' => $prev_room->number_of_beds,
                                ':prev_publish_price' => Num::display($prev_room->publish_price),
                                ':prev_adult_breakfast_price' => Num::display($prev_room->adult_breakfast_price),
                                ':prev_child_breakfast_price' => Num::display($prev_room->child_breakfast_price),
                                ':prev_is_breakfast_included' => $prev_room->is_breakfast_included ? 'Yes' : 'No',
                                ':prev_number_of_persons_breakfast_included' => $prev_room->number_of_persons_breakfast_included,
                                ':prev_extrabed_price' => Num::display($prev_room->extrabed_price),
                                ':prev_maximum_number_of_extrabeds' => $prev_room->maximum_number_of_extrabeds,
                                ':prev_is_extrabed_get_breakfast' => $prev_room->is_extrabed_get_breakfast ? 'Yes' : 'No',
                                ':prev_room_capacities' => $prev_room_capacities_string,
                                ':size' => $room->size,
                                ':number_of_beds' => $room->number_of_beds,
                                ':publish_price' => Num::display($room->publish_price),
                                ':adult_breakfast_price' => Num::display($room->adult_breakfast_price),
                                ':child_breakfast_price' => Num::display($room->child_breakfast_price),
                                ':is_breakfast_included' => $room->is_breakfast_included ? 'Yes' : 'No',
                                ':number_of_persons_breakfast_included' => $room->number_of_persons_breakfast_included,
                                ':extrabed_price' => Num::display($room->extrabed_price),
                                ':maximum_number_of_extrabeds' => $room->maximum_number_of_extrabeds,
                                ':is_extrabed_get_breakfast' => $room->is_extrabed_get_breakfast ? 'Yes' : 'No',
                                ':room_capacities' => $room_capacities_string,
                            )))
                            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                            ->to($qa_emails)
                            ->send();
                }

                // Commit transaction
                Database::instance()->commit();

                //vacation trigger
                $vacation_triger = array(
                    'hotel_id' => $this->selected_hotel->id,
                    'room_id' => $room->id,
                    'campaign_id' => NULL,
                    // 'start_date' =>$booking_data['check_in'], 
                    // 'end_date' =>$booking_data['check_out'],
                    'item_id' => NULL,
                    'start_date' => NULL,
                    'end_date' => NULL,
                    'type_trigger' => 4,
                    'status' => 0
                );

                Controller_Vacation_Trigger::insertData($vacation_triger);

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
            } catch (ORM_Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('room'));
            } catch (Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        // Get all languages
        $languages = ORM::factory('language')
                ->find_all();

        // Get selected hotel currency
        $currency = ORM::factory('currency')
                ->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        // Get room
        $room = ORM::factory('room')
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('rooms.id', '=', $room_id)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
                ->find();

        // Get room texts
        $room_texts = ORM::factory('room_text')
                ->where('room_id', '=', $room->id)
                ->find_all()
                ->as_array('language_id');

        // Get room facility
        $room_facility = ORM::factory('room_facility')
                ->where('room_id', '=', $room->id)
                ->find();

        // Get room capacities
        $room_capacities = ORM::factory('room_capacity')
                ->where('room_id', '=', $room->id)
                ->find_all();

        // We want to make room capacities using array id
        $capacities = array();
        foreach ($room_capacities as $room_capacity) {
            $capacities[$room_capacity->number_of_adults][$room_capacity->number_of_children] = $room_capacity;
        }
        $room_capacities = $capacities;

        // Get hotel photos
        $hotel_photos = ORM::factory('hotel_photo')
                ->select(
                        array('hotel_photo_texts.name', 'name')
                )
                ->join('hotel_photo_texts')->on('hotel_photo_texts.hotel_photo_id', '=', 'hotel_photos.id')
                ->where('hotel_photos.hotel_id', '=', $this->selected_hotel->id)
                ->where('hotel_photo_texts.language_id', '=', $this->selected_language->id)
                ->order_by('hotel_photo_texts.name')
                ->find_all();

        $this->template->main = Kostache::factory('room/edit')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('selected_hotel', $this->selected_hotel)
                ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                ->set('currency', $currency)
                ->set('languages', $languages)
                ->set('room', $room)
                ->set('room_texts', $room_texts)
                ->set('room_facility', $room_facility)
                ->set('room_capacities', $room_capacities)
                ->set('hotel_photos', $hotel_photos);
    }

    public function action_delete($room_id = NULL) {
        set_time_limit(0);
        // Get room id
        if ($room_id == NULL) {
            $room_id = $this->request->param('id');
        }

        // Get room
        $room = ORM::factory('room')
                ->where('id', '=', $room_id)
                ->find();

        // Is Authorized ?
        if (!A2::instance()->allowed($room, 'add')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage rooms
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
        }

        $booked_room = DB::select('bookings.room_id')
                ->from('bookings')
                ->where('bookings.room_id', '=', $room_id)
                ->execute()
                ->current();

        if ($booked_room) {
            Notice::add(Notice::ERROR, Kohana::message('general', 'room_booked'));
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
        }

        try {
            // Write log
            ORM::factory('log')
                    ->values(array(
                        'admin_id' => A1::instance()->get_user()->id,
                        'hotel_id' => $room->hotel_id,
                        'action' => 'Deleted Room',
                        'room_texts' => serialize(DB::select()
                                ->from('room_texts')
                                ->where('room_id', '=', $room->id)
                                ->execute()
                                ->as_array('language_id')),
                        'text' => NULL,
                        'data' => NULL,
                        'created' => time(),
                    ))
                    ->create();

            // Get QA admins emails
            $qa_emails = ORM::factory('admin')
                    ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                    ->where('admins.role', '=', 'quality-assurance')
                    ->where('admins_hotels.hotel_id', '=', $room->hotel_id)
                    ->find_all()
                    ->as_array('email', 'name');

            // If there is QA admins
            if (count($qa_emails) > 0) {
                // Send email
                Email::factory(strtr(':hotel_name - Deleted Room', array(
                            ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $room->hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                )), strtr(
                                        "Action: Deleted Room.\n"
                                        . "By: :admin_username (:admin_name).\n"
                                        . "Room Name: :room_name.", array(
                            ':admin_username' => A1::instance()->get_user()->username,
                            ':admin_name' => A1::instance()->get_user()->name,
                            ':room_name' => ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name,
                        )))
                        ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                        ->to($qa_emails)
                        ->send();
            }

            /// Insert to delete_room
            // Get room texts
            $room_texts = ORM::factory('room_text')
                    ->where('room_id', '=', $room->id)
                    ->find_all()
                    ->as_array('language_id');

            foreach ($room_texts as $key => $room_text) {
                DB::insert('room_deleted')
                        ->columns(array('room_id', 'hotel_id', 'language_id', 'name'))
                        ->values(array($room->id, $room->hotel_id, $key, $room_text->name))
                        ->execute();
            }

            //vacation_trigger
            $vacation_triger = array(
                'hotel_id' => $room->hotel_id,
                'room_id' => $room->id,
                'campaign_id' => NULL,
                // 'start_date' =>$booking_data['check_in'], 
                // 'end_date' =>$booking_data['check_out'],
                'item_id' => NULL,
                'start_date' => NULL,
                'end_date' => NULL,
                'type_trigger' => 4,
                'status' => 0
            );

            Controller_Vacation_Trigger::roomDelete($vacation_triger);

            // Delete room
            $room->delete();

            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
        } catch (Exception $e) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
        }

        // Redirect back
        $this->request->redirect($this->request->referrer());
    }

    
    public function action_campaign() {
        $agent = DB::select()->from('agent_hotels')->where('user_id','=',15)->execute()->as_array();
        foreach($agent as $each){
            $rooms = DB::select()->from('rooms')->where('rooms.hotel_id','=',$each['hotel_id'])->as_object()->execute();
            $i=0;$j=0;
            foreach($rooms as $room){
                
                $campaign_room = DB::select()
                                ->from('campaigns_rooms')
                                ->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
                                ->where('campaigns_rooms.room_id','=',$room->id)
                                ->where('campaigns.booking_start_date','=','0000-01-01')
                                ->where('campaigns.booking_end_date','=','9999-12-31')
                                ->where('campaigns.stay_start_date','=','0000-01-01')
                                ->where('campaigns.stay_end_date','=','9999-12-31')
                                ->where('campaigns.status','=','b')
                                ->execute()
                                ->as_array();
                
                if(empty($campaign_room)){
                    
                    // Start transaction
//                    Database::instance()->begin();

                    try {

                        // Create default campaign for room
                        $campaign = DB::insert('campaigns')
                                ->columns(array(
                            'is_show',
                            'is_default' ,
                            'type' ,
                            'booking_start_date',
                            'booking_end_date' ,
                            'stay_start_date' ,
                            'stay_end_date' ,
                            'is_stay_sunday_active' ,
                            'is_stay_monday_active' ,
                            'is_stay_tuesday_active' ,
                            'is_stay_wednesday_active' ,
                            'is_stay_thursday_active' ,
                            'is_stay_friday_active',
                            'is_stay_saturday_active' ,
                            'discount_rate',
                            'discount_amount',
                            'minimum_number_of_nights' ,
                            'minimum_number_of_rooms' ,
                            'number_of_free_nights' ,
                            'days_in_advance' ,
                            'within_days_of_arrival' ,
                            'stock_limit',
                            'other_benefits' ,
                            'status' 
                        ))
                        ->values(array(
                             1,
                             1,
                             1,
                             '0000-01-01',
                             '9999-12-31',
                             '0000-01-01',
                             '9999-12-31',
                             1,
                             1,
                             1,
                             1,
                             1,
                             1,
                             1,
                             0,
                             0,
                             1,
                             1,
                             0,
                             0,
                             0,
                             NULL,
                             NULL,
                             'b'
                        ))
                        ->execute();
                        
                        DB::insert('campaigns_rooms')
                        ->columns(array('campaign_id','room_id','sold'))
                        ->values(array($campaign[0],$room->id,0))
                        ->execute();        
                        
                        /* Show campaign to all languages */

                        // Get all languages
                        $languages = ORM::factory('language')
                                ->find_all();

                        foreach ($languages as $language) {
                            DB::insert('campaigns_languages')
                            ->columns(array('campaign_id','language_id'))
                            ->values(array($campaign[0],$language))
                            ->execute();
                        }

                    } catch (Exception $e) {
                        // Rollback transaction
//                        Database::instance()->rollback();
                        // Add error notice
                        echo '<pre>';print_r($e->getMessage());echo '</pre>';
                        
                    }
                }else{
                    echo '<pre>';print_r('campaign exist');echo '</pre>';
                }
            }
        }     
    }
    
}
