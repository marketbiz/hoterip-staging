<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Promotion extends Controller_Layout_Admin {

    public function before() {
        if ($this->request->action() == 'delete') {
            $this->auto_render = FALSE;
        }

        parent::before();

        if ($this->auto_render) {
            // Calculate number of rooms
            $number_of_rooms = ORM::factory('room')
                    ->where('hotel_id', '=', $this->selected_hotel->id)
                    ->count_all();

            // If there is no rooms
            if ($number_of_rooms <= 0) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('item', 'no_room'));
                // Redirect to manage campaign
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
            }
        }
    }

    public function action_manage() {
        // Is Authorized ?
        if (!A2::instance()->allowed('campaign', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        if ($this->request->post('delete_checked')) {
            $campaign_ids = Arr::get($this->request->post(), 'ids', array());

            foreach ($campaign_ids as $campaign_id) {
                // Get campaign
                $campaign = ORM::factory('campaign')
                        ->where('id', '=', $campaign_id)
                        ->where('is_default', '=', 0)
                        ->find();

                // If is allowed
                if (A2::instance()->allowed($campaign, 'delete')) {
                    // Get campaign values for logging
                    $log_values = $campaign->as_log();

                    // Write log
                    ORM::factory('log')
                            ->values(array(
                                'admin_id' => A1::instance()->get_user()->id,
                                'hotel_id' => $this->selected_hotel->id,
                                'action' => 'Deleted Promotion',
                                'room_texts' => NULL,
                                'text' => "Room Names: :room_names. "
                                . "Benefit: :benefit "
                                . "Booking Dates: :booking_start_date - :booking_end_date. "
                                . "Stay Dates: :stay_start_date - :stay_end_date. "
                                . "Cancellation Policy: :cancellation. ",
                                'data' => serialize(array(
                                    ':room_names' => Arr::get($log_values, 'room_names'),
                                    ':benefit' => Arr::get($log_values, 'benefit'),
                                    ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                    ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                    ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                    ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                    ':cancellation' => Arr::get($log_values, 'cancellation'),
                                )),
                                'created' => time(),
                            ))
                            ->create();

                    // Get QA admins emails
                    $qa_emails = ORM::factory('admin')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->where('admins.role', '=', 'quality-assurance')
                            ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                            ->find_all()
                            ->as_array('email', 'name');

                    // If there is QA admins
                    if (count($qa_emails) > 0) {
                        // Send email
                        Email::factory(strtr(':hotel_name - Deleted Promotion', array(
                                    ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                        )), strtr(
                                                "Action: Deleted Promotion.\n"
                                                . "By: :admin_username (:admin_name).\n"
                                                . "Room Names: :room_names.\n"
                                                . "Benefit: :benefit\n"
                                                . "Booking Dates: :booking_start_date - :booking_end_date.\n"
                                                . "Stay Dates: :stay_start_date - :stay_end_date.\n"
                                                . "Cancellation Policy: :cancellation.", array(
                                    ':admin_username' => A1::instance()->get_user()->username,
                                    ':admin_name' => A1::instance()->get_user()->name,
                                    ':room_names' => Arr::get($log_values, 'room_names'),
                                    ':benefit' => Arr::get($log_values, 'benefit'),
                                    ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                    ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                    ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                    ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                    ':cancellation' => Arr::get($log_values, 'cancellation'),
                                )))
                                ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                ->to($qa_emails)
                                ->send();
                    }

                    // Delete campaign
                    $campaign->delete();
                }
            }
        }

        if ($this->request->post('show')) {
            $campaign_ids = Arr::get($this->request->post(), 'ids', array());

            foreach ($campaign_ids as $campaign_id) {
                $campaign = ORM::factory('campaign')
                        ->where('id', '=', $campaign_id)
                        ->where('is_default', '=', 0)
                        ->find();

                if (A2::instance()->allowed($campaign, 'edit')) {
                    $campaign->is_show = 1;
                    $campaign->update();

                    // Get campaign values for logging
                    $log_values = $campaign->as_log();

                    // Write log
                    ORM::factory('log')
                            ->values(array(
                                'admin_id' => A1::instance()->get_user()->id,
                                'hotel_id' => $this->selected_hotel->id,
                                'action' => 'Show Promotion',
                                'room_texts' => NULL,
                                'text' => "Room Names: :room_names. "
                                . "Benefit: :benefit "
                                . "Booking Dates: :booking_start_date - :booking_end_date. "
                                . "Stay Dates: :stay_start_date - :stay_end_date. "
                                . "Cancellation Policy: :cancellation. ",
                                'data' => serialize(array(
                                    ':room_names' => Arr::get($log_values, 'room_names'),
                                    ':benefit' => Arr::get($log_values, 'benefit'),
                                    ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                    ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                    ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                    ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                    ':cancellation' => Arr::get($log_values, 'cancellation'),
                                )),
                                'created' => time(),
                            ))
                            ->create();

                    // Get QA admins emails
                    $qa_emails = ORM::factory('admin')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->where('admins.role', '=', 'quality-assurance')
                            ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                            ->find_all()
                            ->as_array('email', 'name');

                    // If there is QA admins
                    if (count($qa_emails) > 0) {
                        // Send email
                        Email::factory(strtr(':hotel_name - Show Promotion', array(
                                    ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                        )), strtr(
                                                "Action: Show Promotion.\n"
                                                . "By: :admin_username (:admin_name).\n"
                                                . "Room Names: :room_names.\n"
                                                . "Benefit: :benefit\n"
                                                . "Booking Dates: :booking_start_date - :booking_end_date.\n"
                                                . "Stay Dates: :stay_start_date - :stay_end_date.\n"
                                                . "Cancellation Policy: :cancellation.", array(
                                    ':admin_username' => A1::instance()->get_user()->username,
                                    ':admin_name' => A1::instance()->get_user()->name,
                                    ':room_names' => Arr::get($log_values, 'room_names'),
                                    ':benefit' => Arr::get($log_values, 'benefit'),
                                    ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                    ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                    ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                    ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                    ':cancellation' => Arr::get($log_values, 'cancellation'),
                                )))
                                ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                ->to($qa_emails)
                                ->send();
                    }
                }
            }
        }

        if ($this->request->post('hide')) {
            $campaign_ids = Arr::get($this->request->post(), 'ids', array());

            foreach ($campaign_ids as $campaign_id) {
                $campaign = ORM::factory('campaign')
                        ->where('id', '=', $campaign_id)
                        ->where('is_default', '=', 0)
                        ->find();

                if (A2::instance()->allowed($campaign, 'edit')) {
                    $campaign->is_show = 0;
                    $campaign->update();

                    // Get campaign values for logging
                    $log_values = $campaign->as_log();

                    // Write log
                    ORM::factory('log')
                            ->values(array(
                                'admin_id' => A1::instance()->get_user()->id,
                                'hotel_id' => $this->selected_hotel->id,
                                'action' => 'Hide Promotion',
                                'room_texts' => NULL,
                                'text' => "Room Names: :room_names. "
                                . "Benefit: :benefit "
                                . "Booking Dates: :booking_start_date - :booking_end_date. "
                                . "Stay Dates: :stay_start_date - :stay_end_date. "
                                . "Cancellation Policy: :cancellation. ",
                                'data' => serialize(array(
                                    ':room_names' => Arr::get($log_values, 'room_names'),
                                    ':benefit' => Arr::get($log_values, 'benefit'),
                                    ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                    ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                    ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                    ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                    ':cancellation' => Arr::get($log_values, 'cancellation'),
                                )),
                                'created' => time(),
                            ))
                            ->create();

                    // Get QA admins emails
                    $qa_emails = ORM::factory('admin')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->where('admins.role', '=', 'quality-assurance')
                            ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                            ->find_all()
                            ->as_array('email', 'name');

                    // If there is QA admins
                    if (count($qa_emails) > 0) {
                        // Send email
                        Email::factory(strtr(':hotel_name - Hide Promotion', array(
                                    ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                        )), strtr(
                                                "Action: Hide Promotion.\n"
                                                . "By: :admin_username (:admin_name).\n"
                                                . "Room Names: :room_names.\n"
                                                . "Benefit: :benefit\n"
                                                . "Booking Dates: :booking_start_date - :booking_end_date.\n"
                                                . "Stay Dates: :stay_start_date - :stay_end_date.\n"
                                                . "Cancellation Policy: :cancellation.", array(
                                    ':admin_username' => A1::instance()->get_user()->username,
                                    ':admin_name' => A1::instance()->get_user()->name,
                                    ':room_names' => Arr::get($log_values, 'room_names'),
                                    ':benefit' => Arr::get($log_values, 'benefit'),
                                    ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                    ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                    ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                    ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                    ':cancellation' => Arr::get($log_values, 'cancellation'),
                                )))
                                ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                ->to($qa_emails)
                                ->send();
                    }
                }
            }
        }

        $campaigns = ORM::factory('campaign')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->where('campaigns.is_default', '=', 0)
                ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                ->group_by('campaigns.id')
                ->find_all();

        $languages = ORM::factory('language')
                ->find_all()
                ->as_array('id');

        $rooms = ORM::factory('room')
                ->where('hotel_id', '=', $this->selected_hotel->id)
                ->find_all();

        $currency = ORM::factory('currency')
                ->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        $this->template->main = Kostache::factory('promotion/manage')
                ->set('notice', Notice::render())
                ->set('selected_language', $this->selected_language)
                ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                ->set('campaigns', $campaigns)
                ->set('languages', $languages)
                ->set('rooms', $rooms)
                ->set('currency', $currency);
    }

    public function action_add() {
        // Is Authorized ?
        if (!A2::instance()->allowed('campaign', 'add')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage campaigns
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'campaign', 'action' => 'manage')));
        }

        if ($this->request->post('submit')) {
            $other_benefits = $this->request->post('other_benefit');
            $other_benefits_value = $this->request->post('other_benefit_value');

            $count = count($other_benefits);
            $additional_benefits = NULL;

            if ($count > 0) {

                foreach ($other_benefits as $key => $other_benefit) {
                    $additional_benefits[] = array(
                        'benefit_id' => $other_benefit,
                        'value' => $other_benefits_value[$key]
                    );
                }

                unset($other_benefit);
                unset($other_benefits_value);
            }
            
            /// Vacation rates notif. Saving status
            $save_status = TRUE;
            // Start transaction
            Database::instance()->begin();

            try {
                $values = $this->request->post();

                //values benefits
                $values['additional_benefits'] = $additional_benefits;

                $booking_start_date = $this->request->post('booking_start_date');
                $booking_end_date = $this->request->post('booking_end_date');
                $stay_start_date = $this->request->post('stay_start_date');
                $stay_end_date = $this->request->post('stay_end_date');

                if (strtotime($booking_start_date) > strtotime($booking_end_date))
                    throw new Kohana_Exception(Kohana::message('campaign', 'booking_date_overlap'));

                if (strtotime($stay_start_date) > strtotime($stay_end_date))
                    throw new Kohana_Exception(Kohana::message('campaign', 'stay_date_overlap'));

                if (strtotime($booking_start_date) < strtotime(date('Y-m-d')))
                    throw new Kohana_Exception(Kohana::message('campaign', 'booking_start_date_less_than_now'));

                if (strtotime($stay_start_date) < strtotime(date('Y-m-d')))
                    throw new Kohana_Exception(Kohana::message('campaign', 'stay_start_date_less_than_now'));


                switch (Arr::get($values, 'type', 1)) {
                    case 1 :
                        $values['days_in_advance'] = 0;
                        $values['within_days_of_arrival'] = 0;
                        break;
                    case 2:
                        if ($values['days_in_advance'] <= 0) {
                            throw new Kohana_Exception(Kohana::message('campaign', 'days_in_advance_empty'));
                        }
                        $values['within_days_of_arrival'] = 0;
                        break;
                    case 3 :
                        $values['days_in_advance'] = 0;
                        if ($values['within_days_of_arrival'] <= 0) {
                            throw new Kohana_Exception(Kohana::message('campaign', 'within_days_of_arrival_empty'));
                        }
                        break;
                    case 4 :
                        $values['days_in_advance'] = 0;
                        $values['within_days_of_arrival'] = 0;
                        break;
                    case 5 :
                        $values['days_in_advance'] = 0;
                        $values['within_days_of_arrival'] = 0;

                        $values['benefit'] = 0;
                        $values['discount_rate'] = 50;
                        $values['discount_amount'] = 0;
                        $values['number_of_free_nights'] = 0;

                        break;
                }

                switch (Arr::get($values, 'benefit', 0)) {
                    case 0 :
                        $values['discount_amount'] = 0;
                        $values['number_of_free_nights'] = 0;
                        if ($values['discount_rate'] <= 0 AND empty($values['other_benefit']))
                            throw new Kohana_Exception(Kohana::message('campaign', 'no_benefit'));
                        if ($values['discount_rate'] >= 100)
                            throw new Kohana_Exception(Kohana::message('campaign', 'discount_rate_too_big'));
                        break;
                    case 1:
                        $values['discount_rate'] = 0;
                        $values['number_of_free_nights'] = 0;
                        if ($values['discount_amount'] <= 0 AND empty($values['other_benefit']))
                            throw new Kohana_Exception(Kohana::message('campaign', 'no_benefit'));
                        break;
                    case 2 :
                        $values['discount_rate'] = 0;
                        $values['discount_amount'] = 0;
                        if ($values['number_of_free_nights'] <= 0 AND empty($values['other_benefit']))
                            throw new Kohana_Exception(Kohana::message('campaign', 'no_benefit'));
                        if ($values['number_of_free_nights'] >= $values['minimum_number_of_nights'])
                            throw new Kohana_Exception(Kohana::message('campaign', 'free_night_too_big'));
                        break;
                }
                // Set campaign as promotion
                $values['is_default'] = 0;

                // Fill with 0 if keys does not exist
                $values['is_stay_sunday_active'] = Arr::get($values, 'is_stay_sunday_active', 0);
                $values['is_stay_monday_active'] = Arr::get($values, 'is_stay_monday_active', 0);
                $values['is_stay_tuesday_active'] = Arr::get($values, 'is_stay_tuesday_active', 0);
                $values['is_stay_wednesday_active'] = Arr::get($values, 'is_stay_wednesday_active', 0);
                $values['is_stay_thursday_active'] = Arr::get($values, 'is_stay_thursday_active', 0);
                $values['is_stay_friday_active'] = Arr::get($values, 'is_stay_friday_active', 0);
                $values['is_stay_saturday_active'] = Arr::get($values, 'is_stay_saturday_active', 0);
                
                // Create campaign
                $campaign = ORM::factory('campaign')
                        ->create_campaign($values);
                //echo '<pre>';print_r($campaign);echo '<pre>';
                //exit();
                // Get room ids
                $room_ids = Arr::get($values, 'room_ids', array());

                // If there is no room selected
                if (count($room_ids) == 0) {
                    throw new Kohana_Exception(Kohana::message('campaign', 'no_room_selected'));
                }

                // If discount amount more than zero and valid
                if ($values['discount_amount'] > 0) {
                    // If discount amount not valid
                    if (!Model_Campaign::valid_discount_amount($values['discount_amount'], $room_ids, $stay_start_date, $stay_end_date))
                        throw new Kohana_Exception(Kohana::message('campaign', 'discount_amount_too_big'));
                }

                // Link campaign to room
                foreach ($room_ids as $room_id) {
                    $room = ORM::factory('room')
                            ->where('id', '=', $room_id)
                            ->find();

                    // If logged in admin authorized to edit the room
                    if (A2::instance()->allowed($room, 'edit')) {
                        $campaign->add('rooms', $room);
                    } else {
                        throw new Kohana_Exception(Kohana::message('general', 'not_authorized'));
                    }
                }

                // If non refundable cancellation policy
                if (Arr::get($values, 'type') == 4) {
                    // Get non refundable cancellation rule
                    $cancellation_rule = ORM::factory('cancellation_rule')
                            ->where('within_days', '=', 10000)
                            ->where('percent_charged', '=', 100)
                            ->where('night_charged', '=', 0)
                            ->find();

                    // Create non refundable cancellation policy
                    $cancellation = ORM::factory('cancellation')
                            ->values(array(
                                'level_1_cancellation_rule_id' => $cancellation_rule->id,
                                'start_date' => '0000-01-01',
                                'end_date' => '9999-12-31',
                            ))
                            ->create();

                    // Link cancellation with campaign
                    $campaign->add('cancellations', $cancellation);
                } else {
                    // If not using default cancellation policy
                    if (Arr::get($values, 'is_default_cancellation', 0) == 1) {
                        $values['start_date'] = '0000-01-01';
                        $values['end_date'] = '9999-12-31';

                        // Create cancellation policy
                        $cancellation = ORM::factory('cancellation')
                                ->create_cancellation($values);

                        // Link cancellation with campaign
                        $campaign->add('cancellations', $cancellation);
                    }
//          else
//          {
//            // Check each room cancellation policies
//            foreach ($room_ids as $room_id)
//            {
//              $room = ORM::factory('room')
//                ->select(
//                  array('room_texts.name', 'name')
//                )
//                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
//                ->where('rooms.id', '=', $room_id)
//                ->where('room_texts.language_id', '=', $this->selected_language->id)
//                ->find();
//
//              // Check cancellation policy period
//              if ( ! Model_Cancellation::valid_date_range($stay_start_date, $stay_end_date, $room_id))
//                throw new Kohana_Exception(Kohana::message('campaign', 'no_cancellation'), array(':room_name' => $room->name));
//            }
//          }
                }

                // Get language ids
                $language_ids = Arr::get($values, 'language_ids', array());

                // If there is language selected
                if (count($language_ids) == 0) {
                    throw new Kohana_Exception(Kohana::message('campaign', 'no_language_selected'));
                }

                // Link campaign to languages
                foreach ($language_ids as $language_id) {
                    $language = ORM::factory('language')
                            ->where('id', '=', $language_id)
                            ->find();

                    $campaign->add('languages', $language);
                }

                // Get campaign values for logging
                $log_values = $campaign->as_log();

                // Write log
                ORM::factory('log')
                        ->values(array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $this->selected_hotel->id,
                            'action' => 'Added Promotion',
                            'room_texts' => NULL,
                            'text' => 'Room Names: :room_names. '
                            . 'Benefit: :benefit '
                            . 'Booking Dates: :booking_start_date - :booking_end_date. '
                            . 'Stay Dates: :stay_start_date - :stay_end_date. '
                            . 'Cancellation Policy: :cancellation.',
                            'data' => serialize(array(
                                ':room_names' => Arr::get($log_values, 'room_names'),
                                ':benefit' => Arr::get($log_values, 'benefit'),
                                ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                ':cancellation' => Arr::get($log_values, 'cancellation'),
                            )),
                            'created' => time(),
                        ))
                        ->create();

                // Get QA admins emails
                $qa_emails = ORM::factory('admin')
                        ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                        ->where('admins.role', '=', 'quality-assurance')
                        ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                        ->find_all()
                        ->as_array('email', 'name');

                // If there is QA admins
                if (count($qa_emails) > 0) {
                    // Send email
                    Email::factory(strtr(':hotel_name - Added Promotion', array(
                                ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                    )), strtr(
                                            "Action: Added Promotion.\n"
                                            . "By: :admin_username (:admin_name).\n"
                                            . "Room Names: :room_names.\n"
                                            . "Benefit: :benefit\n"
                                            . "Booking Dates: :booking_start_date - :booking_end_date.\n"
                                            . "Stay Dates: :stay_start_date - :stay_end_date.\n"
                                            . "Cancellation Policy: :cancellation.", array(
                                ':admin_username' => A1::instance()->get_user()->username,
                                ':admin_name' => A1::instance()->get_user()->name,
                                ':room_names' => Arr::get($log_values, 'room_names'),
                                ':benefit' => Arr::get($log_values, 'benefit'),
                                ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                ':cancellation' => Arr::get($log_values, 'cancellation'),
                            )))
                            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                            ->to($qa_emails)
                            ->send();
                }

                // Commit transaction
                Database::instance()->commit();
                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'promotion', 'action' => 'add')), __('Add another promotion'))));
                // Redirect to edit
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotion', 'action' => 'edit', 'id' => $campaign->id)));
            } catch (ORM_Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                /// Vacation rates notif. Saving status
                $save_status = FALSE;
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('campaign'));
            } catch (Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                /// Vacation rates notif. Saving status
                $save_status = FALSE;
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }

            /*
             *  Rates Notification for API
             */
            if ($save_status) {
                $values['campaign_id'] = $campaign->id;
                $values['hotel_id'] = $this->selected_hotel->id;

                // Add logs
                // Log::instance()->add(Log::INFO, 'API Vacation add promotions');
                // Log::instance()->add(Log::INFO, 'Admin ID: '.A1::instance()->get_user()->id);
                // Log::instance()->add(Log::INFO, 'Hotel ID: '.$this->selected_hotel->id);
                // Log::instance()->add(Log::INFO, 'Campaign ID: '.$campaign->id);
                //new log
                CustomLog::factory()->add(2, 'DEBUG', 'API Vacation add promotions');
                CustomLog::factory()->add(2, 'DEBUG', 'Admin ID: ' . A1::instance()->get_user()->id);
                CustomLog::factory()->add(2, 'DEBUG', 'Hotel ID: ' . $this->selected_hotel->id);
                CustomLog::factory()->add(2, 'DEBUG', 'Campaign ID: ' . $campaign->id);

                // OTA_HotelRatePlanNotifRQ
                if ($values['number_of_free_nights']) {
                    foreach ($values['room_ids'] as $room_id) {
                        $values['room_id'] = $room_id;

                        // Vacation notifications
                        $rateplan = Controller_Vacation_Notifications_Rateplan::promotion($values) ?
                                'success' : 'failed';
                    }
                } else {
                    //OTA_HotelRateAmountNotifRQ
                    $amount = Controller_Vacation_Notifications_Amount::promotion($values) ?
                            'success' : 'failed';
                }

                // OTA_HotelAvailNotifRQ
                $availability = Controller_Vacation_Notifications_Availability::promotion($values) ?
                        'success' : 'failed';

                // Sucess or error message vacation
                if ($rateplan == 'success') {
                    Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_notif_save_success'));
                } elseif ($rateplan == 'failed') {
                    Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_notif_save_failure'));
                }

                if ($amount == 'success') {
                    Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_amount_save_success'));
                } elseif ($amount == 'failed') {
                    Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_amount_notif_save_failure'));
                }

                if ($availability == 'success') {
                    Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_availability_vacation_save_success'));
                } elseif ($availability == 'failed') {
                    Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_availability_vacation_save_failure'));
                }
            }
            // Redirect to edit
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotion', 'action' => 'edit', 'id' => $campaign->id)));
        }


        //Load other benefit data
        $query = DB::select()->from('benefit_texts')->where('language_id', '=', $this->selected_language->id);
        $data_benefits = $query->execute()->as_array();

        $currency = ORM::factory('currency')
                ->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        $rooms = ORM::factory('room')
                ->select(
                        'rooms.*', array('room_texts.name', 'name')
                )
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
                ->order_by('rooms.publish_price', 'ASC')
                ->find_all();

        $languages = ORM::factory('language')
                ->find_all();

        $cancellation_level_1_rules = ORM::factory('cancellation_rule')
                ->where('level', '=', 1)
                ->find_all();

        $cancellation_level_2_rules = ORM::factory('cancellation_rule')
                ->where('level', '=', 2)
                ->find_all();

        $cancellation_level_3_rules = ORM::factory('cancellation_rule')
                ->where('level', '=', 2)
                ->find_all();

        $this->template->main = Kostache::factory('promotion/add')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('currency', $currency)
                ->set('rooms', $rooms)
                ->set('languages', $languages)
                ->set('data_benefits', $data_benefits)
                ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                ->set('cancellation_level_1_rules', $cancellation_level_1_rules)
                ->set('cancellation_level_2_rules', $cancellation_level_2_rules)
                ->set('cancellation_level_3_rules', $cancellation_level_3_rules);
    }

    public function action_edit() {
        // Get campaign id
        $campaign_id = (int) $this->request->param('id');

        /// Search Promotions. Get campaign Hotel
        $campaign = ORM::factory('campaign')
                ->select(
                        array('rooms.hotel_id', 'hotel_id')
                )
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->where('campaigns.id', '=', $campaign_id)
                ->find();
         
        // Is Authorized ?
        if (!A2::instance()->allowed($campaign, 'edit')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage campaigns
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotion', 'action' => 'manage')));
        }
        
        /// Search Promotions. Change Hotel selected
        if ($campaign->hotel_id != $this->selected_hotel->id) {
            // Set selected hotel id in cookie
            Cookie::set('selected_hotel_id', $campaign->hotel_id);
            // Add notice
            Notice::add(Notice::INFO, 'Your selected hotel has been changed in top right corner.');

            $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotion', 'action' => 'edit')) . '/' . $campaign_id);
        }

        if ($this->request->post('submit')) {
            $other_benefits = $this->request->post('other_benefit');
            $other_benefits_value = $this->request->post('other_benefit_value');

            $count = count($other_benefits);
            $additional_benefits = NULL;

            /// Vacation rates notif. Saving status
            $save_status = TRUE;

            if ($count > 0) {

                foreach ($other_benefits as $key => $other_benefit) {
                    $additional_benefits[] = array(
                        'benefit_id' => $other_benefit,
                        'value' => $other_benefits_value[$key]
                    );
                }

                unset($other_benefit);
                unset($other_benefits_value);
            }

            // Start transaction
            Database::instance()->begin();

            try {
                // Get values from post
                $values = $this->request->post();

                //values benefits
                $values['additional_benefits'] = $additional_benefits;

                $booking_start_date = $this->request->post('booking_start_date');
                $booking_end_date = $this->request->post('booking_end_date');
                $stay_start_date = $this->request->post('stay_start_date');
                $stay_end_date = $this->request->post('stay_end_date');
                if (strtotime($booking_start_date) > strtotime($booking_end_date))
                    throw new Kohana_Exception(Kohana::message('campaign', 'booking_date_overlap'));

                if (strtotime($stay_start_date) > strtotime($stay_end_date))
                    throw new Kohana_Exception(Kohana::message('campaign', 'stay_date_overlap'));

                switch (Arr::get($values, 'type', 1)) {
                    case 1 :
                        $values['days_in_advance'] = 0;
                        $values['within_days_of_arrival'] = 0;
                        break;
                    case 2:
                        if ($values['days_in_advance'] <= 0) {
                            throw new Kohana_Exception(Kohana::message('campaign', 'days_in_advance_empty'));
                        }
                        $values['within_days_of_arrival'] = 0;
                        break;
                    case 3 :
                        $values['days_in_advance'] = 0;
                        if ($values['within_days_of_arrival'] <= 0) {
                            throw new Kohana_Exception(Kohana::message('campaign', 'within_days_of_arrival_empty'));
                        }
                        break;
                    case 4 :
                        $values['days_in_advance'] = 0;
                        $values['within_days_of_arrival'] = 0;
                        break;
                    case 5 :
                        $values['days_in_advance'] = 0;
                        $values['within_days_of_arrival'] = 0;

                        $values['benefit'] = 0;
                        $values['discount_rate'] = 50;
                        $values['discount_amount'] = 0;
                        $values['number_of_free_nights'] = 0;

                        break;
                }

                switch (Arr::get($values, 'benefit', 0)) {
                    case 0 :
                        $values['discount_amount'] = 0;
                        $values['number_of_free_nights'] = 0;
                        if ($values['discount_rate'] <= 0 AND empty($values['other_benefit']))
                            throw new Kohana_Exception(Kohana::message('campaign', 'no_benefit'));
                        if ($values['discount_rate'] >= 100)
                            throw new Kohana_Exception(Kohana::message('campaign', 'discount_rate_too_big'));
                        break;
                    case 1:
                        $values['discount_rate'] = 0;
                        $values['number_of_free_nights'] = 0;
                        if ($values['discount_amount'] <= 0 AND empty($values['other_benefit']))
                            throw new Kohana_Exception(Kohana::message('campaign', 'no_benefit'));
                        break;
                    case 2 :
                        $values['discount_rate'] = 0;
                        $values['discount_amount'] = 0;
                        if ($values['number_of_free_nights'] <= 0 AND empty($values['other_benefit']))
                            throw new Kohana_Exception(Kohana::message('campaign', 'no_benefit'));
                        if ($values['number_of_free_nights'] >= $values['minimum_number_of_nights'])
                            throw new Kohana_Exception(Kohana::message('campaign', 'free_night_too_big'));
                        break;
                }

                // Set campaign as promotion
                $values['is_default'] = 0;

                // Fill with 0 if keys does not exist
                $values['is_stay_sunday_active'] = Arr::get($values, 'is_stay_sunday_active', 0);
                $values['is_stay_monday_active'] = Arr::get($values, 'is_stay_monday_active', 0);
                $values['is_stay_tuesday_active'] = Arr::get($values, 'is_stay_tuesday_active', 0);
                $values['is_stay_wednesday_active'] = Arr::get($values, 'is_stay_wednesday_active', 0);
                $values['is_stay_thursday_active'] = Arr::get($values, 'is_stay_thursday_active', 0);
                $values['is_stay_friday_active'] = Arr::get($values, 'is_stay_friday_active', 0);
                $values['is_stay_saturday_active'] = Arr::get($values, 'is_stay_saturday_active', 0);

                // Get campaign
                $campaign = ORM::factory('campaign')
                        ->where('id', '=', $campaign_id)
                        ->find();

                // Clone as prev campaign
                $prev_campaign = clone $campaign;

                // Get room names
                $prev_room_names = $prev_campaign->rooms
                        ->select(array('room_texts.name', 'name'))
                        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                        ->where('room_texts.language_id', '=', 1)
                        ->find_all()
                        ->as_array('id', 'name');

                // Get prev cancellation
                $prev_cancellation = $prev_campaign->cancellations
                        ->select(
                                array('rule_1.name', 'rule_1_name'), array('rule_2.name', 'rule_2_name'), array('rule_3.name', 'rule_3_name')
                        )
                        ->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'level_1_cancellation_rule_id')
                        ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'level_2_cancellation_rule_id')
                        ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'level_3_cancellation_rule_id')
                        ->find();

                // Update campaign
                $campaign = $campaign->update_campaign($values);

                $other_benefits = DB::select(
                                'campaigns_benefits.*', array('benefit_texts.name', 'name')
                        )
                        ->from('campaigns_benefits')
                        ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                        ->where('benefit_texts.language_id', '=', 1)
                        ->where('campaigns_benefits.campaign_id', '=', $campaign_id)
                        ->execute();

                $prev_other_benefits = '';
                if (count($other_benefits)) {
                    $c = 1;
                    foreach ($other_benefits as $key => $other_benefit) {
                        if (count($other_benefits) != $c) {
                            $prev_other_benefits .= $other_benefit['name'] . ', ';
                        } else {
                            $prev_other_benefits .= $other_benefit['name'];
                        }
                        $c++;
                    }
                }

                // Get room ids
                $room_ids = Arr::get($values, 'room_ids', array());

                // If there is no room selected
                if (count($room_ids) == 0) {
                    throw new Kohana_Exception(Kohana::message('campaign', 'no_room_selected'));
                }

                // If discount amount more than zero
                if ($values['discount_amount'] > 0) {
                    // If discount amount not valid
                    if (!Model_Campaign::valid_discount_amount($values['discount_amount'], $room_ids, $stay_start_date, $stay_end_date))
                        throw new Kohana_Exception(Kohana::message('campaign', 'discount_amount_too_big'));
                }

                // Remove campaign link to all rooms
                DB::delete('campaigns_rooms')
                        ->where('campaign_id', '=', $campaign->id)
                        ->execute();

                // Link campaign to room
                foreach ($room_ids as $room_id) {
                    $room = ORM::factory('room')
                            ->where('id', '=', $room_id)
                            ->find();

                    // If logged in admin authorized to edit the room
                    if (A2::instance()->allowed($room, 'edit')) {
                        $campaign->add('rooms', $room);
                    } else {
                        throw new Kohana_Exception(Kohana::message('general', 'not_authorized'));
                    }
                }

                // Get all campaign cancellation policies
                $cancellations = $campaign->cancellations->find_all();

                foreach ($cancellations as $cancellation) {
                    // Delete cancellation policy
                    // No need to remove the link between campaign and cancellation because
                    // it's already linked cascade on delete in database
                    $cancellation->delete();
                }

                // If non refundable cancellation policy
                if (Arr::get($values, 'type') == 4) {
                    // Get non refundable cancellation rule
                    $cancellation_rule = ORM::factory('cancellation_rule')
                            ->where('within_days', '=', 10000)
                            ->where('percent_charged', '=', 100)
                            ->where('night_charged', '=', 0)
                            ->find();

                    // Create non refundable cancellation policy
                    $cancellation = ORM::factory('cancellation')
                            ->values(array(
                                'level_1_cancellation_rule_id' => $cancellation_rule->id,
                                'start_date' => '0000-01-01',
                                'end_date' => '9999-12-31',
                            ))
                            ->create();

                    // Link cancellation with campaign
                    $campaign->add('cancellations', $cancellation);
                } else {
                    // If not using default cancellation policy
                    if (Arr::get($values, 'is_default_cancellation', 0) == 1) {
                        $values['start_date'] = '0000-01-01';
                        $values['end_date'] = '9999-12-31';

                        // Create cancellation policy
                        $cancellation = ORM::factory('cancellation')
                                ->create_cancellation($values);

                        // Link cancellation with campaign
                        $campaign->add('cancellations', $cancellation);
                    }
//          else
//          {
//            // Check each room cancellation policies
//            foreach ($room_ids as $room_id)
//            {
//              $room = ORM::factory('room')
//                ->select(
//                  array('room_texts.name', 'name')
//                )
//                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
//                ->where('rooms.id', '=', $room_id)
//                ->where('room_texts.language_id', '=', $this->selected_language->id)
//                ->find();
//
//              // Check cancellation policy period
//              if ( ! Model_Cancellation::valid_date_range($stay_start_date, $stay_end_date, $room_id))
//                throw new Kohana_Exception(Kohana::message('campaign', 'no_cancellation'), array(':room_name' => $room->name));
//            }
//          }
                }

                // Get language ids
                $language_ids = Arr::get($values, 'language_ids', array());

                // If there is language selected
                if (count($language_ids) == 0) {
                    throw new Kohana_Exception(Kohana::message('campaign', 'no_language_selected'));
                }

                // Remove campaign link to all languages
                DB::delete('campaigns_languages')
                        ->where('campaign_id', '=', $campaign->id)
                        ->execute();

                // Link campaign to languages
                foreach ($language_ids as $language_id) {
                    $language = ORM::factory('language')
                            ->where('id', '=', $language_id)
                            ->find();

                    $campaign->add('languages', $language);
                }

                // Get campaign values for logging
                $log_values = $campaign->as_log($prev_campaign, $prev_room_names, $prev_cancellation, $prev_other_benefits);

                // Write log
                ORM::factory('log')
                        ->values(array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $this->selected_hotel->id,
                            'action' => 'Edited Promotion',
                            'room_texts' => NULL,
                            'text' => 'Room Names: :prev_room_names -> :room_names. '
                            . 'Benefit: :prev_benefit -> :benefit '
                            . 'Additional Benefits: :prev_other_benefit -> :other_benefit. '
                            . 'Booking Dates: :prev_booking_start_date - :prev_booking_end_date -> :booking_start_date - :booking_end_date. '
                            . 'Stay Dates: :prev_stay_start_date - :prev_stay_end_date -> :stay_start_date - :stay_end_date. '
                            . 'Cancellation Policy: :prev_cancellation -> :cancellation.',
                            'data' => serialize(array(
                                ':prev_room_names' => Arr::get($log_values, 'prev_room_names'),
                                ':prev_benefit' => Arr::get($log_values, 'prev_benefit'),
                                ':prev_other_benefit' => Arr::get($log_values, 'prev_other_benefit'),
                                ':prev_booking_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                                ':prev_booking_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                                ':prev_stay_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                                ':prev_stay_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                                ':prev_cancellation' => Arr::get($log_values, 'prev_cancellation'),
                                ':room_names' => Arr::get($log_values, 'room_names'),
                                ':benefit' => Arr::get($log_values, 'benefit'),
                                ':other_benefit' => Arr::get($log_values, 'other_benefit'),
                                ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                ':cancellation' => Arr::get($log_values, 'cancellation'),
                            )),
                            'created' => time(),
                        ))
                        ->create();

                // Get QA admins emails
                $qa_emails = ORM::factory('admin')
                        ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                        ->where('admins.role', '=', 'quality-assurance')
                        ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                        ->find_all()
                        ->as_array('email', 'name');

                // If there is QA admins
                if (count($qa_emails) > 0) {
                    // Send email
                    Email::factory(strtr(':hotel_name - Edited Promotion', array(
                                ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                    )), strtr(
                                            "Action: Edited Promotion.\n"
                                            . "By: :admin_username (:admin_name).\n"
                                            . "Room Names: :prev_room_names -> :room_names.\n"
                                            . "Benefit: :prev_benefit -> :benefit.\n"
                                            . "Additional Benefits: :prev_other_benefit -> :other_benefit.\n"
                                            . "Booking Dates: :prev_booking_start_date - :prev_booking_end_date -> :booking_start_date - :booking_end_date.\n"
                                            . "Stay Dates: :prev_stay_start_date - :prev_stay_end_date -> :stay_start_date - :stay_end_date.\n"
                                            . "Cancellation Policy: :prev_cancellation -> :cancellation.", array(
                                ':admin_username' => A1::instance()->get_user()->username,
                                ':admin_name' => A1::instance()->get_user()->name,
                                ':prev_room_names' => Arr::get($log_values, 'prev_room_names'),
                                ':prev_benefit' => Arr::get($log_values, 'prev_benefit'),
                                ':prev_other_benefit' => Arr::get($log_values, 'prev_other_benefit'),
                                ':prev_booking_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                                ':prev_booking_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                                ':prev_stay_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                                ':prev_stay_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                                ':prev_cancellation' => Arr::get($log_values, 'prev_cancellation'),
                                ':room_names' => Arr::get($log_values, 'room_names'),
                                ':benefit' => Arr::get($log_values, 'benefit'),
                                ':other_benefit' => Arr::get($log_values, 'other_benefit'),
                                ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                                ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                                ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                                ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                                ':cancellation' => Arr::get($log_values, 'cancellation'),
                            )))
                            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                            ->to($qa_emails)
                            ->send();
                }

                // Commit transaction
                Database::instance()->commit();
                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
            } catch (ORM_Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                /// Vacation rates notif. Saving status
                $save_status = FALSE;
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('campaign'));
            } catch (Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                /// Vacation rates notif. Saving status
                $save_status = FALSE;
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }

            /*
             *  Rates Notification for API
             */
            if ($save_status) {
                $vacation_triger = array(
                    'hotel_id' => $this->selected_hotel->id,
                    'room_id' => NULL,
                    'campaign_id' => $campaign->id,
                    // 'start_date' =>$booking_data['check_in'], 
                    // 'end_date' =>$booking_data['check_out'],
                    'item_id' => NULL,
                    'start_date' => NULL,
                    'end_date' => NULL,
                    'type_trigger' => 2,
                    'status' => 0
                );

                //insert data vacation trigger into database
                Controller_Vacation_Trigger::insertData($vacation_triger);

                // $values['campaign_id'] = (int) $this->request->param('id');
                // $values['hotel_id'] = $this->selected_hotel->id;
                // $values['check_in'] = date('Y-m-d');
                // $values['check_out'] = $values['booking_end_date'];
                // // Add logs
                // Log::instance()->add(Log::INFO, 'API Vacation edit promotions');
                // Log::instance()->add(Log::INFO, 'Admin ID: '.A1::instance()->get_user()->id);
                // Log::instance()->add(Log::INFO, 'Hotel ID: '.$this->selected_hotel->id);
                // Log::instance()->add(Log::INFO, 'Campaign ID: '.$this->request->param('id'));
                // // OTA_HotelRatePlanNotifRQ
                // if($values['number_of_free_nights'])
                // {
                // 	foreach ($values['room_ids'] as $room_id)
                // 	{
                // 		$values['room_id'] = $room_id;
                // 		// Vacation notifications
                // 		$rateplan = Controller_Vacation_Notifications_Rateplan::promotion($values) ? 
                // 	 		'success' : 'failed';
                // 	}
                // }
                // else
                // {
                // 	//OTA_HotelRateAmountNotifRQ
                // 	$amount = Controller_Vacation_Notifications_Amount::promotion($values) ? 
                // 	 'success' : 'failed';
                // }
                // // OTA_HotelAvailNotifRQ
                // $availability = Controller_Vacation_Notifications_Availability::promotion($values) ? 
                // 	 'success' : 'failed';
                // // Sucess or error message vacation
                // if($rateplan == 'success')
                // {
                // 	Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_notif_save_success'));
                // }
                // elseif($rateplan == 'failed')
                // {
                // 	Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_notif_save_failure'));
                // }
                // if($amount == 'success')
                // {
                // 	Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_amount_save_success'));
                // }
                // elseif($amount == 'failed')
                // {
                // 	Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_amount_notif_save_failure'));
                // }
                // if($availability == 'success')
                // {
                // 	Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_availability_vacation_save_success'));
                // }
                // elseif($availability == 'failed')
                // {
                // 	Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_availability_vacation_save_failure'));
                // }
            }
        }
        // Get benefit texts
        $query = DB::select()->from('benefit_texts')->where('language_id', '=', $this->selected_language->id);
        $data_benefits = $query->execute()->as_array();

        $campaign = ORM::factory('campaign')
                ->where('id', '=', $campaign_id)
                ->find();

        $languages = ORM::factory('language')
                ->find_all();

        $currency = ORM::factory('currency')
                ->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        $rooms = ORM::factory('room')
                ->select(
                        'rooms.*', array('room_texts.name', 'name')
                )
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
                ->order_by('rooms.publish_price', 'ASC')
                ->find_all();

        $other_benefits = DB::select(
                        'campaigns_benefits.*', array('benefit_texts.name', 'name')
                )
                ->from('campaigns_benefits')
                ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                ->where('benefit_texts.language_id', '=', $this->selected_language->id)
                ->where('campaigns_benefits.campaign_id', '=', $campaign_id)
                ->execute();

        $cancellations = $campaign->cancellations
                ->find_all();

        if (count($cancellations) > 0) {
            $cancellation = $cancellations->current();
        } else {
            $cancellation = ORM::factory('cancellation');
            $cancellation->level_1_cancellation_rule_id = 0;
            $cancellation->level_2_cancellation_rule_id = 0;
            $cancellation->level_3_cancellation_rule_id = 0;
        }

        $cancellation_level_1_rules = ORM::factory('cancellation_rule')
                ->where('level', '=', 1)
                ->find_all();

        $cancellation_level_2_rules = ORM::factory('cancellation_rule')
                ->where('level', '=', 2)
                ->find_all();

        $cancellation_level_3_rules = ORM::factory('cancellation_rule')
                ->where('level', '=', 2)
                ->find_all();

        // Is hoterip campaign exist
        $hoterip_campaign = $campaign->rooms
                        ->where('rooms.hoterip_campaign', '=', 1)
                        ->count_all() > 0;

        $this->template->main = Kostache::factory('promotion/edit')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('campaign', $campaign)
                ->set('languages', $languages)
                ->set('selected_language', $this->selected_language->id)
                ->set('currency', $currency)
                ->set('rooms', $rooms)
                ->set('cancellations', $cancellations)
                ->set('cancellation', $cancellation)
                ->set('data_benefits', $data_benefits)
                ->set('other_benefits', $other_benefits)
                ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                ->set('hoterip_campaign', $hoterip_campaign)
                ->set('cancellation_level_1_rules', $cancellation_level_1_rules)
                ->set('cancellation_level_2_rules', $cancellation_level_2_rules)
                ->set('cancellation_level_3_rules', $cancellation_level_3_rules);
    }

    public function action_delete() {
        // Load Config
        if (Kohana::$environment == Kohana::PRODUCTION) {
            $config = Kohana::$config->load('api')->vacation;
        } else {
            $config = Kohana::$config->load('api')->vacation_test;
        }

        // Get campaign id
        $campaign_id = $this->request->param('id');

        // Get campaign
        $campaign = ORM::factory('campaign')
                ->where('id', '=', $campaign_id)
                ->where('is_default', '=', 0)
                ->find();

        // Is Authorized ?
        if (!A2::instance()->allowed($campaign, 'delete')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        try {
            // Get hotel id
            $hotel_id = ORM::factory('campaign')
                            ->select(array('rooms.hotel_id', 'hotel_id'))
                            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                            ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                            ->where('campaigns.id', '=', $campaign_id)
                            ->group_by('rooms.hotel_id')
                            ->find()
                    ->hotel_id;

            // Get campaign values for logging
            $log_values = $campaign->as_log();

            // Write log
            ORM::factory('log')
                    ->values(array(
                        'admin_id' => A1::instance()->get_user()->id,
                        'hotel_id' => $hotel_id,
                        'action' => 'Deleted Promotion',
                        'room_texts' => NULL,
                        'text' => "Room Names: :room_names. "
                        . "Benefit: :benefit "
                        . "Booking Dates: :booking_start_date - :booking_end_date. "
                        . "Stay Dates: :stay_start_date - :stay_end_date. "
                        . "Cancellation Policy: :cancellation. ",
                        'data' => serialize(array(
                            ':room_names' => Arr::get($log_values, 'room_names'),
                            ':benefit' => Arr::get($log_values, 'benefit'),
                            ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                            ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                            ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                            ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                            ':cancellation' => Arr::get($log_values, 'cancellation'),
                        )),
                        'created' => time(),
                    ))
                    ->create();

            // Get QA admins emails
            $qa_emails = ORM::factory('admin')
                    ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                    ->where('admins.role', '=', 'quality-assurance')
                    ->where('admins_hotels.hotel_id', '=', $hotel_id)
                    ->find_all()
                    ->as_array('email', 'name');

            // If there is QA admins
            if (count($qa_emails) > 0) {
                // Send email
                Email::factory(strtr(':hotel_name - Deleted Promotion', array(
                            ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                )), strtr(
                                        "Action: Deleted Promotion.\n"
                                        . "By: :admin_username (:admin_name).\n"
                                        . "Room Names: :room_names.\n"
                                        . "Benefit: :benefit\n"
                                        . "Booking Dates: :booking_start_date - :booking_end_date.\n"
                                        . "Stay Dates: :stay_start_date - :stay_end_date.\n"
                                        . "Cancellation Policy: :cancellation.", array(
                            ':admin_username' => A1::instance()->get_user()->username,
                            ':admin_name' => A1::instance()->get_user()->name,
                            ':room_names' => Arr::get($log_values, 'room_names'),
                            ':benefit' => Arr::get($log_values, 'benefit'),
                            ':booking_start_date' => Arr::get($log_values, 'booking_start_date'),
                            ':booking_end_date' => Arr::get($log_values, 'booking_end_date'),
                            ':stay_start_date' => Arr::get($log_values, 'stay_start_date'),
                            ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                            ':cancellation' => Arr::get($log_values, 'cancellation'),
                        )))
                        ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                        ->to($qa_emails)
                        ->send();
            }

            //Set data from vacation trigger
            //filter hotel
            $vacation_triger = array(
                'hotel_id' => $hotel_id,
                'room_id' => NULL,
                'campaign_id' => $campaign_id,
                // 'start_date' =>$booking_data['check_in'], 
                // 'end_date' =>$booking_data['check_out'],
                'item_id' => NULL,
                'start_date' => $campaign->stay_start_date,
                'end_date' => $campaign->stay_end_date,
                'type_trigger' => 4,
                'status' => 0
            );

            //insert data vacation trigger into database
            Controller_Vacation_Trigger::promotionDelete($vacation_triger);

            // Delete campaign
            $campaign->delete();

            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
        } catch (Exception $e) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage() . $e->getFile() . $e->getLine()));
        }

        // Redirect back
        $this->request->redirect($this->request->referrer());
    }

}
