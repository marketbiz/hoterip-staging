<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Log extends Controller_Layout_Admin {
	
	public function action_hotel()
	{
    /// API key redirect if role is api
    if (A1::instance()->get_user()->role == 'api')
    {
      // Redirect to home dashboard
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'api', 'action' => 'edit', 'id' => A1::instance()->get_user()->id)));
    }
    $start_date = $this->request->query('start_date');
    $end_date = $this->request->query('end_date');

    $items_per_page = 100;
    if($this->request->query('limit_log') > 0)
    {
      $items_per_page = $this->request->query('limit_log');
    }

    if ( ! $start_date)
    {
      $start_date = date('Y-m-d', strtotime('yesterday'));
    }
    
    if ( ! $end_date)
    {
      $end_date = date('Y-m-d');
    }
    
    $start_date_timestamp = strtotime($start_date);
    $end_date_timestamp = strtotime($end_date);

    $logs = ORM::factory('log')
      ->select(
        array('admins.username', 'admin_username')
      )
      ->join('admins')->on('admins.id', '=', 'logs.admin_id')
      ->where('logs.hotel_id', '=', $this->selected_hotel->id)
      ->where('logs.created', '>=', $start_date_timestamp)
      ->where('logs.created', '<', $end_date_timestamp + Date::DAY)
      ->order_by('logs.created', 'ASC');

    $total_logs = $logs->count_all();
    $pagination = Pagination::factory(array(
      'total_items' => $total_logs,
      'items_per_page' => $items_per_page
      ));

    $logs = ORM::factory('log') 
    ->select(
      array('admins.username', 'admin_username')
    )
    ->join('admins')->on('admins.id', '=', 'logs.admin_id')
    ->where('logs.hotel_id', '=', $this->selected_hotel->id)
    ->where('logs.created', '>=', $start_date_timestamp)
    ->where('logs.created', '<', $end_date_timestamp + Date::DAY)
    ->offset($pagination->offset)
    ->limit($pagination->items_per_page)
    ->order_by('logs.created', 'ASC')
    ->find_all();
    
		$this->template->main = Kostache::factory('log/hotel')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
      ->set('selected_hotel', $this->selected_hotel)
      ->set('selected_language', $this->selected_language)
      ->set('start_date', $start_date)
      ->set('end_date', $end_date)
      ->set('logs', $logs)
      ->set('pagination', $pagination);
  }
  
}