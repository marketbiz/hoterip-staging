<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Page extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('page', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    $field = $this->request->query('field');
    $keyword = $this->request->query('keyword');
    
    $page = Arr::get($this->request->query(), 'page', 1);
		$items_per_page = 30;
		$offset = ($page - 1) * $items_per_page;
    
    // Get countries
		$countries = ORM::factory('country')
      ->select(
      	array('countries.id', 'country_id'),
      	array('country_texts.name', 'country_name'),
      	array('cities.id', 'city_id'),
      	array('city_texts.name', 'city_name'),
      	array('city_pages.id', 'city_page_id')
      )
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->join('cities')->on('cities.country_id', '=', 'countries.id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('city_pages', 'left')->on('city_pages.city_id', '=', 'cities.id')
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id);

    if ($keyword)
    {
      if ($field == 1)
      {
        $countries->where('city_texts.name', 'LIKE', "%$keyword%");
      }
      elseif ($field == 2)
      {
      	$countries->where('country_texts.name', 'LIKE', "%$keyword%");
      }
    }
    
    // Get total users
    $total_countries = $countries
      ->reset(FALSE)
      ->count_all();

    // Get users
    $countries = $countries
      ->offset($offset)
      ->limit($items_per_page)
      ->find_all();

		// Create pagination
		$pagination = Pagination::factory(array(
			'items_per_page' => $items_per_page,
			'total_items' => $total_countries,
		));
		
		$this->template->main = Kostache::factory('page/manage')
			->set('notice', Notice::render())
      ->set('filters', $this->request->query())
			->set('countries', $countries)
      ->set('pagination', $pagination);
	}
  
  public function action_edit()
  {
    $city_page_id = $this->request->param('id') ? $this->request->param('id') : NULL;
		$country_id = $this->request->query('country_id'); 
		$city_id = $this->request->query('city_id');
		$type = $this->request->query('type');
		
		$now = Date::formatted_time('now', 'Y-m-d');
		$languages = ORM::factory('language')->find_all();

		$descriptions = array();

    if (!A2::instance()->allowed('user', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}

		$hotels = ORM::factory('hotel')
			->select(
				array('hotel_texts.name', 'name'),
				array('city_texts.name', 'city_name'),
        array('country_texts.name', 'country_name'),
        array('district_texts.name', 'district_name'),
        array('campaigns.id', 'campaign_id'),
        array('city_pages.id', 'city_pages_id')
			)
			->join('city_pages', 'left')->on('city_pages.city_id', '=', 'hotels.city_id')
			->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
			->join('districts')->on('districts.id', '=', 'hotels.district_id')
			->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
			->join('cities')->on('cities.id', '=', 'hotels.city_id')
			->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
			->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
			->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
			->join('items')->on('rooms.id', '=', 'items.room_id')
			->where('hotel_texts.language_id', '=', $this->selected_language->id)
			->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->where('district_texts.language_id', '=', $this->selected_language->id)
	    ->where('cities.id', '=', $city_id)
	    ->where('hotels.active', '=', 1)
			->order_by('district_texts.name', 'ASC')
			->group_by('hotels.id')
			->find_all()
			;

		if(!empty($city_page_id))
		{
			$descriptions = DB::select(
	    	array('city_pages.id', 'city_page_id'),
	    	array('city_pages.special_promotions', 'special_promotions'),
	    	array('city_pages.area_hotels', 'area_hotels'),
				array('city_descriptions.name', 'description_name'),
				array('city_descriptions.language_id', 'language_id')
				)
				->from('city_pages')
	  		->join('city_descriptions')->on('city_descriptions.city_pages_id', '=', 'city_pages.id')
				->where('city_pages.id', '=', $city_page_id)
				->as_object()
				->execute();
		}

    // If ajax request
    if ($this->request->is_ajax())
    {
    	$hotel_data = array();

			echo '<table class="list-table" id="'.$this->request->query('type').'">
				<thead>
					<tr>
						<th>Check</th>
						<th>Hotel Name</th>
						<th>Districts Name</th>
					</tr>
				</thead>
				<tbody>';

			foreach ($hotels as $key => $hotel)
			{
				$hotel_data[$key]['id'] = $hotel->id;
				$hotel_data[$key]['name'] = $hotel->name;

				echo	'<tr>';

				if($this->request->query('data') != NULL)
				{
					$datas = explode(",", $this->request->query('data'));

					foreach ($datas as $data) {
				    if($hotel->id == $data)
				    {
				    	switch ($this->request->query('type')) {
				    		case 'special-promotion':
				    			$hotel_data[$key]['special_promotion'] = TRUE;
				    			break;

				    		case 'area-hotel':
				    			$hotel_data[$key]['area_hotel'] = TRUE;
				    			break;

				    		default:
				    			$hotel_data[$key]['special_promotion'] = TRUE;
				    			$hotel_data[$key]['area_hotel'] = TRUE;
				    			break;
				    	}
						}
					}
				}
				else
				{
					foreach ($descriptions as $description)
					{
						if(!empty($description->special_promotions) || $description->special_promotions != '')
						{
					    $special_promotions = unserialize($description->special_promotions);

					    if($special_promotions)
					    {
						    foreach ($special_promotions as $special_promotion)
						    {
							    if($hotel->id == $special_promotion['id'])
							    {
							    	$hotel_data[$key]['special_promotion'] = TRUE;
									}
						    }
						  }

					    $area_hotels = unserialize($description->area_hotels);

					    if($area_hotels)
					    {
						    foreach ($area_hotels as $area_hotel)
						    {
							    if($hotel->id == $area_hotel['id'])
							    {
							    	$hotel_data[$key]['area_hotel'] = TRUE;
							    	break;
							    }
					    	}
					    }
				  	}
					}
				}

				switch ($this->request->query('type')) {
					case 'special-promotion':
				  	if(!empty($hotel_data[$key]['special_promotion']))
				  	{
				  		echo	'<td class="checkbox"><input class="promotion-'.$hotel->id.'" type="checkbox" data-id="'.$hotel->id.'" data-name="'.$hotel->name.'" value="1" checked="checked">';
				  	}
				  	else
				  	{
				  		echo	'<td class="checkbox"><input class="promotion-'.$hotel->id.'" type="checkbox" data-id="'.$hotel->id.'"data-name="'.$hotel->name.'" value="1">';
				  	}
						break;

					case 'area-hotel':
				  	if(!empty($hotel_data[$key]['area_hotel']))
				  	{
				  		echo	'<td class="checkbox"><input class="district-'.$hotel->id.'" type="checkbox" data-id="'.$hotel->id.'" data-name="'.$hotel->name.'" value="1" checked="checked">';
				  	}
				  	else
				  	{
				  		echo	'<td class="checkbox"><input class="district-'.$hotel->id.'" type="checkbox" data-id="'.$hotel->id.'" data-name="'.$hotel->name.'" value="1">';
				  	}
						break;

					default:
						echo "<td>Not defined</td>";
						break;
				}

				echo	'</td><td>'.$hotel->name.'</td>';
				echo	'</td><td>'.$hotel->district_name.'</td>';
				echo	'</tr>';
			}
			echo '</tbody>
			</table>';

      exit;
    }

		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();
				$values['special_promotions'] = NULL;
				$values['area_hotels'] = NULL;

				if(!empty($values['promotion_ids']))
				{
					foreach ($values['promotion_ids'] as $key => $promotion_id ) {
						$promotion[] = array(
							'id' => $key
							); 
					}

					$values['special_promotions'] = serialize($promotion);
				}

				if(!empty($values['area_ids']))
				{
					foreach ($values['area_ids'] as $key => $area_id ) {
						$area[] = array(
							'id' => $key
							); 

					$values['area_hotels'] = serialize($area);
					}
				}

				if($values['city_page_id'] == NULL)
				{
		      $description = DB::insert('city_pages')
		        ->columns(array('country_id', 'city_id', 'special_promotions', 'area_hotels'))
		        ->values(array($values['country_id'], $values['city_id'], $values['special_promotions'], $values['area_hotels']))
		        ->execute();

			    foreach ($languages as $language)
			    {
			      DB::insert('city_descriptions')
			        ->columns(array('city_pages_id', 'language_id', 'name'))
			        ->values(array($description[0], $language->id, $values['names'][$language->id]))
			        ->execute();
			    }
			    $city_page_id = $description[0];
		    }
		    else
		    {
					DB::update('city_pages')
						->set(array(
							'country_id' => $values['country_id'],
							'city_id' => $values['city_id'],
							'special_promotions' => $values['special_promotions'],
							'area_hotels' => $values['area_hotels'],
						))
		        ->where('id', '=', $city_page_id)
						->execute();

			    foreach ($languages as $language)
			    {
						DB::update('city_descriptions')
							->set(array(
								'language_id' => $language->id,
								'name' => $values['names'][$language->id],
							))
			        ->where('city_pages_id', '=', $city_page_id)
							->where('language_id', '=', $language->id)
							->execute();
			    }
		    }

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));

				// Redirect to home dashboard
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'page', 'action' => 'edit', 'id' => $city_page_id.'?country_id='.$country_id.'&city_id='.$city_id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('benefit'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}

    // Get countries
		$countries = ORM::factory('country')
      ->select(
      	array('countries.id', 'country_id'),
      	array('countries.url_segment', 'country_segment'),
      	array('country_texts.name', 'country_name'),
      	array('cities.id', 'city_id'),
      	array('cities.url_segment', 'city_segment'),
      	array('city_texts.name', 'city_name')
      )
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->join('cities')->on('cities.country_id', '=', 'countries.id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->where('cities.id', '=', $city_id)
      ->where('countries.id', '=', $country_id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->find();

			$this->template->main = Kostache::factory('page/edit')
				->set('notice', Notice::render())
	      ->set('filters', $this->request->query())
				->set('languages', $languages)
				->set('country_id', $countries->country_id)
				->set('country_segment', $countries->country_segment)
				->set('country_name', $countries->country_name)
				->set('city_id', $countries->city_id)
				->set('city_segment', $countries->city_segment)
				->set('city_name', $countries->city_name)
      	->set('city_page_id', $city_page_id)
				->set('hotels', $hotels)
				->set('descriptions', $descriptions);
  }
}