<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Banner extends Controller_Layout_Admin {
	
	public function before()
	{
		if (in_array($this->request->action(), array('reorder', 'delete')))
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
  
  public function action_reorder()
  {
    // If there is post
    if ($this->request->post())
    {
      // Get banners id
      $banner_ids = $this->request->post('banner_ids');

      try
      {
        foreach ($banner_ids as $key => $banner_id)
        {
          // Update order
          ORM::factory('banner')
            ->where('id', '=', $banner_id)
            ->find()
            ->set('order', $key + 1)
            ->update();
        }

        $return = array(
          'success' => TRUE,
          'message' => Kohana::message('general', 'save_success'),
        );
      }
      catch (Exception $e)
      {
        $return = array(
          'success' => FALSE,
          'message' => $e->getMessage(),
        );
      }

      $this->response->body(json_encode($return));
    }
  }
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('banner', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		$banners = ORM::factory('banner')
      ->select(
        array('languages.code', 'language_code'),
        array('languages.name', 'language_name')
      )
      ->join('languages')->on('languages.id', '=', 'banners.language_id')
      ->order_by('order', 'ASC')
			->find_all()
      ->as_array();
    
		$this->template->main = Kostache::factory('banner/manage')
			->set('notice', Notice::render())
      ->set('banner_url', Kohana::config('application.banner_url'))
			->set('banners', $banners);
	}
	
	public function action_add()
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('banner', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage cities
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('submit'))
		{
			// Create files rule
			$files = Validation::factory($_FILES)
				->rule('image', 'Upload::not_empty', array(':value'))
				->rule('image', 'Upload::valid', array(':value'))
        ->rule('image', 'Upload::size', array(':value', '1M'))
				->rule('image', 'Upload::type', array(':value', array('jpg')))
				->label('image', 'Image');
			
			// If file valid
			if ($files->check())
			{
				$values = $this->request->post();
        $values['order'] =  ORM::factory('banner')->count_all() + 1;
        
				$banner = ORM::factory('banner')
					->create_banner($values);
				
				try
				{
					// Get image file
					$image = $_FILES['image'];

					// Get hotel directory
					$directory = Kohana::config('application.banner_directory');

					// If directory not exist
					if ( ! is_dir($directory))
					{
						// Create directory
						mkdir($directory);
					}

					// Save physical file
					$filename = Upload::save($image, $banner->id.'.jpg', $directory);

					// Check Mime Type
					$fileinfo = mime_content_type($filename);
					
						switch ($fileinfo) {
							case 'image/jpeg':
								break;
							case 'image/png':
								break;
							case 'image/gif':
								break;
							
							default:

								// Rollback transaction
								Database::instance()->rollback();
								
								unlink($filename);
								
								throw new Exception('Invalid Image Type');

								break;
						}


					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
					
					// Redirect to edit image
					$this->request->redirect(Route::get('default')->uri(array('controller' => 'banner', 'action' => 'edit', 'id' => $banner->id)));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, $e->getMessage());
				}
			}
			else
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $files->errors('banner'));
			}
		}
    
    $languages = ORM::factory('language')
      ->find_all();
    
		$this->template->main = Kostache::factory('banner/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
      ->set('languages', $languages);
	}
	
	public function action_edit()
	{
    // Get banner id
    $banner_id = $this->request->param('id');
    
    // Get banner
    $banner = ORM::factory('banner')
      ->where('id', '=', $banner_id)
      ->find();
      
    // Is Authorized ?
		if ( ! A2::instance()->allowed('banner', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage cities
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('submit'))
		{
			// Create files rule
			$files = Validation::factory($_FILES)
				->rule('image', 'Upload::not_empty', array(':value'))
				->rule('image', 'Upload::valid', array(':value'))
        ->rule('image', 'Upload::size', array(':value', '1M'))
				->rule('image', 'Upload::type', array(':value', array('jpg')))
				->label('image', 'Image');
			
			// If file valid
			if ($files->check())
			{
				$values = $this->request->post();
        
				$banner = ORM::factory('banner')
          ->where('id', '=', $banner_id)
          ->find()
					->update_banner($values);
				
				try
				{
					// Get image file
					$image = $_FILES['image'];

					// Get hotel directory
					$directory = Kohana::config('application.banner_directory');

					// If directory not exist
					if ( ! is_dir($directory))
					{
						// Create directory
						mkdir($directory);
					}

					// Save physical file
					$filename = Upload::save($image, $banner->id.'.jpg', $directory);

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
					
					// Redirect to edit image
					$this->request->redirect(Route::get('default')->uri(array('controller' => 'banner', 'action' => 'edit', 'id' => $banner->id)));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, $e->getMessage());
				}
			}
			else
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $files->errors('banner'));
			}
		}
    
    $languages = ORM::factory('language')
      ->find_all();
    
		$this->template->main = Kostache::factory('banner/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
      ->set('banner_url', Kohana::config('application.banner_url'))
      ->set('banner', $banner)
      ->set('languages', $languages)
      ->set('time', time());
	}


	public function action_delete()
	{
		// Get banner id
		$banner_id = $this->request->param('id');
		
		// Get banner
		$banner = ORM::factory('banner')
			->where('id', '=', $banner_id)
			->find();
		
		if (A2::instance()->allowed('banner', 'delete'))
		{
			try
			{
        $filename = Kohana::config('application.banner_directory').'/'.$banner_id.'.jpg';
        
				// If file exist
				if (file_exists($filename))
				{
					unlink($filename);
				}
			
				// Delete banner
				$banner->delete();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}