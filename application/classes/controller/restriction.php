<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Restriction extends Controller_Layout_Admin {
	
	public function action_manage()
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('restriction', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		/**
		 * Get values from post
		 */
    
		$start_date = $this->request->post('start_date');
		$end_date = $this->request->post('end_date');
		
		/**
		 * Filter 
		 */
		
		if ( ! $start_date)
		{
			$start_date = date('Y-m-d');
		}
		
		if ( ! $end_date)
		{
      $end_date = date('Y-m-d', strtotime('+7 days'));
		}
		
		$start_date_timestamp = strtotime($start_date);
		$end_date_timestamp = strtotime($end_date);
		
		// If end date before start date
		if ($end_date_timestamp < $start_date_timestamp)
		{
			// Swap start date with end date
			$temp = $start_date;
			$start_date = $end_date;
			$end_date = $temp;
			
			$start_date_timestamp = strtotime($start_date);
			$end_date_timestamp = strtotime($end_date);
		}
    
		/**
		 * Form submitted
		 */
    
		if ($this->request->post('submit'))
		{
			// Get dates
			$dates = $this->request->post('dates');
			
			// Get QA admins emails
			$qa_emails = ORM::factory('admin')
				->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
				->where('admins.role', '=', 'quality-assurance')
				->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
				->find_all()
				->as_array('email', 'name');
      
      foreach ($dates as $date)
      {
        $values = array(
          'date' => $date,
          'is_no_arrival' => (int) Arr::get($this->request->post('is_no_arrivals'), $date, 0),
          'is_no_departure' => (int) Arr::get($this->request->post('is_no_departures'), $date, 0),
        );
        
        if ($values['is_no_arrival'])
        {
          // Create no arrival
          ORM::factory('hotel_no_arrival')
            ->values(array(
              'hotel_id' => $this->selected_hotel->id,
              'date' => $date,
            ))
            ->create();
          
          // Write log
          ORM::factory('log')
            ->values(array(
              'admin_id' => A1::instance()->get_user()->id,
              'hotel_id' => $this->selected_hotel->id,
              'action' => 'Changed No Arrival',
              'room_texts' => NULL,
              'text' => 'Date: :date. No Arrival: :is_no_arrival',
              'data' => serialize(array(
                ':date' => date('M j, Y', strtotime($date)),
                ':is_no_arrival' => 'Yes',
              )),
              'created' => time(),
            ))
            ->create();
					
					// If there is QA admins
					if (count($qa_emails) > 0)
					{
						// Send email
						Email::factory(strtr(':hotel_name - Changed No Arrival', array(
								':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
							)), strtr(
							"Action: Changed No Arrival.\n"
							."By: :admin_username (:admin_name).\n"
							."Date: :date.\n"
							."No Arrival: :is_no_arrival.",
							array(
								':admin_username' => A1::instance()->get_user()->username,
								':admin_name' => A1::instance()->get_user()->name,
								':date' => date('M j, Y', strtotime($date)),
                ':is_no_arrival' => 'Yes',
							)))
							->from(Kohana::config('application.email'), Kohana::config('application.name'))
							->to($qa_emails)
							->send();
					}
        }
        else
        {
          // Find no arrival
          $no_arrival = ORM::factory('hotel_no_arrival')
            ->where('hotel_id', '=', $this->selected_hotel->id)
            ->where('date', '=', $date)
            ->find();
          
          // If exist
          if ($no_arrival->loaded())
          {
            // Delete
            DB::delete('hotel_no_arrivals')
              ->where('hotel_id', '=', $this->selected_hotel->id)
              ->where('date', '=', $date)
              ->execute();

            // Write log
            ORM::factory('log')
              ->values(array(
                'admin_id' => A1::instance()->get_user()->id,
                'hotel_id' => $this->selected_hotel->id,
                'action' => 'Changed No Arrival',
                'room_texts' => NULL,
                'text' => 'Date: :date. No Arrival: :is_no_arrival',
                'data' => serialize(array(
                  ':date' => date('M j, Y', strtotime($date)),
                  ':is_no_arrival' => 'No',
                )),
                'created' => time(),
              ))
              ->create();
						
						// If there is QA admins
						if (count($qa_emails) > 0)
						{
							// Send email
							Email::factory(strtr(':hotel_name - Changed No Arrival', array(
									':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
								)), strtr(
								"Action: Changed No Arrival.\n"
								."By: :admin_username (:admin_name).\n"
								."Date: :date.\n"
								."No Arrival: :is_no_arrival.",
								array(
									':admin_username' => A1::instance()->get_user()->username,
									':admin_name' => A1::instance()->get_user()->name,
									':date' => date('M j, Y', strtotime($date)),
									':is_no_arrival' => 'No',
								)))
								->from(Kohana::config('application.email'), Kohana::config('application.name'))
								->to($qa_emails)
								->send();
						}
          }
        }
        
        if ($values['is_no_departure'])
        {
          ORM::factory('hotel_no_departure')
            ->values(array(
              'hotel_id' => $this->selected_hotel->id,
              'date' => $date,
            ))
            ->create();
          
          // Write log
          ORM::factory('log')
            ->values(array(
              'admin_id' => A1::instance()->get_user()->id,
              'hotel_id' => $this->selected_hotel->id,
              'action' => 'Changed No Departure',
              'room_texts' => NULL,
              'text' => 'Date: :date. No Departure: :is_no_departure',
              'data' => serialize(array(
                ':date' => date('M j, Y', strtotime($date)),
                ':is_no_departure' => 'Yes',
              )),
              'created' => time(),
            ))
            ->create();
					
					// If there is QA admins
					if (count($qa_emails) > 0)
					{
						// Send email
						Email::factory(strtr(':hotel_name - Changed No Departure', array(
								':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
							)), strtr(
							"Action: Changed No Departure.\n"
							."By: :admin_username (:admin_name).\n"
							."Date: :date.\n"
							."No Departure: :is_no_departure.",
							array(
								':admin_username' => A1::instance()->get_user()->username,
								':admin_name' => A1::instance()->get_user()->name,
								':date' => date('M j, Y', strtotime($date)),
                ':is_no_departure' => 'Yes',
							)))
							->from(Kohana::config('application.email'), Kohana::config('application.name'))
							->to($qa_emails)
							->send();
					}
        }
        else
        {
          // Find no departure
          $no_departure = ORM::factory('hotel_no_departure')
            ->where('hotel_id', '=', $this->selected_hotel->id)
            ->where('date', '=', $date)
            ->find();
          
          // If exist
          if ($no_departure->loaded())
          {
            // Delete
            DB::delete('hotel_no_departures')
              ->where('hotel_id', '=', $this->selected_hotel->id)
              ->where('date', '=', $date)
              ->execute();
          
            // Write log
            ORM::factory('log')
              ->values(array(
                'admin_id' => A1::instance()->get_user()->id,
                'hotel_id' => $this->selected_hotel->id,
                'action' => 'Changed No Departure',
                'room_texts' => NULL,
                'text' => 'Date: :date. No Departure: :is_no_departure',
                'data' => serialize(array(
                  ':date' => date('M j, Y', strtotime($date)),
                  ':is_no_departure' => 'No',
                )),
                'created' => time(),
              ))
              ->create();
						
						// If there is QA admins
						if (count($qa_emails) > 0)
						{
							// Send email
							Email::factory(strtr(':hotel_name - Changed No Departure', array(
									':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
								)), strtr(
								"Action: Changed No Departure.\n"
								."By: :admin_username (:admin_name).\n"
								."Date: :date.\n"
								."No Departure: :is_no_departure.",
								array(
									':admin_username' => A1::instance()->get_user()->username,
									':admin_name' => A1::instance()->get_user()->name,
									':date' => date('M j, Y', strtotime($date)),
									':is_no_departure' => 'No',
								)))
								->from(Kohana::config('application.email'), Kohana::config('application.name'))
								->to($qa_emails)
								->send();
						}
          }
        }
      }
      // Add success notice
      Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));

    	/*
			 *  Restrictions Notification for API
			 */

    	$values = $this->request->post();
    	$valuesDate = $values['dates'];

    	$valueStartDate = reset($valuesDate);
    	$valueEndDate = end($valuesDate);

      //Set data from vacation trigger
      $vacation_triger = array(
        'hotel_id' => $this->selected_hotel,
        'room_id' => NULL, 
        'campaign_id' => NULL, 
        // 'start_date' =>$booking_data['check_in'], 
        // 'end_date' =>$booking_data['check_out'],
        'item_id' => NULL,
        'start_date' => $valueStartDate,
        'end_date' => $valueEndDate,
        'type_trigger' => 5,
        'status' => 0
        );

        //insert data vacation trigger into database
        Controller_Vacation_Trigger::insertData($vacation_triger);

			// Add Hotel
			// $values= $this->request->post();
			// $values['hotel_id'] = $this->selected_hotel->id;
			// $values['end_date'] = date( "Y-m-d", strtotime( "$end_date +1 day" ) );

      $values = $this->request->post();
      $valuesDate = $values['dates'];
      $startDateValues = reset($valuesDate);
      $endDateValues = (end($valuesDate));

      //Set data from vacation trigger
      $vacation_triger = array(
        'hotel_id' => $this->selected_hotel->id,
        'room_id' => NULL, 
        'campaign_id' => NULL, 
        // 'start_date' =>$booking_data['check_in'], 
        // 'end_date' =>$booking_data['check_out'],
        'item_id' => NULL,
        'start_date' => $startDateValues,
        'end_date' => $endDateValues,
        'type_trigger' => 5,
        'status' => 0
        );

        //insert data vacation trigger into database
        Controller_Vacation_Trigger::insertData($vacation_triger);			
			// Logs
			// Log::instance()->add(Log::INFO, 'API Vacation Items');
			// Log::instance()->add(Log::INFO, 'Admin ID: '.A1::instance()->get_user());
			// Log::instance()->add(Log::INFO, 'Hotel ID: '.$this->selected_hotel->id);

			// // Vacation notifictions
			// $availability = Controller_Vacation_Notifications_Availability::restrictions($values) ? 
			//  'success' : 'failed';
			
			//  Not Used yet
			// $rateplan = Controller_Vacation_Notifications_Rateplan::restrictions($values) ? 
			//  'success' : 'failed';

			//  Not Used yet
			// $amount = Controller_Vacation_Notifications_Amount::restrictions($values) ? 
			//  'success' : 'failed';

			// Sucess or error message vacation

			// if($availability == 'success')
			// {
			// 	Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_availability_vacation_save_success'));
			// }
			// elseif($availability == 'failed')
			// {
			// 	Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_availability_vacation_save_failure'));
			// }

			// if($rateplan == 'success')
			// {
			// 	Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_notif_save_success'));
			// }
			// elseif($rateplan == 'failed')
			// {
			// 	Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_notif_save_failure'));
			// }

			// if($amount == 'success')
			// {
			// 	Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_amount_save_success'));
			// }
			// elseif($amount == 'failed')
			// {
			// 	Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_amount_notif_save_failure'));
			// }
		}
		
		/**
		 * Build variables for view
		 */
		
		// Create dates array
		$dates = array();
		for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY)
		{
			$dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
		}		
		
		$no_arrivals = ORM::factory('hotel_no_arrival')
      ->where('hotel_id', '=', $this->selected_hotel->id)
      ->where('date', '>=', $start_date)
      ->where('date', '<=', $end_date)
			->find_all()
			->as_array('date');
    
    $no_departures = ORM::factory('hotel_no_departure')
      ->where('hotel_id', '=', $this->selected_hotel->id)
      ->where('date', '>=', $start_date)
      ->where('date', '<=', $end_date)
			->find_all()
			->as_array('date');
    
		$this->template->main = Kostache::factory('restriction/manage')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
      ->set('logged_in_admin', A1::instance()->get_user())
			->set('start_date', $start_date)
			->set('end_date', $end_date)
			->set('dates', $dates)
      ->set('no_arrivals', $no_arrivals)
      ->set('no_departures', $no_departures);
	}
	
}