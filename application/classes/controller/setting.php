<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Setting extends Controller_Layout_Admin {
  
  public function action_edit()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('setting', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    if ($this->request->post('submit'))
    {
      // Get posted settings
      $settings = $this->request->post('settings');
      
      try
      {
        // Start transaction
        Database::instance()->begin();
        
        // Loop each settings
        foreach ($settings as $key => $value)
        {
          // Save setting
          ORM::factory('setting')
            ->where('key', '=', $key)
            ->find()
            ->set('value', $value)
            ->save();
        }
        
        // Commit transaction
        Database::instance()->commit();
        
        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
      }
      catch (ORM_Validation_Exception $e)
      {
        // Rollback transaction
        Database::instance()->rollback();
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('setting'));
      }
    }
    
    $setting = ORM::factory('setting')
      ->find_all()
      ->as_array('key', 'value');
    
    $this->template->main = Kostache::factory('setting/edit')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('setting', $setting);
  }
  
}
	