<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Photo extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage()
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('hotel_photo', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
		
		$hotel = ORM::factory('hotel')
			->select(
				array('hotel_photos.basename', 'photo_basename')
			)
			->join('hotel_photos', 'left')->on('hotel_photos.id', '=', 'hotels.hotel_photo_id')
			->where('hotels.id', '=', $this->selected_hotel->id)
			->find();
    
		$hotel_photos = ORM::factory('hotel_photo')
			->select(
				array('hotel_photo_texts.name', 'name')
			)
			->join('hotel_photo_texts')->on('hotel_photo_texts.hotel_photo_id', '=', 'hotel_photos.id')
			->where('hotel_photos.hotel_id', '=', $this->selected_hotel->id)
			->where('hotel_photo_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name')
			->find_all();
		
		$this->template->main = Kostache::factory('photo/manage')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('selected_hotel', $this->selected_hotel)
			->set('hotel', $hotel)
			->set('hotel_photos', $hotel_photos);
	}
	
	public function action_main()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('hotel_photo', 'main'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				// Get hotel
				$hotel = ORM::factory('hotel')
					->where('id', '=', $this->selected_hotel->id)
					->find();
				
				// Update hotel photo
				$hotel->hotel_photo_id = Arr::get($this->request->post(), 'hotel_photo_id', NULL);
				$hotel->update();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'photo', 'action' => 'manage')), Kohana::message('photo', 'manage_photos'))));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		// Get hotel
		$hotel = ORM::factory('hotel')
			->where('id', '=', $this->selected_hotel->id)
			->find();
		
		// Get hotel photos
		$hotel_photos = ORM::factory('hotel_photo')
			->select(
				array('hotel_photo_texts.name', 'name')
			)
			->join('hotel_photo_texts')->on('hotel_photo_texts.hotel_photo_id', '=', 'hotel_photos.id')
			->where('hotel_photos.hotel_id', '=', $this->selected_hotel->id)
			->where('hotel_photo_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name')
			->find_all(); 
		
		$this->template->main = Kostache::factory('photo/main')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('hotel', $hotel)
			->set('hotel_photos', $hotel_photos);
	}
	
	public function action_add()
	{
		if ($this->request->post('submit'))
		{
			// Create files rule
			$files = Validation::factory($_FILES)
				->rule('photo', 'Upload::not_empty', array(':value'))
				->rule('photo', 'Upload::valid', array(':value'))
        ->rule('photo', 'Upload::size', array(':value', '1M'))
				->rule('photo', 'Upload::type', array(':value', array('jpg', 'gif', 'png')))
				->label('photo', 'Photo File');
			
			// If file valid
			if ($files->check())
			{
        // Start transaction
        Database::instance()->begin();
        
        try
				{
          $values = $this->request->post();
          $values['hotel_id'] = $this->selected_hotel->id;

          // Get field value from post array
          $names = array_filter(Arr::get($values, 'names', array()));

          // Get default language id
          $default_language_id = ORM::factory('language')
            ->where('code', '=', 'en-us')
            ->find()
            ->id;

          // Create base filename
          $base_filename = $this->selected_hotel->url_segment.'-'.URL::title(Arr::get($names, $default_language_id));

          $basename = $base_filename.'.jpg';
          $large_basename = $base_filename.'-large.jpg';
          $medium_basename = $base_filename.'-medium.jpg';
          $small_basename = $base_filename.'-small.jpg';
          
          $counter = 2;
          $stop = FALSE;

          do 
          {
            if (ORM::factory('hotel_photo')
              ->where('hotel_id', '=', $this->selected_hotel->id)
              ->where('basename', '=', $basename)
              ->count_all() > 0)
            {
              $basename = $base_filename.'-'.$counter.'.jpg';
              $large_basename = $base_filename.'-'.$counter.'-large.jpg';
              $medium_basename = $base_filename.'-'.$counter.'-medium.jpg';
              $small_basename = $base_filename.'-'.$counter.'-small.jpg';
              
              $counter++;
            }
            else
            {
              $counter--;
              $stop = TRUE;
            }
          } while ( ! $stop);

          $values['basename'] = $basename;
          $values['small_basename'] = $small_basename;
          $values['medium_basename'] = $medium_basename;
          $values['large_basename'] = $large_basename;

          // Create hotel photo
          $hotel_photo = ORM::factory('hotel_photo')
            ->create_hotel_photo($values);
				
					// Get photo file
					$photo = $_FILES['photo'];

					// Get hotel directory
					$directory = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment;

					// If directory not exist
					if ( ! is_dir($directory))
					{
						// Create directory
						mkdir($directory);
					}

					// Save physical file
					$filepath = Upload::save($photo, $hotel_photo->basename, $directory);

					// Set file name
					$standard_filepath = $directory.'/'.$hotel_photo->basename;
					$large_filepath = $directory.'/'.$hotel_photo->large_basename;
					$medium_filepath = $directory.'/'.$hotel_photo->medium_basename;
					$small_filepath = $directory.'/'.$hotel_photo->small_basename;

					// Check Mime Type
					$fileinfo = mime_content_type($standard_filepath);
						switch ($fileinfo) {
							case 'image/jpeg':
								break;
							case 'image/png':
								break;
							case 'image/gif':
								break;
							
							default:

								// Rollback transaction
								Database::instance()->rollback();
								
								unlink($standard_filepath);
								
								throw new Exception('Invalid Image Type');

								break;
						}
						
					// Resize image
					Image::factory($filepath)
						->resize(640, 480, Image::INVERSE)
						->crop(640, 480)
						->save($large_filepath);
					
					// Resize image
					Image::factory($filepath)
						->resize(320, 240, Image::INVERSE)
						->crop(320, 240)
						->save($medium_filepath);
					
					// Resize image
					Image::factory($filepath)
						->resize(168, 126, Image::INVERSE)
						->crop(168, 126)
						->save($standard_filepath);

					// Resize image
					Image::factory($filepath)
						->resize(80, 60, Image::INVERSE)
						->crop(80, 60)
						->save($small_filepath);
          
          // Commit transaction
          Database::instance()->commit();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'photo', 'action' => 'add')), Kohana::message('photo', 'add_photo'))));
					
					// Redirect to edit photo
					$this->request->redirect(Route::get('default')->uri(array('controller' => 'photo', 'action' => 'edit', 'id' => $hotel_photo->id)));
				}
				catch (Exception $e)
				{
          // Rollback transaction
          Database::instance()->rollback();
					// Add error notice
					Notice::add(Notice::ERROR, $e->getMessage());
				}
			}
			else
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $files->errors('photo'));
			}
		}
		
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('photo/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('languages', $languages);
	}
	
	public function action_edit()
	{
		// Get hotel photo id
		$hotel_photo_id = (int) $this->request->param('id');
		
		// Get hotel photo
		$hotel_photo = ORM::factory('hotel_photo')
			->where('id', '=', $hotel_photo_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed($hotel_photo, 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage rooms
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'photo', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
      // Start transaction
      Database::instance()->begin();
      
			try
			{
				$values = $this->request->post();
				$values['hotel_id'] = $this->selected_hotel->id;

				// Get field value from post array
				$names = array_filter(Arr::get($values, 'names', array()));

				// Set default language id using english
				$default_language_id = ORM::factory('language')
					->where('code', '=', 'en-us')
					->find()
					->id;
        
        // Create base filename
        $base_filename = $this->selected_hotel->url_segment.'-'.URL::title(Arr::get($names, $default_language_id));

        $basename = $base_filename.'.jpg';
        $large_basename = $base_filename.'-large.jpg';
        $medium_basename = $base_filename.'-medium.jpg';
        $small_basename = $base_filename.'-small.jpg';

        $counter = 2;
        $stop = FALSE;

        do 
        {
          if (ORM::factory('hotel_photo')
            ->where('hotel_id', '=', $this->selected_hotel->id)
            ->where('basename', '=', $basename)
            ->count_all() > 0)
          {
            $basename = $base_filename.'-'.$counter.'.jpg';
            $large_basename = $base_filename.'-'.$counter.'-large.jpg';
            $medium_basename = $base_filename.'-'.$counter.'-medium.jpg';
            $small_basename = $base_filename.'-'.$counter.'-small.jpg';

            $counter++;
          }
          else
          {
            $counter--;
            $stop = TRUE;
          }
        } while ( ! $stop);

        $values['basename'] = $basename;
        $values['small_basename'] = $small_basename;
        $values['medium_basename'] = $medium_basename;
        $values['large_basename'] = $large_basename;

				// Get old photo filepath
				$old_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->basename;
        $old_small_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->small_basename;
        $old_medium_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->medium_basename;
        $old_large_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->large_basename;

				// Update hotel photo
				$hotel_photo->update_hotel_photo($values);

				// Get new photo filepath
				$new_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->basename;
        $new_small_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->small_basename;
        $new_medium_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->medium_basename;
        $new_large_filepath = Kohana::config('application.hotel_images_directory').'/'.$this->selected_hotel->url_segment.'/'.$hotel_photo->large_basename;

				// Rename files
				if (file_exists($old_filepath))
        {
          rename($old_filepath, $new_filepath);
        }
				
				// Rename files
        if (file_exists($old_large_filepath))
        {
          rename($old_large_filepath, $new_large_filepath);
        }
        
        if (file_exists($old_medium_filepath))
        {
          rename($old_medium_filepath, $new_medium_filepath);
        }
        
        if (file_exists($old_small_filepath))
        {
          rename($old_small_filepath, $new_small_filepath);
        }
        
        // Commit transaction
        Database::instance()->commit();
				
				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'photo', 'action' => 'manage')), Kohana::message('photo', 'manage_photos'))));
			}
			catch (ORM_Validation_Exception $e)
			{
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->erros('photo'));
			}
			catch (Exception $e)
			{
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		// Get hotel photo
		$hotel_photo = ORM::factory('hotel_photo')
			->where('id', '=', $hotel_photo_id)
			->find();
		
		// Get hotel photo texts
		$hotel_photo_texts = ORM::factory('hotel_photo_text')
			->where('hotel_photo_id', '=', $hotel_photo->id)
			->find_all()
			->as_array('language_id');
		
		// Get languages
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('photo/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('hotel_photo', $hotel_photo)
			->set('hotel_photo_texts', $hotel_photo_texts)
			->set('languages', $languages)
			->set('selected_hotel', $this->selected_hotel);
	}
	
	public function action_delete()
	{
		// Get hotel_photo id
		$hotel_photo_id = $this->request->param('id');
		
		// Get hotel_photo
		$hotel_photo = ORM::factory('hotel_photo')
			->where('id', '=', $hotel_photo_id)
			->find();
		
		if (A2::instance()->allowed($hotel_photo, 'delete'))
		{
			try
			{
				// Get hotel
				$hotel = ORM::factory('hotel')
					->where('id', '=', $hotel_photo->hotel_id)
					->find();
				
				// Get filepath
				$filepath = Kohana::config('application.hotel_images_directory').'/'.$hotel->url_segment.'/'.$hotel_photo->basename;
        $small_filepath = Kohana::config('application.hotel_images_directory').'/'.$hotel->url_segment.'/'.$hotel_photo->small_basename;
        $medium_filepath = Kohana::config('application.hotel_images_directory').'/'.$hotel->url_segment.'/'.$hotel_photo->medium_basename;
        $large_filepath = Kohana::config('application.hotel_images_directory').'/'.$hotel->url_segment.'/'.$hotel_photo->large_basename;

				if (file_exists($filepath))
				{
          unlink($filepath);
				}
        
        if (file_exists($small_filepath))
				{
          unlink($small_filepath);
				}
				
				if (file_exists($medium_filepath))
				{
          unlink($medium_filepath);
				}
        
        if (file_exists($large_filepath))
				{
          unlink($large_filepath);
				}
			
				// Delete hotel_photo
				$hotel_photo->delete();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}