<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of husen
 *
 * @author travolta
 */
// classes/controller/baz/bar.php
class Controller_Husen extends Controller {

    //put your code here

    public function action_index() {

        die('husen');
    }

    public function action_cancelsiteminder() {


        $request = $this->xml_cancel();

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: " . Kohana::config('api.siteminder.url_connect'),
            "Content-length: " . strlen($request),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Kohana::config('api.siteminder.url_connect'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Content-length: " . strlen($request)));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $response = curl_exec($ch);
        // Close the handle
        curl_close($ch);
        echo "<pre>";
        print_r($response);
        echo "</pre>";
    }

    private function xml_cancel() {
        $xml = '<?xml version="1.0" encoding="iso-8859-1"?>
            <soap-env:envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
    <soap-env:header>
        <wsse:security soap:mustunderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <wsse:usernametoken>
                <wsse:username>Hoterip</wsse:username>
                <wsse:password type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">qw7Y4S98fq2Z4X5</wsse:password>
            </wsse:usernametoken>
        </wsse:security>
    </soap-env:header>
    <soap-env:body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <ota_hotelresnotifrq xmlns="http://www.opentravel.org/OTA/2003/05" resstatus="Cancel" version="1.0" echotoken="uuid-AB84EB80-FC34-250B-AFAA-D7B23EC6A26A-A" timestamp="2015-10-19T10:29:33+08:00">
            <pos>
                <source>
                    <requestorid type="14" id="HTP">
                        <bookingchannel primary="true" type="7">
                            <companyname code="HOT">Hoterip</companyname>
                        </bookingchannel>
                    </requestorid>
                </source>
            </pos>
            <hotelreservations>
                <hotelreservation lastmodifydatetime="2015-10-19T10:29:33+08:00">
                    <uniqueid type="3001" id="14591">
                        <roomstays>
                            <roomstay promotioncode="3886">
                                <roomtypes>
                                    <roomtype roomtypecode="3001" numberofunits="1">
                                        <roomdescription name="Deluxe Room">
                                        </roomdescription>
                                    </roomtype>
                                </roomtypes>
                                <rateplans>
                                    <rateplan rateplancode="3886">
                                        <rateplandescription>Default Promotions.</rateplandescription>
                                    </rateplan>
                                </rateplans>
                                <roomrates>
                                    <roomrate rateplancode="3886" roomtypecode="3001" numberofunits="1">
                                        <rates>
                                            <rate effectivedate="2015-06-20" expiredate="2015-06-21" unitmultiplier="1" ratetimeunit="Day">
                                                <base amountaftertax="20.36" amountbeforetax="16.83" currencycode="USD"/>
                                            </rate>
                                        </rates>
                                    </roomrate>
                                </roomrates>
                                <guestcounts>
                                    <guestcount agequalifyingcode="10" count="1">
                                    </guestcount>
                                </guestcounts>
                                <timespan start="2015-06-20" end="2015-06-21">
                                    <total amountaftertax="20.36" amountbeforetax="16.83" currencycode="USD">
                                        <basicpropertyinfo hotelcode="1117" hotelname="Cempaka Hotel Lovina">
                                            <comments>
                                                <comment guestviewable="true">
                                                    <text></text>
                                                </comment>
                                            </comments>
                                            <specialrequests>
                                            </specialrequests>
                                        </basicpropertyinfo>
                                    </total>
                                </timespan>
                            </roomstay>
                        </roomstays>
                        <resguests>
                            <resguest resguestrph="1" primaryindicator="1">
                                <profiles>
                                    <profileinfo>
                                        <profile profiletype="1">
                                            <customer>
                                                <personname>
                                                    <givenname>PUTU</givenname>
                                                    <surname>AFRIONI</surname>
                                                </personname>
                                                <email>sukadana@his-world.com</email>
                                            </customer>
                                        </profile>
                                    </profileinfo>
                                </profiles>
                            </resguest>
                        </resguests>
                        <resglobalinfo>
                            <total amountaftertax="20" amountbeforetax="17" currencycode="USD">
                                <taxes>
                                </taxes>
                            </total>
                            <hotelreservationids>
                                <hotelreservationid resid_type="14" resid_value="14591">
                                </hotelreservationid>
                            </hotelreservationids>
                        </resglobalinfo>
                    </uniqueid>
                </hotelreservation>
            </hotelreservations>
        </ota_hotelresnotifrq>
    </soap-env:body>
</soap-env:envelope>';
        $doc = new DOMDocument('1.0');
        $doc->loadXML($xml);
        return $doc->saveXML();
//        return $xml;
    }

    public function action_query() {
        $booking_exist = DB::select(
                                'bookings.*', 'booking_datas.*', 'api_reservation_queues.api', 'api_reservation_queues.request_type', 'api_reservation_queues.retries'
                        )
                        ->from('api_reservation_queues')
                        ->join('bookings')->on('bookings.id', '=', 'api_reservation_queues.booking_id')
                        ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id');
        // ->where_open()
        //   ->where('bookings.siteminder_notifications', '=', 0)
        //   ->or_where('bookings.rategain_notifications', '=', 0)
        // ->where_close()
        // ->where('bookings.created', '<=', strtotime("now -10 minutes"))
        // Siteminder empty response bypass temporary fix
        // ->where('bookings.id', '>', '12680')
//                ->execute()
//                ->current();
        die($booking_exist);
    }

    public function action_notifreservation() {
        set_time_limit(0);

        $booking_exist = DB::select(
                        'bookings.*', 'booking_datas.*', 'api_reservation_queues.api', 'api_reservation_queues.request_type', 'api_reservation_queues.retries'
                )
                ->from('api_reservation_queues')
                ->join('bookings')->on('bookings.id', '=', 'api_reservation_queues.booking_id')
                ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                // ->where_open()
                //   ->where('bookings.siteminder_notifications', '=', 0)
                //   ->or_where('bookings.rategain_notifications', '=', 0)
                // ->where_close()
                // ->where('bookings.created', '<=', strtotime("now -10 minutes"))
                // Siteminder empty response bypass temporary fix
                // ->where('bookings.id', '>', '12680')
                ->execute()
                ->current();


        if ($booking_exist['request_type'] == 2 || $booking_exist['request_type'] == 1) {
            $set_limit_retry = 5;
            if ($booking_exist['retries'] <= $set_limit_retry) {
                // continue;
            } elseif ($booking_exist['retries'] > $set_limit_retry) {
                DB::delete('api_reservation_queues')
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();
            }
        }

        if (!empty($booking_exist['room_id'])) {
            $price = Model_Booking::calculate_prices($booking_exist['booking_id']);

            $booking_exist['data'] = unserialize($booking_exist['data']);
            $booking_exist['room_texts'] = unserialize($booking_exist['room_texts']);
            $booking_exist['room'] = unserialize($booking_exist['room']);
            $booking_exist['items'] = $price['rooms'];
            $booking_exist['hotel'] = unserialize($booking_exist['hotel']);

            // Guest Comment
            $booking_exist['note'] = 'Requested';
            $booking_exist['note'] .= $booking_exist['request_note'] ? ', guest comment:' . $booking_exist['request_note'] : NULL;
            $booking_exist['note'] .= $booking_exist['is_early_check_in_request'] ? ', Early Check In' : NULL;
            $booking_exist['note'] .= $booking_exist['is_high_floor_request'] ? ', High flor' : NULL;
            $booking_exist['note'] .= $booking_exist['is_large_bed_request'] ? ', Large Bed' : NULL;
            $booking_exist['note'] .= $booking_exist['is_twin_beds_request'] ? ', Twin Bed' : NULL;
            $booking_exist['note'] .= $booking_exist['is_airport_transfer_request'] ? ', Airport Transfer' : NULL;

            // Guid
            $s_message = strtoupper(md5(uniqid(rand(), true)));
            $booking_exist['guid_message'] = 'urn:uuid' . '-' .
                    substr($s_message, 0, 8) . '-' .
                    substr($s_message, 8, 4) . '-' .
                    substr($s_message, 12, 4) . '-' .
                    substr($s_message, 16, 4) . '-' .
                    substr($s_message, 20, 1);

            $s_token = strtoupper(md5(uniqid(rand(), true)));
            $booking_exist['guid_token'] = 'uuid' . '-' .
                    substr($s_token, 0, 8) . '-' .
                    substr($s_token, 8, 4) . '-' .
                    substr($s_token, 12, 4) . '-' .
                    substr($s_token, 16, 4) . '-' .
                    substr($s_token, 20, 12) . '-' .
                    substr($s_token, 31, 1);

            $campaign = (object) unserialize($booking_exist['campaign']);

            /*
             * Benefit name
             */

            $benefit = '';

            if ($campaign->discount_rate > 0) {
                $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
                    ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                    ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                    ':discount_rate' => $campaign->discount_rate,
                ));
            } elseif ($campaign->discount_amount > 0) {
                $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
                    ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                    ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                    ':currency_code' => 'USD',
                    ':discount_amount' => $campaign->discount_amount,
                ));
            } elseif ($campaign->number_of_free_nights) {
                $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
                    ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                    ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                    ':number_of_free_nights' => $campaign->number_of_free_nights,
                ));
            } elseif (!$campaign->is_default) {
                $benefit = __('Default Promotions');
            }
            /// Additional Benefit promotion name
            else {
                $other_benefits = DB::select(
                                'campaigns_benefits.*', array('benefit_texts.name', 'name')
                        )
                        ->from('campaigns_benefits')
                        ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                        ->where('benefit_texts.language_id', '=', 1)
                        ->where('campaigns_benefits.campaign_id', '=', $campaign->id)
                        ->execute();

                if (count($other_benefits)) {
                    $room_campaigns = count($this->rooms) == count($room_names) ? __('All Rooms') : implode(', ', $room_names);

                    $benefit = $room_campaigns;
                    $benefit .= __(' get ');

                    foreach ($other_benefits as $count => $other_benefit) {
                        $benefit .= $other_benefit['name'];
                        if ($other_benefit['value']) {
                            $benefit .= ' for ' . $other_benefit['value'];
                        }

                        $benefit .= count($other_benefits) == ($count + 1) ? '.' : ', ';
                    }
                }
            }

            if ($campaign->type == 2) {
                $benefit .= ' ' . __('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
            } elseif ($campaign->type == 3) {
                $benefit .= ' ' . __('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
            } elseif ($campaign->type == 4) {
                $benefit .= ' ' . __('Non refundable.');
            } elseif ($campaign->type == 5) {
                $benefit .= ' ' . __('Flash Deal.');
            }

            $booking_exist['benefit'] = $benefit;



            // xml process
            if ($booking_exist['api'] == 1 AND $booking_exist['request_type'] == 1) {
                Controller_api_Rategainreservation::HotelRes_rategain($booking_exist);
                CustomLog::factory()->add(4, 'DEBUG', '1Controller_api_Rategainreservation::HotelRes_rategain($booking_exist);');
            } elseif ($booking_exist['api'] == 1 AND $booking_exist['request_type'] == 2) {
                Controller_api_Rategainreservation::HotelRes_rategain_cancel($booking_exist);
                CustomLog::factory()->add(4, 'DEBUG', '2Controller_api_Rategainreservation::HotelRes_rategain_cancel($booking_exist);');
            }

            if ($booking_exist['api'] == 2 AND $booking_exist['request_type'] == 1) {
//                $xml = Controller_Siteminder_Reservation::HotelRes_siteminder($booking_exist);
                $xml = $this->HotelRes_siteminder($booking_exist);

                CustomLog::factory()->add(7, 'DEBUG', '3Controller_Siteminder_Reservation::HotelRes_siteminder($booking_exist);');
            } elseif ($booking_exist['api'] == 2 AND $booking_exist['request_type'] == 2) {
//                Controller_Siteminder_Reservation::HotelRes_siteminder_cancel($booking_exist);
                $this->HotelRes_siteminder_cancel($booking_exist);
                CustomLog::factory()->add(7, 'DEBUG', '4Controller_Siteminder_Reservation::HotelRes_siteminder_cancel($booking_exist);');
            }
        }
    }

    private function HotelRes_siteminder_cancel($booking_exist) {

        $success = TRUE;

        /// Hoterip campaign
        $exchange = Model::factory('exchange');
        // service_tax_rate
        $service_tax_rate = (($booking_exist['hotel']->service_charge_rate + $booking_exist['hotel']->tax_rate) + 100) / 100;

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
            $envelope->setAttribute('xmlns:soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');

            $head = $envelope->appendChild($doc->createElement('soap-env:Header'));
            $Security = $head->appendChild($doc->createElement('wsse:Security'));
            $Security->setAttribute('soap:mustUnderstand', 1);
            $Security->setAttribute('xmlns:wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');
            $Security->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $UsernameToken = $Security->appendChild($doc->createElement('wsse:UsernameToken'));
            $Username = $UsernameToken->appendChild($doc->createElement('wsse:Username', Kohana::config('api.siteminder.User')));
            $Password = $UsernameToken->appendChild($doc->createElement('wsse:Password', Kohana::config('api.siteminder.Pwd')));
            $Password->setAttribute('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');

            $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
            $body->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');

            $response_node = $body->appendChild($doc->createElement('OTA_HotelResNotifRQ'));
            $response_node->setAttribute('ResStatus', 'Cancel');
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('Version', '1.0');
            $response_node->setAttribute('EchoToken', $booking_exist['guid_token']);
            $response_node->setAttribute('ResStatus', 'Cancel');
            $response_node->setAttribute('TimeStamp', date('c'));

            $pos = $response_node->appendChild($doc->createElement('POS'));
            $source = $pos->appendChild($doc->createElement('Source'));
            $RequestorID = $source->appendChild($doc->createElement('RequestorID'));
            $RequestorID->setAttribute('Type', 14);
            /* change */
            $RequestorID->setAttribute('ID', Kohana::config('api.siteminder.id'));
            $BookingChannel = $source->appendChild($doc->createElement('BookingChannel'));
            $BookingChannel->setAttribute('Primary', 'true');
            $BookingChannel->setAttribute('Type', 7);
            $CompanyName = $BookingChannel->appendChild($doc->createElement('CompanyName', 'Hoterip'));
            $CompanyName->setAttribute('Code', Kohana::config('api.siteminder.code'));
            /* change */

            $HotelReservations = $response_node->appendChild($doc->createElement('HotelReservations'));
            $HotelReservation = $HotelReservations->appendChild($doc->createElement('HotelReservation'));
            $HotelReservation->setAttribute('LastModifyDateTime', date('c'));

            $UniqueID = $HotelReservation->appendChild($doc->createElement('UniqueID'));
            $UniqueID->setAttribute('Type', $booking_exist['room_id']);
            $UniqueID->setAttribute('ID', $booking_exist['booking_id']);

            $RoomStays = $HotelReservation->appendChild($doc->createElement('RoomStays'));

            foreach ($booking_exist['items'] as $num => $items) {

                // echo "<pre>";
                // print_r($booking_exist);
                // die();
                // $price_before_tax =  ($item['price'] / $service_tax_rate)  * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // $price_after_tax =  $item['price']  * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // // Extrabed Price
                // $extrabed_price = $item['extrabed_item_price'] != 0.1 ? $item['extrabed_item_price'] : $booking_exist['data']['extrabed_price'];
                // $extrabed_price = ($extrabed_price * $items['number_of_extrabeds']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // // Surcharge Price
                // $surcharge_price = ($booking_exist['data']['total_surcharge']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // // Total Price Final Calculations
                // $price_before_tax = $price_before_tax + $extrabed_price + $surcharge_price;
                // $price_after_tax = $price_after_tax + $extrabed_price + $surcharge_price;

                $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));
                $RoomStay->setAttribute('PromotionCode', $booking_exist['campaign_id']);

                $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                $RoomType->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomType->setAttribute('NumberOfUnits', 1);
                $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
                $RoomDescription->setAttribute('Name', $booking_exist['room_texts'][1]->name);

                // Rateplan descriptions
                $campaign = (array) unserialize($booking_exist['campaign']);
                $campaign['campaign_default'] = $campaign['is_default'];
                $campaign['rate'] = $campaign['discount_rate'];
                $campaign['amount'] = $campaign['discount_amount'];
                $campaign['currency_code'] = $booking_exist['data']['hotel_currency']->code;
                $campaign['free_night'] = $campaign['minimum_number_of_nights'];
                $campaign['last_days'] = $campaign['within_days_of_arrival'];
                $campaign['hotel_timezone'] = $booking_exist['hotel']->timezone;
                $campaign['check_in'] = !empty($campaign['check_in_date']) ? $campaign['check_in_date'] : '0000-00-00';
                $campaign['check_out'] = !empty($campaign['check_out_date']) ? $campaign['check_out_date'] : '0000-00-00';
                $campaign['early_days'] = $campaign['days_in_advance'];
                $campaign['min_nights'] = $campaign['minimum_number_of_nights'];
                $campaign['min_rooms'] = $campaign['minimum_number_of_rooms'];

                $benefit = Controller_Siteminder_Static::promotion_descriptions($campaign);

                $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription', $benefit));
                $RatePlan->setAttribute('RatePlanCode', $booking_exist['campaign_id']);

                // Supposed to be commisions

                $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));
                $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                $RoomRate->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $RoomRate->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomRate->setAttribute('NumberOfUnits', 1);

                // Items
                $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                $AmountAfterTax_total = 0;
                $AmountBeforeTax_total = 0;

                foreach ($items['items'] as $ord => $item) {
                    // if($item['stock'] > 0 && !$item['is_blackout'] && !$item['is_campaign_blackout'])
                    // {
                    // Add one day
                    $date = $item['date'];
                    $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');

                    $price_before_tax = ($item['price'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    $price_after_tax = $item['price'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Extrabed Price
                    $extrabed_price = $item['extrabed_item_price'] != 0.1 ? $item['extrabed_item_price'] : $booking_exist['data']['extrabed_price'];
                    $extrabed_price = ($extrabed_price * $items['number_of_extrabeds']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Surcharge Price
                    $surcharge_price = ($booking_exist['data']['total_surcharge']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    // Total Price Final Calculations
                    $price_before_tax = $price_before_tax + $extrabed_price + $surcharge_price;
                    $price_after_tax = $price_after_tax + $extrabed_price + $surcharge_price;

                    $Rate = $Rates->appendChild($doc->createElement('Rate'));
                    $Rate->setAttribute('EffectiveDate', $date);
                    $Rate->setAttribute('ExpireDate', $add_one_day);
                    $Rate->setAttribute('UnitMultiplier', 1);
                    $Rate->setAttribute('RateTimeUnit', 'Day');

                    $Base = $Rate->appendChild($doc->createElement('Base'));
                    $Base->setAttribute('AmountAfterTax', round($price_after_tax, 2));
                    $Base->setAttribute('AmountBeforeTax', round($price_before_tax, 2));
                    $Base->setAttribute('CurrencyCode', 'USD');
                    // }
                    // Count for total prices
                    $AmountAfterTax_total += $price_after_tax;
                    $AmountBeforeTax_total += $price_before_tax;
                }

                $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                $GuestCount->setAttribute('AgeQualifyingCode', 10);
                $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['adult']);

                if ($booking_exist['data']['capacities'][$num]['child']) {
                    $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                    $GuestCount->setAttribute('AgeQualifyingCode', 8);
                    $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['child']);
                }

                // Night span
                $night_span = (strtotime($booking_exist['data']['check_in']) - strtotime($booking_exist['data']['check_out'])) / (60 * 60 * 24);

                $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
                $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);

                // $AmountAfterTax = $booking_exist['data']['invoice_amount'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // $AmountBeforeTax = ($booking_exist['data']['invoice_amount'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // //$tax = $AmountAfterTax - $AmountBeforeTax;
                // Total Amount all
                $AmountAfterTax_total_all = 0;
                $AmountBeforeTax_total_all = 0;
                $AmountAfterTax_total_all += round($AmountAfterTax_total);
                $AmountBeforeTax_total_all += round($AmountBeforeTax_total);

                $Total = $RoomStay->appendChild($doc->createElement('Total'));
                $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total, 2));
                $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total, 2));
                $Total->setAttribute('CurrencyCode', 'USD');
                // $Taxes = $Rate->appendChild($doc->createElement('Taxes'));
                // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
                // $Tax->setAttribute('Amount', round($tax));
                // $Tax->setAttribute('CurrencyCode', 'USD');

                $BasicPropertyInfo = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                $BasicPropertyInfo->setAttribute('HotelCode', $booking_exist['data']['hotel_id']);
                $BasicPropertyInfo->setAttribute('HotelName', $booking_exist['data']['hotel_name']);

                // $ResGuestRPHs = $RoomStay->appendChild($doc->createElement('ResGuestRPHs'));
                // $ResGuestRPH = $ResGuestRPHs->appendChild($doc->createElement('ResGuestRPH'));
                // $ResGuestRPH->setAttribute('RPH', 1);

                $Comments = $RoomStay->appendChild($doc->createElement('Comments'));
                $Comment = $Comments->appendChild($doc->createElement('Comment'));
                $Comment->setAttribute('GuestViewable', 'true');
                $Text = $Comment->appendChild($doc->createElement('Text', $booking_exist['request_note']));

                $booking_exist['note'] .= $booking_exist['request_note'] ? ', guest comment:' . $booking_exist['request_note'] : NULL;
                $booking_exist['note'] .= $booking_exist['is_early_check_in_request'] ? ', Early Check In' : NULL;
                $booking_exist['note'] .= $booking_exist['is_high_floor_request'] ? ', High flor' : NULL;
                $booking_exist['note'] .= $booking_exist['is_large_bed_request'] ? ', Large Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_twin_beds_request'] ? ', Twin Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_airport_transfer_request'] ? ', Airport Transfer' : NULL;

                $SpecialRequests = $RoomStay->appendChild($doc->createElement('SpecialRequests'));

                if ($booking_exist['is_early_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'early check in request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Early check in request'));
                }
                if ($booking_exist['is_high_floor_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'high floor');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'high floor request'));
                }
                if ($booking_exist['is_large_bed_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'large bed request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'large bed request'));
                }
                if ($booking_exist['is_twin_beds_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'twin beds');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'twin beds request'));
                }
                if ($booking_exist['is_airport_transfer_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'airport transfer');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'airport transfer request'));
                }
                if ($booking_exist['is_non_smoking_room_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'non smoking room request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'non smoking room request'));
                }
                if ($booking_exist['is_late_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'late check in');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'late check in request'));
                }
            }

            $ResGuests = $HotelReservation->appendChild($doc->createElement('ResGuests'));
            $ResGuest = $ResGuests->appendChild($doc->createElement('ResGuest'));
            $ResGuest->setAttribute('ResGuestRPH', 1);
            $ResGuest->setAttribute('PrimaryIndicator', 1);
            //$ResGuest->setAttribute('ArrivalTime', 'true');
            $Profiles = $ResGuest->appendChild($doc->createElement('Profiles'));
            $ProfileInfo = $Profiles->appendChild($doc->createElement('ProfileInfo'));
            $Profile = $ProfileInfo->appendChild($doc->createElement('Profile'));
            $Profile->setAttribute('ProfileType', 1);
            $Customer = $Profile->appendChild($doc->createElement('Customer'));
            $PersonName = $Customer->appendChild($doc->createElement('PersonName'));
            $GivenName = $PersonName->appendChild($doc->createElement('GivenName', $booking_exist['first_name']));
            $Surname = $PersonName->appendChild($doc->createElement('Surname', $booking_exist['last_name']));
            //$Telephone = $Customer->appendChild($doc->createElement('Telephone'));
            $Email = $Customer->appendChild($doc->createElement('Email', $booking_exist['email']));
            //$Address = $Customer->appendChild($doc->createElement('Address'));

            $ResGlobalInfo = $HotelReservation->appendChild($doc->createElement('ResGlobalInfo'));


            // $TimeSpan = $ResGlobalInfo->appendChild($doc->createElement('TimeSpan'));
            // $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
            // $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
            // $TimeSpan->setAttribute('Duration', 'P1D');

            $Total = $ResGlobalInfo->appendChild($doc->createElement('Total'));
            $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total_all, 2));
            $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total_all, 2));
            $Total->setAttribute('CurrencyCode', 'USD');
            $Taxes = $Total->appendChild($doc->createElement('Taxes'));
            // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
            // $Tax->setAttribute('Amount', round($tax));
            // $Tax->setAttribute('CurrencyCode', 'USD');

            $HotelReservationIDs = $ResGlobalInfo->appendChild($doc->createElement('HotelReservationIDs'));
            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $booking_exist['booking_id']);

            $doc->appendChild($envelope);

            $request = $doc->saveXML();

            Log::instance()->add(Log::INFO, $request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, Kohana::config('api.siteminder.url_connect'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Content-length: " . strlen($request)));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $response = curl_exec($ch);
            // Close the handle
            curl_close($ch);
            echo "<pre>";
            print_r($response);
            echo "</pre>";
            die('cancel');
            /*
             * XML process
             */
            try {
                $xml = new SimpleXMLElement($response);
                $body = $xml->children('SOAP-ENV', true)->Body->children();

                $OTA = $body->OTA_HotelResNotifRS;



                // Check response Success/ Errors
                if (isset($OTA->Success)) {
                    DB::delete('api_reservation_queues')
                            ->where('api', '=', 2)
                            ->where('request_type', '=', 2)
                            ->where('booking_id', '=', $booking_exist['booking_id'])
                            ->execute();

                    Log::instance()->add(Log::INFO, 'Siteminder cancel reservation notification Success');
                    Log::instance()->add(Log::INFO, $response);
                } else {
                    $success = FALSE;
                }
            } catch (Exception $e) {
                Database::instance()->rollback();

                $booking_exist['retries'] = $booking_exist['retries'] + 1;

                DB::update('api_reservation_queues')
                        ->set(
                                array('retries' => $booking_exist['retries'])
                        )
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();

                $success = FALSE;

                Log::instance()->add(Log::INFO, 'Siteminder cancel reservation notification Failed');
                Log::instance()->add(Log::INFO, $response);
            }
        } catch (Kohana_Exception $e) {
            $booking_exist['retries'] = $booking_exist['retries'] + 1;

            DB::update('api_reservation_queues')
                    ->set(
                            array('retries' => $booking_exist['retries'])
                    )
                    ->where('booking_id', '=', $booking_exist['booking_id'])
                    ->where('request_type', '=', $booking_exist['request_type'])
                    ->where('api', '=', $booking_exist['api'])
                    ->execute();

            Log::instance()->add(Log::INFO, 'Siteminder cancel reservation notification failed  Build response');

            $success = FALSE;
        }
        return $success;
    }

    private function HotelRes_siteminder($booking_exist) {
        $result = FALSE;

        /// Hoterip campaign
        $exchange = Model::factory('exchange');
        // service_tax_rate
        $service_tax_rate = (($booking_exist['hotel']->service_charge_rate + $booking_exist['hotel']->tax_rate) + 100) / 100;
        // Total Amount
        $AmountAfterTax_total_all = 0;
        $AmountBeforeTax_total_all = 0;

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('SOAP-ENV:Envelope'));
            $envelope->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

            $head = $envelope->appendChild($doc->createElement('SOAP-ENV:Header'));
            $Security = $head->appendChild($doc->createElement('wsse:Security'));
            $Security->setAttribute('soap:mustUnderstand', 1);
            $Security->setAttribute('xmlns:wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');
            $Security->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $UsernameToken = $Security->appendChild($doc->createElement('wsse:UsernameToken'));
            $Username = $UsernameToken->appendChild($doc->createElement('wsse:Username', Kohana::config('api.siteminder.User')));
            $Password = $UsernameToken->appendChild($doc->createElement('wsse:Password', Kohana::config('api.siteminder.Pwd')));
            $Password->setAttribute('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');

            $body = $envelope->appendChild($doc->createElement('SOAP-ENV:Body'));
            $body->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');

            $response_node = $body->appendChild($doc->createElement('OTA_HotelResNotifRQ'));
            $response_node->setAttribute('ResStatus', 'Commit');
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('Version', '1.0');
            $response_node->setAttribute('EchoToken', $booking_exist['guid_token']);
            $response_node->setAttribute('ResStatus', 'Commit');
            $response_node->setAttribute('TimeStamp', date('c'));

            $pos = $response_node->appendChild($doc->createElement('POS'));
            $source = $pos->appendChild($doc->createElement('Source'));
            $RequestorID = $source->appendChild($doc->createElement('RequestorID'));
            $RequestorID->setAttribute('Type', 14);
            /* change */
            $RequestorID->setAttribute('ID', 'HTP'); //Kohana::config('api.siteminder.siteminder_code'));
            $BookingChannel = $source->appendChild($doc->createElement('BookingChannel'));
            $BookingChannel->setAttribute('Primary', 'true');
            $BookingChannel->setAttribute('Type', 7);
            $CompanyName = $BookingChannel->appendChild($doc->createElement('CompanyName', 'Hoterip'));
            $CompanyName->setAttribute('Code', Kohana::config('api.siteminder.Code'));
            /* change */

            $HotelReservations = $response_node->appendChild($doc->createElement('HotelReservations'));
            $HotelReservation = $HotelReservations->appendChild($doc->createElement('HotelReservation'));
            $HotelReservation->setAttribute('CreateDateTime', date('c'));

            $UniqueID = $HotelReservation->appendChild($doc->createElement('UniqueID'));
            $UniqueID->setAttribute('Type', $booking_exist['room_id']);
            $UniqueID->setAttribute('ID', $booking_exist['booking_id']);

            $RoomStays = $HotelReservation->appendChild($doc->createElement('RoomStays'));

            foreach ($booking_exist['items'] as $num => $items) {
                $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));
                $RoomStay->setAttribute('PromotionCode', $booking_exist['campaign_id']);

                $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                $RoomType->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomType->setAttribute('NumberOfUnits', 1);
                $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
                $RoomDescription->setAttribute('Name', $booking_exist['room_texts'][1]->name);

                // Rateplan descriptions
                $campaign = (array) unserialize($booking_exist['campaign']);
                $campaign['campaign_default'] = $campaign['is_default'];
                $campaign['rate'] = $campaign['discount_rate'];
                $campaign['amount'] = $campaign['discount_amount'];
                $campaign['currency_code'] = $booking_exist['data']['hotel_currency']->code;
                $campaign['free_night'] = $campaign['minimum_number_of_nights'];
                $campaign['last_days'] = $campaign['within_days_of_arrival'];
                $campaign['hotel_timezone'] = $booking_exist['hotel']->timezone;
                $campaign['check_in'] = $booking_exist['check_in_date'];
                $campaign['check_out'] = $booking_exist['check_out_date'];
                $campaign['early_days'] = $campaign['days_in_advance'];
                $campaign['min_nights'] = $campaign['minimum_number_of_nights'];
                $campaign['min_rooms'] = $campaign['minimum_number_of_rooms'];

                $benefit = Controller_Siteminder_Static::promotion_descriptions($campaign);

                $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription', $benefit));
                $RatePlan->setAttribute('RatePlanCode', $booking_exist['campaign_id']);

                // Supposed to be commisions

                $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));
                $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                $RoomRate->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $RoomRate->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomRate->setAttribute('NumberOfUnits', 1);

                // Items
                $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                $AmountAfterTax_total = 0;
                $AmountBeforeTax_total = 0;
                $price_after_tax = 0;
                $price_before_tax = 0;

                foreach ($items['items'] as $ord => $item) {
                    if ($item['stock'] > 0 && !$item['is_blackout'] && !$item['is_campaign_blackout']) {
                        // Add one day
                        $date = $item['date'];
                        $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');

                        $price_before_tax = round(($item['price'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1));
                        $price_after_tax = round($item['price'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1));

                        $Rate = $Rates->appendChild($doc->createElement('Rate'));
                        $Rate->setAttribute('EffectiveDate', $date);
                        $Rate->setAttribute('ExpireDate', $add_one_day);
                        $Rate->setAttribute('UnitMultiplier', 1);
                        $Rate->setAttribute('RateTimeUnit', 'Day');

                        $Base = $Rate->appendChild($doc->createElement('Base'));
                        $Base->setAttribute('AmountAfterTax', $price_after_tax);
                        $Base->setAttribute('AmountBeforeTax', $price_before_tax);
                        $Base->setAttribute('CurrencyCode', 'USD');
                    }

                    // Count for total prices
                    $AmountAfterTax_total += $price_after_tax;
                    $AmountBeforeTax_total += $price_before_tax;
                }

                $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                $GuestCount->setAttribute('AgeQualifyingCode', 10);
                $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['adult']);

                if ($booking_exist['data']['capacities'][$num]['child']) {
                    $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                    $GuestCount->setAttribute('AgeQualifyingCode', 8);
                    $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['child']);
                }

                // Night span
                $night_span = (strtotime($booking_exist['data']['check_in']) - strtotime($booking_exist['data']['check_out'])) / (60 * 60 * 24);

                $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
                $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);

                // $AmountAfterTax = $booking_exist['data']['invoice_amount'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // $AmountBeforeTax = ($booking_exist['data']['invoice_amount'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // //$tax = $AmountAfterTax - $AmountBeforeTax;
                // Total Amount all
                $AmountAfterTax_total_all += round($AmountAfterTax_total);
                $AmountBeforeTax_total_all += round($AmountBeforeTax_total);

                $Total = $RoomStay->appendChild($doc->createElement('Total'));
                $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total));
                $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total));
                $Total->setAttribute('CurrencyCode', 'USD');
                // $Taxes = $Rate->appendChild($doc->createElement('Taxes'));
                // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
                // $Tax->setAttribute('Amount', round($tax));
                // $Tax->setAttribute('CurrencyCode', 'USD');

                $BasicPropertyInfo = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                $BasicPropertyInfo->setAttribute('HotelCode', $booking_exist['data']['hotel_id']);
                $BasicPropertyInfo->setAttribute('HotelName', $booking_exist['data']['hotel_name']);

                // $ResGuestRPHs = $RoomStay->appendChild($doc->createElement('ResGuestRPHs'));
                // $ResGuestRPH = $ResGuestRPHs->appendChild($doc->createElement('ResGuestRPH'));
                // $ResGuestRPH->setAttribute('RPH', 1);

                $Comments = $RoomStay->appendChild($doc->createElement('Comments'));
                $Comment = $Comments->appendChild($doc->createElement('Comment'));
                $Comment->setAttribute('GuestViewable', 'true');
                $Text = $Comment->appendChild($doc->createElement('Text', htmlspecialchars(utf8_encode($booking_exist['request_note']))));

                $booking_exist['note'] .= $booking_exist['request_note'] ? ', guest comment:' . $booking_exist['request_note'] : NULL;
                $booking_exist['note'] .= $booking_exist['is_early_check_in_request'] ? ', Early Check In' : NULL;
                $booking_exist['note'] .= $booking_exist['is_high_floor_request'] ? ', High flor' : NULL;
                $booking_exist['note'] .= $booking_exist['is_large_bed_request'] ? ', Large Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_twin_beds_request'] ? ', Twin Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_airport_transfer_request'] ? ', Airport Transfer' : NULL;

                $SpecialRequests = $RoomStay->appendChild($doc->createElement('SpecialRequests'));

                if ($booking_exist['is_early_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'early check in request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Early check in request'));
                }
                if ($booking_exist['is_high_floor_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'high floor');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'high floor request'));
                }
                if ($booking_exist['is_large_bed_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'large bed request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'large bed request'));
                }
                if ($booking_exist['is_twin_beds_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'twin beds');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'twin beds request'));
                }
                if ($booking_exist['is_airport_transfer_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'airport transfer');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'airport transfer request'));
                }
                if ($booking_exist['is_non_smoking_room_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'non smoking room request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'non smoking room request'));
                }
                if ($booking_exist['is_late_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'late check in');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'late check in request'));
                }
            }

            $ResGuests = $HotelReservation->appendChild($doc->createElement('ResGuests'));
            $ResGuest = $ResGuests->appendChild($doc->createElement('ResGuest'));
            $ResGuest->setAttribute('ResGuestRPH', 1);
            $ResGuest->setAttribute('PrimaryIndicator', 1);
            //$ResGuest->setAttribute('ArrivalTime', 'true');
            $Profiles = $ResGuest->appendChild($doc->createElement('Profiles'));
            $ProfileInfo = $Profiles->appendChild($doc->createElement('ProfileInfo'));
            $Profile = $ProfileInfo->appendChild($doc->createElement('Profile'));
            $Profile->setAttribute('ProfileType', 1);
            $Customer = $Profile->appendChild($doc->createElement('Customer'));
            $PersonName = $Customer->appendChild($doc->createElement('PersonName'));
            $GivenName = $PersonName->appendChild($doc->createElement('GivenName', $booking_exist['first_name']));
            $Surname = $PersonName->appendChild($doc->createElement('Surname', $booking_exist['last_name']));
            //$Telephone = $Customer->appendChild($doc->createElement('Telephone'));
            $Email = $Customer->appendChild($doc->createElement('Email', $booking_exist['email']));
            //$Address = $Customer->appendChild($doc->createElement('Address'));

            $ResGlobalInfo = $HotelReservation->appendChild($doc->createElement('ResGlobalInfo'));

            // Night span
            //$night_span = (strtotime($booking_exist['data']['check_in']) - strtotime($booking_exist['data']['check_out'])) / (60 * 60 * 24);
            // $TimeSpan = $ResGlobalInfo->appendChild($doc->createElement('TimeSpan'));
            // $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
            // $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
            // $TimeSpan->setAttribute('Duration', 'P1D');

            $Total = $ResGlobalInfo->appendChild($doc->createElement('Total'));
            $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total_all));
            $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total_all));
            $Total->setAttribute('CurrencyCode', 'USD');
            $Taxes = $Total->appendChild($doc->createElement('Taxes'));
            // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
            // $Tax->setAttribute('Amount', round($tax));
            // $Tax->setAttribute('CurrencyCode', 'USD');

            $HotelReservationIDs = $ResGlobalInfo->appendChild($doc->createElement('HotelReservationIDs'));
            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $booking_exist['booking_id']);

            $doc->appendChild($envelope);

            $request = $doc->saveXML();


            Log::instance()->add(Log::INFO, 'Siteminder reservation notification request');
            Log::instance()->add(Log::INFO, $request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, Kohana::config('api.siteminder.url_connect'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Content-length: " . strlen($request)));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $response = curl_exec($ch);

            // Close the handle
            curl_close($ch);
            echo "<pre>";
            print_r($response);
            echo "</pre>";
            die('booking');
            /*
             * XML process
             */
            try {
                $xml = new SimpleXMLElement($response);
                $body = $xml->children('SOAP-ENV', true)->Body->children();
                $OTA = $body->OTA_HotelResNotifRS;

                // Check response Success/ Errors
                if ($OTA->Success != null) {
                    DB::delete('api_reservation_queues')
                            ->where('api', '=', 2)
                            ->where('request_type', '=', 1)
                            ->where('booking_id', '=', $booking_exist['booking_id'])
                            ->execute();

                    Log::instance()->add(Log::INFO, 'Siteminder reservation notification Success');
                    Log::instance()->add(Log::INFO, $response);
                }
            } catch (Exception $e) {
                Database::instance()->rollback();

                $booking_exist['retries'] = $booking_exist['retries'] + 1;

                DB::update('api_reservation_queues')
                        ->set(
                                array('retries' => $booking_exist['retries'])
                        )
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();
                Log::instance()->add(Log::INFO, 'Siteminder reservation notification Failed');
                Log::instance()->add(Log::INFO, $response);
            }
        } catch (Kohana_Exception $e) {
            $booking_exist['retries'] = $booking_exist['retries'] + 1;

            DB::update('api_reservation_queues')
                    ->set(
                            array('retries' => $booking_exist['retries'])
                    )
                    ->where('booking_id', '=', $booking_exist['booking_id'])
                    ->where('request_type', '=', $booking_exist['request_type'])
                    ->where('api', '=', $booking_exist['api'])
                    ->execute();

            Log::instance()->add(Log::INFO, 'Siteminder reservation notification failed');

            $values = $this->request->post();
            foreach ($values as $key => $value) {
                Log::instance()->add(Log::INFO, "$key: $value");
            }
        }
    }

    private function xml_reserve() {
        $xml = '<!--?xml version="1.0" encoding="UTF-8"?-->
<soap-env:envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
  <soap-env:header>
    <wsse:security soap:mustunderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <wsse:usernametoken>
        <wsse:username>Hoterip</wsse:username>
        <wsse:password type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">qw7Y4S98fq2Z4X5</wsse:password>
      </wsse:usernametoken>
    </wsse:security>
  </soap-env:header>
  <soap-env:body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <ota_hotelresnotifrq xmlns="http://www.opentravel.org/OTA/2003/05" resstatus="Cancel" version="1.0" echotoken="uuid-F5C9F35A-A761-038C-311D-F36157A29260-0" timestamp="2015-10-20T09:56:54+08:00">
      <pos>
        <source>
          <requestorid type="14" id="HTP">
          <bookingchannel primary="true" type="7">
            <companyname code="HOT">Hoterip</companyname>
          </bookingchannel>
        
      </requestorid></pos>
      <hotelreservations>
        <hotelreservation lastmodifydatetime="2015-10-20T09:56:54+08:00">
          <uniqueid type="3001" id="14591">
          <roomstays>
            <roomstay promotioncode="3886">
              <roomtypes>
                <roomtype roomtypecode="3001" numberofunits="1">
                  <roomdescription name="Deluxe Room">
                </roomdescription></roomtype>
              </roomtypes>
              <rateplans>
                <rateplan rateplancode="3886">
                  <rateplandescription>Default Promotions.</rateplandescription>
                </rateplan>
              </rateplans>
              <roomrates>
                <roomrate rateplancode="3886" roomtypecode="3001" numberofunits="1">
                  <rates>
                    <rate effectivedate="2015-06-20" expiredate="2015-06-21" unitmultiplier="1" ratetimeunit="Day">
                      <base amountaftertax="20.35" amountbeforetax="16.82" currencycode="USD">
                    </rate>
                  </rates>
                </roomrate>
              </roomrates>
              <guestcounts>
                <guestcount agequalifyingcode="10" count="1">
              </guestcount></guestcounts>
              <timespan start="2015-06-20" end="2015-06-21">
              <total amountaftertax="20.35" amountbeforetax="16.82" currencycode="USD">
              <basicpropertyinfo hotelcode="1117" hotelname="Cempaka Hotel Lovina">
              <comments>
                <comment guestviewable="true">
                  <text></text>
                </comment>
              </comments>
              <specialrequests>
            </specialrequests></basicpropertyinfo></total></timespan></roomstay>
          </roomstays>
          <resguests>
            <resguest resguestrph="1" primaryindicator="1">
              <profiles>
                <profileinfo>
                  <profile profiletype="1">
                    <customer>
                      <personname>
                        <givenname>PUTU</givenname>
                        <surname>AFRIONI</surname>
                      </personname>
                      <email>sukadana@his-world.com</email>
                    </customer>
                  </profile>
                </profileinfo>
              </profiles>
            </resguest>
          </resguests>
          <resglobalinfo>
            <total amountaftertax="20" amountbeforetax="17" currencycode="USD">
              <taxes>
            </taxes></total>
            <hotelreservationids>
              <hotelreservationid resid_type="14" resid_value="14591">
            </hotelreservationid></hotelreservationids>
          </resglobalinfo>
        </uniqueid></hotelreservation>
      </hotelreservations>
    </ota_hotelresnotifrq>
  </soap-env:body>
</soap-env:envelope>';
        return $xml;
    }

}
