<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Booking extends Controller_Layout_Admin {

    public function action_see() {
        $booking_id = $this->request->param('id');

        $booking = ORM::factory('booking')
                ->select(array('hotels.id', 'hotel_id'))
                ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
                ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                ->where('bookings.id', '=', $booking_id)
                ->find();

        // Add data from deleted room
        if (empty($booking->id)) {
            $booking = ORM::factory('booking')
                    ->select(array('room_deleted.hotel_id', 'hotel_id'))
                    ->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
                    ->where('room_deleted.language_id', '=', $this->selected_language->id)
                    ->where('bookings.id', '=', $booking_id)
                    ->find();
        }

        if (!A2::instance()->allowed($booking, 'read')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        if ($booking->hotel_id != $this->selected_hotel->id) {
            // Set selected hotel id in cookie
            Cookie::set('selected_hotel_id', $booking->hotel_id);
            // Add notice
            Notice::add(Notice::INFO, 'Your selected hotel has been changed in top right corner.');
        }

        $this->request->redirect(Route::get('default')->uri(array('controller' => 'booking', 'action' => 'manage')) . URL::query(array('id' => $booking->id)));
    }

    public function action_toggle() {
        // Get parameters 
        $booking_id = $this->request->param('id');
        $field = $this->request->query('field');
        $value = $this->request->query('value');

        // Get logged in admin
        $logged_in_admin = A2::instance()->get_user();

        // Get booking
        $booking = ORM::factory('booking')
                ->select(
                        array('lockers.username', 'locker_username'), array('lockers.name', 'locker_name')
                )
                ->join(array('admins', 'lockers'), 'left')->on('lockers.id', '=', 'bookings.locker_id')
                ->where('bookings.id', '=', $booking_id)
                ->find();

        if ($field == 'lock') {
            // If logged in user want to lock booking, but booking has already been locked by another user
            if ($value > 0 AND $booking->locker_id > 0) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('booking', 'locked_by'), array(':username' => $booking->locker_username, ':name' => $booking->locker_name));
            } else {
                // Start transaction
                Database::instance()->begin();

                try {
                    // Update booking 
                    DB::update('bookings')
                            ->set(array(
                                'locker_id' => $value ? $logged_in_admin->id : $value,
                            ))
                            ->where('id', '=', $booking_id)
                            ->execute();

                    // Add to booking comment
                    ORM::factory('booking_comment')
                            ->values(array(
                                'booking_id' => $booking_id,
                                'admin_id' => $logged_in_admin->id,
                                'content' => $value ? Kohana::message('booking', 'lock') : Kohana::message('booking', 'unlock'),
                                'created' => time(),
                            ))
                            ->create();

                    // Get hotel admins except me
                    $admins = DB::select('admins.*')
                            ->from('admins')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->join('rooms')->on('rooms.hotel_id', '=', 'admins_hotels.hotel_id')
                            ->where('rooms.id', '=', $booking->room_id)
                            ->where('admins_hotels.admin_id', '<>', $logged_in_admin->id)
                            ->as_object()
                            ->execute();

                    // Notify hotel admins
                    foreach ($admins as $admin) {
                        $booking_comment_notification = DB::select()
                                ->from('booking_comment_notifications')
                                ->where('admin_id', '=', $admin->id)
                                ->where('booking_id', '=', $booking_id)
                                ->limit(1)
                                ->as_object()
                                ->execute()
                                ->current();

                        // If hotel admin still don't have notification
                        if (!$booking_comment_notification) {
                            ORM::factory('booking_comment_notification')
                                    ->values(array(
                                        'booking_id' => $booking_id,
                                        'admin_id' => $admin->id,
                                    ))
                                    ->create();
                        }
                    }

                    // Commit transaction
                    Database::instance()->commit();
                } catch (Exception $e) {
                    // Rollback transaction
                    Database::instance()->rollback();
                    // Add error notice
                    Notice::add(Notice::ERROR, $e->getMessage());
                }
            }
        } elseif ($field == 'read_comment') {
            // Remove unread coment
            DB::delete('booking_comment_notifications')
                    ->where('admin_id', '=', $logged_in_admin->id)
                    ->where('booking_id', '=', $booking_id)
                    ->execute();
            // No redirect needed
            return;
        } elseif ($field == 'read_conversation') {
            // Remove unread conversation
            DB::delete('booking_conversation_notifications')
                    ->where('admin_id', '=', $logged_in_admin->id)
                    ->where('booking_id', '=', $booking_id)
                    ->execute();
            // No redirect needed
            return;
        } else {
            // If booking is not locked
            if ($booking->locker_id == 0) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('booking', 'lock_needed'));
            }
            // If booking is already locked by another user
            elseif ($booking->locker_id != $logged_in_admin->id) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('booking', 'locked_by'), array(':username' => $booking->locker_username, ':name' => $booking->locker_name));
            } else {
                switch ($field) {
                    case 'done':
                        DB::update('bookings')
                                ->set(array(
                                    'is_done' => (int) !(bool) $booking->is_done,
                                ))
                                ->where('id', '=', $booking_id)
                                ->execute();
                        break;
                }
            }
        }

        $this->request->redirect($this->request->referrer());
    }

    public function action_manage() {
        // Is Authorized ?
        if (!A2::instance()->allowed('booking', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        // If conversation submitted
        if ($this->request->post('submit_conversation')) {
            $conversations = $this->request->post('conversations');

            foreach ($conversations as $booking_id => $conversation) {
                // Get booking ORM
                $booking = ORM::factory('booking')
                        ->where('id', '=', $booking_id)
                        ->find();

                // If allowed
                if (A2::instance()->allowed($booking, 'read')) {
                    // Start transaction
                    Database::instance()->begin();

                    try {
                        // Create conversation
                        ORM::factory('booking_conversation')
                                ->values(array(
                                    'booking_id' => $booking_id,
                                    'admin_id' => A2::instance()->get_user()->id,
                                    'user_id' => NULL,
                                    'content' => $conversation,
                                    'created' => time(),
                                ))
                                ->create();

                        // Get hotel admins except me
                        $admins = DB::select('admins.*')
                                ->from('admins')
                                ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                                ->join('rooms')->on('rooms.hotel_id', '=', 'admins_hotels.hotel_id')
                                ->where('rooms.id', '=', $booking->room_id)
                                ->where('admins_hotels.admin_id', '<>', A2::instance()->get_user()->id)
                                ->as_object()
                                ->execute();

                        // Notify hotel admins
                        foreach ($admins as $admin) {
                            $booking_conversation_notification = DB::select()
                                    ->from('booking_conversation_notifications')
                                    ->where('admin_id', '=', $admin->id)
                                    ->where('booking_id', '=', $booking_id)
                                    ->limit(1)
                                    ->as_object()
                                    ->execute()
                                    ->current();

                            // If hotel admin still don't have notification
                            if (!$booking_conversation_notification) {
                                // Create notificaiton
                                ORM::factory('booking_conversation_notification')
                                        ->values(array(
                                            'booking_id' => $booking_id,
                                            'admin_id' => $admin->id,
                                            'user_id' => NULL,
                                        ))
                                        ->create();
                            }
                        }

                        // If there is booking user
                        if ($booking->user_id) {
                            // Add notification to user
                            ORM::factory('booking_conversation_notification')
                                    ->values(array(
                                        'booking_id' => $booking_id,
                                        'admin_id' => NULL,
                                        'user_id' => $booking->user_id,
                                    ))
                                    ->create();
                        }

                        // Get booking language
                        $booking_language = ORM::factory('language')
                                ->where('id', '=', $booking->language_id)
                                ->find();

                        // Create email subject
                        $subject = strtr(I18n::get('Message From :hotel_name', $booking_language->code), array(':hotel_name' => $this->selected_hotel->name));

                        // Create email message body
                        $message = Kostache::factory('email/conversation')
                                ->set('message', HTML::chars($conversation))
                                ->set('language', $booking_language)
                                ->set('selected_hotel', $this->selected_hotel)
                                ->set('booking_id', $booking->id)
                                ->set('booking', $booking)
                                ->render();

                        // Send email
                        Email::factory($subject, $message, 'text/plain')
                                ->to($booking->email)
                                ->from(Kohana::config('application.conversation_email_address'), Kohana::config('application.name'))
                                ->bcc(Kohana::config('application.mass_email_address'), Kohana::config('application.name'))
                                ->send();

                        // Commit transaction
                        Database::instance()->commit();
                        // Add success notice
                        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
                    } catch (Exception $e) {
                        // Rollback transaction
                        Database::instance()->rollback();
                        // Add error notice
                        Notice::add(Notice::ERROR, $e->getMessage());
                    }
                } else {
                    // Add error notice
                    Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
                }
            }
        }

        // If comment submitted
        if ($this->request->post('submit_comment')) {
            $comments = $this->request->post('comments');

            foreach ($comments as $booking_id => $comment) {
                // Get booking ORM
                $booking = ORM::factory('booking')
                        ->where('id', '=', $booking_id)
                        ->find();

                // If allowed
                if (A2::instance()->allowed($booking, 'read')) {
                    // Start transaction
                    Database::instance()->begin();

                    try {
                        // Create comment
                        ORM::factory('booking_comment')
                                ->values(array(
                                    'booking_id' => $booking_id,
                                    'admin_id' => A2::instance()->get_user()->id,
                                    'content' => $comment,
                                    'created' => time(),
                                ))
                                ->create();

                        // Get hotel admins except me
                        $admins = DB::select('admins.*')
                                ->from('admins')
                                ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                                ->join('rooms')->on('rooms.hotel_id', '=', 'admins_hotels.hotel_id')
                                ->where('rooms.id', '=', $booking->room_id)
                                ->where('admins_hotels.admin_id', '<>', A2::instance()->get_user()->id)
                                ->as_object()
                                ->execute();

                        // Notify hotel admins
                        foreach ($admins as $admin) {
                            $booking_comment_notification = DB::select()
                                    ->from('booking_comment_notifications')
                                    ->where('admin_id', '=', $admin->id)
                                    ->where('booking_id', '=', $booking_id)
                                    ->limit(1)
                                    ->as_object()
                                    ->execute()
                                    ->current();

                            // If hotel admin still don't have notification
                            if (!$booking_comment_notification) {
                                ORM::factory('booking_comment_notification')
                                        ->values(array(
                                            'booking_id' => $booking_id,
                                            'admin_id' => $admin->id,
                                        ))
                                        ->create();
                            }
                        }

                        // Commit transaction
                        Database::instance()->commit();
                        // Add success notice
                        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
                    } catch (Exception $e) {
                        // Rollback transaction
                        Database::instance()->rollback();
                        // Add error notice
                        Notice::add(Notice::ERROR, $e->getMessage());
                    }
                }
            }
        }

        $page = Arr::get($this->request->query(), 'page', 1);
        $items_per_page = 30;
        $offset = ($page - 1) * $items_per_page;

        // Get logged in admin
        $logged_in_admin = A1::instance()->get_user();

        // Get selected hotel currency
        $currency = ORM::factory('currency')
                ->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        /*
         * Get filters
         */
        $booking_id = $this->request->query('id');
        $hotel_id = $this->request->query('hotel_id');
        $guest_name = $this->request->query('guest_name');
        $booking_start_date = $this->request->query('booking_start_date');
        $booking_end_date = $this->request->query('booking_end_date');
        $check_in_start_date = $this->request->query('check_in_start_date');
        $check_in_end_date = $this->request->query('check_in_end_date');
        $is_canceled = $this->request->query('is_canceled');
        $is_done = $this->request->query('is_done');

        // Get bookings
        $bookings = ORM::factory('booking')
                ->select(
                        array('room_texts.name', 'room_name'), array('lockers.username', 'locker_username'), array('lockers.name', 'locker_name'), array('nationalities.name', 'nationality_name')
                )
                ->from('bookings')
                ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
                ->join(array('admins', 'lockers'), 'left')->on('lockers.id', '=', 'bookings.locker_id')
                ->join('nationalities')->on('nationalities.id', '=', 'bookings.nationality_id')
                ->where('hotels.id', '=', $this->selected_hotel->id)
                ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
                ->order_by('bookings.id', 'DESC');

        if ($booking_id) {
            $bookings->where('bookings.id', '=', $booking_id);
        }

        if ($guest_name) {
            $bookings->where(DB::expr('CONCAT(bookings.first_name, bookings.last_name)'), 'LIKE', "%$guest_name%");
        }

        if ($booking_start_date AND $booking_end_date) {
            $date_offset = Date::offset($this->selected_hotel->timezone);

            $bookings->where(DB::expr("bookings.created + $date_offset"), '>=', strtotime($booking_start_date));
            $bookings->where(DB::expr("bookings.created + $date_offset"), '<=', strtotime($booking_end_date));
        }

        if ($check_in_start_date AND $check_in_end_date) {
            $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
            $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

            $bookings->where('bookings.check_in_date', '>=', $check_in_start_date);
            $bookings->where('bookings.check_in_date', '<=', $check_in_end_date);
        }

        $total_bookings = $bookings
                ->reset(FALSE)
                ->count_all();

        $bookings = $bookings
                ->offset($offset)
                ->limit($items_per_page)
                ->find_all();

        /// Manage booking. add data for deleted room
        $deleted_room_search = FALSE;

        $bookings_values = ORM::factory('booking')
                ->select(
                        array('room_deleted.name', 'room_name'), array('lockers.username', 'locker_username'), array('lockers.name', 'locker_name'), array('nationalities.name', 'nationality_name')
                )
                ->from('bookings')
                ->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
                ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'room_deleted.hotel_id')
                ->join(array('admins', 'lockers'), 'left')->on('lockers.id', '=', 'bookings.locker_id')
                ->join('nationalities')->on('nationalities.id', '=', 'bookings.nationality_id')
                ->where('room_deleted.hotel_id', '=', $this->selected_hotel->id)
                ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
                ->where('room_deleted.language_id', '=', $this->selected_language->id)
                ->group_by('bookings.id')
                ->order_by('bookings.id', 'DESC');

        if ($booking_id) {
            $deleted_room_search = TRUE;
            $bookings_values->where('bookings.id', '=', $booking_id);
        }

        if ($guest_name) {
            $deleted_room_search = TRUE;
            $bookings_values->where(DB::expr('CONCAT(bookings.first_name, bookings.last_name)'), 'LIKE', "%$guest_name%");
        }

        if ($booking_start_date AND $booking_end_date) {
            $deleted_room_search = TRUE;
            $date_offset = Date::offset($this->selected_hotel->timezone);

            $bookings_values->where(DB::expr("bookings.created + $date_offset"), '>=', strtotime($booking_start_date));
            $bookings_values->where(DB::expr("bookings.created + $date_offset"), '<=', strtotime($booking_end_date));
        }

        if ($check_in_start_date AND $check_in_end_date) {
            $deleted_room_search = TRUE;
            $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
            $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

            $bookings_values->where('bookings.check_in_date', '>=', $check_in_start_date);
            $bookings_values->where('bookings.check_in_date', '<=', $check_in_end_date);
        }

        $total_bookings_values = $bookings_values
                ->reset(FALSE)
                ->count_all();

        $bookings_values = $bookings_values
                ->offset($offset)
                ->limit($items_per_page)
                ->find_all();

        // Create pagination
        $pagination = Pagination::factory(array(
                    'items_per_page' => $items_per_page,
                    'total_items' => $total_bookings + $total_bookings_values,
        ));

        $this->template->main = Kostache::factory('booking/manage')
                ->set('notice', Notice::render())
                ->set('filters', $this->request->query())
                ->set('logged_in_admin', $logged_in_admin)
                ->set('currency', $currency)
                ->set('bookings', $bookings)
                ->set('bookings_values', $bookings_values)
                ->set('selected_language', $this->selected_language)
                ->set('pagination', $pagination);
    }

    public function action_detail() {
        // Get booking id
        $booking_id = (int) $this->request->param('id');

        // Get booking
        $booking = ORM::factory('booking')
                ->where('id', '=', $booking_id)
                ->find();

        // Authorization
        if (!A2::instance()->allowed($booking, 'read')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'booking', 'action' => 'manage')));
        }

        if ($this->request->post('no_show') OR $this->request->post('cancel')) {
            $booking = ORM::factory('booking')
                    ->select(
                            'booking_datas.*', array('hotels.timezone', 'hotel_timezone')
                    )
                    ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                    ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
                    ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                    ->where('bookings.id', '=', $booking_id)
                    ->find();

            if (empty($booking->id)) {
                $booking = ORM::factory('booking')
                        ->select(
                                'booking_datas.*', array('hotels.timezone', 'hotel_timezone')
                        )
                        ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                        ->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
                        ->join('hotels')->on('hotels.id', '=', 'room_deleted.hotel_id')
                        ->where('bookings.id', '=', $booking_id)
                        ->find();
            }

            // If cancel no show button clicked
            if ($this->request->post('no_show')) {
                // Booking can be canceled if today >= check in date, means the guest didn't show at the hotel
                $can_cancel = (strtotime('now') + Date::offset($booking->hotel_timezone) >= strtotime($booking->check_in_date));
            } else {
                // Booking can be canceled by hoterip staff
                $can_cancel = A2::instance()->allowed('all', 'hoterip');
            }

            if ($can_cancel) {
                $is_sucess = TRUE;
                try {
                    $booking->is_canceled = 1;
                    $booking->canceled_timestamp = time();
                    $booking->update();

                    // Get user language when booking
                    $language = ORM::factory('language')
                            ->where('id', '=', $booking->language_id)
                            ->find();

                    // Get hotel admins
                    $admin_emails = ORM::factory('admin')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->join('hotels')->on('hotels.id', '=', 'admins_hotels.hotel_id')
                            ->join('rooms')->on('rooms.hotel_id', '=', 'hotels.id')
                            ->where('rooms.id', '=', $booking->room_id)
                            ->find_all()
                            ->as_array('email', 'name');

                    // Create message
                    $message = Kostache::factory('email/cancel')
                            ->set('booking', $booking)
                            ->set('language', $language);

                    set_time_limit(300);

                    // Send email
                    Email::factory(I18n::get('Booking Canceled', $language->code), $message)
                            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                            ->to($booking->email)
                            ->cc(Kohana::config('application.email'))
                            ->bcc($admin_emails)
                            ->send();
                } catch (Exception $e) {
                    $is_sucess = FALSE;
                    Notice::add(Notice::ERROR, $e->getMessage());
                }
                if ($is_sucess) {
                    $booking_data = unserialize($booking->data);

                    // Added API Queues Reservations
                    if ($this->authHotelRategain($booking_data['hotel_id'])) {
                        // rategain
                        DB::insert('api_reservation_queues')
                                ->columns(array('booking_id', 'api', 'request_type'))
                                ->values(array($booking_id, 1, 2))
                                ->execute();
                    }

                    // siteminder
                    DB::insert('api_reservation_queues')
                            ->columns(array('booking_id', 'api', 'request_type'))
                            ->values(array($booking_id, 2, 2))
                            ->execute();


                    $vacation_triger = array(
                        'hotel_id' => $booking_data['hotel_id'],
                        'room_id' => $booking_data['room_id'],
                        'campaign_id' => $booking_data['campaign_id'],
                        // 'start_date' =>$booking_data['check_in'], 
                        // 'end_date' =>$booking_data['check_out'],
                        'item_id' => $booking_data['item_ids'],
                        'start_date' => NULL,
                        'end_date' => NULL,
                        'type_trigger' => NULL,
                        'status' => 0
                    );

                    //insert data vacation trigger into database
                    Controller_Vacation_Trigger::insertData($vacation_triger);
                }
            } else {
                Notice::add(Notice::ERROR, Kohana::message('booking', 'no_show_cancel_failed'));
            }
        }

        // Get booking 
        $booking = ORM::factory('booking')
                ->select(
                        'booking_datas.*', array('hotels.timezone', 'hotel_timezone'), array('users.email', 'user_email'), array('users.mobile_phone', 'user_mobile_phone'), array('users.home_phone', 'user_home_phone'), array('nationalities.name', 'nationality_name'), array('languages.name', 'language_name'), array('languages.code', 'language_code')
                )
                ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
                ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                ->join('users', 'left')->on('users.id', '=', 'bookings.user_id')
                ->join('nationalities')->on('nationalities.id', '=', 'bookings.nationality_id')
                ->join('languages')->on('languages.id', '=', 'bookings.language_id')
                ->where('bookings.id', '=', $booking_id)
                ->find();

        /// Manage Booking, Add exception for room deleted
        if (!$booking->id) {
            $booking = ORM::factory('booking')
                    ->select(
                            'booking_datas.*', array('hotels.timezone', 'hotel_timezone'), array('users.email', 'user_email'), array('users.mobile_phone', 'user_mobile_phone'), array('users.home_phone', 'user_home_phone'), array('nationalities.name', 'nationality_name'), array('languages.name', 'language_name'), array('languages.code', 'language_code')
                    )
                    ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                    ->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
                    ->join('hotels')->on('hotels.id', '=', 'room_deleted.hotel_id')
                    ->join('users', 'left')->on('users.id', '=', 'bookings.user_id')
                    ->join('nationalities')->on('nationalities.id', '=', 'bookings.nationality_id')
                    ->join('languages')->on('languages.id', '=', 'bookings.language_id')
                    ->where('bookings.id', '=', $booking_id)
                    ->find();
        }

        /// Manage Booking, set timezone
        $booking_hotel_timezone = unserialize($booking->hotel)->timezone;

        // Create view
        $this->template->main = Kostache::factory('booking/detail')
                ->set('notice', Notice::render())
                ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                ->set('logged_in_admin', A2::instance()->get_user())
                ->set('selected_language', $this->selected_language)
                ->set('booking', $booking)
                ->set('show_cancel_no_show_button', strtotime('now') + Date::offset($booking->hotel_timezone) >= strtotime($booking->check_in_date));
    }

    public function authHotelRategain($hotel_id) {
        $filteredHotel = DB::select('*')
                ->from('settings')
                ->where('key', '=', 'rategain_hotel_filter')
                ->execute()
                ->current();

        $filteredHotel = json_decode($filteredHotel['value']);

        if (in_array($hotel_id, $filteredHotel)) {
            return true;
        } else {
            return false;
        }
    }

}
