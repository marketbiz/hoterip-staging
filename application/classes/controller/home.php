<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Layout_Admin {
	
	public function action_dashboard() 
	{
    /// API key redirect if role is api
    if (A1::instance()->get_user()->role == 'api')
    {
      // Redirect to home dashboard
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'api', 'action' => 'edit', 'id' => A1::instance()->get_user()->id)));
    }

		$start_date = $this->request->query('start_date');
		$end_date = $this->request->query('end_date');
		
		if ( ! Valid::date($start_date))
		{
			$start_date = date('Y-m-d', strtotime('-1 months'));
		}
		
		if ( ! Valid::date($end_date))
		{
			$end_date = date('Y-m-d');
		}
    
		$start_date_timestamp = strtotime($start_date);
		$end_date_timestamp = strtotime($end_date);

    /*
     * Hotel Active
    */
    if ($this->request->post('hotel-active'))
    {
      // Check if used by deals
      $deals = DB::select()
        ->from('deals_hotels')
        ->where('hotel_id', '=', $this->selected_hotel->id)
        ->order_by('order')
        ->execute();

      // Check if used by features
      $features = DB::select()
        ->from('features')
        ->where('hotel_id', '=', $this->selected_hotel->id)
        ->execute();

      // Check if used by landing page
      $pages_promotions = FALSE;
      $pages_areas = FALSE;

      $pages = DB::select()
        ->from('city_pages')
        ->execute();

      if(count($pages))
      {
        foreach ($pages as $key => $page) {

          $hotels_promotions = unserialize($page['special_promotions']);
          $hotels_areas = unserialize($page['area_hotels']);

          if($hotels_promotions)
          {
            foreach ($hotels_promotions as $hotels_promotion) {
              if($hotels_promotion['id'] == $this->selected_hotel->id)
              {
                $pages_promotions = TRUE;
              }
            }
          }

          if($hotels_areas)
          {
            foreach ($hotels_areas as $hotels_area) {
              if($hotels_area['id'] == $this->selected_hotel->id)
              {
                $pages_areas = TRUE;
              }
            }
          }
        }
      }

      /*
      *
      *VACATION TRIGGER PROCCEESS
      *
      */
        $vacation_triger = array(
        'hotel_id' => $this->selected_hotel->id,
        'room_id' => NULL, 
        'campaign_id' => NULL, 
        // 'start_date' =>$booking_data['check_in'], 
        // 'end_date' =>$booking_data['check_out'],
        'item_id' => NULL,
        'start_date' => NULL,
        'end_date' => NULL,
        'type_trigger' => 1,
        'status' => 0
        );

        Controller_Vacation_Trigger::insertData($vacation_triger);

      // Check if useitemsd by features, deals, landing page
      if(!count($deals) AND !count($features) AND !$pages_promotions AND !$pages_areas)
      {
        try
        {
          $this->selected_hotel->active = $this->request->post('hotel_active');

          $this->selected_hotel->update();

          // Add success notice
          Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        }
        catch (Exception $e)
        {
          // Add error notice
          Notice::add(Notice::ERROR, $e->getMessage());
        }
      }
      else
      {
        Notice::add(Notice::ERROR, Kohana::message('home', 'used_by_features_deals'));
      }
    }

    /*
     * Rate Hotel
    */
    if ($this->request->post('submit-rate') OR $this->request->post('submit-minimum-rate'))
    {
      $commission_rate = (int) $this->request->post('commission_rate');
      $commission_rate_minimum = $this->request->post('commission_rate_minimum') ? (int) $this->request->post('commission_rate_minimum') : $this->selected_hotel->commission_rate_minimum;
      
      // Minimum rate commision validation 
      if ($commission_rate < $this->selected_hotel->commission_rate_minimum)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('home', 'commission_rate_too_low'));
      }
      elseif ($commission_rate > $this->setting->maximum_commission_rate)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('home', 'commission_rate_too_high'));
      }
      elseif ($commission_rate_minimum > $commission_rate)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('home', 'commission_rate_minimum_too_high'));
      }
      else
      {
        try
        {
          $this->selected_hotel->commission_rate = $commission_rate;
          // Minimum rate commision
          $this->selected_hotel->commission_rate_minimum = $commission_rate_minimum;

          $this->selected_hotel->update();
          
          // Get the selected hotel
          $hotel = ORM::factory('hotel')
            ->select('hotel_texts.name')
            ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
            ->where('hotels.id', '=', $this->selected_hotel->id)
            ->where('hotel_texts.language_id', '=', 1)
            ->find();
            
          // Update rank
          set_time_limit(Date::MINUTE * 10);
          Request::factory('cron/update_hotel_rank')
            ->method(Request::GET)
            ->execute();
          
          /// Minimum rate commision success message
          if($this->request->post('submit-rate'))
          {
            // Send email
            Email::factory("$hotel->name Commission Changed", "$hotel->name commission changed to $commission_rate%")
              ->from(Kohana::config('application.email'), Kohana::config('application.name'))
              ->to(Kohana::config('application.email'))
              ->send();
            
            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('home', 'commission_rate_changed'));
          }
          elseif($this->request->post('submit-minimum-rate'))
          {
            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('home', 'commission_rate_minimum_changed'));
          }
        }
        catch (Exception $e)
        {
          // Add error notice
          Notice::add(Notice::ERROR, $e->getMessage());
        }
      }
    }
    
    $hotel_currency = ORM::factory('currency')
      ->where('id', '=', $this->selected_hotel->currency_id)
      ->find();
    
    // Get top rank hotels
    $top_city_hotels = ORM::factory('hotel')
      ->select(
        array('hotel_texts.name', 'name')
      )
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->where('hotels.city_id', '=', $this->selected_hotel->city_id)
      ->limit(10)
      ->order_by('hotels.rank', 'DESC')
      ->order_by('hotels.id', 'ASC')
      ->find_all()
      ->as_array();
    
    $rank_position_in_cities = DB::select(
        'x.id',
        'x.position'
      )
      ->from(array(DB::expr("
        (SELECT t.id, t.city_id, @rownum := @rownum + 1 AS position 
        FROM hotels t
        JOIN (SELECT @rownum := 0) r
        WHERE t.city_id = {$this->selected_hotel->city_id}
        ORDER BY t.rank DESC, t.id ASC)"), 'x')
      )
      ->where('x.id', '=', $this->selected_hotel->id)
      ->where('x.city_id', '=', $this->selected_hotel->city_id)
      ->execute()
      ->get('position');
    
    if ($this->selected_hotel->district_id)
    {
      // Get district rank hotels
      $top_district_hotels = ORM::factory('hotel')
        ->select(
          array('hotel_texts.name', 'name')
        )
        ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
        ->where('hotel_texts.language_id', '=', $this->selected_language->id)
        ->where('hotels.district_id', '=', $this->selected_hotel->district_id)
        ->limit(10)
        ->order_by('rank', 'DESC')
        ->find_all()
        ->as_array();
      
      $rank_position_in_districts = DB::select(
          'x.id',
          'x.position'
        )
        ->from(array(DB::expr("
          (SELECT t.id, t.district_id, @rownum := @rownum + 1 AS position 
          FROM hotels t
          JOIN (SELECT @rownum := 0) r
          WHERE t.district_id = {$this->selected_hotel->district_id}
          ORDER BY t.rank DESC, t.id ASC)"), 'x')
        )
        ->where('x.id', '=', $this->selected_hotel->id)
        ->where('x.district_id', '=', $this->selected_hotel->district_id)
        ->execute()
        ->get('position');
    }
    else
    {
      $top_district_hotels = array();
      $rank_position_in_districts = 0;
    }
    
    // Get selected hotel city name
    $selected_hotel_city = ORM::factory('city')
      ->select(
        array('city_texts.name', 'name')
      )
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->where('cities.id', '=', $this->selected_hotel->city_id)
      ->find();
    
    // Get selected hotel district name
    $selected_hotel_district = ORM::factory('district')
      ->select(
        array('district_texts.name', 'name')
      )
      ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
      ->where('districts.id', '=', $this->selected_hotel->district_id)
      ->find();
    
    $offset = Date::offset($this->selected_hotel->timezone);
     
    // Get today booking
    $today_number_of_bookings = ORM::factory('booking')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where(DB::expr("bookings.created + $offset"), '>=', strtotime('today'))
      ->count_all();
    
    // Get yesterday booking
    $yesterday_number_of_bookings = ORM::factory('booking')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where(DB::expr("bookings.created + $offset"), '>=', strtotime('yesterday'))
      ->where(DB::expr("bookings.created + $offset"), '<=', strtotime('today'))
      ->count_all();
    
    // Get last 7 days booking
    $last_7_days_number_of_bookings = ORM::factory('booking')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where(DB::expr("bookings.created + $offset"), '>=', strtotime('-7 days'))
      ->count_all();
    
    // Get this month booking
    $this_month_number_of_bookings = ORM::factory('booking')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where(DB::expr("bookings.created + $offset"), '>=', strtotime('first day of this month'))
      ->count_all();
    
    // Get top campaigns
    $campaigns = DB::select(
        'campaigns.*',
        array('room_texts.name', 'room_name'),
        array(DB::expr('COUNT(bookings.campaign_id)'), 'total_campaigns')
      )
      ->from('bookings')
      ->join('campaigns')->on('campaigns.id', '=', 'bookings.campaign_id')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
      ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->group_by('bookings.campaign_id')
      ->order_by('total_campaigns', 'DESC')
      ->limit(5)
      ->as_object()
      ->execute()
      ->as_array();
    
    $acceses = DB::select(
				array(DB::expr('COUNT(*)'), 'count'),
				array(DB::expr("FROM_UNIXTIME(created, '%Y-%m-%d')"), 'date')
			)
			->from('accesses')
			->where('hotel_id', '=', $this->selected_hotel->id)
			->group_by('date')
      ->having('date', '>=', $start_date)
      ->having('date', '<=', $end_date)
      ->order_by('date', 'DESC')
			->execute()
			->as_array('date');
		
		$bookings = DB::select(
				array(DB::expr('COUNT(*)'), 'count'),
				array(DB::expr("FROM_UNIXTIME(created, '%Y-%m-%d')"), 'date')
			)
			->from('bookings')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
			->where('rooms.hotel_id', '=', $this->selected_hotel->id)
			->group_by(DB::expr("DAY(FROM_UNIXTIME(created))"))
			->execute()
			->as_array('date');
    
		/**
		 * Create visitors per day plot data
		 */
		
		$visits_per_day = array();
		$bookings_per_day = array();
		
		for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY)
		{
			$date = date('Y-m-d', $i);
      
			if ($access = Arr::get($acceses, $date)) 
			{
				$visits_per_day[] = array($i * 1000, (int) $access['count']);
			}
			else
			{
				$visits_per_day[] = array($i * 1000, 0);
			}
			
			if ($booking = Arr::get($bookings, $date))
			{
				$bookings_per_day[] = array($i * 1000, (int) $booking['count']);
			}
			else
			{
				$bookings_per_day[] = array($i * 1000, 0);
			}
		}
    
		$this->template->main = Kostache::factory('home/dashboard')
			->set('notice', Notice::render())
      ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
      ->set('setting', $this->setting)
      ->set('selected_hotel', $this->selected_hotel)
      ->set('hotel_currency', $hotel_currency)
      ->set('selected_hotel_city', $selected_hotel_city)
      ->set('selected_hotel_district', $selected_hotel_district)
      ->set('top_city_hotels', $top_city_hotels)
      ->set('top_district_hotels', $top_district_hotels)
      ->set('rank_position_in_cities', $rank_position_in_cities)
      ->set('rank_position_in_districts', $rank_position_in_districts)
      ->set('today_number_of_bookings', $today_number_of_bookings)
      ->set('yesterday_number_of_bookings', $yesterday_number_of_bookings)
      ->set('last_7_days_number_of_bookings', $last_7_days_number_of_bookings)
      ->set('this_month_number_of_bookings', $this_month_number_of_bookings)
      ->set('campaigns', $campaigns)
			->set('accesses', $acceses)
			->set('visits_per_day', json_encode($visits_per_day))
			->set('bookings_per_day', json_encode($bookings_per_day));
	}
	
}