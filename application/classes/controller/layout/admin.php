<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Layout_Admin extends Controller {

    public $template;
    public $auto_render = TRUE;
    public $selected_language;
    public $selected_hotel;
    public $setting;

    public function before() {
        // If not logged in 
        if (!A1::instance()->logged_in()) {
            // Redirect to login page
            $this->request->redirect(Route::get('auth')->uri(array('action' => 'login')));
        }

        parent::before();

        if ($this->auto_render) {
            // Get current setting
            $this->setting = (object) ORM::factory('setting')
                            ->find_all()
                            ->as_array('key', 'value');

            // Get logged in admin
            $logged_in_admin = A1::instance()->get_user();

            // Get available languages
            $languages = ORM::factory('language')
                    ->order_by('name')
                    ->find_all();

            // Get selected language id from cookie
            $selected_language_id = Cookie::get('selected_language_id', 1);

            // Get selected language
            $this->selected_language = ORM::factory('language')
                    ->where('id', '=', $selected_language_id)
                    ->find();

            // Set website language
            I18n::lang($this->selected_language->code);

            // Get hotels managed by admin
            $hotels = ORM::factory('hotel')
                    ->select(
                            'hotels.*', array('hotel_texts.name', 'name')
                    )
                    ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
                    ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                    ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
                    ->where('hotel_texts.language_id', '=', $selected_language_id)
                    ->order_by('name')
                    ->find_all();

            // If there is no managed hotels
            if (count($hotels) == 0) {
                // Add notice message
                Notice::add(Notice::ERROR, Kohana::message('admin', 'no_assigned_hotels'));
                // Log user out
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'auth', 'action' => 'logout')));
            }

            // If there is no history of last selected hotel in cookie
            if (!$selected_hotel_id = Cookie::get('selected_hotel_id', 0)) {
                // Set the first managed hotels as selected hotel
                $selected_hotel_id = $hotels->current()->id;
            } else {
                // Check if admin do not have right to manage this selected hotel in the history
                if (ORM::factory('hotel')
                                ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
                                ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
                                ->where('admins_hotels.hotel_id', '=', $selected_hotel_id)
                                ->count_all() == 0) {
                    // Set the first managed hotels as selected hotel
                    $selected_hotel_id = $hotels->current()->id;
                }
            }

            // Get selected hotel
            $this->selected_hotel = ORM::factory('hotel')
                    ->select(
                            'hotels.*', array('hotel_texts.name', 'name')
                    )
                    ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                    ->where('hotels.id', '=', $selected_hotel_id)
                    ->where('hotel_texts.language_id', '=', $selected_language_id)
                    ->find();

            if (!A2::instance()->allowed($this->selected_hotel, 'read')) {
                $this->selected_hotel = $hotels->current();
                // Set cookie
                Cookie::set('selected_hotel_id', $this->selected_hotel->id);
            }

            $total_new_hotel_bookings = ORM::factory('booking')
                    ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
                    ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                    ->where_open()
                    ->where('locker_id', 'IS', NULL)
                    ->or_where('locker_id', '=', 0)
                    ->where_close()
                    ->count_all();

            $new_bookings = ORM::factory('booking')
                    ->select(
                            array('hotel_texts.name', 'hotel_name'), array('room_texts.name', 'room_name')
                    )
                    ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
                    ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                    ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
                    ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                    ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                    ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
                    ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                    ->where('room_texts.language_id', '=', $this->selected_language->id)
                    ->where_open()
                    ->where('locker_id', 'IS', NULL)
                    ->or_where('locker_id', '=', 0)
                    ->where_close()
                    ->order_by('created', 'DESC')
                    ->find_all();

            // Get notice text
            $notice_text = DB::select()
                    ->from('settings')
                    ->where('key', '=', 'admin_notice_text')
                    ->limit(1)
                    ->execute()
                    ->get('value');

            // Check number of data
            $photo_less = ORM::factory('hotel_photo')
                            ->where('hotel_photos.hotel_id', '=', $this->selected_hotel->id)
                            ->count_all() < 8;

            $room_lesss = ORM::factory('room')
                            ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                            ->count_all() < 1;

            $now = date('Y-m-d');
            $end_month = Date::formatted_time("$now +10 month", 'Y-m-d');

            $new_review = ORM::factory('review')
                    ->where('reviews.is_approved', '=', 0)
                    ->count_all();

            $policies_less = ORM::factory('cancellation')
                            ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
                            ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
                            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                            ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                            ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                            ->where('campaigns.is_default', '=', 1)
                            ->where('cancellations.end_date', '>', $end_month)
                            ->count_all() > 0;

            $policies_less = !empty($policies_less) ? FALSE : TRUE;

            $stock_less = ORM::factory('item')
                            ->join('rooms')->on('items.room_id', '=', 'rooms.id')
                            ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                            ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                            ->where('room_texts.language_id', '=', $this->selected_language->id)
                            ->where('items.date', '>', $end_month)
                            ->count_all() < 1;
                            
            $b2b_stock_less = ORM::factory('itemb2b')
                              ->join('rooms')->on('itemb2bs.room_id', '=', 'rooms.id')
                              ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                              ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                              ->where('room_texts.language_id', '=', $this->selected_language->id)
                              ->where('itemb2bs.date', '>', $end_month)
                              ->count_all() < 1;  

            $campaigns_less = ORM::factory('campaign')
                            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                            ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                            ->where('campaigns.is_default', '=', 0)
                            ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                            ->group_by('campaigns.id')
                            ->count_all() < 1;

            $this->template = Kostache::factory('layout/admin')
                    ->partial('sidebar', 'layout/admin/sidebar')
                    ->partial('header', 'layout/admin/header')
                    ->set('title', ucwords($this->request->controller() . ' - ' . $this->request->action()))
                    ->set(array(
                        'home_dashboard' => $logged_in_admin->role == 'api' ? FALSE : TRUE,
                        'hotel_edit' => A2::instance()->allowed($this->selected_hotel, 'edit'),
                        'photo_manage' => A2::instance()->allowed('hotel_photo', 'manage'),
                        'booking_manage' => A2::instance()->allowed('booking', 'manage'),
                        'room_manage' => A2::instance()->allowed('room', 'manage'),
                        'night_manage' => A2::instance()->allowed('night', 'manage'),
                        'cancellation_manage' => A2::instance()->allowed('cancellation', 'manage'),
                        'item_manage' => A2::instance()->allowed('item', 'manage'),
                        'itemb2b_manage' => A2::instance()->allowed('itemb2b', 'manage'),
                        'promotion_manage' => A2::instance()->allowed('promotion', 'manage'),
                        'promotionb2b_manage' => A2::instance()->allowed('promotionb2b', 'manage'),
                        'announcement_manage' => A2::instance()->allowed('announcement', 'manage'),
                        'surcharge_manage' => A2::instance()->allowed('surcharge', 'manage'),
                        'restriction_manage' => A2::instance()->allowed('restriction', 'manage'),
                        'report_finance' => A2::instance()->allowed('report', 'finance'),
                        'log_hotel' => A2::instance()->allowed('log', 'hotel'),
                        'report_booking_all' => A2::instance()->allowed('report', 'booking_all'),
                        'report_finance_all' => A2::instance()->allowed('report', 'finance_all'),
                        'report_csv' => A2::instance()->allowed('csv', 'manage'),
                        'admin_manage' => A2::instance()->allowed('admin', 'manage'),
                        'user_manage' => A2::instance()->allowed('user', 'manage'),
                        'subscribe_manage' => A2::instance()->allowed('subscribe', 'manage'),
                        'hotel_manage' => A2::instance()->allowed('hotel', 'manage'),
                        'hotel_assign' => A2::instance()->allowed('hotel', 'assign'),
                        'review_manage' => A2::instance()->allowed('review', 'manage'),
                        'banner_manage' => A2::instance()->allowed('banner', 'manage'),
                        'feature_manage' => A2::instance()->allowed('feature', 'manage'),
                        'deal_manage' => A2::instance()->allowed('deal', 'manage'),
                        'deal_hotel_manage' => A2::instance()->allowed('deal_hotel', 'manage'),
                        'promotionpage_manage' => A2::instance()->allowed('promotionpage', 'manage'), 
                        'promotionpageb2b_manage' => A2::instance()->allowed('promotionpageb2b', 'manage'), 
                        'district_manage' => A2::instance()->allowed('district', 'manage'),
                        'city_manage' => A2::instance()->allowed('city', 'manage'),
                        'country_manage' => A2::instance()->allowed('country', 'manage'),
                        'page_manage' => A2::instance()->allowed('page', 'manage'),
                        'benefit_manage' => A2::instance()->allowed('benefit', 'manage'),
                        'promotion_search' => A2::instance()->allowed('promotion_search', 'manage'),
                        'promotionb2b_search' => A2::instance()->allowed('promotionb2b_search', 'manage'),
                        'email_send' => A2::instance()->allowed('email', 'send'),
                        'item_export' => A2::instance()->allowed('item', 'export'),
                        'setting_edit' => A2::instance()->allowed('setting', 'edit'),
                        'log_all' => A2::instance()->allowed('log', 'all'),
                        'api_manage' => A2::instance()->allowed('api', 'manage'),
                        'api_access' => A2::instance()->allowed('api', 'edit'),
                        'photo_less' => $photo_less,
                        'room_lesss' => $room_lesss,
                        'policies_less' => $policies_less,
                        'stock_less' => $stock_less,
                        'campaigns_less' => $campaigns_less,
                        'new_review' => $new_review,
                    ))
                    ->set($this->request->controller() . '_' . $this->request->action() . '_selected', TRUE)
                    ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                    ->set('languages', $languages)
                    ->set('hotels', $hotels)
                    ->set('selected_language', $this->selected_language)
                    ->set('selected_hotel', $this->selected_hotel)
                    ->set('logged_in_admin', A1::instance()->get_user())
                    ->set('total_new_hotel_bookings', $total_new_hotel_bookings)
                    ->set('new_bookings', $new_bookings)
                    ->set('total_new_bookings', count($new_bookings))

                    /// Check is role API (including header.mustache)
                    ->set('is_api', $logged_in_admin->role == 'api' ? FALSE : TRUE)
                    ->set('show_notice_text', strlen($notice_text) > 0)
                    ->set('notice_text', $notice_text)
                    ->set('hotelier_index', in_array($this->request->controller() . '-' . $this->request->action(), array(
                        'account-profile',
                        'account-password',
                        'home-dashboard',
                        'hotel-edit',
                        'photo-manage',
                        'photo-add',
                        'photo-edit',
                        'photo-main',
                        'booking-manage',
                        'booking-detail',
                        'booking-edit',
                        'cancellation-manage',
                        'cancellation-add',
                        'cancellation-edit',
                        'room-manage',
                        'room-add',
                        'room-edit',
                        'night-manage',
                        'night-add',
                        'night-edit',
                        'item-manage',
                        'itemb2b-manage',
                        'promotion-manage',
                        'promotionb2b-manage',
                        'promotion-add',
                        'promotion-edit',
                        'promotionb2b-add',
                        'promotionb2b-edit',
                        'announcement-manage',
                        'announcement-detail',
                        'announcement-edit',
                        'surcharge-manage',
                        'restriction-manage',
                        'report-finance',
                        'log-hotel'
                    )))
                    ->set('system_index', in_array($this->request->controller() . '-' . $this->request->action(), array(
                'report-finance_all',
                'report-booking_all',
                'search-promotion',
                'search-promotionb2b',
                'admin-manage',
                'admin-add',
                'admin-edit',
                'subscribe-manage',
                'user-manage',
                'user-edit',
                'hotel-manage',
                'hotel-add',
                'hotel-assign',
                'banner-manage',
                'banner-add',
                'banner-edit',
                'feature-manage',
                'feature-add',
                'feature-edit',
                'deal-manage',
                'deal-add',
                'deal-edit',
                'deal_hotel-manage',
                'deal_hotel-add',
                'deal_hotel-edit',
                'promotionpage-manage',
                'promotionpage-add',
                'promotionpage-edit',
                'promotionpageb2b-manage',
                'promotionpageb2b-add',
                'promotionpageb2b-edit',
                'review-manage',
                'review-add',
                'review-edit',
                'district-manage',
                'district-add',
                'district-edit',
                'city-manage',
                'city-add',
                'city-edit',
                'page-manage',
                'page-edit',
                'country-manage',
                'country-add',
                'country-edit',
                'benefit-manage',
                'email-send',
                'benefit-manage',
                'benefit-edit',
                'benefit-add',
                'item-export',
                'setting-edit',
                'api-manage',
                'api-edit',
                'api-add',
            )));
        }
    }

    public function after() {
        if ($this->auto_render) {
            $this->response->body($this->template);
        }

        parent::after();
    }

}
