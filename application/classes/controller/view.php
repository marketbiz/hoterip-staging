<?php defined('SYSPATH') or die('No direct script access.');

class Controller_View extends Controller_Layout_Admin {
	
	public function action_spreadsheet()
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('view', 'spreadsheet'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		/**
		 * Get values from post
		 */
		$campaign_id = (int) $this->request->post('campaign_id');
		$start_date = $this->request->post('start_date');
		$end_date = $this->request->post('end_date');
		
		/**
		 * Filter 
		 */
		
		if ( ! $campaign_id)
		{
			$campaign = ORM::factory('campaign')
				->select(
					'campaigns.id',
					'campaign_texts.name'
				)
				->join('campaign_texts')->on('campaign_texts.campaign_id', '=', 'campaigns.id')
				->join('rooms')->on('rooms.id', '=', 'campaigns.room_id')
				->where('rooms.hotel_id', '=', $this->selected_hotel->id)
				->order_by('campaign_texts.name')
				->find();
			
			$campaign_id = $campaign->id;
		}
		
		if ( ! $start_date)
		{
			$start_date = date('Y-m-d');
		}
		
		if ( ! $end_date)
		{
			$end_date = date('Y-m-d', strtotime('+1 months'));
		}
		
		$start_date_timestamp = strtotime($start_date);
		$end_date_timestamp = strtotime($end_date);
		
		// If end date before start date
		if ($end_date_timestamp < $start_date_timestamp)
		{
			// Swap start date with end date
			$temp = $start_date;
			$start_date = $end_date;
			$end_date = $temp;
			
			$start_date_timestamp = strtotime($start_date);
			$end_date_timestamp = strtotime($end_date);
		}
		
		/**
		 * Form submitted
		 */
		if ($this->request->post('submit'))
		{
			$dates = $this->request->post('dates');
			
			try
			{
				foreach ($dates as $date)
				{
					$single_sell_replacements = array_filter($this->request->post('single_sell_replacements'));
					$twin_sell_replacements = array_filter($this->request->post('twin_sell_replacements'));
					$other_sell_replacements = array_filter($this->request->post('other_sell_replacements'));

					$values = array(
						'single_sell_replacement' => Arr::get($single_sell_replacements, $date, NULL),
						'twin_sell_replacement' => Arr::get($twin_sell_replacements, $date, NULL),
						'other_sell_replacement' => Arr::get($other_sell_replacements, $date, NULL),
						'is_blackout' => Arr::get($this->request->post('is_blackouts'), $date, 0),
						'is_campaign_inactive' => Arr::get($this->request->post('is_campaign_inactives'), $date, 0) ? 1 : 0,
						'is_earlybird_inactive' => Arr::get($this->request->post('is_earlybird_inactives'), $date, 0)  ? 1 : 0,
						'is_lastminute_inactive' => Arr::get($this->request->post('is_lastminute_inactives'), $date, 0)  ? 1 : 0,
					);

					// Update item campaign
					$item_campaign = ORM::factory('item_campaign')
						->join('items')->on('items.id', '=', 'items_campaigns.item_id')
						->where('items_campaigns.campaign_id', '=', $campaign_id)
						->where('items.date', '=', $date)
						->find();
					
					if ($item_campaign->loaded())
					{
						$item_campaign->update_item_campaign($values);
					}
				}
				
				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('notice', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Rollback transaction
				Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('item_campaign'));
			}
			catch (Exception $e)
			{
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		/**
		 * Build variables for view
		 */
		
		// Create dates array
		$dates = array();
		for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY)
		{
			$dates[] = date('Y-m-d', $i);
		}
		
		// Get selected campaign
		$campaign = ORM::factory('campaign')
			->where('campaigns.id', '=', $campaign_id)
			->find();
		
		// Is Authorized ?
//		if ( ! A2::instance()->allowed($campaign, 'edit'))
//		{
//			// Add error notice
//			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
//			// Redirect to manage rooms
//			$this->request->redirect(Route::get('default')->uri(array('controller' => 'campaign', 'action' => 'manage')));
//		}
		
		// Get campaigns
    $campaigns_query = ORM::factory('campaign')
      ->select(
        array('campaign_texts.name', 'name')
      )
      ->join('campaign_texts')->on('campaign_texts.campaign_id', '=', 'campaigns.id')
			->join('rooms')->on('rooms.id', '=', 'campaigns.room_id')
			->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where('campaign_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name', 'ASC');
		
		if ( ! A2::instance()->allowed('all', 'hoterip'))
		{
			$campaigns_query->where('campaigns.is_hoterip_campaign', '=', 0);
		}
		
		$campaigns = $campaigns_query
      ->find_all();
		
		// If there is no campaigns
		if (count($campaigns) <= 0)
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('campaign', 'no_campaigns'));
			// Redirect to manage campaign
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'campaign', 'action' => 'manage')));
		}
		
		// Get all item campaigns
		$items_campaigns = ORM::factory('item_campaign')
			->select(
				array('campaign_texts.name', 'campaign_name'),
				array('earlybird_texts.name', 'earlybird_name'),
				array('lastminute_texts.name', 'lastminute_name'),
				array('items.date', 'item_date'),
				array('items.stock', 'item_stock'),
				array('items.hoterip_stock', 'item_hoterip_stock'),
				array('items.single_sell', 'item_single_sell'),
				array('items.hoterip_single_net', 'item_hoterip_single_net'),
				array('items.twin_sell', 'item_twin_sell'),
				array('items.hoterip_twin_net', 'item_hoterip_twin_net'),
				array('items.other_sell', 'item_other_sell'),
				array('items.hoterip_other_net', 'item_hoterip_other_net'),
				array('campaigns.price_rate', 'campaign_price_rate'),
				array('campaigns.price_addition', 'campaign_price_addition'),
				array('campaigns.is_hoterip_campaign', 'campaign_is_hoterip_campaign')
			)
			->join('items')->on('items.id', '=', 'items_campaigns.item_id')
			->join('campaigns')->on('campaigns.id', '=', 'items_campaigns.campaign_id')
			->join('campaign_texts')->on('campaign_texts.campaign_id', '=', 'campaigns.id')
			->join('earlybirds', 'left')->on('earlybirds.id', '=', 'campaigns.earlybird_id')
			->join('earlybird_texts', 'left')->on('earlybird_texts.earlybird_id', '=', 'earlybirds.id')
			->join('lastminutes', 'left')->on('lastminutes.id', '=', 'campaigns.lastminute_id')
			->join('lastminute_texts', 'left')->on('lastminute_texts.lastminute_id', '=', 'lastminutes.id')
			->where('items_campaigns.campaign_id', '=', $campaign->id)
			->and_where('items.date', '>=', $start_date)
			->and_where('items.date', '<=', $end_date)
			->and_where('campaign_texts.language_id', '=', $this->selected_language->id)
			->and_where_open()
				->where('earlybird_texts.language_id', '=', $this->selected_language->id)
				->or_where('earlybird_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->and_where_open()
				->where('lastminute_texts.language_id', '=', $this->selected_language->id)
				->or_where('lastminute_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->find_all()
			->as_array('item_date');
		
		$campaign_options = $campaigns->as_array('id', 'name');
		
		// Views
		$this->template->main = Kostache::factory('view/spreadsheet')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('campaign_options', $campaign_options)
			->set('dates', $dates)
			->set('campaign_id', $campaign_id)
			->set('start_date', $start_date)
			->set('end_date', $end_date)
			->set('campaign', $campaign)
			->set('campaigns', $campaigns)
			->set('items_campaigns', $items_campaigns);
	}
	
	public function action_table()
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('view', 'table'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		// Get campaigns
    $campaigns_query = ORM::factory('campaign')
      ->select(
        array('campaign_texts.name', 'name'),
        array('campaign_texts.description', 'description'),
				array('room_texts.name', 'room_name')
      )
      ->join('campaign_texts')->on('campaign_texts.campaign_id', '=', 'campaigns.id')
			->join('rooms')->on('rooms.id', '=', 'campaigns.room_id')
			->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
			->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where('campaign_texts.language_id', '=', $this->selected_language->id)
			->where('room_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name', 'ASC');
		
		if ( ! A2::instance()->allowed('all', 'hoterip'))
		{
			$campaigns_query->where('campaigns.is_hoterip_campaign', '=', 0);
		}
		
		$campaigns = $campaigns_query
      ->find_all()
			->as_array('id');

		// Get today
		$today = Arr::get($_POST, 'today', date('Y-m-d'));
		// Get start date
		$start_date = Arr::get($_POST, 'start_date', date('Y-m-d'));
		// Get end date
		$end_date = date('Y-m-d', strtotime("$start_date +14 days"));
		// Get day difference
    $day_diff = Date::span(strtotime($start_date), strtotime($end_date), 'days');
		
		// Start date < today
		if (strtotime($start_date) < strtotime($today))
		{
			$start_date = $today;
		}

		$single_price_query = "get_price(
			'$today',
			items.date,
			earlybirds.id,
			items_campaigns.is_earlybird_exist,
			items_campaigns.is_earlybird_inactive,
			earlybirds.booking_start_date,
			earlybirds.booking_end_date,
			lastminutes.id,
			items_campaigns.is_lastminute_exist,
			items_campaigns.is_lastminute_inactive,
			lastminutes.days_before,
			lastminutes.booking_start_date,
			lastminutes.booking_end_date,
			items_campaigns.single_sell,
			items_campaigns.single_sell_earlybird_120,
			items_campaigns.single_sell_earlybird_90,
			items_campaigns.single_sell_earlybird_60,
			items_campaigns.single_sell_earlybird_45,
			items_campaigns.single_sell_earlybird_30,
			items_campaigns.single_sell_lastminute,
			items_campaigns.is_blackout,
			campaigns.booking_prohibited_days_before
		)";
		
		$twin_price_query = "get_price(
			'$today',
			items.date,
			earlybirds.id,
			items_campaigns.is_earlybird_exist,
			items_campaigns.is_earlybird_inactive,
			earlybirds.booking_start_date,
			earlybirds.booking_end_date,
			lastminutes.id,
			items_campaigns.is_lastminute_exist,
			items_campaigns.is_lastminute_inactive,
			lastminutes.days_before,
			lastminutes.booking_start_date,
			lastminutes.booking_end_date,
			items_campaigns.twin_sell,
			items_campaigns.twin_sell_earlybird_120,
			items_campaigns.twin_sell_earlybird_90,
			items_campaigns.twin_sell_earlybird_60,
			items_campaigns.twin_sell_earlybird_45,
			items_campaigns.twin_sell_earlybird_30,
			items_campaigns.twin_sell_lastminute,
			items_campaigns.is_blackout,
			campaigns.booking_prohibited_days_before
		)";
					
		$other_price_query = "get_price(
			'$today',
			items.date,
			earlybirds.id,
			items_campaigns.is_earlybird_exist,
			items_campaigns.is_earlybird_inactive,
			earlybirds.booking_start_date,
			earlybirds.booking_end_date,
			lastminutes.id,
			items_campaigns.is_lastminute_exist,
			items_campaigns.is_lastminute_inactive,
			lastminutes.days_before,
			lastminutes.booking_start_date,
			lastminutes.booking_end_date,
			items_campaigns.other_sell,
			items_campaigns.other_sell_earlybird_120,
			items_campaigns.other_sell_earlybird_90,
			items_campaigns.other_sell_earlybird_60,
			items_campaigns.other_sell_earlybird_45,
			items_campaigns.other_sell_earlybird_30,
			items_campaigns.other_sell_lastminute,
			items_campaigns.is_blackout,
			campaigns.booking_prohibited_days_before
		)";

		// Get single price item campaigns
		$single_items_campaigns = ORM::factory('item_campaign')
			->select(
				array('campaign_texts.name', 'campaign_name'),
				array('earlybird_texts.name', 'earlybird_name'),
				array('lastminute_texts.name', 'lastminute_name'),
				array(DB::expr($single_price_query), 'price'),
				array('items.date', 'item_date')
			)
			->join('items')->on('items.id', '=', 'items_campaigns.item_id')
			->join('campaigns')->on('campaigns.id', '=', 'items_campaigns.campaign_id')
			->join('campaign_texts')->on('campaign_texts.campaign_id', '=', 'campaigns.id')
			->join('rooms')->on('rooms.id', '=', 'campaigns.room_id')
			->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('earlybirds', 'left')->on('earlybirds.id', '=', 'campaigns.earlybird_id')
			->join('earlybird_texts', 'left')->on('earlybird_texts.earlybird_id', '=', 'earlybirds.id')
			->join('lastminutes', 'left')->on('lastminutes.id', '=', 'campaigns.lastminute_id')
			->join('lastminute_texts', 'left')->on('lastminute_texts.lastminute_id', '=', 'lastminutes.id')
			->where('hotels.id', '=', $this->selected_hotel->id)
			->and_where('items.date', '>=', $start_date)
			->and_where('items.date', '<=', $end_date)
			->and_where('campaigns.booking_start_date', '<=', $today)
			->and_where('campaigns.booking_end_date', '>=', $today)
			->and_where('campaign_texts.language_id', '=', $this->selected_language->id)
			->and_where_open()
				->where('earlybird_texts.language_id', '=', $this->selected_language->id)
				->or_where('earlybird_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->and_where_open()
				->where('lastminute_texts.language_id', '=', $this->selected_language->id)
				->or_where('lastminute_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->find_all();

		// Create array of items campaigns with date as it's key
		$containers = array();
		foreach ($single_items_campaigns as $item_campaign)
		{
			$containers[$item_campaign->campaign_id][$item_campaign->item_date] = $item_campaign;
		}

		$single_grid = array();

		foreach ($campaigns as $campaign)
		{
			for ($i = 0; $i <= $day_diff; $i++)
			{
				$date = date('Y-m-d', strtotime("$start_date +$i day"));
					
				if (isset($containers[$campaign->id][$date]))
				{
					$single_grid[$campaign->id][$date] = $containers[$campaign->id][$date];
				}
				else
				{
					$item_campaign = new stdClass;
					$item_campaign->price = NULL;
					
					$single_grid[$campaign->id][$date] = $item_campaign;
				}
			}
		}
		
		// Get twin price item campaigns
		$twin_items_campaigns = ORM::factory('item_campaign')
			->select(
				array('campaign_texts.name', 'campaign_name'),
				array('earlybird_texts.name', 'earlybird_name'),
				array('lastminute_texts.name', 'lastminute_name'),
				array(DB::expr($twin_price_query), 'price'),
				array('items.date', 'item_date')
			)
			->join('items')->on('items.id', '=', 'items_campaigns.item_id')
			->join('campaigns')->on('campaigns.id', '=', 'items_campaigns.campaign_id')
			->join('campaign_texts')->on('campaign_texts.campaign_id', '=', 'campaigns.id')
			->join('rooms')->on('rooms.id', '=', 'campaigns.room_id')
			->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('earlybirds', 'left')->on('earlybirds.id', '=', 'campaigns.earlybird_id')
			->join('earlybird_texts', 'left')->on('earlybird_texts.earlybird_id', '=', 'earlybirds.id')
			->join('lastminutes', 'left')->on('lastminutes.id', '=', 'campaigns.lastminute_id')
			->join('lastminute_texts', 'left')->on('lastminute_texts.lastminute_id', '=', 'lastminutes.id')
			->where('hotels.id', '=', $this->selected_hotel->id)
			->and_where('items.date', '>=', $start_date)
			->and_where('items.date', '<=', $end_date)
			->and_where('campaigns.booking_start_date', '<=', $today)
			->and_where('campaigns.booking_end_date', '>=', $today)
			->and_where('campaign_texts.language_id', '=', $this->selected_language->id)
			->and_where_open()
				->where('earlybird_texts.language_id', '=', $this->selected_language->id)
				->or_where('earlybird_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->and_where_open()
				->where('lastminute_texts.language_id', '=', $this->selected_language->id)
				->or_where('lastminute_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->find_all();

		// Create array of items campaigns with date as it's key
		$containers = array();
		foreach ($twin_items_campaigns as $item_campaign)
		{
			$containers[$item_campaign->campaign_id][$item_campaign->item_date] = $item_campaign;
		}

		$twin_grid = array();

		foreach ($campaigns as $campaign)
		{
			for ($i = 0; $i <= $day_diff; $i++)
			{
				$date = date('Y-m-d', strtotime("$start_date +$i day"));
					
				if (isset($containers[$campaign->id][$date]))
				{
					$twin_grid[$campaign->id][$date] = $containers[$campaign->id][$date];
				}
				else
				{
					$item_campaign = new stdClass;
					$item_campaign->price = NULL;
					
					$twin_grid[$campaign->id][$date] = $item_campaign;
				}
			}
		}
		
		// Get other price item campaigns
		$other_items_campaigns = ORM::factory('item_campaign')
			->select(
				array('campaign_texts.name', 'campaign_name'),
				array('earlybird_texts.name', 'earlybird_name'),
				array('lastminute_texts.name', 'lastminute_name'),
				array(DB::expr($other_price_query), 'price'),
				array('items.date', 'item_date')
			)
			->join('items')->on('items.id', '=', 'items_campaigns.item_id')
			->join('campaigns')->on('campaigns.id', '=', 'items_campaigns.campaign_id')
			->join('campaign_texts')->on('campaign_texts.campaign_id', '=', 'campaigns.id')
			->join('rooms')->on('rooms.id', '=', 'campaigns.room_id')
			->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('earlybirds', 'left')->on('earlybirds.id', '=', 'campaigns.earlybird_id')
			->join('earlybird_texts', 'left')->on('earlybird_texts.earlybird_id', '=', 'earlybirds.id')
			->join('lastminutes', 'left')->on('lastminutes.id', '=', 'campaigns.lastminute_id')
			->join('lastminute_texts', 'left')->on('lastminute_texts.lastminute_id', '=', 'lastminutes.id')
			->where('hotels.id', '=', $this->selected_hotel->id)
			->and_where('items.date', '>=', $start_date)
			->and_where('items.date', '<=', $end_date)
			->and_where('campaigns.booking_start_date', '<=', $today)
			->and_where('campaigns.booking_end_date', '>=', $today)
			->and_where('campaign_texts.language_id', '=', $this->selected_language->id)
			->and_where_open()
				->where('earlybird_texts.language_id', '=', $this->selected_language->id)
				->or_where('earlybird_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->and_where_open()
				->where('lastminute_texts.language_id', '=', $this->selected_language->id)
				->or_where('lastminute_texts.language_id', 'IS', DB::expr('NULL'))
			->and_where_close()
			->find_all();

		// Create array of items campaigns with date as it's key
		$containers = array();
		foreach ($other_items_campaigns as $item_campaign)
		{
			$containers[$item_campaign->campaign_id][$item_campaign->item_date] = $item_campaign;
		}

		$other_grid = array();

		foreach ($campaigns as $campaign)
		{
			for ($i = 0; $i <= $day_diff; $i++)
			{
				$date = date('Y-m-d', strtotime("$start_date +$i day"));
					
				if (isset($containers[$campaign->id][$date]))
				{
					$other_grid[$campaign->id][$date] = $containers[$campaign->id][$date];
				}
				else
				{
					$item_campaign = new stdClass;
					$item_campaign->price = NULL;
					
					$other_grid[$campaign->id][$date] = $item_campaign;
				}
			}
		}

		// Views
		$this->template->main = Kostache::factory('view/table')
			->set('notice', Notice::render())
			->set('campaigns', $campaigns)
			->set('today', $today)
			->set('start_date', $start_date)
			->set('end_date', $end_date)
			->set('day_diff', $day_diff)
			->set('single_grid', $single_grid)
			->set('twin_grid', $twin_grid)
			->set('other_grid', $other_grid);
	}
	
}