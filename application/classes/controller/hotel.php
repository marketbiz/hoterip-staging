<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Hotel extends Controller_Layout_Admin {

    public function before() {
        if ($this->request->action() == 'change') {
            $this->auto_render = FALSE;
        }

        return parent::before();
    }

    public function action_change() {
        // Get hotel id
        $hotel_id = $this->request->param('id');

        // Get hotel
        $hotel = ORM::factory('hotel')
                ->where('id', '=', $hotel_id)
                ->find();

        if (A2::instance()->allowed($hotel, 'read')) {
            // Set selected hotel id in cookie
            Cookie::set('selected_hotel_id', $hotel_id);
        }

        // Redirect back
        $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }

    public function action_change_edit() {
        // Get hotel id
        $hotel_id = $this->request->param('id');

        // Get hotel
        $hotel = ORM::factory('hotel')
                ->where('id', '=', $hotel_id)
                ->find();

        // If admin is allowed to edit this hotel
        if (A2::instance()->allowed($hotel, 'read')) {
            // If hotel id is not the same with selected hotel
            if ($hotel->id != $this->selected_hotel->id) {
                // Set selected hotel id in cookie
                Cookie::set('selected_hotel_id', $hotel_id);
            }
            // Redirect to edit
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'hotel', 'action' => 'edit')));
        } else {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect back
            $this->request->redirect($this->request->referrer());
        }
    }

    public function action_manage() {
        // Is Authorized ?
        if (!A2::instance()->allowed('hotel', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        $page = Arr::get($this->request->query(), 'page', 1);
        $items_per_page = 30;
        $offset = ($page - 1) * $items_per_page;

        if ($this->request->post('delete_checked')) {
            if (A2::instance()->allowed('hotel', 'delete')) {
                $hotel_ids = $this->request->post('ids');

                foreach ($hotel_ids as $hotel_id) {
                    // Get hotel
                    $hotel = ORM::factory('hotel')
                            ->where('id', '=', $hotel_id)
                            ->find();

                    // Get hotel images directory
                    $hotel_images_directory = Kohana::config('application.hotel_images_directory');

                    // Get hotel directory
                    $hotel_directory = $hotel_images_directory . DIRECTORY_SEPARATOR . $hotel->url_segment;

                    // If directory exist
                    if (is_dir($hotel_directory)) {
                        // Delete hotel image folders
                        File::remove_directory($hotel_directory);
                    }

                    // Delete hotel
                    $hotel->delete();
                }
            }
        }

        $hotels = ORM::factory('hotel')
                ->select(
                        array('hotel_texts.name', 'name'), array('city_texts.name', 'city_name'), array('country_texts.name', 'country_name')
                )
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->join('countries')->on('countries.id', '=', 'cities.country_id')
                ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->where('country_texts.language_id', '=', $this->selected_language->id)
                ->order_by('hotel_texts.name', 'ASC')
                ->order_by('city_texts.name', 'ASC');

        if ($keyword = $this->request->post('keyword')) {
            $hotels->where('hotel_texts.name', 'LIKE', "%$keyword%");
        }

        $total_hotels = $hotels
                ->reset(FALSE)
                ->count_all();

        $hotels = $hotels
                ->limit($items_per_page)
                ->offset($offset)
                ->find_all();

        // Create pagination
        $pagination = Pagination::factory(array(
                    'items_per_page' => $items_per_page,
                    'total_items' => $total_hotels,
        ));

        $this->template->main = Kostache::factory('hotel/manage')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('hotels', $hotels)
                ->set('pagination', $pagination);
    }

    public function action_add() {
        // Is Authorized ?
        if (!A2::instance()->allowed('hotel', 'add')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        if ($this->request->post('submit')) {
            // Start transaction
            Database::instance()->begin();

            try {
                // Get post request
                $values = $this->request->post();
                // Remove blank values so we can insert blank value as NULL
                $values = array_filter($values);
                // Set default value for checkboxes value
                $values['is_pet_allowed'] = Arr::get($this->request->post(), 'is_pet_allowed') ? 1 : 0;
                // Set default value for number_of_rooms
                $values['number_of_rooms'] = Arr::get($this->request->post(), 'number_of_rooms', 0);
                // Set default value for number_of_floors
                $values['number_of_floors'] = Arr::get($this->request->post(), 'number_of_floors', 0);
                // Set default value for fax
                $values['fax'] = Arr::get($this->request->post(), 'fax', 0);
                // Set default value for opening_year
                $values['opening_year'] = Arr::get($this->request->post(), 'opening_year', 0);
                // Set default value for renovation_year
                $values['renovation_year'] = Arr::get($this->request->post(), 'renovation_year', 0);
                // Set default value for distance_from_airport
                $values['distance_from_airport'] = Arr::get($this->request->post(), 'distance_from_airport', 0);
                // Set default value for time_from_airport
                $values['time_from_airport'] = Arr::get($this->request->post(), 'time_from_airport', 0);
                // Set default value for distance_from_city_center
                $values['distance_from_city_center'] = Arr::get($this->request->post(), 'distance_from_city_center', 0);
                // Set default value for distance_from_station
                $values['distance_from_station'] = Arr::get($this->request->post(), 'distance_from_station', 0);
                // Set default value for room_voltage
                $values['room_voltage'] = Arr::get($this->request->post(), 'room_voltage', 0);
                // Set default value for check_out_time
                $values['check_in_time'] = Arr::get($this->request->post(), 'check_in_time', NULL);
                // Set default value for check_out_time
                $values['check_out_time'] = Arr::get($this->request->post(), 'check_out_time', NULL);

                /*
                 * Validation
                 */

                //validation description
                foreach ($values['descriptions'] as $des_k => $description) {
                    $validation = Validation::factory(array(
                                'Description' => $description)
                    );
                    if ($des_k == 1) {
                        $validation = $validation
                                ->rule('Description', 'not_empty');
                    }
                    $validation = $validation
                            ->rule('Description', 'max_length', array($description, 1000));

                    if (!$validation->check()) {
                        throw new Validation_Exception($validation);
                    }
                }

                /*
                 * Create url segment
                 */
                $base_url_segment = URL::title(Arr::path($values, 'names.1'));

                $url_segment = $base_url_segment;
                $counter = 2;
                $stop = FALSE;

                do {
                    if (ORM::factory('hotel')
                                    ->where('url_segment', '=', $url_segment)
                                    ->count_all() > 0) {
                        $url_segment = $base_url_segment . '-' . $counter;
                        $counter++;
                    } else {
                        $stop = TRUE;
                    }
                } while (!$stop);

                $values['url_segment'] = $url_segment;

                // Create hotel
                $hotel = ORM::factory('hotel')
                        ->create_hotel($values);

                // Get hotel images directory
                $hotel_images_directory = Kohana::config('application.hotel_images_directory');

                // Get hotel directory
                $hotel_directory = $hotel_images_directory . DIRECTORY_SEPARATOR . $hotel->url_segment;

                // If directory not exist
                if (!is_dir($hotel_directory)) {
                    mkdir($hotel_directory);
                }

                // Get admin
                $admin = A1::instance()->get_user();
                // Assign hotel to admin
                $admin->add('hotels', $hotel);

                /// Assign Hotels to New Admin
                // Get admin
                $admins = ORM::factory('admin')
                        ->where('role', '=', 'admin')
                        ->join('roles')->on('roles.value', '=', 'admins.role')
                        ->find_all();

                if ($hotel->id AND count($admins) > 0) {
                    // If hotel found
                    if ($hotel->loaded()) {
                        foreach ($admins as $key => $admin_data) {

                            $admins_hotels = DB::select()
                                    ->from('admins_hotels')
                                    ->where('admins_hotels.admin_id', '=', $admin_data->id)
                                    ->where('hotel_id', '=', $hotel->id)
                                    ->limit(1)
                                    ->execute();

                            // If admin not assigned
                            if (!count($admins_hotels)) {
                                // Assign hotel to admin
                                DB::insert('admins_hotels')
                                        ->columns(array('admin_id', 'hotel_id'))
                                        ->values(array($admin_data->id, $hotel->id))
                                        ->execute();
                            }
                        }
                    }
                }

                // Write Log
                ORM::factory('log')
                        ->values(array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $hotel->id,
                            'action' => 'Added Hotel',
                            'room_texts' => NULL,
                            'text' => '-',
                            'data' => NULL,
                            'created' => time(),
                        ))
                        ->create();

                // Commit database
                Database::instance()->commit();

                // Set selected hotel id in cookie
                Cookie::set('selected_hotel_id', $hotel->id);

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
                // Redirect to edit
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'hotel', 'action' => 'edit')));
            } catch (Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->array->errors('hotel'));
            } catch (ORM_Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('hotel'));
            } catch (Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        $languages = ORM::factory('language')
                ->find_all();

        $hotel_types = ORM::factory('hotel_type')
                ->find_all();

        $cities = DB::select(
                        'cities.id', array('city_texts.name', 'name'), array('country_texts.name', 'country_name')
                )
                ->from('cities')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->join('countries')->on('countries.id', '=', 'cities.country_id')
                ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->where('country_texts.language_id', '=', $this->selected_language->id)
                ->order_by('city_texts.name')
                ->order_by('country_texts.name')
                ->as_object()
                ->execute()
                ->as_array();

        $districts = DB::select(
                        'districts.id', 'districts.city_id', array('district_texts.name', 'name')
                )
                ->from('districts')
                ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
                ->where('district_texts.language_id', '=', $this->selected_language->id)
                ->order_by('district_texts.name')
                ->as_object()
                ->execute()
                ->as_array();

        $timezones = ORM::factory('timezone')
                ->find_all();

        $currencies = DB::select()
                ->from('currencies')
                ->as_object()
                ->execute()
                ->as_array();

        $this->template->main = Kostache::factory('hotel/add')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('languages', $languages)
                ->set('hotel_types', $hotel_types)
                ->set('cities', $cities)
                ->set('districts', $districts)
                ->set('timezones', $timezones)
                ->set('currencies', $currencies);
    }

    public function action_edit() {
        /// API key redirect if role is api
        if (A1::instance()->get_user()->role == 'api') {
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'api', 'action' => 'edit', 'id' => A1::instance()->get_user()->id)));
        }
        if ($this->request->post('submit')) {
            // If posted hotel id different with current selected hotel
            // We are doing this check to prevent user change selected hotel in new window
            // and then submit this page
            if ($this->request->post('id') != $this->selected_hotel->id) {
                // Add warning message
                Notice::add(Notice::WARNING, Kohana::message('general', 'selected_hotel_changed'));
                // Redirect
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'hotel', 'action' => 'edit')));
            } else {
                // Get hotel
                $hotel = ORM::factory('hotel')
                        ->select(array('hotel_types.name', 'type'))
                        ->join('hotel_types')->on('hotel_types.id', '=', 'hotels.hotel_type_id')
                        ->where('hotels.id', '=', $this->selected_hotel->id)
                        ->find();

                // If hotel not found 
                if (!$hotel->loaded()) {
                    // Add error notice
                    Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
                    // Redirect to home dashboard
                    $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
                }

                // Start transaction
                Database::instance()->begin();

                try {
                    // Get post request
                    $values = $this->request->post();
                    // Remove blank values so we can insert blank value as NULL
                    $values = array_filter($values);
                    // Set default value for checkboxes value
                    $values['is_pet_allowed'] = Arr::get($this->request->post(), 'is_pet_allowed') ? 1 : 0;
                    // Set default value for number_of_rooms
                    $values['number_of_rooms'] = Arr::get($this->request->post(), 'number_of_rooms', 0);
                    // Set default value for number_of_floors
                    $values['number_of_floors'] = Arr::get($this->request->post(), 'number_of_floors', 0);
                    // Set default value for fax
                    $values['fax'] = Arr::get($this->request->post(), 'fax', 0);
                    // Set default value for opening_year
                    $values['opening_year'] = Arr::get($this->request->post(), 'opening_year', 0);
                    // Set default value for renovation_year
                    $values['renovation_year'] = Arr::get($this->request->post(), 'renovation_year', 0);
                    // Set default value for distance_from_airport
                    $values['distance_from_airport'] = Arr::get($this->request->post(), 'distance_from_airport', 0);
                    // Set default value for time_from_airport
                    $values['time_from_airport'] = Arr::get($this->request->post(), 'time_from_airport', 0);
                    // Set default value for distance_from_city_center
                    $values['distance_from_city_center'] = Arr::get($this->request->post(), 'distance_from_city_center', 0);
                    // Set default value for distance_from_station
                    $values['distance_from_station'] = Arr::get($this->request->post(), 'distance_from_station', 0);
                    // Set default value for check_out_time
                    $values['check_in_time'] = Arr::get($this->request->post(), 'check_in_time', NULL);
                    // Set default value for check_out_time
                    $values['check_out_time'] = Arr::get($this->request->post(), 'check_out_time', NULL);

                    /*
                     * Validation
                     */

                    //validation description
                    foreach ($values['descriptions'] as $des_k => $description) {
                        $validation = Validation::factory(array(
                                    'Description' => $description)
                        );
                        if ($des_k == 1) {
                            $validation = $validation
                                    ->rule('Description', 'not_empty');
                        }
                        $validation = $validation
                                ->rule('Description', 'max_length', array($description, 1000));

                        if (!$validation->check()) {
                            throw new Validation_Exception($validation);
                        }
                    }

                    /*
                     * Create url segment
                     */

                    $base_url_segment = URL::title(Arr::path($values, 'names.1'));

                    $url_segment = $base_url_segment;
                    $counter = 2;
                    $stop = FALSE;

                    do {
                        if (ORM::factory('hotel')
                                        ->where('id', '<>', $hotel->id)
                                        ->where('url_segment', '=', $url_segment)
                                        ->count_all() > 0) {
                            $url_segment = $base_url_segment . '-' . $counter;
                            $counter++;
                        } else {
                            $stop = TRUE;
                        }
                    } while (!$stop);

                    $values['url_segment'] = $url_segment;

                    // Get url segment before hotel updated
                    $old_url_segment = $hotel->url_segment;

                    // Clone hotel to write log
                    $prev_hotel = clone $hotel;

                    //get admin
                    $admin = A1::instance()->get_user();

                    //if currency is change
                    if (($hotel->currency_id != $values['currency_id']) && ($admin->role == 'admin')) {
                        $this->isChangeCurrencyHotel($hotel, $values);
                    }
                    // Update hotel
                    $hotel->update_hotel($values);

                    // If url_segment is different we should update hotel images
                    if ($old_url_segment != $hotel->url_segment) {
                        // Get hotel images directory
                        $hotel_images_directory = Kohana::config('application.hotel_images_directory');

                        // Get hotel directory
                        $hotel_directory = $hotel_images_directory . DIRECTORY_SEPARATOR . $old_url_segment;

                        // If directory not exist
                        if (!is_dir($hotel_directory)) {
                            mkdir($hotel_directory);
                        }

                        // If file exists
                        if ($hotel->basename AND file_exists($hotel_directory . DIRECTORY_SEPARATOR . $hotel->basename)) {
                            // Get hotel basename
                            $old_basename = $hotel->basename;

                            // Update hotel basename
                            $hotel->basename = $hotel->url_segment . '.jpg';
                            $hotel->update();

                            // Rename main photo
                            rename($hotel_directory . DIRECTORY_SEPARATOR . $old_basename, $hotel_directory . DIRECTORY_SEPARATOR . $hotel->basename);
                        }

                        // If file exists
                        if ($hotel->small_basename AND file_exists($hotel_directory . DIRECTORY_SEPARATOR . $hotel->small_basename)) {
                            // Get hotel small basename
                            $old_small_basename = $hotel->small_basename;

                            // Update hotel basename
                            $hotel->small_basename = $hotel->url_segment . '-small.jpg';
                            $hotel->update();

                            // Rename main photo
                            rename($hotel_directory . DIRECTORY_SEPARATOR . $old_small_basename, $hotel_directory . DIRECTORY_SEPARATOR . $hotel->small_basename);
                        }

                        // Rename hotel photos
                        $hotel_photos = ORM::factory('hotel_photo')
                                ->where('hotel_id', '=', $hotel->id)
                                ->find_all();

                        foreach ($hotel_photos as $hotel_photo) {
                            // If file exists
                            if (file_exists($hotel_directory . DIRECTORY_SEPARATOR . $hotel_photo->basename) AND
                                    file_exists($hotel_directory . DIRECTORY_SEPARATOR . $hotel_photo->small_basename) AND
                                    file_exists($hotel_directory . DIRECTORY_SEPARATOR . $hotel_photo->large_basename)) {
                                // Get new basename
                                $new_photo_basename = str_replace($old_url_segment, $hotel->url_segment, $hotel_photo->basename);
                                $new_photo_small_basename = str_replace($old_url_segment, $hotel->url_segment, $hotel_photo->small_basename);
                                $new_photo_large_basename = str_replace($old_url_segment, $hotel->url_segment, $hotel_photo->large_basename);

                                // Rename photo
                                rename($hotel_directory . DIRECTORY_SEPARATOR . $hotel_photo->basename, $hotel_directory . DIRECTORY_SEPARATOR . $new_photo_basename);
                                rename($hotel_directory . DIRECTORY_SEPARATOR . $hotel_photo->small_basename, $hotel_directory . DIRECTORY_SEPARATOR . $new_photo_small_basename);
                                rename($hotel_directory . DIRECTORY_SEPARATOR . $hotel_photo->large_basename, $hotel_directory . DIRECTORY_SEPARATOR . $new_photo_large_basename);

                                // Update row
                                ORM::factory('hotel_photo')
                                        ->where('id', '=', $hotel_photo->id)
                                        ->find()
                                        ->values(array(
                                            'basename' => $new_photo_basename,
                                            'small_basename' => $new_photo_small_basename,
                                            'large_basename' => $new_photo_large_basename,
                                        ))
                                        ->update();
                            }
                        }

                        // Rename directory
                        rename($hotel_directory, $hotel_images_directory . DIRECTORY_SEPARATOR . $hotel->url_segment);
                    }

                    // Write log
                    ORM::factory('log')
                            ->values(array(
                                'admin_id' => A1::instance()->get_user()->id,
                                'hotel_id' => $hotel->id,
                                'action' => 'Edited Hotel',
                                'room_texts' => NULL,
                                'text' => 'Star: :prev_star -> :star. '
                                . 'Address: :prev_address -> :address. '
                                . 'Type: :prev_type -> :type. '
                                . 'Telephone: :prev_telephone -> :telephone. '
                                . 'Fax: :prev_fax -> :fax. '
                                . 'Currency: :prev_curr -> :curr. '
                                . 'Opening Year: :prev_opening_year -> :opening_year. '
                                . 'Renovation Year: :prev_renovation_year -> :renovation_year. '
                                . 'Number of Rooms: :prev_number_of_rooms -> :number_of_rooms. '
                                . 'Number of Floors: :prev_number_of_floors -> :number_of_floors. '
                                . 'Distance from Airport: :prev_distance_from_airport -> :distance_from_airport. '
                                . 'Time from Airport: :prev_time_from_airport -> :time_from_airport. '
                                . 'Distance from City Center: :prev_distance_from_city_center -> :distance_from_city_center. '
                                . 'Distance from Station: :prev_distance_from_station -> :distance_from_station. '
                                . 'Room Voltage: :prev_room_voltage -> :room_voltage. '
                                . 'Children Age: :prev_child_age_from - :prev_child_age_until -> :child_age_from - :child_age_until. '
                                . 'Check-in Time: :prev_check_in_time -> :check_in_time. '
                                . 'Check-out Time: :prev_check_out_time -> :check_out_time. '
                                . 'Pet Allowed: :prev_is_pet_allowed -> :is_pet_allowed. '
                                . 'Timezone: :prev_timezone -> :timezone. '
                                . 'Coordinate: :prev_coordinate -> :coordinate. '
                                . 'Sales Tax: :prev_tax_rate -> :tax_rate. '
                                . 'Service Charge: :prev_service_charge_rate -> :service_charge_rate. '
                                . 'Commission: :prev_commission_rate -> :commission_rate. '
                                . 'Point Rate: :prev_point_rate -> :point_rate. ',
                                'data' => serialize(array(
                                    ':prev_star' => $prev_hotel->star,
                                    ':prev_address' => $prev_hotel->address,
                                    ':prev_type' => $prev_hotel->type,
                                    ':prev_telephone' => $prev_hotel->telephone,
                                    ':prev_fax' => $prev_hotel->fax,
                                    ':prev_curr' => $prev_hotel->currency_id,
                                    ':prev_opening_year' => $prev_hotel->opening_year,
                                    ':prev_renovation_year' => $prev_hotel->renovation_year,
                                    ':prev_number_of_rooms' => $prev_hotel->number_of_rooms,
                                    ':prev_number_of_floors' => $prev_hotel->number_of_floors,
                                    ':prev_distance_from_airport' => $prev_hotel->distance_from_airport,
                                    ':prev_time_from_airport' => $prev_hotel->time_from_airport,
                                    ':prev_distance_from_city_center' => $prev_hotel->distance_from_city_center,
                                    ':prev_distance_from_station' => $prev_hotel->distance_from_station,
                                    ':prev_room_voltage' => $prev_hotel->room_voltage,
                                    ':prev_child_age_from' => $prev_hotel->child_age_from,
                                    ':prev_child_age_until' => $prev_hotel->child_age_until,
                                    ':prev_check_in_time' => $prev_hotel->check_in_time,
                                    ':prev_check_out_time' => $prev_hotel->check_out_time,
                                    ':prev_is_pet_allowed' => $prev_hotel->is_pet_allowed ? 'Yes' : 'No',
                                    ':prev_timezone' => $prev_hotel->timezone,
                                    ':prev_coordinate' => $prev_hotel->coordinate,
                                    ':prev_tax_rate' => $prev_hotel->tax_rate,
                                    ':prev_service_charge_rate' => $prev_hotel->service_charge_rate,
                                    ':prev_commission_rate' => $prev_hotel->commission_rate,
                                    ':prev_point_rate' => $prev_hotel->point_rate,
                                    ':star' => $hotel->star,
                                    ':address' => $hotel->address,
                                    ':type' => $hotel->type,
                                    ':telephone' => $hotel->telephone,
                                    ':fax' => $hotel->fax,
                                    ':curr' => $hotel->currency_id,
                                    ':opening_year' => $hotel->opening_year,
                                    ':renovation_year' => $hotel->renovation_year,
                                    ':number_of_rooms' => $hotel->number_of_rooms,
                                    ':number_of_floors' => $hotel->number_of_floors,
                                    ':distance_from_airport' => $hotel->distance_from_airport,
                                    ':time_from_airport' => $hotel->time_from_airport,
                                    ':distance_from_city_center' => $hotel->distance_from_city_center,
                                    ':distance_from_station' => $hotel->distance_from_station,
                                    ':room_voltage' => $hotel->room_voltage,
                                    ':child_age_from' => $hotel->child_age_from,
                                    ':child_age_until' => $hotel->child_age_until,
                                    ':check_in_time' => $hotel->check_in_time,
                                    ':check_out_time' => $hotel->check_out_time,
                                    ':is_pet_allowed' => $hotel->is_pet_allowed ? 'Yes' : 'No',
                                    ':timezone' => $hotel->timezone,
                                    ':coordinate' => $hotel->coordinate,
                                    ':tax_rate' => $hotel->tax_rate,
                                    ':service_charge_rate' => $hotel->service_charge_rate,
                                    ':commission_rate' => $hotel->commission_rate,
                                    ':point_rate' => $hotel->point_rate,
                                )),
                                'created' => time(),
                            ))
                            ->create();

                    // Get QA admins emails
                    $qa_emails = ORM::factory('admin')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->where('admins.role', '=', 'quality-assurance')
                            ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                            ->find_all()
                            ->as_array('email', 'name');

                    // If there is QA admins
                    if (count($qa_emails) > 0) {
                        // Send email
                        Email::factory(strtr(':hotel_name - Edited Hotel', array(
                                    ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                        )), strtr(
                                                "Action: Edited Hotel.\n"
                                                . "By: :admin_username (:admin_name).\n"
                                                . "Address: :prev_address -> :address.\n"
                                                . "Type: :prev_type -> :type.\n"
                                                . "Telephone: :prev_telephone -> :telephone.\n"
                                                . "Fax: :prev_fax -> :fax.\n"
                                                . "Opening Year: :prev_opening_year -> :opening_year.\n"
                                                . "Renovation Year: :prev_renovation_year -> :renovation_year.\n"
                                                . "Number of Rooms: :prev_number_of_rooms -> :number_of_rooms.\n"
                                                . "Number of Floors: :prev_number_of_floors -> :number_of_floors.\n"
                                                . "Distance from Airport: :prev_distance_from_airport -> :distance_from_airport.\n"
                                                . "Time from Airport: :prev_time_from_airport -> :time_from_airport.\n"
                                                . "Distance from City Center: :prev_distance_from_city_center -> :distance_from_city_center.\n"
                                                . "Distance from Station: :prev_distance_from_station -> :distance_from_station.\n"
                                                . "Room Voltage: :prev_room_voltage -> :room_voltage.\n"
                                                . "Children Age: :prev_child_age_from - :prev_child_age_until -> :child_age_from - :child_age_until.\n"
                                                . "Check-in Time: :prev_check_in_time -> :check_in_time.\n"
                                                . "Check-out Time: :prev_check_out_time -> :check_out_time.\n"
                                                . "Pet Allowed: :prev_is_pet_allowed -> :is_pet_allowed.\n"
                                                . "Timezone: :prev_timezone -> :timezone.\n"
                                                . "Coordinate: :prev_coordinate -> :coordinate.\n"
                                                . "Sales Tax: :prev_tax_rate -> :tax_rate.\n"
                                                . "Service Charge: :prev_service_charge_rate -> :service_charge_rate.\n"
                                                . "Commission: :prev_commission_rate -> :commission_rate.\n"
                                                . "Point Rate: :prev_point_rate -> :point_rate.", array(
                                    ':admin_username' => A1::instance()->get_user()->username,
                                    ':admin_name' => A1::instance()->get_user()->name,
                                    ':prev_star' => $prev_hotel->star,
                                    ':prev_address' => $prev_hotel->address,
                                    ':prev_type' => $prev_hotel->type,
                                    ':prev_telephone' => $prev_hotel->telephone,
                                    ':prev_fax' => $prev_hotel->fax,
                                    ':prev_opening_year' => $prev_hotel->opening_year,
                                    ':prev_renovation_year' => $prev_hotel->renovation_year,
                                    ':prev_number_of_rooms' => $prev_hotel->number_of_rooms,
                                    ':prev_number_of_floors' => $prev_hotel->number_of_floors,
                                    ':prev_distance_from_airport' => $prev_hotel->distance_from_airport,
                                    ':prev_time_from_airport' => $prev_hotel->time_from_airport,
                                    ':prev_distance_from_city_center' => $prev_hotel->distance_from_city_center,
                                    ':prev_distance_from_station' => $prev_hotel->distance_from_station,
                                    ':prev_room_voltage' => $prev_hotel->room_voltage,
                                    ':prev_child_age_from' => $prev_hotel->child_age_from,
                                    ':prev_child_age_until' => $prev_hotel->child_age_until,
                                    ':prev_check_in_time' => $prev_hotel->check_in_time,
                                    ':prev_check_out_time' => $prev_hotel->check_out_time,
                                    ':prev_is_pet_allowed' => $prev_hotel->is_pet_allowed ? 'Yes' : 'No',
                                    ':prev_timezone' => $prev_hotel->timezone,
                                    ':prev_coordinate' => $prev_hotel->coordinate,
                                    ':prev_tax_rate' => $prev_hotel->tax_rate,
                                    ':prev_service_charge_rate' => $prev_hotel->service_charge_rate,
                                    ':prev_commission_rate' => $prev_hotel->commission_rate,
                                    ':prev_point_rate' => $prev_hotel->point_rate,
                                    ':star' => $hotel->star,
                                    ':address' => $hotel->address,
                                    ':type' => $hotel->type,
                                    ':telephone' => $hotel->telephone,
                                    ':fax' => $hotel->fax,
                                    ':opening_year' => $hotel->opening_year,
                                    ':renovation_year' => $hotel->renovation_year,
                                    ':number_of_rooms' => $hotel->number_of_rooms,
                                    ':number_of_floors' => $hotel->number_of_floors,
                                    ':distance_from_airport' => $hotel->distance_from_airport,
                                    ':time_from_airport' => $hotel->time_from_airport,
                                    ':distance_from_city_center' => $hotel->distance_from_city_center,
                                    ':distance_from_station' => $hotel->distance_from_station,
                                    ':room_voltage' => $hotel->room_voltage,
                                    ':child_age_from' => $hotel->child_age_from,
                                    ':child_age_until' => $hotel->child_age_until,
                                    ':check_in_time' => $hotel->check_in_time,
                                    ':check_out_time' => $hotel->check_out_time,
                                    ':is_pet_allowed' => $hotel->is_pet_allowed ? 'Yes' : 'No',
                                    ':timezone' => $hotel->timezone,
                                    ':coordinate' => $hotel->coordinate,
                                    ':tax_rate' => $hotel->tax_rate,
                                    ':service_charge_rate' => $hotel->service_charge_rate,
                                    ':commission_rate' => $hotel->commission_rate,
                                    ':point_rate' => $hotel->point_rate,
                                )))
                                ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                ->to($qa_emails)
                                ->send();
                    }

                    // Commit transaction
                    Database::instance()->commit();

                    // Add success notice
                    Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
                } catch (Validation_Exception $e) {
                    // Rollback transaction
                    Database::instance()->rollback();
                    // Add error notice
                    Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->array->errors('hotel'));
                } catch (ORM_Validation_Exception $e) {
                    // Rollback transaction
                    Database::instance()->rollback();
                    // Add error notice
                    Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('hotel')));
                } catch (Exception $e) {
                    // Rollback transaction
                    Database::instance()->rollback();
                    // Add error notice
                    Notice::add(Notice::ERROR, $e->getMessage());
                }
            }
        }

        $hotel = ORM::factory('hotel')
                ->where('id', '=', $this->selected_hotel->id)
                ->find();

        $hotel_texts = ORM::factory('hotel_text')
                ->where('hotel_id', '=', $hotel->id)
                ->find_all()
                ->as_array('language_id');

        $hotel_facility = ORM::factory('hotel_facility')
                ->where('hotel_id', '=', $hotel->id)
                ->find();

        $hotel_service = ORM::factory('hotel_service')
                ->where('hotel_id', '=', $hotel->id)
                ->find();

        $languages = ORM::factory('language')
                ->find_all();

        $hotel_types = ORM::factory('hotel_type')
                ->order_by('name')
                ->find_all();

        $cities = DB::select(
                        'cities.id', array('city_texts.name', 'name'), array('country_texts.name', 'country_name')
                )
                ->from('cities')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->join('countries')->on('countries.id', '=', 'cities.country_id')
                ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->where('country_texts.language_id', '=', $this->selected_language->id)
                ->order_by('country_texts.name')
                ->order_by('city_texts.name')
                ->as_object()
                ->execute()
                ->as_array();

        $districts = DB::select(
                        'districts.id', 'districts.city_id', array('district_texts.name', 'name')
                )
                ->from('districts')
                ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
                ->where('district_texts.language_id', '=', $this->selected_language->id)
                ->order_by('district_texts.name')
                ->as_object()
                ->execute()
                ->as_array();

        $timezones = ORM::factory('timezone')
                ->find_all();

        $currencies = DB::select()
                ->from('currencies')
                ->as_object()
                ->execute()
                ->as_array();

        $this->template->main = Kostache::factory('hotel/edit')
                ->set('notice', Notice::render())
                ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
                ->set('values', $this->request->post())
                ->set('languages', $languages)
                ->set('hotel_types', $hotel_types)
                ->set('cities', $cities)
                ->set('districts', $districts)
                ->set('hotel', $hotel)
                ->set('hotel_texts', $hotel_texts)
                ->set('hotel_facility', $hotel_facility)
                ->set('hotel_service', $hotel_service)
                ->set('timezones', $timezones)
                ->set('currencies', $currencies);
    }

    public function action_assign() {
        // Is Authorized ?
        if (!A2::instance()->allowed('hotel', 'assign')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        /**
         * Get values from post
         */
        $admin_id = (int) $this->request->post('admin_id');
        $hotel_id = (int) $this->request->post('hotel_id');

        /**
         * Filter 
         */
        if (!$admin_id) {
            $admin = ORM::factory('admin')->find();
            $admin_id = $admin->id;
        }

        if ($this->request->post('assign')) {
            // Get admin
            $admin = ORM::factory('admin')
                    ->where('id', '=', $admin_id)
                    ->find();

            if ($hotel_id > 0) {
                // Get hotel
                $hotel = ORM::factory('hotel')
                        ->where('id', '=', $hotel_id)
                        ->find();

                // If hotel found
                if ($hotel->loaded()) {
                    // If admin not assigned
                    if (!$admin->has('hotels', $hotel)) {
                        // Assign hotel to admin
                        $admin->add('hotels', $hotel);
                    }
                }
            } else {
                // Get all hotels
                $hotels = ORM::factory('hotel')
                        ->find_all();

                foreach ($hotels as $hotel) {
                    // If admin not assigned
                    if (!$admin->has('hotels', $hotel)) {
                        // Assign hotel to admin
                        $admin->add('hotels', $hotel);
                    }
                }
            }

            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        }

        if ($this->request->post('unassign')) {
            $admin = ORM::factory('admin')
                    ->where('id', '=', $admin_id)
                    ->find();

            $hotel_ids = Arr::get($this->request->post(), 'ids', array());

            foreach ($hotel_ids as $hotel_id) {
                $hotel = ORM::factory('hotel')
                        ->where('id', '=', $hotel_id)
                        ->find();

                // If hotel assigned to admin
                if ($admin->has('hotels', $hotel)) {
                    // Unassign hotel from admin
                    $admin->remove('hotels', $hotel);
                }
            }

            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        }

        /**
         * Build variables for view
         */
        $admins = ORM::factory('admin')
                ->find_all();

        $admin_hotels = ORM::factory('hotel')
                ->select(
                        array('hotel_texts.name', 'name'), array('city_texts.name', 'city_name')
                )
                ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->where('admins_hotels.admin_id', '=', $admin_id)
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->order_by('hotel_texts.name')
                ->order_by('city_texts.name')
                ->find_all();

        $hotels = ORM::factory('hotel')
                ->select(
                        array('hotel_texts.name', 'name'), array('city_texts.name', 'city_name')
                )
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->order_by('hotel_texts.name')
                ->order_by('city_texts.name')
                ->find_all();

        $this->template->main = Kostache::factory('hotel/assign')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('admins', $admins)
                ->set('admin_hotels', $admin_hotels)
                ->set('hotels', $hotels);
    }

    public function action_delete() {
        // Get hotel id
        $hotel_id = $this->request->param('id');

        $hotel = ORM::factory('hotel')
                ->where('id', '=', $hotel_id)
                ->find();

        if (A2::instance()->allowed('hotel', 'delete')) {
            try {
                // Get hotel images directory
                $hotel_images_directory = Kohana::config('application.hotel_images_directory');

                // Get hotel directory
                $hotel_directory = $hotel_images_directory . DIRECTORY_SEPARATOR . $hotel->url_segment;

                // If directory exist
                if (is_dir($hotel_directory)) {
                    // Delete hotel image folders
                    File::remove_directory($hotel_directory);
                }

                // Delete hotel
                $hotel->delete();

                // Write log
                ORM::factory('log')
                        ->values(array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $hotel->id,
                            'username' => A1::instance()->get_user()->username,
                            'text' => 'Deleted hotel',
                            'created' => time(),
                        ))
                        ->create();

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
            } catch (Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
            }
        } else {
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
        }

        // Redirect back
        $this->request->redirect($this->request->referrer());
    }

    private function isChangeCurrencyHotel($hotel, $values) {
        CustomLog::factory()->add(3, 'DEBUG', 'Hotel Change Currency : Hotel ' . $hotel->url_segment . ' change currency from ' . $hotel->currency_id . ' to ' . $values['currency_id']);
        try {

            // --------------------------------Reset Price Room--------------------------------------
            //get all room by hotel_id
            $rooms = ORM::factory('room')
                    ->where('hotel_id', '=', $hotel->id)
                    ->find_all();

            //reset rooms price
            Model::factory('room')->resetCurrencyRooms($rooms);




            // --------------------------------Delete Surcharge--------------------------------------
            //get data surcharges
            $surcharges = ORM::factory('surcharge')
                    ->where('hotel_id', '=', $this->selected_hotel->id)
                    ->where('date', '>=', date('Y-m-d'))
                    ->find_all();

            //proceess reset surcharges
            Model::factory('surcharge')->resetCurrencySurcharges($surcharges);



            // -----------------------------Convert Benifit Campaign-------------------------
            //get data campaign have discount
            $campaigns = ORM::factory('campaign')
                    ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                    ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                    ->where('campaigns.is_default', '=', 0)
                    ->where('rooms.hotel_id', '=', $hotel->id)
                    ->where('discount_amount', '>', 0)
                    ->group_by('campaigns.id')
                    ->find_all();

            //process convert
            Model::factory('campaign')->ConvertCurrencyCampaignBenifit($campaigns, $hotel, $values);


            // --------------------------------Delete Item--------------------------------------
            //loop per room
            CustomLog::factory()->add(3, 'DEBUG', 'reset item start');
            foreach ($rooms as $rk => $room) {
                $last_item = ORM::factory('item')
                        ->where('room_id', '=', $room->id)
                        ->where('date', '>=', date('Y-m-d'))
                        ->order_by('date', 'desc')
                        ->limit(1)
                        ->find();

                if ($last_item->loaded()) {

                    //vacation_trigger
                    $vacation_triger = array(
                        'hotel_id' => $hotel->id,
                        'room_id' => $room->id,
                        'campaign_id' => NULL,
                        'item_id' => NULL,
                        'start_date' => date('Y-m-d'),
                        'end_date' => $last_item->date,
                        'type_trigger' => 6,
                        'status' => 0
                    );

                    Controller_Vacation_Trigger::insertData($vacation_triger);

                    // get data items
                    DB::delete('items')
                            ->where('room_id', '=', $room->id)
                            ->where('date', '>=', date('Y-m-d'))
                            ->order_by('date', 'desc')
                            ->execute();
                }
            }
            CustomLog::factory()->add(3, 'DEBUG', 'reset item finished');
            //add log success
            CustomLog::factory()->add(3, 'DEBUG', 'Success Reset Currency Hotel');
        } catch (Exception $e) {
            //add log error
            CustomLog::factory()->add(3, 'DEBUG', 'Failed Reset Currency Hotel');
            $error = $e->getTrace();
            CustomLog::factory()->add(3, 'DEBUG', print_r($error[0], true));
        }
    }

}
