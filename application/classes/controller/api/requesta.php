<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Requesta extends Controller {
    /*
     * List Frontend Model
     * Hotel :search_hotel, search_campaign, get_campaign_items
     * Room capacity :gerroom, checkroom, extrabed
     * Country/City  : getbysegment
     * Timezone  : timezone identifier
     * Setting  : get
     * Exchane  : To
     */

    public function before() {
        //Check Direct API
        if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
            $ip = getenv("REMOTE_ADDR");
        else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
            $ip = $_SERVER['REMOTE_ADDR'];
        else
            $ip = "unknown";

        parent::before();
    }

    public function errors($errors) {
        //Generate Error XML
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $response_node = $doc->createElement('Errors');

        foreach ($errors as $error) {
            $error_node = $response_node->appendChild($doc->createElement('Error', $error['Desc']));
            $error_node->setAttribute('Type', $error['Type']);
            $error_node->setAttribute('Code', $error['Code']);
        }
        $doc->appendChild($response_node);

        $response = $doc->saveXML();

        $this->output($response);
    }

    public static function selected_timezone($identifier) {
        // Set default language id using english
        $selected_timezone = ORM::factory('timezone')->get_timezone_by_identifier($identifier);
        $selected_timezone = $selected_timezone->identifier;

        return $selected_timezone;
    }

    public static function default_language() {
        // Set default language id using english
        $default_language_id = ORM::factory('language')
                        ->where('code', '=', 'en-us')
                        ->find()
                ->id;

        return $default_language_id;
    }

    public static function selected_currency($id) {
        // Set default currency
        $selected_currency = ORM::factory('currency')->get_currency_by_id($id);

        return $selected_currency;
    }

    public function time_format($hotel_query) {
        if (Arr::get($hotel_query, 'check_in')) {
            try {
                $check_in = Date::formatted_time(Arr::get($hotel_query, 'check_in'), 'Y-m-d');
            } catch (Exception $e) {
                $error = TRUE;
            }
        }

        if (Arr::get($hotel_query, 'check_out')) {
            try {
                $check_out = Date::formatted_time(Arr::get($hotel_query, 'check_out'), 'Y-m-d');
            } catch (Exception $e) {
                $error = TRUE;
            }
        }

        if (empty($error)) {
            $time = array(
                'check_in' => $check_in,
                'check_out' => $check_out,
            );
            $hotel_query = array_merge($hotel_query, $time);
        } else {
            $hotel_query = NULL;
        }
        return $hotel_query;
    }

    public function base_url($server) {
        $server = parse_url($server, PHP_URL_HOST);
        $server = explode('.', $server);

        if (in_array('staging', $server)) {
            $base_url = Kohana::config('application.hotel_url.staging');
        } else {
            $base_url = Kohana::config('application.hotel_url.live');
        }

        return $base_url;
    }

    public function get_xml($hotel_query) {
        //Server Name
        $server_name = $this->base_url(URL::base(true));
        // Factory exchange
        $exchange = Model::factory('exchange');

        //Generate XML
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $response_node = $doc->createElement('Hotels');

        $stay_night = Date::span(strtotime($hotel_query['check_out']), strtotime($hotel_query['check_in']), 'days');

        if (!empty($hotel_query['hotel_ids'])) {
            // Get hotel ids
            $hotel_ids = $hotel_query['hotel_ids'];

            $hotels = Model::factory('hotel')->search_hotels_limit($hotel_ids, $hotel_query);
        } else {
            $hotel_ids = NULL;
            //get data api_wego
            $api_wego = DB::select(
                            array('api_wego.hotel_id', 'api_wego_hotel_id'), array('api_wego.room_id', 'api_wego_room_id'), array('api_wego.campaign_id', 'api_wego_campaign_id'), array('api_wego.date', 'api_wego_date'), array('api_wego.currency_id', 'api_wego_currency_id'), array(DB::expr('MIN(api_wego.price)'), 'api_wego_price')
                    )
                    ->from('api_wego')
                    ->where_open()
                    ->where('api_wego.city_id', '=', $hotel_query['city_id'])
                    ->where_close()
                    ->group_by('hotel_id')
                    ->execute()
                    ->as_array()
            ;

            //change index array
            foreach ($api_wego as $key => $value) {
                $data[$value['api_wego_hotel_id']] = $value;
            }

            // Get hotel ids
            $wego_array = $api_wego;

            $hotel_ids = array_keys($data);

            //get hotels data
            $hotels = Model::factory('hotel')->search_hotels_limit($hotel_ids, $hotel_query);
        }

        // Get campaigns in the hotel_ids
        // if request by id
        if (!empty($wego_array)) {
            foreach ($wego_array as $key => $value) {
                $api_wego_data = $value;
                $campaigns[$key] = Model::factory('hotel')->search_campaigns($hotel_ids, $hotel_query, FALSE, $api_wego_data);
            }
            foreach ($campaigns as $key => $values) {
                foreach ($values as $value) {
                    $campaigns[$key] = $value;
                }
            }
        }
        //if request by city 
        else {
            // Get campaigns in the hotel_ids
            $campaigns = Model::factory('hotel')->search_campaigns($hotel_ids, $hotel_query, TRUE);
        }

        //Check Campaign exist
        if (count($campaigns)) {
            //request by id
            if (empty($wego_array)) {
                // Set cheapest campaign values
                $hotel_campaigns = array();

                // First value hotel
                $campaign_start = reset($campaigns);
                $hotel_campaigns[$campaign_start['hotel_id']] = $campaign_start;
                foreach ($campaigns as $value) {
                    // Get hotel id
                    $key = $value['hotel_id'];

                    // Set first appear hotel
                    if (!isset($hotel_campaigns[$key]['campaign_average_price'])) {
                        $hotel_campaigns[$key] = $value;
                    } else {
                        // Compare price
                        if ($hotel_campaigns[$key]['campaign_average_price'] > $value['campaign_average_price']) {
                            $hotel_campaigns[$key] = $value;
                        }
                    }
                }
            }
            //request by city 
            else {
                foreach ($campaigns as $key => $value) {
                    if ($value != NULL) {
                        $hotel_campaigns[$value['hotel_id']] = $value;
                    }
                }
            }
        }

        //Check Campaign exist
        if (count($campaigns)) {
            foreach ($hotel_campaigns as $key => $room) {
                //Generate XML Rooms
                $roomID = $room['room_id'];

                $promotion_exist = FALSE;

                //Determine room available
                $hotel_no_arrival = DB::select()
                        ->from('hotel_no_arrivals')
                        ->where('hotel_id', '=', $room['hotel_id'])
                        ->where('date', '=', $hotel_query['check_in'])
                        ->execute()
                        ->get('date');

                // Get no departure
                $hotel_no_departure = DB::select()
                        ->from('hotel_no_departures')
                        ->where('hotel_id', '=', $room['hotel_id'])
                        ->where('date', '=', $hotel_query['check_out'])
                        ->execute()
                        ->get('date');

                $Onrequest = 'TRUE';

                // Check campaign prices exist
                $room['campaign_average_price'] = isset($room['campaign_average_price']) ? $room['campaign_average_price'] : 0;

                // Determine if guest stay night more than minimum night
                $minimum_night_available = $room['max_minimum_night'];
                if ($room['campaign_average_price'] AND ! $hotel_no_arrival AND ! $hotel_no_departure AND $minimum_night_available) {
                    if (($stay_night >= $minimum_night_available) && ($stay_night >= $room['campaign_min_nights']) && ($hotel_query['number_of_rooms'] >= $room['campaign_min_rooms'])) {
                        $Onrequest = 'FALSE';
                    }

                    if (($stay_night < $minimum_night_available) && ($stay_night >= $room['campaign_min_nights']) && ($hotel_query['number_of_rooms'] >= $room['campaign_min_rooms'])) {
                        $promotion_exist = TRUE;
                    }
                }

                //Determine Promo
                $stay = '';
                $promo = '';

                if ($promotion_exist AND $room['campaign_default'] == 0) {
                    $stay = '( ';

                    if ($room['last_days'] > 0) {
                        $room['last_days_date'] = Date::formatted_time("+{$room['last_days']} days", __('M j, Y'), $room['hotel_timezone']);
                        $last_days = $room['last_days_date'];
                        $last_days = Date::formatted_time("$last_days", 'm-d-Y', $room['hotel_timezone']);

                        if ($hotel_query['check_in'] <= $last_days AND $hotel_query['check_out'] <= $last_days) {
                            $Onrequest = 'FALSE';
                        } elseif ($hotel_query['check_in'] < $last_days AND $hotel_query['check_out'] >= $last_days) {
                            $Onrequest = 'FALSE';
                        } else {
                            $Onrequest = 'TRUE';
                        }

                        $stay .= 'Book before ' . Date::formatted_time("+{$room['last_days']} days", __('M j, Y'), $room['hotel_timezone']) . ', ';
                    } elseif ($room['early_days'] > 0) {
                        $early_days = $room['early_days'] - 1;
                        $room['early_days_date'] = Date::formatted_time("+$early_days days", __('M j, Y'), $room['hotel_timezone']);

                        $early_days = $room['early_days_date'];
                        $early_days = Date::formatted_time("$early_days", 'm-d-Y', $room['hotel_timezone']);

                        if ($hotel_query['check_in'] <= $early_days AND $hotel_query['check_out'] <= $early_days) {
                            $Onrequest = 'TRUE';
                        } elseif ($hotel_query['check_in'] < $early_days AND $hotel_query['check_out'] >= $early_days) {
                            $Onrequest = 'FALSE';
                        } elseif ($hotel_query['check_in'] > $early_days AND $hotel_query['check_out'] >= $early_days) {
                            $Onrequest = 'FALSE';
                        }

                        $stay .= 'Book after ' . Date::formatted_time("+$early_days days", __('M j, Y'), $room['hotel_timezone']) . ', ';
                    } elseif ($room['rate'] > 0) {
                        $stay .= 'Discount ' . $room['rate'] . ' %, ';
                    } elseif ($room['amount'] > 0) {
                        $stay .= 'Discount ' . $room['rate'] . self::selected_currency($hotel_query['currency_code'])->code . ' ' . $room['amount'] . ', ';
                    }

                    $stay .= 'Stay at least ' . $room['campaign_min_nights'] . ' nights and book ' . $room['campaign_min_rooms'] . ' or more rooms)';
                }

                $promotion_exist = $promotion_exist ? $promo . ' ' . $stay : '';

                //Room name and description
                $Description = htmlspecialchars(utf8_encode($room['room_name'] . ' ' . $promotion_exist), ENT_QUOTES);

                //Determine Rate
                $RoomPublishPrice = $room['room_publish_price'] * ($exchange->to($room['currency_id'], $hotel_query['currency_code']));
                if (!empty($wego_array)) {

                    $RatePerNight = $data[$room['hotel_id']]['api_wego_price'] *
                            ($exchange->to($room['currency_id'], $hotel_query['currency_code']));
                } else {
                    $RatePerNight = $room['campaign_average_price'] != 0 ? ($room['campaign_average_price']) * ($exchange->to($room['currency_id'], $hotel_query['currency_code'])) : 0;
                }

                // On Request based campaign price
                $Onrequest = $room['campaign_average_price'] == 0 ? 'TRUE' : 'FALSE';

                // if($Onrequest == 'TRUE')
                // {
                //   continue;
                // }
                //Determine item by promo
                $RatePerNight = number_format(round($RatePerNight)) ? number_format(round($RatePerNight)) : '';

                $url_key = base64_encode($room['campaign_id'] . ',' . $room['room_id']
                        . ',' . $room['hotel_id'] . ',' . $hotel_query['check_in'] . ',' . $hotel_query['check_out']
                        . ',' . $hotel_query['number_of_rooms'] . ',' . $hotel_query['capacities']);

                if (!empty($wego_array)) {
                    $url = $server_name . $room['country_segment']
                            . '/' . $room['city_segment']
                            . '/' . $room['hotel_segment']
                            . '?key=' . $url_key;
                } else {
                    $url = $Onrequest == 'FALSE' ? $server_name . $room['country_segment'] . '/' . $room['city_segment'] . '/' . $room['hotel_segment'] . '?key=' . $url_key : '';
                }

                //Xml Element
                $hotel_node = $doc->createElement('Hotel');
                $hotel_node->appendChild($doc->createElement('ID', $room['hotel_id']));
                $hotel_node->appendChild($doc->createElement('Name', htmlspecialchars(utf8_encode($room['hotel_name']))));
                $hotel_node->appendChild($doc->createElement('Rating', $room['hotel_star']));
                $hotel_node->appendChild($doc->createElement('StreetAddress', htmlspecialchars(utf8_encode($room['hotel_address']))));
                $hotel_node->appendChild($doc->createElement('HotelURL', $server_name . '/' . $room['country_segment'] . '/' . $room['city_segment'] . '/' . $room['hotel_segment']));

                // Generate Hotel Availability
                $hotel_availability = $doc->createElement('HotelAvailability');
                $hotel_availability->appendChild($doc->createElement('CountryCode', 'ID'));
                $hotel_availability->appendChild($doc->createElement('City', $room['city_name']));
                $hotel_availability->appendChild($doc->createElement('StartDate', Arr::get($hotel_query, 'check_in')));
                $hotel_availability->appendChild($doc->createElement('EndDate', Arr::get($hotel_query, 'check_out')));
                $hotel_availability->appendChild($doc->createElement('NumGuests', Arr::get($hotel_query, 'capacities')));
                $hotel_availability->appendChild($doc->createElement('NumRooms', Arr::get($hotel_query, 'number_of_rooms')));
                $hotel_availability->appendChild($doc->createElement('CurrencyCode', Arr::get($hotel_query, 'currency')));
                $hotel_node->appendChild($hotel_availability);

                // Generate XML Amenities
                $amenities_node = $doc->createElement('Amenities');
                $hotel = $hotels[$key];

                if ($hotel->is_banquet_room_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Banquet Room'));
                }
                if ($hotel->is_bars_available == 1) {
                    $facilities [] = $amenities_node->appendChild($doc->createElement('Amenitiy', 'Bars'));
                }
                if ($hotel->is_business_center_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Business Center'));
                }
                if ($hotel->is_car_parking_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Car Parking'));
                }
                if ($hotel->is_casino_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Casino'));
                }
                if ($hotel->is_clinic_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Clinic'));
                }
                if ($hotel->is_club_lounge_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Club Lounge'));
                }
                if ($hotel->is_coffee_shop_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Coffe Shop'));
                }
                if ($hotel->is_departure_lounge_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Departure Lounge'));
                }
                if ($hotel->is_disabled_facilities_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Disabled Facilities'));
                }
                if ($hotel->is_elevator_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Elevator'));
                }
                if ($hotel->is_garden_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Garden'));
                }
                if ($hotel->is_gym_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Gym'));
                }
                if ($hotel->is_gift_shop_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Gift Shop'));
                }
                if ($hotel->is_golf_course_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Golf Course'));
                }
                if ($hotel->is_hair_salon_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Hair Salon'));
                }
                if ($hotel->is_jacuzzi_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Jacuzzi'));
                }
                if ($hotel->is_karaoke_room_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Karaoke Room'));
                }
                if ($hotel->is_kids_club_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Kids Club'));
                }
                if ($hotel->is_kids_pool_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Kids Pool'));
                }
                if ($hotel->is_library_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Library'));
                }
                if ($hotel->is_luggage_room_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Luggage Room'));
                }
                if ($hotel->is_meeting_room_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Meeting Room'));
                }
                if ($hotel->is_night_club_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Night Club'));
                }
                if ($hotel->is_private_beach_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Private Beach'));
                }
                if ($hotel->is_poolside_bar_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Poolside Bar'));
                }
                if ($hotel->is_restaurant_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Restaurant'));
                }
                if ($hotel->is_safety_box_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Safety Box'));
                }
                if ($hotel->is_sauna_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Sauna'));
                }
                if ($hotel->is_spa_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Spa'));
                }
                if ($hotel->is_squash_court_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Squash Court'));
                }
                if ($hotel->is_steam_room_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Steam Room'));
                }
                if ($hotel->is_swimming_pool_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Swimming Pool'));
                }
                if ($hotel->is_tennis_court_available == 1) {
                    $amenities_node->appendChild($doc->createElement('Amenitiy', 'Tennis Court'));
                }

                $hotel_node->appendChild($amenities_node);

                //Generate XML Photo
                $photolinks_node = $doc->createElement('PhotoLinks');

                $PhotoLink_small = $server_name . 'images/hotels/' . $hotel->hotel_segment . '/' . $hotel->hotel_small_basename . '';
                $PhotoLink = $server_name . 'images/hotels/' . $hotel->hotel_segment . '/' . $hotel->hotel_basename . '';

                $photolinks_node->appendChild($doc->createElement('PhotoLink', htmlspecialchars(utf8_encode($PhotoLink_small))));
                $photolinks_node->appendChild($doc->createElement('PhotoLink', htmlspecialchars(utf8_encode($PhotoLink))));

                $hotel_node->appendChild($photolinks_node);
                $hotel_node->appendChild($doc->createElement('City', $room['city_name']));
                $hotel_node->appendChild($doc->createElement('Country', $room['country_name']));
                $hotel_node->appendChild($doc->createElement('CurrencyCode', Arr::get($hotel_query, 'currency')));

                $rooms_node = $doc->createElement('Rooms');
                //Room XML
                $room_node = $doc->createElement('Room');
                $room_node->appendChild($doc->createElement('Onrequest', $Onrequest));
                $room_node->appendChild($doc->createElement('RoomID', $roomID));
                $room_node->appendChild($doc->createElement('Description', $Description));
                $room_node->appendChild($doc->createElement('RatePerNight', $RatePerNight));
                $room_node->appendChild($doc->createElement('ReservationURL', $url));
                $rooms_node->appendChild($room_node);

                $hotel_node->appendChild($rooms_node);
                $response_node->appendChild($hotel_node);
            }

            $doc->appendChild($response_node);
            $response = $doc->saveXML();

            // Log::instance()->add(Log::INFO, 'Wego output');
            // Log::instance()->add(Log::INFO, $response);

            $this->output($response);
        } else {
            $this->errors($errors = array('321' => array('Code' => 2, 'Type' => 425, 'Desc' => 'No data found')));
        }
    }

    public function output($response) {
        $this->response
                ->headers('Content-Type', 'text/xml')
                ->body($response);
    }

    public function process($hotel_query) {
        // try
        // {
        // reformat time
        $hotel_query = $this->time_format($hotel_query);

        //Check Country 
        if (strtolower($hotel_query['country']) != 'indonesia' && strtolower($hotel_query['country']) != 'id') {
            $this->errors($errors = array('321' => array('Code' => 2, 'Type' => 427, 'Desc' => 'Data Not Available')));
        }
        //Check Null request
        elseif ($hotel_query['city'] == Null) {
            $this->errors($errors = array('321' => array('Code' => 2, 'Type' => 321, 'Desc' => 'Required field missing')));
        } else {
            //Determine Country
            if (strtolower($hotel_query['country']) == 'id')
                $hotel_query['country'] = 'indonesia';

            //Determine Currency
            if ($hotel_query['currency'] == 'USD' || $hotel_query['currency'] == 'JPY' || $hotel_query['currency'] == 'IDR') {
                $hotel_query['currency_code'] = $hotel_query['currency'];
            } else {
                $hotel_query['currency_code'] = 'IDR';
            }

            //Determinr currency id
            $currency_code = ORM::factory('currency')
                            ->where('code', '=', $hotel_query['currency_code'])
                            ->find()->as_array();

            $hotel_query['currency_code'] = $currency_code['id'];

            $country = Model::factory('country')->get_by_segment($this->default_language(), $hotel_query['country']);

            $city = Model::factory('city')->get_by_segment($this->default_language(), $hotel_query['city']);

            switch (true) {
                case (empty($country)):
                    $this->errors($errors = array('321' => array('Code' => 2, 'Type' => 321, 'Desc' => 'Invalid Country')));
                    break;

                case (empty($city)):
                    $this->errors($errors = array('321' => array('Code' => 2, 'Type' => 321, 'Desc' => 'Invalid City')));
                    break;

                default:
                    $place = array(
                        'country_id' => $country->id,
                        'city_id' => $city->id,
                    );

                    $hotel_query = array_merge($place, $hotel_query);

                    $response = $this->get_xml($hotel_query);
                    break;
            }
        }
        // }
        // catch (Kohana_Exception $e)
        // {
        //   $this->errors($errors = array('321'=>array('Code' => 450,'Type'=>321,'Desc' => 'Unable to process')));
        // }
    }

    public function action_get() {
        $timezone = 'Asia/Kuala_Lumpur';

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $api_key = !empty($_GET['key']) ? $_GET['key'] : NULL;
            $source = !empty($_GET['source']) ? $_GET['source'] : '';

            //Check Request
            $hotel_query = array(
                'language_id' => $this->default_language(),
                'country' => !empty($_GET['country_code']) ? $_GET['country_code'] : 'id',
                'city' => !empty($_GET['city']) ? $_GET['city'] : '',
                'check_in' => !empty($_GET['check_in']) ? $_GET['check_in'] : Date::formatted_time('now', 'Y-m-d', self::selected_timezone($timezone)),
                'check_out' => !empty($_GET['check_out']) ? $_GET['check_out'] : Date::formatted_time("now +1 day", 'Y-m-d'),
                'number_of_rooms' => !empty($_GET['number_of_rooms']) ? $_GET['number_of_rooms'] : 1,
                'capacities' => !empty($_GET['number_of_guest']) ? $_GET['number_of_guest'] : 1,
                'currency' => !empty($_GET['currency_code']) ? $_GET['currency_code'] : 'IDR',
                'offset' => !empty($_GET['page']) ? $_GET['page'] : 1,
                'limit' => !empty($_GET['limit']) ? $_GET['limit'] : '20',
                'timezone' => $timezone,
                'now' => Date::formatted_time('now', 'Y-m-d', self::selected_timezone($timezone))
            );
        } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $api_key = !empty($_POST['key']) ? $_POST['key'] : NULL;
            $source = !empty($_POST['source']) ? $_POST['source'] : '';

            $hotel_query = array(
                'language_id' => $this->default_language(),
                'country' => !empty($_POST['country_code']) ? $_POST['country_code'] : 'id',
                'city' => !empty($_POST['city']) ? $_POST['city'] : '',
                'check_in' => !empty($_POST['check_in']) ? $_POST['check_in'] : Date::formatted_time('now', 'Y-m-d', self::selected_timezone($timezone)),
                'check_out' => !empty($_POST['check_out']) ? $_POST['check_out'] : Date::formatted_time("now +1 day", 'Y-m-d'),
                'number_of_rooms' => !empty($_POST['number_of_rooms']) ? $_POST['number_of_rooms'] : 1,
                'capacities' => !empty($_POST['number_of_guest']) ? $_POST['number_of_guest'] : 1,
                'currency' => !empty($_POST['currency_code']) ? $_POST['currency_code'] : 'IDR',
                'offset' => !empty($_POST['page']) ? $_POST['page'] : 1,
                'limit' => !empty($_POST['limit']) ? $_POST['limit'] : '20',
                'timezone' => $timezone,
                'now' => Date::formatted_time('now', 'Y-m-d', self::selected_timezone($timezone))
            );
        }

        // Check Auth API KEY or IP

        if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
            $ip = getenv("REMOTE_ADDR");
        else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
            $ip = $_SERVER['REMOTE_ADDR'];
        else
            $ip = "unknown";

        // Get admins
        $api_access = DB::select(
                        array('api_access.api_key', 'api_key'), array('api_access.ip', 'ip')
                )
                ->from('admins')
                ->join('roles')->on('roles.value', '=', 'admins.role')
                ->join('api_access')->on('api_access.admin_id', '=', 'admins.id')
                ->where('admins.role', '=', 'api')
                ->where_open()
                ->where('api_access.api_key', '=', $api_key)
                ->or_where('api_access.ip', '=', $ip)
                ->where_close()
                ->limit(1)
                ->execute();

        // Get Api_KEY
        $api_ip = NULL;
        foreach ($api_access as $key => $api) {
            if ($api['ip']) {
                $api_ip = $api['ip'];
            }
        }

        if (!count($api_access)) {
            $this->errors($errors = array('321' => array('Code' => 450, 'Type' => 321, 'Desc' => 'Auth Error')));
        } elseif (count($api_access) AND ! empty($api_ip) AND $ip != $api_ip) {
            $this->errors($errors = array('321' => array('Code' => 450, 'Type' => 321, 'Desc' => 'Auth Error')));
        } else {
            try {
                // Procces Function
                $hotel_query = $this->process($hotel_query);
            } catch (Kohana_Exception $e) {
                $this->errors($errors = array('321' => array('Code' => 450, 'Type' => 321, 'Desc' => 'Unable to process')));
            }
        }
    }

    public function action_xml() {
        // try
        // {
        $xml_data = trim(file_get_contents('php://input'));
        /*
          $xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/example.xml';
          $data = file_get_contents($xml_location);
          $xmlstr = trim( stripslashes( $data ) );
          $xml = new SimpleXMLElement($xmlstr);
         */

        // Log::instance()->add(Log::INFO, 'Wego Iput');
        // Log::instance()->add(Log::INFO, $xml_data);

        if (!$xml_data) {
            $this->errors($errors = array('448' => array('Code' => 3, 'Type' => 427, 'Desc' => 'XML Empty')));
        } else {
            // Check Auth API KEY or IP

            if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
                $ip = getenv("HTTP_CLIENT_IP");
            else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
                $ip = getenv("HTTP_X_FORWARDED_FOR");
            else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
                $ip = getenv("REMOTE_ADDR");
            else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
                $ip = "unknown";

            $ip = str_replace(' ', '', $ip);
            $ip = explode(',', $ip);

            // Check IP
            if (in_array("175.41.150.124", $ip) || in_array("122.248.251.172", $ip) || in_array("122.248.235.26", $ip) || in_array("202.138.247.73", $ip) || in_array("192.168.20.63", $ip) || in_array("119.81.12.85", $ip) || in_array("127.0.0.1", $ip)
            ) {
                $xml = simplexml_load_string($xml_data);
                $doc = (array) $xml;

                $timezone = 'Asia/Kuala_Lumpur';

                $hotel_query = array(
                    'language_id' => $this->default_language(),
                    'country' => !empty($doc['CountryCode']) ? $doc['CountryCode'] : 'id',
                    'city' => !empty($doc['City']) ? $doc['City'] : '',
                    'check_in' => !empty($doc['StartDate']) ? $doc['StartDate'] : Date::formatted_time('now', 'Y-m-d', self::selected_timezone($timezone)),
                    'check_out' => !empty($doc['EndDate']) ? $doc['EndDate'] : Date::formatted_time("now +1 day", 'Y-m-d'),
                    'number_of_rooms' => !empty($doc['NumRooms']) ? $doc['NumRooms'] : 1,
                    'capacities' => !empty($doc['NumGuests']) ? $doc['NumGuests'] : 1,
                    'currency' => !empty($doc['CurrencyCode']) ? $doc['CurrencyCode'] : 'IDR',
                    'offset' => !empty($doc['Page']) ? $doc['Page'] : 0,
                    'limit' => !empty($doc['Limit']) ? $doc['Limit'] : '20',
                    'timezone' => $timezone,
                    'now' => Date::formatted_time('now', 'Y-m-d', self::selected_timezone($timezone)),
                );

                // For Hotel ID request
                if (!empty($doc['Hotels'])) {
                    $hotel_id = (array) $doc['Hotels'];

                    if (count($hotel_id['ID']) > 1) {
                        $hotel_query['hotel_ids'] = $hotel_id['ID'];
                    } else {
                        $hotel_query['hotel_ids'][0] = $hotel_id['ID'];
                    }

                    // Add Default City to pass city balidation
                    $hotel_query['city'] = 'Bali';
                }

                // Procces Function
                $hotel_query = $this->process($hotel_query);
            } else {
                $this->errors($errors = array('4' => array('Code' => 497, 'Type' => 4, 'Desc' => 'Authorization errors')));
            }
        }
        // }
        // catch (Kohana_Exception $e)
        // {
        //   $this->errors($errors = array('321'=>array('Code' => 450,'Type'=>321,'Desc' => 'Unable to process')));
        // }
    }

}
