<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_api_Rategainreservation extends Controller {

    public static function HotelRes_rategain($booking_exist) {
        $result = FALSE;

        /// Hoterip campaign
        $exchange = Model::factory('exchange');
        // service_tax_rate
        $service_tax_rate = (($booking_exist['hotel']->service_charge_rate + $booking_exist['hotel']->tax_rate) + 100) / 100;
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('s:Envelope'));
            $envelope->setAttribute('xmlns:s', 'http://schemas.xmlsoap.org/soap/envelope/');
            $envelope->setAttribute('xmlns:a', 'http://schemas.xmlsoap.org/ws/2004/08/addressing');
            $envelope->setAttribute('xmlns:u', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd');

            $Header = $envelope->appendChild($doc->createElement('s:Header'));
            $Action = $Header->appendChild($doc->createElement('a:Action', 'http://cgbridge.rategain.com/2011A/ReservationService/HotelResNotif'));
            $Action->setAttribute('s:mustUnderstand', 1);
            $MessageID = $Header->appendChild($doc->createElement('a:MessageID', 'urn:uuid:8deae80c-5d3d-4269-9f0a-6d25a217ef1c'));
            $ReplyTo = $Header->appendChild($doc->createElement('a:ReplyTo'));
            $Address = $ReplyTo->appendChild($doc->createElement('a:Address', 'http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous'));
            $To = $Header->appendChild($doc->createElement('a:To', 'https://t1.cgbeta.rategain.com/OTAPushResv/Reservation.svc'));
            $To->setAttribute('s:mustUnderstand', 1);
            $Security = $Header->appendChild($doc->createElement('o:Security'));
            $Security->setAttribute('s:mustUnderstand', 1);
            $Security->setAttribute('xmlns:o', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');
            $UsernameToken = $Security->appendChild($doc->createElement('o:UsernameToken'));
            $UsernameToken->setAttribute('u:Id', 'uuid-c698aabc-c710-42f9-881c-0dfe3fdcaa4f-2');
            $Username = $UsernameToken->appendChild($doc->createElement('o:Username', Kohana::config('api.rategain.username')));
            $Password = $UsernameToken->appendChild($doc->createElement('o:Password', Kohana::config('api.rategain.password')));
            $Password->setAttribute('o:Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');

            $Body = $envelope->appendChild($doc->createElement('s:Body'));
            $Body->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
            $Body->setAttribute('xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');

            $response_node = $Body->appendChild($doc->createElement('OTA_HotelResNotifRQ'));
            $response_node->setAttribute('TimeStamp', date('c'));
            $response_node->setAttribute('Target', 'Production');
            $response_node->setAttribute('Version', '2.01');
            $response_node->setAttribute('ResStatus', 'Commit');
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');

            $pos = $response_node->appendChild($doc->createElement('POS'));
            $pos->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $source = $pos->appendChild($doc->createElement('Source'));
            $RequestorID = $source->appendChild($doc->createElement('RequestorID'));
            $RequestorID->setAttribute('Type', 14);
            $RequestorID->setAttribute('ID', Kohana::config('api.rategain.rategain_code'));
            $BookingChannel = $source->appendChild($doc->createElement('BookingChannel'));
            $BookingChannel->setAttribute('Primary', 'true');
            $BookingChannel->setAttribute('Type', 7);
            $CompanyName = $BookingChannel->appendChild($doc->createElement('CompanyName', 'Hoterip'));
            $CompanyName->setAttribute('Code', Kohana::config('api.rategain.code'));

            $HotelReservations = $response_node->appendChild($doc->createElement('HotelReservations'));
            $HotelReservations->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $HotelReservation = $HotelReservations->appendChild($doc->createElement('HotelReservation'));
            $HotelReservation->setAttribute('CreateDateTime', date('c'));
            $HotelReservation->setAttribute('ResStatus', 'Commit');

            $RoomStays = $HotelReservation->appendChild($doc->createElement('RoomStays'));

            foreach ($booking_exist['items'] as $num => $items) {
                $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));
                $RoomStay->setAttribute('PromotionVendorCode', null);
                $RoomStay->setAttribute('WarningRPH', null);

                $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                $RoomType->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomType->setAttribute('NumberOfUnits', $booking_exist['number_of_rooms']);
                $RoomType->setAttribute('PromotionVendorCode', null);
                $RoomType->setAttribute('BedTypeCode', null);
                $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
                $RoomDescription->setAttribute('Name', $booking_exist['room_texts'][1]->name);

                // Rateplan descriptions
                $campaign = (array) unserialize($booking_exist['campaign']);
                $campaign['campaign_default'] = $campaign['is_default'];
                $campaign['rate'] = $campaign['discount_rate'];
                $campaign['amount'] = $campaign['discount_amount'];
                $campaign['currency_code'] = $booking_exist['data']['hotel_currency']->code;
                $campaign['free_night'] = $campaign['minimum_number_of_nights'];
                $campaign['last_days'] = $campaign['within_days_of_arrival'];
                $campaign['hotel_timezone'] = $booking_exist['hotel']->timezone;
                $campaign['check_in'] = empty($campaign['check_in_date']) ? '0000-00-00' : $campaign['check_in_date'];
                $campaign['check_out'] = empty($campaign['check_out_date']) ? '0000-00-00' : $campaign['check_out_date'];
                $campaign['early_days'] = $campaign['days_in_advance'];
                $campaign['min_nights'] = $campaign['minimum_number_of_nights'];
                $campaign['min_rooms'] = $campaign['minimum_number_of_rooms'];

                $benefit = Controller_Siteminder_Static::promotion_descriptions($campaign);

                $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription', $benefit));
                $RatePlan->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $MealsIncluded = $RatePlan->appendChild($doc->createElement('MealsIncluded'));
                if ($booking_exist['room']->is_breakfast_included) {
                    $MealsIncluded->setAttribute('Breakfast', 'true');
                    $MealsIncluded->setAttribute('MealPlanCode', 19);
                } else {
                    $MealsIncluded->setAttribute('Breakfast', 'false');
                    $MealsIncluded->setAttribute('MealPlanCode', null);
                }

                $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));
                $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                $RoomRate->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $RoomRate->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomRate->setAttribute('NumberOfUnits', $booking_exist['number_of_rooms']);
                $RoomRate->setAttribute('PromotionVendorCode', null);

                // Items
                $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                $AmountAfterTax_total = 0;
                $AmountBeforeTax_total = 0;
                $price_after_tax = 0;
                $price_before_tax = 0;

                foreach ($items['items'] as $ord => $item) {
                    // if($item['stock'] > 0 && !$item['is_blackout'] && !$item['is_campaign_blackout'])
                    // {
                    // Add one day
                    $date = $item['date'];
                    $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');

                    $price_before_tax = ($item['price'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    $price_after_tax = $item['price'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Extrabed Price
                    $extrabed_price = $item['extrabed_item_price'] != 0.1 ? $item['extrabed_item_price'] : $booking_exist['data']['extrabed_price'];
                    $extrabed_price = ($extrabed_price * $items['number_of_extrabeds']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Surcharge Price
                    $surcharge_price = ($booking_exist['data']['total_surcharge']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    // Total Price Final Calculations
                    $price_before_tax = ($price_before_tax + $extrabed_price + $surcharge_price) * $booking_exist['number_of_rooms'];
                    $price_after_tax = ($price_after_tax + $extrabed_price + $surcharge_price) * $booking_exist['number_of_rooms'];

                    $Rate = $Rates->appendChild($doc->createElement('Rate'));
                    $Rate->setAttribute('EffectiveDate', $date);
                    $Rate->setAttribute('ExpireDate', $add_one_day);

                    $Base = $Rate->appendChild($doc->createElement('Base'));
                    $Base->setAttribute('AmountAfterTax', round($price_after_tax, 2));
                    $Base->setAttribute('AmountBeforeTax', round($price_before_tax, 2));
                    $Base->setAttribute('CurrencyCode', 'USD');
                    // Count for total prices
                    $AmountAfterTax_total += $price_after_tax;
                    $AmountBeforeTax_total += $price_before_tax;
                    // }
                }

                $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                $GuestCount->setAttribute('AgeQualifyingCode', 10);
                $GuestCount->setAttribute('Count', $booking_exist['data']['number_of_adult']);

                $booking_exist['number_of_child'] = empty($booking_exist['number_of_child']) ? 0 : $booking_exist['number_of_child'];
                if ($booking_exist['number_of_child']) {
                    $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                    $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                    $GuestCount->setAttribute('AgeQualifyingCode', 8);
                    $GuestCount->setAttribute('Count', $booking_exist['data']['number_of_child']);
                }

                // Night span
                $night_span = (strtotime($booking_exist['data']['check_out']) - strtotime($booking_exist['data']['check_in'])) / (60 * 60 * 24);

                $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
                $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
                $TimeSpan->setAttribute('Duration', 'P' . $night_span . 'D');

                // $AmountAfterTax = $booking_exist['data']['invoice_transaction']['amount'] * Model::factory('exchange')->to($booking_exist['data']['transaction_currency']->id, 1);
                // $AmountBeforeTax = ($booking_exist['data']['invoice_transaction']['amount'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['transaction_currency']->id, 1);
                $tax = $AmountAfterTax_total - $AmountBeforeTax_total;

                $Total = $RoomStay->appendChild($doc->createElement('Total'));
                $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total, 2));
                $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total, 2));
                $Total->setAttribute('CurrencyCode', 'USD');
                $Taxes = $Total->appendChild($doc->createElement('Taxes'));
                $Tax = $Taxes->appendChild($doc->createElement('Tax'));
                $Tax->setAttribute('Amount', round($tax, 2));
                $Tax->setAttribute('CurrencyCode', 'USD');

                $BasicPropertyInfo = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                $BasicPropertyInfo->setAttribute('HotelCode', $booking_exist['data']['hotel_id']);
                $BasicPropertyInfo->setAttribute('HotelName', $booking_exist['data']['hotel_name']);

                $Comments = $RoomStay->appendChild($doc->createElement('Comments'));
                $Comment = $Comments->appendChild($doc->createElement('Comment'));
                $Comment->setAttribute('GuestViewable', 'true');
                $Text = $Comment->appendChild($doc->createElement('Text', htmlentities($booking_exist['note'])));

                break;
            }

            $ResGuests = $HotelReservation->appendChild($doc->createElement('ResGuests'));
            $ResGuest = $ResGuests->appendChild($doc->createElement('ResGuest'));
            $ResGuest->setAttribute('AgeQualifyingCode', 10);
            $ResGuest->setAttribute('PrimaryIndicator', 'true');
            $Profiles = $ResGuest->appendChild($doc->createElement('Profiles'));
            $ProfileInfo = $Profiles->appendChild($doc->createElement('ProfileInfo'));
            $Profile = $ProfileInfo->appendChild($doc->createElement('Profile'));
            $Profile->setAttribute('ProfileType', 1);
            $Profile->setAttribute('StatusCode', "");
            $Customer = $Profile->appendChild($doc->createElement('Customer'));
            $PersonName = $Customer->appendChild($doc->createElement('PersonName'));
            $GivenName = $PersonName->appendChild($doc->createElement('GivenName', $booking_exist['first_name']));
            $Surname = $PersonName->appendChild($doc->createElement('Surname', $booking_exist['last_name']));
            $Telephone = $Customer->appendChild($doc->createElement('Telephone'));
            $Email = $Customer->appendChild($doc->createElement('Email', $booking_exist['email']));
            $Email->setAttribute('EmailType', 1);
            $Email->setAttribute('DefaultInd', 'true');
            $Address = $Customer->appendChild($doc->createElement('Address'));
            $Address->setAttribute('type', 1);
            $addressline = $Address->appendChild($doc->createElement('Addressline'));
            $cityname = $Address->appendChild($doc->createElement('CityName'));
            $postalcode = $Address->appendChild($doc->createElement('PostalCode'));
            $stateprov = $Address->appendChild($doc->createElement('StateProv'));
            $country = $Address->appendChild($doc->createElement('CountryName'));

            $ResGlobalInfo = $HotelReservation->appendChild($doc->createElement('ResGlobalInfo'));

            $TimeSpan = $ResGlobalInfo->appendChild($doc->createElement('TimeSpan'));
            $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
            $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
            $TimeSpan->setAttribute('Duration', 'P' . $night_span . 'D');

            // Start Cancellation function
            /*
             * Put this cancellation functions under TimeSpan node
             */

            $cancellation = DB::select(
                            'cancellations.*', array('rule_1.name', 'rule_1_name'), array('rule_2.name', 'rule_2_name'), array('rule_3.name', 'rule_3_name')
                    )
                    ->from('campaigns_cancellations')
                    ->join('cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
                    ->join(array('cancellation_rules', 'rule_1'))->on('rule_1.id', '=', 'cancellations.level_1_cancellation_rule_id')
                    ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'cancellations.level_2_cancellation_rule_id')
                    ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'cancellations.level_3_cancellation_rule_id')
                    ->where('campaigns_cancellations.campaign_id', '=', $booking_exist['campaign_id'])
                    ->execute()
                    ->current();

            if (!$cancellation) {
                // Search room default campaign
                $default_campaign_id = Model::factory('campaign')->get_default_campaign_id($booking_exist['room_id']);

                // Get cancellations
                $cancellation = Model::factory('cancellation')->get_cancellation($default_campaign_id, $booking_exist['check_in_date'], $booking_exist['check_out_date']);
            }

            $CancelPenalties = $ResGlobalInfo->appendChild($doc->createElement('CancelPenalties'));

            $CancelPenalty = $CancelPenalties->appendChild($doc->createElement('CancelPenalty'));
            $PenaltyDescription = $CancelPenalty->appendChild($doc->createElement('PenaltyDescription'));
            $PenaltyDescription->setAttribute('Name', $cancellation['rule_1_name']);

            if (!empty($cancellation['rule_2_name'])) {
                $CancelPenalty = $CancelPenalties->appendChild($doc->createElement('CancelPenalty'));
                $PenaltyDescription = $CancelPenalty->appendChild($doc->createElement('PenaltyDescription'));
                $PenaltyDescription->setAttribute('Name', $cancellation['rule_2_name']);
            }

            if (!empty($cancellation['rule_3_name'])) {
                $CancelPenalty = $CancelPenalties->appendChild($doc->createElement('CancelPenalty'));
                $PenaltyDescription = $CancelPenalty->appendChild($doc->createElement('PenaltyDescription'));
                $PenaltyDescription->setAttribute('Name', $cancellation['rule_3_name']);
            }

            // End Cancellation function

            $SpecialRequests = $ResGlobalInfo->appendChild($doc->createElement('SpecialRequests'));

            if ($booking_exist['is_non_smoking_room_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $SpecialRequest->setAttribute('RequestCode', 198);
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Nonsmoking Room'));
            }
            if ($booking_exist['is_early_check_in_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $SpecialRequest->setAttribute('RequestCode', 196);
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Early check in request'));
            }
            if ($booking_exist['is_high_floor_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'High Floor Request'));
            }
            if ($booking_exist['is_large_bed_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Late Check In'));
            }
            if ($booking_exist['is_twin_beds_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Twin Bed Request'));
            }
            if ($booking_exist['is_airport_transfer_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Airport Transfer request'));
            }

            $Total = $ResGlobalInfo->appendChild($doc->createElement('Total'));
            $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total, 2));
            $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total, 2));
            $Total->setAttribute('CurrencyCode', 'USD');
            $Taxes = $Total->appendChild($doc->createElement('Taxes'));
            $Tax = $Taxes->appendChild($doc->createElement('Tax'));
            $Tax->setAttribute('Amount', round($tax, 2));
            $Tax->setAttribute('CurrencyCode', 'USD');

            $HotelReservationIDs = $ResGlobalInfo->appendChild($doc->createElement('HotelReservationIDs'));
            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $booking_exist['booking_id']);

            $doc->appendChild($envelope);

            $request = $doc->saveXML();

            CustomLog::factory()->add(4, 'DEBUG', $request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, Kohana::config('api.rategain.rategain_reservation_url'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Content-length: " . strlen($request)));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $response = curl_exec($ch);
            // Close the handle
            curl_close($ch);

            /*
             * XML process
             */
            try {
                $xml = new SimpleXMLElement($response);
                $body = $xml->children('s', true)->Body->children();
                $OTA = $body->OTA_HotelResNotifRS;

                // Check response Success/ Errors
                if (isset($OTA->Success)) {
                    DB::delete('api_reservation_queues')
                            ->where('api', '=', 1)
                            ->where('request_type', '=', 1)
                            ->where('booking_id', '=', $booking_exist['booking_id'])
                            ->execute();

                    CustomLog::factory()->add(4, 'DEBUG', 'Rategain reservation notification Success');
                    CustomLog::factory()->add(4, 'DEBUG', $response);
                } else {
                    $booking_exist['retries'] = $booking_exist['retries'] + 1;

                    DB::update('api_reservation_queues')
                            ->set(
                                    array('retries' => $booking_exist['retries'])
                            )
                            ->where('booking_id', '=', $booking_exist['booking_id'])
                            ->where('request_type', '=', $booking_exist['request_type'])
                            ->where('api', '=', $booking_exist['api'])
                            ->execute();

                    CustomLog::factory()->add(4, 'DEBUG', 'Rategain reservation notification Failed');
                    CustomLog::factory()->add(4, 'DEBUG', $response);
                }
            } catch (Exception $e) {
                Database::instance()->rollback();
                $booking_exist['retries'] = $booking_exist['retries'] + 1;

                DB::update('api_reservation_queues')
                        ->set(
                                array('retries' => $booking_exist['retries'])
                        )
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();

                CustomLog::factory()->add(4, 'DEBUG', 'Rategain reservation notification Failed');
                CustomLog::factory()->add(4, 'DEBUG', $response);
            }
        } catch (Kohana_Exception $e) {

            $booking_exist['retries'] = $booking_exist['retries'] + 1;

            DB::update('api_reservation_queues')
                    ->set(
                            array('retries' => $booking_exist['retries'])
                    )
                    ->where('booking_id', '=', $booking_exist['booking_id'])
                    ->where('request_type', '=', $booking_exist['request_type'])
                    ->where('api', '=', $booking_exist['api'])
                    ->execute();
            CustomLog::factory()->add(4, 'DEBUG', 'Rategain reservation notification failed');

            $values = $this->request->post();
            foreach ($values as $key => $value) {
                CustomLog::factory()->add(4, 'DEBUG', "$key: $value");
            }
        }
    }

    public static function HotelRes_rategain_cancel($booking_exist) {
        $success = TRUE;

        /// Hoterip campaign
        $exchange = Model::factory('exchange');
        // service_tax_rate
        $service_tax_rate = (($booking_exist['hotel']->service_charge_rate + $booking_exist['hotel']->tax_rate) + 100) / 100;
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('s:Envelope'));
            $envelope->setAttribute('xmlns:s', 'http://schemas.xmlsoap.org/soap/envelope/');
            $envelope->setAttribute('xmlns:a', 'http://schemas.xmlsoap.org/ws/2004/08/addressing');
            $envelope->setAttribute('xmlns:u', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd');

            $Header = $envelope->appendChild($doc->createElement('s:Header'));
            $Action = $Header->appendChild($doc->createElement('a:Action', 'http://cgbridge.rategain.com/2011A/ReservationService/HotelResNotif'));
            $Action->setAttribute('s:mustUnderstand', 1);
            $MessageID = $Header->appendChild($doc->createElement('a:MessageID', 'urn:uuid:8deae80c-5d3d-4269-9f0a-6d25a217ef1c'));
            $ReplyTo = $Header->appendChild($doc->createElement('a:ReplyTo'));
            $Address = $ReplyTo->appendChild($doc->createElement('a:Address', 'http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous'));
            $To = $Header->appendChild($doc->createElement('a:To', 'https://t1.cgbeta.rategain.com/OTAPushResv/Reservation.svc'));
            $To->setAttribute('s:mustUnderstand', 1);
            $Security = $Header->appendChild($doc->createElement('o:Security'));
            $Security->setAttribute('s:mustUnderstand', 1);
            $Security->setAttribute('xmlns:o', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');
            $UsernameToken = $Security->appendChild($doc->createElement('o:UsernameToken'));
            $UsernameToken->setAttribute('u:Id', 'uuid-c698aabc-c710-42f9-881c-0dfe3fdcaa4f-2');
            $Username = $UsernameToken->appendChild($doc->createElement('o:Username', Kohana::config('api.rategain.username')));
            $Password = $UsernameToken->appendChild($doc->createElement('o:Password', Kohana::config('api.rategain.password')));
            $Password->setAttribute('o:Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');

            $Body = $envelope->appendChild($doc->createElement('s:Body'));
            $Body->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
            $Body->setAttribute('xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');

            $response_node = $Body->appendChild($doc->createElement('OTA_HotelResNotifRQ'));
            $response_node->setAttribute('TimeStamp', date('c'));
            $response_node->setAttribute('Target', 'Production');
            $response_node->setAttribute('Version', '2.01');
            $response_node->setAttribute('ResStatus', 'Cancel');
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');

            $pos = $response_node->appendChild($doc->createElement('POS'));
            $pos->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $source = $pos->appendChild($doc->createElement('Source'));
            $RequestorID = $source->appendChild($doc->createElement('RequestorID'));
            $RequestorID->setAttribute('Type', 14);
            $RequestorID->setAttribute('ID', Kohana::config('api.rategain.rategain_code'));
            $BookingChannel = $source->appendChild($doc->createElement('BookingChannel'));
            $BookingChannel->setAttribute('Primary', 'true');
            $BookingChannel->setAttribute('Type', 7);
            $CompanyName = $BookingChannel->appendChild($doc->createElement('CompanyName', 'Hoterip'));
            $CompanyName->setAttribute('Code', Kohana::config('api.rategain.code'));

            $HotelReservations = $response_node->appendChild($doc->createElement('HotelReservations'));
            $HotelReservations->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $HotelReservation = $HotelReservations->appendChild($doc->createElement('HotelReservation'));
            $HotelReservation->setAttribute('CreateDateTime', gmdate('c', $booking_exist['created']));
            $HotelReservation->setAttribute('LastModifyDateTime', date('c'));
            $HotelReservation->setAttribute('ResStatus', 'Cancel');


            $RoomStays = $HotelReservation->appendChild($doc->createElement('RoomStays'));
            foreach ($booking_exist['items'] as $num => $items) {
                $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));
                $RoomStay->setAttribute('PromotionVendorCode', null);
                $RoomStay->setAttribute('WarningRPH', null);

                $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                $RoomType->setAttribute('PromotionVendorCode', null);
                $RoomType->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomType->setAttribute('NumberOfUnits', $booking_exist['number_of_rooms']);
                $RoomType->setAttribute('BedTypeCode', null);
                $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
                $RoomDescription->setAttribute('Name', $booking_exist['room_texts'][1]->name);

                // Rateplan descriptions
                $campaign = (array) unserialize($booking_exist['campaign']);
                $campaign['campaign_default'] = $campaign['is_default'];
                $campaign['rate'] = $campaign['discount_rate'];
                $campaign['amount'] = $campaign['discount_amount'];
                $campaign['currency_code'] = $booking_exist['data']['hotel_currency']->code;
                $campaign['free_night'] = $campaign['minimum_number_of_nights'];
                $campaign['last_days'] = $campaign['within_days_of_arrival'];
                $campaign['hotel_timezone'] = $booking_exist['hotel']->timezone;
                $campaign['check_in'] = empty($campaign['check_in_date']) ? '0000-00-00' : $campaign['check_in_date'];
                $campaign['check_out'] = empty($campaign['check_out_date']) ? '0000-00-00' : $campaign['check_out_date'];
                $campaign['early_days'] = $campaign['days_in_advance'];
                $campaign['min_nights'] = $campaign['minimum_number_of_nights'];
                $campaign['min_rooms'] = $campaign['minimum_number_of_rooms'];

                $benefit = Controller_Siteminder_Static::promotion_descriptions($campaign);

                $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription', $benefit));
                $RatePlan->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $MealsIncluded = $RatePlan->appendChild($doc->createElement('MealsIncluded'));
                if ($booking_exist['room']->is_breakfast_included) {
                    $MealsIncluded->setAttribute('Breakfast', 'true');
                    $MealsIncluded->setAttribute('MealPlanCode', 19);
                } else {
                    $MealsIncluded->setAttribute('Breakfast', 'false');
                    $MealsIncluded->setAttribute('MealPlanCode', null);
                }

                $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));
                $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                $RoomRate->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $RoomRate->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomRate->setAttribute('NumberOfUnits', $booking_exist['number_of_rooms']);
                $RoomRate->setAttribute('PromotionVendorCode', null);

                // Items
                $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                $AmountAfterTax_total = 0;
                $AmountBeforeTax_total = 0;
                $price_after_tax = 0;
                $price_before_tax = 0;

                foreach ($items['items'] as $ord => $item) {
                    // if($item['stock'] > 0 && !$item['is_blackout'] && !$item['is_campaign_blackout'])
                    // {
                    // Add one day
                    $date = $item['date'];
                    $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');

                    // item price amount hack.
                    $item['price'] = $booking_exist['data']['invoice_amount'] / count($items['items']);
                    $price_before_tax = ($item['price'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    $price_after_tax = $item['price'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Extrabed Price
                    $extrabed_price = $item['extrabed_item_price'] != 0.1 ? $item['extrabed_item_price'] : $booking_exist['data']['extrabed_price'];
                    $extrabed_price = ($extrabed_price * $items['number_of_extrabeds']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Surcharge Price
                    $surcharge_price = ($booking_exist['data']['total_surcharge']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    // Total Price Final Calculations
                    $price_before_tax = ($price_before_tax + $extrabed_price + $surcharge_price) * $booking_exist['number_of_rooms'];
                    $price_after_tax = ($price_after_tax + $extrabed_price + $surcharge_price) * $booking_exist['number_of_rooms'];

                    $Rate = $Rates->appendChild($doc->createElement('Rate'));
                    $Rate->setAttribute('EffectiveDate', $date);
                    $Rate->setAttribute('ExpireDate', $add_one_day);

                    $Base = $Rate->appendChild($doc->createElement('Base'));
                    $Base->setAttribute('AmountAfterTax', round($price_after_tax, 2));
                    $Base->setAttribute('AmountBeforeTax', round($price_before_tax, 2));
                    $Base->setAttribute('CurrencyCode', 'USD');
                    // Count for total prices
                    $AmountAfterTax_total += $price_after_tax;
                    $AmountBeforeTax_total += $price_before_tax;
                    // }
                }

                $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                $GuestCount->setAttribute('AgeQualifyingCode', 10);
                $GuestCount->setAttribute('Count', $booking_exist['data']['number_of_adult']);

                $booking_exist['number_of_child'] = empty($booking_exist['number_of_child']) ? 0 : $booking_exist['number_of_child'];
                if ($booking_exist['number_of_child']) {
                    $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                    $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                    $GuestCount->setAttribute('AgeQualifyingCode', 8);
                    $GuestCount->setAttribute('Count', $booking_exist['data']['number_of_child']);
                }

                // Night span
                $night_span = (strtotime($booking_exist['data']['check_out']) - strtotime($booking_exist['data']['check_in'])) / (60 * 60 * 24);

                $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
                $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
                $TimeSpan->setAttribute('Duration', 'P' . $night_span . 'D');

                // $AmountAfterTax = $booking_exist['data']['invoice_transaction']['amount'] * Model::factory('exchange')->to($booking_exist['data']['transaction_currency']->id, 1);
                // $AmountBeforeTax = ($booking_exist['data']['invoice_transaction']['amount'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['transaction_currency']->id, 1);
                $tax = $AmountAfterTax_total - $AmountBeforeTax_total;

                $Total = $RoomStay->appendChild($doc->createElement('Total'));
                $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total, 2));
                $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total, 2));
                $Total->setAttribute('CurrencyCode', 'USD');
                $Taxes = $Total->appendChild($doc->createElement('Taxes'));
                $Tax = $Taxes->appendChild($doc->createElement('Tax'));
                $Tax->setAttribute('Amount', round($tax, 2));
                $Tax->setAttribute('CurrencyCode', 'USD');

                $BasicPropertyInfo = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                $BasicPropertyInfo->setAttribute('HotelCode', $booking_exist['data']['hotel_id']);
                $BasicPropertyInfo->setAttribute('HotelName', $booking_exist['data']['hotel_name']);

                $Comments = $RoomStay->appendChild($doc->createElement('Comments'));
                $Comment = $Comments->appendChild($doc->createElement('Comment'));
                $Comment->setAttribute('GuestViewable', 'true');
                $Text = $Comment->appendChild($doc->createElement('Text', htmlentities($booking_exist['note'])));

                break;
            }

            $ResGuests = $HotelReservation->appendChild($doc->createElement('ResGuests'));
            $ResGuest = $ResGuests->appendChild($doc->createElement('ResGuest'));
            $ResGuest->setAttribute('AgeQualifyingCode', 10);
            $ResGuest->setAttribute('PrimaryIndicator', 'true');
            $Profiles = $ResGuest->appendChild($doc->createElement('Profiles'));
            $ProfileInfo = $Profiles->appendChild($doc->createElement('ProfileInfo'));
            $Profile = $ProfileInfo->appendChild($doc->createElement('Profile'));
            $Profile->setAttribute('ProfileType', 1);
            $Profile->setAttribute('StatusCode', "");
            $Customer = $Profile->appendChild($doc->createElement('Customer'));
            $PersonName = $Customer->appendChild($doc->createElement('PersonName'));
            $GivenName = $PersonName->appendChild($doc->createElement('GivenName', $booking_exist['first_name']));
            $Surname = $PersonName->appendChild($doc->createElement('Surname', $booking_exist['last_name']));
            $Telephone = $Customer->appendChild($doc->createElement('Telephone'));
            $Email = $Customer->appendChild($doc->createElement('Email', $booking_exist['email']));
            $Email->setAttribute('EmailType', 1);
            $Email->setAttribute('DefaultInd', 'true');
            $Address = $Customer->appendChild($doc->createElement('Address'));
            $Address->setAttribute('type', 1);
            $addressline = $Address->appendChild($doc->createElement('Addressline'));
            $cityname = $Address->appendChild($doc->createElement('CityName'));
            $postalcode = $Address->appendChild($doc->createElement('PostalCode'));
            $stateprov = $Address->appendChild($doc->createElement('StateProv'));
            $country = $Address->appendChild($doc->createElement('CountryName'));

            $ResGlobalInfo = $HotelReservation->appendChild($doc->createElement('ResGlobalInfo'));

            $TimeSpan = $ResGlobalInfo->appendChild($doc->createElement('TimeSpan'));
            $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
            $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
            $TimeSpan->setAttribute('Duration', 'P' . $night_span . 'D');

            // Start Cancellation function
            /*
             * Put this cancellation functions under TimeSpan node
             */

            $cancellation = DB::select(
                            'cancellations.*', array('rule_1.name', 'rule_1_name'), array('rule_2.name', 'rule_2_name'), array('rule_3.name', 'rule_3_name')
                    )
                    ->from('campaigns_cancellations')
                    ->join('cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
                    ->join(array('cancellation_rules', 'rule_1'))->on('rule_1.id', '=', 'cancellations.level_1_cancellation_rule_id')
                    ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'cancellations.level_2_cancellation_rule_id')
                    ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'cancellations.level_3_cancellation_rule_id')
                    ->where('campaigns_cancellations.campaign_id', '=', $booking_exist['campaign_id'])
                    ->execute()
                    ->current();

            if (!$cancellation) {
                // Search room default campaign
                $default_campaign_id = Model::factory('campaign')->get_default_campaign_id($booking_exist['room_id']);

                // Get cancellations
                $cancellation = Model::factory('cancellation')->get_cancellation($default_campaign_id, $booking_exist['check_in_date'], $booking_exist['check_out_date']);
            }

            $CancelPenalties = $ResGlobalInfo->appendChild($doc->createElement('CancelPenalties'));

            $CancelPenalty = $CancelPenalties->appendChild($doc->createElement('CancelPenalty'));
            $PenaltyDescription = $CancelPenalty->appendChild($doc->createElement('PenaltyDescription'));
            $PenaltyDescription->setAttribute('Name', $cancellation['rule_1_name']);

            if (!empty($cancellation['rule_2_name'])) {
                $CancelPenalty = $CancelPenalties->appendChild($doc->createElement('CancelPenalty'));
                $PenaltyDescription = $CancelPenalty->appendChild($doc->createElement('PenaltyDescription'));
                $PenaltyDescription->setAttribute('Name', $cancellation['rule_2_name']);
            }

            if (!empty($cancellation['rule_3_name'])) {
                $CancelPenalty = $CancelPenalties->appendChild($doc->createElement('CancelPenalty'));
                $PenaltyDescription = $CancelPenalty->appendChild($doc->createElement('PenaltyDescription'));
                $PenaltyDescription->setAttribute('Name', $cancellation['rule_3_name']);
            }

            // End Cancellation function

            $SpecialRequests = $ResGlobalInfo->appendChild($doc->createElement('SpecialRequests'));

            if ($booking_exist['is_non_smoking_room_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $SpecialRequest->setAttribute('RequestCode', 198);
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Nonsmoking Room'));
            }
            if ($booking_exist['is_early_check_in_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $SpecialRequest->setAttribute('RequestCode', 196);
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Early check in request'));
            }
            if ($booking_exist['is_high_floor_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'High Floor Request'));
            }
            if ($booking_exist['is_large_bed_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Late Check In'));
            }
            if ($booking_exist['is_twin_beds_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Twin Bed Request'));
            }
            if ($booking_exist['is_airport_transfer_request']) {
                $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Airport Transfer request'));
            }

            $Total = $ResGlobalInfo->appendChild($doc->createElement('Total'));
            $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total, 2));
            $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total, 2));
            $Total->setAttribute('CurrencyCode', 'USD');
            $Taxes = $Total->appendChild($doc->createElement('Taxes'));
            $Tax = $Taxes->appendChild($doc->createElement('Tax'));
            $Tax->setAttribute('Amount', round($tax, 2));
            $Tax->setAttribute('CurrencyCode', 'USD');

            $HotelReservationIDs = $ResGlobalInfo->appendChild($doc->createElement('HotelReservationIDs'));
            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $booking_exist['booking_id']);

            $doc->appendChild($envelope);

            $request = $doc->saveXML();

            CustomLog::factory()->add(4, 'DEBUG', $request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, Kohana::config('api.rategain.rategain_reservation_url'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Content-length: " . strlen($request)));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $response = curl_exec($ch);
            // Close the handle
            curl_close($ch);

            /*
             * XML process
             */

            try {
                $xml = new SimpleXMLElement($response);
                $body = $xml->children('s', true)->Body->children();
                $OTA = $body->OTA_HotelResNotifRS;

                // Check response Success/ Errors
                if (isset($OTA->Success)) {

                    DB::delete('api_reservation_queues')
                            ->where('api', '=', 1)
                            ->where('request_type', '=', 2)
                            ->where('booking_id', '=', $booking_exist['booking_id'])
                            ->execute();

                    CustomLog::factory()->add(4, 'DEBUG', 'Rategain cancel reservation notification Success');
                    CustomLog::factory()->add(4, 'DEBUG', $response);
                } else {
                    $booking_exist['retries'] = $booking_exist['retries'] + 1;

                    DB::update('api_reservation_queues')
                            ->set(
                                    array('retries' => $booking_exist['retries'])
                            )
                            ->where('booking_id', '=', $booking_exist['booking_id'])
                            ->where('request_type', '=', $booking_exist['request_type'])
                            ->where('api', '=', $booking_exist['api'])
                            ->execute();
                    $success = FALSE;
                }
            } catch (Exception $e) {
                $success = FALSE;
                $booking_exist['retries'] = $booking_exist['retries'] + 1;

                DB::update('api_reservation_queues')
                        ->set(
                                array('retries' => $booking_exist['retries'])
                        )
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();
                CustomLog::factory()->add(4, 'DEBUG', 'Rategain cancel reservation notification Failed');
                CustomLog::factory()->add(4, 'DEBUG', $response);
            }
        } catch (Kohana_Exception $e) {
            $booking_exist['retries'] = $booking_exist['retries'] + 1;

            DB::update('api_reservation_queues')
                    ->set(
                            array('retries' => $booking_exist['retries'])
                    )
                    ->where('booking_id', '=', $booking_exist['booking_id'])
                    ->where('request_type', '=', $booking_exist['request_type'])
                    ->where('api', '=', $booking_exist['api'])
                    ->execute();
            $success = FALSE;
            CustomLog::factory()->add(4, 'DEBUG', 'Rategain cancel reservation notification failed');

            $values = $this->request->post();
            foreach ($values as $key => $value) {
                CustomLog::factory()->add(4, 'DEBUG', "$key: $value");
            }
        }

        return $success;
    }

}
