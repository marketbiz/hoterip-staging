<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Channel extends Controller {
    /*
     * List Frontend Model
     * Hotel :search_hotel, search_campaign, get_campaign
     * Room capacity :getroom, checkroom, extrabed
     * Country/City  : getbysegment
     * Timezone  : timezone identifier
     * Setting  : get
     * Exchane  : To
     */

    public function checker($head, $xml) {
        $data = array();

        //Add that namespace
        $namespace = $xml->getDocNamespaces(TRUE);
        foreach ($namespace as $html_element => $value) {
            $xmlns = $value;
        }

        $node_head = array(
            'head' => $head,
            'xmlns' => $xmlns,
            'TimeStamp' => (string) $xml->attributes()->TimeStamp,
            'Target' => (string) $xml->attributes()->Target,
            'Version' => (string) $xml->attributes()->Version,
        );

        //if($head == 'PingRQ' || count($xml->Authentication) == 0)
        if ($head == 'PingRQ') {
            if (count($xml->EchoData) > 0) {
                $data = $this->PingRQ($xml);
                return $data;
            } else {
                $error['321'] = '10';
            }
        } else {
            // Check Auth
            $auth = array(
                // 'Token' => (string)$xml->Authentication->attributes()->Token,
                'username' => (string) $xml->Authentication->attributes()->UserName,
                'password' => (string) $xml->Authentication->attributes()->Password,
            );

            if ($head == 'HotelARIGetRQ') {
                $auth['Hotel_Code'] = (string) $xml->HotelARIGetRequests->attributes()->HotelCode;

                // Check null request
                if (empty($auth['Hotel_Code'])) {
                    $error['321'] = '10';
                }

                // Check Hotel Code
                if (!is_numeric($auth['Hotel_Code'])) {
                    $error['143'] = '8';
                }
            } elseif ($head == 'HotelARIUpdateRQ') {
                $auth['Hotel_Code'] = (string) $xml->HotelARIUpdateRequest->attributes()->HotelCode;
            } elseif ($head == 'HotelProductListGetRQ') {
                $auth['Hotel_Code'] = (string) $xml->HotelProductListRequest->attributes()->HotelCode;
            } elseif ($head == 'HotelPropertyListGetRQ') {
                $auth['Hotel_Code'] = (string) $xml->HotelPropertyListRequest->attributes()->ChainCode;
            }

            // Check null request
            if (!$auth['password'])
                $error = array('321' => '4');
            else {
                // Check Auth
                if ($this->auth($auth) == FALSE) {
                    // die('1');
                    $error = array('175' => '4');
                } else {
                    if ((($this->authHotelAdmin($auth) == FALSE) && ($auth['Hotel_Code'] > 0)) || ((!$this->authHotelFilter($auth)) && ($auth['Hotel_Code'] > 0))) {
                        $error['497'] = '6';
                    } else {
                        $error = array();
                        switch ($head) {

                            case $head == 'HotelPropertyListGetRQ':

                                $hotels_ids = DB::select('admins_hotels.admin_id', 'admins_hotels.hotel_id')
                                        ->from('admins')
                                        ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                                        ->where('admins.username', '=', $auth['username'])
                                        ->execute()
                                        ->as_array('hotel_id');

                                $hotels_ids = array_keys($hotels_ids);

                                $data = array(
                                    'ChainCode' => count($xml->HotelPropertyListRequest) > 0 ? (string) $xml->HotelPropertyListRequest->attributes()->ChainCode : NULL,
                                    'hotels_ids' => $hotels_ids
                                );

                                array_push($data, $auth);
                                break;

                            case $head == 'HotelProductListGetRQ':
                                $data = array(
                                    'Hotel_Code' => count($xml->HotelProductListRequest) > 0 ? (string) $xml->HotelProductListRequest->attributes()->HotelCode : $error['392'] = '10',
                                );

                                // Check null request
                                if (empty($data['Hotel_Code'])) {
                                    $error['321'] = '10';
                                }

                                // Check Hotel Code
                                if (!is_numeric($data['Hotel_Code'])) {
                                    $error['143'] = '8';
                                }

                                break;

                            case $head == 'HotelARIGetRQ':
                                $HotelARIGetRequest = $xml->HotelARIGetRequests->HotelARIGetRequest;

                                if (count($xml->HotelARIGetRequests) > 0 && count($HotelARIGetRequest->ProductReference) > 0 && count($HotelARIGetRequest->ApplicationControl) > 0) {
                                    $data = array(
                                        'HotelCode' => (string) $xml->HotelARIGetRequests->attributes()->HotelCode,
                                        'ProductReference' => array(
                                            'InvTypeCode' => (string) $HotelARIGetRequest->ProductReference->attributes()->InvTypeCode,
                                            'RatePlanCode' => (string) $HotelARIGetRequest->ProductReference->attributes()->RatePlanCode,
                                        ),
                                        'ApplicationControl' => array(
                                            'Start' => (string) $HotelARIGetRequest->ApplicationControl->attributes()->Start,
                                            'End' => (string) $HotelARIGetRequest->ApplicationControl->attributes()->End,
                                        ),
                                    );

                                    // Check null request
                                    if (empty($data['HotelCode']) || empty($data['ProductReference']['InvTypeCode']) || empty($data['ProductReference']['RatePlanCode']) || empty($data['ApplicationControl']['Start']) || empty($data['ApplicationControl']['End'])
                                    ) {
                                        $error['321'] = '10';
                                    }

                                    // Check Hotel Code
                                    if (!is_numeric($data['HotelCode'])) {
                                        $error['143'] = '8';
                                    }

                                    // Check start and end date format
                                    if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['ApplicationControl']['Start']))
                                        $error['136'] = '3';
                                    if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['ApplicationControl']['End']))
                                        $error['135'] = '3';

                                    // Check date range
                                    $data['stay_night'] = (strtotime($data['ApplicationControl']['End']) - strtotime($data['ApplicationControl']['Start'])) / (60 * 60 * 24);

                                    if ($data['stay_night'] < 0)
                                        $error['404'] = '8';
                                    if ($data['stay_night'] > 364)
                                        $error['407'] = '3';

                                    // Check Hotel Code
                                    if (!is_numeric($data['HotelCode'])) {
                                        $error['143'] = '8';
                                    }
                                    // Check Rate Code
                                    if (!is_numeric($data['ProductReference']['RatePlanCode'])) {
                                        $error['249'] = '8';
                                    }
                                    // Check Inv Code
                                    if (!is_numeric($data['ProductReference']['InvTypeCode'])) {
                                        $error['402'] = '8';
                                    }
                                } else {
                                    $error['321'] = '10';
                                }

                                break;

                            case $head == 'HotelARIUpdateRQ':

                                $nodes = $xml->HotelARIUpdateRequest;
                                $i = 0;

                                foreach ($nodes->HotelARIData as $order => $node) {

                                    if (count($xml->HotelARIUpdateRequest) > 0 && count($node->ProductReference) > 0 && count($node->ApplicationControl) > 0) {
                                        $data['data'][$i] = array(
                                            'HotelCode' => (string) $xml->HotelARIUpdateRequest->attributes()->HotelCode,
                                            'ItemIdentifier' => (string) $xml->HotelARIUpdateRequest->attributes()->ItemIdentifier,
                                            'UpdateType' => (string) $xml->HotelARIUpdateRequest->attributes()->UpdateType,
                                            'ProductReference' => array(
                                                'InvTypeCode' => (string) $node->ProductReference->attributes()->InvTypeCode,
                                                'RatePlanCode' => (string) $node->ProductReference->attributes()->RatePlanCode,
                                            ),
                                            'ApplicationControl' => array(
                                                'Start' => (string) $node->ApplicationControl->attributes()->Start,
                                                'End' => (string) $node->ApplicationControl->attributes()->End,
                                                '0' => (string) $node->ApplicationControl->attributes()->Sun,
                                                '1' => (string) $node->ApplicationControl->attributes()->Mon,
                                                '2' => (string) $node->ApplicationControl->attributes()->Tue,
                                                '3' => (string) $node->ApplicationControl->attributes()->Wed,
                                                '4' => (string) $node->ApplicationControl->attributes()->Thu,
                                                '5' => (string) $node->ApplicationControl->attributes()->Fri,
                                                '6' => (string) $node->ApplicationControl->attributes()->Sat
                                            ),
                                        );

                                        // Check null request
                                        if (empty($data['data'][$i]['HotelCode']) || empty($data['data'][$i]['UpdateType']) || empty($data['data'][$i]['ProductReference']['InvTypeCode']) || empty($data['data'][$i]['ProductReference']['RatePlanCode']) || empty($data['data'][$i]['ApplicationControl']['Start']) || empty($data['data'][$i]['ApplicationControl']['End'])) {
                                            $error['321'] = '10';
                                        }

                                        // Check Hotel Code
                                        if (!is_numeric($data['data'][$i]['HotelCode'])) {
                                            $error['143'] = '8';
                                        }
                                        // Check Rate Code
                                        if (!is_numeric($data['data'][$i]['ProductReference']['RatePlanCode'])) {
                                            $error['249'] = '8';
                                        }
                                        // Check Inv Code
                                        if (!is_numeric($data['data'][$i]['ProductReference']['InvTypeCode'])) {
                                            $error['402'] = '8';
                                        }

                                        // Check Number Of Nights
                                        $stay_night = Date::span(strtotime($data['data'][$i]['ApplicationControl']['End']), strtotime($data['data'][$i]['ApplicationControl']['Start']), 'days');
                                        if ($stay_night < 0)
                                            $error['404'] = '8';
                                        if ($stay_night > 364)
                                            $error['407'] = '3';

                                        // Check start and end date format
                                        if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['data'][$i]['ApplicationControl']['Start']))
                                            $error['136'] = '3';
                                        if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['data'][$i]['ApplicationControl']['End']))
                                            $error['135'] = '3';

                                        if (count($node->RateAmounts) > 0) {
                                            $rate = array();

                                            // Check discount 
                                            if (count($node->RateAmounts->Discount) > 0) {
                                                $rate['RateAmounts'] = array(
                                                    'Amount' => (string) $node->RateAmounts->Discount->attributes()->Amount,
                                                    'Percent' => (string) $node->RateAmounts->Discount->attributes()->Percent
                                                );

                                                // Check null request
                                                if (empty($rate['RateAmounts']['Amount']) && empty($rate['RateAmounts']['Percent'])) {
                                                    $error['321'] = '10';
                                                }

                                                // Check price
                                                if (!is_numeric($rate['RateAmounts']['Amount']) && !is_numeric($rate['RateAmounts']['Percent'])) {
                                                    $error['143'] = '8';
                                                }
                                            }

                                            // Check RateAmounts
                                            if (in_array((String) $node->RateAmounts->attributes()->Currency, array('USD', 'JPY', 'IDR'))) {
                                                $rate['RateAmounts']['Currency'] = (String) $node->RateAmounts->attributes()->Currency;
                                            } else {
                                                $error['558'] = '8';
                                            }


                                            $data['UpdateType'] = empty($data['UpdateType']) ? '' : $data['UpdateType'];

                                            if (strtolower($data['UpdateType']) == 'complete') {
                                                $rate['RateAmounts']['Currency'] = 'USD';
                                                $rate['RateAmounts']['TaxesIncluded'] = 1;

                                                // Check null request
                                                if (empty($rate['RateAmounts']['Currency'])) {
                                                    $error['321'] = '10';
                                                }

                                                // Check currency settled
                                                if ($rate['RateAmounts']['Currency'] != 'USD' && $rate['RateAmounts']['Currency'] != 'JPY' && $rate['RateAmounts']['Currency'] != 'ER') {
                                                    $error['558'] = '8';
                                                }
                                            }

                                            // Check MealPlan
                                            if (count($node->RateAmounts->MealPlans->MealPlan) > 0) {
                                                // MealPlans
                                                $MealPlans = $node->RateAmounts->MealPlans->MealPlan;

                                                foreach ($MealPlans as $MealPlan) {
                                                    $rate['RateAmounts']['MealPlans'] = array(
                                                        'MealPlanCode' => (string) $MealPlan->attributes()->MealPlanCode,
                                                        'Included' => strtolower($MealPlan->attributes()->Included) == 'true' ? true : false,
                                                        'Amount' => (string) $MealPlan->attributes()->Amount
                                                    );

                                                    // Check null request
                                                    if (empty($rate['RateAmounts']['MealPlans']['MealPlanCode']) && empty($rate['RateAmounts']['MealPlans']['Amount'])) {
                                                        $error['321'] = '10';
                                                    }

                                                    // Check price
                                                    if (!is_numeric($rate['RateAmounts']['MealPlans']['Amount'])) {
                                                        $error['143'] = '8';
                                                    }

                                                    // Break for first data
                                                    break;
                                                }
                                            }

                                            // Check Base
                                            if (count($node->RateAmounts->Base) > 0) {
                                                $Bases = $node->RateAmounts->Base;
                                                foreach ($Bases as $base_key => $Base_single) {
                                                    $rate['RateAmounts']['Base'] = array(
                                                        'base_occupancy' => (string) $node->RateAmounts->Base->attributes()->OccupancyCode,
                                                        'base_before_tax' => (string) $node->RateAmounts->Base->attributes()->AmountBeforeTax ?
                                                                (string) $node->RateAmounts->Base->attributes()->AmountBeforeTax : NULL,
                                                        'base_after_tax' => (string) $node->RateAmounts->Base->attributes()->Amount,
                                                    );

                                                    // Check null request
                                                    if (empty($rate['RateAmounts']['Base']['base_occupancy']) || !isset($rate['RateAmounts']['Base']['base_after_tax'])) {
                                                        $error['321'] = '10';
                                                    }

                                                    // Check price
                                                    if (!is_numeric($rate['RateAmounts']['Base']['base_after_tax'])) {
                                                        $error['143'] = '8';
                                                    }

                                                    // Break for first data
                                                    break;
                                                }
                                                // Check base
                                                if (count($rate['RateAmounts']['Base']) > 10) {
                                                    $error['43'] = '8';
                                                }
                                            }

                                            // Check Additional
                                            if (count($node->RateAmounts->Additional) > 0) {
                                                $Bases = $node->RateAmounts->Additional;
                                                foreach ($Bases as $base_key => $Base_single) {
                                                    $rate['RateAmounts']['Additional'] = array(
                                                        'base_occupancy' => (string) $node->RateAmounts->Additional->attributes()->OccupancyCode,
                                                        'base_before_tax' => (string) $node->RateAmounts->Additional->attributes()->AmountBeforeTax ?
                                                                (string) $node->RateAmounts->Additional->attributes()->AmountBeforeTax : 0,
                                                        'base_after_tax' => (string) $node->RateAmounts->Additional->attributes()->Amount,
                                                    );

                                                    // Check null request
                                                    if (empty($rate['RateAmounts']['Additional']['base_occupancy']) || !isset($rate['RateAmounts']['Additional']['base_after_tax'])) {
                                                        $error['321'] = '10';
                                                    }

                                                    // Check price
                                                    if (!is_numeric($rate['RateAmounts']['Additional']['base_after_tax'])) {
                                                        $error['143'] = '8';
                                                    }

                                                    // Break for first data
                                                    break;
                                                }
                                                // Check base
                                                if (count($rate['RateAmounts']['Additional']) > 10) {
                                                    $error['43'] = '8';
                                                }
                                            }
                                            $data['data'][$i] = array_merge($data['data'][$i], $rate);
                                        }

                                        if (count($node->Availability) > 0) {
                                            $availability['Availability'] = array(
                                                'Master' => strtolower((string) $node->Availability->attributes()->Master),
                                                'Arrival' => strtolower((string) $node->Availability->attributes()->Arrival),
                                                'Departure' => strtolower((string) $node->Availability->attributes()->Departure),
                                            );

                                            // Check null request
                                            if (empty($availability['Availability']['Master'])) {
                                                $error['321'] = '10';
                                            }

                                            $data['data'][$i] = array_merge($data['data'][$i], $availability);
                                        }

                                        if (count($node->BookingRules) > 0) {
                                            $bookingrules = array(
                                                'MinAdvancedBookingOffset' => count($node->BookingRules->MinAdvancedBookingOffset) > 0 ? (string) $node->BookingRules->MinAdvancedBookingOffset : NULL,
                                                'MaxAdvancedBookingOffset' => count($node->BookingRules->MaxAdvancedBookingOffset) > 0 ? (string) $node->BookingRules->MaxAdvancedBookingOffset : NULL,
                                                'MinLoSOnArrival' => count($node->BookingRules->MinLoSOnArrival) > 0 ? (string) $node->BookingRules->MinLoSOnArrival : NULL,
                                                'MaxLoSOnArrival' => count($node->BookingRules->MaxLoSOnArriva) > 0 ? (string) $node->BookingRules->MaxLoSOnArriva : NULL,
                                                'MinLoSThrough' => count($node->BookingRules->MinLoSThrough) > 0 ? (string) $node->BookingRules->MinLoSThrough : NULL,
                                                'MaxLoSThrough' => count($node->BookingRules->MaxLoSThrough) > 0 ? (string) $node->BookingRules->MaxLoSThrough : NULL,
                                                'Description' => count($node->BookingRules->Description) > 0 ? (string) $node->BookingRules->Description : NULL
                                            );

                                            $data['data'][$i] = array_merge($data['data'][$i], $bookingrules);
                                        }

                                        if (count($node->BookingLimit->TransientAllotment) > 0) {
                                            $bookinglimit['BookingLimit'] = array(
                                                'FreeSale' => count($node->BookingLimit->FreeSale) > 0 ? (string) $node->BookingLimit->FreeSale : NULL,
                                                'TransientAllotment' => array(
                                                    'ReleaseDays' => $node->BookingLimit->TransientAllotment->attributes()->ReleaseDays ? (string) $node->BookingLimit->TransientAllotment->attributes()->ReleaseDays : 0,
                                                    'Allotment' => $node->BookingLimit->TransientAllotment->attributes()->Allotment ? (string) $node->BookingLimit->TransientAllotment->attributes()->Allotment : 0
                                            ));

                                            // Check null request
                                            // if(empty($bookinglimit['BookingLimit']['TransientAllotment']['ReleaseDays'])
                                            //   && empty($bookinglimit['BookingLimit']['TransientAllotment']['Allotment']))
                                            // {
                                            //   $error['321'] = '10';
                                            // }
                                        }
                                        $data['data'][$i] = array_merge($data['data'][$i], $bookinglimit);
                                    } else {
                                        $error['321'] = '10';
                                    }
                                    $i++;
                                }

                                break;

                            default :
                                $error['188'] = '2';
                                break;
                        }
                    }
                }
            }
        }
        $data = array_merge($data, $node_head);

        // Guid 
        $s_token = strtoupper(md5(uniqid(rand(), true)));
        $data['token'] = substr($s_token, 0, 8) . '-' .
                substr($s_token, 8, 4) . '-' .
                substr($s_token, 12, 4) . '-' .
                substr($s_token, 16, 4) . '-' .
                substr($s_token, 20, 12);

        // Check error exist
        if (!empty($error)) {
            $this->errors($error, $node_head);
        } else {
            return $data;
        }
    }

    public function rooms($xml) {
        $campaigns = DB::select(
                        array('campaigns.id', 'campaign_id'), array('campaigns.type', 'campaign_type'), array('campaigns.is_default', 'is_default'), array('campaigns.discount_rate', 'discount_rate'), array('campaigns.discount_amount', 'discount_amount'), array('campaigns.days_in_advance', 'days_in_advance'), array('campaigns.minimum_number_of_nights', 'minimum_number_of_nights'), array('campaigns.minimum_number_of_rooms', 'minimum_number_of_rooms'), array('campaigns.within_days_of_arrival', 'within_days_of_arrival'), array('campaigns.number_of_free_nights', 'number_of_free_nights'), array('rooms.id', 'room_id'), array('room_texts.name', 'room_name'), array('hotels.currency_id', 'currency'), array('hotels.timezone', 'hotel_timezone'), array('currencies.code', 'currency_code')
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
                ->join('rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
                ->join('room_texts', 'LEFT')->on('room_texts.room_id', '=', 'rooms.id')
                ->join('hotels')->on('rooms.hotel_id', '=', 'hotels.id')
                ->join('currencies')->on('hotels.currency_id', '=', 'currencies.id')
                ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
                ->where('hotels.id', '=', $xml['Hotel_Code'])
                ->where('campaigns.is_default', '=', 1)
                ->limit(10)
                ->order_by('campaign_id', 'ASC')
                ->execute()
        ;

        return $campaigns;
    }

    public function campaigns($data) {
        $hotel_query = array(
            'language_id' => Controller_Api_Request::default_language(),
            'HotelCode' => $data['HotelCode'],
            'check_in' => $data['ApplicationControl']['Start'],
            'check_out' => $data['ApplicationControl']['End'],
            'InvTypeCode' => $data['ProductReference']['InvTypeCode'],
            'RatePlanCode' => $data['ProductReference']['RatePlanCode'],
            'now' => date("Y-m-d")
        );

        $start_date_timestamp = strtotime($data['ApplicationControl']['Start']);
        $end_date_timestamp = strtotime($data['ApplicationControl']['End']);

        // Get data
        $campaigns = array();
        for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
            $date = date('Y-m-d', $i);

            $add_day = strtotime("+1 day", $i);
            $hotel_query['check_in'] = $date;
            $hotel_query['check_out'] = date('Y-m-d', $add_day);

            $campaign = ORM::factory('campaign')->get_campaign_items($hotel_query);

            if (count($campaign)) {
                $campaigns[$date] = $campaign[$date];
                $campaigns[$date]['discount_rate'] = $campaign[$date]['rate'];
                $campaigns[$date]['discount_amount'] = $campaign[$date]['amount'];
                $campaigns[$date]['check_in'] = $hotel_query['check_in'];
                $campaigns[$date]['check_out'] = $hotel_query['check_out'];
            } else {
                $campaigns[$date] = array(
                    'date' => $date,
                    'status' => 'no_inventory'
                );
            }
        }

        return $campaigns;
    }

    public function promo_description($hotels_product_list) {
        $promo = '';

        $hotels_product_list['is_default'] = empty($hotels_product_list['is_default']) ? 0 : $hotels_product_list['is_default'];
        $hotels_product_list['number_of_free_nights'] = empty($hotels_product_list['number_of_free_nights']) ? 0 : $hotels_product_list['number_of_free_nights'];

        if ($hotels_product_list['is_default']) {
            $promo = 'BAR.';
        } else {
            if ($hotels_product_list['discount_rate'] > 0) {
                $promo = 'Discount ' . $hotels_product_list['discount_rate'] . '%. ';
            } elseif ($hotels_product_list['discount_amount'] > 0) {
                $promo = 'Discount ' . $hotels_product_list['currency_code'] . ' ' . $hotels_product_list['discount_amount'] . '. ';
            } elseif ($hotels_product_list['number_of_free_nights'] > 0) {
                $promo = 'Free ' . $hotels_product_list['number_of_free_nights'] . ' Nights. ';
            }

            if ($hotels_product_list['within_days_of_arrival'] > 0) {
                $within_days_of_arrival = $hotels_product_list['within_days_of_arrival'];
                $promo = 'Book before' . '' . Date::formatted_time("+{$within_days_of_arrival} days", __('M j, Y'), $hotels_product_list['hotel_timezone']) . '. ';
            }
            if ($hotels_product_list['days_in_advance'] > 0) {
                $early_days = $hotels_product_list['days_in_advance'] - 1;
                $promo = 'Book after' . ' ' . Date::formatted_time("+$early_days days", __('M j, Y'), $hotels_product_list['hotel_timezone']) . '. ';
            }

            $promo .= 'Stay at least ' . $hotels_product_list['minimum_number_of_nights'] . ' nights and book ' . $hotels_product_list['minimum_number_of_rooms'] . ' or more rooms.';
        }

        return $promo;
    }

    public function auth($auth) {
        if (A1::instance()->login($auth['username'], $auth['password'])) {
            return TRUE;
        }
        return FALSE;
    }

    public function authHotelAdmin($auth) {

        $admin = ORM::factory('admin')
                ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                ->where('admins_hotels.hotel_id', '=', $auth['Hotel_Code'])
                ->where('admins.username', '=', $auth['username'])
                ->find();

        if ($admin->loaded()) {
            return true;
        } else {
            return false;
        }
    }

    public function authHotelFilter($auth) {
        $filteredHotel = DB::select('*')
                ->from('settings')
                ->where('key', '=', 'rategain_hotel_filter')
                ->execute()
                ->current();

        $filteredHotel = json_decode($filteredHotel['value']);

        if (in_array($auth['Hotel_Code'], $filteredHotel)) {
            return true;
        } else {
            return false;
        }
    }

    public function errors($errors, $node_head) {

        $list_error_type = array(
            '1' => 'Unknown',
            '2' => 'No implementation',
            '3' => 'Biz rule',
            '4' => 'Authentication',
            '5' => 'Authentication timeout',
            '6' => 'Authorization',
            '7' => 'Protocol violation',
            '8' => 'Transaction model',
            '9' => 'Authentication model',
            '10' => 'Required field missing',
            '11' => 'Advisory',
            '12' => 'Processing exception',
            '3' => 'Application error',
        );

        $list_error_code = array(
            '15' => 'Invalid Date',
            '41' => 'Occupancy must include an adult',
            '44' => 'No under/over occupancy allowed',
            '43' => 'Too many accommodation types',
            '51' => 'Prices not yet released',
            '135' => 'Invalid End Date',
            '136' => 'Invalid Start Date',
            '137' => 'Adult numbers/occupancy mismatch',
            '138' => 'Child numbers/occupancy mismatch',
            '143' => 'Price incorrect for room/unit',
            '175' => 'Password invalid',
            '177' => 'Premises suspended - access denied',
            '182' => 'Password required',
            '186' => 'Booking closed - contact provider',
            '187' => 'System currently unavailable',
            '188' => 'Transaction error - please report',
            '190' => 'Wrong network - access denied',
            '191' => 'System busy - please try later',
            '197' => 'Undetermined error - please report',
            '249' => 'Invalid rate code',
            '320' => 'Invalid value',
            '321' => 'Required field missing',
            '356' => 'Invalid Action/Status code',
            '361' => 'Invalid hotel',
            '362' => 'Invalid number of nights',
            '375' => 'Hotel not active',
            '392' => 'Invalid Hotel Code',
            '397' => 'Invalid number of adults',
            '400' => 'Invalid property code',
            '402' => 'Invalid room type code',
            '404' => 'Invalid start date-end date combination',
            '407' => 'Item too long',
            '424' => 'No hotels found which match this input',
            '425' => 'No match found',
            '427' => 'No rooms available for requested dates',
            '447' => 'Unable to update – simultaneous updates',
            '448' => 'System error',
            '450' => 'Unable to process',
            '458' => 'Date outside inventory period',
            '497' => 'Authorization error',
            '502' => 'Advance notice requirements not met',
            '558' => 'Accounts are settled in a currency different from the quoted rate currency',
            '731' => 'Room type discontinued at this hotel',
            '732' => 'Invalid room type for rate requested',
            '784' => 'Time out – Please modify your request',
            '842' => 'Rate not loaded',
        );

        //Generate Error XML
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $response_node = $doc->appendChild($doc->createElement(substr($node_head['head'], 0, -2) . 'RS'));
        $response_node->setAttribute('xmlns', $node_head['xmlns']);
        $response_node->setAttribute('TimeStamp', $node_head['TimeStamp']);
        $response_node->setAttribute('Target', $node_head['Target']);
        $response_node->setAttribute('Version', $node_head['Version']);

        $errors_node = $response_node->appendChild($doc->createElement('Errors'));

        foreach ($errors as $key => $error) {
            $error_node = $errors_node->appendChild($doc->createElement('Error', $list_error_code[$key]));
            $error_node->setAttribute('Type', $error);
            $error_node->setAttribute('Code', $key);
        }
        $doc->appendChild($response_node);

        $response = $doc->saveXML();

        $this->output($response);
    }

    public function HotelPropertyListGetRQ($data) {
        // Get QA admins emails
        $hotels = ORM::factory('hotel')
                ->select(
                        array('hotels.id', 'hotel_id'), array('hotel_texts.name', 'hotel_name')
                )
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('admins_hotels', 'left')->on('admins_hotels.hotel_id', '=', 'hotels.id')
                ->join('admins')->on('admins_hotels.admin_id', '=', 'admins.id')
                ->where('hotel_texts.language_id', '=', Controller_Api_Request::default_language());

        if (!empty($data['ChainCode'])) {
            $hotels = $hotels
                    ->where('hotels.id', '=', $data['ChainCode']);
        } else {
            $hotels = $hotels
                    ->where('hotels.id', 'IN', $data['hotels_ids']);
        }

        $hotels = $hotels
                ->where('hotel_texts.language_id', '=', Controller_Api_Request::default_language())
                //->where('admins.username', '=', $data[0]['username'])
                ->group_by('hotel_id')
                ->find_all();

        if (count($hotels) == 0) {
            $this->errors($error = array('424' => '8'), $data);
        } else {
            // Build response
            return $this->HotelPropertyListGetRS($hotels, $data);
        }
    }

    public function HotelPropertyListGetRS($hotels, $data) {
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $response_node = $doc->createElement((substr($data['head'], 0, -2) . 'RS'));
            $response_node->setAttribute('xmlns', $data['xmlns']);
            $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
            $response_node->setAttribute('Target', $data['Target']);
            $response_node->setAttribute('Version', $data['Version']);
            $response_node->setAttribute('TransactionIdentifier', $data['token']);

            $success = $response_node->appendChild($doc->createElement('Success'));

            $Hotels = $response_node->appendChild($doc->createElement('Hotels'));
            $Hotels->setAttribute('ChainCode', $data['ChainCode']);

            foreach ($hotels as $key => $hotel) {
                $Hotel = $Hotels->appendChild($doc->createElement('Hotel'));
                $Hotel->setAttribute('HotelCode', $hotel->hotel_id);
                $Hotel->setAttribute('Name', htmlspecialchars(utf8_encode($hotel->hotel_name)));
            }

            $doc->appendChild($response_node);

            $response = $doc->saveXML();

            $this->output($response);
        } catch (Kohana_Exception $e) {
            $this->errors($error = array('424' => '8'), $data);
        }
    }

    public function HotelProductListGetRQ($data) {
        // Get hotel product
        $hotel_products_list = $this->rooms($data);

        if (!count($hotel_products_list)) {
            $this->errors($error = array('392' => '8'), $data);
        } else {
            // Build response
            return $this->HotelProductListGetRS($hotel_products_list, $data);
        }
    }

    public function HotelProductListGetRS($hotel_products_list, $data) {
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $response_node = $doc->createElement((substr($data['head'], 0, -2) . 'RS'));

            $response_node->setAttribute('xmlns', $data['xmlns']);
            $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
            $response_node->setAttribute('Target', $data['Target']);
            $response_node->setAttribute('Version', $data['Version']);
            $response_node->setAttribute('TransactionIdentifier', $data['token']);

            $response_node->appendChild($doc->createElement('Success'));

            $hotels_products = $response_node->appendChild($doc->createElement('HotelProducts'));
            $hotels_products->setAttribute('HotelCode', $data['Hotel_Code']);

            foreach ($hotel_products_list as $hotel_product_single) {
                $hotels_product = $hotels_products->appendChild($doc->createElement('HotelProduct'));

                $product = $hotels_product->appendChild($doc->createElement('ProductReference'));
                $product->setAttribute('InvTypeCode', $hotel_product_single['room_id']);
                $product->setAttribute('RatePlanCode', $hotel_product_single['campaign_id']);

                $hotels_product->appendChild($doc->createElement('RoomTypeName', htmlspecialchars(utf8_encode($hotel_product_single['room_name']))));

                //Rate Type Description
                $promo = $this->promo_description($hotel_product_single);

                $promo = $promo ? $promo : NULL;
                $hotels_product->appendChild($doc->createElement('RateTypeName', htmlspecialchars(utf8_encode($promo))));
            }

            $doc->appendChild($response_node);

            $response = $doc->saveXML();

            $this->output($response);
        } catch (Kohana_Exception $e) {
            if ($this->auth($auth) == FALSE) {
                $error[] = array(
                    'Type' => 3,
                    'Code' => '842',
                    'Desc' => 'Inventory not loaded for the requested date range.'
                );

                return $this->errors($error, $data);
            }
        }
    }

    public function HotelARIGetRQ($data) {


        $exchange = Model::factory('exchange');

        // Get hotel campaign
        $hotel_products_data_lists = $this->campaigns($data);

        $data['campaign_data'] = DB::select(
                        'campaigns.*', array(DB::expr('COUNT(campaigns_rooms.campaign_id)'), 'count')
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->where('campaigns_rooms.campaign_id', '=', $data['ProductReference']['RatePlanCode'])
                ->where('campaigns_rooms.room_id', '=', $data['ProductReference']['InvTypeCode'])
                ->where('campaigns.is_default', '=', 1)
                ->execute()
                ->current()
        ;

        if (empty($data['campaign_data']['id'])) {
            $this->errors($error = array('732' => '8'), $data);
        } else {
            // Occupancy Room
            $occupancies = ORM::factory('room_capacity')->get_room_capacities($data['ProductReference']['InvTypeCode']);

            foreach ($hotel_products_data_lists as $date => &$hotel_products_list) {
                if ($hotel_products_list['status'] == 'no_inventory') {
                    continue;
                }
                // Services Tax Rate
                $service_tax_rate = (($hotel_products_list['hotel_service_charge_rate'] + $hotel_products_list['hotel_tax_rate']) + 100) / 100;

                // Get surcharges
                // For additional guest
                // $surcharges = Model::factory('surcharge')->get_surcharges($data['HotelCode'], 
                //   $hotel_products_list['date'], $hotel_products_list['check_out']);
                // if(count($surcharges))
                // {
                //   foreach ($surcharges as $surcharge)
                //   {
                //     if($surcharge['adult_price'])
                //     {
                //       $adult_price = $surcharge['adult_price'] * $exchange->to($hotel_products_list['currency_id'], 1);
                //       $hotel_products_list['additional']['aa']['occupancy_code'] = 'AA';
                //       $hotel_products_list['additional']['aa']['price'] = number_format(round($adult_price));
                //       $hotel_products_list['additional']['aa']['before_price'] =  NULL;
                //     }
                //     if($surcharge['child_price'])
                //     {
                //       $child_price = $surcharge['child_price'] * $exchange->to($hotel_products_list['currency_id'], 1);
                //       $hotel_products_list['additional']['ac']['occupancy_code'] = 'AC';
                //       $hotel_products_list['additional']['ac']['price'] = number_format(round($child_price));
                //       $hotel_products_list['additional']['ac']['before_price'] = NULL;
                //     }
                //   }
                // }
                // foreach ($occupancies as $occupy => $occupancy_single)
                // {
                //   // Occupancy Code filter
                //   $occupancy = $occupancy_single['number_of_adults'].','.$occupancy_single['number_of_children'];
                //   switch ($occupancy) {
                //     case '1,0':
                //       $occupancy_code = 'A1';
                //       break;
                //     case '2,0':
                //       $occupancy_code = 'A2';
                //       break;
                //     case '2,1':
                //       $occupancy_code = 'A2C1';
                //       break;
                //     case '2,2':
                //       $occupancy_code = 'A1C2';
                //       break;
                //     case '3,0':
                //       $occupancy_code = 'A3';
                //       break;
                //     case '4,0':
                //       $occupancy_code = 'A4';
                //       break;
                //     default:
                //      $occupancy_code = 'X1';
                //     break;
                //   }
                //   // Rate Prices
                //   $after_price = $hotel_products_list['price'] * $exchange->to($hotel_products_list['currency_id'], 1);
                //   $before_price = ($hotel_products_list['price'] / $service_tax_rate) * $exchange->to($hotel_products_list['currency_id'], 1);
                //   $hotel_products_list['rates'][$occupancy_code]['occupancy_code'] = $occupancy_code;
                //   $hotel_products_list['rates'][$occupancy_code]['after_price'] = $after_price ? number_format(round($after_price)) : NULL;
                //   $hotel_products_list['rates'][$occupancy_code]['before_price'] = $before_price ? number_format(round($before_price / $service_tax_rate)) : NULL;
                // }
                // Rate Prices
                $after_price = $hotel_products_list['price'] * $exchange->to($hotel_products_list['currency_id'], $hotel_products_list['currency_id']);
                $before_price = ($hotel_products_list['price'] / $service_tax_rate) * $exchange->to($hotel_products_list['currency_id'], $hotel_products_list['currency_id']);

                $hotel_products_list['rates']['SR']['occupancy_code'] = 'SR';

                $hotel_products_list['is_campaign_prices'] = empty($hotel_products_list['is_campaign_prices']) ? 0 : $hotel_products_list['is_campaign_prices'];

                if ($hotel_products_list['is_campaign_prices'] === 0) {
                    if ($hotel_products_list['discount_amount']) {
                        $after_price = $after_price - ($hotel_products_list['discount_amount'] * $exchange->to($hotel_products_list['currency_id'], 1));
                        $before_price = $before_price - ($hotel_products_list['discount_amount'] * $exchange->to($hotel_products_list['currency_id'], 1));
                    } elseif ($hotel_products_list['discount_rate']) {
                        $after_price = $after_price - (((int) $after_price * $hotel_products_list['discount_rate']) / 100);
                        $before_price = $before_price - (((int) $before_price * $hotel_products_list['discount_rate']) / 100);
                    }
                }

                $hotel_products_list['rates']['SR']['after_price'] = $after_price ? number_format(round($after_price)) : NULL;
                $hotel_products_list['rates']['SR']['before_price'] = $before_price ? number_format(round($before_price / $service_tax_rate)) : NULL;

                // Additional
                if ($hotel_products_list['maximum_number_of_extrabeds'] > 0) {

                    $hotel_products_list['extrabed_item_price'] = empty($hotel_products_list['extrabed_item_price']) ? 0 : $hotel_products_list['extrabed_item_price'];

                    /// extrabed item price
                    $extrabed_price = $hotel_products_list['extrabed_item_price'] != 0.1 ? $hotel_products_list['extrabed_price'] : $hotel_products_list['extrabed_item_price'];
                    $extrabed_price = $extrabed_price * $exchange->to($hotel_products_list['currency_id'], 1);

                    $hotel_products_list['additional']['eb']['occupancy_code'] = 'EB';
                    $hotel_products_list['additional']['eb']['price'] = $hotel_products_list['maximum_number_of_extrabeds'] > 0 ?
                            number_format(round($extrabed_price)) : NULL;
                    $hotel_products_list['additional']['eb']['before_price'] = NULL;
                }

                // Meal Plans
                if ($hotel_products_list['is_breakfast_included'] == 1) {

                    $extrabed_price = $hotel_products_list['adult_breakfast_price'] * $exchange->to($hotel_products_list['currency_id'], 1);

                    $hotel_products_list['MealPlan']['19']['occupancy_code'] = '19';
                    $hotel_products_list['MealPlan']['19']['included'] = $hotel_products_list['is_breakfast_included'] ? 'True' : 'False';
                    $hotel_products_list['MealPlan']['19']['price'] = $hotel_products_list['is_breakfast_included'] ? number_format(round($extrabed_price)) : NULL;
                }

                // Determine room available
                $hotel_no_arrival = DB::select()
                        ->from('hotel_no_arrivals')
                        ->where('hotel_id', '=', $data['HotelCode'])
                        ->where('date', '=', $data['ApplicationControl']['Start'])
                        ->execute()
                        ->get('date');

                // Get no departure
                $hotel_no_departure = DB::select()
                        ->from('hotel_no_departures')
                        ->where('hotel_id', '=', $data['HotelCode'])
                        ->where('date', '=', $data['ApplicationControl']['End'])
                        ->execute()
                        ->get('date');

                $hotel_products_list['Availability']['arrival'] = $hotel_no_arrival ? 'Close' : 'Open';

                $hotel_products_list['Availability']['departure'] = $hotel_no_departure ? 'Close' : 'Open';

                // Determine if guest stay night more than minimum night
                $Onrequest = TRUE;

                $hotel_products_list['campaign_average_price'] = empty($hotel_products_list['campaign_average_price']) ? 0 : $hotel_products_list['campaign_average_price'];

                if ($hotel_products_list['campaign_average_price'] AND ! $hotel_no_arrival AND ! $hotel_no_departure AND $hotel_products_list['minimum_number_of_nights']) {
                    if (($data['stay_night'] >= $hotel_products_list['minimum_number_of_nights'])) {
                        $Onrequest = FALSE;
                    }
                }

                $hotel_products_list['Availability']['master'] = $Onrequest ? 'Close' : 'Open';

                // Application Control
                $hotel_products_list['Sun'] = $hotel_products_list['sun'] ? 'true' : 'false';
                $hotel_products_list['Mon'] = $hotel_products_list['mon'] ? 'true' : 'false';
                $hotel_products_list['Tue'] = $hotel_products_list['tue'] ? 'true' : 'false';
                $hotel_products_list['Wed'] = $hotel_products_list['wed'] ? 'true' : 'false';
                $hotel_products_list['Thu'] = $hotel_products_list['thu'] ? 'true' : 'false';
                $hotel_products_list['Fri'] = $hotel_products_list['fri'] ? 'true' : 'false';
                $hotel_products_list['Sat'] = $hotel_products_list['sat'] ? 'true' : 'false';

                // Currency // Change for now
                // $hotel_products_list['currency'] = 'USD';
                switch ($hotel_products_list['currency_id']) {
                    case 3:
                        $hotel_products_list['currency'] = 'IDR';
                        break;
                    case 2:
                        $hotel_products_list['currency'] = 'JPY';
                        break;
                    default:
                        $hotel_products_list['currency'] = 'USD';
                        break;
                }

                // Discount
                $discount_amount = $hotel_products_list['amount'] ? $hotel_products_list['amount'] * $exchange->to($hotel_products_list['currency_id'], 1) : NULL;
                $hotel_products_list['discount_amount'] = number_format(ceil($discount_amount));
                $hotel_products_list['discount_rate'] = $hotel_products_list['rate'] ? $hotel_products_list['rate'] : NULL;

                // Booking data
                // Remove from fetch and update in sandbox
                $hotel_products_list['MaxAdvancedBookingOffset'] = isset($hotel_products_list['days_in_advance']) ? $hotel_products_list['days_in_advance'] : NULL;
                //$hotel_products_list['MaxAdvancedBookingOffset'] = isset($hotel_products_list['within_days_of_arrival']) ? $hotel_products_list['within_days_of_arrival']:NULL;
                // $hotel_products_list['MinLoSOnArrival'] = isset($hotel_products_list['minimum_number_of_nights']) ? $hotel_products_list['minimum_number_of_nights']:NULL;
                // $hotel_products_list['MaxLoSOnArrival'] = NULL;
                $hotel_products_list['MinLoSThrough'] = isset($hotel_products_list['minimum_number_of_nights']) ? $hotel_products_list['minimum_number_of_nights'] : NULL;
                //$hotel_products_list['MaxLoSThrough'] = NULL;

                $promo = $this->promo_description($hotel_products_list);

                $hotel_products_list['Description'] = $promo ? $promo : NULL;
            }

            // Build response
            return $this->HotelARIGetRS($hotel_products_data_lists, $data);
        }
    }

    public function HotelARIGetRS($hotel_products_lists, $data) {
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $response_node = $doc->createElement((substr($data['head'], 0, -2) . 'RS'));
            $response_node->setAttribute('xmlns', $data['xmlns']);
            $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
            $response_node->setAttribute('Target', $data['Target']);
            $response_node->setAttribute('Version', $data['Version']);
            $response_node->setAttribute('TransactionIdentifier', $data['token']);

            $response_node->appendChild($doc->createElement('Success'));

            $hotels_data_set = $response_node->appendChild($doc->createElement('HotelARIDataSet'));
            $hotels_data_set->setAttribute('HotelCode', $data['HotelCode']);

            if (!empty($hotel_products_lists)) {
                $i = 1;
                foreach ($hotel_products_lists as $hotel_products_list) {
                    $i++;

                    if ($hotel_products_list['status'] == 'no_inventory') {
                        $HotelARIData = 'HotelARIStatus';
                    } else {
                        $HotelARIData = 'HotelARIData';
                    }

                    $hotels_data = $hotels_data_set->appendChild($doc->createElement($HotelARIData));
                    empty($hotel_products_list['status']) ? $hotels_data->setAttribute('ItemIdentifier', $i) : '';

                    $product_reference = $hotels_data->appendChild($doc->createElement('ProductReference'));
                    $product_reference->setAttribute('InvTypeCode', $data['ProductReference']['InvTypeCode']);
                    $product_reference->setAttribute('RatePlanCode', $data['ProductReference']['RatePlanCode']);
                    $application_control = $hotels_data->appendChild($doc->createElement('ApplicationControl'));
                    $application_control->setAttribute('Start', $hotel_products_list['date']);
                    $application_control->setAttribute('End', $hotel_products_list['date']);
                    $application_control->setAttribute('Sun', $data['campaign_data']['is_stay_sunday_active'] ? 'true' : 'false');
                    $application_control->setAttribute('Mon', $data['campaign_data']['is_stay_monday_active'] ? 'true' : 'false');
                    $application_control->setAttribute('Tue', $data['campaign_data']['is_stay_tuesday_active'] ? 'true' : 'false');
                    $application_control->setAttribute('Wed', $data['campaign_data']['is_stay_wednesday_active'] ? 'true' : 'false');
                    $application_control->setAttribute('Thu', $data['campaign_data']['is_stay_thursday_active'] ? 'true' : 'false');
                    $application_control->setAttribute('Fri', $data['campaign_data']['is_stay_friday_active'] ? 'true' : 'false');
                    $application_control->setAttribute('Sat', $data['campaign_data']['is_stay_saturday_active'] ? 'true' : 'false');

                    if ($hotel_products_list['status'] == 'no_inventory') {
                        $status = $hotels_data->appendChild($doc->createElement('Status', 'No inventory loaded for the requested dates'));
                        $status->setAttribute('code', '842');
                        continue;
                    }

                    if (is_array($hotel_products_list)) {
                        $rate_amounts = $hotels_data->appendChild($doc->createElement('RateAmounts'));
                        $rate_amounts->setAttribute('Currency', $hotel_products_list['currency']);
                        //$rate_amounts->setAttribute('TaxesIncluded', 'true');
                        // Check additional List
                        if (!empty($hotel_products_list['rates'])) {
                            foreach ($hotel_products_list['rates'] as $rates_key => $rates_single) {
                                $base = $rate_amounts->appendChild($doc->createElement('Base'));
                                $base->setAttribute('OccupancyCode', $rates_single['occupancy_code']);
                                // $base->setAttribute('AmountBeforeTax', $rates_single['before_price']);
                                // $base->setAttribute('AmountAfterTax', $rates_single['after_price']);
                                $base->setAttribute('Amount', $rates_single['after_price']);
                            }
                        }

                        // Check additional List
                        if (!empty($hotel_products_list['additional'])) {
                            foreach ($hotel_products_list['additional'] as $additional_key => $additional_single) {
                                $additional = $rate_amounts->appendChild($doc->createElement('Additional'));
                                $additional->setAttribute('OccupancyCode', $additional_single['occupancy_code']);
                                // $base->setAttribute('AmountBeforeTax', $additional_single['before_price']);
                                // $base->setAttribute('AmountAfterTax', $additional_single['price']);
                                $additional->setAttribute('Amount', $additional_single['price']);
                            }
                        }

                        /// disable because noy suitable
                        // if (!empty($hotel_products_list['MealPlan']))
                        // {
                        //   $MealPlans = $rate_amounts->appendChild($doc->createElement('MealPlans'));
                        //   foreach ($hotel_products_list['MealPlan'] as $meal_list) {
                        //     $MealPlan = $MealPlans->appendChild($doc->createElement('MealPlan'));
                        //     $MealPlan->setAttribute('MealPlanCode', $meal_list['occupancy_code']);
                        //     $MealPlan->setAttribute('Included', $meal_list['included']);
                        //     $MealPlan->setAttribute('Amount', $meal_list['price']);
                        //   }
                        // }
                        /// disable because noy suitable
                        // if($hotel_products_list['discount_amount'] || $hotel_products_list['discount_rate'])
                        // {
                        //   $discount = $rate_amounts->appendChild($doc->createElement('Discount'));
                        //   if($hotel_products_list['discount_amount']){
                        //     $discount->setAttribute('Amount', $hotel_products_list['discount_amount']);
                        //   }
                        //   elseif ($hotel_products_list['discount_rate']) {
                        //     $discount->setAttribute('Percent', $hotel_products_list['discount_rate']);
                        //   }
                        // }

                        $availability = $hotels_data->appendChild($doc->createElement('Availability'));
                        $availability->setAttribute('Master', $hotel_products_list['blackout'] ? 'Closed' : 'Open');
                        // $availability->setAttribute('Arrival',$hotel_products_list['Availability']['arrival']);
                        // $availability->setAttribute('Departure',$hotel_products_list['Availability']['departure']);

                        $booking_limit = $hotels_data->appendChild($doc->createElement('BookingLimit'));
                        // $booking_limit->appendChild($doc->createElement('Sold'));
                        // $booking_limit->appendChild($doc->createElement('FreeSale'));
                        // $base_allotment = $booking_limit->appendChild($doc->createElement('BaseAllotment'));
                        // // $base_allotment->appendChild($doc->createElement('Sold'));
                        // // $base_allotment->appendChild($doc->createElement('RealeaseDays'));
                        // //$base_allotment->appendChild($doc->createElement('Allotment'));
                        // $base_allotment->setAttribute('Allotment',$hotel_products_list['stock']);

                        $transient_allotment = $booking_limit->appendChild($doc->createElement('TransientAllotment'));
                        // $transient_allotment->appendChild($doc->createElement('Sold'));
                        // $transient_allotment->appendChild($doc->createElement('ReleaseDays'));
                        $transient_allotment->setAttribute('Allotment', $hotel_products_list['stock']);

                        $booking_rules = $booking_limit->appendChild($doc->createElement('BookingRules'));
                        //$booking_rules->appendChild($doc->createElement('MinAdvancedBookingOffset',$hotel_products_list['MinAdvancedBookingOffset']));
                        /// disable because noy suitable
                        // if ($hotel_products_list['MinAdvancedBookingOffset']) {
                        //   $booking_rules->appendChild($doc->createElement('MinAdvancedBookingOffset',$hotel_products_list['MinAdvancedBookingOffset']));
                        // }
                        // if ($hotel_products_list['MaxAdvancedBookingOffset']) {
                        //   $booking_rules->appendChild($doc->createElement('MaxAdvancedBookingOffset',$hotel_products_list['MaxAdvancedBookingOffset']));
                        // }
                        // $booking_rules->appendChild($doc->createElement('MinLoSThrough',$hotel_products_list['MinLoSThrough']));
                        // $booking_rules->appendChild($doc->createElement('MinLoSOnArrival',$hotel_products_list['MinLoSOnArrival']));
                        // $booking_rules->appendChild($doc->createElement('MaxLoSOnArrival',$hotel_products_list['MaxLoSOnArrival']));
                        //$booking_rules->appendChild($doc->createElement('MaxLoSThrough',$hotel_products_list['MaxLoSThrough']));
                        // Disable because not supported in DC
                        //$booking_rules->appendChild($doc->createElement('Description', htmlspecialchars(utf8_encode($hotel_products_list['Description']))));
                    }
                }
            } else {
                $status = $hotels_data_set->appendChild($doc->createElement('Status', 'No inventory loaded for the requested dates'));
                $status->setAttribute('code', 842);
            }

            $doc->appendChild($response_node);

            $response = $doc->saveXML($doc, LIBXML_NOEMPTYTAG);

            $this->output($response);
        } catch (Kohana_Exception $e) {
            if ($this->auth($auth) == FALSE) {
                $error[] = array(
                    'Type' => 3,
                    'Code' => '392',
                    'Desc' => 'Invalid hotel code.'
                );

                return $this->errors($error, $data);
            }
        }
    }

    public function HotelARIUpdateRQ($xmls) {

        try {
            if (is_array($xmls['data'])) {
                foreach ($xmls['data'] as $key => $xml) {
                    $startDateUpdateVacation = $xml['ApplicationControl']['Start'];
                    $endDateUpdateVacation = $xml['ApplicationControl']['End'];
                    $room_update = array();
                    $hotel_id = $xml['HotelCode'];
                    $room_id = $xml['ProductReference']['InvTypeCode'];
                    $campaign_id = $xml['ProductReference']['RatePlanCode'];

                    // Check room and rate availabilty
                    $check_data = DB::select(array(DB::expr('COUNT(campaigns_rooms.campaign_id)'), 'count'))
                                    ->from('campaigns_rooms')
                                    ->where('campaigns_rooms.campaign_id', '=', $campaign_id)
                                    ->where('campaigns_rooms.room_id', '=', $room_id)
                                    ->execute()
                                    ->get('count') > 0;

                    if ($check_data) {
                        // Create dates array
                        $start_date_timestamp = strtotime($xml['ApplicationControl']['Start']);
                        $end_date_timestamp = strtotime($xml['ApplicationControl']['End']);

                        $dates = array();
                        for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                            $dates[date('Y-m-d', $i)] = date('w,Y-m-d', $i);
                        }

                        // Check false application control
                        $ApplicationControls = array();
                        foreach ($xml['ApplicationControl'] as $day => $ApplicationControl) {
                            if (strtolower($day) != 'start' AND strtolower($day) != 'end') {
                                $ApplicationControls[$day] = $ApplicationControl == 'true' ? 1 : 0;
                            }
                        }

                        // Load Currency Hotel
                        $currency = DB::select('currency_id')->from('hotels')
                                ->where('id', '=', $hotel_id)
                                ->as_object()
                                ->execute()
                                ->current();

                        $currency_id = $currency ? $currency->currency_id : 1;

                        // Load id currency request
                        $currency_request = $xml['RateAmounts']['Currency'] ?
                                ORM::factory('currency')
                                        ->where('code', '=', $xml['RateAmounts']['Currency'])
                                        ->find()->as_array() : $currency_id;

                        // Factory exchange
                        $exchange = Model::factory('exchange');

                        // Changed items 
                        $changed_prev_items = array();
                        $changed_items = array();

                        // Item Price
                        $base = $xml['RateAmounts']['Base'];
                        $item_price = round($base['base_after_tax'] * ($exchange->to($currency_request['id'], $currency_id)), 0);

                        // Additional price
                        $extrabed_price = $xml['RateAmounts']['Additional'];

                        if (!empty($extrabed_price)) {
                            $extrabed_price = round($extrabed_price['base_after_tax'] * ($exchange->to($currency_request['id'], $currency_id)), 0);
                        } else {
                            $extrabed_price = 0;
                        }
                        /*
                         * Meal plan and extrabed price
                         */
                        // // Meal Plans
                        // $MealPlan = $xml['RateAmounts']['MealPlans'];
                        // $MealPlanCode = $MealPlan['MealPlanCode'];
                        // switch ($MealPlanCode) {
                        //   case '11':
                        //     $room_update['is_breakfast_included'] = $MealPlan['Included'];
                        //   case '14':
                        //     break;
                        //     $room_update['is_breakfast_included'] = $MealPlan['Included'];
                        //     $room_update['adult_breakfast_price'] = round($MealPlan['Amount'] * ($exchange->to($currency_request['id'], $currency_id)), 0);
                        //     break;
                        //   case '19':
                        //     $room_update['is_breakfast_included'] = $MealPlan['Included'];
                        //     $room_update['adult_breakfast_price'] = round($MealPlan['Amount'] * ($exchange->to($currency_request['id'], $currency_id)), 0);
                        //     break;
                        //   case '20':
                        //     $room_update['is_breakfast_included'] = $MealPlan['Included'];
                        //     $room_update['adult_breakfast_price'] = round($MealPlan['Amount'] * ($exchange->to($currency_request['id'], $currency_id)), 0);
                        //     break;
                        //   default:
                        //     $Included = NULL;
                        //     break;
                        // }
                        // // Start transaction
                        // Database::instance()->begin();
                        // // Save extrabed price and breakfast price if Meal plan is not included
                        // if(!is_null($Included) || !empty($extrabed_price))
                        // {
                        //   DB::update('rooms')
                        //     ->set($room_update)
                        //     ->where('id', '=', $room_id)
                        //     ->execute();
                        // }

                        foreach ($dates as $date) {
                            // Check ApplicationControl false value
                            $is_campaign_blackout = 0;

                            $day = explode(",", $date);
                            $date = end($day);
                            foreach ($ApplicationControls as $date_application_control => $ApplicationControl) {
                                if (reset($day) == $date_application_control AND ! $ApplicationControl) {
                                    $is_campaign_blackout = 1;
                                }
                            }

                            /*
                             * Update Item
                             */
                            // Get the campaigns
                            $campaign = ORM::factory('campaign')
                                    ->where('id', '=', $campaign_id)
                                    ->find();

                            // If item  and items already exist
                            if ($campaign) {
                                // Get the item
                                $item = DB::select()
                                        ->from('items')
                                        ->where('room_id', '=', $room_id);

                                if (!$campaign->is_default) {
                                    $item = $item->where('campaign_id', '=', $campaign_id);
                                } else {
                                    $item = $item->where('campaign_id', '=', 0);
                                }

                                $item = $item->where('date', '=', $date)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $values = array(
                                    'room_id' => $room_id,
                                    'campaign_id' => $campaign_id,
                                    'date' => $date,
                                    'price' => isset($base['base_after_tax']) ? $item_price : 0,
                                    'net_price' => 0,
                                    'stock' => isset($xml['BookingLimit']['TransientAllotment']['Allotment']) ? $xml['BookingLimit']['TransientAllotment']['Allotment'] : 0,
                                    'minimum_night' => !empty($xml['MinLoSThrough']) ? $xml['MinLoSThrough'] : 1,
                                    'is_campaign_blackout' => $is_campaign_blackout,
                                    'is_blackout' => strtolower($xml['Availability']['Master']) == 'closed' ? 1 : 0,
                                    'application_control' => serialize($ApplicationControls),
                                    'extrabed_item_price' => isset($xml['RateAmounts']['Additional']) ? $extrabed_price : 1.0,
                                    'is_campaign_prices' => $campaign->is_default ? 0 : 1,
                                );

                                // Check item campaign id exist

                                if ($item) {
                                    $values['extrabed_item_price'] = isset($xml['RateAmounts']['Additional']) ? $extrabed_price : $item->extrabed_item_price;
                                    $values['minimum_night'] = $values['minimum_night'] > 1 ? $values['minimum_night'] : $item->minimum_night;
                                    $values['stock'] = isset($xml['BookingLimit']['TransientAllotment']['Allotment']) ? $values['stock'] : $item->stock;
                                    $values['minimum_night'] = $values['minimum_night'] ? $item->minimum_night : $values['minimum_night'];
                                    $values['price'] = isset($base['base_after_tax']) ? $item_price : $item->price;
                                    $values['campaign_id'] = $campaign->is_default ? 0 : $campaign_id;

                                    // Update
                                    DB::update('items')
                                            ->set($values)
                                            ->where('id', '=', $item->id)
                                            ->execute();
                                } else {
                                    // campaign id
                                    $values['campaign_id'] = $campaign->is_default ? 0 : $campaign_id;

                                    // Insert
                                    DB::insert('items')
                                            ->columns(array_keys($values))
                                            ->values(array_values($values))
                                            ->execute();
                                }

                                // Clone item to write log
                                if ($item) {
                                    $prev_item = clone $item;
                                } else {
                                    $prev_item = new stdClass;
                                    $prev_item->stock = NULL;
                                    $prev_item->price = NULL;
                                    $prev_item->net_price = NULL;
                                    $prev_item->minimum_night = NULL;
                                    $prev_item->is_blackout = NULL;
                                    $prev_item->is_campaign_blackout = NULL;
                                }

                                // Get the item
                                $item = DB::select()
                                        ->from('items')
                                        ->where('room_id', '=', $room_id);

                                // Check campaign default
                                if ($campaign->is_default) {
                                    $item = $item->where('campaign_id', '=', 0);
                                } else {
                                    $item = $item->where('campaign_id', '=', $campaign_id);
                                }

                                $item = $item->where('date', '=', $date)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                // Get room
                                $room = DB::select()
                                        ->from('rooms')
                                        ->where('id', '=', $room_id)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                // Set log values
                                $log_values = array(
                                    'admin_id' => A1::instance()->get_user()->id,
                                    'hotel_id' => $room->hotel_id,
                                    'action' => 'Changed Item',
                                    'room_texts' => serialize(DB::select()
                                                    ->from('room_texts')
                                                    ->where('room_id', '=', $room->id)
                                                    ->execute()
                                                    ->as_array('language_id')),
                                    'text' => 'Date: :date. '
                                    . 'Stock: :prev_stock -> :stock. '
                                    . 'Price: :prev_price -> :price. '
                                    . 'Minimum Night: :prev_minimum_night -> :minimum_night. '
                                    . 'Stop Sell: :prev_is_blackout -> :is_blackout. '
                                    . 'Stop Promotion: :prev_is_campaign_blackout -> :is_campaign_blackout.',
                                    'data' => serialize(array(
                                        ':prev_stock' => $prev_item->stock ? $prev_item->stock : 'None',
                                        ':prev_price' => $prev_item->price ? Num::display($prev_item->price) : 'None',
                                        ':prev_minimum_night' => $prev_item->minimum_night ? $prev_item->minimum_night : 'None',
                                        ':prev_is_blackout' => $prev_item->is_blackout ? 'Yes' : 'No',
                                        ':prev_is_campaign_blackout' => $prev_item->is_campaign_blackout ? 'Yes' : 'No',
                                        ':date' => date('M j, Y', strtotime($item->date)),
                                        ':stock' => $item->stock,
                                        ':price' => Num::display($item->price),
                                        ':minimum_night' => $item->minimum_night,
                                        ':is_blackout' => $item->is_blackout ? 'Yes' : 'No',
                                        ':is_campaign_blackout' => $item->is_campaign_blackout ? 'Yes' : 'No',
                                    )),
                                    'created' => time(),
                                );

                                // Write log
                                DB::insert('logs')
                                        ->columns(array_keys($log_values))
                                        ->values(array_values($log_values))
                                        ->execute();

                                // Track changed items
                                if ($prev_item->stock !== $item->stock OR
                                        $prev_item->price !== $item->price OR
                                        $prev_item->minimum_night !== $item->minimum_night OR
                                        $prev_item->is_blackout !== $item->is_blackout OR
                                        $prev_item->is_campaign_blackout !== $item->is_campaign_blackout) {
                                    $changed_prev_items[$date] = $prev_item;
                                    $changed_items[$date] = $item;
                                }

                                $vacation_triger = array(
                                    'hotel_id' => $room->hotel_id,
                                    'room_id' => $room->id,
                                    'campaign_id' => NULL,
                                    // 'start_date' =>$booking_data['check_in'], 
                                    // 'end_date' =>$booking_data['check_out'],
                                    'item_id' => NULL,
                                    'start_date' => $startDateUpdateVacation,
                                    'end_date' => $endDateUpdateVacation,
                                    'type_trigger' => 7,
                                    'status' => 0
                                );

                                //insert data vacation trigger into database
                                Controller_Vacation_Trigger::insertData($vacation_triger);
                            } else {
                                // Rollback transaction
                                Database::instance()->rollback();
                                // Add error notice
                                $error['8'] = '38';
                                break;
                            }

                            /*
                             * /// disable because not suitable
                             */
                            //   //Determine room available
                            //   $hotel_no_arrival = DB::select()
                            //       ->from('hotel_no_arrivals')
                            //       ->where('hotel_id', '=', $hotel_id)
                            //       ->where('date', '=', $date)
                            //       ->execute()
                            //       ->get('date');
                            //   // Master Arrival
                            //   if (strtolower($xml['Availability']['Arrival']) == 'closed') {
                            //     if(!$hotel_no_arrival)
                            //     {
                            //       $no_arrival_data = array(
                            //         'hotel_id' =>$hotel_id,
                            //         'date' =>$date,
                            //         );
                            //     DB::insert('hotel_no_arrivals', array_keys($no_arrival_data))
                            //       ->values($no_arrival_data)
                            //       ->execute();
                            //     }
                            //   }
                            //   elseif(strtolower($xml['Availability']['Arrival']) == 'open')
                            //   {
                            //     // Delete
                            //     DB::delete('hotel_no_arrivals')
                            //       ->where('hotel_id', '=', $hotel_id)
                            //       ->where('date', '=', $date)
                            //       ->execute();
                            //   }
                            //   // Get no departure
                            //   $hotel_no_departure = DB::select()
                            //     ->from('hotel_no_departures')
                            //     ->where('hotel_id', '=', $hotel_id)
                            //     ->where('date', '=', $date)
                            //     ->execute()
                            //     ->get('date');
                            //   // Master Departure
                            //   if (strtolower($xml['Availability']['Departure']) == 'closed') {
                            //     if(!$hotel_no_departure)
                            //     {
                            //       $no_departure_data = array(
                            //         'hotel_id' =>$hotel_id,
                            //         'date' =>$date,
                            //         );
                            //     DB::insert('hotel_no_departures', array_keys($no_departure_data))
                            //       ->values($no_departure_data)
                            //       ->execute();
                            //     }
                            //   }
                            //   elseif (strtolower($xml['Availability']['Departure']) == 'open') {
                            //     // Delete
                            //     DB::delete('hotel_no_departures')
                            //       ->where('hotel_id', '=', $hotel_id)
                            //       ->where('date', '=', $date)
                            //       ->execute();
                            //   }
                            // }

                            /*
                             *  Email for item changed
                             */

                            // Get QA admins emails
                            $qa_emails = ORM::factory('admin')
                                    ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                                    ->where('admins.role', '=', 'quality-assurance')
                                    ->where('admins_hotels.hotel_id', '=', $hotel_id)
                                    ->find_all()
                                    ->as_array('email', 'name');

                            // If there is QA admins
                            if (count($qa_emails) > 0) {
                                ksort($changed_prev_items);
                                ksort($changed_items);

                                // Create email message
                                $message = Kostache::factory('item/qa/manage')
                                        ->set('admin_username', A1::instance()->get_user()->username)
                                        ->set('admin_name', A1::instance()->get_user()->name)
                                        ->set('room_name', ORM::factory('room_text')->where('room_texts.room_id', '=', $room_id)->where('room_texts.language_id', '=', 1)->find()->name)
                                        ->set('changed_prev_items', $changed_prev_items)
                                        ->set('changed_items', $changed_items)
                                        ->render();

                                // Send email
                                Email::factory(strtr(':hotel_name - Changed Item', array(
                                            ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                                )), $message, 'text/html')
                                        ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                        ->to($qa_emails)
                                ; //->send();
                            }

                            /*
                             * Update campaign
                              /// disable because not suitable
                             */
                            // Early days or days in advanced
                            // if(!empty($xml['MinAdvancedBookingOffset']))
                            // {
                            //   $within_days_of_arrival = 0;
                            //   $days_in_advance = $xml['MinAdvancedBookingOffset'];
                            //   $type = 2;
                            // }
                            // else
                            // if (!empty($xml['MaxAdvancedBookingOffset'])) 
                            // {
                            //   $days_in_advance = $xml['MaxAdvancedBookingOffset'];
                            //   $type = 2;
                            // }
                            // else
                            // {
                            //   $type = 1;
                            // }
                            // $campaign_data = array(
                            //   'id' => $campaign_id,
                            //   'type' => $type,
                            //   'discount_rate' => !empty($xml['RateAmounts']['Amount']) ? 0 : $campaign->discount_rate,
                            //   'discount_amount' => !empty($xml['RateAmounts']['Amount']) ? round($xml['RateAmounts']['Amount'] * ($exchange->to($currency_request['id'], $currency->currency_id)))
                            //     : round($campaign->discount_amount * ($exchange->to($currency_request['id'], $currency->currency_id))),
                            //   //'within_days_of_arrival' => !empty($within_days_of_arrival) ? $within_days_of_arrival : $campaign->within_days_of_arrival,
                            //   'days_in_advance' => !empty($days_in_advance) ? $days_in_advance : $campaign->days_in_advance,
                            //   'minimum_number_of_nights' => !empty($xml['MinLoSThrough']) ? $xml['MinLoSThrough'] : $campaign->minimum_number_of_nights,
                            //   );
                            // // Clone as prev campaign
                            // $prev_campaign = clone $campaign;
                            // // Get room names
                            // $prev_room_names = $prev_campaign->rooms
                            //   ->select(array('room_texts.name', 'name'))
                            //   ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                            //   ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
                            //   ->find_all()
                            //   ->as_array('id', 'name');
                            // // Get prev cancellation
                            // $prev_cancellation = $prev_campaign->cancellations
                            //   ->select(
                            //     array('rule_1.name', 'rule_1_name'),
                            //     array('rule_2.name', 'rule_2_name'),
                            //     array('rule_3.name', 'rule_3_name')
                            //   )
                            //   ->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'level_1_cancellation_rule_id')
                            //   ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'level_2_cancellation_rule_id')
                            //   ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'level_3_cancellation_rule_id')
                            //   ->find();
                            // // Update campaign
                            // $campaign = $campaign->update_campaign($campaign_data);
                            // if(!$campaign)
                            // {
                            //   // Rollback transaction
                            //   Database::instance()->rollback();
                            //   // Add error notice
                            //   $error['842'] = '3';
                            // }
                            // else
                            // {
                            //   // Get campaign values for logging
                            //   $log_values = $campaign->as_log($prev_campaign, $prev_room_names, $prev_cancellation);
                            //   // Write log
                            //   ORM::factory('log')
                            //     ->values(array(
                            //       'admin_id' => A1::instance()->get_user()->id,
                            //       'hotel_id' => $hotel_id,
                            //       'action' => 'Edited Promotion',
                            //       'room_texts' => NULL,
                            //       'text' => 'Room Names: :prev_room_names -> :room_names. '
                            //         .'Benefit: :prev_benefit -> :benefit '
                            //         .'Booking Dates: :prev_booking_start_date - :prev_booking_end_date -> :booking_start_date - :booking_end_date. '
                            //         .'Stay Dates: :prev_stay_start_date - :prev_stay_end_date -> :stay_start_date - :stay_end_date. '
                            //         .'Cancellation Policy: :prev_cancellation -> :cancellation.'
                            //         .'Rategain Update: :prev_discount_rate - :prev_discount_amount - :prev_days_in_advance - prev_minimum_number_of_nights -> :discount_rate - :discount_amount - :days_in_advance - minimum_number_of_nights.',
                            //       'data' => serialize(array(
                            //         ':prev_room_names' => Arr::get($log_values, 'prev_room_names'),
                            //         ':prev_benefit' => Arr::get($log_values, 'prev_benefit'),
                            //         ':prev_booking_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':prev_booking_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                            //         ':prev_stay_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':prev_stay_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                            //         ':prev_cancellation' => Arr::get($log_values, 'prev_cancellation'),
                            //         ':prev_discount_rate' => Arr::get($log_values, 'prev_discount_rate'),
                            //         ':prev_discount_amount' => Arr::get($log_values, 'prev_discount_amount'),
                            //         ':prev_days_in_advance' => Arr::get($log_values, 'prev_days_in_advance'),
                            //         ':prev_minimum_number_of_nights' => Arr::get($log_values, 'prev_minimum_number_of_nights'),
                            //         ':room_names' => Arr::get($log_values, 'prev_room_names'),
                            //         ':benefit' => Arr::get($log_values, 'prev_benefit'),
                            //         ':booking_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':booking_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                            //         ':stay_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':stay_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                            //         ':cancellation' => Arr::get($log_values, 'prev_cancellation'),
                            //         ':discount_rate' => Arr::get($log_values, 'discount_rate'),
                            //         ':discount_amount' => Arr::get($log_values, 'discount_amount'),
                            //         ':days_in_advance' => Arr::get($log_values, 'days_in_advance'),
                            //         ':minimum_number_of_nights' => Arr::get($log_values, 'minimum_number_of_nights'),
                            //       )),
                            //       'created' => time(),
                            //     ))
                            //     ->create();
                            //   // Get QA admins emails
                            //   $qa_emails = ORM::factory('admin')
                            //     ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            //     ->where('admins.role', '=', 'quality-assurance')
                            //     ->where('admins_hotels.hotel_id', '=', $hotel_id)
                            //     ->find_all()
                            //     ->as_array('email', 'name');
                            //   // If there is QA admins
                            //   if (count($qa_emails) > 0)
                            //   {
                            //     // Send email
                            //     Email::factory(strtr(':hotel_name - Edited Promotion', array(
                            //         ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                            //       )), strtr(
                            //       "Action: Edited Promotion.\n"
                            //       ."By: :admin_username (:admin_name).\n"
                            //       ."Room Names: :prev_room_names -> :room_names.\n"
                            //       ."Benefit: :prev_benefit -> :benefit\n"
                            //       ."Booking Dates: :prev_booking_start_date - :prev_booking_end_date -> :booking_start_date - :booking_end_date.\n"
                            //       ."Stay Dates: :prev_stay_start_date - :prev_stay_end_date -> :stay_start_date - :stay_end_date.\n"
                            //       ."Cancellation Policy: :prev_cancellation -> :cancellation.\n"
                            //       ."Rategain Update:\n"
                            //       ."Discount Rate: :prev_discount_rate -> :discount_rate.\n"
                            //       ."Discount Amount: :prev_discount_amount -> :discount_amount.\n"
                            //       ."Days In Advanced: :prev_days_in_advance -> :days_in_advance.\n"
                            //       ."Minimum Number Of Nights: :prev_minimum_number_of_nights -> :minimum_number_of_nights.",
                            //       array(
                            //         ':admin_username' => A1::instance()->get_user()->username,
                            //         ':admin_name' => A1::instance()->get_user()->name,
                            //         ':prev_room_names' => Arr::get($log_values, 'prev_room_names'),
                            //         ':prev_benefit' => Arr::get($log_values, 'prev_benefit'),
                            //         ':prev_booking_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':prev_booking_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                            //         ':prev_stay_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':prev_stay_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                            //         ':prev_cancellation' => Arr::get($log_values, 'prev_cancellation'),
                            //         ':prev_discount_rate' => Arr::get($log_values, 'prev_discount_rate'),
                            //         ':prev_discount_amount' => Arr::get($log_values, 'prev_discount_amount'),
                            //         ':prev_days_in_advance' => Arr::get($log_values, 'prev_days_in_advance'),
                            //         ':prev_minimum_number_of_nights' => Arr::get($log_values, 'prev_minimum_number_of_nights'),
                            //         ':room_names' => Arr::get($log_values, 'prev_room_names'),
                            //         ':benefit' => Arr::get($log_values, 'prev_benefit'),
                            //         ':booking_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':booking_end_date' => Arr::get($log_values, 'prev_booking_end_date'),
                            //         ':stay_start_date' => Arr::get($log_values, 'prev_booking_start_date'),
                            //         ':stay_end_date' => Arr::get($log_values, 'stay_end_date'),
                            //         ':cancellation' => Arr::get($log_values, 'prev_cancellation'),
                            //         ':discount_rate' => Arr::get($log_values, 'discount_rate'),
                            //         ':discount_amount' => Arr::get($log_values, 'discount_amount'),
                            //         ':days_in_advance' => Arr::get($log_values, 'days_in_advance'),
                            //         ':minimum_number_of_nights' => Arr::get($log_values, 'minimum_number_of_nights'),
                            //       )))
                            //       ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                            //       ->to($qa_emails)
                            //       ;//->send();
                            //   }
                        }
                        // Commit transaction
                        Database::instance()->commit();
                    } else {
                        // Rollback transaction
                        Database::instance()->rollback();
                        // Add error notice
                        $error['732'] = '8';
                    }
                }
            }
        } catch (Kohana_Exception $e) {
            // Rollback transaction
            Database::instance()->rollback();
            // Add error notice
            $error['188'] = '12';
        }

        // Check error exist
        if (!empty($error)) {
            $this->errors($error, $xmls);
        } else {
            // Build response
            return $this->HotelARIUpdateRS($xmls);
        }
    }

    public function HotelARIUpdateRS($xml) {
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $response_node = $doc->createElement((substr($xml['head'], 0, -2) . 'RS'));
        $response_node->setAttribute('xmlns', $xml['xmlns']);
        $response_node->setAttribute('TimeStamp', $xml['TimeStamp']);
        $response_node->setAttribute('Target', $xml['Target']);
        $response_node->setAttribute('Version', $xml['Version']);

        $response_node->appendChild($doc->createElement('Success'));
        $doc->appendChild($response_node);

        $response = $doc->saveXML();

        $this->output($response);
    }

    public function PingRQ($xml) {
        $data = (string) $xml->EchoData;
        return $this->PingRS($data);
    }

    public function PingRS($data) {
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $response_node = $doc->createElement((substr($data['head'], 0, -2) . 'RS'));
        $response_node->setAttribute('xmlns', $data['xmlns']);
        $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
        $response_node->setAttribute('Target', $data['Target']);
        $response_node->setAttribute('Version', $data['Version']);

        $response_node->appendChild($doc->createElement('Success'));
        $response_node->appendChild($doc->createElement('EchoData', $data));
        $doc->appendChild($response_node);

        $response = $doc->saveXML();

        // Log::instance()->add(Log::INFO, "Rategain API Response: Availability");
        // Log::instance()->add(Log::INFO, $response);

        $this->output($response);
    }

    public function output($response) {
        CustomLog::factory()->add(4, 'DEBUG', "Rategain API Response:");
        CustomLog::factory()->add(4, 'DEBUG', $response);
        // Logout account
        A1::instance()->logout();

        $this->response
                ->headers('Content-Type', 'text/xml')
                ->body($response);
    }

    public function action_request() {
        // Set time limit
        set_time_limit(60);

        $xml_location = file_get_contents('php://input');
        //$xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/list.xml';

        CustomLog::factory()->add(4, 'DEBUG', "Rategain API Request: Availability");
        CustomLog::factory()->add(4, 'DEBUG', $xml_location);

        if (!$xml_location) {
            $node_head = array(
                'xmlns' => '',
                'TimeStamp' => '',
                'Target' => '',
                'Version' => '',
            );

            $error['450'] = '2';
            $this->errors($error, $node_head);
        } else {
            try {
                /*
                  $data = file_get_contents($xml_location);
                  $xml_location = trim( stripslashes( $data ));
                 */
                $xml = new SimpleXMLElement($xml_location);

                $head = $xml->getName();

                $data = $this->checker($head, $xml);

                if ($data) {
                    //Called function
                    $response = call_user_func(array($this, $head), $data);
                }
            } catch (Kohana_Exception $e) {
                $response = $this->create_error($e->getMessage());
            }
        }
    }

}
