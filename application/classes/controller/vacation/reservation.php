<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Reservation extends Controller {
    /*
     * Module : UIDD, IPG
     *
     * List View and template:
     * Email/Booking
     * Pdf/voucher
     *
     * List Frontend Model:
     * Admin : get_email
     * Hotel :search_hotel, search_campaign, get_campaign
     * Campaign: get_default_campaign_id
     * Item : decrease_item_stock, increase_item_stock,Surcharge
     * increase_item_sold :get_surcharges
     * Extrabed :get_extrabed
     * Language : get_languange_by_id
     * Timezone  : timezone identifier
     * Hotel: get_hotel_by_id
     * Room Get By Id
     * Booking Data
     * Booking Room :create_booking_room
     * Currency:get_currency_by_code
     * Cancellation: get_cancellation
     * Booking: update_booking_pending, create_booking
     * Setting  : get
     * Exchane  : To
     */

    public function checker($transaction, $head, $body, $xmlns, $request_id) {
        $data = array();

        $OTA_HotelResRQ = $body->$transaction->attributes();

        $node_head = array(
            'head' => $transaction,
            'request_id' => $request_id,
            'xmlns' => $xmlns,
            'TimeStamp' => (string) $OTA_HotelResRQ->TimeStamp,
            'PrimaryLangID' => (string) $OTA_HotelResRQ->PrimaryLangID,
            'Target' => (string) $OTA_HotelResRQ->Target,
            'Version' => (string) $OTA_HotelResRQ->Version,
            'EchoToken' => (string) $body->$transaction->attributes()->EchoToken,
            //Auth for booking
            'User' => (string) $head->ComponentInfo->attributes()->User,
            'Pwd' => (string) $head->ComponentInfo->attributes()->Pwd,
        );

        $auth = array(
            'User' => (string) $head->ComponentInfo->attributes()->User,
            'Pwd' => (string) $head->ComponentInfo->attributes()->Pwd,
            'key' => '',
            'source' => '',
        );

        // Check Target Environment
        // $vacation_environment = strtolower((string)$OTA_HotelResRQ->Target) == 'production' ? 'production' : 'development';
        $vacation_environment = strtolower((string) $OTA_HotelResRQ->Target) == 'production' ? Kohana::PRODUCTION : Kohana::DEVELOPMENT;


        // Check node head null
        if (empty($node_head['xmlns']) || empty($node_head['TimeStamp']) || empty($node_head['PrimaryLangID']) || empty($node_head['Target']) || empty($node_head['Version']) || empty($node_head['EchoToken'])) {
            $error['Empty_Property']['type'] = '2';
            $error['Empty_Property']['code'] = '321';
        }
        // Check Target Environment
        elseif (Kohana::$environment != $vacation_environment) {
            $error['Authorization']['type'] = '6';
            $error['Authorization']['code'] = '146';
        }
        // 
        // Check Auth null request
        elseif (empty($auth['User']) || empty($auth['Pwd'])) {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '321';
        } else {
            // Check Auth
            if (Controller_Vacation_Static::auth($auth) == FALSE) {
                $error['Authentification']['type'] = '4';
                $error['Authentification']['code'] = '9900';
            } else {
                $error = array();

                $HotelReservations = $body->$transaction->HotelReservations->HotelReservation;

                $RoomStaysNum = 0;
                foreach ($HotelReservations as $key => $HotelReservation) {
                    // $RoomStays = $HotelReservation->RoomStays->RoomStay;
                    $RoomStays = $HotelReservation->RoomStays;

                    // define guest data
                    $guestcountdata = array();
                    foreach ($RoomStays->RoomStay as $keyRoomStaySingular => $RoomStaySingular) {
                        // $RoomStaysNum++;
                        $RoomTypes = $RoomStaySingular->RoomTypes->RoomType;
                        $RatePlans = $RoomStaySingular->RatePlans->RatePlan;
                        $RoomRates = $RoomStaySingular->RoomRates->RoomRate;
                        $RequiredPayment = $RoomStaySingular->DepositPayments->RequiredPayment;

                        // Load Campaign ID
                        $campaign_id = (string) $RatePlans->attributes()->RatePlanCode;

                        // Data fill
                        $data[$campaign_id] = array(
                            'RoomStayReservation' => (string) $HotelReservation->attributes()->RoomStayReservation,
                            // 'CreateDateTime' => (string)$HotelReservation->attributes()->CreateDateTime,
                            'Type' => (string) $HotelReservation->UniqueID->attributes()->Type,
                            'ID' => (string) $HotelReservation->UniqueID->attributes()->ID,
                            'RoomTypeCode' => (string) $RoomTypes->attributes()->RoomTypeCode,
                            'RatePlan' => (string) $RatePlans->attributes()->RatePlanCode,
                            'MealPlanCodes' => (string) $RatePlans->MealsIncluded->attributes()->MealPlanCodes,
                            'Start' => (string) $RoomStaySingular->TimeSpan->attributes()->Start,
                            'End' => (string) $RoomStaySingular->TimeSpan->attributes()->End,
                            // 'RPH' => (string)$RequiredPayment->AcceptedPayments->AcceptedPayment->attributes()->RPH,
                            // 'CardType' => (string)$RequiredPayment->AcceptedPayments->AcceptedPayment->PaymentCard->attributes()->CardType,
                            // 'CardCode' => (string)$RequiredPayment->AcceptedPayments->AcceptedPayment->PaymentCard->attributes()->CardCode,
                            // 'CardNumber' => (string)$RequiredPayment->AcceptedPayments->AcceptedPayment->PaymentCard->attributes()->CardNumber,
                            // 'ExpireDate' => (string)$RequiredPayment->AcceptedPayments->AcceptedPayment->PaymentCard->attributes()->ExpireDate,
                            // 'Amount' => (string)$RequiredPayment->AmountPercent->attributes()->Amount,
                            // 'DecimalPlaces' => (string)$RequiredPayment->AmountPercent->attributes()->DecimalPlaces,
                            // 'AbsoluteDeadline' => (string)$RequiredPayment->Deadline->attributes()->AbsoluteDeadline,
                            // 'CurrencyCode' => (string)$RequiredPayment->AmountPercent->attributes()->CurrencyCode,
                            'HotelCode' => (string) $RoomStaySingular->BasicPropertyInfo->attributes()->HotelCode,
                        );

                        // Check Null request
                        if (empty($data[$campaign_id]['RoomStayReservation'])
                                // || empty($data[$campaign_id]['CreateDateTime'])
                                || empty($data[$campaign_id]['Type']) || empty($data[$campaign_id]['ID']) || empty($data[$campaign_id]['RoomTypeCode']) || empty($data[$campaign_id]['RatePlan']) || empty($data[$campaign_id]['MealPlanCodes']) || empty($data[$campaign_id]['Start']) || empty($data[$campaign_id]['End']) || empty($data[$campaign_id]['HotelCode'])
                        ) {
                            $error['Empty_Property']['type'] = '2';
                            $error['Empty_Property']['code'] = '321';
                        }

                        // Check Format
                        if (!ctype_alnum($data[$campaign_id]['RoomTypeCode']) || !ctype_alnum($data[$campaign_id]['RatePlan'])
                        ) {
                            $error['SendData']['type'] = '3';
                            $error['SendData']['code'] = '459';
                        }

                        // Check start and end date format
                        if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data[$campaign_id]['Start']) || !preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data[$campaign_id]['End'])
                        ) {
                            $error['Start_or_End']['type'] = '3';
                            $error['Start_or_End']['code'] = '15';
                        }

                        // Check Number Of Nights
                        $data[$campaign_id]['stay_night'] = Date::span(strtotime($data[$campaign_id]['End']), strtotime($data[$campaign_id]['Start']), 'days');
                        if ($data[$campaign_id]['stay_night'] < 1) {
                            $error['Stay_minimum_night']['type'] = '2';
                            $error['Stay_minimum_night']['code'] = '69';
                        }

                        if ($data[$campaign_id]['stay_night'] > 364) {
                            $error['Stay_maximum_night']['type'] = '2';
                            $error['Stay_maximum_night']['code'] = '70';
                        }

                        if (!empty($RequiredPayment)) {
                            // Check Null request
                            if (empty($data[$campaign_id]['CardType']) || empty($data[$campaign_id]['CardCode']) || empty($data[$campaign_id]['CardNumber']) || empty($data[$campaign_id]['ExpireDate']) || empty($data[$campaign_id]['Amount'])
                            ) {
                                $error['Empty_Property']['type'] = '2';
                                $error['Empty_Property']['code'] = '321';
                            }

                            // Check Format
                            if (!is_numeric($data[$campaign_id]['ExpireDate']) || !preg_match('/^\d[\d\,\.]+/', $data[$campaign_id]['Amount'])
                            ) {
                                $error['SendData']['type'] = '3';
                                $error['SendData']['code'] = '459';
                            }
                        }

                        // Data fill
                        $room_id = (string) $RoomTypes->attributes()->RoomTypeCode;

                        $Rates = $RoomRates->Rates->Rate;
                        foreach ($Rates as $Rate) {
                            $data[$campaign_id]['RoomRates'][$room_id] = array(
                                'NumberOfUnits' => (string) $RoomRates->attributes()->NumberOfUnits,
                                'EffectiveDate' => (string) $RoomRates->attributes()->EffectiveDate,
                                'ExpireDate' => (string) $RoomRates->attributes()->ExpireDate,
                                'RoomTypeCode' => (string) $RoomRates->attributes()->RoomTypeCode,
                                'RatePlanCode' => (string) $RoomRates->attributes()->RatePlanCode,
                            );

                            /*
                             * Item, pass because load item from database
                             */
                            /*
                              $j=0;
                              foreach ($Rates as $Rate)
                              {
                              $data[$k]['RoomRates'][$i]['rate'][$j] = array(
                              'EffectiveDate' => (string)$Rate->attributes()->EffectiveDate,
                              'ExpireDate' => (string)$Rate->attributes()->ExpireDate,
                              'RateTimeUnit' => (string)$Rate->attributes()->RateTimeUnit,

                              'Base_AmountBeforeTax' => (string)$Rate->Base->attributes()->AmountBeforeTax,
                              'Base_AmountAfterTax' => (string)$Rate->Base->attributes()->AmountAfterTax,
                              'Base_DecimalPlaces' => (string)$Rate->Base->attributes()->DecimalPlaces ? (string)$Rate->Base->attributes()->DecimalPlaces : 0,
                              'Base_CurrencyCode' => (string)$Rate->Base->attributes()->CurrencyCode,

                              'Total_AmountBeforeTax' => (string)$Rate->Total->attributes()->AmountBeforeTax,
                              'Total_AmountAfterTax' => (string)$Rate->Total->attributes()->AmountAfterTax,
                              'Total_DecimalPlaces' => (string)$Rate->Total->attributes()->DecimalPlaces ? (string)$Rate->Total->attributes()->DecimalPlaces : 0,
                              'Total_CurrencyCode' => (string)$Rate->Total->attributes()->CurrencyCode,
                              );

                              // Check Null request
                              if(empty($data[$k]['RoomRates'][$i]['rate'][$j]['EffectiveDate'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['ExpireDate'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['RateTimeUnit'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['Base_AmountBeforeTax'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['Base_AmountAfterTax'])
                              //|| empty($data[$k]['RoomRates'][$i]['rate'][$j]['Base_DecimalPlaces'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['Base_CurrencyCode'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['Total_AmountBeforeTax'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['Total_AmountAfterTax'])
                              //|| empty($data[$k]['RoomRates'][$i]['rate'][$j]['Total_DecimalPlaces'])
                              || empty($data[$k]['RoomRates'][$i]['rate'][$j]['Total_CurrencyCode'])
                              )
                              {
                              $error['SendData']['type'] = '10';
                              $error['SendData']['code'] = '321';
                              break;
                              }

                              // Check Format
                              if ( ! preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data[$k]['RoomRates'][$i]['rate'][$j]['EffectiveDate'])
                              || ! preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data[$k]['RoomRates'][$i]['rate'][$j]['ExpireDate'])
                              || ! preg_match('/^\d[\d\,\.]+/', $data[$k]['RoomRates'][$i]['rate'][$j]['Base_AmountBeforeTax'])
                              || ! preg_match('/^\d[\d\,\.]+/', $data[$k]['RoomRates'][$i]['rate'][$j]['Base_AmountAfterTax'])
                              || ! is_numeric($data[$k]['RoomRates'][$i]['rate'][$j]['Base_DecimalPlaces'])
                              || ! preg_match('/^\d[\d\,\.]+/', $data[$k]['RoomRates'][$i]['rate'][$j]['Total_AmountBeforeTax'])
                              || ! preg_match('/^\d[\d\,\.]+/', $data[$k]['RoomRates'][$i]['rate'][$j]['Total_AmountAfterTax'])
                              || ! is_numeric($data[$k]['RoomRates'][$i]['rate'][$j]['Total_DecimalPlaces'])
                              )
                              {
                              $error['SendData']['type'] = '3';
                              $error['SendData']['code'] = '459';
                              }
                              $j++;
                              }
                             */
                            // Check Null request
                            if (empty($data[$campaign_id]['RoomRates'][$room_id]['NumberOfUnits']) || empty($data[$campaign_id]['RoomRates'][$room_id]['RoomTypeCode']) || empty($data[$campaign_id]['RoomRates'][$room_id]['RatePlanCode'])
                            ) {
                                $error['SendData']['type'] = '10';
                                $error['SendData']['code'] = '321';
                                break;
                            }
                        }

                        // Data fill
                        $i = 0;
                        $GuestCounts = $RoomStaySingular->GuestCounts->GuestCount;

                        foreach ($GuestCounts as $GuestCount) {
                            $guestcountdataSingular = array(
                                'AgeQualifyingCode' => (string) $GuestCount->attributes()->AgeQualifyingCode,
                                'Count' => (string) $GuestCount->attributes()->Count,
                            );

                            $data[$campaign_id]['GuestCounts'][$RoomStaysNum] = array_push($guestcountdata, $guestcountdataSingular);
                            // Check Null request
                            if (empty($guestcountdata[$i]['AgeQualifyingCode']) || empty($guestcountdata[$i]['Count'])) {
                                $error['SendData']['type'] = '10';
                                $error['SendData']['code'] = '321';
                                break;
                            }

                            // Check Format request
                            if (!is_numeric($guestcountdata[$i]['AgeQualifyingCode']) || !is_numeric($guestcountdata[$i]['Count'])) {
                                $error['SendData']['type'] = '3';
                                $error['SendData']['code'] = '459';
                                break;
                            }
                            $i++;
                        }

                        $var_guest[$RoomStaysNum] = $guestcountdata;
                        $guestcountdata = array();

                        // $data[$campaign_id]['GuestCounts'] = $guestcountdata;
                        // Data fill
                        $i = 0;
                        $ResGuestRPHs = $RoomStaySingular->ResGuestRPHs->ResGuestRPH;
                        if (!empty($ResGuestRPHs)) {
                            foreach ($ResGuestRPHs as $ResGuestRPH) {
                                $data[$campaign_id]['ResGuestRPHs'][$i] = array(
                                    'RPH' => (string) $ResGuestRPH->attributes()->RPH,
                                );
                                $i++;
                            }
                        }

                        // Data fill
                        $i = 0;
                        $SpecialRequests = $RoomStaySingular->SpecialRequests->SpecialRequest;
                        empty($SpecialRequests) ? $SpecialRequests = array() : $SpecialRequests = $SpecialRequests;
                        foreach ($SpecialRequests as $SpecialRequest) {
                            $data[$campaign_id]['SpecialRequests'][$i] = array(
                                'Language' => (string) $SpecialRequest->attributes()->Language,
                                'RequestCode' => (string) $SpecialRequest->attributes()->RequestCode,
                                'SpecialText' => (string) $SpecialRequest->Text,
                            );

                            // Check Null request
                            if (empty($data[$campaign_id]['SpecialRequests'][$i]['Language']) || empty($data[$campaign_id]['SpecialRequests'][$i]['RequestCode']) || empty($data[$campaign_id]['SpecialRequests'][$i]['SpecialText'])
                            ) {
                                $error['SendData']['type'] = '10';
                                $error['SendData']['code'] = '321';
                                break;
                            }

                            $i++;
                        }

                        // Data fill
                        $i = 0;
                        $ResGuests = $HotelReservations->ResGuests->ResGuest;

                        // define Guest Number by Age
                        $data[$campaign_id]['ResGuestNumAdult'] = 0;
                        $data[$campaign_id]['ResGuestNumChild'] = 0;

                        foreach ($ResGuests as $ResGuest) {
                            $data[$campaign_id]['ResGuests'][$i] = array(
                                'ResGuestRPH' => (string) $ResGuest->attributes()->ResGuestRPH,
                                'AgeQualifyingCode' => (string) $ResGuest->attributes()->AgeQualifyingCode,
                                'Age' => (string) $ResGuest->attributes()->Age,
                                'ProfileType' => (string) $ResGuest->Profiles->ProfileInfo->Profile->attributes()->ProfileType,
                                'GivenName' => (string) $ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->GivenName,
                                'Surname' => (string) $ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->Surname,
                            );

                            // Check guest type is an adult?
                            if ((string) $ResGuest->attributes()->AgeQualifyingCode == '10') {
                                $data[$campaign_id]['ResGuestNumAdult'] ++;
                            } else {
                                $data[$campaign_id]['ResGuestNumChild'] ++;
                            }

                            // Check Null request
                            if (empty($data[$campaign_id]['ResGuests'][$i]['ProfileType']) || empty($data[$campaign_id]['ResGuests'][$i]['GivenName']) || empty($data[$campaign_id]['ResGuests'][$i]['Surname'])
                            ) {
                                $error['SendData']['type'] = '10';
                                $error['SendData']['code'] = '321';
                                break;
                            }

                            // Check Format request
                            if (!is_numeric($data[$campaign_id]['ResGuests'][$i]['ProfileType'])) {
                                $error['SendData']['type'] = '3';
                                $error['SendData']['code'] = '459';
                                break;
                            }

                            $i++;
                        }
                        $RoomStaysNum++;
                    } // # foreach $RoomRates
                    // set Room Stay Number
                    $data[$campaign_id]['RoomStaysNum'] = $RoomStaysNum;
                    $data[$campaign_id]['GuestCounts'] = $var_guest;
                } // # foreach $HotelReservations
            }
        }

        // Used for RoomStay response limitation.
        if (count($data) != 1) {
            $error['Allowed_only_one_reservations']['type'] = '8';
            $error['Allowed_only_one_reservations']['code'] = '146';
        }

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $node_head);
            $this->output($errors);
        } else {
            $this->process($data, $node_head);
        }
    }

    public function process($datas, $node_head) {
        // Load Config
        if (Kohana::$environment == Kohana::PRODUCTION) {
            $config = Kohana::$config->load('api')->vacation;
        } else {
            $config = Kohana::$config->load('api')->vacation_test;
        }

        $error = array();
        $warning_datas = array();

        if (count($datas) > 0) {
            foreach ($datas as $campaign_id => &$data) {
                if (is_array($data)) {
                    /**
                     *  Get User Data
                     */
                    $guest_charge = count($data['ResGuests']) > 0 ? reset($data['ResGuests']) : '';

                    /**
                     *  Get hotel
                     */
                    $hotel_query = array(
                        'language_id' => Controller_Vacation_Static::default_language(),
                        'hotel_ids' => $data['HotelCode'],
                    );

                    // Load current hotel
                    $hotels = Model::factory('hotel')->search_hotels($hotel_query);
                    $hotel = reset($hotels);

                    /*
                     * Get selected currency
                     */
                    $this->selected_currency = Model::factory('currency')->get_currency_by_id(1);

                    /**
                     *  Get and calculate capacities
                     */
                    foreach ($data['GuestCounts'] as $key => $GuestCounts) {
                        $capacity[$key]['adult'] = 0;
                        $capacity[$key]['child'] = 0;

                        foreach ($GuestCounts as $kguest => $value) {
                            if ($value['AgeQualifyingCode'] == 4 || $value['AgeQualifyingCode'] == 8
                            ) {
                                $capacity[$key]['child'] = $value['Count'];
                            } else {
                                $capacity[$key]['adult'] = $value['Count'];
                            }
                        }
                        // Set capacities for booking datas
                        $capacities = $capacity;
                        $data['GuestCounts'] = $capacities;
                    }

                    /**
                     *  Get surcharges
                     */
                    // Get surcharges
                    $surcharges = Model::factory('surcharge')->get_surcharges($data['HotelCode'], $data['Start'], $data['End']);

                    // calculate surcharges price
                    foreach ($surcharges as &$surcharge) {
                        $surcharge['adult_total'] = $surcharge['adult_price'] * $data['ResGuestNumAdult'];
                        $surcharge['child_total'] = $surcharge['child_price'] * $data['ResGuestNumChild'];
                        $surcharge['total'] = $surcharge['adult_total'] + $surcharge['child_total'];
                    }

                    //reset key $surcharges into date
                    foreach ($surcharges as $key => $value) {
                        $surcharges[$value['date']] = $surcharges[$key];
                        unset($surcharges[$key]);
                    }

                    /**
                     *  Get prices
                     */
                    // Get total Price from rates
                    foreach ($data['RoomRates'] as $RoomRate) {
                        // Get campaigns
                        $hotel_query = array(
                            'language_id' => Controller_Vacation_Static::default_language(),
                            'check_in' => $data['Start'],
                            'check_out' => $data['End'],
                            'now' => date('Y-m-d'),
                            'InvTypeCode' => $RoomRate['RoomTypeCode'],
                            'RatePlanCode' => $RoomRate['RatePlanCode'],
                            // 'number_of_rooms' => $RoomRate['NumberOfUnits'],
                            'number_of_rooms' => $data['RoomStaysNum'],
                            'capacities' => $capacities,
                        );


                        $campaigns = Model::factory('hotel')->get_campaigns($data['HotelCode'], $hotel_query, TRUE);

                        if (count($campaigns)) {
                            // Load campaign
                            $campaigns = Controller_Vacation_Static::campaign_items($hotel_query, $campaigns);

                            $campaign = reset($campaigns);
                            $campaign_id = $campaign['campaign_id'];

                            // total item price
                            // $total_item_price = $campaign['total_price'] * $RoomRate['NumberOfUnits'];
                            $total_item_price = $campaign['total_price'] * $data['RoomStaysNum'];

                            // total surcharge
                            $total_surcharge = array_sum(Arr::pluck($surcharges, 'total'));

                            // Get number of Extrabed
                            foreach ($capacities as $capacity) {
                                $number_of_extrabed += Model::factory('Room_Capacity')->get_extrabed($RoomRate['RoomTypeCode'], $capacity);
                                $number_of_extrabed = $number_of_extrabed ? $number_of_extrabed : 0;
                            }

                            // Get extrabed price
                            $extrabed_price = $campaign['extrabed_price'] * $number_of_extrabed * $data['stay_night'];

                            // Get invoice_amount
                            $invoice_amount = $total_item_price + $total_surcharge + $extrabed_price;

                            // Create invoice_point
                            $invoice_point = ceil($invoice_amount * Model::factory('exchange')->to($hotel->currency_id, 1));

                            // Create point get
                            $point_get = floor(($total_item_price * Model::factory('exchange')->to($hotel->currency_id, 1)) * ($hotel->point_rate / 100));

                            /*
                             *  Currencies
                             */
                            // Get hotel currency
                            $hotel_currency = Model::factory('currency')->get_currency_by_id($hotel->currency_id);

                            // Get transaction currency
                            $transaction_currency = Model::factory('currency')->get_currency_by_code($hotel_currency->transaction_code);

                            // Create invoice transaction
                            $invoice_transaction = array(
                                'symbol' => $transaction_currency->symbol,
                                'name' => $transaction_currency->name,
                                'amount' => $invoice_amount * Model::factory('exchange')->to($hotel_currency->id, $transaction_currency->id),
                            );

                            /**
                             *  Cancellation
                             */
                            // Get cancellations
                            $cancellation = Model::factory('cancellation')->get_cancellation($campaign['campaign_id'], $hotel_query['check_in'], $hotel_query['check_out']);

                            // If cancellation does not exist
                            if (!$cancellation) {
                                // Search room default campaign
                                $default_campaign_id = Model::factory('campaign')->get_default_campaign_id($campaign['room_id']);

                                // Get cancellations
                                $cancellation = Model::factory('cancellation')->get_cancellation($default_campaign_id, $hotel_query['check_in'], $hotel_query['check_out']);
                            }

                            // If cancellation does not exist
                            if (!$cancellation) {
                                $cancellation = new stdClass();
                                $cancellation->id = NULL;
                                $cancellation->level_1_cancellation_rule_id = 8;
                                $cancellation->level_2_cancellation_rule_id = NULL;
                                $cancellation->level_3_cancellation_rule_id = NULL;
                                $cancellation->start_date = $hotel_query['check_in'];
                                $cancellation->end_date = $hotel_query['check_in'];
                                $cancellation->rule_1_name = 'Non Refundable';
                                $cancellation->rule_2_name = NULL;
                                $cancellation->rule_3_name = NULL;
                            }

                            // Get Additional benefit
                            $other_benefits = DB::select(
                                            'campaigns_benefits.*', array('benefit_texts.name', 'name')
                                    )
                                    ->from('campaigns_benefits')
                                    ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                                    ->where('benefit_texts.language_id', '=', Controller_Vacation_Static::default_language())
                                    ->where('campaigns_benefits.campaign_id', '=', $campaign['campaign_id'])
                                    ->execute();

                            if (count($other_benefits) > 0) {
                                foreach ($other_benefits as $key => $other_benefit) {
                                    $additional_benefit[] = array(
                                        'name' => $other_benefit['name'],
                                        'value' => $other_benefit['value']
                                    );
                                }
                            }

                            /**
                             *  Special Request
                             */
                            $data['SpecialRequests'] = empty($data['SpecialRequests']) ? array() : $data['SpecialRequests'];
                            if (count($data['SpecialRequests']) > 0) {
                                foreach ($data['SpecialRequests'] as $SpecialRequest) {
                                    $request_notes[] = $SpecialRequest['SpecialText'];
                                }
                                $request_note = implode(', ', $request_notes);
                            } else {
                                $request_note = '';
                            }

                            /*
                             * Promotions Descriptions
                             */
                            if ($campaign['type'] != 0) {
                                $hotel_query['currency_code'] = 1;
                                $hotel_query['hotel_currency'] = $campaign['currency_id'];
                                $data['promotions_descriptions'] = Controller_Vacation_Static::promotion_descriptions($hotel_query, $campaign, 1);
                            } else {
                                $data['promotions_descriptions'] = 'Default Promotions';
                            }

                            // Is Breakfast Included
                            $data['is_breakfast_included'] = $campaign['is_breakfast_included'];

                            // Hotel Rate
                            $data['hotel_service_charge_rate'] = $campaign['hotel_service_charge_rate'];
                            $data['hotel_tax_rate'] = $campaign['hotel_tax_rate'];

                            // Room name
                            $data['room_name'] = $campaign['room_name'];

                            // Room name
                            $data['currency_id'] = $campaign['currency_id'];

                            /*
                             *  Set new Item Price based day
                             */
                            if ($campaign) {
                                $hotel_no_arrival = DB::select()
                                        ->from('hotel_no_arrivals')
                                        ->where('hotel_id', '=', $data['HotelCode'])
                                        ->where('date', '=', $data['Start'])
                                        ->execute()
                                        ->get('date');

                                // TODO campaignに入れなくてもOKかな。
                                // Get no departure
                                $hotel_no_departure = DB::select()
                                        ->from('hotel_no_departures')
                                        ->where('hotel_id', '=', $data['HotelCode'])
                                        ->where('date', '=', $data['End'])
                                        ->execute()
                                        ->get('date');

                                foreach ($campaign['items'] as $key => &$item) {
                                    if (isset($item['date'])) {
                                        // Show flag true
                                        $data['RoomRates'][$campaign['room_id']]['item'][$key]['show_flag'] = TRUE;
                                        $data['RoomRates'][$campaign['room_id']]['item'][$key]['remaining_flag'] = ($item['stock']) ? $item['stock'] : FALSE;

                                        if (array_key_exists($key, $surcharges)) {
                                            $data['RoomRates'][$campaign['room_id']]['item'][$key]['surcharge_total'] = $surcharges[$key]['total'];
                                        }

                                        if ($extrabed_price > 0) {
                                            $data['RoomRates'][$campaign['room_id']]['item'][$key]['extrabed_price'] = $extrabed_price;
                                        } else {
                                            $data['RoomRates'][$campaign['room_id']]['item'][$key]['extrabed_price'] = 0;
                                        }

                                        // If room capacity is true
                                        if ($campaign['room_capacity'] && !$hotel_no_arrival && !$hotel_no_arrival) {
                                            // If not blackout or not weekout
                                            if (!($item['blackout'] OR $item['weekout'])) {
                                                //If there is stock
                                                if ($item['stock'] >= $hotel_query['number_of_rooms']) {
                                                    // Out flag ture
                                                    $data['RoomRates'][$campaign['room_id']]['item'][$key]['price_flag'] = TRUE;
                                                    $campaign['night_count'] ++;

                                                    // If campaign blackout
                                                    if ($item['campaign_blackout']) {
                                                        $data['RoomRates'][$campaign['room_id']]['item'][$key]['price'] = $item['price'];
                                                    }
                                                    /// Campaign Prices. Is campaign prices
                                                    // elseif ($item['is_campaign_prices'] != 0)
                                                    // {
                                                    //   //application control
                                                    //   if($item['application_control'] != NULL)
                                                    //   {
                                                    //     $application_controls =  unserialize($item['application_control']);
                                                    //     if (count($application_controls) > 0) 
                                                    //     {
                                                    //       foreach ($application_controls as $key_application_control => $application_control) {
                                                    //         if (date('w', strtotime($item['date'])) == $key_application_control AND $application_control) 
                                                    //         {
                                                    //           $day['price'] = $item['price'];
                                                    //         }
                                                    //       }
                                                    //     }
                                                    //   }
                                                    // }
                                                    else {
                                                        // If promotion
                                                        if ($item['promotion']) {
                                                            // If free night
                                                            if ($item['free_night']) {
                                                                $data['RoomRates'][$campaign['room_id']]['item'][$key]['price'] = 0;
                                                                $data['RoomRates'][$campaign['room_id']]['item'][$key]['free_flag'] = TRUE;
                                                            } else {
                                                                $data['RoomRates'][$campaign['room_id']]['item'][$key]['price'] = ($item['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                                                            }
                                                        } else {
                                                            $data['RoomRates'][$campaign['room_id']]['item'][$key]['price'] = $item['price'];
                                                        }
                                                    }
                                                } else {
                                                    $error['Reservation_failed']['type'] = '8';
                                                    $error['Reservation_failed']['code'] = '394';
                                                    continue;
                                                }
                                            } else {
                                                $error['Reservation_failed']['type'] = '8';
                                                $error['Reservation_failed']['code'] = '394';
                                                continue;
                                            }
                                        } else {
                                            $error['Reservation_failed']['type'] = '8';
                                            $error['Reservation_failed']['code'] = '394';
                                            continue;
                                        }
                                    }
                                }
                            }

                            if (!empty($error)) {
                                continue;
                            }
                            /**
                             *  Set booking data
                             */
                            // Set booking data
                            $booking_data = array(
                                'hotel_id' => $hotel->hotel_id,
                                'hotel_segment' => $hotel->hotel_segment,
                                'hotel_name' => $hotel->hotel_name,
                                'hotel_type' => $hotel->type,
                                'hotel_basename' => $hotel->hotel_basename,
                                'hotel_small_basename' => $hotel->hotel_small_basename,
                                'hotel_medium_basename' => $hotel->hotel_medium_basename,
                                'hotel_star' => $hotel->hotel_star,
                                'hotel_address' => $hotel->hotel_address,
                                'hotel_telephone' => $hotel->telephone,
                                'hotel_fax' => $hotel->fax,
                                'hotel_tax_charge' => $hotel->hotel_service_charge_rate + $hotel->hotel_tax_rate,
                                'hotel_coordinate' => $hotel->coordinate,
                                'check_in_time' => $hotel->check_in_time,
                                'check_out_time' => $hotel->check_out_time,
                                'country_id' => $hotel->country_id,
                                'country_segment' => $hotel->country_segment,
                                'country_name' => $hotel->country_name,
                                'city_id' => $hotel->city_id,
                                'city_segment' => $hotel->city_segment,
                                'city_name' => $hotel->city_name,
                                'district_id' => $hotel->district_id,
                                'district_segment' => $hotel->district_segment,
                                'district_name' => $hotel->district_name,
                                'hotel_currency' => $hotel_currency,
                                'transaction_currency' => $transaction_currency,
                                'point_rate' => $hotel->point_rate,
                                'room_id' => Arr::get($campaign, 'room_id'),
                                'room_name' => Arr::get($campaign, 'room_name'),
                                'room_publish_price' => Arr::get($campaign, 'room_publish_price'),
                                'room_photo' => Arr::get($campaign, 'room_photo'),
                                'campaign_id' => Arr::get($campaign, 'campaign_id'),
                                'adult_breakfast_price' => Arr::get($campaign, 'adult_breakfast_price'),
                                'child_breakfast_price' => Arr::get($campaign, 'child_breakfast_price'),
                                'child_breakfast_price' => Arr::get($campaign, 'child_breakfast_price'),
                                'is_breakfast_included' => Arr::get($campaign, 'is_breakfast_included'),
                                'number_of_persons_breakfast_included' => Arr::get($campaign, 'number_of_persons_breakfast_included'),
                                'maximum_number_of_extrabeds' => Arr::get($campaign, 'maximum_number_of_extrabeds'),
                                'is_extrabed_get_breakfast' => Arr::get($campaign, 'is_extrabed_get_breakfast'),
                                'number_of_beds' => Arr::get($campaign, 'number_of_beds'),
                                'size' => Arr::get($campaign, 'size'),
                                'item_ids' => Arr::pluck(Arr::get($campaign, 'items'), 'item_id'),
                                'check_in' => Arr::get($hotel_query, 'check_in'),
                                'check_out' => Arr::get($hotel_query, 'check_out'),
                                'number_of_rooms' => Arr::get($hotel_query, 'number_of_rooms'),
                                'nights' => Arr::get($campaign, 'nights'),
                                'capacities' => $capacities,
                                // 'number_of_adult' => $capacity['adult'],
                                // 'number_of_child' => $capacity['child'],
                                'number_of_adult' => $datas[$campaign_id]['ResGuestNumAdult'],
                                'number_of_child' => $datas[$campaign_id]['ResGuestNumChild'],
                                'number_of_extrabed' => $number_of_extrabed,
                                'surcharges' => $surcharges,
                                'average_price' => Arr::get($campaign, 'average_price'),
                                'total_price' => $total_item_price,
                                'total_surcharge' => $total_surcharge,
                                'extrabed_price' => $extrabed_price,
                                'point_get' => $point_get,
                                'cancellation' => $cancellation,
                                'invoice_amount' => $invoice_amount,
                                'invoice_point' => $invoice_point,
                                'invoice_transaction' => $invoice_transaction,
                            );

                            // Transaction start
                            Database::instance()->begin();

                            try {
                                /**
                                 *  Calculate currency
                                 */
                                // Create points used amount
                                $points_used_amount = $this->request->post('points_used') * Model::factory('exchange')->to(1, $hotel->currency_id);

                                // Create remaining amount
                                $remaining_amount = $invoice_amount - $points_used_amount;

                                if ($remaining_amount <= 0) {
                                    $remaining_amount = 0;
                                }

                                // Create remaining amount transaction
                                $remaining_amount_transaction = round($remaining_amount * Model::factory('exchange')->to(1, $transaction_currency->id), 2);

                                // Create remaining point
                                $remaining_point = 0;

                                // Create point get
                                $point_get = floor(($remaining_amount * Model::factory('exchange')->to($hotel->currency_id, 1)) * ($hotel->point_rate / 100));

                                // Set to booking data array
                                $booking_data['points_used_amount'] = $points_used_amount;
                                $booking_data['remaining_amount'] = $remaining_amount;
                                $booking_data['remaining_point'] = $remaining_point;
                                $booking_data['remaining_amount_transaction'] = $remaining_amount_transaction;
                                $booking_data['point_get'] = $point_get;

                                // Add remaing amount user currency
                                $booking_data['remaining_amount_user_currency'] = array(
                                    'symbol' => $this->selected_currency->symbol,
                                    'name' => $this->selected_currency->name,
                                    'amount' => $remaining_amount * Model::factory('exchange')->to($hotel_currency->id, $this->selected_currency->id),
                                );

                                //if CC holder is exist
                                /*
                                  $booking_data['cc_holder'] = array(
                                  'cc_holder' => 'VACATION',
                                  'name_on_card' => Arr::get($this->request->post(), 'name_on_card'),
                                  'phone_number' => Arr::get($this->request->post(), 'phone_number'),
                                  'billing_address' => Arr::get($this->request->post(), 'billing_address'),
                                  );
                                 */
                                $booking_data['additional_benefit'] = !empty($additional_benefit) ? $additional_benefit : '';

                                /**
                                 *  Build booking pendings table row
                                 */
                                // Set booking pending array
                                $booking_pending_array = array(
                                    'data' => serialize($booking_data),
                                    'created' => time(),
                                    'is_can_delete' => 0,
                                    'room_id' => Arr::get($booking_data, 'room_id'),
                                    'campaign_id' => Arr::get($booking_data, 'campaign_id'),
                                    'language_id' => Controller_Vacation_Static::default_language(),
                                    'currency_id' => $this->selected_currency->id,
                                    'nationality_id' => $config['nationality_id'],
                                    'check_in_date' => Arr::get($booking_data, 'check_in'),
                                    'check_out_date' => Arr::get($booking_data, 'check_out'),
                                    'title_id' => 2,
                                    'first_name' => $guest_charge['GivenName'],
                                    'last_name' => $guest_charge['Surname'],
                                    'email' => $config['email'],
                                    'is_non_smoking_room_request' => Arr::path($this->request->post(), 'special_requests.non_smorking_room', 0),
                                    'is_late_check_in_request' => Arr::path($this->request->post(), 'special_requests.late_check_in', 0),
                                    'is_early_check_in_request' => Arr::path($this->request->post(), 'special_requests.early_check_in', 0),
                                    'is_high_floor_request' => Arr::path($this->request->post(), 'special_requests.high_floor', 0),
                                    'is_large_bed_request' => Arr::path($this->request->post(), 'special_requests.large_bed', 0),
                                    'is_twin_beds_request' => Arr::path($this->request->post(), 'special_requests.twin_bed', 0),
                                    'is_airport_transfer_request' => Arr::path($this->request->post(), 'special_requests.airport_transfer', 0),
                                    'request_note' => $request_note,
                                    'number_of_points_used' => Arr::get($this->request->post(), 'points_used', 0),
                                    'phone_number' => NULL,
                                );

                                /**
                                 *  Build booking datas table row
                                 */
                                // Get datas
                                $data_hotel = DB::select()
                                        ->from('hotels')
                                        ->where('id', '=', $hotel->hotel_id)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_hotel_texts = DB::select()
                                        ->from('hotel_texts')
                                        ->where('hotel_id', '=', $hotel->hotel_id)
                                        ->as_object()
                                        ->execute()
                                        ->as_array('language_id');

                                $data_room = DB::select()
                                        ->from('rooms')
                                        ->where('id', '=', $campaign['room_id'])
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_room_texts = DB::select()
                                        ->from('room_texts')
                                        ->where('room_id', '=', $campaign['room_id'])
                                        ->as_object()
                                        ->execute()
                                        ->as_array('language_id');

                                $data_campaign = DB::select()
                                        ->from('campaigns')
                                        ->where('id', '=', $campaign['campaign_id'])
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_items = DB::select()
                                        ->from('items')
                                        ->where('campaign_id', '=', 0)
                                        ->where('room_id', '=', $campaign['room_id'])
                                        ->where('date', '>=', $hotel_query['check_in'])
                                        ->where('date', '<', $hotel_query['check_out'])
                                        ->where('stock', '!=', 0)
                                        ->where('is_blackout', '=', 0)
                                        ->order_by('campaign_id', 'ASC')
                                        ->as_object()
                                        ->execute()
                                        ->as_array('date');

                                $data_surcharges = DB::select()
                                        ->from('surcharges')
                                        ->where('hotel_id', '=', $hotel->hotel_id)
                                        ->where('date', '>=', $hotel_query['check_in'])
                                        ->where('date', '<', $hotel_query['check_out'])
                                        ->as_object()
                                        ->execute()
                                        ->as_array('date');

                                $data_cancellation = (object) $cancellation;

                                $data_guest_currency = DB::select()
                                        ->from('currencies')
                                        ->where('id', '=', $this->selected_currency->id)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_hotel_currency = DB::select()
                                        ->from('currencies')
                                        ->where('id', '=', $hotel->currency_id)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_transaction_currency = DB::select()
                                        ->from('currencies')
                                        ->where('code', '=', $data_hotel_currency->transaction_code)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_hotel_to_guest_exchange = DB::select()
                                        ->from('exchanges')
                                        ->where('from_currency_id', '=', $hotel->currency_id)
                                        ->where('to_currency_id', '=', $this->selected_currency->id)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_hotel_to_transaction_exchange = DB::select()
                                        ->from('exchanges')
                                        ->where('from_currency_id', '=', $hotel->currency_id)
                                        ->where('to_currency_id', '=', $data_transaction_currency->id)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_hotel_to_point_exchange = DB::select()
                                        ->from('exchanges')
                                        ->where('from_currency_id', '=', $hotel->currency_id)
                                        ->where('to_currency_id', '=', 1)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                $data_hotel_to_all_exchange = DB::select()
                                        ->from('exchanges')
                                        ->where('from_currency_id', '=', $hotel->currency_id)
                                        ->as_object()
                                        ->execute()
                                        ->as_array('to_currency_id');

                                $data_point_to_hotel_exchange = DB::select()
                                        ->from('exchanges')
                                        ->where('from_currency_id', '=', 1)
                                        ->where('to_currency_id', '=', $hotel->currency_id)
                                        ->as_object()
                                        ->execute()
                                        ->current();

                                //if CC holder is exist
                                /*
                                  $cc_holder = array(
                                  'cc_holder' => Arr::get($this->request->post(), 'cc_holder'),
                                  'name_on_card' => Arr::get($this->request->post(), 'name_on_card'),
                                  'phone_number' => Arr::get($this->request->post(), 'phone_number'),
                                  'billing_address' => Arr::get($this->request->post(), 'billing_address')
                                  );
                                 */

                                // Set booking datas array
                                $booking_datas_array = array(
                                    'hotel' => serialize($data_hotel),
                                    'hotel_texts' => serialize($data_hotel_texts),
                                    'room' => serialize($data_room),
                                    'room_texts' => serialize($data_room_texts),
                                    'campaign' => serialize($data_campaign),
                                    'items' => serialize($data_items),
                                    'surcharges' => serialize($data_surcharges),
                                    'cancellation' => serialize($data_cancellation),
                                    'guest_currency' => serialize($data_guest_currency),
                                    'transaction_currency' => serialize($data_transaction_currency),
                                    'hotel_to_guest_exchange' => serialize($data_hotel_to_guest_exchange),
                                    'hotel_to_transaction_exchange' => serialize($data_hotel_to_transaction_exchange),
                                    'hotel_to_point_exchange' => serialize($data_hotel_to_point_exchange),
                                    'hotel_to_all_exchange' => serialize($data_hotel_to_all_exchange),
                                    'point_to_hotel_exchange' => serialize($data_point_to_hotel_exchange),
                                        //'cc_holder' => serialize($cc_holder)
                                );

                                // Set booking datas array to bookin pending array
                                $booking_pending_array['booking_datas'] = serialize($booking_datas_array);

                                /**
                                 *  Build booking rooms table row
                                 */
                                // Create booking rooms
                                $booking_rooms_array = array();

                                // Create booking rooms
                                foreach ($capacities as $capacitie) {
                                    $booking_rooms_array[] = array(
                                        'number_of_adults' => $capacitie['adult'],
                                        'number_of_children' => $capacitie['child'],
                                        'number_of_extrabeds' => Model::factory('Room_Capacity')->get_extrabed($RoomRate['RoomTypeCode'], $capacitie),
                                    );
                                }

                                // Set booking datas array to bookin pending array
                                $booking_pending_array['booking_rooms'] = serialize($booking_rooms_array);

                                // Set booking key
                                $booking_pending_array['key'] = UUID::v4();

                                // Set booking pending array
                                $booking_pending_array['created'] = time();
                                $booking_pending_array['number_of_rooms'] = Arr::get($hotel_query, 'number_of_rooms');
                                $booking_pending_array['item_ids'] = serialize(Arr::pluck(Arr::get($campaign, 'items'), 'item_id'));

                                list($booking_pending_id, $total_rows) = Model::factory('booking_pending')->create_booking_pending($booking_pending_array);

                                // Update each item stock
                                foreach (Arr::pluck($campaign['items'], 'item_id') as $item_id) {
                                    Model::factory('Item')->decrease_item_stock($item_id, $hotel_query['number_of_rooms']);
                                }

                                Model::factory('booking_pending')->update_booking_pending($booking_pending_array['key'], $booking_pending_array);

                                /**
                                 *  Success processing
                                 */
                                // Commit transaction
                                Database::instance()->commit();
                            } catch (Exception $e) {
                                $error['Reservation_failed']['type'] = '8';
                                $error['Reservation_failed']['code'] = '450';


                                $warning_datas[$RoomRate['RoomTypeCode']]['short_text'] = 'Reservation Failed';
                                $warning_datas[$RoomRate['RoomTypeCode']]['text'] = 'Rate: ' . $data['RatePlan'] . ', ';
                                $warning_datas[$RoomRate['RoomTypeCode']]['text'] .= 'CheckIn: ' . $data['Start'] . ', ';
                                $warning_datas[$RoomRate['RoomTypeCode']]['text'] .= 'CheckOut: ' . $data['End'] . ', ';
                                $warning_datas[$RoomRate['RoomTypeCode']]['text'] .= 'Reservation Failed' . '.';

                                continue;
                            }

                            /*
                             * If Booking Pending process is sucess
                             */
                            if ($booking_pending_id) {
                                // Get booking pending from model
                                $booking_pending = Model::factory('booking_pending')->get_booking_pending_by_id($booking_pending_id);

                                // Get booking data
                                $booking_data = unserialize($booking_pending->data);

                                // Get Additional benefit
                                $other_benefits = DB::select(
                                                'campaigns_benefits.*', array('benefit_texts.name', 'name')
                                        )
                                        ->from('campaigns_benefits')
                                        ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                                        ->where('benefit_texts.language_id', '=', Controller_Vacation_Static::default_language())
                                        ->where('campaigns_benefits.campaign_id', '=', $booking_pending->campaign_id)
                                        ->execute();

                                if (count($other_benefits) > 0) {
                                    foreach ($other_benefits as $key => $other_benefit) {
                                        $additional_benefit[] = array(
                                            'name' => $other_benefit['name'],
                                            'value' => $other_benefit['value']
                                        );
                                    }
                                }

                                // Booking Process
                                $data['booking_id'] = $this->adminpayment($booking_pending_id);

                                if (!$data['booking_id']) {
                                    $error['Reservation_failed']['type'] = '8';
                                    $error['Reservation_failed']['code'] = '450';

                                    continue;
                                }
                            } else {
                                $error['Reservation_failed']['type'] = '8';
                                $error['Reservation_failed']['code'] = '450';

                                /*
                                  $warning_datas[$RoomRate['RoomTypeCode']]['short_text'] = 'Booking Pending not exist';
                                  $warning_datas[$RoomRate['RoomTypeCode']]['text'] = 'Rate: '.$data['RatePlan'].', ';
                                  $warning_datas[$RoomRate['RoomTypeCode']]['text'] .= 'CheckIn: '.$data['Start'].', ';
                                  $warning_datas[$RoomRate['RoomTypeCode']]['text'] .= 'CheckOut: '.$data['End'].', ';
                                  $warning_datas[$RoomRate['RoomTypeCode']]['text'] .= 'Booking Pending not exist'.'.';
                                 */
                            }
                        } else {
                            $error['Empty_campaign']['type'] = '8';
                            $error['Empty_campaign']['code'] = '450';

                            continue;
                        }
                    }
                }
            }
        }

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $node_head);
            $this->output($errors);
        } else {
            // Build response
            $this->xml_build($datas, $node_head, $warning_datas);
        }
    }

    public function adminpayment($booking_pending_id) {
        // Process Status
        $booking_id = 0;

        // Get booking pending
        $booking_pending = Model::factory('booking_pending')->get_booking_pending_by_id($booking_pending_id);

        //add to log if booking pending is not found
        if (empty($booking_pending)) {
            Log::instance()->add(Log::INFO, 'Vacation Reservation Booking_Pending not Found : ');
        }

        // If there is booking pending
        if ($booking_pending) {
            // Get booking data
            $booking_data = unserialize($booking_pending->data);

            // Process booking
            $booking_id = $this->payment_process($booking_pending, $booking_data, 3);

            //add to log if booking ID is not found
            if (empty($booking_id)) {
                Log::instance()->add(Log::INFO, 'Vacation Reservation Booking_id not Found : ');
            }
        }

        return $booking_id;
    }

    public function payment_process($booking_pending, $booking_data, $payment_method_id, $payment_values = array()) {
        // Load Config
        if (Kohana::$environment == Kohana::PRODUCTION) {
            $config = Kohana::$config->load('api')->vacation;
        } else {
            $config = Kohana::$config->load('api')->vacation_test;
        }

        // Transaction start
        Database::instance()->begin();

        try {
            // Process Status
            $booking_id = 0;

            // Get language code
            $language = Model::factory('language')->get_language_by_id($booking_pending->language_id);

            // Set website language
            I18n::lang($language->code);

            // Build bookin array
            $booking_array = (array) $booking_pending;
            unset($booking_array['id']);
            unset($booking_array['key']);
            unset($booking_array['item_ids']);
            unset($booking_array['is_can_delete']);
            unset($booking_array['booking_datas']);
            unset($booking_array['booking_rooms']);
            unset($booking_array['status']);
            unset($booking_array['retry_counts']);

            // Build bookin datas array
            $booking_datas_array = unserialize($booking_pending->booking_datas);
            $booking_datas_array['booking_pending_id'] = $booking_pending->id;

            // Save CC Holder
            /*
              $booking_array['cc_holder'] = $booking_datas_array['cc_holder'];
              unset($booking_datas_array['cc_holder']);


              // Save CC Data
              $booking_array['card_type'] = $payment_values['card_type'];
              $booking_array['card_number_last_4digits'] = $payment_values['card_last4'];
             */

            // Create booking
            // Add notice success
            list($booking_id, $total_row) = Model::factory('booking')->create_booking($booking_array);


            // Create booking datas
            $booking_datas_array['booking_id'] = $booking_id;
            list($booking_data_id, $total_row) = Model::factory('booking_data')->create_booking_data($booking_datas_array);
            // Build booking rooms array
            $booking_rooms_arrays = unserialize($booking_pending->booking_rooms);

            $no_item_data = false;
            if (count(unserialize($booking_datas_array['items'])) == 0) {
                $no_item_data = true;
                throw new Exception("Invalid Item");
            }

            // Create booking rooms
            foreach ($booking_rooms_arrays as &$booking_rooms_array) {
                $booking_rooms_array['booking_id'] = $booking_id;
                list($booking_room_id, $total_row) = Model::factory('booking_room')->create_booking_room($booking_rooms_array);
            }

            // Increase item sould
            $item_ids = unserialize($booking_pending->item_ids);

            if ($item_ids) {
                Model::factory('item')->increase_item_sold($item_ids, $booking_pending->number_of_rooms);
            }

            // delete this booking pending from booking pendings table
            Model::factory('booking_pending')->delete_booking_pending($booking_pending->id);

            // GOT $booking_id? send e-mail after booking pending is correct outside below
            // transaction commit
            Database::instance()->commit();
        } catch (Exception $e) {
            // Process Status
            $booking_id = 0;

            // transaction rollback
            Database::instance()->rollback();

            /**
             * Item stock return
             */
            // Get item ids
            $item_ids = unserialize($booking_pending->item_ids);

            // only if 
            if (count(unserialize($booking_datas_array['items'])) > 0) {
                // Increase item stock
                foreach ($item_ids as $item_id) {
                    Model::factory('item')->increase_item_stock($item_id, $booking_pending->number_of_rooms);
                }
            }

            // delete this booking pending from booking pendings table
            Model::factory('booking_pending')->delete_booking_pending($booking_pending->id);

            /**
             *  Email to user
             */
            $subject = __('Booking Failed - ' . $e->getMessage());

            $mail_body = Kostache::factory('email/booking/fail')
                    ->set('booking_id', $booking_id)
                    ->set('booking_pending', $booking_pending)
                    ->set('booking_data', $booking_data);

            set_time_limit(360);

            // Send email
            Email::factory($subject, $mail_body, 'text/plain')
                    ->from(Kohana::config('application')->get('email'), 'Hoterip')
                    ->to($booking_pending->email)
                    //->cc(Kohana::config('application')->get('email'), 'Hoterip')
                    // Un/comment to send mail
                    // ;
                    ->send();
        }

        /**
         *  After database process success
         *
         *  Pdf
         */
        if ($booking_id) {

            $hotel = Model::factory('hotel')->get_hotel_by_id($booking_pending->language_id, $booking_data['hotel_id']);
            $hotel_en = Model::factory('hotel')->get_hotel_by_id(1, $booking_data['hotel_id']);
            $room_en = Model::factory('room')->get_by_id($booking_pending->language_id, $booking_data['room_id']);

            //Pdf filename
            $filename = Controller_Vacation_Static::config()->get('pdf_directory') . "HoteripVoucher-{$booking_id}.pdf";

            // Create html for pdf
            $html = Kostache::factory('pdf/voucher')
                    ->set('booking_id', $booking_id)
                    ->set('booking_pending', $booking_pending)
                    ->set('booking_data', $booking_data)
                    ->set('hotel', $hotel)
                    ->set('hotel_en', $hotel_en)
                    ->set('payment_method_id', $payment_method_id)
                    ->set('payment_values', $payment_values)
                    ->render();

            // Create path and filename
            Kompdf::factory($language->code, 'A4')
                    ->set_html($html)
                    ->save($filename);

            // Get admin emails
            $admin_emails = Model::factory('admin')->get_email($booking_data['hotel_id']);
            // Get extranet only emails
            $extranet_emails = Model::factory('admin')->get_email_extranet($booking_data['hotel_id']);

            /**
             *  Email to user
             */
            $subject = __('Booking has been completed');

            // Create email message
            $mail_body = Kostache::factory('email/booking/user')
                    ->set('booking_id', $booking_id)
                    ->set('booking_pending', $booking_pending)
                    ->set('booking_data', $booking_data);

            set_time_limit(360);

            // Send email
            Email::factory($subject, $mail_body, 'text/plain')
                    ->from(Kohana::config('application')->get('email'), 'Hoterip')
                    ->to($config['email'], 'VACATION')
                    ->cc(Kohana::config('application')->get('email'), 'Hoterip')
                    ->bcc($extranet_emails)
                    ->attach_file($filename)
                    ->send();

            /**
             *  Email to hotel
             */
            $subject = 'Booking has been completed';

            $mail_body = Kostache::factory('email/booking/hotel')
                    ->set('booking_id', $booking_id)
                    ->set('booking_pending', $booking_pending)
                    ->set('booking_data', $booking_data)
                    ->set('items', Model::factory('item')->get_items_by_item_id(unserialize($booking_pending->item_ids)))
                    ->set('hotel_en', $hotel_en)
                    ->set('room_en', $room_en);

            set_time_limit(360);

            // Send email

            Email::factory($subject, $mail_body, 'text/plain')
                    ->from(Kohana::config('application')->get('email'), 'Hoterip')
                    ->to($extranet_emails)
                    ->to(Kohana::config('application')->get('email'), 'Hoterip')
                    ->bcc($admin_emails)
                    ->send();
            // Delete pdf file
            if (file_exists($filename)) {
                unlink($filename);
            }

            //get item to send_mail for notification if stock = 0
            try {
                $item_send_to_mail = $this->checkItemForNotifications($item_ids);
                if (!empty($item_send_to_mail)) {

                    foreach ($item_send_to_mail as $item_send_mail => $item_mail) {
                        $subject_email_notification = 'Hoterip Room Stock Notification';

                        $mail_body = Kostache::factory('email/notifstock')
                                ->set('hotel_en', $hotel_en)
                                ->set('room_en', $room_en);

                        // Get admin emails (ALL ADMINs OF HOTEL: Admin and Extranet Role)
                        // $admin_emails = Model::factory('admin')->get_email($booking_data['hotel_id']);
                        // Get admin only emails
                        // $admin_emails = Model::factory('admin')->get_email_admin($booking_data['hotel_id']);
                        // Get extranet only emails
                        // $extranet_emails = Model::factory('admin')->get_email_extranet($booking_data['hotel_id']);

                        set_time_limit(360);

                        // Send email
                        Email::factory($subject_email_notification, $mail_body, 'text/plain')
                                ->from(Kohana::config('application')->get('email'), 'Hoterip')
                                ->to($extranet_emails)
                                ->cc('sam.fajar@wgs.co.id')
                                ->send();

                        Log::instance()->add(Log::INFO, 'Send email notification stock success to hotel ' . $hotel_en->name . ' Room ' . $room_en->name);
                    }
                }
            }
            /**
             * Hendle error function
             * Not send email if error exist
             */ catch (Exception $e) {

                //add to log
                Log::instance()->add(Log::INFO, 'Error : On Send email notification stock');
                Log::instance()->add(Log::INFO, 'Error : Send Notification stock email failed');

                //add log last tarce
                $error = $e->getTrace();
                Log::instance()->add(Log::INFO, print_r($error[0], true));
            }
        }

        return $booking_id;
    }

    public function xml_build($datas, $node_head, $warning_datas) {
        // Factory exchange
        $exchange = Model::factory('exchange');

        // Transaction
        $transaction = str_replace('OTA_', '', $node_head['head']);
        $transaction = substr($transaction, 0, -2) . 'RS';

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
            $envelope->setAttribute('xmlns:soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
            $head = $envelope->appendChild($doc->createElement('soap-env:Header'));
            $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
            $body->setAttribute('RequestId', $node_head['request_id']);
            $body->setAttribute('Transaction', $transaction);

            $response_node = $body->appendChild($doc->createElement(substr($node_head['head'], 0, -2) . 'RS'));
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('TimeStamp', $node_head['TimeStamp']);
            $response_node->setAttribute('EchoToken', $node_head['EchoToken']);
            $response_node->setAttribute('Target', $node_head['Target']);
            $response_node->setAttribute('Version', $node_head['Version']);

            $success = $response_node->appendChild($doc->createElement('Success'));

            if (count($warning_datas)) {
                $warnings = $response_node->appendChild($doc->createElement('Warnings'));
                foreach ($warning_datas as $warning_text => $warning_data) {

                    $warning = $warnings->appendChild($doc->createElement('Warning', $warning_data['text']));
                    $warning->setAttribute('ShortText', $warning_data['short_text']);
                }
            }

            $HotelReservations = $response_node->appendChild($doc->createElement('HotelReservations'));
            $HotelReservation = $HotelReservations->appendChild($doc->createElement('HotelReservation'));

            foreach ($datas as $campaign_id => $data) {
                // Booking Variable
                $vacation_reservation_id = $data['ID'];
                $hoterip_reservation_id = $data['booking_id'];

                // Booking Variable
                $vacation_reservation_id = $data['ID'];

                DB::insert('api_booking_vacations', array(
                            'booking_id',
                            'booking_vacation_id',
                            'hotel_id'
                        ))
                        ->values(array(
                            $hoterip_reservation_id,
                            $vacation_reservation_id,
                            $data['HotelCode']
                        ))
                        ->execute();

                // Date Process Variable
                // $vacation_process_time = $data['CreateDateTime'];
                $hoterip_process_time = date('c');

                $check_in = $data['Start'];
                $check_out = $data['End'];

                // RoomStays Group
                $RoomStays = $HotelReservation->appendChild($doc->createElement('RoomStays'));
//////////

                for ($i = 0; $i < $data['RoomStaysNum']; $i++) {
                    // Total Value
                    $total_price = 0;
                    $total_price_tax = 0;

                    // Singular RoomStay
                    $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));

                    $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                    $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                    $RoomType->setAttribute('RoomTypeCode', $data['RoomTypeCode']);
                    $RoomType->setAttribute('NumberOfUnits', $data['RoomRates'][$data['RoomTypeCode']]['NumberOfUnits']);

                    $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                    $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                    $RatePlan->setAttribute('RatePlanCode', $campaign_id);

                    $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription'));
                    $RatePlanDescription->setAttribute('Name', $data['promotions_descriptions']);

                    $MealsIncluded = $RatePlan->appendChild($doc->createElement('MealsIncluded'));

                    $breakfast = $data['is_breakfast_included'] ? 'BRE' : 'NOM';
                    $MealsIncluded->setAttribute('Name', $breakfast);

                    $service_tax_rate = (($data['hotel_service_charge_rate'] + $data['hotel_tax_rate']) + 100) / 100;
                    $room_name = $data['room_name'];

                    // Number Of Units
                    $NumberOfUnits = $data['RoomRates'][$data['RoomTypeCode']]['NumberOfUnits'];

                    // Cancelation Value
                    $cancellations = Controller_Vacation_Static::cancellations($campaign_id, $data['HotelCode'], FALSE, $data['Start']);

                    //set cancellation default if no exist
                    if (empty($cancellations)) {
                        $cancellations = array(
                            'first' => array(
                                'rule' => 8,
                                'within_days' => 10000,
                                'night_charged' => 0,
                                'percent_charged' => 100
                            )
                        );
                    }

                    $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));

                    $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                    $RoomRate->setAttribute('NumberOfUnits', $NumberOfUnits);
                    $RoomRate->setAttribute('EffectiveDate', $data['Start']);
                    $RoomRate->setAttribute('ExpireDate', $data['End']);
                    $RoomRate->setAttribute('RoomTypeCode', $data['RoomTypeCode']);
                    $RoomRate->setAttribute('RatePlanCode', $campaign_id);

                    foreach ($data['RoomRates'] as $item) {
                        $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                        // $Rate = $Rates->appendChild($doc->createElement('Rate'));

                        foreach ($item['item'] as $date => $room_item) {

                            $Rate = $Rates->appendChild($doc->createElement('Rate'));
                            $Rate->setAttribute('EffectiveDate', $date);
                            $Rate->setAttribute('ExpireDate', Date::formatted_time("$date + 1 days", 'Y-m-d'));
                            $Rate->setAttribute('RateTimeUnit', 'Day');

                            $base_single_price = ($room_item['price'] / $service_tax_rate);
                            $base_single_price = $base_single_price * $NumberOfUnits;
                            $base_single_price = $base_single_price + $room_item['extrabed_price'];
                            $base_single_price = $base_single_price + $room_item['surcharge_total'];
                            $base_single_price = round($base_single_price * $exchange->to($data['currency_id'], 1), 2);

                            $base_single_price_tax = $room_item['price'];
                            $base_single_price_tax = $base_single_price_tax * $NumberOfUnits;
                            $base_single_price_tax = $base_single_price_tax + $room_item['extrabed_price'];
                            $base_single_price_tax = $base_single_price_tax + ($room_item['surcharge_total']);
                            $base_single_price_tax = round($base_single_price_tax * $exchange->to($data['currency_id'], 1), 2);

                            // $total_single_price = $room_item['price'] / $service_tax_rate;
                            $total_single_price = $base_single_price;
                            // $total_single_price = round($total_single_price * $exchange->to($data['currency_id'], 1),2);
                            // $total_single_price_tax = $room_item['price'];
                            $total_single_price_tax = $base_single_price_tax;
                            // $total_single_price_tax = round($total_single_price_tax * $exchange->to($data['currency_id'], 1),2);

                            $Base = $Rate->appendChild($doc->createElement('Base'));
                            $Base->setAttribute('AmountBeforeTax', number_format(round($base_single_price, 2), '2', '', ''));
                            $Base->setAttribute('AmountAfterTax', number_format(round($base_single_price_tax, 2), '2', '', ''));
                            $Base->setAttribute('DecimalPlaces', 2);
                            $Base->setAttribute('CurrencyCode', 'USD');

                            $Total = $Rate->appendChild($doc->createElement('Total'));
                            $Total->setAttribute('AmountBeforeTax', number_format(round($total_single_price, 2), '2', '', ''));
                            $Total->setAttribute('AmountAfterTax', number_format(round($total_single_price_tax, 2), '2', '', ''));
                            $Total->setAttribute('DecimalPlaces', 2);
                            $Total->setAttribute('CurrencyCode', 'USD');

                            $total_price += $total_single_price;
                            $total_price_tax += $total_single_price_tax;
                        }
                    }

                    $RoomDescription = $RoomTypes->appendChild($doc->createElement('RoomDescription'));
                    $RoomDescription->appendChild($doc->createElement('Text', htmlspecialchars(utf8_encode($room_name))));

                    $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                    if (!empty($data['GuestCounts'])) {
                        if ($data['GuestCounts'][$i]['adult'] > 0) {
                            $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                            $GuestCount->setAttribute('AgeQualifyingCode', 10);
                            $GuestCount->setAttribute('Count', $data['GuestCounts'][$i]['adult']);
                        }
                        if ($data['GuestCounts'][$i]['child'] > 0) {
                            $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                            $GuestCount->setAttribute('AgeQualifyingCode', 8);
                            $GuestCount->setAttribute('Count', $data['GuestCounts'][$i]['child']);
                        }
                    }

                    $night_span = (strtotime($data['End']) - strtotime($data['Start'])) / (60 * 60 * 24);
                    $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                    $TimeSpan->setAttribute('Start', $check_in);
                    $TimeSpan->setAttribute('End', $check_out);
                    $TimeSpan->setAttribute('Duration', 'P' . $night_span . 'N');

                    $CancelPolicies = $RoomStay->appendChild($doc->createElement('CancelPolicies'));
                    foreach ($cancellations as $key => $cancellation) {
                        $CancelPenalty = $CancelPolicies->appendChild($doc->createElement('CancelPenalty'));


                        if (!empty($cancellation['rule'])) {
                            // Within Days

                            $within_days = (int) $cancellation['within_days'];

                            $within_days = Date::formatted_time("$check_in - $within_days days", 'Y-m-d');

                            // Non refundable
                            if ($cancellation['within_days'] >= 1000 AND $cancellation['percent_charged'] == 100) {
                                $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                //$DeadLine->setAttribute('AbsoluteDeadline', $cancellation['within_days']);
                                $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                $DeadLine->setAttribute('OffsetUnitMultiplier', 999);
                                $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                $AmountPercent->setAttribute('Percent', 100);
                                break;
                            }
                            // no show 1st night charged
                            elseif ($cancellation['within_days'] == 0 AND $cancellation['night_charged'] != 0 AND $cancellation['percent_charged'] == 0) {
                                $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                //$DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                //$DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);

                                $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                $AmountPercent->setAttribute('Percent', 100);
                                $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                            }
                            // no show percent charged
                            elseif ($cancellation['within_days'] == 0 AND $cancellation['night_charged'] == 0 AND $cancellation['percent_charged'] != 0) {

                                $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                //$DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                //$DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);

                                $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                $AmountPercent->setAttribute('Percent', $cancellation['percent_charged']);
                                // $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                            }
                            // within days show  night charged
                            elseif ($cancellation['within_days'] > 0 AND $cancellation['percent_charged'] == 0 AND $cancellation['night_charged'] != 0) {

                                if ((int) $cancellation['within_days'] > 999) {
                                    $cancellation['within_days'] = '999';
                                }

                                $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));

                                if (in_array($cancellation['rule'], array(5, 6, 7))) {
                                    $within_days = date('Y-m-d');
                                    $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                } else {
                                    $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                    $DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);
                                }


                                $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                // $AmountPercent->setAttribute('Percent', 100);
                                if ($cancellation['night_charged']) {
                                    $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                }
                            }
                            // within days show  charged
                            elseif ($cancellation['within_days'] > 0 AND $cancellation['night_charged'] == 0 AND $cancellation['percent_charged'] != 0) {



                                $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));

                                if (in_array($cancellation['rule'], array(5, 6, 7))) {
                                    $within_days = date('Y-m-d');
                                    $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                } else {
                                    $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                    $DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);
                                }
                                // $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                // $DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);
                                // $DeadLine->setAttribute('AbsoluteDeadline', $within_days);

                                $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                $AmountPercent->setAttribute('Percent', $cancellation['percent_charged']);
                                if ($cancellation['night_charged']) {
                                    $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                }
                            }
                            // free cancelaltion
                            elseif ($cancellation['within_days'] == 10000 AND $cancellation['percent_charged'] == 0 AND $cancellation['night_charged'] == 0) {
                                $CancelPenalty->setAttribute('PolicyCode', 'CFC');
                                break;
                            } else {
                                $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                //$DeadLine->setAttribute('AbsoluteDeadline', $check_in);
                                $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                $DeadLine->setAttribute('OffsetUnitMultiplier', 999);

                                $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));

                                if ($cancellation['percent_charged']) {
                                    $AmountPercent->setAttribute('Percent', $cancellation['percent_charged']);
                                }
                                if ($cancellation['night_charged']) {
                                    $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                }

                                break;
                            }
                        }
                        // non refundable for no cancellation
                        else {
                            $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                            $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                            //$DeadLine->setAttribute('AbsoluteDeadline', $check_in);
                            $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                            $DeadLine->setAttribute('OffsetUnitMultiplier', 999);

                            $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                            $AmountPercent->setAttribute('Percent', '100');
                            break;
                        }
                    }


                    $Total = $RoomStay->appendChild($doc->createElement('Total'));
                    $Total->setAttribute('AmountBeforeTax', number_format(round($total_price, 2), '2', '', ''));
                    $Total->setAttribute('AmountAfterTax', number_format(round($total_price_tax, 2), '2', '', ''));
                    $Total->setAttribute('DecimalPlaces', 2);
                    $Total->setAttribute('CurrencyCode', 'USD');

                    $BasicProperty = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                    $BasicProperty->setAttribute('HotelCode', $data['HotelCode']);
                }
            }

            $ResGlobalInfo = $HotelReservation->appendChild($doc->createElement('ResGlobalInfo'));
            $HotelReservationIDs = $ResGlobalInfo->appendChild($doc->createElement('HotelReservationIDs'));

            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $vacation_reservation_id);
            $HotelReservationID->setAttribute('ResID_Source', 'HIS_TOKYO');
            $HotelReservationID->setAttribute('ResID_Date', $hoterip_process_time);

            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $hoterip_reservation_id);
            $HotelReservationID->setAttribute('ResID_Source', 'HOTERIP');
            $HotelReservationID->setAttribute('ResID_Date', $hoterip_process_time);

            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $hoterip_reservation_id);
            $HotelReservationID->setAttribute('ResID_Source', 'Hotel');
            $HotelReservationID->setAttribute('ResID_Date', $hoterip_process_time);

            $response = $doc->saveXML();

            $this->output($response);
        } catch (Kohana_Exception $e) {
            $error['XML_build']['type'] = '2';
            $error['XML_build']['code'] = '450';

            $errors = Controller_Vacation_Static::errors($error, $data);
            $this->output($errors);
        }
    }

    public function output($response) {
        // old log
        //add to log
        // Log::instance()->add(Log::INFO, 'Vacation Reservation Response : '.$response);
        //new log
        CustomLog::factory()->add(2, 'DEBUG', 'Vacation Reservation Response : ' . $response);

        // Logout account
        A1::instance()->logout();

        $this->response
                ->headers('Content-Type', 'text/xml')
                ->body($response)
                ->status(200)
        ;
    }

    public function action_request() {
        // Set time limit
        set_time_limit(60);

        $xml_location = file_get_contents('php://input');
        // $xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/reservation.xml';
        // $data = file_get_contents($xml_location);
        // $xml_location = trim( stripslashes( $data ));
        // old log
        // Log::instance()->add(Log::INFO, 'Vacation Reservation Request : '.$xml_location);
        //new log
        CustomLog::factory()->add(2, 'DEBUG', 'Vacation Reservation Request : ' . $xml_location);

        if (!$xml_location) {
            $node_head = array(
                'head' => 'ResponseRQ',
                'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                'TimeStamp' => date('c'),
                'EchoToken' => '000',
                'Target' => 'Production',
                'Version' => '1.00',
                'PrimaryLangID' => 'En',
            );

            $error['Welcome']['type'] = '2';
            $error['Welcome']['code'] = '450';

            $errors = Controller_Vacation_Static::errors($error, $node_head);

            $this->output($errors);
        } else {
            $xml = new SimpleXMLElement($xml_location);

            $namespace = $xml->getDocNamespaces(TRUE);
            foreach ($namespace as $html_element => $value) {
                $xmlns = $value;
            }

            $head = $xml->children('soap-env', true)->Header->children()->Interface;
            $body = $xml->children('soap-env', true)->Body->children();
            $transaction = 'OTA_' . (string) $xml->children('soap-env', true)->Body->attributes()->Transaction;

            if ($transaction == 'OTA_HotelResRQ') {
                $request_id = 'OTA_' . (string) $xml->children('soap-env', true)->Body->attributes()->RequestId;

                $data = $this->checker($transaction, $head, $body, $xmlns, $request_id);
            } else {
                $node_head = array(
                    'head' => 'ResponseRQ',
                    'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                    'TimeStamp' => date('c'),
                    'EchoToken' => '000',
                    'Target' => 'Production',
                    'Version' => '1.00',
                    'PrimaryLangID' => 'En',
                );

                $error['False_request']['type'] = '2';
                $error['False_request']['code'] = '450';
                $errors = Controller_Vacation_Static::errors($error, $node_head);

                $this->output($errors);
            }
        }
    }

    public function checkItemForNotifications($item_ids) {
        try {
            $item_stock = array();
            $item_stock = DB::select('id', 'stock')
                    ->from('items')
                    ->where('id', 'IN', $item_ids)
                    ->where('stock', '<=', 0)
                    ->execute()
                    ->as_array();

            // $item_booking_pendings= array();
            $item_booking_pendings = DB::select('item_ids')
                    ->from('booking_pendings')
                    ->execute()
                    ->as_array();

            $item_send_to_mail = array();
            foreach ($item_stock as $key => $itm_stock) {
                if (!empty($item_booking_pendings)) {
                    foreach ($item_booking_pendings as $booking_pending => $val) {
                        if (in_array($itm_stock['id'], unserialize($val['item_ids']))) {
                            if (in_array($itm_stock['id'], $item_send_to_mail)) {
                                array_push($item_send_to_mail, $itm_stock['id']);
                            }
                        }
                    }
                } else {
                    array_push($item_send_to_mail, $itm_stock['id']);
                }
            }
            return $item_send_to_mail;
        }

        /**
         * Hendle error function
         * @return empty array() if error exist
         */ catch (Exception $e) {

            //add log
            Log::instance()->add(Log::INFO, 'Error :On Function checkItemForNotifications ');
            //add log last tarce
            $error = $e->getTrace();
            Log::instance()->add(Log::INFO, print_r($error[0], true));

            return array();
        }
    }

}
