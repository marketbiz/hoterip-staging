<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Trigger extends Controller {

    public static function checkHotelVacation($hotel_id) {
        $settings = DB::select('key', 'value')
                ->from('settings')
                ->where('key', 'IN', array('vacation_hotel_active_list', 'vacation_hotel_listed_only'))
                ->execute()
                ->as_array('key');

        $hotel_lists = json_decode($settings['vacation_hotel_active_list']['value']);

        if (($settings['vacation_hotel_listed_only']['value'] && in_array(intval($hotel_id), $hotel_lists)) || !$settings['vacation_hotel_listed_only']['value']) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function insertData($vacation_triger) {
        if (Controller_Vacation_Trigger::checkHotelVacation($vacation_triger['hotel_id'])) {
            //get data exist
            $datas_exist = DB::select()
                    ->from('api_vacation_updates')
                    ->where('hotel_id', '=', $vacation_triger['hotel_id'])
                    ->where('room_id', '=', $vacation_triger['room_id'])
                    ->where('campaign_id', '=', $vacation_triger['campaign_id'])
                    ->where('item_id', '=', $vacation_triger['item_id'])
                    ->where('status', '=', '0')
                    ->execute()
                    ->current();

            //set setatus.
            // $save_status = TRUE;
            //if data exist not empty
            if (!empty($datas_exist)) {
                if ($vacation_triger['type_trigger'] == 3 || $vacation_triger['type_trigger'] == 7
                ) {
                    if ($vacation_triger['start_date'] < $datas_exist['start_date']) {
                        $query_update = DB::update('api_vacation_updates')
                                ->set(array(
                                    'start_date' => $vacation_triger['start_date']
                                ))
                                ->where('id', '=', $datas_exist['id'])
                                ->execute();
                    }
                    if ($vacation_triger['end_date'] > $datas_exist['end_date']) {
                        $query_update = DB::update('api_vacation_updates')
                                ->set(array(
                                    'end_date' => $vacation_triger['end_date']
                                ))
                                ->where('id', '=', $datas_exist['id'])
                                ->execute();
                    }
                }
            } else {
                //insert per item if item_id is not null(bboking/cancel)
                if (!empty($vacation_triger['item_id'])) {
                    foreach ($vacation_triger['item_id'] as $key => $item_id) {
                        DB::insert('api_vacation_updates')
                                ->columns(array_keys($vacation_triger))
                                ->values(array($vacation_triger['hotel_id'], $vacation_triger['room_id'], $vacation_triger['campaign_id'], $item_id, NULL, NULL, NULL, 0))
                                ->execute();
                    }
                } else {
                    //insert data vacation trigger into database
                    DB::insert('api_vacation_updates')
                            ->columns(array_keys($vacation_triger))
                            ->values(array_values($vacation_triger))
                            ->execute();
                }
            }
        }
    }

    public static function updatePerItem($vacation) {
        //get data item valid
        $data_items = DB::select(
                        array('items.id', 'item_id'), array('items.room_id', 'room_id'), array('items.date', 'date'), array('items.minimum_night', 'minimum_night'), array('items.stock', 'stock'), array('items.price', 'price'), array('items.campaign_id', 'campaign_id')
                )
                ->from('items')
                ->join('rooms')->on('rooms.id', '=', 'items.room_id')
                ->where('items.id', '=', $vacation['item_id'])
                ->where('rooms.is_breakfast_included', '=', 1)
                ->where('rooms.hoterip_campaign', '=', 0)
                ->execute()
                ->as_array();

        //get data campaign valid
        $campaigns = DB::select(
                        array('campaigns.id', 'campaign_id')
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->where('campaigns_rooms.room_id', '=', $vacation['room_id'])
                ->where('campaigns.is_show', '=', 1)
                ->where('campaigns.booking_end_date', '>=', date('Y-m-d'))
                ->execute()
                ->as_array();

        foreach ($campaigns as $campaign) {
            foreach ($data_items as $item) {
                //generate data post
                $post['start_date'] = $item['date'];
                $start_date = $post['start_date'];
                $post['end_date'] = Date::formatted_time("$start_date + 1 days", 'Y-m-d');
                $post['room_id'] = $vacation['room_id'];
                $post['campaign_id'] = $campaign['campaign_id'];
                $post['submit'] = 'Submit';
                $post['hotel_id'] = $vacation['hotel_id'];

                ########################Generate Data Post#####################################
                for ($i = 0; $i < 1; $i++) {
                    $date = Date::formatted_time("$start_date + $i days", 'Y-m-d');
                    $post['dates'][$date] = $date;
                }

                for ($i = 0; $i < 1; $i++) {
                    $date = Date::formatted_time("$start_date + $i days", 'Y-m-d');
                    $post['stocks'][$date] = $item['stock'];
                }

                for ($i = 0; $i < 1; $i++) {
                    $date = Date::formatted_time("$start_date + $i days", 'Y-m-d');
                    $post['price'][$date] = $item['price'];
                }

                for ($i = 0; $i < 1; $i++) {
                    $date = Date::formatted_time("$start_date + $i days", 'Y-m-d');
                    $post['minimum_nights'][$date] = $item['minimum_night'];
                }
                ##############################################################################
                //proccess API Vavation
                Controller_Vacation_Notifications_Availability::item($post, TRUE);
                Controller_Vacation_Notifications_Amount::item($post);
            }
        }
    }

    public static function updateDatePeriod($vacation) {
        //get status hotel
        $status_hotel = DB::select('hotels.active')
                ->from('hotels')
                ->where('hotels.id', '=', $vacation['hotel_id'])
                ->execute()
                ->as_array();

        //get room valid
        $now = date('Y-m-d');
        $rooms = DB::select(
                        array('rooms.id', 'room_id')
                )
                ->from('rooms')
                ->where('rooms.hotel_id', '=', $vacation['hotel_id']);
        if ($vacation['room_id'] > 0) {
            $rooms = $rooms
                    //type triggger 4
                    ->where('rooms.id', '=', $vacation['room_id']);
        }
        $rooms = $rooms
                ->where('rooms.is_breakfast_included', '=', 1)
                ->where('rooms.hoterip_campaign', '=', 0)
                ->execute()
                ->as_array();

        foreach ($rooms as $room) {
            //get campaigns valid
            $campaigns = DB::select(
                            array('campaigns.id', 'campaign_id'), array('campaigns.is_default', 'is_default')
                    )
                    ->from('campaigns')
                    ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                    ->where('campaigns.is_show', '=', 1)
                    ->where('campaigns_rooms.room_id', '=', $room['room_id']);

            //if type trigger 2 dont query by booking_end_date
            if ($vacation['type_trigger'] != 2) {
                $campaigns = $campaigns
                        ->where('campaigns.booking_end_date', '>=', date('Y-m-d'));
            }

            if ($vacation['campaign_id'] > 0) {
                $campaigns = $campaigns
                        ->where('campaigns.id', '=', $vacation['campaign_id']);
            }

            $campaigns = $campaigns
                    ->execute()
                    ->as_array();

            foreach ($campaigns as $key => $campaign) {
                unset($post);
                //get items valid
                $items = DB::select(
                                array('items.stock', 'stock'), array('items.date', 'date'), array('items.price', 'price'), array('items.campaign_id', 'campaign_id'), array('items.minimum_night', 'minimum_night'), array('items.is_blackout', 'is_blackout'), array('items.is_campaign_blackout', 'is_campaign_blackout')
                        )
                        ->from('items');
                if ($vacation['type_trigger'] == 3 || $vacation['type_trigger'] == 7
                ) {
                    $items = $items
                            ->where('items.date', 'BETWEEN', array($vacation['start_date'], $vacation['end_date']))
                            ->where('items.campaign_id', '=', 0)
                            ->where('items.room_id', '=', $room['room_id']);
                    // ->where('items.hotel', '=', $vacation['hotel_id']);
                } else {
                    $items = $items
                            ->where('items.campaign_id', '=', 0)
                            ->where('items.room_id', '=', $room['room_id'])
                            ->where('items.date', '>=', date('Y-m-d'));
                }

                $items = $items
                        ->order_by('date')
                        ->execute()
                        ->as_array('date')
                ;

                $last_array = end($items);
                $date_start = date_create(date('Y-m-d'));
                $date_end = date_create($last_array['date']);

                if ($vacation['type_trigger'] == 3 || $vacation['type_trigger'] == 7
                ) {
                    $campaign_default = DB::select('campaigns.id')
                            ->from('campaigns')
                            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                            ->where('campaigns.is_default', '=', 1)
                            ->where('campaigns_rooms.room_id', '=', $room['room_id'])
                            ->execute()
                            ->current();

                    $cancellation_available = DB::select('cancellations.end_date')
                            ->join('cancellations')->on('cancellations.id', '=', 'campaigns_cancellations.cancellation_id')
                            ->from('campaigns_cancellations')
                            ->where('campaigns_cancellations.campaign_id', '=', $campaign_default['id'])
                            ->where('cancellations.end_date', '>=', $vacation['start_date'])
                            // ->where('cancellations.end_date', '<=', $vacation['end_date'])
                            ->order_by('cancellations.end_date', 'desc')
                            ->execute()
                            ->current()
                    ;

                    if ($vacation['end_date'] > $cancellation_available['end_date']) {
                        $vacation['end_date'] = $cancellation_available['end_date'];
                    }

                    //change variable now to start_date
                    $now = $vacation['start_date'];

                    $date_start = date_create($vacation['start_date']);
                    $date_end = date_create($vacation['end_date']);
                }

                $diff = date_diff($date_start, $date_end)->days;
                $diff = $diff + 1;


                if (!empty($items)) {

                    //+++++++++++++++++++++++++ Generate Date Post +++++++++++++++++++++++++

                    for ($i = 0; $i < $diff; $i++) {
                        $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                        $post['dates'][$date] = $date;
                    }

                    for ($i = 0; $i < $diff; $i++) {
                        $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                        $post['stocks'][$date] = $items[$date]['stock'];

                        if (empty($post['stocks'][$date])) {
                            $post['stocks'][$date] = 0;
                        }
                    }

                    for ($i = 0; $i < $diff; $i++) {
                        $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                        $post['minimum_nights'][$date] = $items[$date]['minimum_night'];

                        if (empty($post['minimum_nights'][$date])) {
                            $post['minimum_nights'][$date] = 0;
                        }
                    }

                    for ($i = 0; $i < $diff; $i++) {
                        $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                        $post['price'][$date] = $items[$date]['price'];

                        if (empty($post['price'][$date])) {
                            $post['price'][$date] = 0;
                        }

                        if ($post['price'][$date] == 0) {
                            $post['is_blackouts'][$date] = 1;
                        }
                    }

                    foreach ($items as $date => $value) {
                        if ($value['is_blackout'] == 1) {
                            $post['is_blackouts'][$date] = $value['is_blackout'];
                        }
                    }


                    foreach ($items as $date => $value) {
                        if ($value['is_campaign_blackout'] == 1) {

                            $post['is_campaign_blackouts'][$date] = $value['is_campaign_blackout'];
                        }
                    }

                    $post['submit'] = 'Submit';
                    $post['room_id'] = $room['room_id'];
                    $post['hotel_id'] = $vacation['hotel_id'];
                    $post['home'] = TRUE;

                    //check hotel want to active or not
                    if ($status_hotel[0]['active'] == 0) {
                        $post['hotel_active_vacation'] = FALSE;
                    } else {
                        $post['hotel_active_vacation'] = TRUE;
                    }

                    if (!$campaign['is_default']) {
                        $post['campaign_id'] = $campaign['campaign_id'];
                    }
                    $post['start_date'] = $now;
                    $post['end_date'] = date('Y-m-d', strtotime($last_array['date'] . "+ 1 days"));

                    // Vacation send API proccess
                    Controller_Vacation_Notifications_Availability::item($post);
                    Controller_Vacation_Notifications_Rateplan::item($post);
                    Controller_Vacation_Notifications_Amount::item($post);
                }
            }
        }
    }

    public static function promotionDelete($vacation) {
        //Set data from vacation trigger
        $rooms = DB::select('id')
                ->from('rooms')
                ->where('hotel_id', '=', $vacation['hotel_id'])
                ->where('hoterip_campaign', '=', 0)
                ->where('is_breakfast_included', '=', 1)
                ->execute()
                ->as_array()
        ;

        foreach ($rooms as $room) {
            $items_datas = array(
                $vacation['campaign_id'] => array(
                    'campaign_id' => $vacation['campaign_id'],
                    'room_id' => $room['id'],
                    'campaigns_minimum_night' => 1,
                    // 'days_in_advance' => $campaign->days_in_advance,
                    // 'within_days' => $campaign->within_days_of_arrival,
                    // 'is_default' => $campaign->is_default,
                    // 'type' => $campaign->type,
                    'stay_start_date' => $vacation['start_date'],
                    'stay_end_date' => $vacation['end_date'],
                    'items' => array(
                        0 => array(
                            'start' => $vacation['start_date'],
                            'end' => $vacation['end_date'],
                            'restriction' => 'Master',
                            'status' => 'Close',
                            'room_id' => $room['id'],
                            'campaign_id' => $vacation['campaign_id'],
                            'stock' => 0,
                            'minimum_night' => 0
                        )
                    )
                )
            );

            $post['hotel_id'] = $vacation['hotel_id'];

            Controller_Vacation_Notifications_Availability::process($post, $items_datas);
        }
    }

    public static function roomDelete($vacation) {
        //Set data from vacation trigger
        $campaigns = DB::select('campaigns_rooms.campaign_id')
                ->from('campaigns_rooms')
                ->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
                ->where('campaigns_rooms.room_id', '=', $vacation['room_id'])
                ->execute()
                ->as_array()
        ;

        //get data all items valid
        $date_last_item = DB::select('date')
                ->from('items')
                ->where('room_id', '=', $vacation['room_id'])
                ->where('campaign_id', '=', 0)
                ->order_by('date', 'desc')
                ->limit(1)
                ->execute()
                ->current();

        foreach ($campaigns as $campaign) {
            $items_datas = array(
                $campaign['campaign_id'] => array(
                    'campaign_id' => $campaign['campaign_id'],
                    'room_id' => $vacation['room_id'],
                    'campaigns_minimum_night' => 1,
                    // 'days_in_advance' => $campaign->days_in_advance,
                    // 'within_days' => $campaign->within_days_of_arrival,
                    // 'is_default' => $campaign->is_default,
                    // 'type' => $campaign->type,
                    'stay_start_date' => date('Y-m-d'),
                    'stay_end_date' => $date_last_item['date'],
                    'items' => array(
                        0 => array(
                            'start' => date('Y-m-d'),
                            'end' => $date_last_item['date'],
                            'restriction' => 'Master',
                            'status' => 'Close',
                            'room_id' => $vacation['room_id'],
                            'campaign_id' => $campaign['campaign_id'],
                            'stock' => 0,
                            'minimum_night' => 0
                        )
                    )
                )
            );

            $post['hotel_id'] = $vacation['hotel_id'];

            Controller_Vacation_Notifications_Availability::process($post, $items_datas);
        }
    }

    public static function roomDeleteV2($vacation) {
        //Set data from vacation trigger
        $campaigns = DB::select('campaigns_rooms.campaign_id')
                ->from('campaigns_rooms')
                ->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
                ->where('campaigns_rooms.room_id', '=', $vacation['room_id'])
                ->execute()
                ->as_array()
        ;

        foreach ($campaigns as $campaign) {
            $items_datas = array(
                $campaign['campaign_id'] => array(
                    'campaign_id' => $campaign['campaign_id'],
                    'room_id' => $vacation['room_id'],
                    'campaigns_minimum_night' => 1,
                    // 'days_in_advance' => $campaign->days_in_advance,
                    // 'within_days' => $campaign->within_days_of_arrival,
                    // 'is_default' => $campaign->is_default,
                    // 'type' => $campaign->type,
                    'stay_start_date' => date('Y-m-d'),
                    'stay_end_date' => $vacation['end_date'],
                    'items' => array(
                        0 => array(
                            'start' => date('Y-m-d'),
                            'end' => $vacation['end_date'],
                            'restriction' => 'Master',
                            'status' => 'Close',
                            'room_id' => $vacation['room_id'],
                            'campaign_id' => $campaign['campaign_id'],
                            'stock' => 0,
                            'minimum_night' => 0
                        )
                    )
                )
            );

            $post['hotel_id'] = $vacation['hotel_id'];

            Controller_Vacation_Notifications_Availability::process($post, $items_datas);
        }
    }

}
