<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Cancel extends Controller {
    /*
     * List Backend Model
     * Booking : Booking
     */

    public function checker($transaction, $head, $body, $xmlns, $request_id) {
        $data = array();

        $OTA_CancelRQ = $body->$transaction->attributes();
        $node_head = array(
            'head' => $transaction,
            'request_id' => $request_id,
            'xmlns' => $xmlns,
            'TimeStamp' => (string) $OTA_CancelRQ->TimeStamp,
            'PrimaryLangID' => (string) $OTA_CancelRQ->PrimaryLangID,
            'Target' => (string) $OTA_CancelRQ->Target,
            'Version' => (string) $OTA_CancelRQ->Version,
            'EchoToken' => (string) $body->$transaction->attributes()->EchoToken,
            'CancelType' => (string) $OTA_CancelRQ->CancelType,
        );

        $auth = array(
            'User' => (string) $head->ComponentInfo->attributes()->User,
            'Pwd' => (string) $head->ComponentInfo->attributes()->Pwd,
            'key' => '',
            'source' => '',
        );

        // Check Target Environment
        // $vacation_environment = strtolower((string)$OTA_CancelRQ->Target) == 'production' ? 'production' : 'development';
        $vacation_environment = strtolower((string) $OTA_CancelRQ->Target) == 'production' ? Kohana::PRODUCTION : Kohana::DEVELOPMENT;

        // Check node head null
        if (empty($node_head['xmlns']) || empty($node_head['TimeStamp']) || empty($node_head['PrimaryLangID']) || empty($node_head['Target']) || empty($node_head['Version']) || empty($node_head['EchoToken'])) {
            $error['Empty_Property']['type'] = '2';
            $error['Empty_Property']['code'] = '321';
        }
        // Check Target Environment
        elseif (Kohana::$environment != $vacation_environment) {
            $error['Authorization']['type'] = '6';
            $error['Authorization']['code'] = '146';
        }
        // 
        // Check Auth null request
        elseif (empty($auth['User']) || empty($auth['Pwd'])) {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '321';
        } else {
            // Check Auth
            if (Controller_Vacation_Static::auth($auth) == FALSE) {
                $error['Authentification']['type'] = '4';
                $error['Authentification']['code'] = '9900';
            } else {
                $error = array();

                $UniqueID = $body->$transaction->UniqueID;

                $data = array(
                    'Type' => (string) $UniqueID->attributes()->Type,
                    'ID' => (string) $UniqueID->attributes()->ID,
                );

                // Check Null request
                if (empty($data['Type']) || empty($data['ID'])
                ) {
                    $error['Type_or_ID_empty']['type'] = '2';
                    $error['Type_or_ID_empty']['code'] = '321';
                }

                // Check Format request
                if (!is_numeric($data['Type'])) {
                    $error['Type']['type'] = '3';
                    $error['Type']['code'] = '459';
                }
            }
        }

        $data = array_merge($data, $node_head);

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $node_head);
            $this->output($errors);
        } else {
            return $this->process($data);
        }
    }

    public function process($data) {
        $error = array();

        $booking = ORM::factory('booking')
                ->select(
                        'booking_datas.*', array(DB::expr('CONCAT(hotels.timezone)'), 'hotel_timezone')
                )
                ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                ->join('api_booking_vacations')->on('api_booking_vacations.booking_id', '=', 'bookings.id')
                ->join('hotels')->on('hotels.id', '=', 'api_booking_vacations.hotel_id')
                ->where('api_booking_vacations.booking_vacation_id', '=', $data['ID'])
                ->find();

        // Is Booking Exist
        if (!empty($booking->id)) {
            // Is Booking canceled
            if (!$booking->is_canceled) {
                // If cancel no show 
                if ((strtotime('now') + Date::offset($booking->hotel_timezone) >= strtotime($booking->check_in_date))) {
                    // Booking can be canceled if today >= check in date, means the guest didn't show at the hotel
                    $can_cancel = (strtotime('now') + Date::offset($booking->hotel_timezone) >= strtotime($booking->check_in_date)) ? TRUE : FALSE;
                } else {
                    // Booking can be canceled by API role
                    $can_cancel = A2::instance()->allowed('api', 'edit');
                }

                if ($can_cancel) {
                    try {
                        $booking->is_canceled = 1;
                        $booking->canceled_timestamp = time();
                        $booking->update();

                        // Get user language when booking
                        $language = ORM::factory('language')
                                ->where('id', '=', $booking->language_id)
                                ->find();

                        // Get hotel admins
                        $admin_emails = ORM::factory('admin')
                                ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                                ->join('hotels')->on('hotels.id', '=', 'admins_hotels.hotel_id')
                                ->join('rooms')->on('rooms.hotel_id', '=', 'hotels.id')
                                ->where('rooms.id', '=', $booking->room_id)
                                ->where('admins_hotels.admin_id', '<>', A2::instance()->get_user()->id)
                                ->find_all()
                                ->as_array('email', 'name');

                        // Create message
                        $message = Kostache::factory('email/cancel')
                                ->set('booking', $booking)
                                ->set('language', $language);

                        set_time_limit(300);

                        // Send email
                        Email::factory(I18n::get('Booking Canceled', $language->code), $message)
                                ->from(Kohana::config('application')->get('email'), 'Hoterip')
                                ->to($booking->email)
                                ->bcc($admin_emails)
                                ->send();

                        // If success
                        $data['Status'] = 'Cancelled';
                        $data['booking_id'] = $booking->id;
                    } catch (Exception $e) {
                        $error['Booking']['type'] = '2';
                        $error['Booking']['code'] = '450';
                    }
                } else {
                    $error['Booking']['type'] = '2';
                    $error['Booking']['code'] = '148';
                }
            } else {
                $error['Invalid']['type'] = '2';
                $error['Invalid']['code'] = '95';
            }
        } else {
            $error['Not_Found']['type'] = '2';
            $error['Not_Found']['code'] = '97';
        }

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $data);
            $this->output($errors);
        } else {
            // Build response
            return $this->xml_build($data);
        }
    }

    public function xml_build($data) {

        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n";

        $xml .= '<soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">' . "\r\n";
        $xml .= '<soap-env:Header/>' . "\r\n";
        $xml .= '<soap-env:Body';
        $xml .= ' RequestId="' . $data['request_id'] . '"';
        $xml .= ' Transaction="' . str_replace('OTA_', '', $data['head']) . '"';
        $xml .= '>' . "\r\n";

        $xml .= '<' . substr($data['head'], 0, -2) . 'RS';
        $xml .= ' xmlns="http://www.opentravel.org/OTA/2003/05"';
        $xml .= ' TimeStamp="' . $data['TimeStamp'] . '"';
        $xml .= ' EchoToken="' . $data['EchoToken'] . '"';
        $xml .= ' Target="' . $data['Target'] . '"';
        $xml .= ' Version="' . $data['Version'] . '"';
        $xml .= ' Status="' . $data['Status'] . '"';
        $xml .= '>' . "\r\n";

        $xml .= '<Success/>' . "\r\n";

        $xml .= '<UniqueID';
        $xml .= ' Type="15"';
        $xml .= ' ID="' . $data['booking_id'] . '"';
        $xml .= '/>' . "\r\n";

        $xml .= '<CancelInfoRS>';
        $xml .= '<UniqueID';
        $xml .= ' Type="15"';
        $xml .= ' ID="' . $data['ID'] . '"';
        $xml .= '/>' . "\r\n";
        $xml .= '</CancelInfoRS>' . "\r\n";

        $xml .= '</' . substr($data['head'], 0, -2) . 'RS>' . "\r\n";

        $xml .= '</soap-env:Body>' . "\r\n";
        $xml .= '</soap-env:Envelope>' . "\r\n";

        $this->output($xml);
    }

    public function output($response) {
        // Auth Logout
        A1::instance()->logout();

        // Build ouput
        $this->response
                ->headers('Content-Type', 'text/xml')
                ->body($response)
                ->status(200)
        ;

        // log old
        // Log::instance()->add(Log::INFO, 'Vacation Cancelation Response : '.$response);
        //new log
        CustomLog::factory()->add(2, 'DEBUG', 'Vacation Cancelation Response : ' . $response);
    }

    public function action_request() {
        // Set time limit
        set_time_limit(60);

        $xml_location = file_get_contents('php://input');

        /*
         * Tool Test
         */
        // $xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/cancel.xml';
        // $data = file_get_contents($xml_location);
        // $xml_location = trim( stripslashes( $data ));
        //old log
        // Log::instance()->add(Log::INFO, 'Vacation Cancelation Request : '.$xml_location);

        CustomLog::factory()->add(2, 'DEBUG', 'Vacation Cancelation Request : ' . $xml_location);

        if (!$xml_location) {
            $node_head = array(
                'head' => 'ResponseRQ',
                'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                'TimeStamp' => date('c'),
                'EchoToken' => '000',
                'Target' => 'Production',
                'Version' => '1.00',
                'PrimaryLangID' => 'En',
            );

            $error['Welcome']['type'] = '2';
            $error['Welcome']['code'] = '450';

            $errors = Controller_Vacation_Static::errors($error, $node_head);

            $this->output($errors);
        } else {

            $xml = new SimpleXMLElement($xml_location);

            $namespace = $xml->getDocNamespaces(TRUE);
            foreach ($namespace as $html_element => $value) {
                $xmlns = $value;
            }

            $head = $xml->children('soap-env', true)->Header->children()->Interface;
            $body = $xml->children('soap-env', true)->Body->children();
            $transaction = 'OTA_' . (string) $xml->children('soap-env', true)->Body->attributes()->Transaction;

            if ($transaction == 'OTA_CancelRQ') {
                $request_id = 'OTA_' . (string) $xml->children('soap-env', true)->Body->attributes()->RequestId;

                $data = $this->checker($transaction, $head, $body, $xmlns, $request_id);
            } else {
                $node_head = array(
                    'head' => 'ResponseRQ',
                    'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                    'TimeStamp' => date('c'),
                    'EchoToken' => '000',
                    'Target' => 'Production',
                    'Version' => '1.00',
                    'PrimaryLangID' => 'En',
                );

                $error['False_request']['type'] = '2';
                $error['False_request']['code'] = '450';
                $errors = Controller_Vacation_Static::errors($error, $node_head);

                $this->output($errors);
            }
        }
    }

}
