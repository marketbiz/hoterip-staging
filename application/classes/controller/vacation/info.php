<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Info extends Controller {
    /*
     * List Frontend Model
     * Hotel :search_hotel, search_campaign, get_campaign
     * Room capacity :getroom, checkroom, extrabed
     * Country/City  : getbysegment
     * Timezone  : timezone identifier
     * Setting  : get
     * Exchane  : To
     */

    public function checker($transaction, $head, $body, $xmlns, $request_id) {
        $data = array();

        $OTA = $body->$transaction->attributes();

        $node_head = array(
            'head' => $transaction,
            'request_id' => $request_id,
            'xmlns' => $xmlns,
            'TimeStamp' => (string) $OTA->TimeStamp,
            'PrimaryLangID' => (string) $OTA->PrimaryLangID,
            'Target' => (string) $OTA->Target,
            'Version' => (string) $OTA->Version,
            'EchoToken' => (string) $body->$transaction->attributes()->EchoToken,
        );

        $auth = array(
            'User' => (string) $head->ComponentInfo->attributes()->User,
            'Pwd' => (string) $head->ComponentInfo->attributes()->Pwd,
            'key' => '',
            'source' => '',
        );

        // Check Target Environment
        // $vacation_environment = strtolower((string)$OTA->Target) == 'production' ? 'production' : 'development';
        $vacation_environment = strtolower((string) $OTA->Target) == 'production' ? Kohana::PRODUCTION : Kohana::DEVELOPMENT;

        // Check node head null
        if (empty($node_head['xmlns']) || empty($node_head['TimeStamp']) || empty($node_head['PrimaryLangID']) || empty($node_head['Target']) || empty($node_head['Version']) || empty($node_head['EchoToken'])) {
            $error['Empty_Property']['type'] = '2';
            $error['Empty_Property']['code'] = '321';
        }
        // Check Target Environment
        elseif (Kohana::$environment != $vacation_environment) {
            $error['Authorization']['type'] = '6';
            $error['Authorization']['code'] = '146';
        }
        // Check Auth null request
        elseif (empty($auth['User']) || empty($auth['Pwd'])) {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '321';
        } else {
            // Check Auth
            if (Controller_Vacation_Static::auth($auth) == FALSE) {
                $error['Authentification']['type'] = '4';
                $error['Authentification']['code'] = '9900';
            } else {
                $error = array();

                $HotelDescriptiveInfos = $body->$transaction->HotelDescriptiveInfos->HotelDescriptiveInfo;

                foreach ($HotelDescriptiveInfos as $HotelDescriptiveInfo) {

                    $hotel_id = (string) $HotelDescriptiveInfo->attributes()->HotelCode;
                    $data['Info'][$hotel_id] = array(
                        'HotelCode' => $hotel_id,
                        'SendData' => (string) $HotelDescriptiveInfo->HotelInfo->attributes()->SendData,
                        'SendRefPoints' => (string) $HotelDescriptiveInfo->AreaInfo->attributes()->SendRefPoints,
                        'SendDistribSystems' => (string) $HotelDescriptiveInfo->AffiliationInfo->attributes()->SendDistribSystems,
                        'InfoSendData' => (string) $HotelDescriptiveInfo->ContactInfo->attributes()->SendData,
                    );

                    // Check Null request
                    if (empty($data['Info'][$hotel_id]['HotelCode']) || empty($data['Info'][$hotel_id]['SendData']) || empty($data['Info'][$hotel_id]['SendRefPoints']) || empty($data['Info'][$hotel_id]['SendDistribSystems']) || empty($data['Info'][$hotel_id]['InfoSendData'])
                    ) {
                        $error['Empty_Property']['type'] = '2';
                        $error['Empty_Property']['code'] = '321';
                    }

                    // Check Format request
                    if (strtolower($data['Info'][$hotel_id]['SendData']) != 'false' && strtolower($data['Info'][$hotel_id]['SendData']) != 'true'
                    ) {
                        $error['SendData']['type'] = '3';
                        $error['SendData']['code'] = '459';
                    }

                    if (strtolower($data['Info'][$hotel_id]['SendRefPoints']) != 'false' && strtolower($data['Info'][$hotel_id]['SendRefPoints']) != 'true'
                    ) {
                        $error['SendRefPoints']['type'] = '3';
                        $error['SendRefPoints']['code'] = '459';
                    }

                    if (strtolower($data['Info'][$hotel_id]['SendDistribSystems']) != 'false' && strtolower($data['Info'][$hotel_id]['SendDistribSystems']) != 'true'
                    ) {
                        $error['SendDistribSystems']['type'] = '3';
                        $error['SendDistribSystems']['code'] = '459';
                    }

                    if (strtolower($data['Info'][$hotel_id]['InfoSendData']) != 'false' && strtolower($data['Info'][$hotel_id]['InfoSendData']) != 'true'
                    ) {
                        $error['InfoSendData']['type'] = '3';
                        $error['InfoSendData']['code'] = '459';
                    }

                    if (!ctype_alnum($data['Info'][$hotel_id]['HotelCode'])) {
                        $error['HotelCode']['type'] = '3';
                        $error['HotelCode']['code'] = '459';
                    }
                }
            }
        }

        $data = array_merge($data, $node_head);

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $node_head);
            $this->output($errors);
        } else {
            return $this->process($data);
        }
    }

    public function process($data) {
        $hotel_query = array(
            'language_id' => Controller_Vacation_Static::default_language(),
            'check_in' => '0000-01-01',
            'check_out' => '9999-12-31',
            'number_of_rooms' => 1,
            'capacities' => 1,
            'now' => date('Y-m-d'),
        );

        foreach ($data['Info'] as $key => $info) {
            $hotel_ids[] = $info['HotelCode'];

            // Get Hotel Data
            $hotels = ORM::factory('hotel')
                    ->select(
                            array('hotels.id', 'hotel_id'), array('hotels.timezone', 'hotel_timezone'), array('hotels.child_age_until', 'child_age_until'), array('hotel_texts.name', 'hotel_name'), array('rooms.id', 'room_id'), array('rooms.size', 'room_size'), array('rooms.is_breakfast_included', 'is_breakfast_included'), array('room_texts.name', 'name'), array('room_capacities.number_of_adults', 'number_of_adults'), array('room_capacities.number_of_children', 'number_of_children'), array('campaigns.id', 'campaign_id'), array('campaigns.is_default', 'campaign_default'), array('campaigns.type', 'type'), array('campaigns.minimum_number_of_nights', 'min_nights'), array('campaigns.minimum_number_of_rooms', 'min_rooms'), array('campaigns.days_in_advance', 'early_days'), array('campaigns.within_days_of_arrival', 'last_days'), array('campaigns.number_of_free_nights', 'free_night'), array('campaigns.discount_rate', 'rate'), array('campaigns.discount_amount', 'amount'), array('district_texts.name', 'districts'), array('city_texts.name', 'city'), array('country_texts.name', 'country'), array('room_facilities.is_air_conditioner_available', 'is_air_conditioner_available'), array('room_facilities.is_alarm_clock_available', 'is_alarm_clock_available'), array('room_facilities.is_balcony_available', 'is_balcony_available'), array('room_facilities.is_bathrobe_available', 'is_bathrobe_available'), array('room_facilities.is_bathtub_available', 'is_bathtub_available'), array('room_facilities.is_body_lotion_available', 'is_body_lotion_available'), array('room_facilities.is_cable_tv_available', 'is_cable_tv_available'), array('room_facilities.is_coffee_maker_available', 'is_coffee_maker_available'), array('room_facilities.is_complimentary_water_bottle_available', 'is_complimentary_water_bottle_available'), array('room_facilities.is_cotton_bud_available', 'is_cotton_bud_available'), array('room_facilities.is_dvd_player_available', 'is_dvd_player_available'),
                            // array('room_facilities.is_fan_available', 'is_fan_available'),
                            array('room_facilities.is_hair_dryer_available', 'is_hair_dryer_available'), array('room_facilities.is_idd_telephone_available', 'is_idd_telephone_available'), array('room_facilities.is_independent_shower_room_available', 'is_independent_shower_room_available'), array('room_facilities.is_safety_box_available', 'is_safety_box_available'), array('room_facilities.is_internet_jack_available', 'is_internet_jack_available'), array('room_facilities.is_iron_available', 'is_iron_available'),
                            // array('room_facilities.is_jacuzzi_available', 'is_jacuzzi_available'),
                            // array('room_facilities.is_kitchen_set_available', 'is_kitchen_set_available'),
                            // array('room_facilities.is_living_room_available', 'is_living_room_available'),
                            array('room_facilities.is_mini_bar_available', 'is_mini_bar_available'), array('room_facilities.is_mosquito_equipment_available', 'is_mosquito_equipment_available'), array('room_facilities.is_moveable_shower_head_available', 'is_moveable_shower_head_available'), array('room_facilities.is_radio_available', 'is_radio_available'), array('room_facilities.is_room_wear_available', 'is_room_wear_available'), array('room_facilities.is_shampoo_available', 'is_shampoo_available'), array('room_facilities.is_shaver_available', 'is_shaver_available'), array('room_facilities.is_soap_available', 'is_soap_available'), array('room_facilities.is_toothbrush_available', 'is_toothbrush_available'), array('room_facilities.is_towel_available', 'is_towel_available'), array('room_facilities.is_tv_available', 'is_tv_available')
                    )
                    ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                    ->join('hotel_services')->on('hotels.id', '=', 'hotel_services.hotel_id')
                    ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
                    ->join('room_facilities')->on('rooms.id', '=', 'room_facilities.room_id')
                    ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
                    ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                    ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                    ->join('campaigns_languages')->on('campaigns.id', '=', 'campaigns_languages.campaign_id')
                    ->join('room_texts', 'LEFT')->on('room_texts.room_id', '=', 'rooms.id')
                    ->join('districts')->on('districts.id', '=', 'hotels.district_id')
                    ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
                    ->join('cities')->on('hotels.city_id', '=', 'cities.id')
                    ->join('city_texts')->on('hotels.city_id', '=', 'city_texts.city_id')
                    ->join('countries')->on('cities.country_id', '=', 'countries.id')
                    ->join('country_texts')->on('cities.country_id', '=', 'country_texts.country_id')
                    ->where_open()
                    // ->where('hotel_texts.language_id', '=', Controller_Vacation_Static::default_language())
                    // ->where('campaigns_languages.language_id', '=', Controller_Vacation_Static::default_language())
                    ->where('hotels.id', '=', $info['HotelCode'])
                    ->where('campaigns.is_show', '=', 1)
                    ->where_close()
                    ->and_where_open()
                    ->where('rooms.hoterip_campaign', '=', 0)
                    ->where('rooms.is_breakfast_included', '=', 1)

                    //select for campaign active
                    ->where('campaigns.booking_end_date', '>', Date('Y-m-d'))

                    //request date 2014-06-16 -> campaign early bird send again to vacation
                    // ->where('campaigns.days_in_advance', '=', 0)
                    ->and_where_close()
                    // ->execute();
                    ->group_by('hotel_id')
                    ->group_by('room_id')
                    ->group_by('campaign_id')
                    ->find_all();

            $hotel_data = array();
            $max_adults = 0;
            $min_adults = 1;

            if (count($hotels)) {
                foreach ($hotels as $key => $hotel) {

                    //get room capacities
                    $occupancies = ORM::factory('room_capacity')->get_room_capacities($hotel->room_id);

                    //select max room capasities
                    $tmp = 0;
                    foreach ($occupancies as $key => $value) {
                        if ($key == 0) {
                            $max_adults = $value['number_of_children'] + $value['number_of_adults'];
                        } else {
                            if (($value['number_of_adults'] + $value['number_of_children']) > ($tmp['number_of_children'] + $tmp['number_of_adults'])) {
                                $max_adults = $value['number_of_children'] + $value['number_of_adults'];
                            }
                        }
                    }

                    if (strtolower($info['SendData']) == 'true') {
                        // Hotel Rate
                        $hotel_data[$hotel->hotel_id]['hotel_name'] = $hotel->hotel_name;
                    }

                    if (strtolower($info['SendDistribSystems']) == 'true') {
                        // Hotel Rate
                        $hotel_data[$hotel->hotel_id]['rate'][$hotel->campaign_id] = $hotel->campaign_id;
                        // Hotel Room
                        $hotel_data[$hotel->hotel_id]['room'][$hotel->room_id] = $hotel->room_id;
                        // Hotel Meal
                        //kemungkinan di sini
                        $meal = $hotel->is_breakfast_included ? 'BRE' : 'NOM';
                        $hotel_data[$hotel->hotel_id]['mealplan'][$meal] = $meal;

                        // Hotel age
                        if ($hotel->number_of_adults > 0) {
                            $hotel_data[$hotel->hotel_id]['age']['ADULT'] = 10;
                        }
                        if ($hotel->number_of_children > 0) {
                            $hotel_data[$hotel->hotel_id]['age']['CHILD'] = 8;
                        }

                        // Rate plans
                        $hotel_data[$hotel->hotel_id]['rate_plans'][$hotel->campaign_id]['key'] = $hotel->campaign_id;
                        $hotel_data[$hotel->hotel_id]['rate_plans'][$hotel->campaign_id]['meal'] = $hotel->is_breakfast_included ? 'BRE' : 'NOM';
                    }

                    if (strtolower($info['SendRefPoints']) == 'true') {
                        $rooms = array(
                            'campaign_default' => $hotel->campaign_default,
                            'last_days' => $hotel->last_days,
                            'campaign_default' => $hotel->campaign_default,
                            'hotel_timezone' => $hotel->hotel_timezone,
                            'early_days' => $hotel->early_days,
                            'rate' => $hotel->rate,
                            'amount' => $hotel->amount,
                            'min_nights' => $hotel->min_nights,
                            'min_rooms' => $hotel->min_rooms,
                            'id' => $hotel->campaign_id,
                            'free_night' => $hotel->free_night,
                        );

                        $hotel_query = array(
                            'check_in' => date('Y-m-d'),
                            'check_out' => date('Y-m-d', strtotime(' +1 day')),
                            'currency_code' => '1',
                            'hotel_currency' => $hotel->currency_id,
                        );

                        if ($hotel->type == 2) {
                            $temp_early_name = Controller_Vacation_Static::promotion_descriptions($hotel_query, $rooms, $promotion_exis = TRUE);
                            // $temp_early_name = explode(',',$temp_early_name);
                            // $temp_early_name[0] = 'Book '.$hotel->early_days.' days in advance';
                            // if($hotel->rate != 0)
                            // {
                            //   $temp_early_promotion = $hotel->rate.'% Discount';
                            // }
                            // elseif($hotel->amount != 0)
                            // {
                            //   $temp_early_promotion = Controller_Vacation_Static::selected_currency($hotel->currency_id)->symbol
                            //     .$hotel->amount.' Discount';
                            // }
                            // elseif($hotel->free_night != 0)
                            // {
                            //   $string  = $hotel->free_night>1? 's' : '' ;
                            //   $temp_early_promotion = $hotel->free_night.' day'.$string;
                            // }
                            // $temp_early_name = $temp_early_name[0].', Get '.$temp_early_promotion.','.$temp_early_name[2];

                            $hotel_data[$hotel->hotel_id]['rate_plans'][$hotel->campaign_id]['description'] = $temp_early_name;
                        } else {
                            $hotel_data[$hotel->hotel_id]['rate_plans'][$hotel->campaign_id]['description'] = Controller_Vacation_Static::promotion_descriptions($hotel_query, $rooms, $promotion_exis = TRUE);
                        }

                        if ($hotel->rate != 0) {
                            $promotion_name = $hotel->rate . '% Discount';
                        } elseif ($hotel->amount != 0) {
                            $promotion_name = Controller_Vacation_Static::selected_currency($hotel->currency_id)->symbol
                                    . $hotel->amount . ' Discount';
                        } elseif ($hotel->last_days != 0) {
                            $promotion_name = 'LAST MINUTE';
                        } elseif ($hotel->early_days != 0) {
                            $promotion_name = 'EARLY BIRDS';
                        } else {
                            $promotion_name = 'Best Available Rate';
                        }

                        $hotel_data[$hotel->hotel_id]['rate_plans'][$hotel->campaign_id]['name'] = $promotion_name;

                        // $max_adults = $hotel->number_of_adults + $hotel->number_of_children;
                        $min_adults = $hotel->number_of_adults > $min_adults ? $min_adults : $hotel->number_of_adults;

                        // Room Types
                        $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['key'] = $hotel->room_id;
                        $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['max_capacities'] = $max_adults;
                        $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['min_capacities'] = $min_adults ? $min_adults : '1';
                        $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['name'] = $hotel->name;

                        // Room Size
                        $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['description_size'] = '';
                        if ($hotel->room_size > 0) {
                            // Entity code of meter square
                            // $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['description_size'] = $hotel->room_size.' &#13217;, ';
                            // Special character of meter square
                            // $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['description_size'] = $hotel->room_size.' ㎡, ';
                            // Square Meters as text..
                            $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['description_size'] = $hotel->room_size . ' square meters, ';
                        }

                        // define roomfacilities
                        $hotel_data_room_description = array();

                        // Room services for Descriptions
                        if ($hotel->is_air_conditioner_available) {
                            array_push($hotel_data_room_description, 'Air Conditioner');
                        }
                        if ($hotel->is_alarm_clock_available) {
                            array_push($hotel_data_room_description, 'Alarm Clock');
                        }
                        if ($hotel->is_balcony_available) {
                            array_push($hotel_data_room_description, 'Balcony');
                        }
                        if ($hotel->is_bathrobe_available) {
                            array_push($hotel_data_room_description, 'Bath Robe');
                        }
                        if ($hotel->is_bathtub_available) {
                            array_push($hotel_data_room_description, 'Bathtub');
                        }
                        if ($hotel->is_body_lotion_available) {
                            array_push($hotel_data_room_description, 'Body Lotion');
                        }
                        if ($hotel->is_cable_tv_available) {
                            array_push($hotel_data_room_description, 'Cable TV');
                        }
                        if ($hotel->is_coffee_maker_available) {
                            array_push($hotel_data_room_description, 'Coffe Maker');
                        }
                        if ($hotel->is_cotton_bud_available) {
                            array_push($hotel_data_room_description, 'Cotton Bud');
                        }
                        if ($hotel->is_dvd_player_available) {
                            array_push($hotel_data_room_description, 'DVD player');
                        }
                        if ($hotel->is_hair_dryer_available) {
                            array_push($hotel_data_room_description, 'Hair Dryer');
                        }
                        if ($hotel->is_idd_telephone_available) {
                            array_push($hotel_data_room_description, 'IDD Telephone');
                        }
                        if ($hotel->is_independent_shower_room_available) {
                            array_push($hotel_data_room_description, 'Independent Shower Room');
                        }
                        if ($hotel->is_safety_box_available) {
                            array_push($hotel_data_room_description, 'Safety Box');
                        }
                        if ($hotel->is_internet_jack_available) {
                            array_push($hotel_data_room_description, 'Internet Jack');
                        }
                        if ($hotel->is_iron_available) {
                            array_push($hotel_data_room_description, 'Iron');
                        }
                        if ($hotel->is_mini_bar_available) {
                            array_push($hotel_data_room_description, 'Mini Bar');
                        }
                        if ($hotel->is_mosquito_equipment_available) {
                            array_push($hotel_data_room_description, 'Mosquito Equipment');
                        }
                        if ($hotel->is_moveable_shower_head_available) {
                            array_push($hotel_data_room_description, 'Movable Shower Head');
                        }
                        if ($hotel->is_radio_available) {
                            array_push($hotel_data_room_description, 'Radio');
                        }
                        if ($hotel->is_room_wear_available) {
                            array_push($hotel_data_room_description, 'Room Wear');
                        }
                        if ($hotel->is_shampoo_available) {
                            array_push($hotel_data_room_description, 'Shampoo');
                        }
                        if ($hotel->is_shaver_available) {
                            array_push($hotel_data_room_description, 'Shaver');
                        }
                        if ($hotel->is_soap_available) {
                            array_push($hotel_data_room_description, 'Soap');
                        }
                        if ($hotel->is_toothbrush_available) {
                            array_push($hotel_data_room_description, 'Toothbrush');
                        }
                        if ($hotel->is_towel_available) {
                            array_push($hotel_data_room_description, 'Towel');
                        }
                        if ($hotel->is_tv_available) {
                            array_push($hotel_data_room_description, 'TV');
                        }

                        // implode facilities to text
                        $hotel_data_room_facilities = implode(', ', $hotel_data_room_description);
                        $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['description'] = $hotel_data_room_facilities;

                        // meal_plan
                        $hotel_data[$hotel->hotel_id]['rate_plans'][$hotel->campaign_id]['meal'] = $hotel->is_breakfast_included ? 'BRE' : 'NOM';

                        // Age Categories
                        $hotel_data[$hotel->hotel_id]['age_catagories']['ADULT']['from'] = $hotel->child_age_until + 1;
                        $hotel_data[$hotel->hotel_id]['age_catagories']['ADULT']['until'] = NULL;
                        $hotel_data[$hotel->hotel_id]['age_catagories']['CHILD']['from'] = $hotel->child_age_from;
                        $hotel_data[$hotel->hotel_id]['age_catagories']['CHILD']['until'] = $hotel->child_age_until;
                    }

                    if (strtolower($info['InfoSendData']) == 'true') {
                        // Hotel Info
                        $hotel_data[$hotel->hotel_id]['info'] = array(
                            'address' => $hotel->address,
                            'city' => $hotel->city,
                            'state' => $hotel->districts,
                            'country' => $hotel->country,
                            'phone' => $hotel->telephone,
                        );
                    }

                    // Request Data
                    $hotel_data[$hotel->hotel_id]['rq']['SendData'] = $info;
                }
            } else {
                $error['Hotel Not Found']['type'] = '2';
                $error['Hotel Not Found']['code'] = '161';
            }
        }

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $data);
            $this->output($errors);
        } else {
            return $this->xml_build($data, $hotel_data);
        }
    }

    public function xml_build($data, $hotel_data) {
        // Transaction
        $transaction = str_replace('OTA_', '', $data['head']);
        $transaction = substr($transaction, 0, -2) . 'RS';

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
            $envelope->setAttribute('xmlns:soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
            $head = $envelope->appendChild($doc->createElement('soap-env:Header'));
            $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
            $body->setAttribute('RequestId', $data['request_id']);
            $body->setAttribute('Transaction', $transaction);

            $response_node = $body->appendChild($doc->createElement(substr($data['head'], 0, -2) . 'RS'));
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
            $response_node->setAttribute('EchoToken', $data['EchoToken']);
            $response_node->setAttribute('Target', $data['Target']);
            $response_node->setAttribute('Version', $data['Version']);

            $success = $response_node->appendChild($doc->createElement('Success'));

            $HotelDescriptiveContents = $response_node->appendChild($doc->createElement('HotelDescriptiveContents'));

            foreach ($hotel_data as $key => $hotel) {
                $hotel_id = $key;

                $HotelDescriptiveContent = $HotelDescriptiveContents->appendChild($doc->createElement('HotelDescriptiveContent'));
                $HotelDescriptiveContent->setAttribute('HotelCode', $key);

                if (($hotel['rq']['SendData']['SendData'] == 'true') && (!empty($hotel['hotel_name']))) {
                    $HotelInfo = $HotelDescriptiveContent->appendChild($doc->createElement('HotelInfo'));
                    $HotelName = $HotelInfo->appendChild($doc->createElement('HotelName', htmlspecialchars(utf8_encode($hotel['hotel_name']))));
                }

                if (($hotel['rq']['SendData']['InfoSendData'] == 'true') && (count($hotel['info']) > 0)) {
                    $ContactInfos = $HotelDescriptiveContent->appendChild($doc->createElement('ContactInfos'));
                    $ContactInfo = $ContactInfos->appendChild($doc->createElement('ContactInfo'));
                    $Addresses = $ContactInfo->appendChild($doc->createElement('Addresses'));
                    $Address = $Addresses->appendChild($doc->createElement('Address'));
                    $Address->appendChild($doc->createElement('AddressLine', htmlspecialchars(utf8_encode($hotel['info']['address']))));
                    $Address->appendChild($doc->createElement('CityName', htmlspecialchars(utf8_encode($hotel['info']['city']))));
                    $Address->appendChild($doc->createElement('CountryName', htmlspecialchars(utf8_encode($hotel['info']['country']))));
                    $Phones = $ContactInfo->appendChild($doc->createElement('Phones'));
                    $Phone = $Phones->appendChild($doc->createElement('Phone'));
                    $Phone->setAttribute('PhoneTechType', 1);
                    $Phone->setAttribute('PhoneNumber', $hotel['info']['phone']);
                }

                if (($hotel['rq']['SendData']['SendDistribSystems'] == 'true') && (count($hotel['rate']) > 0)) {
                    $TPA_Extensions = $HotelDescriptiveContent->appendChild($doc->createElement('TPA_Extensions'));
                    $TPA_Extension = $TPA_Extensions->appendChild($doc->createElement('TPA_Extension'));
                    $TPA_Extension->setAttribute('ID', 'VQC');

                    $Extension = $TPA_Extension->appendChild($doc->createElement('Extension'));
                    $Extension->setAttribute('Name', 'InterfaceSetup');

                    // If rate  exist
                    if (count($hotel['rate']) > 0) {
                        foreach ($hotel['rate'] as $rate) {
                            $Item = $Extension->appendChild($doc->createElement('Item', $rate));
                            $Item->setAttribute('Key', 'Mapping_Rateplan');
                            $Item->setAttribute('Value', $rate);
                        }
                    }

                    // If room  exist
                    if (count($hotel['room']) > 0) {
                        foreach ($hotel['room'] as $room) {
                            $Item = $Extension->appendChild($doc->createElement('Item', $room));
                            $Item->setAttribute('Key', 'Mapping_Roomtype');
                            $Item->setAttribute('Value', $room);
                        }
                    }

                    // If MealPlan exist
                    if (count($hotel['mealplan']) > 0) {
                        foreach ($hotel['mealplan'] as $mealplan) {
                            $Item = $Extension->appendChild($doc->createElement('Item', $mealplan));
                            $Item->setAttribute('Key', 'Mapping_Mealplan');
                            $Item->setAttribute('Value', $mealplan);
                        }
                    }

                    // If Age is exist
                    if (count($hotel['age']) > 0) {
                        foreach ($hotel['age'] as $key => $age) {
                            $Item = $Extension->appendChild($doc->createElement('Item', $age));
                            $Item->setAttribute('Key', 'Mapping_AgeCategory');
                            $Item->setAttribute('Value', $key);
                        }
                    }

                    if (!count($hotel['age']['CHILD'])) {
                        $Item = $Extension->appendChild($doc->createElement('Item', 8));
                        $Item->setAttribute('Key', 'Mapping_AgeCategory');
                        $Item->setAttribute('Value', 'CHILD');
                    }
                }

                // If rate_plans is exist
                if ($hotel['rq']['SendData']['SendRefPoints'] == 'true') {

                    if (($hotel['rq']['SendData']['SendDistribSystems'] == 'false')) {
                        $TPA_Extensions = $HotelDescriptiveContent->appendChild($doc->createElement('TPA_Extensions'));
                        $TPA_Extension = $TPA_Extensions->appendChild($doc->createElement('TPA_Extension'));
                        $TPA_Extension->setAttribute('ID', 'VQC');
                    }


                    $Extension = $TPA_Extension->appendChild($doc->createElement('Extension'));
                    $Extension->setAttribute('Name', 'Rateplans');

                    if (count($hotel['rate_plans']) > 0) {
                        foreach ($hotel['rate_plans'] as $key => $rate_plans) {
                            // $rate_plans['description'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', $rate_plans['description']);
                            $rate_plans['description'] = substr($rate_plans['description'], 0, 100);
                            $Item = $Extension->appendChild($doc->createElement('Item'));
                            $Item->setAttribute('Key', $key);

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'IncludedMealplan');
                            $Detail->setAttribute('Value', htmlspecialchars(utf8_encode($rate_plans['meal'])));

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'Caption_ENG');
                            $Detail->setAttribute('Value', htmlspecialchars(utf8_encode($rate_plans['description'])));

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'Description_ENG');
                            $Detail->setAttribute('Value', htmlspecialchars(utf8_encode($rate_plans['description'])));
                        }
                    }
                }

                // If room_type is exist
                if (!empty($hotel['room_type'])) {
                    $Extension = $TPA_Extension->appendChild($doc->createElement('Extension'));
                    $Extension->setAttribute('Name', 'Roomtypes');

                    if (count($hotel['room_type']) > 0) {
                        foreach ($hotel['room_type'] as $key => $room_type) {
                            $Item = $Extension->appendChild($doc->createElement('Item'));
                            $Item->setAttribute('Key', $key);

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'MinimumOccupancy');
                            $Detail->setAttribute('Value', $room_type['min_capacities']);

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'MaximumOccupancy');
                            $Detail->setAttribute('Value', $room_type['max_capacities']);

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'Caption_ENG');
                            $Detail->setAttribute('Value', htmlspecialchars(utf8_encode($room_type['name'])));

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'Description_ENG');
                            $Detail->setAttribute('Value', $room_type['description_size'] . htmlspecialchars(utf8_encode($room_type['description'])));
                        }
                    }

                    $Extension = $TPA_Extension->appendChild($doc->createElement('Extension'));
                    $Extension->setAttribute('Name', 'AgeCategories');

                    if (count($hotel['age_catagories']) > 0) {
                        foreach ($hotel['age_catagories'] as $key => $age_catagories) {
                            $Item = $Extension->appendChild($doc->createElement('Item'));
                            $Item->setAttribute('Key', $key);

                            $Detail = $Item->appendChild($doc->createElement('Detail'));
                            $Detail->setAttribute('Key', 'AgeFrom');
                            $Detail->setAttribute('Value', 0);

                            if ($age_catagories['until']) {
                                $Detail = $Item->appendChild($doc->createElement('Detail'));
                                $Detail->setAttribute('Key', 'AgeUntil');
                                $Detail->setAttribute('Value', $age_catagories['until']);
                            }
                        }
                    }
                }
            }

            $response = $doc->saveXML();

            $this->output($response);
        } catch (Kohana_Exception $e) {
            $error['XML_build']['type'] = '2';
            $error['XML_build']['code'] = '450';

            $errors = Controller_Vacation_Static::errors($error, $data);
            $this->output($errors);
        }
    }

    public function output($response) {

        //old log
        // Log::instance()->add(Log::INFO, "Vacation API Response: HotelDescriptiveInfo");
        // Log::instance()->add(Log::INFO, $response);
        // new log
        CustomLog::factory()->add(2, 'DEBUG', 'Vacation API Response: HotelDescriptiveInfo');
        CustomLog::factory()->add(2, 'DEBUG', $response);

        // Auth Logout
        A1::instance()->logout();

        // Build ouput
        $this->response
                ->headers('Content-Type', 'text/xml')
                ->body($response)
                ->status(200)
        ;
    }

    public function action_request() {
        // Set time limit
        set_time_limit(60);

        $xml_location = file_get_contents('php://input');
        // $xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/info.xml';
        // $data = file_get_contents($xml_location);
        // $xml_location = trim( stripslashes( $data ));
        //old log
        // Log::instance()->add(Log::INFO, "Vacation API Request: HotelDescriptiveInfo");
        // Log::instance()->add(Log::INFO, $xml_location);
        // new log
        CustomLog::factory()->add(2, 'DEBUG', 'Vacation API Request: HotelDescriptiveInfo');
        CustomLog::factory()->add(2, 'DEBUG', $xml_location);

        if (!$xml_location) {
            $node_head = array(
                'head' => 'ResponseRQ',
                'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                'TimeStamp' => date('c'),
                'EchoToken' => '000',
                'Target' => 'Production',
                'Version' => '1.00',
                'PrimaryLangID' => 'En',
            );

            $error['Welcome']['type'] = '2';
            $error['Welcome']['code'] = '450';

            $errors = Controller_Vacation_Static::errors($error, $node_head);

            $this->output($errors);
        } else {
            $xml = new SimpleXMLElement($xml_location);

            // $namespace = $xml->getDocNamespaces(TRUE);
            // foreach($namespace as $html_element => $value)
            // {
            //   $xmlns = $value;
            // }

            $xmlns = 'http://www.opentravel.org/OTA/2003/05';

            $head = $xml->children('soap-env', true)->Header->children()->Interface;
            $body = $xml->children('soap-env', true)->Body->children();
            $transaction = 'OTA_HotelDescriptiveInfoRQ';

            if ($transaction == 'OTA_HotelDescriptiveInfoRQ') {
                $request_id = 'OTA_' . (string) $xml->children('soap-env', true)->Body->attributes()->RequestId;

                $data = $this->checker($transaction, $head, $body, $xmlns, $request_id);
            } else {
                $node_head = array(
                    'head' => 'ResponseRQ',
                    'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                    'TimeStamp' => date('c'),
                    'EchoToken' => '000',
                    'Target' => 'Production',
                    'Version' => '1.00',
                    'PrimaryLangID' => 'En',
                );

                $error['False_request']['type'] = '2';
                $error['False_request']['code'] = '450';
                $errors = Controller_Vacation_Static::errors($error, $node_head);

                $this->output($errors);
            }
        }
    }

}
