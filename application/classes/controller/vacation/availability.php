<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Availability extends Controller {
    /*
     * List USED Model
     * Hotel :get_campaign
     * Campaign :get_items
     * Room capacity :getroom, checkroom, extrabed
     * Country/City  : getbysegment
     * Timezone  : timezone identifier
     * Setting  : get
     * Exchane  : To
     */

    public function checker($transaction, $head, $body, $xmlns, $request_id) {
        $data = array();
        $error = array();

        $OTA_HotelAvailRQ = $body->$transaction->attributes();
        $node_head = array(
            'head' => $transaction,
            'request_id' => $request_id,
            'xmlns' => $xmlns,
            'TimeStamp' => (string) $OTA_HotelAvailRQ->TimeStamp,
            'PrimaryLangID' => (string) $OTA_HotelAvailRQ->PrimaryLangID,
            'Target' => (string) $OTA_HotelAvailRQ->Target,
            'Version' => (string) $OTA_HotelAvailRQ->Version,
            'EchoToken' => (string) $body->$transaction->attributes()->EchoToken,
        );

        $auth = array(
            'User' => (string) $head->ComponentInfo->attributes()->User,
            'Pwd' => (string) $head->ComponentInfo->attributes()->Pwd,
            'key' => '',
            'source' => '',
        );

        // Check Target Environment
        // $vacation_environment = strtolower((string)$OTA_HotelAvailRQ->Target) == 'production' ? 'production' : 'development';
        $vacation_environment = strtolower((string) $OTA_HotelAvailRQ->Target) == 'production' ? Kohana::PRODUCTION : Kohana::DEVELOPMENT;

        // Check node head null
        if (empty($node_head['xmlns']) || empty($node_head['TimeStamp']) || empty($node_head['PrimaryLangID']) || empty($node_head['Target']) || empty($node_head['Version']) || empty($node_head['request_id']) || empty($node_head['EchoToken'])) {
            $error['Empty_Property']['type'] = '2';
            $error['Empty_Property']['code'] = '321';
        }
        // Check Target Environment
        elseif (Kohana::$environment != $vacation_environment) {
            $error['Authorization']['type'] = '6';
            $error['Authorization']['code'] = '146';
        }
        // Check Auth null request
        elseif (empty($auth['User']) || empty($auth['Pwd'])) {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '321';
        } else {
            // Check Auth
            if (Controller_Vacation_Static::auth($auth) == FALSE) {
                $error['Authentification']['type'] = '4';
                $error['Authentification']['code'] = '9900';
            } else {
                $AvailRequestSegment = $body->$transaction->AvailRequestSegments->AvailRequestSegment;
                $RatePlanCandidates = $AvailRequestSegment->RatePlanCandidates->RatePlanCandidate;
                $HotelRefs = $RatePlanCandidates->HotelRefs->HotelRef;

                $data = array(
                    'Start' => (string) $AvailRequestSegment->StayDateRange->attributes()->Start,
                    'End' => (string) $AvailRequestSegment->StayDateRange->attributes()->End,
                    'Duration' => (string) $AvailRequestSegment->StayDateRange->attributes()->Duration,
                    'RatePlanType' => (string) $RatePlanCandidates->attributes()->RatePlanType,
                    'RatePlanCode' => (string) $RatePlanCandidates->attributes()->RatePlanCode,
                    'RPH' => (string) $RatePlanCandidates->attributes()->RPH,
                    'HotelCode' => (string) $HotelRefs->attributes()->HotelCode,
                    'MealsIncluded' => (string) $RatePlanCandidates->attributes()->MealPlanCodes,
                        //update husen
//                    'MealsIncluded' => (string) '*',
                );

                $room_count = 0;

                $RoomStayCandidates = $AvailRequestSegment->RoomStayCandidates->RoomStayCandidate;

                foreach ($RoomStayCandidates as $RoomStayCandidate) {

                    $room_id = (string) $RoomStayCandidate->attributes()->RoomTypeCode;

                    $data['rooms'][$room_count] = array(
                        'language_id' => 1,
                        'InvTypeCode' => $room_id,
                        'RatePlanCode' => (string) $RatePlanCandidates->attributes()->RatePlanCode,
                        'RatePlanCandidateRPH' => (string) $RoomStayCandidate->attributes()->RatePlanCandidateRPH,
                        'Quantity' => (string) $RoomStayCandidate->attributes()->Quantity,
                    );

                    $j = 0;
                    foreach ($RoomStayCandidate->GuestCounts->GuestCount as $GuestCount) {
                        $data['rooms'][$room_count]['GuestCounts'][$j] = array(
                            'AgeQualifyingCode' => (string) $GuestCount->attributes()->AgeQualifyingCode,
                            'Count' => (string) $GuestCount->attributes()->Count,
                        );

                        // Check Null request
                        if (empty($data['rooms'][$room_count]['GuestCounts'][$j]['AgeQualifyingCode']) || empty($data['rooms'][$room_count]['GuestCounts'][$j]['Count'])) {
                            $error['GuestCounts_empty']['type'] = '10';
                            $error['GuestCounts_empty']['code'] = '321';
                            break;
                        }

                        // Check Format request
                        if (!is_numeric($data['rooms'][$room_count]['GuestCounts'][$j]['AgeQualifyingCode']) || !is_numeric($data['rooms'][$room_count]['GuestCounts'][$j]['Count'])) {
                            $error['GuestCounts_format']['type'] = '3';
                            $error['GuestCounts_format']['code'] = '459';
                            break;
                        }
                        $j++;
                    }

                    // Check Null request
                    if (empty($data['rooms'][$room_count]['InvTypeCode']) || empty($data['rooms'][$room_count]['RatePlanCandidateRPH']) || empty($data['rooms'][$room_count]['Quantity'])) {
                        $error['Rooms']['type'] = '10';
                        $error['Rooms']['code'] = '321';
                        break;
                    }

                    // Check Format request
                    if (!ctype_alnum($data['rooms'][$room_count]['InvTypeCode'])) {
                        if ($data['rooms'][$room_count]['InvTypeCode'] != '*') {
                            $error['InvTypeCode']['type'] = '3';
                            $error['InvTypeCode']['code'] = '402';
                            break;
                        }
                    }

                    if (!is_numeric($data['rooms'][$room_count]['RatePlanCandidateRPH']) || !is_numeric($data['rooms'][$room_count]['Quantity'])) {
                        $error['RatePlanCandidateRPH_or_Quantity']['type'] = '3';
                        $error['RatePlanCandidateRPH_or_Quantity']['code'] = '459';
                        break;
                    }
                    $room_count++;
                }

                // Check Null request
                if (empty($data['Start']) || empty($data['End']) || empty($data['Duration']) || empty($data['RatePlanType']) || empty($data['RatePlanCode']) || empty($data['HotelCode'])
                ) {
                    $error['Empty_Property']['type'] = '10';
                    $error['Empty_Property']['code'] = '321';
                }

                // Check start and end date format
                if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['Start']) || !preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['End'])
                ) {
                    $error['Start_or_End']['type'] = '3';
                    $error['Start_or_End']['code'] = '15';
                }

                // Check Number Of Nights
                $data['stay_night'] = Date::span(strtotime($data['End']), strtotime($data['Start']), 'days');
                if ($data['stay_night'] < 1) {
                    $error['Stay_minimum_night']['type'] = '2';
                    $error['Stay_minimum_night']['code'] = '69';
                }

                if ($data['stay_night'] > 364) {
                    $error['Stay_maximum_night']['type'] = '2';
                    $error['Stay_maximum_night']['code'] = '70';
                }

                // Check Format request
                if (!is_numeric($data['RatePlanType'])) {
                    if ($data['rooms'][$room_count]['RatePlanType'] != '*') {
                        $error['RatePlanType']['type'] = '3';
                        $error['RatePlanType']['code'] = '249';
                        break;
                    }
                }

                // Check Format request
                if (!ctype_alnum($data['RatePlanCode'])) {
                    if ($data['RatePlanCode'] != '*') {
                        $error['RatePlanCode']['type'] = '3';
                        $error['RatePlanCode']['code'] = '402';
                    }
                }

                if (!ctype_alnum($data['HotelCode'])) {
                    $error['HotelCode']['type'] = '3';
                    $error['HotelCode']['code'] = '459';
                }
            }
        }

        $data = array_merge($data, $node_head);

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $node_head);
            $this->output($errors);
        } else {
            $this->process($data);
        }
    }

    public function process($data) {
        $error = array();
        $warning_datas = array();

        // Get hotel
        $hotel = ORM::factory('hotel')
                ->where('id', '=', $data['HotelCode'])
                ->find();

        if (count($hotel)) {
            $rooms = array();
            $hotel_ids = array(
                0 => $data['HotelCode']
            );

            $count_room = 0;
            $counter = 0;

            // Search Room Data
            foreach ($data['rooms'] as $room_id => $room) {

                // Room data and Capacities
                $room['hotel_id'] = $data['HotelCode'];
                $room['check_in'] = $data['Start'];
                //$room['check_out'] =  date('Y-m-d', strtotime($data['End']."-1 day"));
                $room['check_out'] = $data['End'];
                $room['number_of_rooms'] = $room['Quantity'];

                for ($i = 0; $i < $room['Quantity']; $i++) {
                    $child = 0;
                    $adult = 0;

                    foreach ($room['GuestCounts'] as $GuestCount) {
                        if ($GuestCount['AgeQualifyingCode'] == 8 || $GuestCount['AgeQualifyingCode'] == 4) {
                            $child += $GuestCount['Count'];
                        } else {
                            $adult += $GuestCount['Count'];
                        }
                    }
                    $room['capacities'][$i]['child'] = $child;
                    $room['capacities'][$i]['adult'] = $adult;
                }

                $room['now'] = Date::formatted_time('now', 'Y-m-d', $hotel->timezone);

                $campaigns = ORM::factory('hotel')->get_campaigns($hotel_ids, $room, TRUE);

                // Count item flags
                if (count($campaigns)) {
                    foreach ($campaigns as &$campaign) {
                        // Assign varibale room
                        $campaign['quantity'] = $room['Quantity'] ? $room['Quantity'] : 1;
                        $campaign['capacities'] = $room['GuestCounts'];

                        $campaign['room_capacity'] = Model::factory('room_capacity')->check_room_capacity($campaign['room_id'], $room['capacities']);

                        //$campaign['room_capacities'] = Model::factory('room_capacity')->get_room_capacities($campaign['room_id']);

                        $campaign['nights'] = Date::span(Helper_Date::create_date($room['check_in'])->getTimestamp(), Helper_Date::create_date($room['check_out'])->getTimestamp(), 'days');
                        $campaign['night_count'] = 0;
                        $campaign['total_price'] = 0;
                        $campaign['average_price'] = NULL;

                        // Redeclare room
                        $room['InvTypeCode'] = $campaign['room_id'];
                        $room['campaign_default'] = $campaign['campaign_default'];
                        $campaign['items'] = ORM::factory('campaign')->get_items($campaign['campaign_id'], $room);

                        if (empty($campaign['items'])) {
                            $campaign['items'] = ORM::factory('campaign')->get_items($campaign['campaign_id'], $room, TRUE);
                        }

                        // Get campaign min stock
                        $min_stock = isset($campaign['items'][$room['check_in']]) ? min(Arr::pluck($campaign['items'], 'stock')) : 99999;

                        $remaining_stock = Kohana::$config->load('hoterip')->get('remaining_stock');

                        // If min stock less then remaining stock
                        if ($min_stock <= $remaining_stock) {
                            $campaign['remaining_flag'] = $min_stock;
                        }

                        // Determine if guest stay night more than minimum night
                        $campaign['minimum_night_available'] = $campaign['max_minimum_night'];
                    }

                    foreach ($campaigns as $key => &$campaign) {
                        $campaign['default'] = 0;
                        if ($campaign['type'] == 3) {
                            $campaign['last_days_date'] = Date::formatted_time("+{$campaign['last_days']} days", __('M j, Y'), $campaign['hotel_timezone']);

                            $last_days = $campaign['last_days_date'];
                            $last_days = Date::formatted_time("$last_days", 'Y-m-d', $campaign['hotel_timezone']);

                            if ($room['check_in'] <= $last_days AND $room['check_out'] <= $last_days) {
                                $campaign['default'] = 0;
                            } elseif ($room['check_in'] < $last_days AND $room['check_out'] >= $last_days) {
                                $campaign['default'] = 0;
                            } else {
                                $campaign['default'] = 1;
                                unset($campaigns[$key]);
                            }
                        }

                        if ($campaign['type'] == 2) {

                            $early_days = $campaign['early_days'] - 1;
                            $campaign['early_days_date'] = Date::formatted_time("now + $early_days days", 'Y-m-d');

                            $early_days = $campaign['early_days_date'];

                            if ($room['check_in'] <= $early_days) {
                                unset($campaigns[$key]);
                            } else {
                                if ($room['check_in'] > $early_days AND $room['check_out'] <= $early_days) {
                                    $campaign['default'] = 1;
                                } elseif ($room['check_in'] < $early_days AND $room['check_out'] >= $early_days) {
                                    $campaign['default'] = 0;
                                } elseif ($room['check_in'] > $early_days AND $room['check_out'] >= $early_days) {
                                    $campaign['default'] = 0;
                                }
                            }
                        }

                        foreach ($campaign['items'] as $key => &$item) {
                            if (isset($item['date'])) {
                                // Show flag true
                                $item['remaining_flag'] = FALSE;

                                // If room capacity is true
                                if ($campaign['room_capacity']) {
                                    // If not blackout or not weekout
                                    if (!($item['blackout'] OR $item['weekout'])) {
                                        //If there is stock
                                        if ($item['stock'] >= count($data['rooms'])) {
                                            // Out flag ture
                                            $item['price_flag'] = TRUE;
                                            $campaign['night_count'] ++;

                                            // If campaingn blackout
                                            if ($item['campaign_blackout'] && !$campaign['campaign_default']) {
                                                $item['price'] = $item['price'];
                                            }

                                            /// Campaign Prices. Is campaign prices
                                            // elseif ($item['is_campaign_prices'])
                                            // {
                                            //   //application control
                                            //   if($week_item['application_control'] != NULL)
                                            //   {
                                            //     $application_controls =  unserialize($week_item['application_control']);
                                            //     if (count($application_controls) > 0) 
                                            //     {
                                            //       foreach ($application_controls as $key_application_control => $application_control) {
                                            //         if (date('w', strtotime($week_item['date'])) == $key_application_control AND $application_control) 
                                            //         {
                                            //           $day['price'] = $week_item['price'];
                                            //         }
                                            //       }
                                            //     }
                                            //   }
                                            // }
                                            else {
                                                // If promotion
                                                if ($item['promotion']) {
                                                    // If free night
                                                    if ($item['free_night']) {
                                                        $item['price'] = 0;
                                                        $item['free_flag'] = TRUE;
                                                    } else {
                                                        $item['price'] = ($item['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                                                    }
                                                } else {
                                                    $item['price'] = $item['price'];
                                                }
                                                $item['remaining_flag'] = TRUE;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // unset $campaing
                    unset($campaign);
                    $room_is_valid = true;
                    foreach ($campaigns as $key => $campaign) {
                        $campaign_is_valid = true;
                        foreach ($campaign['items'] as $kcampaign => $icampaign) {
                            if (!($icampaign['remaining_flag'] AND ( $icampaign['price'] || $campaign['free_night']))) {
                                $campaign_is_valid = false;
                                break;
                            }
                        }

                        if (!$campaign_is_valid) {
                            continue;
                        }
                        $hotel_data = array();

                        //get surcharge
                        $surcharge_all = Model::factory('surcharge')->get_surcharges($data['HotelCode'], $data['Start'], $data['End']);
                        $surcharge_date = array();

                        //change key array surcharge
                        if (count($surcharge_all)) {
                            foreach ($surcharge_all as $key => $value) {
                                $surcharge_date[$value['date']] = $value;
                            }

                            //combine array surcharge to campaign items
                            foreach ($surcharge_date as $key => $value) {
                                $campaign['items'][$key]['surcharge'] = $value;
                            }
                        }

                        $occupancies[0] = array();
                        //get room capacities from khow extrabed
                        $occupancies[0] = DB::select(
                                        'room_capacities.*'
                                )
                                ->from('room_capacities')
                                ->where('room_id', '=', $campaign['room_id'])
                                ->where('number_of_adults', '=', $adult)
                                ->execute()
                                ->current();

                        foreach ($occupancies as $key => $occupancie) {
                            $extrabed = $occupancie['number_of_extrabeds'];
                            $campaign['extrabed'] = $extrabed;
                        }

                        if (!empty($campaign['campaign_id'])) {
                            $hotel_data['promotion_exist'] = 1;
                            $hotel_data['on_requested'] = 1;
                            $hotel_data['total_price'] = 0;
                            $hotel_data['average_price'] = 0;

                            foreach ($campaign['items'] as &$item) {
                                $hotel_data['total_price'] += $item['price'];
                            }

                            if ($campaign['night_count'] == $campaign['nights']) {
                                $hotel_data['average_price'] = $hotel_data['total_price'] / $campaign['nights'];
                            }

                            // TODO campaignに入れなくてもOKかな。
                            // Get no arrival
                            $hotel_no_arrival = DB::select()
                                    ->from('hotel_no_arrivals')
                                    ->where('hotel_id', '=', $campaign['hotel_id'])
                                    ->where('date', '=', $room['check_in'])
                                    ->execute()
                                    ->get('date');

                            // TODO campaignに入れなくてもOKかな。
                            // Get no departure
                            $hotel_no_departure = DB::select()
                                    ->from('hotel_no_departures')
                                    ->where('hotel_id', '=', $campaign['hotel_id'])
                                    ->where('date', '=', $room['check_out'])
                                    ->execute()
                                    ->get('date');

                            // Determine if guest stay night more than minimum night
                            $minimum_night_available = $campaign['max_minimum_night'];
                            if (!$hotel_no_arrival AND ! $hotel_no_departure AND $minimum_night_available) {
                                if (($data['stay_night'] >= $minimum_night_available) && ($data['stay_night'] >= $campaign['min_nights']) && ($room['number_of_rooms'] >= $campaign['min_rooms'])) {
                                    $hotel_data['on_requested'] = 0;
                                } else {
                                    $warning_datas[$campaign['room_id']]['short_text'] = 'Room Availability';
                                    $warning_datas[$campaign['room_id']]['text'] = 'Room: ' . $campaign['room_name'] . ', ';
                                    $warning_datas[$campaign['room_id']]['text'] = 'Campaign: ' . $campaign['campaign_id'] . ', ';
                                    $warning_datas[$campaign['room_id']]['text'] = 'CheckIn: ' . $room['check_in'] . ', ';
                                    $warning_datas[$campaign['room_id']]['text'] = 'CheckOut: ' . $room['check_out'] . ', ';
                                    $warning_datas[$campaign['room_id']]['text'] = 'Campaign and Room is not available' . '.';
                                    continue;
                                }

                                if (($data['stay_night'] < $minimum_night_available) && ($data['stay_night'] >= $campaign['min_nights']) && ($room['number_of_rooms'] >= $campaign['min_rooms'])) {
                                    $hotel_data['promotion_exist'] = 0;
                                }
                            }

                            // Promotions Descriptions
                            if ($hotel_data['promotion_exist']) {
                                $room['hotel_currency'] = $campaign['currency_id'];
                                $room['currency_code'] = 1;
                                $hotel_data['descriptions'] = Controller_Vacation_Static::promotion_descriptions($room, $campaign, $hotel_data['promotion_exist']);
                            } else {
                                $hotel_data['descriptions'] = 'Default Promotions';
                            }

                            if ($hotel_data['on_requested'] == 0) {
                                $hotel_data = array_merge($campaign, $hotel_data);
                                //break;
                            } else {
                                $warning_datas[$campaign['room_id']]['short_text'] = 'Room Availability';
                                $warning_datas[$campaign['room_id']]['text'] .= 'Room: ' . $campaign['room_name'] . ', ';
                                $warning_datas[$campaign['room_id']]['text'] .= 'Campaign: ' . $campaign['campaign_id'] . ', ';
                                $warning_datas[$campaign['room_id']]['text'] .= 'CheckIn: ' . $room['check_in'] . ', ';
                                $warning_datas[$campaign['room_id']]['text'] .= 'CheckOut: ' . $room['check_out'] . ', ';
                                $warning_datas[$campaign['room_id']]['text'] .= 'Campaign and Room is not available' . '.';

                                // Redeclare hotel_data
                                $hotel_data = array();
                                continue;
                            }
                        } else {
                            $warning_datas[$room['InvTypeCode']]['short_text'] = 'Empty Campaign';
                            $warning_datas[$room['InvTypeCode']]['text'] .= 'Room: ' . $room['InvTypeCode'] . ', ';
                            $warning_datas[$room['InvTypeCode']]['text'] .= 'CheckIn: ' . $room['check_in'] . ', ';
                            $warning_datas[$room['InvTypeCode']]['text'] .= 'CheckOut: ' . $room['check_out'] . ', ';
                            $warning_datas[$room['InvTypeCode']]['text'] .= 'Campaign Not Found' . '.';

                            continue;
                        }
                        $rooms[$counter][$campaign['room_id']][$campaign['campaign_id']] = $hotel_data;
                    }


                    // if(count($hotel_data) > 0)
                    // {
                    //   $rooms[$campaign['room_id']][$campaign['campaign_id']] = $hotel_data;
                    // }
                } else {
                    $warning_datas[$room['InvTypeCode']]['short_text'] = 'Empty Campaign';
                    $warning_datas[$room['InvTypeCode']]['text'] .= 'Room: ' . $room['InvTypeCode'] . ', ';
                    $warning_datas[$room['InvTypeCode']]['text'] .= 'CheckIn: ' . $room['check_in'] . ', ';
                    $warning_datas[$room['InvTypeCode']]['text'] .= 'CheckOut: ' . $room['check_out'] . ', ';
                    $warning_datas[$room['InvTypeCode']]['text'] .= 'Campaign Not Found' . '.';

                    continue;
                }
                $counter ++;
            }
        } else {
            $error['Hotel_not_found']['type'] = '2';
            $error['Hotel_not_found']['code'] = '161';
        }

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Vacation_Static::errors($error, $data);
            $this->output($errors);
        } else {
            // Build response
            $this->xml_build($rooms, $data, $warning_datas);
        }
    }

    public function xml_build($rooms, $data, $warning_datas) {
        $cancellations = array();

        // Factory exchange
        $exchange = Model::factory('exchange');
        $currency_code = Model::factory('currency');

        // Get guest number of night
        $check_in = $data['Start'];
        $check_out = $data['End'];
        $stay_night = Date::span(strtotime($check_in), strtotime($check_out), 'days');

        // Transaction
        $transaction = str_replace('OTA_', '', $data['head']);
        $transaction = substr($transaction, 0, -2) . 'RS';

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
            $envelope->setAttribute('xmlns:soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
            $head = $envelope->appendChild($doc->createElement('soap-env:Header'));
            $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
            $body->setAttribute('RequestId', $data['request_id']);
            $body->setAttribute('Transaction', $transaction);

            $response_node = $body->appendChild($doc->createElement(substr($data['head'], 0, -2) . 'RS'));
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
            $response_node->setAttribute('EchoToken', $data['EchoToken']);
            $response_node->setAttribute('Target', $data['Target']);
            $response_node->setAttribute('Version', $data['Version']);

            $success = $response_node->appendChild($doc->createElement('Success'));

            if (count($warning_datas)) {
                $warnings = $response_node->appendChild($doc->createElement('Warnings'));
                foreach ($warning_datas as $warning_text => $warning_data) {

                    $warning = $warnings->appendChild($doc->createElement('Warning', $warning_data['text']));
                    $warning->setAttribute('ShortText', $warning_data['short_text']);
                }
            }

            $RoomStays = $response_node->appendChild($doc->createElement('RoomStays'));

            if (is_array($rooms)) {
                foreach ($rooms as $key_rooms => $rooms_hirarchy) {
                    foreach ($rooms_hirarchy as $count_room => $campaign_id) {
                        foreach ($campaign_id as $room) {

                            // Total Value
                            $total_price = 0;
                            $total_price_tax = 0;

                            $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));

                            $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                            $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                            $RoomType->setAttribute('RoomTypeCode', $room['room_id']);
                            $RoomType->setAttribute('NumberOfUnits', $room['quantity']);

                            $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                            $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                            $RatePlan->setAttribute('RatePlanCode', $room['campaign_id']);

                            $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription'));
                            $RatePlanDescription->setAttribute('Name', $room['descriptions']);

                            $MealsIncluded = $RatePlan->appendChild($doc->createElement('MealsIncluded'));
//update husen
//                            $breakfast = $room['is_breakfast_included'] ? 'BRE' : 'NOM';
                            $breakfast = '*';
                            $MealsIncluded->setAttribute('MealPlanCodes', $breakfast);

                            $service_tax_rate = (($room['hotel_service_charge_rate'] + $room['hotel_tax_rate']) + 100) / 100;
                            $room_name = $room['room_name'];

                            // Number Of Units
                            $NumberOfUnits = $room['quantity'];

                            // Cancelation Value
                            $cancellations = Controller_Vacation_Static::cancellations($room['campaign_id'], $room['hotel_id'], FALSE, $data['Start']);

                            if (empty($cancellations)) {
                                $cancellations = array(
                                    'first' => array(
                                        'rule' => 8,
                                        'within_days' => 10000,
                                        'night_charged' => 0,
                                        'percent_charged' => 100
                                    )
                                );
                            }

                            $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));

                            $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                            $RoomRate->setAttribute('NumberOfUnits', $NumberOfUnits);
                            $RoomRate->setAttribute('EffectiveDate', $data['Start']);
                            $RoomRate->setAttribute('ExpireDate', $data['End']);
                            $RoomRate->setAttribute('RoomTypeCode', $room['room_id']);
                            $RoomRate->setAttribute('RatePlanCode', $room['campaign_id']);

                            if (is_array($room['items'])) {
                                $total_AmountBeforeTax = 0;
                                $total_AmountAfterTax = 0;
                                $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                                foreach ($room['items'] as $item) {
                                    $date = $item['date'];

                                    if ($item['remaining_flag'] AND ( $item['price'] || $room['free_night'])) {
                                        $Rate = $Rates->appendChild($doc->createElement('Rate'));
                                        $Rate->setAttribute('EffectiveDate', $item['date']);
                                        $Rate->setAttribute('ExpireDate', Date::formatted_time("$date + 1 days", 'Y-m-d'));
                                        $Rate->setAttribute('RateTimeUnit', 'Day');

                                        //move value AQC to single variable
                                        $child = 0;
                                        $adult = 0;
                                        foreach ($room['capacities'] as $key => $value) {
                                            if ($value['AgeQualifyingCode'] == 10) {
                                                $adult = $value['Count'];
                                            } elseif ($value['AgeQualifyingCode'] == 8) {
                                                $child = $value['Count'];
                                            }
                                        }


                                        // $base_single_price = ($item['price'] / $service_tax_rate);
                                        // // $base_single_price = round($base_single_price);
                                        // $base_single_price_tax = $item['price'];
                                        // // $base_single_price_tax = round($base_single_price_tax);
                                        //if extrabed true
                                        $extrabed_price = 0;
                                        if (!empty($room['extrabed'])) {
                                            $extrabed_price = !empty($room['extrabed_price']) ? ($room['extrabed_price'] * $room['extrabed']) : 0;
                                        }
                                        //adult caculation
                                        $surcharge_adult = !empty($item['surcharge']) ? $item['surcharge']['adult_price'] : 0;

                                        //child calculaation
                                        $child_single_price_tax = 0;
                                        $surcharge_child = 0;
                                        foreach ($room['capacities'] as $key => $value) {
                                            if ($value['AgeQualifyingCode'] == 8) {
                                                $surcharge_child = !empty($item['surcharge']) ? $item['surcharge']['child_price'] : 0;
                                                $child_single_price_tax = !empty($item['extrabed_item_price']) ? $item['extrabed_item_price'] : $room['extrabed_price'];
                                                $child_single_price_tax = $child_single_price_tax * $NumberOfUnits;
                                            }
                                        }

                                        $base_single_price = $item['price'] / $service_tax_rate;
                                        $base_single_price = $base_single_price * $NumberOfUnits;
                                        $base_single_price = $base_single_price + $extrabed_price;
                                        $base_single_price = $base_single_price + (($surcharge_adult * $adult) + ($child_single_price_tax * $child) + ($surcharge_child * $child));
                                        // $base_single_price = round($base_single_price);

                                        $base_single_price_tax = $item['price'];
                                        $base_single_price_tax = $base_single_price_tax * $NumberOfUnits;
                                        $base_single_price_tax = $base_single_price_tax + $extrabed_price;
                                        $base_single_price_tax = $base_single_price_tax + (($surcharge_adult * $adult) + ($child_single_price_tax * $child) + ($surcharge_child * $child));
                                        // $base_single_price_tax = round($base_single_price_tax);

                                        $total_price = $base_single_price;
                                        $total_price_tax = $base_single_price_tax;

                                        $total_AmountBeforeTax += $total_price;
                                        $total_AmountAfterTax += $total_price_tax;

                                        $Base = $Rate->appendChild($doc->createElement('Base'));
                                        $Base->setAttribute('AmountBeforeTax', number_format(round($exchange->to($room['currency_id'], 1) * $base_single_price, 2), '2', '', ''));
                                        $Base->setAttribute('AmountAfterTax', number_format(round($exchange->to($room['currency_id'], 1) * $base_single_price_tax, 2), '2', '', ''));
                                        $Base->setAttribute('DecimalPlaces', 2);
                                        $Base->setAttribute('CurrencyCode', 'USD');

                                        $Total = $Rate->appendChild($doc->createElement('Total'));
                                        $Total->setAttribute('AmountBeforeTax', number_format(round($exchange->to($room['currency_id'], 1) * $total_price, 2), '2', '', ''));
                                        $Total->setAttribute('AmountAfterTax', number_format(round($exchange->to($room['currency_id'], 1) * $total_price_tax, 2), '2', '', ''));
                                        $Total->setAttribute('DecimalPlaces', 2);
                                        $Total->setAttribute('CurrencyCode', 'USD');
                                    }
                                }
                            }

                            $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
                            $RoomDescription->appendChild($doc->createElement('Text', htmlspecialchars(utf8_encode($room_name))));

                            $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));

                            if (is_array($room['capacities'])) {
                                foreach ($room['capacities'] as $GuestCount_single) {
                                    $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                                    $GuestCount->setAttribute('AgeQualifyingCode', $GuestCount_single['AgeQualifyingCode']);
                                    $GuestCount->setAttribute('Count', $GuestCount_single['Count']);
                                }
                            }

                            $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                            $TimeSpan->setAttribute('Start', $check_in);
                            $TimeSpan->setAttribute('End', $check_out);
                            $TimeSpan->setAttribute('Duration', 'P' . $stay_night . 'N');

                            $CancelPenalties = $RoomStay->appendChild($doc->createElement('CancelPenalties'));

                            foreach ($cancellations as $key => $cancellation) {
                                $CancelPenalty = $CancelPenalties->appendChild($doc->createElement('CancelPenalty'));


                                if (!empty($cancellation['rule'])) {
                                    // Within Days

                                    $within_days = (int) $cancellation['within_days'];

                                    $within_days = Date::formatted_time("$check_in - $within_days days", 'Y-m-d');

                                    // Non refundable
                                    if ($cancellation['within_days'] >= 1000 AND $cancellation['percent_charged'] == 100) {
                                        $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                        $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                        //$DeadLine->setAttribute('AbsoluteDeadline', $cancellation['within_days']);
                                        $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                        $DeadLine->setAttribute('OffsetUnitMultiplier', 999);
                                        $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                        $AmountPercent->setAttribute('Percent', 100);
                                        break;
                                    }
                                    // no show 1st night charged
                                    elseif ($cancellation['within_days'] == 0 AND $cancellation['night_charged'] != 0 AND $cancellation['percent_charged'] == 0) {
                                        $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                        $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                        $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                        //$DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                        //$DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);

                                        $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                        $AmountPercent->setAttribute('Percent', 100);
                                        $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                    }
                                    // no show percent charged
                                    elseif ($cancellation['within_days'] == 0 AND $cancellation['night_charged'] == 0 AND $cancellation['percent_charged'] != 0) {

                                        $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                        $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                        $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                        //$DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                        //$DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);

                                        $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                        $AmountPercent->setAttribute('Percent', $cancellation['percent_charged']);
                                        // $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                    }
                                    // within days show  night charged
                                    elseif ($cancellation['within_days'] > 0 AND $cancellation['percent_charged'] == 0 AND $cancellation['night_charged'] != 0) {

                                        if ((int) $cancellation['within_days'] > 999) {
                                            $cancellation['within_days'] = '999';
                                        }

                                        $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                        $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));

                                        if (in_array($cancellation['rule'], array(5, 6, 7))) {
                                            $within_days = date('Y-m-d');
                                            $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                        } else {
                                            $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                            $DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);
                                        }


                                        $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                        // $AmountPercent->setAttribute('Percent', 100);
                                        if ($cancellation['night_charged']) {
                                            $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                        }
                                    }
                                    // within days show  charged
                                    elseif ($cancellation['within_days'] > 0 AND $cancellation['night_charged'] == 0 AND $cancellation['percent_charged'] != 0) {



                                        $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                        $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));

                                        if (in_array($cancellation['rule'], array(5, 6, 7))) {
                                            $within_days = date('Y-m-d');
                                            $DeadLine->setAttribute('AbsoluteDeadline', $within_days);
                                        } else {
                                            $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                            $DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);
                                        }
                                        // $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                        // $DeadLine->setAttribute('OffsetUnitMultiplier', $cancellation['within_days']);
                                        // $DeadLine->setAttribute('AbsoluteDeadline', $within_days);

                                        $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                        $AmountPercent->setAttribute('Percent', $cancellation['percent_charged']);
                                        if ($cancellation['night_charged']) {
                                            $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                        }
                                    }
                                    // free cancelaltion
                                    elseif ($cancellation['within_days'] == 10000 AND $cancellation['percent_charged'] == 0 AND $cancellation['night_charged'] == 0) {
                                        $CancelPenalty->setAttribute('PolicyCode', 'CFC');
                                        break;
                                    } else {
                                        $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                        $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                        //$DeadLine->setAttribute('AbsoluteDeadline', $check_in);
                                        $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                        $DeadLine->setAttribute('OffsetUnitMultiplier', 999);

                                        $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));

                                        if ($cancellation['percent_charged']) {
                                            $AmountPercent->setAttribute('Percent', $cancellation['percent_charged']);
                                        }
                                        if ($cancellation['night_charged']) {
                                            $AmountPercent->setAttribute('NmbrOfNights', $cancellation['night_charged']);
                                        }

                                        break;
                                    }
                                }
                                // non refundable for no cancellation
                                else {
                                    $CancelPenalty->setAttribute('PolicyCode', 'CXP');

                                    $DeadLine = $CancelPenalty->appendChild($doc->createElement('Deadline'));
                                    //$DeadLine->setAttribute('AbsoluteDeadline', $check_in);
                                    $DeadLine->setAttribute('OffsetTimeUnit', 'Day');
                                    $DeadLine->setAttribute('OffsetUnitMultiplier', 999);

                                    $AmountPercent = $CancelPenalty->appendChild($doc->createElement('AmountPercent'));
                                    $AmountPercent->setAttribute('Percent', '100');
                                    break;
                                }
                            }


                            $Total = $RoomStay->appendChild($doc->createElement('Total'));
                            $Total->setAttribute('AmountBeforeTax', number_format(round($exchange->to($room['currency_id'], 1) * $total_AmountBeforeTax, 2), '2', '', ''));
                            $Total->setAttribute('AmountAfterTax', number_format(round($exchange->to($room['currency_id'], 1) * $total_AmountAfterTax, 2), '2', '', ''));
                            $Total->setAttribute('DecimalPlaces', 2);
                            $Total->setAttribute('CurrencyCode', 'USD');

                            $BasicProperty = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                            $BasicProperty->setAttribute('HotelCode', $data['HotelCode']);
                        }
                    }
                }
            }

            $response = $doc->saveXML();

            $this->output($response);
        } catch (Kohana_Exception $e) {
            $error['XML_build']['type'] = '2';
            $error['XML_build']['code'] = '450';

            $errors = Controller_Vacation_Static::errors($error, $data);
            $this->output($errors);
        }
    }

    public function output($response) {
        //old log
        // Log::instance()->add(Log::INFO, "Vacation API respondse: Availability");
        //  Log::instance()->add(Log::INFO, $response);
        //new log
        CustomLog::factory()->add(2, 'DEBUG', 'Vacation API respondse: Availability');
        CustomLog::factory()->add(2, 'DEBUG', $response);

        // Logout account
        A1::instance()->logout();
        $this->response->headers('Content-Type', 'text/xml')// ->headers('Status:', '100')
                ->body($response)
                ->status(200)
        ;
    }

    public function action_request() {
        // Set time limit
        set_time_limit(60);

        $xml_location = file_get_contents('php://input');

        // old log
        // Log::instance()->add(Log::INFO, "Vacation API Request: Availability");
        // Log::instance()->add(Log::INFO, $xml_location);

        CustomLog::factory()->add(2, 'DEBUG', 'Vacation API Request: Availability');
        CustomLog::factory()->add(2, 'DEBUG', $xml_location);


        // $xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/vacation.xml';
        // $data = file_get_contents($xml_location);
        // $xml_location = trim( stripslashes( $data ));

        if (!$xml_location) {
            $node_head = array(
                'head' => 'ResponseRQ',
                'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                'TimeStamp' => date('c'),
                'EchoToken' => '000',
                'Target' => 'Production',
                'Version' => '1.00',
                'PrimaryLangID' => 'En',
            );

            $error['Welcome']['type'] = '2';
            $error['Welcome']['code'] = '450';

            $errors = Controller_Vacation_Static::errors($error, $node_head);

            $this->output($errors);
        } else {
            $xml = new SimpleXMLElement($xml_location);

            $namespace = $xml->getDocNamespaces(TRUE);
            foreach ($namespace as $html_element => $value) {
                $xmlns = $value;
            }

            $head = $xml->children('soap-env', true)->Header->children()->Interface;
            $body = $xml->children('soap-env', true)->Body->children();
            $transaction = 'OTA_' . (string) $xml->children('soap-env', true)->Body->attributes()->Transaction;

            if ($transaction === 'OTA_HotelAvailRQ') {
                $request_id = 'OTA_' . (string) $xml->children('soap-env', true)->Body->attributes()->RequestId;

                $data = $this->checker($transaction, $head, $body, $xmlns, $request_id);
            } else {
                $node_head = array(
                    'head' => 'ResponseRQ',
                    'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                    'TimeStamp' => date('c'),
                    'EchoToken' => '000',
                    'Target' => 'Production',
                    'Version' => '1.00',
                    'PrimaryLangID' => 'En',
                );

                $error['False_request']['type'] = '2';
                $error['False_request']['code'] = '450';
                $errors = Controller_Vacation_Static::errors($error, $node_head);

                $this->output($errors);
            }
        }
    }

}
