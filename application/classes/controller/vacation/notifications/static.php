<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Vacation_Notifications_Static extends Controller_Layout_Admin {


public static function checkXML($xml_to_check)
{
  //get node AvailStatusMessages
  $AvailStatusMessages = $xml_to_check->children('soap-env', true)->Body
  ->children()->OTA_HotelAvailNotifRQ
  ->children()->AvailStatusMessages
  ->children()->AvailStatusMessage;

  $correct_data = TRUE;
  //start check per AvailStatusMessage
  foreach ($AvailStatusMessages as $AvailStatusMessagek => $AvailStatusMessage)
  {
    $node['ristriction'] = $AvailStatusMessage->children()->RestrictionStatus->attributes();
    $node['StatusApplicationControl'] = $AvailStatusMessage->children()->StatusApplicationControl->attributes();

    // check per value
    if(
      ((string)$node['ristriction']->Restriction == 'C' || (string)$node['ristriction']->Restriction == '')  ||
      ((string)$node['ristriction']->Status == 'C' || (string)$node['ristriction']->Status == '')  ||
      ((string)$node['StatusApplicationControl']->RatePlanCode == 'C' || (string)$node['StatusApplicationControl']->RatePlanCode == '')  ||
      ((string)$node['StatusApplicationControl']->InvCode == 'C' || (string)$node['StatusApplicationControl']->InvCode == '')  ||
      ((string)$node['StatusApplicationControl']->End == ''  || (string)$node['StatusApplicationControl']->End == '') ||
      ((string)$AvailStatusMessage->attributes()->BookingLimit == '')
      )
    { 
      //if not valid set variable false
      $correct_data = FALSE;
      // exit loop
      break;
    }
  }
    
    // return result
    return $correct_data;
  }

  public static function post($url_response, $request_id)
  {
    $result = FALSE;
    $transaction = '';
    
    $xml = new SimpleXMLElement($url_response);
    //HotelAvailNotifRQ
    if((string)$xml->children('soap-env', true)->Body->attributes()->Transaction == 'HotelAvailNotifRQ')
    {
      //check
      if(!self::checkXML($xml))
      {
        //set to log
        CustomLog::factory()->add(2,'DEBUG', 'API RateAvailNotifRQ Invalid RQ: '.$url_response);
        //return false
        return FALSE;
      }
    }
    
    //header("Content-type: text/xml");
//    echo '<pre>';print_r($url_response);echo '</pre>';
//    exit();
    $ch = curl_init();

    // Load Config
    if (Kohana::$environment == Kohana::PRODUCTION)
    {
      $config = Kohana::$config->load('api')->vacation;
    }
    else
    {
      $config = Kohana::$config->load('api')->vacation_test;
    }

    curl_setopt($ch, CURLOPT_URL, $config['url_connect']);
    //bisa ganti http://www.google.com
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    //check environment for use curl ssl
    if (Kohana::$environment == Kohana::PRODUCTION) 
    {
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSLVERSION, 3);
    }
    
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $url_response);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
   
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);

    // Close the handle
    //echo '<pre>';print_r($info);echo '</pre>';
    curl_close($ch);

    // old location log log
    // Log::instance()->add(Log::INFO, 'API  Request : '.$url_response);
    // Log::instance()->add(Log::INFO, 'API  Response : '.$response);

    //new location log
    // CustomLog::factory()->add(2,'DEBUG', 'API  Request Info');
    // CustomLog::factory()->add(2,'DEBUG', 'API  Response Info');
    /*
     * XML process
     */
    
    //echo '<pre>';print_r($url_response);echo '</pre>';
    try
    {
      $xml = new SimpleXMLElement($response);
      $namespace = $xml->getDocNamespaces(TRUE);
      foreach($namespace as $html_element => $value)
      {
        $xmlns = $value;
      }

      $head = $xml->children('soap-env', true)->Header->children()->Interface;
      $body = $xml->children('soap-env', true)->Body->children();
      $transaction = 'OTA_'.(string)$xml->children('soap-env', true)->Body->attributes()->Transaction;
      $request_id_RS = (string)$xml->children('soap-env', true)->Body->attributes()->RequestId;

      // Check request ID exist
      if($request_id == $request_id_RS)
      {
        $data = array();

        $OTA = $body->$transaction;

        // Check response Success/ Errors
        if (!is_null($OTA->success)) {
          $result = TRUE;
          CustomLog::factory()->add(2,'DEBUG', 'API  Response Success');
        }
      }
    }
    catch (Exception $e)
    {
      $result = FALSE;
      CustomLog::factory()->add(2,'DEBUG', 'API  Response Failed');
    }
    // Add Logs
    $result_text = $result ? 'Success' : 'Failed';

    // Log::instance()->add(Log::INFO, 'API Vacation Results : '.$result);
    // Log::instance()->add(Log::INFO, 'API Vacation Response : '.$transaction);
    return $result;
  }

  public static function date_ranges($stay_start_date, $stay_end_date)
  {
    /*
     * Build time span
     */
    $timespans = array();
    $i=0;
    // Build data range
    $begin = new DateTime($stay_start_date);
    $end = new DateTime($stay_end_date);
    $end = $end->modify( '+1 day' ); 
    $interval = new DateInterval('P1D');
    $dateranges = new DatePeriod($begin, $interval ,$end);

    return $dateranges;
  }

  public static function update_api_access()
  {
    // Get number of Access API
     $admin = DB::select(
      'api_access.access',
      array('api_access.admin_id', 'id')
      )
      ->from('admins')
      ->join('api_access')->on('api_access.admin_id', '=', 'admins.id')
      ->where('api_access.name_segment', '=', 'vacation')
      ->execute()
      ->current();

    // Request ID
    $request_time = $admin['access'] + 1;

    // Update number of access
    DB::update('api_access')
      ->set(array('access' => $request_time))
      ->where('admin_id', '=', $admin['id'])
      ->execute();

    return $request_time;
  }
}