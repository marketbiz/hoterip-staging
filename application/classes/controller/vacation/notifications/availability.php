<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Notifications_Availability extends Controller_Layout_Admin {

    public static function item($post, $cron = FALSE) {
        //request date 2014-06-16 -> campaign early bird send again to vacation
        $cron = True;
        // Get Available Campaign
        $campaigns = DB::select(
                        array('campaigns.id', 'campaign_id'), array('rooms.id', 'room_id'), array('campaigns.minimum_number_of_nights', 'campaigns_minimum_night'), array('items.minimum_night', 'minimum_night'), array('items.stock', 'stock'), array('campaigns.days_in_advance', 'days_in_advance'), array('campaigns.within_days_of_arrival', 'within_days'), array('campaigns.is_default', 'is_default'), array('campaigns.type', 'type'), array('campaigns.stay_start_date', 'stay_start_date'), array('campaigns.stay_end_date', 'stay_end_date'), array('campaigns.booking_end_date', 'booking_end_date'), array('campaigns.booking_start_date', 'booking_start_date')
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->join('items')->on('items.room_id', '=', 'rooms.id')
                ->where('rooms.id', '=', $post['room_id'])
                ->where('campaigns.is_show', '=', 1)
                ->where('campaigns.status', '=', 'c')
                ->and_where_open()
                ->where('rooms.hoterip_campaign', '=', 0)
                ->where('rooms.is_breakfast_included', '=', 1);

        //if not from cron
        if (!$cron) {
            $campaigns = $campaigns
                    ->where('campaigns.days_in_advance', '=', 0);
        }

        $campaigns = $campaigns
                ->and_where_close();
        // Is campaign default
        if ($post['campaign_id']) {
            $campaigns = $campaigns->where('campaigns.id', '=', $post['campaign_id']);
        } else {
            $campaigns = $campaigns->where('campaigns.is_default', '=', 1);
        }

        // Filter stay date
        $campaigns = $campaigns
                /* Kawamura's Confirmation
                 * "but the example is what I am saying."
                 * from:   kawamura@his-bali.com
                 * date:   Mon, Mar 17, 2014 at 12:46 PM
                 * subject:   Re: Vacation test case 05032014
                 */
                // ->where_open()
                // ->and_where('campaigns.stay_start_date', '<=', $post['start_date'])
                // ->and_where('campaigns.stay_end_date', '>=', $post['end_date'])
                // ->where_close()
                // Filter check in out
                // ->where('items.date', '>=', $post['start_date'])
                // ->where('items.date', '<=', $post['end_date'])
                // Group
                ->group_by('campaign_id')
                ->group_by('room_id')
                ->execute()
                ->as_array('campaign_id');

        /*
         * Combine item data
         *
         * array[0] = date 
         * array[1] = stock
         * array[2] = price
         * array[3] = minimun nights
         */
        if (count($campaigns)) {
            // from cron set time now
            if ($cron) {
                $now = date('Y-m-d');
            }
            $item_posts = array_merge_recursive($post['dates'], $post['stocks']);

            //if not from cron set price
            if (!$cron) {
                $item_posts = array_merge_recursive($item_posts, $post['prices']);
            }

            $item_posts = array_merge_recursive($item_posts, $post['minimum_nights']);

            if (!empty($post['is_blackouts'])) {
                $item_posts = array_merge_recursive($item_posts, $post['is_blackouts']);

                foreach ($post['is_blackouts'] as $key => $item_post) {
                    $item_posts[$key]['is_blackouts'] = 1;
                }
            }

            if (!empty($post['is_campaign_blackouts'])) {
                $item_posts = array_merge_recursive($item_posts, $post['is_campaign_blackouts']);

                foreach ($post['is_campaign_blackouts'] as $key => $item_post) {
                    $item_posts[$key]['is_campaign_blackouts'] = 1;
                }
            }
            
            foreach ($campaigns as $campaign_id => &$campaign) {
                $items_datas = array();
                foreach ($item_posts as $date => $item_post) {
                    // Redeclare variable, including minimum night(load from campaign instead from items)
                    $items_datas[$campaign_id][$date] = $item_post;
                    $items_datas[$campaign_id][$date]['date'] = $item_post[0];
                    $items_datas[$campaign_id][$date]['stock'] = $item_post[1];

                    //if not from cron set index array price and minimum_night 
                    if (!$cron) {
                        $items_datas[$campaign_id][$date]['price'] = $item_post[2];
                        $items_datas[$campaign_id][$date]['minimum_night'] = $item_post[3];
                    } else {
                        $items_datas[$campaign_id][$date]['minimum_night'] = $item_post[2];
                    }

                    $items_datas[$campaign_id][$date]['campaign_id'] = $campaign_id;
                    $items_datas[$campaign_id][$date]['room_id'] = $post['room_id'];

                    // Get no arrival
                    $no_arrival = DB::select()
                            ->from('hotel_no_arrivals')
                            ->where('hotel_id', '=', $post['hotel_id'])
                            ->where('date', '=', $date)
                            ->execute()
                            ->get('date');

                    // Get no departure
                    $no_departure = DB::select()
                            ->from('hotel_no_departures')
                            ->where('hotel_id', '=', $post['hotel_id'])
                            ->where('date', '=', $date)
                            ->execute()
                            ->get('date');

                    /*
                     * Set Restrictions
                     */
                    $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                    $items_datas[$campaign_id][$date]['status'] = 'Open';

                    //if post from home/dashboard
                    if ($post['home']) {
                        //check hotel active or not
                        if ($post['hotel_active_vacation'] == FALSE) {
                            $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                            $items_datas[$campaign_id][$date]['status'] = 'Close';
                        }
                    }

                    //get status hotel
                    $status_hotel = DB::select('active')
                            ->from('hotels')
                            ->where('id', '=', $post['hotel_id'])
                            ->execute()
                            ->current();

                    $post['status_hotel'] = $status_hotel;
                    //master close if status hotel close
                    if (!$post['status_hotel']) {
                        $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                        $items_datas[$campaign_id][$date]['status'] = 'Close';
                    }

                    if (($now < $campaign['booking_start_date']) || ($now > $campaign['booking_end_date'])) {
                        $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                        $items_datas[$campaign_id][$date]['status'] = 'Close';
                    }

                    //if campaigns is standard promotion
                    if ($campaign['type'] == 1) {
                        //filter by staydate and endate
                        if ($campaign['stay_start_date'] > $items_datas[$campaign_id][$date]['date'] || $campaign['stay_end_date'] < $items_datas[$campaign_id][$date]['date']
                        ) {
                            $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                            $items_datas[$campaign_id][$date]['status'] = 'Close';
                        }
                    }
                    //if campaigns is early bird
                    elseif ($campaign['type'] == 2) {
                        $now = date('Y-m-d');
                        $plus_day = $campaign['days_in_advance'];
                        $days_in_advance = Date::formatted_time("$now + $plus_day days", 'Y-m-d');

                        if ($items_datas[$campaign_id][$date]['date'] < $days_in_advance) {
                            $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                            $items_datas[$campaign_id][$date]['status'] = 'Close';
                        }
                        //filter by staydate and endate
                        if ($campaign['stay_start_date'] > $items_datas[$campaign_id][$date]['date'] || $campaign['stay_end_date'] < $items_datas[$campaign_id][$date]['date']
                        ) {
                            $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                            $items_datas[$campaign_id][$date]['status'] = 'Close';
                        }
                    }
                    //if campaigns is last minute
                    elseif ($campaign['type'] == 3) {
                        $now = date('Y-m-d');
                        $plus_day = $campaign['within_days'];
                        $within_days = Date::formatted_time("$now + $plus_day days", 'Y-m-d');

                        if ($items_datas[$campaign_id][$date]['date'] > $within_days) {
                            $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                            $items_datas[$campaign_id][$date]['status'] = 'Close';
                        }
                        //filter by staydate and endate
                        if ($campaign['stay_start_date'] > $items_datas[$campaign_id][$date]['date'] || $campaign['stay_end_date'] < $items_datas[$campaign_id][$date]['date']
                        ) {
                            $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                            $items_datas[$campaign_id][$date]['status'] = 'Close';
                        }
                    }

                    if (date('Y-m-d') > $items_datas[$campaign_id][$date]['date']) {
                        $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                        $items_datas[$campaign_id][$date]['status'] = 'Close';
                    }
                    if ($items_datas[$campaign_id][$date]['stock'] == 0) {
                        $items_datas[$campaign_id][$date]['restriction'] = 'Master';
                        $items_datas[$campaign_id][$date]['status'] = 'Close';
                    }
                    if (!empty($item_post['is_blackouts'])) {
                        $items_datas[$campaign_id][$date]['status'] = 'Close';
                    }
                    if (!empty($item_post['is_campaign_blackouts'])) {
                        if (!$campaign['is_default']) {
                            $items_datas[$campaign_id][$date]['status'] = 'Close';
                        }
                    }
                    //if($post['price'][$date]==0)
                    //{
                    //$items_datas[$campaign_id][$date]['status'] = 'Close';
                    //}
                    if (!empty($no_arrival)) {
                        $items_datas[$campaign_id][$date]['restriction'] = 'Arrival';
                        $items_datas[$campaign_id][$date]['status'] = 'Close';
                        $items_datas[$campaign_id][$date]['restriction_arrival'] = 'Arrival';
                    }
                    if (!empty($no_departure)) {
                        $items_datas[$campaign_id][$date]['restriction'] = 'Departure';
                        $items_datas[$campaign_id][$date]['status'] = 'Close';
                        $items_datas[$campaign_id][$date]['restriction_departure'] = 'Departure';
                    }
                }

                // Date ranges
                $i = 0;
                $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['start_date'], $post['end_date']);

                foreach ($dateranges as $daterange) {
                    $date = $daterange->format("Y-m-d");

                    reset($items_datas);
                    $first_array = key($items_datas);

                    if (!empty($items_datas[$campaign_id][$date])) {
                        $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
                        $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

                        if (count($items_datas[$campaign_id]) == 1) {
                            $campaign['items'][$i]['SetMinLOS'] = TRUE;
                        }
                        if ($first_array == $date || $items_datas[$campaign_id][$min_one_day]['status'] != $items_datas[$campaign_id][$date]['status']) {
                            $campaign['items'][$i]['start'] = $date;
                        } elseif ($items_datas[$campaign_id][$min_one_day]['restriction'] != $items_datas[$campaign_id][$date]['restriction']) {
                            $campaign['items'][$i]['start'] = $date;
                        } elseif ($items_datas[$campaign_id][$min_one_day]['minimum_night'] != $items_datas[$campaign_id][$date]['minimum_night']) {
                            $campaign['items'][$i]['start'] = $date;
                        } elseif ($items_datas[$campaign_id][$min_one_day]['stock'] != $items_datas[$campaign_id][$date]['stock']) {
                            $campaign['items'][$i]['start'] = $date;
                        } elseif (!array_key_exists($min_one_day, $items_datas[$campaign_id])) {
                            $campaign['items'][$i]['start'] = $date;
                        }

                        if ($items_datas[$campaign_id][$add_one_day]['status'] != $items_datas[$campaign_id][$date]['status']
                        ) {
                            $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                            $campaign['items'][$i]['end'] = $end_date;

                            $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                            $campaign['items'][$i]['status'] = $items_datas[$campaign_id][$date]['status'];
                            $campaign['items'][$i]['room_id'] = $items_datas[$campaign_id][$date]['room_id'];
                            $campaign['items'][$i]['campaign_id'] = $items_datas[$campaign_id][$date]['campaign_id'];
                            $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                            $campaign['items'][$i]['date'] = $items_datas[$campaign_id][$date]['date'];
                            $campaign['items'][$i]['stock'] = $items_datas[$campaign_id][$date]['stock'];
                            $campaign['items'][$i]['restriction_departure'] = $items_datas[$campaign_id][$date]['restriction_departure'];
                            $campaign['items'][$i]['restriction_arrival'] = $items_datas[$campaign_id][$date]['restriction_arrival'];

                            //if not from cron set price
                            if (!$cron) {
                                $campaign['items'][$i]['price'] = $items_datas[$campaign_id][$date]['price'];
                            }

                            $campaign['items'][$i]['minimum_night'] = $items_datas[$campaign_id][$date]['minimum_night'];

                            $i++;
                        } elseif (($items_datas[$campaign_id][$add_one_day]['stock'] != $items_datas[$campaign_id][$date]['stock']) || ($items_datas[$campaign_id][$add_one_day]['minimum_night'] != $items_datas[$campaign_id][$date]['minimum_night']) || ($items_datas[$campaign_id][$add_one_day]['restriction'] != $items_datas[$campaign_id][$date]['restriction']) || ($campaign['items'][$add_one_day]['restriction_arrival'] != $items_datas[$campaign_id][$date]['restriction_arrival']) || ($campaign['items'][$add_one_day]['restriction_departure'] != $items_datas[$campaign_id][$date]['restriction_departure'])
                        ) {
                            $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                            $campaign['items'][$i]['end'] = $end_date;

                            $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                            $campaign['items'][$i]['status'] = $items_datas[$campaign_id][$date]['status'];
                            $campaign['items'][$i]['room_id'] = $items_datas[$campaign_id][$date]['room_id'];
                            $campaign['items'][$i]['campaign_id'] = $items_datas[$campaign_id][$date]['campaign_id'];
                            $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                            $campaign['items'][$i]['date'] = $items_datas[$campaign_id][$date]['date'];
                            $campaign['items'][$i]['stock'] = $items_datas[$campaign_id][$date]['stock'];
                            $campaign['items'][$i]['restriction_departure'] = $items_datas[$campaign_id][$date]['restriction_departure'];
                            $campaign['items'][$i]['restriction_arrival'] = $items_datas[$campaign_id][$date]['restriction_arrival'];

                            //if not from cron set price
                            if (!$cron) {
                                $campaign['items'][$i]['price'] = $items_datas[$campaign_id][$date]['price'];
                            }

                            $campaign['items'][$i]['minimum_night'] = $items_datas[$campaign_id][$date]['minimum_night'];

                            $i++;
                        } elseif (!array_key_exists($min_one_day, $items_datas[$campaign_id])) {
                            $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                            $campaign['items'][$i]['end'] = $end_date;

                            $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                            $campaign['items'][$i]['status'] = $items_datas[$campaign_id][$date]['status'];
                            $campaign['items'][$i]['room_id'] = $items_datas[$campaign_id][$date]['room_id'];
                            $campaign['items'][$i]['campaign_id'] = $items_datas[$campaign_id][$date]['campaign_id'];
                            $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                            $campaign['items'][$i]['date'] = $items_datas[$campaign_id][$date]['date'];
                            $campaign['items'][$i]['stock'] = $items_datas[$campaign_id][$date]['stock'];
                            $campaign['items'][$i]['restriction_departure'] = $items_datas[$campaign_id][$date]['restriction_departure'];
                            $campaign['items'][$i]['restriction_arrival'] = $items_datas[$campaign_id][$date]['restriction_arrival'];
                            //if not from cron set price
                            if (!$cron) {
                                $campaign['items'][$i]['price'] = $items_datas[$campaign_id][$date]['price'];
                            }

                            $campaign['items'][$i]['minimum_night'] = $items_datas[$campaign_id][$date]['minimum_night'];

                            //$i++;
                        } else {
                            // Nothing to do here, move along!
                        }
                    }
                }
            }
            // CustomLog::factory()->add(2,'DEBUG', 'API  Var Post '.print_r(json_encode($post), 1));
            // CustomLog::factory()->add(2,'DEBUG', 'API  Var Campaigns '.print_r(json_encode($campaigns), 1));
            $process = self::process($post, $campaigns);
        }
        return $process;
    }

    public static function promotion($post) {
        $campaigns = array();
        $success = TRUE;

        if (count($post['room_ids']) > 0) {
            foreach ($post['room_ids'] as $room_id) {
                $campaign_id = $post['campaign_id'];
                $post['room_id'] = $room_id;

                $hotel_query = array(
                    'language_id' => Controller_Api_Request::default_language(),
                    'HotelCode' => $post['hotel_id'],
                    'check_in' => date("Y-m-d"),
                    'check_out' => $post['stay_end_date'],
                    'InvTypeCode' => $room_id,
                    'RatePlanCode' => $campaign_id,
                    'now' => date("Y-m-d")
                );

                // Attempt to get items of matching campaign
                $items = ORM::factory('campaign')->get_items($campaign_id, $hotel_query);

                // Retry to get items of matching campaign by ignoring stay date
                if (!count($items)) {
                    $items = ORM::factory('campaign')->get_items($campaign_id, $hotel_query, TRUE);
                }

                // Get no arrival
                $no_arrivals = DB::select(
                                'hotel_no_arrivals.date'
                        )
                        ->from('hotel_no_arrivals')
                        ->where('hotel_id', '=', $post['hotel_id'])
                        ->where('date', '>=', date("Y-m-d"))
                        ->execute()
                        ->as_array('date');

                // Get no departure
                $no_departures = DB::select(
                                'hotel_no_departures.date'
                        )
                        ->from('hotel_no_departures')
                        ->where('hotel_id', '=', $post['hotel_id'])
                        ->where('date', '>=', date("Y-m-d"))
                        ->execute()
                        ->as_array('date');

                /*
                 * Set Restrictions
                 */
                // Split all blackout or weekout
                $items_datas = array();
                foreach ($items as $date => $item) {
                    if ($item['blackout'] || $item['weekout']
                    ) {
                        if (!empty($item['is_blackouts'])) {
                            $items_datas[$room_id][$date]['status'] = 'Close';
                        }
                        if (!empty($item_post['is_campaign_blackouts'])) {
                            $items_datas[$room_id][$date]['status'] = 'Close';
                        }
                    } elseif (!empty($no_arrivals[$date])) {
                        $items_datas[$room_id][$date]['restriction'] = 'Arrival';
                        $items_datas[$room_id][$date]['status'] = 'Close';
                    } elseif (!empty($no_departures[$date])) {
                        $items_datas[$room_id][$date]['restriction'] = 'Departure';
                        $items_datas[$room_id][$date]['status'] = 'Close';
                    } else {
                        $items_datas[$room_id][$date]['restriction'] = 'Master';
                        $items_datas[$room_id][$date]['status'] = 'Open';
                    }

                    $items_datas[$room_id][$date]['room_id'] = $item['room_id'];
                    $items_datas[$room_id][$date]['campaign_id'] = $item['campaign_id'];
                    $items_datas[$room_id][$date]['date'] = $item['date'];
                    $items_datas[$room_id][$date]['price'] = $item['price'];
                    $items_datas[$room_id][$date]['stock'] = $item['stock'];
                    $items_datas[$room_id][$date]['minimum_night'] = $item['minimum_night'];
                }

                /*
                 * Build time span
                 */

                $i = 0;
                $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['check_in'], $post['stay_end_date']);

                $campaign['campaigns_minimum_night'] = $items[$date]['campaigns_minimum_night'];
                foreach ($dateranges as $daterange) {
                    $date = $daterange->format("Y-m-d");

                    reset($items_datas);
                    $first_array = key($items_datas);

                    if (!empty($items_datas[$room_id][$date])) {
                        $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
                        $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

                        if ($first_array == $date || $items_datas[$room_id][$min_one_day]['status'] != $items_datas[$room_id][$date]['status']) {
                            $campaign['items'][$i]['start'] = $date;
                        } elseif ($items_datas[$room_id][$min_one_day]['restriction'] != $items_datas[$room_id][$date]['restriction']) {
                            $campaign['items'][$i]['start'] = $date;
                        } elseif ($items_datas[$room_id][$min_one_day]['stock'] != $items_datas[$room_id][$date]['stock']) {
                            $campaign['items'][$i]['start'] = $date;
                        } elseif (!array_key_exists($min_one_day, $items_datas[$room_id])) {
                            $campaign['items'][$i]['start'] = $date;
                        }

                        if ($items_datas[$room_id][$add_one_day]['status'] != $items_datas[$room_id][$date]['status']
                        ) {
                            $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                            $campaign['items'][$i]['end'] = $end_date;

                            $campaign['items'][$i]['restriction'] = $items_datas[$room_id][$date]['restriction'];
                            $campaign['items'][$i]['status'] = $items_datas[$room_id][$date]['status'];
                            $campaign['items'][$i]['room_id'] = $items_datas[$room_id][$date]['room_id'];
                            $campaign['items'][$i]['campaign_id'] = $items_datas[$room_id][$date]['campaign_id'];
                            $campaign['items'][$i]['restriction'] = $items_datas[$room_id][$date]['restriction'];
                            $campaign['items'][$i]['date'] = $items_datas[$room_id][$date]['date'];
                            $campaign['items'][$i]['stock'] = $items_datas[$room_id][$date]['stock'];
                            $campaign['items'][$i]['price'] = $items_datas[$room_id][$date]['price'];
                            $campaign['items'][$i]['minimum_night'] = $items_datas[$room_id][$date]['minimum_night'];

                            $i++;
                        } elseif (($items_datas[$room_id][$add_one_day]['stock'] != $items_datas[$room_id][$date]['stock']) || ($items_datas[$room_id][$add_one_day]['restriction'] != $items_datas[$room_id][$date]['restriction'])
                        ) {
                            $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                            $campaign['items'][$i]['end'] = $end_date;

                            $campaign['items'][$i]['restriction'] = $items_datas[$room_id][$date]['restriction'];
                            $campaign['items'][$i]['status'] = $items_datas[$room_id][$date]['status'];
                            $campaign['items'][$i]['room_id'] = $items_datas[$room_id][$date]['room_id'];
                            $campaign['items'][$i]['campaign_id'] = $items_datas[$room_id][$date]['campaign_id'];
                            $campaign['items'][$i]['restriction'] = $items_datas[$room_id][$date]['restriction'];
                            $campaign['items'][$i]['date'] = $items_datas[$room_id][$date]['date'];
                            $campaign['items'][$i]['stock'] = $items_datas[$room_id][$date]['stock'];
                            $campaign['items'][$i]['price'] = $items_datas[$room_id][$date]['price'];
                            $campaign['items'][$i]['minimum_night'] = $items_datas[$room_id][$date]['minimum_night'];

                            $i++;
                        } elseif (!array_key_exists($min_one_day, $items_datas[$room_id])) {
                            $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                            $campaign['items'][$i]['end'] = $end_date;

                            $campaign['items'][$i]['restriction'] = $items_datas[$room_id][$date]['restriction'];
                            $campaign['items'][$i]['status'] = $items_datas[$room_id][$date]['status'];
                            $campaign['items'][$i]['room_id'] = $items_datas[$room_id][$date]['room_id'];
                            $campaign['items'][$i]['campaign_id'] = $items_datas[$room_id][$date]['campaign_id'];
                            $campaign['items'][$i]['restriction'] = $items_datas[$room_id][$date]['restriction'];
                            $campaign['items'][$i]['date'] = $items_datas[$room_id][$date]['date'];
                            $campaign['items'][$i]['stock'] = $items_datas[$room_id][$date]['stock'];
                            $campaign['items'][$i]['price'] = $items_datas[$room_id][$date]['price'];
                            $campaign['items'][$i]['minimum_night'] = $items_datas[$room_id][$date]['minimum_night'];

                            $i++;
                        }
                    }
                }

                // Set Campaign's items
                $campaigns[$campaign_id] = $campaign;

                $process = self::process($post, $campaigns);
                if (!$process)
                    $success = FALSE;
            }
        }

        return $process;
    }

    public static function restrictions($post) {
        $campaigns = DB::select(
                        array('campaigns.id', 'campaign_id'), array('rooms.id', 'room_id'), array('items.stock', 'stock'), array('campaigns.minimum_number_of_nights', 'campaigns_minimum_night'), array('items.price', 'price'), array('campaigns.is_default', 'is_default'), array('items.minimum_night', 'minimum_night')
                )
                ->from('campaigns')
                ->join('campaigns_rooms', 'left')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                ->join('items')->on('items.room_id', '=', 'rooms.id')
                ->where('hotels.id', '=', $post['hotel_id'])

                // Filter stay date
                ->where_open()
                ->and_where('campaigns.stay_start_date', '<=', $post['start_date'])
                ->and_where('campaigns.stay_end_date', '>=', $post['end_date'])
                ->where_close()

                // Filter check in out
                ->where('items.date', '>=', $post['start_date'])
                ->where('items.date', '<=', $post['end_date'])
                ->group_by('campaign_id')
                ->group_by('room_id')
                ->execute()
                ->as_array('campaign_id');

        if (count($campaigns) > 0) {
            foreach ($campaigns as $campaign_id => &$campaign) {
                $campaign_id = $campaign['campaign_id'];
                $hotel_query = array(
                    'language_id' => Controller_Api_Request::default_language(),
                    'HotelCode' => $post['hotel_id'],
                    'check_in' => $post['start_date'],
                    'check_out' => $post['end_date'],
                    'InvTypeCode' => $campaign['room_id'],
                    'RatePlanCode' => $campaign['campaign_id'],
                    'now' => date("Y-m-d")
                );

                $i = 0;
                $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);

                /*
                 * Set Restrictions
                 */
                // Split all blackout or weekout
                $items_datas = array();
                foreach ($items as $date => $item) {
                    if (!empty($post['is_no_arrivals'][$date])) {
                        $restrictions['arrival'][$campaign_id][$date]['room_id'] = $item['room_id'];
                        $restrictions['arrival'][$campaign_id][$date]['campaign_id'] = $item['campaign_id'];
                        $restrictions['arrival'][$campaign_id][$date]['date'] = $item['date'];
                        $restrictions['arrival'][$campaign_id][$date]['price'] = $item['price'];
                        $restrictions['arrival'][$campaign_id][$date]['stock'] = $item['stock'];
                        $restrictions['arrival'][$campaign_id][$date]['minimum_night'] = $item['minimum_night'];
                        $restrictions['arrival'][$campaign_id][$date]['restriction'] = 'Arrival';
                        $restrictions['arrival'][$campaign_id][$date]['status'] = 'Close';
                    }
                    if (!empty($post['is_no_departures'][$date])) {
                        $restrictions['departure'][$campaign_id][$date]['room_id'] = $item['room_id'];
                        $restrictions['departure'][$campaign_id][$date]['campaign_id'] = $item['campaign_id'];
                        $restrictions['departure'][$campaign_id][$date]['date'] = $item['date'];
                        $restrictions['departure'][$campaign_id][$date]['price'] = $item['price'];
                        $restrictions['departure'][$campaign_id][$date]['stock'] = $item['stock'];
                        $restrictions['departure'][$campaign_id][$date]['minimum_night'] = $item['minimum_night'];
                        $restrictions['departure'][$campaign_id][$date]['restriction'] = 'Departure';
                        $restrictions['departure'][$campaign_id][$date]['status'] = 'Close';
                    }
                }

                /*
                 * Build time span
                 */
                $i = 0;
                $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['start_date'], $post['end_date']);
                foreach ($restrictions as $key => $items_datas) {
                    foreach ($dateranges as $daterange) {




                        $date = $daterange->format("Y-m-d");

                        reset($items_datas);
                        $first_array = key($items_datas);


                        if (!empty($items_datas[$campaign_id][$date])) {

                            $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
                            $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

                            if ($first_array == $date || $items_datas[$campaign_id][$min_one_day]['status'] != $items_datas[$campaign_id][$date]['status']) {
                                $campaign['items'][$i]['start'] = $date;
                            } elseif ($items_datas[$campaign_id][$min_one_day]['stock'] != $items_datas[$campaign_id][$date]['stock']) {
                                $campaign['items'][$i]['start'] = $date;
                            } elseif (!array_key_exists($min_one_day, $items_datas[$campaign_id])) {
                                $campaign['items'][$i]['start'] = $date;
                            }

                            if ($items_datas[$campaign_id][$add_one_day]['status'] != $items_datas[$campaign_id][$date]['status']
                            ) {
                                $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                                // Set new End date
                                $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                                $campaign['items'][$i]['end'] = $end_date;

                                $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                                $campaign['items'][$i]['status'] = $items_datas[$campaign_id][$date]['status'];
                                $campaign['items'][$i]['room_id'] = $items_datas[$campaign_id][$date]['room_id'];
                                $campaign['items'][$i]['campaign_id'] = $items_datas[$campaign_id][$date]['campaign_id'];
                                $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                                $campaign['items'][$i]['date'] = $items_datas[$campaign_id][$date]['date'];
                                $campaign['items'][$i]['stock'] = $items_datas[$campaign_id][$date]['stock'];
                                $campaign['items'][$i]['price'] = $items_datas[$campaign_id][$date]['price'];
                                $campaign['items'][$i]['minimum_night'] = $items_datas[$campaign_id][$date]['minimum_night'];

                                $i++;
                            } elseif ($items_datas[$campaign_id][$add_one_day]['stock'] != $items_datas[$campaign_id][$date]['stock']
                            ) {
                                $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                                // Set new End date
                                $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                                $campaign['items'][$i]['end'] = $end_date;

                                $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                                $campaign['items'][$i]['status'] = $items_datas[$campaign_id][$date]['status'];
                                $campaign['items'][$i]['room_id'] = $items_datas[$campaign_id][$date]['room_id'];
                                $campaign['items'][$i]['campaign_id'] = $items_datas[$campaign_id][$date]['campaign_id'];
                                $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                                $campaign['items'][$i]['date'] = $items_datas[$campaign_id][$date]['date'];
                                $campaign['items'][$i]['stock'] = $items_datas[$campaign_id][$date]['stock'];
                                $campaign['items'][$i]['price'] = $items_datas[$campaign_id][$date]['price'];
                                $campaign['items'][$i]['minimum_night'] = $items_datas[$campaign_id][$date]['minimum_night'];

                                $i++;
                            } elseif (!array_key_exists($min_one_day, $items_datas[$campaign_id])) {
                                $campaign['items'][$i]['start'] = $campaign['items'][$i]['start'] ? $campaign['items'][$i]['start'] : $date;

                                // Set new End date
                                $end_date = date('Y-m-d', strtotime($date . ' +1 day'));

                                $campaign['items'][$i]['end'] = $end_date;

                                $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                                $campaign['items'][$i]['status'] = $items_datas[$campaign_id][$date]['status'];
                                $campaign['items'][$i]['room_id'] = $items_datas[$campaign_id][$date]['room_id'];
                                $campaign['items'][$i]['campaign_id'] = $items_datas[$campaign_id][$date]['campaign_id'];
                                $campaign['items'][$i]['restriction'] = $items_datas[$campaign_id][$date]['restriction'];
                                $campaign['items'][$i]['date'] = $items_datas[$campaign_id][$date]['date'];
                                $campaign['items'][$i]['stock'] = $items_datas[$campaign_id][$date]['stock'];
                                $campaign['items'][$i]['price'] = $items_datas[$campaign_id][$date]['price'];
                                $campaign['items'][$i]['minimum_night'] = $items_datas[$campaign_id][$date]['minimum_night'];

                                $i++;
                            }
                        }
                    }
                }
            }
        }
        $process = self::process($post, $campaigns);

        return $process;
    }

    public static function process($post, $items_datas) {
        // Load Config
        if (Kohana::$environment == Kohana::PRODUCTION) {
            $config = Kohana::$config->load('api')->vacation;
        } else {
            $config = Kohana::$config->load('api')->vacation_test;
        }

        // Get request ID
        $request_id = Controller_Vacation_Notifications_Static::update_api_access();

        $request_id = date('YmdHms') . str_pad($request_id, 9, '0', STR_PAD_LEFT);        
        /*
         * XML build
         */
        $transaction = 'OTA_HotelAvailNotifRQ';
        $transactionRQ = str_replace('OTA_', '', $transaction);
        
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
            $envelope->setAttribute('xmlns:soap-env', $config['soap_env']);

            $head = $envelope->appendChild($doc->createElement('soap-env:Header'));

            $Interface = $head->appendChild($doc->createElement('Interface'));
            $Interface->setAttribute('ChannelIdentifierId', 'HIS_VACATION_XML4H');
            $Interface->setAttribute('Version', $config['Version']);
            $Interface->setAttribute('Interface', $config['Interface']);
            $Interface->setAttribute('xmlns', 'http://api.hotels-vacation.com/Documentation/XML/OTA/4/2011B/');

            $ComponentInfo = $Interface->appendChild($doc->createElement('ComponentInfo'));
            $ComponentInfo->setAttribute('User', $config['User']);
            $ComponentInfo->setAttribute('Pwd', $config['Pwd']);
            $ComponentInfo->setAttribute('ComponentType', $config['ComponentType']);

            $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
            $body->setAttribute('RequestId', $request_id);
            $body->setAttribute('Transaction', $transactionRQ);

            $response_node = $body->appendChild($doc->createElement($transaction));
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('TimeStamp', date('c'));
            $response_node->setAttribute('Target', $config['Target']);
            $response_node->setAttribute('Version', '1.000');
            $response_node->setAttribute('PrimaryLangID', 'en');

            $AvailStatusMessages = $response_node->appendChild($doc->createElement('AvailStatusMessages'));
            $AvailStatusMessages->setAttribute('HotelCode', $post['hotel_id']);

            foreach ($items_datas as $campaign_id => $item_data) {

                if (is_array($item_data['items']) && count($item_data['items']) > 0) {
                    foreach ($item_data['items'] as $data) {
                        $AvailStatusMessage = $AvailStatusMessages->appendChild($doc->createElement('AvailStatusMessage'));
                        $AvailStatusMessage->setAttribute('BookingLimit', $data['stock']);
                        $AvailStatusMessage->setAttribute('BookingLimitMessageType', 'SetLimit');

                        $StatusApplicationControl = $AvailStatusMessage->appendChild($doc->createElement('StatusApplicationControl'));
                        $StatusApplicationControl->setAttribute('Start', $data['start']);
                        $StatusApplicationControl->setAttribute('End', $data['end']);
                        $StatusApplicationControl->setAttribute('RatePlanCode', $data['campaign_id']);
                        $StatusApplicationControl->setAttribute('InvCode', $data['room_id']);
                        $StatusApplicationControl->setAttribute('IsRoom', 'true');
                        $StatusApplicationControl->setAttribute('InvCodeApplication', 'InvCode');
                        $StatusApplicationControl->setAttribute('RatePlanCodeType', 'RatePlanCode');

                        $LengthsOfStay = $AvailStatusMessage->appendChild($doc->createElement('LengthsOfStay'));
                        $LengthsOfStay->setAttribute('ArrivalDateBased', 1);

                        if (($item_data['is_default'] == 1) && ($data['minimum_night'] > 1)) {
                            $LengthsOfStay_min = $LengthsOfStay->appendChild($doc->createElement('LengthOfStay'));
                            $LengthsOfStay_min->setAttribute('MinMaxMessageType', 'SetForwardMinStay');
                            $LengthsOfStay_min->setAttribute('TimeUnit', 'Day');
                            $LengthsOfStay_min->setAttribute('Time', $data['minimum_night']);
                        } else {
                            $LengthsOfStay_min = $LengthsOfStay->appendChild($doc->createElement('LengthOfStay'));
                            $LengthsOfStay_min->setAttribute('MinMaxMessageType', 'SetMinLOS');
                            $LengthsOfStay_min->setAttribute('TimeUnit', 'Day');
                            $LengthsOfStay_min->setAttribute('Time', $item_data['campaigns_minimum_night']);
                        }

                        $LengthsOfStay_max = $LengthsOfStay->appendChild($doc->createElement('LengthOfStay'));
                        $LengthsOfStay_max->setAttribute('MinMaxMessageType', 'SetMaxLOS');
                        $LengthsOfStay_max->setAttribute('TimeUnit', 'Day');
                        $LengthsOfStay_max->setAttribute('Time', 14);

                        $RestrictionStatus = $AvailStatusMessage->appendChild($doc->createElement('RestrictionStatus'));
                        $RestrictionStatus->setAttribute('Restriction', $data['restriction']);
                        $RestrictionStatus->setAttribute('Status', $data['status']);

                        if ($data['restriction'] == 'Departure') {
                            if ($data['restriction_arrival'] != '') {
                                $data['restriction'] = 'Arrival';
                                self::regenerateNodeXmlWhenRestrictionsMoreThan1($data, $AvailStatusMessages, $doc, $item_data);
                            }
                        }
                    }
                } else {
                    // Nothing to do here, move along!
                }
            }
            
            $response = $doc->saveXML();
            $post['status_hotel'] = $post['status_hotel'] ? 'Active' : 'Inactive';
            CustomLog::factory()->add(2, 'DEBUG', 'Hotel ' . $post['hotel_id'] . ' Status : ' . $post['status_hotel']);
            
            // Request Process, check hotel is in specific hotel or not
            if (Controller_Vacation_Trigger::checkHotelVacation($post['hotel_id'])) {
                $result = Controller_Vacation_Notifications_Static::post($response, $request_id);
//                exit();
            } else {
                $result = FALSE;
            }
            return $result;
        } catch (Kohana_Exception $e) {
            $result = FALSE;
            return $result;
        }
    }

    public static function regenerateNodeXmlWhenRestrictionsMoreThan1($data, $AvailStatusMessages, $doc, $item_data) {
        $AvailStatusMessage = $AvailStatusMessages->appendChild($doc->createElement('AvailStatusMessage'));
        $AvailStatusMessage->setAttribute('BookingLimit', $data['stock']);
        $AvailStatusMessage->setAttribute('BookingLimitMessageType', 'SetLimit');

        $StatusApplicationControl = $AvailStatusMessage->appendChild($doc->createElement('StatusApplicationControl'));
        $StatusApplicationControl->setAttribute('Start', $data['start']);
        $StatusApplicationControl->setAttribute('End', $data['end']);
        $StatusApplicationControl->setAttribute('RatePlanCode', $data['campaign_id']);
        $StatusApplicationControl->setAttribute('InvCode', $data['room_id']);
        $StatusApplicationControl->setAttribute('IsRoom', 'true');
        $StatusApplicationControl->setAttribute('InvCodeApplication', 'InvCode');
        $StatusApplicationControl->setAttribute('RatePlanCodeType', 'RatePlanCode');

        $LengthsOfStay = $AvailStatusMessage->appendChild($doc->createElement('LengthsOfStay'));
        $LengthsOfStay->setAttribute('ArrivalDateBased', 1);

        if (($item_data['is_default'] == 1) && ($data['minimum_night'] > 1)) {
            $LengthsOfStay_min = $LengthsOfStay->appendChild($doc->createElement('LengthOfStay'));
            $LengthsOfStay_min->setAttribute('MinMaxMessageType', 'SetForwardMinStay');
            $LengthsOfStay_min->setAttribute('TimeUnit', 'Day');
            $LengthsOfStay_min->setAttribute('Time', $data['minimum_night']);
        } else {
            $LengthsOfStay_min = $LengthsOfStay->appendChild($doc->createElement('LengthOfStay'));
            $LengthsOfStay_min->setAttribute('MinMaxMessageType', 'SetMinLOS');
            $LengthsOfStay_min->setAttribute('TimeUnit', 'Day');
            $LengthsOfStay_min->setAttribute('Time', $item_data['campaigns_minimum_night']);
        }

        $LengthsOfStay_max = $LengthsOfStay->appendChild($doc->createElement('LengthOfStay'));
        $LengthsOfStay_max->setAttribute('MinMaxMessageType', 'SetMaxLOS');
        $LengthsOfStay_max->setAttribute('TimeUnit', 'Day');
        $LengthsOfStay_max->setAttribute('Time', 14);

        $RestrictionStatus = $AvailStatusMessage->appendChild($doc->createElement('RestrictionStatus'));
        $RestrictionStatus->setAttribute('Restriction', $data['restriction']);
        $RestrictionStatus->setAttribute('Status', $data['status']);
    }

}
