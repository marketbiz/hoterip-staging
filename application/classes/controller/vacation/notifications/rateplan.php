<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Notifications_Rateplan extends Controller_Layout_Admin {

    public static function item($post) {
        $process = TRUE;

        // Get Available Campaign
        $campaigns = DB::select(
                        array('campaigns.id', 'campaign_id'), array('campaigns.minimum_number_of_nights', 'minimum_number_of_nights'), array('campaigns.number_of_free_nights', 'number_of_free_nights'), array('campaigns.id', 'campaign_id'), array('campaigns_rooms.room_id', 'room_id'), array('room_texts.name', 'room_name')
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->join('room_texts')->on('room_texts.room_id', '=', 'campaigns_rooms.room_id')
                ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
                ->where('campaigns_rooms.room_id', '=', $post['room_id'])
                ->where('campaigns.is_show', '=', 1)
                ->where('rooms.hoterip_campaign', '=', 0)
                ->where('campaigns.number_of_free_nights', '!=', 0)
        ;
        // Is campaign default
        if ($post['campaign_id']) {
            $campaigns = $campaigns->where('campaigns.id', '=', $post['campaign_id']);
        } else {
            $campaigns = $campaigns->where('campaigns.is_default', '=', 1);
        }

        // Filter stay date
        $campaigns = $campaigns
                ->where_open()
                ->and_where('campaigns.stay_start_date', '<=', $post['start_date'])
                ->and_where('campaigns.stay_end_date', '>=', $post['end_date'])
                ->where_close()
                ->group_by('campaign_id')
                ->execute()
                ->as_array('campaign_id');

        if (count($campaigns)) {
            foreach ($campaigns as $key => &$campaign) {
                $hotel_query = array(
                    'language_id' => Controller_Api_Request::default_language(),
                    'HotelCode' => $post['hotel_id'],
                    'check_in' => $post['start_date'],
                    'check_out' => $post['end_date'],
                    'InvTypeCode' => $campaign['room_id'],
                    'RatePlanCode' => $campaign['campaign_id'],
                    'now' => date("Y-m-d")
                );

                $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);

                /*
                 * Find no arrival date
                 */
                $no_arrivals = DB::select(
                                'hotel_no_arrivals.date'
                        )
                        ->from('hotel_no_arrivals')
                        ->where('hotel_id', '=', $post['hotel_id'])
                        ->where('date', '>=', date("Y-m-d"))
                        ->execute()
                        ->as_array('date');

                // Remove all default price items
                if (count($items)) {
                    foreach ($items as $date => &$item) {
                        $item['RestrictionType'] = 'stay';
                        // if($no_arrivals[$date])
                        // {
                        //   $item['RestrictionType'] = 'Arrival';
                        // }
                        // elseif($item['blackout'])
                        // {
                        //   $item['RestrictionType'] = 'Stay';
                        // }
                        // elseif($item['weekout'])
                        // {
                        //   $item['RestrictionType'] = 'Stay';
                        // }
                        // elseif(!$item['weekout'] AND !$item['blackout'])
                        // {
                        //   unset($items[$date]);
                        // }
                        // else
                        // {
                        //   unset($items[$date]);
                        // }
                    }
                }

                $i = 0;
                // Date ranges
                $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['start_date'], $post['end_date']);

                foreach ($dateranges as $daterange) {
                    $date = $daterange->format("Y-m-d");

                    reset($items);
                    $first_array = key($items);

                    if (!empty($items[$date])) {
                        $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
                        $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

                        if ($items[$date]['RestrictionType'] == 'Arrival') {
                            $campaign['timespans'][$i]['start'] = $date;
                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));
                            $campaign['timespans'][$i]['end'] = $end_date;
                            $campaign['timespans'][$i]['RestrictionType'] = $items[$date]['RestrictionType'];
                            $i++;
                        } else {
                            if ($first_array == $date || empty($items[$min_one_day]) || !array_key_exists($min_one_day, $items) || $items[$min_one_day]['RestrictionType'] != $items[$date]['RestrictionType']
                            ) {
                                $campaign['timespans'][$i]['start'] = $date;
                            }

                            if (empty($items[$add_one_day]) || !array_key_exists($add_one_day, $items) || $items[$add_one_day]['RestrictionType'] != $items[$date]['RestrictionType']
                            ) {
                                // Set new End date
                                $end_date = date('Y-m-d', strtotime($date . ' +1 day'));
                                $campaign['timespans'][$i]['end'] = $end_date;
                                // Restrictions type
                                $campaign['timespans'][$i]['RestrictionType'] = $items[$date]['RestrictionType'];
                                $i++;
                            }
                        }
                    }
                }

                // Process
                $status_process = self::process($post, $campaign);

                if (!$status_process) {
                    $process = FALSE;
                }
            }
        }

        return $process;
    }

    public static function promotion($post) {
        $campaign = array();

        $hotel_query = array(
            'language_id' => Controller_Api_Request::default_language(),
            'HotelCode' => $post['hotel_id'],
            'check_in' => date("Y-m-d"),
            'check_out' => $post['stay_end_date'],
            'InvTypeCode' => $post['room_id'],
            'RatePlanCode' => $post['campaign_id'],
            'now' => date("Y-m-d")
        );

        // Attempt to get items of matching campaign
        $items = ORM::factory('campaign')->get_items($post['campaign_id'], $hotel_query);

        // Retry to get items of matching campaign by ignoring stay date
        if (!count($items)) {
            $items = ORM::factory('campaign')->get_items($post['campaign_id'], $hotel_query, TRUE);
        }

        // Room Name
        $campaign['room_name'] = DB::select()
                ->from('room_texts')
                ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
                ->where('room_texts.room_id', '=', $post['room_id'])
                ->execute()
                ->get('name');

        $campaign['campaign_id'] = $post['campaign_id'];
        $campaign['room_id'] = $post['room_id'];
        $campaign['minimum_number_of_nights'] = $post['minimum_number_of_nights'];
        $campaign['number_of_free_nights'] = $post['number_of_free_nights'];

        /*
         * Find no arrival date
         */
        $no_arrivals = DB::select(
                        'hotel_no_arrivals.date'
                )
                ->from('hotel_no_arrivals')
                ->where('hotel_id', '=', $post['hotel_id'])
                ->where('date', '>=', date("Y-m-d"))
                ->execute()
                ->as_array('date');

        // Remove all default price items
        if (count($items) > 0) {
            foreach ($items as $date => &$item) {
                if ($no_arrivals[$date]) {
                    $item['RestrictionType'] = 'Arrival';
                } elseif ($item['blackout']) {
                    $item['RestrictionType'] = 'Stay';
                } elseif ($item['weekout']) {
                    $item['RestrictionType'] = 'Stay';
                } elseif (!$item['weekout'] AND ! $item['blackout']) {
                    unset($items[$date]);
                } else {
                    unset($items[$date]);
                }
            }

            // Date ranges
            $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['stay_start_date'], $post['stay_end_date']);

            foreach ($dateranges as $daterange) {
                $date = $daterange->format("Y-m-d");

                reset($items);
                $first_array = key($items);

                if (!empty($items[$date])) {
                    $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
                    $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

                    if ($items[$date]['RestrictionType'] == 'Arrival') {
                        $campaign['timespans'][$i]['start'] = $date;
                        // Set new End date
                        $end_date = date('Y-m-d', strtotime($date . ' +1 day'));
                        $campaign['timespans'][$i]['end'] = $end_date;
                        $campaign['timespans'][$i]['RestrictionType'] = $items[$date]['RestrictionType'];
                        $i++;
                    } else {
                        if ($first_array == $date || empty($items[$min_one_day]) || !array_key_exists($min_one_day, $items) || $items[$min_one_day]['RestrictionType'] != $items[$date]['RestrictionType']
                        ) {
                            $campaign['timespans'][$i]['start'] = $date;
                        }

                        if (empty($items[$add_one_day]) || !array_key_exists($add_one_day, $items) || $items[$add_one_day]['RestrictionType'] != $items[$date]['RestrictionType']
                        ) {
                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));
                            $campaign['timespans'][$i]['end'] = $end_date;
                            // Restrictions type
                            $campaign['timespans'][$i]['RestrictionType'] = $items[$date]['RestrictionType'];
                            $i++;
                        }
                    }
                }
            }
        }

        // Process
        $process = self::process($post, $campaign);

        return $process;
    }

    public static function restrictions($post) {
        $process = TRUE;

        // Get Available Campaign
        $campaigns = DB::select(
                        array('campaigns.id', 'campaign_id'), array('campaigns.minimum_number_of_nights', 'minimum_number_of_nights'), array('campaigns.number_of_free_nights', 'number_of_free_nights'), array('campaigns_rooms.room_id', 'room_id'), array('room_texts.name', 'room_name')
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                ->join('room_texts')->on('room_texts.room_id', '=', 'campaigns_rooms.room_id')
                ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
                ->where('hotels.id', '=', $post['hotel_id'])
                ->where('campaigns.number_of_free_nights  ', '!=', 0)
                ->where('campaigns.is_show', '=', 1)

                // Filter stay date
                ->where_open()
                ->and_where('campaigns.stay_start_date', '<=', $post['start_date'])
                ->and_where('campaigns.stay_end_date', '>=', $post['end_date'])
                ->where_close()
                ->group_by('campaign_id')
                ->group_by('room_id')
                ->execute()
                ->as_array('campaign_id');

        foreach ($campaigns as $key => &$campaign) {
            $hotel_query = array(
                'language_id' => Controller_Api_Request::default_language(),
                'HotelCode' => $post['hotel_id'],
                'check_in' => $post['start_date'],
                'check_out' => $post['end_date'],
                'InvTypeCode' => $campaign['room_id'],
                'RatePlanCode' => $campaign['campaign_id'],
                'now' => date("Y-m-d")
            );

            $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);

            // Remove all default price items
            if (count($items)) {
                foreach ($items as $date => &$item) {
                    if ($post['is_no_arrivals'][$date]) {
                        $item['RestrictionType'] = 'Arrival';
                    } else {
                        unset($items[$date]);
                    }
                }
            }

            $i = 0;
            // Date ranges
            $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['start_date'], $post['end_date']);

            foreach ($dateranges as $daterange) {
                $date = $daterange->format("Y-m-d");

                reset($items);
                $first_array = key($items);

                if (!empty($items[$date])) {
                    $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
                    $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

                    if ($items[$date]['RestrictionType'] == 'Arrival') {
                        $campaign['timespans'][$i]['start'] = $date;
                        // Set new End date
                        $end_date = date('Y-m-d', strtotime($date . ' +1 day'));
                        $campaign['timespans'][$i]['end'] = $end_date;
                        $campaign['timespans'][$i]['RestrictionType'] = $items[$date]['RestrictionType'];
                        $i++;
                    } else {
                        if ($first_array == $date || empty($items[$min_one_day]) || !array_key_exists($min_one_day, $items) || $items[$min_one_day]['RestrictionType'] != $items[$date]['RestrictionType']
                        ) {
                            $campaign['timespans'][$i]['start'] = $date;
                        }

                        if (empty($items[$add_one_day]) || !array_key_exists($add_one_day, $items) || $items[$add_one_day]['RestrictionType'] != $items[$date]['RestrictionType']
                        ) {
                            // Set new End date
                            $end_date = date('Y-m-d', strtotime($date . ' +1 day'));
                            $campaign['timespans'][$i]['end'] = $end_date;
                            // Restrictions type
                            $campaign['timespans'][$i]['RestrictionType'] = $items[$date]['RestrictionType'];
                            $i++;
                        }
                    }
                }
            }

            // Process
            $status_process = self::process($post, $campaign);

            if (!$status_process) {
                $process = FALSE;
            }
        }

        return $process;
    }

    public static function process($post, $campaign) {
        // Load Config
        if (Kohana::$environment == Kohana::PRODUCTION) {
            $config = Kohana::$config->load('api')->vacation;
        } else {
            $config = Kohana::$config->load('api')->vacation_test;
        }

        // Get request ID
        $request_id = Controller_Vacation_Notifications_Static::update_api_access();

        $request_id = date('YmdHms') . str_pad($request_id, 9, '0', STR_PAD_LEFT);

        /*
         * XML build
         */

        $transaction = 'OTA_HotelRatePlanNotifRQ';
        $transactionRQ = str_replace('OTA_', '', $transaction);

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
            $envelope->setAttribute('xmlns:soap-env', $config['soap_env']);

            $head = $envelope->appendChild($doc->createElement('soap-env:Header'));

            $Interface = $head->appendChild($doc->createElement('Interface'));
            $Interface->setAttribute('ChannelIdentifierId', 'HIS_VACATION_XML4H');
            $Interface->setAttribute('Version', $config['Version']);
            $Interface->setAttribute('Interface', $config['Interface']);
            $Interface->setAttribute('xmlns', 'http://api.hotels-vacation.com/Documentation/XML/OTA/4/2011B/');

            $ComponentInfo = $Interface->appendChild($doc->createElement('ComponentInfo'));
            $ComponentInfo->setAttribute('User', $config['User']);
            $ComponentInfo->setAttribute('Pwd', $config['Pwd']);
            $ComponentInfo->setAttribute('ComponentType', $config['ComponentType']);

            $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
            $body->setAttribute('RequestId', $request_id);
            $body->setAttribute('Transaction', $transactionRQ);

            $response_node = $body->appendChild($doc->createElement($transaction));
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('TimeStamp', date('c'));
            $response_node->setAttribute('Target', $config['Target']);
            $response_node->setAttribute('Version', '1.000');
            $response_node->setAttribute('PrimaryLangID', 'en');

            $RatePlans = $response_node->appendChild($doc->createElement('RatePlans'));
            $RatePlans->setAttribute('HotelCode', $post['hotel_id']);

            $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
            $RatePlan->setAttribute('RatePlanCode', $campaign['campaign_id']);
            // $Rates = $RatePlan->appendChild($doc->createElement('Rates'));
            // $Rate = $Rates->appendChild($doc->createElement('Rate'));
            // $Rate->setAttribute('RatePlanCode', $campaign['campaign_id']);
            // $Rate->setAttribute('InvCode', $campaign['room_id']);

            $SellableProducts = $RatePlan->appendChild($doc->createElement('SellableProducts'));
            $SellableProduct = $SellableProducts->appendChild($doc->createElement('SellableProduct'));
            $SellableProduct->setAttribute('InvCode', $campaign['room_id']);

            $Offers = $response_node->appendChild($doc->createElement('Offers'));
            $Offer = $Offers->appendChild($doc->createElement('Offer'));
            $OfferRules = $Offer->appendChild($doc->createElement('OfferRules'));
            $OfferRule = $OfferRules->appendChild($doc->createElement('OfferRule'));

            // Create text Descriptions
            $text = 'Stay ' . (($campaign['minimum_number_of_nights'] + $campaign['number_of_free_nights']) - 1) . ' - pay ' . ($campaign['minimum_number_of_nights'] - 1) . ' nights';

            if (!empty($campaign['number_of_free_nights']) && $campaign['number_of_free_nights'] > 0) {
                $DateRestriction = $OfferRule->appendChild($doc->createElement('DateRestriction'));
                $DateRestriction->setAttribute('Start', $post['stay_start_date']);
                $DateRestriction->setAttribute('End', $post['stay_end_date']);
                $DateRestriction->setAttribute('RestrictionType', strtolower('Stay'));
            } elseif (count($campaign['timespans'])) {
                foreach ($campaign['timespans'] as $key => $timespan) {
                    $DateRestriction = $OfferRule->appendChild($doc->createElement('DateRestriction'));
                    $DateRestriction->setAttribute('Start', $timespan['start']);
                    $DateRestriction->setAttribute('End', $timespan['end']);
                    $DateRestriction->setAttribute('RestrictionType', strtolower($timespan['RestrictionType']));
                }
            }



            $Discount = $Offer->appendChild($doc->createElement('Discount'));
            $Discount->setAttribute('NightsRequired', $campaign['minimum_number_of_nights']);
            $Discount->setAttribute('NightsDiscounted', $campaign['number_of_free_nights']);
            $Discount->setAttribute('Percent', 100);

            $OfferDescription = $Offer->appendChild($doc->createElement('OfferDescription'));
            $Text = $OfferDescription->appendChild($doc->createElement('Text', $text));

            $response = $doc->saveXML();

            // Request Process, check hotel is in specific hotel or not
            if (Controller_Vacation_Trigger::checkHotelVacation($post['hotel_id'])) {
                $result = Controller_Vacation_Notifications_Static::post($response, $request_id);
            } else {
                $result = FALSE;
            }
        } catch (Kohana_Exception $e) {
            $result = FALSE;
        }

        return $result;
    }

}
