<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Vacation_Notifications_Amount extends Controller_Layout_Admin {

  public static function item($post)
  {
      // Load Campaigns
      $campaigns = DB::select(
        'campaigns.*',
        array('campaigns.id', 'campaign_id'),
        array('hotels.service_charge_rate', 'service_charge_rate'),
        array('hotels.tax_rate', 'tax_rate'),
        array('hotels.currency_id', 'currency_id'),
        array('rooms.id', 'room_id'),
        array('rooms.extrabed_price', 'extrabed'),
        array('rooms.adult_breakfast_price', 'adult_breakfast_price'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_adults'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_extrabeds'),
        array(DB::expr('MAX(room_capacities.number_of_children)'), 'number_of_children')
        )
        ->from('campaigns')
        ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
        ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
        ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
        ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
        ->where('hotels.id', '=', $post['hotel_id'])
        ->where('rooms.id', '=', $post['room_id'])
        
        ->and_where_open()
          ->where('rooms.hoterip_campaign', '=', 0)
          ->where('rooms.is_breakfast_included', '=', 1)
          ->where('campaigns.is_show', '=', 1)

          //request date 2014-06-16 -> campaign early bird send again to vacation
          // ->where('campaigns.days_in_advance', '=', 0)
        ->and_where_close()

        ;
        // Is campaign default
        if($post['campaign_id'])
        {
          $campaigns = $campaigns->where('campaigns.id', '=', $post['campaign_id']);
        }
        else
        {
          $campaigns = $campaigns->where('campaigns.is_default', '=', 1);
        }

        // Filter amount discount
        // ->where_open()
        // ->where('campaigns.discount_amount', '>', 0)
        // ->or_where('campaigns.discount_rate', '>', 0)
        // ->where_close()

        // Filter stay date
        $campaigns = $campaigns
        /* Kawamura's Confirmation
         * "but the example is what I am saying."
         * from:   kawamura@his-bali.com
         * date:   Mon, Mar 17, 2014 at 12:46 PM
         * subject:   Re: Vacation test case 05032014
         */
        // ->where_open()
        // ->and_where('campaigns.stay_start_date', '<=', $post['start_date'])
        // ->and_where('campaigns.stay_end_date', '>=', $post['end_date'])
        // ->where_close()
        ->group_by('campaigns.id')
        ->execute()
        ->as_array('campaign_id');

      if(count($campaigns))
      {
        foreach ($campaigns as $key => &$campaign)
        {
          $hotel_query = array(
            'language_id' => Controller_Api_Request::default_language(),
            'HotelCode' =>$post['hotel_id'],
            'check_in' => $post['start_date'],
            'check_out' => $post['end_date'],
            'InvTypeCode' => $campaign['room_id'],
            'RatePlanCode' => $campaign['campaign_id'],
            'now' => date("Y-m-d")
          );

          if($post['home'])
          {
             if(!$post['hotel_active_vacation'])
            {
              $hotel_query['hotel_active_vacation'] = TRUE;
            }
            else
            {
              $hotel_query['hotel_active_vacation'] = FALSE;
            }           
          }

          //add parameter for get item campaign not valid
          $hotel_query['campaign_valid'] = 1;
          if($campaign['booking_end_date']<$hotel_query['now'])
          {
            $hotel_query['campaign_valid'] = 0;
          }

          // Attempt to get items of matching campaign
          $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);

          //get surcharge
          $surcharge_all = Model::factory('surcharge')->get_surcharges($post['hotel_id'], $hotel_query['check_in'], $hotel_query['check_out']);
          $surcharge_date = array();

          // Retry to get items of matching campaign by ignoring stay date
          if (!count($items))
          {
            $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query, TRUE);
          }

          //change key array surcharge_all to surcharge_date
          foreach ($surcharge_all as $key => $value) {
              $surcharge_date[$value['date']] = $value ;
          }

          //combine array surcharge_date to items by
          foreach ($surcharge_date as $key => $value) {
            $items[$key]['surcharge']= TRUE;
          }

          // Remove all default price items
          foreach ($items as $date => $item)
          {
            if($item['blackout']
              || $item['weekout']
              || ($item['price'] == 0)
              )
            {
              unset($items[$date]);
            }
          }

          // Date ranges
          $i = 0;
          $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['start_date'], $post['end_date']);

          foreach($dateranges as $daterange)
          {
            $date = $daterange->format("Y-m-d");

            reset($items);
            $first_array = key($items);

            if(!empty($items[$date]))
            {
              $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
              $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

              if($first_array == $date
                || empty($items[$min_one_day]) 
                || !array_key_exists($min_one_day, $items)
                || ($items[$min_one_day]['price'] != $items[$date]['price']
                || $items[$min_one_day]['extrabed_item_price'] != $items[$date]['extrabed_item_price']
                //open date if surcharge is true in frist array items
                || $items[$date]['surcharge'] == TRUE)  
                )
              {

                if($items[$date]['surcharge'] == TRUE)
                {
                  $campaign['items'][$i]['surcharge'] = $surcharge_date[$date];
                }
                $campaign['items'][$i]['start'] = $date;
                $campaign['items'][$i]['extrabed_item_price'] = $items[$date]['extrabed_item_price'];
                $campaign['items'][$i]['room_id'] = $campaign['room_id'];
                $campaign['items'][$i]['campaign_id'] = $campaign['campaign_id'];

                // Determine Price in this items conditions
                if ($item['campaign_blackout'])
                {
                  $campaign['items'][$i]['price'] = $items[$date]['price'];
                }
                else
                {
                  // If promotion
                  if (!$campaign['is_default'])
                  {
                    // If free night
                    if ($item['free_night'])
                    {
                      $campaign['items'][$i]['price'] = $items[$date]['price'];
                    }
                    // If campaign prices
                    elseif ($items[$date]['is_campaign_prices'])
                    {
                      $campaign['items'][$i]['price'] = $items[$date]['price'];
                    }
                    else
                    {
                      $campaign['items'][$i]['price'] = ($items[$date]['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                    }
                  }
                  else
                  {
                    $campaign['items'][$i]['price'] = $items[$date]['price'];
                  }
                }
              }
              //open date if surcharge is True 
              elseif($items[$min_one_day]['surcharge'] == TRUE)
              {
                if($items[$date]['surcharge'] == TRUE)
                {
                  $campaign['items'][$i]['surcharge'] = $surcharge_date[$date];
                }
                $campaign['items'][$i]['start'] = $date;
                $campaign['items'][$i]['extrabed_item_price'] = $items[$date]['extrabed_item_price'];
                $campaign['items'][$i]['room_id'] = $campaign['room_id'];
                $campaign['items'][$i]['campaign_id'] = $campaign['campaign_id'];

                // Determine Price in this items conditions
                if ($item['campaign_blackout'])
                {
                  $campaign['items'][$i]['price'] = $items[$date]['price'];
                }
                else
                {
                  // If promotion
                  if (!$campaign['is_default'])
                  {
                    // If free night
                    if ($item['free_night'])
                    {
                      $campaign['items'][$i]['price'] = $items[$date]['price'];
                    }
                    // If campaign prices
                    elseif ($items[$date]['is_campaign_prices'])
                    {
                      $campaign['items'][$i]['price'] = $items[$date]['price'];
                    }
                    else
                    {
                      $campaign['items'][$i]['price'] = ($items[$date]['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                    }
                  }
                  else
                  {
                    $campaign['items'][$i]['price'] = $items[$date]['price'];
                  }
                }
              }
              //close date if surcharge is true
              if($items[$date]['surcharge'])
              {
                $end_date = date( 'Y-m-d', strtotime( $date . ' +1 day' ) );

                $campaign['items'][$i]['end'] = $end_date;
                $i++;
              }
              elseif(empty($items[$add_one_day]) 
                || !array_key_exists($add_one_day, $items)
                || ($items[$add_one_day]['price'] != $items[$date]['price']
                || $items[$add_one_day]['extrabed_item_price'] != $items[$date]['extrabed_item_price'])
                //close date if surcharge is true in frist array items
                || $items[$add_one_day]['surcharge']) 
              {
                // Set new End date
                $end_date = date( 'Y-m-d', strtotime( $date . ' +1 day' ) );

                $campaign['items'][$i]['end'] = $end_date;
                $i++;
              }
            }
          }
        }

        // CustomLog::factory()->add(2,'DEBUG', 'API  Var Post '.print_r(json_encode($post), 1));
        // CustomLog::factory()->add(2,'DEBUG', 'API  Var Campaigns '.print_r(json_encode($campaigns), 1));

        // Process
        $process = self::process($post, $campaigns);
      }
    return $process;
  }

  public static function promotion($post)
  {
    // Load Campaigns
    $campaigns = DB::select(
      'campaigns.*',
      array('campaigns.id', 'campaign_id'),
      array('hotels.service_charge_rate', 'service_charge_rate'),
      array('hotels.tax_rate', 'tax_rate'),
      array('hotels.currency_id', 'currency_id'),
      array('rooms.id', 'room_id'),
      array('rooms.extrabed_price', 'extrabed'),
      array('rooms.adult_breakfast_price', 'adult_breakfast_price'),
      array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_adults'),
      array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_extrabeds')
      )
      ->from('campaigns')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
      ->where('rooms.id', 'in', $post['room_ids'])
      ->where('campaigns.is_show', '=', 1)
      ->where('campaigns.id', '=', $post['campaign_id'])
      ->where('rooms.hoterip_campaign', '=', 0)

      /* Kawamura's Confirmation
       * "but the example is what I am saying."
       * from:   kawamura@his-bali.com
       * date:   Mon, Mar 17, 2014 at 12:46 PM
       * subject:   Re: Vacation test case 05032014
       */
      // Filter stay date
      // ->where_open()
      // ->and_where('campaigns.stay_start_date', '<=', $post['stay_start_date'])
      // ->and_where('campaigns.stay_end_date', '>=', $post['stay_end_date'])
      // ->where_close()

      ->group_by('campaign_id')
      ->group_by('room_id')
      ->execute()
      ->as_array('campaign_id');

    foreach ($campaigns as $key => &$campaign)
    {
      $hotel_query = array(
        'language_id' => Controller_Api_Request::default_language(),
        'HotelCode' =>$post['hotel_id'],
        'check_in' => date("Y-m-d"),
        'check_out' => $post['stay_end_date'],
        'InvTypeCode' => $campaign['room_id'],
        'RatePlanCode' => $post['campaign_id'],
        'now' => date("Y-m-d")
      );

      // Attempt to get items of matching campaign
      $items = ORM::factory('campaign')->get_items($post['campaign_id'], $hotel_query);

      // Retry to get items of matching campaign by ignoring stay date
      if (!count($items))
      {
        $items = ORM::factory('campaign')->get_items($post['campaign_id'], $hotel_query, TRUE);
      }

      //Log::instance()->add(Log::INFO, 'terjadi kejanggalan harga item - '.print_r($items, TRUE));

      // Remove all default price items
      foreach ($items as $date => $item) {
        if($item['blackout']
          || $item['weekout']
          )
        {
          unset($items[$date]);
        }
      }
      // Date ranges
      $i = 0;
      $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['stay_start_date'], $post['stay_end_date']);

      foreach($dateranges as $daterange)
      {
        $date = $daterange->format("Y-m-d");

        if(!empty($items[$date]))
        {
          $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
          $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

          if(empty($items[$min_one_day]) 
            || !array_key_exists($min_one_day, $items)
            || $items[$min_one_day]['price'] != $items[$date]['price']
            )
          {
            $campaign['items'][$i]['start'] = $date;

            $campaign['items'][$i]['room_id'] = $campaign['room_id'];
            $campaign['items'][$i]['campaign_id'] = $campaign['campaign_id'];

            // Determine Price in this items conditions
            if ($item['campaign_blackout'])
            {
              $campaign['items'][$i]['price'] = $items[$date]['price'];
            }
            else
            {
              // If promotion
              if ($item['promotion'])
              {
                // If free night
                if ($item['free_night'])
                {
                  $campaign['items'][$i]['price'] = 0;
                }
                // If campaign prices
                  elseif ($items[$date]['is_campaign_prices'])
                  {
                    $campaign['items'][$i]['price'] = $items[$date]['price'];
                  }
                else
                {
                  $campaign['items'][$i]['price'] = ($items[$date]['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                }
              }
              else
              {
                $campaign['items'][$i]['price'] = $items[$date]['price'];
              }
            }
          }
          if(empty($items[$add_one_day]) 
            || !array_key_exists($add_one_day, $items)
            || $items[$add_one_day]['price'] != $items[$date]['price']
            )
          {
            // Set new End date
            $end_date = date( 'Y-m-d', strtotime( $date . ' +1 day' ) );

            $campaign['items'][$i]['end'] = $end_date;
            $i++;
          }
        }
      }
    }

    // Process
    $process = self::process($post, $campaigns);

    return $process;
  }

  public static function restrictions($post)
  {
    // Get Available Campaign
    $campaigns = DB::select(
        'campaigns.*',
        array('campaigns.id', 'campaign_id'),
        array('hotels.service_charge_rate', 'service_charge_rate'),
        array('hotels.tax_rate', 'tax_rate'),
        array('hotels.currency_id', 'currency_id'),
        array('rooms.id', 'room_id'),
        array('rooms.extrabed_price', 'extrabed'),
        array('rooms.adult_breakfast_price', 'adult_breakfast_price'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_adults'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_extrabeds')
      )
      ->from('campaigns')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('room_texts')->on('room_texts.room_id', '=', 'campaigns_rooms.room_id')

      ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
      ->where('hotels.id', '=', $post['hotel_id'])
      ->where('rooms.hoterip_campaign', '=', 0)

      // Filter amount discount
      ->where_open()
      ->where('campaigns.discount_amount', '>', 0)
      ->where('campaigns.is_show', '=', 1)
      ->or_where('campaigns.discount_rate', '>', 0)
      ->or_where('campaigns.is_default', '=', 1)
      ->where_close()

      // Filter stay date
      ->where_open()
      ->and_where('campaigns.stay_start_date', '<=', $post['start_date'])
      ->and_where('campaigns.stay_end_date', '>=', $post['end_date'])
      ->where_close()

      ->group_by('campaign_id')
      ->group_by('room_id')
      ->execute()
      ->as_array('campaign_id');

      if(count($campaigns))
      {
        foreach ($campaigns as $key => &$campaign)
        {
          $hotel_query = array(
            'language_id' => Controller_Api_Request::default_language(),
            'HotelCode' =>$post['hotel_id'],
            'check_in' => $post['start_date'],
            'check_out' => $post['end_date'],
            'InvTypeCode' => $campaign['room_id'],
            'RatePlanCode' => $campaign['campaign_id'],
            'now' => date("Y-m-d")
          );

          $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);

          // Remove all default price items
          foreach ($items as $date => $item)
          {
            if($item['blackout']
              || $item['weekout']
              )
            {
              unset($items[$date]);
            }
          }

          // Date ranges
          $i = 0;
          $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['start_date'], $post['end_date']);

          foreach($dateranges as $daterange)
          {
            $date = $daterange->format("Y-m-d");
            reset($items);
            $first_array = key($items);

            if(!empty($items[$date]))
            {
              $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
              $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

              if($first_array == $date
                || empty($items[$min_one_day]) 
                || !array_key_exists($min_one_day, $items)
                || $items[$min_one_day]['price'] != $items[$date]['price']
                )
              {
                $campaign['items'][$i]['start'] = $date;

                $campaign['items'][$i]['room_id'] = $campaign['room_id'];
                $campaign['items'][$i]['campaign_id'] = $campaign['campaign_id'];

                // Determine Price in this items conditions
                if ($item['campaign_blackout'])
                {
                  $campaign['items'][$i]['price'] = $items[$date]['price'];
                }
                else
                {
                  // If promotion
                  if ($item['promotion'])
                  {
                    // If free night
                    if ($item['free_night'])
                    {
                      $campaign['items'][$i]['price'] = 0;
                    }
                    elseif ($items[$date]['is_campaign_prices'])
                    {
                      $campaign['items'][$i]['price'] = $items[$date]['price'];
                    }
                    else
                    {
                      $campaign['items'][$i]['price'] = ($items[$date]['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                    }
                  }
                  else
                  {
                    $campaign['items'][$i]['price'] = $items[$date]['price'];
                  }
                }
              }

              if(empty($items[$add_one_day]) 
                || !array_key_exists($add_one_day, $items)
                || $items[$add_one_day]['price'] != $items[$date]['price']
                )
              {
                // Set new End date
                $end_date = date( 'Y-m-d', strtotime( $date . ' +1 day' ) );

                $campaign['items'][$i]['end'] = $end_date;
                $i++;
              }
            }
          }
        }
      }

    // Process
    $process = self::process($post, $campaigns);

    return $process;
  }

  public static function surcharges($post)
  {
    // Get Available Campaign
    $campaigns = DB::select(
        'campaigns.*',
        array('campaigns.id', 'campaign_id'),
        array('hotels.service_charge_rate', 'service_charge_rate'),
        array('hotels.tax_rate', 'tax_rate'),
        array('hotels.currency_id', 'currency_id'),
        array('rooms.id', 'room_id'),
        array('rooms.extrabed_price', 'extrabed'),
        array('rooms.adult_breakfast_price', 'adult_breakfast_price'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_adults'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_child'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_extrabeds')
      )
      ->from('campaigns')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('room_texts')->on('room_texts.room_id', '=', 'campaigns_rooms.room_id')

      ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
      ->where('hotels.id', '=', $post['hotel_id'])
      ->where('rooms.hoterip_campaign', '=', 0)
      ->where('campaigns.is_show', '=', 1)

      // Filter stay date
      ->where_open()
      ->and_where('campaigns.stay_start_date', '<=', $post['start_date'])
      ->and_where('campaigns.stay_end_date', '>=', $post['end_date'])
      ->where_close()

      ->group_by('campaign_id')
      ->group_by('room_id')
      ->execute()
      ->as_array('campaign_id');

      if(count($campaigns))
      {
        foreach ($campaigns as $key => &$campaign)
        {
          $hotel_query = array(
            'language_id' => Controller_Api_Request::default_language(),
            'HotelCode' =>$post['hotel_id'],
            'check_in' => $post['start_date'],
            'check_out' => $post['end_date'],
            'InvTypeCode' => $campaign['room_id'],
            'RatePlanCode' => $campaign['campaign_id'],
            'now' => date("Y-m-d")
          );

          // Attempt to get items of matching campaign
          $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query, FALSE, TRUE);

          // Retry to get items of matching campaign by ignoring stay date
          if (!count($items))
          {
            $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query, TRUE, TRUE);
          }

          // Remove all default price items
          foreach ($items as $date => $item)
          {
            if($item['blackout']
              || $item['weekout']
              )
            {
              unset($items[$date]);
            }
          }

          // Date ranges
          $i = 0;
          $surcharges = array_merge_recursive($post['adult_prices'], $post['child_prices']);

          foreach($surcharges as $date => $post_single)
          {
            if(empty($post_single[0]))
            {
              continue;
            }

            if(!empty($post_single))
            {
              $campaign['items'][$i]['start'] = $date;

              // Set new End date
              $end_date = date( 'Y-m-d', strtotime( $date . ' +1 day' ) );
              $campaign['items'][$i]['end'] = $end_date;

              $campaign['items'][$i]['room_id'] = $campaign['room_id'];
              $campaign['items'][$i]['campaign_id'] = $campaign['campaign_id'];

              // Determine Price in this items conditions
              if ($item['campaign_blackout'])
              {
                $campaign['items'][$i]['price'] = $items[$date]['price'] ? $items[$date]['price'] : 0;
              }
              else
              {
                // If promotion
                if ($item['promotion'])
                {
                  // If free night
                  if ($item['free_night'])
                  {
                    $campaign['items'][$i]['price'] = 0;
                  }
                  elseif ($items[$date]['is_campaign_prices'])
                  {
                    $campaign['items'][$i]['price'] = $items[$date]['price'];
                  }
                  else
                  {
                    $campaign['items'][$i]['price'] = ($items[$date]['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                  }
                }
                else
                {
                  $campaign['items'][$i]['price'] = $items[$date]['price'] ? $items[$date]['price'] : 0;
                }
              }
              $i++;
            }
          }
        }
      }

    // Process
    $process = self::process($post, $campaigns);

    return $process;
  }
  
  public static function rooms($post)
  {
    set_time_limit(0);
    // Get Available Campaign
    $campaigns = DB::select(
        'campaigns.*',
        array('campaigns.id', 'campaign_id'),
        array('campaigns.stay_end_date', 'stay_end_date'),
        array('hotels.service_charge_rate', 'service_charge_rate'),
        array('hotels.tax_rate', 'tax_rate'),
        array('hotels.currency_id', 'currency_id'),
        array('rooms.id', 'room_id'),
        array('rooms.extrabed_price', 'extrabed'),
        array('rooms.is_breakfast_included', 'is_breakfast_included'),
        array('rooms.adult_breakfast_price', 'adult_breakfast_price'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_adults'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_child'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_extrabeds')
      )
      ->from('campaigns')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('room_texts')->on('room_texts.room_id', '=', 'campaigns_rooms.room_id')

      ->where('room_texts.language_id', '=', Controller_Api_Request::default_language())
      ->where('rooms.id', '=', $post['room_id'])
      ->where('campaigns.is_show', '=', 1)
      ->where('rooms.hoterip_campaign', '=', 0)

      // Filter amount discount
      ->where_open()
      ->where('campaigns.discount_amount', '>', 0)
      ->or_where('campaigns.discount_rate', '>', 0)
      ->or_where('campaigns.is_default', '=', 1)
      ->where_close()

      // Filter stay date
      ->where('campaigns.stay_start_date', '<=', $post['start_date'])

      ->group_by('campaign_id')
      ->group_by('room_id')
      ->execute()
      ->as_array('campaign_id');

      if(count($campaigns))
      {
        foreach ($campaigns as $key => &$campaign)
        {
          $post['end_date'] = $campaign['stay_end_date'];

          $hotel_query = array(
            'language_id' => Controller_Api_Request::default_language(),
            'HotelCode' =>$post['hotel_id'],
            'check_in' => date("Y-m-d"),
            'check_out' => $campaign['stay_end_date'],
            'InvTypeCode' => $campaign['room_id'],
            'RatePlanCode' => $campaign['campaign_id'],
            'now' => date("Y-m-d")
          );

          $items = ORM::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);

          // Remove all default price items
          foreach ($items as $date => $item)
          {
            if($item['blackout']
              || $item['weekout']
              )
            {
              unset($items[$date]);
            }
          }

          // Date ranges
          $i = 0;
          $dateranges = Controller_Vacation_Notifications_Static::date_ranges($post['start_date'], $post['end_date']);

          foreach($dateranges as $daterange)
          {
            $date = $daterange->format("Y-m-d");

            reset($items);
            $first_array = key($items);

            if(!empty($items[$date]))
            {
              $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');
              $min_one_day = Date::formatted_time("$date - 1 days", 'Y-m-d');

              if($first_array == $date
                || empty($items[$min_one_day]) 
                || !array_key_exists($min_one_day, $items)
                || $items[$min_one_day]['price'] != $items[$date]['price']
                )
              {
                $campaign['items'][$i]['start'] = $date;

                $campaign['items'][$i]['room_id'] = $campaign['room_id'];
                $campaign['items'][$i]['campaign_id'] = $campaign['campaign_id'];

                // Determine Price in this items conditions
                if ($item['campaign_blackout'])
                {
                  $campaign['items'][$i]['price'] = $items[$date]['price'];
                }
                else
                {
                  // If promotion
                  if ($item['promotion'])
                  {
                    // If free night
                    if ($item['free_night'])
                    {
                      $campaign['items'][$i]['price'] = 0;
                    }
                    elseif ($items[$date]['is_campaign_prices'])
                    {
                      $campaign['items'][$i]['price'] = $items[$date]['price'];
                    }
                    else
                    {
                      $campaign['items'][$i]['price'] = ($items[$date]['price'] * ((100 - $item['discount_rate']) / 100) - $item['discount_amount']);
                    }
                  }
                  else
                  {
                    $campaign['items'][$i]['price'] = $items[$date]['price'];
                  }
                }
              }
              if(empty($items[$add_one_day]) 
                || !array_key_exists($add_one_day, $items)
                || $items[$add_one_day]['price'] != $items[$date]['price']
                )
              {
                // Set new End date
                $end_date = date( 'Y-m-d', strtotime( $date . ' +1 day' ) );
                
                $campaign['items'][$i]['end'] = $end_date;
                $i++;
              }
            }
          }
        }
      }

    // Process
    $process = self::process($post, $campaigns);

    return $process;
  }

  public static function process($post, $campaigns)
  {
    // Load Config
    if (Kohana::$environment == Kohana::PRODUCTION)
    {
      $config = Kohana::$config->load('api')->vacation;
    }
    else
    {
      $config = Kohana::$config->load('api')->vacation_test;
    }

    // Factory exchange
    $exchange = Model::factory('exchange');
    $currency_code = Model::factory('currency');

    // Get request ID
    $request_id = Controller_Vacation_Notifications_Static::update_api_access();

    $request_id = date('YmdHms').str_pad($request_id, 9, '0', STR_PAD_LEFT);
    /*
     * XML build
     */
    $transaction = 'OTA_HotelRateAmountNotifRQ';
    $transactionRQ = str_replace('OTA_', '', $transaction);

    try
    {
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
      $envelope->setAttribute('xmlns:soap-env', $config['soap_env']);

      $head = $envelope->appendChild($doc->createElement('soap-env:Header'));

      $Interface = $head->appendChild($doc->createElement('Interface'));
      $Interface->setAttribute('ChannelIdentifierId', 'HIS_VACATION_XML4H');
      $Interface->setAttribute('Version', $config['Version']);
      $Interface->setAttribute('Interface', $config['Interface']);
      $Interface->setAttribute('xmlns', 'http://api.hotels-vacation.com/Documentation/XML/OTA/4/2011B/');

      $ComponentInfo = $Interface->appendChild($doc->createElement('ComponentInfo'));
      $ComponentInfo->setAttribute('User', $config['User']);
      $ComponentInfo->setAttribute('Pwd', $config['Pwd']);
      $ComponentInfo->setAttribute('ComponentType', $config['ComponentType']);

      $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
      $body->setAttribute('RequestId', $request_id);
      $body->setAttribute('Transaction', $transactionRQ);
      $response_node = $body->appendChild($doc->createElement($transaction));

      $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
      $response_node->setAttribute('TimeStamp', date('c'));
      $response_node->setAttribute('Target', $config['Target']);
      $response_node->setAttribute('Version', '1.000');
      $response_node->setAttribute('PrimaryLangID', 'en');

      $RateAmountMessages = $response_node->appendChild($doc->createElement('RateAmountMessages'));
      $RateAmountMessages->setAttribute('HotelCode', $post['hotel_id']);
      if(count($campaigns))
      {
        foreach ($campaigns as $room_id => $campaign)
        {
          // Campaign data
          $service_tax_rate = (($campaign['service_charge_rate'] + $campaign['tax_rate']) +100) /100;
          $is_breakfast_included = $campaign['is_breakfast_included'] ? 'BRE' : 'NOM';

          

          if(count($campaign['items']))
          {
            $offset_price = 0;
            foreach ($campaign['items'] as $item)
            {
              // Set Currency
              $campaign['currency'] = $currency_code->get_currency_by_id($campaign['currency_id'])->code;

              // Set date
              $date = $item['start'];
              $RateAmountMessage = $RateAmountMessages->appendChild($doc->createElement('RateAmountMessage'));
              $StatusApplicationControl  = $RateAmountMessage->appendChild($doc->createElement('StatusApplicationControl'));
              $StatusApplicationControl->setAttribute('Start', $item['start']);
              $StatusApplicationControl->setAttribute('End', $item['end']);
              $StatusApplicationControl->setAttribute('RatePlanCode', $item['campaign_id']);
              $StatusApplicationControl->setAttribute('InvCode', $item['room_id']);
              $StatusApplicationControl->setAttribute('IsRoom', 'true');

              // Request Change: date:  Thu, Feb 20, 2014 at 9:14 AM | subject:  Vacation HotelRateAmountNotifRQ
              // Since Hoterip campaign's doesn't set meal price.
              // When empty, it will use default meal plan as in OTA_HotelDescriptiveInfoRS
              // if(!empty($campaign['is_breakfast_included']))
              // {
              //   $StatusApplicationControl->setAttribute('InvTypeCode', $is_breakfast_included);
              // }
              
              $StatusApplicationControl->setAttribute('InvCodeApplication', 'InvCode');
              $StatusApplicationControl->setAttribute('RatePlanCodeType', 'RatePlanCode');

              $Rates = $RateAmountMessage->appendChild($doc->createElement('Rates'));

              $Rate = $Rates->appendChild($doc->createElement('Rate'));
              $Rate->setAttribute('RateTimeUnit', 'Day');
              $Rate->setAttribute('NumberOfUnits', 1);
              $Rate->setAttribute('UnitMultiplier', 0);

              // Count Price
              $BaseByGuestAmts = $Rate->appendChild($doc->createElement('BaseByGuestAmts'));

              // Base Price
              $base_price = ($item['price'] / $service_tax_rate);
              $base_price_tax = $item['price'];

              // Count Surcharges
              $surcharges_adult = 0;
              $surcharges_child = 0;

              if(is_array($post[$date]['adult_prices']) AND is_array($post[$date]['child_prices']))
              {
                if(!empty($post[$date]['adult_prices']))
                {
                  $surcharges_adult = $post[$date]['adult_prices'];
                }

                if(!empty($post['child_prices'][$item]))
                {
                  $surcharges_child = $post[$date]['child_prices'];
                }
              }
              else
              {
                if(count($item['surcharge']))
                {
                  $surcharges_adult = $item['surcharge']['adult_price'];
                  $surcharges_child = $item['surcharge']['child_price'];
                }
              }

              // Exchage Price
              $base_exchange_price = $base_price;
              $base_exchange_price_tax = $base_price_tax;

              // Count Base Price
              $occupancies = ORM::factory('room_capacity')->get_room_capacities($item['room_id']);

              $occupy_number_of_adults = array();
              $occupy_number_of_childrens = array();

              if(count($occupancies))
              {
                // $BaseByGuestAmt = $BaseByGuestAmts->appendChild($doc->createElement('BaseByGuestAmt'));
                //   $BaseByGuestAmt->setAttribute('AmountBeforeTax', number_format(round($base_exchange_price,2),'2','',''));
                //   $BaseByGuestAmt->setAttribute('AmountAfterTax', number_format(round($base_exchange_price_tax,2),'2','',''));
                //   $BaseByGuestAmt->setAttribute('CurrencyCode', $campaign['currency']);
                //   $BaseByGuestAmt->setAttribute('DecimalPlaces', 2);
                //   $BaseByGuestAmt->setAttribute('NumberOfGuests', 0);

                foreach ($occupancies as $occupancy_key => $occupancy) 
                {
                    if(empty($occupy_number_of_adults[$occupancy['number_of_adults']]))
                    {

                      $base_exchange_price_calc = $base_exchange_price + ($surcharges_adult * $occupancy['number_of_adults']);
                      $base_exchange_price_tax_calc = $base_exchange_price_tax + ($surcharges_adult * $occupancy['number_of_adults']);

                      if($occupancy['number_of_extrabeds'])
                      {
                        //$base_surcharges_adult = ($occupancy['extrabed_price'] * $surcharges_adult);
                        $extrabed_price = $campaign['extrabed'] ? ($campaign['extrabed'] * $occupancy['number_of_extrabeds']) : 0;
                        //$breakfast_price = $campaign['adult_breakfast_price'] ? ($campaign['adult_breakfast_price'] * $occupancy['number_of_extrabeds'])  * $exchange->to($campaign['currency_id'], 1) : 0;

                        // Extrabed price
                         // $base_exchange_price_calc = $base_exchange_price_calc + $base_surcharges_adult + $extrabed_price + $breakfast_price;
                         // $base_exchange_price_tax_calc = $base_exchange_price_tax_calc + $base_surcharges_adult + $extrabed_price + $breakfast_price;
                        $base_exchange_price_calc = $base_exchange_price_calc + $extrabed_price;
                        $base_exchange_price_tax_calc = $base_exchange_price_tax_calc  + $extrabed_price;
                      }
              //                                                               echo "<pre>";
              // print_r($base_exchange_price_calc);
              // die();
                      $BaseByGuestAmt = $BaseByGuestAmts->appendChild($doc->createElement('BaseByGuestAmt'));
                      $BaseByGuestAmt->setAttribute('AmountBeforeTax', number_format(round(($exchange->to($campaign['currency_id'], 1)*$base_exchange_price_calc),2),'2','',''));
                      $BaseByGuestAmt->setAttribute('AmountAfterTax', number_format(round(($exchange->to($campaign['currency_id'], 1)*$base_exchange_price_tax_calc),2),'2','',''));
                      $BaseByGuestAmt->setAttribute('CurrencyCode', 'USD');
                      $BaseByGuestAmt->setAttribute('DecimalPlaces', 2);
                      $BaseByGuestAmt->setAttribute('NumberOfGuests', $occupancy['number_of_adults']);

                    // rstriction rerpeat same number of adults
                    $occupy_number_of_adults[$occupancy['number_of_adults']] = $occupancy['number_of_adults'];
                  }
                }
              }

                if(!empty($campaign['number_of_extrabeds']))
                {

                  $maxBedforOneChild = 0;

                  foreach ($occupancies as $occupancie) {
                    if ($occupancie['number_of_children'] == 1 && $maxBedforOneChild < $occupancie['number_of_extrabeds'])
                    {
                      // get extrabed num
                      $maxBedforOneChild = $occupancie['number_of_extrabeds'];
                    }
                  }

                  if ($maxBedforOneChild > 0)
                  {
                    $childRate = $maxBedforOneChild * $campaign['extrabed'];
                  }

                  //if items not have extrabed price
                  // if($campaign['items'][$offset_price]['extrabed_item_price']==0)
                  // {
                    // $amount_after_child = ($childRate)+($surcharges_child);

                   // Set child to extrabed price
                    $amount_after_child = ($campaign['extrabed'])+($surcharges_child);
                    
                    $before_tax_child = ($amount_after_child/$service_tax_rate);


                    $BaseByGuestAmt = $BaseByGuestAmts->appendChild($doc->createElement('BaseByGuestAmt'));
                    $BaseByGuestAmt->setAttribute('AmountBeforeTax', number_format(round((($exchange->to($campaign['currency_id'], 1)*$before_tax_child)),2),'2','',''));
                    $BaseByGuestAmt->setAttribute('AmountAfterTax', number_format(round(($exchange->to($campaign['currency_id'], 1)*$amount_after_child),2),'2','',''));
                    $BaseByGuestAmt->setAttribute('CurrencyCode', 'USD');
                    $BaseByGuestAmt->setAttribute('DecimalPlaces', 2);
                    $BaseByGuestAmt->setAttribute('NumberOfGuests', 1);
                    $BaseByGuestAmt->setAttribute('AgeQualifyingCode', 8);
                  // }
                  //if items have extrabed price
                  // else
                  // {
                  //   $before_tax_child = ($campaign['items'][$offset_price]['extrabed_item_price']/$service_tax_rate)*$offset;
                  //   $amount_after_child = ($offset*$campaign['items'][$offset_price]['extrabed_item_price'])+($surcharges_child*$offset);

                  //   $BaseByGuestAmt = $BaseByGuestAmts->appendChild($doc->createElement('BaseByGuestAmt'));
                  //   $BaseByGuestAmt->setAttribute('AmountBeforeTax', number_format(round($before_tax_child,2),'2','',''));
                  //   $BaseByGuestAmt->setAttribute('AmountAfterTax', number_format(round($amount_after_child,2),'2','',''));
                  //   $BaseByGuestAmt->setAttribute('CurrencyCode', $campaign['currency']);
                  //   $BaseByGuestAmt->setAttribute('DecimalPlaces', 2);
                  //   $BaseByGuestAmt->setAttribute('NumberOfGuests', $offset);
                  //   $BaseByGuestAmt->setAttribute('AgeQualifyingCode', 8);
                  // }
                }
              $offset_price++;
              }
            }
          }
        }

      $response = $doc->saveXML();
        
      // Request Process, check hotel is in specific hotel or not
      if (Controller_Vacation_Trigger::checkHotelVacation($post['hotel_id']))
      {
        $result = Controller_Vacation_Notifications_Static::post($response, $request_id);
      }
      else
      {
        $result = FALSE;
      }
    }
    catch (Kohana_Exception $e)
    {
      $result = FALSE;
    }
    
    return $result;
  }
}