<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Vacation_Static extends Controller {

    public static function config() {
        $config = Kohana::config('application');

        return $config;
    }

    public static function default_language() {
        // Set default language id using english
        $default_language_id = ORM::factory('language')
                        ->where('code', '=', 'en-us')
                        ->find()
                ->id;

        return $default_language_id;
    }

    public static function errors($errors, $node_head) {

        // Logout account
        A1::instance()->logout();

        $list_error_type = array(
            '1' => 'Unknown',
            '2' => 'No implementation',
            '3' => 'Biz rule',
            '4' => 'Authentication',
            '5' => 'Authentication timeout',
            '6' => 'Authorization',
            '7' => 'Protocol violation',
            '8' => 'Transaction model',
            '9' => 'Authentication model',
            '10' => 'Required field missing',
            '00' => 'Welcome',
        );

        $list_error_code = array(
            '15' => 'Invalid Date',
            '61' => 'Invalid currency code',
            '69' => 'Minimum stay criteria not fulfilled',
            '70' => 'Maximum stay criteria not fulfilled',
            '87' => 'Booking reference invalid',
            '95' => 'Booking already cancelled',
            '97' => 'Booking reference not found',
            '111' => 'Booking invalid',
            '127' => 'Reservation already exists',
            '146' => 'Service requested incorrect',
            '147' => 'Taxes incorrect',
            '161' => 'Search criteria invalid',
            '245' => 'Invalid confirmation number',
            '249' => 'Invalid rate code',
            '251' => 'Last name and customer number do not match',
            '321' => 'Required field missing',
            '364' => 'Error rate range',
            '394' => 'Invalid item',
            '397' => 'Invalid number of adults',
            '400' => 'Invalid property code',
            '402' => 'Invalid room type',
            '450' => 'Unable to process',
            '459' => 'Invalid request code',
            '00' => 'Welcome',
            '9900' => 'Invalid Username or Password',
        );

        //Generate Error XML
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $RS = $doc->appendChild($doc->createElement(substr($node_head['head'], 0, -2) . 'RS'));
        $RS->setAttribute('xmlns', $node_head['xmlns']);
        $RS->setAttribute('TimeStamp', $node_head['TimeStamp']);
        $RS->setAttribute('Target', $node_head['Target']);
        $RS->setAttribute('Version', $node_head['Version']);

        $errors_node = $RS->appendChild($doc->createElement('Errors'));

        foreach ($errors as $key => $error) {
            $error_descriptions = str_replace("_", " ", $key) . ', ' . $list_error_code[$error['code']] . '.';

            $error_node = $errors_node->appendChild($doc->createElement('Error', $error_descriptions));
            $error_node->setAttribute('Language', 'en-us');
            $error_node->setAttribute('Type', $error['type']);
            $error_node->setAttribute('ShortText', 4000);
            $error_node->setAttribute('Code', $error['code']);
        }

        $response = $doc->saveXML();

        return $response;
    }

    public static function auth($auth) {
        //Check Direct API
        if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
            $ip = getenv("REMOTE_ADDR");
        else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
            $ip = $_SERVER['REMOTE_ADDR'];
        else
            $ip = "unknown";

        // Get admins
        $api_access = DB::select(
                        array('api_access.api_key', 'api_key'), array('api_access.ip', 'ip')
                )
                ->from('admins')
                ->join('roles')->on('roles.value', '=', 'admins.role')
                ->join('api_access')->on('api_access.admin_id', '=', 'admins.id')
                ->where('admins.role', '=', 'api')
                ->where('admins.name', '=', $auth['source'])
                ->where_open()
                ->where('api_access.ip', '=', $ip)
                ->or_where('api_access.api_key', '=', $auth['key'])
                ->where_close()
                ->limit(1)
                ->as_object()
                ->execute()
                ->current();

        if (A1::instance()->login($auth['User'], $auth['Pwd'], (bool) False)) {
            return TRUE;
        }
        if (!empty($api_access)) {
            if ($api_access->api_key == $auth['key']) {
                return TRUE;
            } elseif ($api_access->ip == $ip) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function selected_currency($id) {
        // Set default currency
        $selected_currency = ORM::factory('currency')->get_currency_by_id($id);

        return $selected_currency;
    }

    public static function cancellations($id, $hotel_id, $predified = TRUE, $check_in = FALSE) {
        $cancellation_availables = DB::select(
                        'campaigns_cancellations.*', array('cancellations.level_1_cancellation_rule_id', 'rule_1'), array('cancellations.level_2_cancellation_rule_id', 'rule_2'), array('cancellations.level_3_cancellation_rule_id', 'rule_3'), array('rule_1.name', 'rule_1_name'), array('rule_1.within_days', '1_within_days'), array('rule_1.percent_charged', '1_percent_charged'), array('rule_1.night_charged', '1_night_charged'), array('rule_2.name', 'rule_2_name'), array('rule_2.within_days', '2_within_days'), array('rule_2.percent_charged', '2_percent_charged'), array('rule_2.night_charged', '2_night_charged'), array('rule_3.name', 'rule_3_name'), array('rule_3.within_days', '3_within_days'), array('rule_3.percent_charged', '3_percent_charged'), array('rule_3.night_charged', '3_night_charged')
                )
                ->from('cancellations')
                ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
                ->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'level_1_cancellation_rule_id')
                ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'level_2_cancellation_rule_id')
                ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'level_3_cancellation_rule_id')
                ->where('campaigns_cancellations.campaign_id', '=', $id);

        //vacation
        if ($predified) {
            $cancellation_availables = $cancellation_availables
                    ->where('cancellations.end_date', '>=', date('Y-m-d'));
        } elseif ($check_in) {
            $cancellation_availables = $cancellation_availables
                    ->where('cancellations.end_date', '>=', $check_in)
                    ->where('cancellations.start_date', '<=', $check_in);
        }

        $cancellation_availables = $cancellation_availables
                ->execute()
                ->current();

        if (empty($cancellation_availables['rule_1'])) {
            $cancellation_availables = ORM::factory('cancellation')
                    ->select(
                            'campaigns_cancellations.*', array('cancellations.level_1_cancellation_rule_id', 'rule_1'), array('cancellations.level_2_cancellation_rule_id', 'rule_2'), array('cancellations.level_3_cancellation_rule_id', 'rule_3'), array('rule_1.name', 'rule_1_name'), array('rule_1.within_days', '1_within_days'), array('rule_1.percent_charged', '1_percent_charged'), array('rule_1.night_charged', '1_night_charged'), array('rule_2.name', 'rule_2_name'), array('rule_2.within_days', '2_within_days'), array('rule_2.percent_charged', '2_percent_charged'), array('rule_2.night_charged', '2_night_charged'), array('rule_3.name', 'rule_3_name'), array('rule_3.within_days', '3_within_days'), array('rule_3.percent_charged', '3_percent_charged'), array('rule_3.night_charged', '3_night_charged')
                    )
                    ->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'cancellations.level_1_cancellation_rule_id')
                    ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'cancellations.level_2_cancellation_rule_id')
                    ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'cancellations.level_3_cancellation_rule_id')
                    ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
                    ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
                    ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                    ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                    ->where('rooms.hotel_id', '=', $hotel_id)
                    ->where('campaigns.is_default', '=', 1);

            //Vacation
            if ($predified) {
                $cancellation_availables = $cancellation_availables
                        ->where('cancellations.end_date', '>=', date('Y-m-d'));
            } elseif ($check_in) {
                $cancellation_availables = $cancellation_availables
                        ->where('cancellations.end_date', '>=', $check_in)
                        ->where('cancellations.start_date', '<=', $check_in);
            }

            $cancellation_availables = $cancellation_availables
                    ->order_by('cancellations.start_date', 'ASC')
                    ->group_by('cancellations.id')
                    ->find()
                    ->as_array();
        }

        $cancellations_datas = array();

        if (!empty($cancellation_availables['1_within_days']) || !empty($cancellation_availables['1_night_charged']) || !empty($cancellation_availables['1_percent_charged'])
        ) {
            $cancellations_datas['first']['rule'] = $cancellation_availables['rule_1'];
            $cancellations_datas['first']['within_days'] = $cancellation_availables['1_within_days'];
            $cancellations_datas['first']['night_charged'] = $cancellation_availables['1_night_charged'];
            $cancellations_datas['first']['percent_charged'] = $cancellation_availables['1_percent_charged'];
        }

        if (!empty($cancellation_availables['2_within_days']) || !empty($cancellation_availables['2_night_charged']) || !empty($cancellation_availables['2_percent_charged'])
        ) {
            $cancellations_datas['second']['rule'] = $cancellation_availables['rule_2'];
            $cancellations_datas['second']['within_days'] = $cancellation_availables['2_within_days'];
            $cancellations_datas['second']['night_charged'] = $cancellation_availables['2_night_charged'];
            $cancellations_datas['second']['percent_charged'] = $cancellation_availables['2_percent_charged'];
        }
        if (!empty($cancellation_availables['3_within_days']) || !empty($cancellation_availables['3_night_charged']) || !empty($cancellation_availables['3_percent_charged'])
        ) {
            $cancellations_datas['third']['rule'] = $cancellation_availables['rule_3'];
            $cancellations_datas['third']['within_days'] = $cancellation_availables['3_within_days'];
            $cancellations_datas['third']['night_charged'] = $cancellation_availables['3_night_charged'];
            $cancellations_datas['third']['percent_charged'] = $cancellation_availables['3_percent_charged'];
        }


        return $cancellations_datas;
    }

    public static function promotion_descriptions($hotel_query, $room, $promotion_exist) {

        //exchange amount to selected currency
        $exchange = Model::factory('exchange')->to($hotel_query['hotel_currency'], $hotel_query['currency_code']);
        //Determine Promo
        $stay_adds = '';

        if ($promotion_exist AND $room['campaign_default'] == 0) {
            if ($room['last_days'] > 0) {
                $room['last_days_date'] = Date::formatted_time("+{$room['last_days']} days", __('M j, Y'), $room['hotel_timezone']);
                $last_days_date = $room['last_days_date'];
                $last_days = Date::formatted_time("$last_days_date", 'm-d-Y', $room['hotel_timezone']);

                if ($hotel_query['check_in'] <= $last_days AND $hotel_query['check_out'] <= $last_days) {
                    $Onrequest = 'FALSE';
                } elseif ($hotel_query['check_in'] < $last_days AND $hotel_query['check_out'] >= $last_days) {
                    $Onrequest = 'FALSE';
                } else {
                    $Onrequest = 'TRUE';
                }

                $stay_adds .= 'Guest must book within ' . $room['last_days'] . ' days of arrival';
            } elseif ($room['early_days'] > 0) {
                $early_days = $room['early_days'] - 1;
                $room['early_days_date'] = Date::formatted_time("+$early_days days", __('M j, Y'), $room['hotel_timezone']);

                $early_days_date = $room['early_days_date'];
                $early_days = Date::formatted_time("$early_days_date", 'm-d-Y', $room['hotel_timezone']);

                if ($hotel_query['check_in'] <= $early_days AND $hotel_query['check_out'] <= $early_days) {
                    $Onrequest = 'TRUE';
                } elseif ($hotel_query['check_in'] < $early_days AND $hotel_query['check_out'] >= $early_days) {
                    $Onrequest = 'FALSE';
                } elseif ($hotel_query['check_in'] > $early_days AND $hotel_query['check_out'] >= $early_days) {
                    $Onrequest = 'FALSE';
                }

                // $stay_adds .= 'Book after '.Date::formatted_time("$early_days_date", __('M j, Y'), $room['hotel_timezone']).', ';
                $stay_adds .= 'Book ' . $room['early_days'] . ' days in advance';
                if ($room['free_night'] > 0) {
                    $string = $room['free_night'] > 1 ? 'nights' : 'night';
                    $stay_adds .= '. Get free ' . $room['free_night'] . ' ' . $string;
                }
            } elseif ($room['rate'] > 0) {
                $stay_adds .= 'Discount ' . $room['rate'] . ' %';
            } elseif ($room['amount'] > 0) {
                $stay_adds .= 'Discount ' . round($room['amount'] * $exchange) . ' ' . self::selected_currency($hotel_query['currency_code'])->code;
            } elseif ($room['free_night'] > 0) {
                $string = $room['free_night'] > 1 ? 'nights' : 'night';
                $stay_adds .= 'Get free ' . $room['free_night'] . ' ' . $string;
            }

            $stay = 'Stay at least ' . $room['min_nights'] . ' nights and book ' . $room['min_rooms'] . ' or more rooms. ' . $stay_adds;
        }

        if ($room['campaign_default']) {
            $stay = 'Best Available Rate';
        }

        $promotion_exist = $promotion_exist ? $stay . '.' : '';

        $promotion_exist = htmlspecialchars(utf8_encode($promotion_exist), ENT_QUOTES);

        return $promotion_exist;
    }

    public static function campaign_items($hotel_query, $campaigns) {
        /**
         *  Campains
         */
        // Create between the days of chack-in and chack-out in campaigns
        $start_offset = -Date::formatted_time($hotel_query['check_in'], 'w');
        $start_date = Date::formatted_time($hotel_query['check_in'] . " +$start_offset days", 'Y-m-d 00:00:00');

        $end_offset = 6 - Date::formatted_time("{$hotel_query['check_out']}", 'w');
        $end_date = Date::formatted_time($hotel_query['check_out'] . " +$end_offset days", 'Y-m-d 24:00:00');

        $days = Date::span(Helper_Date::create_date($start_date)->getTimestamp(), Helper_Date::create_date($end_date)->getTimestamp(), 'days');

        // Create items
        for ($i = 0; $i < $days; $i++) {
            $week_item['weeks'][(int) ($i / 7)]['days'][] = array(
                'date' => Date::formatted_time($start_date . " +$i days", 'Y-m-d'),
                'day' => Date::formatted_time($start_date . " +$i days", 'j'),
                'price' => NULL,
                'show_flag' => FALSE,
                'free_flag' => FALSE,
                'price_flag' => FALSE,
            );

            if ($i % 7 === 0) {
                $week_item['weeks'][(int) ($i / 7)]['first'] = Date::formatted_time($start_date . " +$i days", 'M-d');
            }

            if ($i % 7 === 6) {
                $week_item['weeks'][(int) ($i / 7)]['last'] = Date::formatted_time($start_date . " +$i days", 'M-d');
            }
        }

        // Get guest number of night
        $stay_night = Date::span(strtotime($hotel_query['check_in']), strtotime($hotel_query['check_out']), 'days');

        // Altered campaigns
        foreach ($campaigns as &$campaign) {
            //$campaign['room_facilities'] = Model::factory('room_facility')->get_facility_by_room_id($campaign['room_id']);
            //$campaign['room_photo'] = Model::factory('hotel_photo')->get_photo_by_id($this->selected_language->id,$campaign['room_photo_id']);

            $campaign['room_capacity'] = Model::factory('room_capacity')->check_room_capacity($campaign['room_id'], $hotel_query['capacities']);

            $campaign['room_capacities'] = Model::factory('room_capacity')->get_room_capacities($campaign['room_id']);

            $campaign['other_benefits'] = DB::select(
                            'campaigns_benefits.*', array('benefit_texts.name', 'name')
                    )
                    ->from('campaigns_benefits')
                    ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                    ->where('benefit_texts.language_id', '=', $hotel_query['language_id'])
                    ->where('campaigns_benefits.campaign_id', '=', $campaign['campaign_id'])
                    ->execute();

            $campaign['nights'] = Date::span(Helper_Date::create_date($hotel_query['check_in'])->getTimestamp(), Helper_Date::create_date($hotel_query['check_out'])->getTimestamp(), 'days');
            $campaign['night_count'] = 0;
            $campaign['total_price'] = 0;
            $campaign['average_price'] = NULL;

            $campaign['items'] = Model::factory('campaign')->get_items($campaign['campaign_id'], $hotel_query);
            $campaign['week_items'] = $week_item;

            // Get campaign min stock
            $min_stock = min(Arr::pluck($campaign['items'], 'stock'));

            $remaining_stock = Kohana::$config->load('hoterip')->get('remaining_stock');

            // If min stock less then remaining stock
            if ($min_stock <= $remaining_stock) {
                $campaign['remaining_flag'] = $min_stock;
            }

            // Determine if guest stay night more than minimum night
            $campaign['minimum_night_available'] = $campaign['max_minimum_night'];
        }

        foreach ($campaigns as &$campaign) {
            foreach ($campaign['week_items']['weeks'] as &$week) {
                foreach ($week['days'] as &$day) {
                    if (isset($campaign['items'][$day['date']])) {
                        // Set item
                        $week_item = $campaign['items'][$day['date']];

                        // Show flag true
                        $day['show_flag'] = TRUE;
                        $day['remaining_flag'] = ($week_item['stock'] <= 5) ? $week_item['stock'] : FALSE;

                        // If room capacity is true
                        if ($campaign['room_capacity']) {
                            // If not blackout or not weekout
                            if (!($week_item['blackout'] OR $week_item['weekout'])) {
                                //If there is stock
                                if ($week_item['stock'] >= $hotel_query['number_of_rooms']) {
                                    // Out flag ture
                                    $day['price_flag'] = TRUE;
                                    $campaign['night_count'] ++;

                                    // If campaingn blackout
                                    if ($week_item['campaign_blackout']) {
                                        $day['price'] = $week_item['price'];
                                    } else {
                                        // If promotion
                                        if ($week_item['promotion']) {
                                            // If free night
                                            if ($week_item['free_night']) {
                                                $day['price'] = 0;
                                                $day['free_flag'] = TRUE;
                                            } else {
                                                $day['price'] = ($week_item['price'] * ((100 - $week_item['discount_rate']) / 100) - $week_item['discount_amount']);
                                            }
                                        } else {
                                            $day['price'] = $week_item['price'];
                                        }
                                    }
                                }
                            }
                        }
                        // ----------------------------------------------------------------------------

                        $day['check_data'] = array(
                            'item_id' => $week_item['item_id'],
                            'room_id' => $week_item['room_id'],
                            'date' => $week_item['date'],
                            'stock' => $week_item['stock'],
                            'blackout' => $week_item['blackout'],
                            'weekout' => $week_item['weekout'],
                            'campaign_blackout' => $week_item['campaign_blackout'],
                            'promotion' => $week_item['promotion'],
                            'discount_rate' => $week_item['discount_rate'],
                            'discount_amount' => $week_item['discount_amount'],
                            'free_night' => $week_item['free_night'],
                        );

                        // ----------------------------------------------------------------------------
                    }
                }
            }
        }

        // unset $campaing
        unset($campaign);

        // Set average price, total price
        foreach ($campaigns as &$campaign) {
            foreach ($campaign['week_items']['weeks'] as &$week) {
                foreach ($week['days'] as &$day) {
                    $campaign['total_price'] += $day['price'];
                }
            }

            if ($campaign['night_count'] == $campaign['nights']) {
                $campaign['average_price'] = $campaign['total_price'] / $campaign['nights'];
            }

            // TODO campaignに入れなくてもOKかな。
            // Get no arrival
            $campaign['hotel_no_arrival'] = DB::select()
                    ->from('hotel_no_arrivals')
                    ->where('hotel_id', '=', $campaign['hotel_id'])
                    ->where('date', '=', $hotel_query['check_in'])
                    ->execute()
                    ->get('date');

            // TODO campaignに入れなくてもOKかな。
            // Get no departure
            $campaign['hotel_no_departure'] = DB::select()
                    ->from('hotel_no_departures')
                    ->where('hotel_id', '=', $campaign['hotel_id'])
                    ->where('date', '=', $hotel_query['check_out'])
                    ->execute()
                    ->get('date');
        }

        // Rekey
        $campaigns = array_values($campaigns);

        return $campaigns;
    }

}
