<?php defined('SYSPATH') or die('No direct script access.');

class Controller_City extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('city', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			if (A2::instance()->allowed('city', 'delete'))
			{
				try
				{
					// Get cities id
					$city_ids = $this->request->post('ids');
			
					// Delete cities
					DB::delete('cities')
						->where('id', 'IN', $city_ids)
						->execute();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			}
		}
		
		$cities = ORM::factory('city')
			->select(
				array('city_texts.name', 'name'),
				array('country_texts.name', 'country_name'),
				array(DB::expr('COUNT(hotels.id)'), 'total_hotels')
			)
			->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
			->join('countries')->on('countries.id', '=', 'cities.country_id')
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
			->join('hotels', 'left')->on('hotels.city_id', '=', 'cities.id')
			->where('city_texts.language_id', '=', $this->selected_language->id)
			->where('country_texts.language_id', '=', $this->selected_language->id)
			->group_by('cities.id')
			->order_by('city_texts.name', 'ASC')
			->find_all();
		
		$this->template->main = Kostache::factory('city/manage')
			->set('notice', Notice::render())
			->set('cities', $cities);
	}
	
	public function action_add()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('city', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage cities
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'city', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Create city
				$city = ORM::factory('city')
					->create_city($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'city', 'action' => 'add')), __('Add another city'))));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'city', 'action' => 'edit', 'id' => $city->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('city'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$languages = ORM::factory('language')
			->find_all();
		
		$countries = ORM::factory('country')
			->select(
				array('country_texts.name', 'name')
			)
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('country_texts.language_id', '=', $this->selected_language->id)
			->order_by('country_texts.name')
			->find_all();
		
		$this->template->main = Kostache::factory('city/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('languages', $languages)
			->set('countries', $countries);
	}
	
	public function action_edit()
	{
		// Get city id
		$city_id = (int) $this->request->param('id');
		
		// Get city
		$city = ORM::factory('city')
			->where('id', '=', $city_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed('city', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage cities
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'city', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Update city
				ORM::factory('city')
					->where('id', '=', $city_id)
					->find()
					->update_city($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('city'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$city = ORM::factory('city')
			->where('id', '=', $city_id)
			->find();
		
		// Get city texts
		$city_texts = ORM::factory('city_text')
			->where('city_id', '=', $city->id)
			->find_all()
			->as_array('language_id');
		
		$languages = ORM::factory('language')
			->find_all();
		
		$countries = ORM::factory('country')
			->select(
				array('country_texts.name', 'name')
			)
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('country_texts.language_id', '=', $this->selected_language->id)
			->order_by('country_texts.name')
			->find_all();
		
		$this->template->main = Kostache::factory('city/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('city', $city)
			->set('city_texts', $city_texts)
			->set('languages', $languages)
			->set('countries', $countries);
	}

	public function action_delete()
	{
		// Get city id
		$city_id = $this->request->param('id');
		
		if (A2::instance()->allowed('city', 'delete'))
		{
			try
			{
				// Delete city
				DB::delete('cities')
					->where('id', '=', $city_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}