<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Email extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_send() 
	{
		set_time_limit(Date::MINUTE * 10);
		
				// If ajax request
    if ($this->request->is_ajax())
    {
	    // Load email data
	    $emails = DB::select(
	      'email_queues.*',
	      array('email_templates.subject', 'subject'),
	      array('email_templates.template', 'template')
	      )
	      ->from('email_queues')
	      ->join('email_templates')->on('email_queues.template_id', '=', 'email_templates.id')
	      ->where('email_queues.status', '=', 0)
	      ->limit(50)
	      ->order_by('id', 'ASC')
	      ->execute();

	    $emails_data = array();

			echo '<table class="list-table" id="email-list">
				<thead>
					<tr>
						<th>Email</th>
						<th>Subject</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>';

			if(count($emails) > 0)
			{
				foreach ($emails as $key => $email) 
				{
					$subject = strtr($email['subject'], array(
          '{first_name}' => $email['first_name'],
          '{last_name}' => $email['last_name'],
          '{point}' => $email['point'],
          ));

					$status = $email['status'] == 1 ? 'Sent' : 'Pending';
					echo	'<tr><td>'.$email['to'].'</td><td>'.$subject.'</td><td>'.$status.'</td></tr>';
				}
			}
			exit();
	   }
    $user = ORM::Factory('user')
      ->where('id', '=', 1)
      ->find();
    
    // Is Authorized ?
		if ( ! A2::instance()->allowed('email', 'send'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}

		if ($this->request->post('submit'))
    {
      switch ($this->request->post('to'))
      {
        case 0:
        {
          $language_id = $this->request->post('language_id');
          
          // Load user
          $users = ORM::factory('user') 
            ->where('is_subscribe_newsletter', '=', 1);
          
          if ($language_id)
          {
            $users->where('language_id', '=', $language_id);
          }

          $users = $users->find_all();

          // Load Subscriber
					$subscribers = ORM::factory('subscriber') 
            ->where('is_subscribe_newsletter', '=', 1);
          
          if ($language_id)
          {
            $subscribers->where('language_id', '=', $language_id);
          }

          $subscribers = $subscribers->find_all();

					// Start transaction
					Database::instance()->begin();
					
					try
					{
						// Insert into email templates
						$template_id = DB::insert('email_templates')
							->columns(array('subject', 'template'))
							->values(array($this->request->post('subject'), $this->request->post('message')))
							->execute();

						if(count($users))
						{
							foreach ($users as $user)
							{
								// Insert into email queue
								DB::insert('email_queues')
									->columns(array('to', 'first_name','last_name','point', 'template_id'))
									->values(array($user->email, $user->first_name, $user->last_name, $user->point, $template_id[0]))
									->execute();
							}
						}

						if(count($subscribers))
						{
							foreach ($subscribers as $user)
							{
								// Insert into email queue
								DB::insert('email_queues')
									->columns(array('to', 'first_name', 'template_id'))
									->values(array($user->email, $user->email, $template_id[0]))
									->execute();
							}
						}

						// Commit transaction
						Database::instance()->commit();
						// Add success notice
						Notice::add(Notice::SUCCESS, 'Successfully queued emails and 50 emails will be sent every minute.');
					}
					catch (Exception $e)
					{
						// Rollback transaction
						Database::instance()->rollback();
						// Add error notice
						Notice::add(Notice::ERROR, $e->getMessage());
					}
          
          break;
        }
        case 1:
        {
          $language_id = $this->request->post('language_id');
          
          $users = ORM::factory('user');
          
          if ($language_id)
          {
            $users->where('language_id', '=', $language_id);
          }

          $users = $users->find_all();
          
					// Start transaction
					Database::instance()->begin();
					
					try
					{
						// Insert into email templates
						$template_id = DB::insert('email_templates')
							->columns(array('subject', 'template'))
							->values(array($this->request->post('subject'), $this->request->post('message')))
							->execute();

						foreach ($users as $user)
						{
							// Insert into email queue
							DB::insert('email_queues')
								->columns(array('to', 'first_name','last_name','point', 'template_id'))
								->values(array($user->email, $user->first_name, $user->last_name, $user->point, $template_id[0]))
								->execute();
						}
          
						// Commit transaction
						Database::instance()->commit();
						// Add success notice
						Notice::add(Notice::SUCCESS, 'Successfully queued emails and 50 emails will be sent every minute.');
					}
					catch (Exception $e)
					{
						// Rollback transaction
						Database::instance()->rollback();
						// Add error notice
						Notice::add(Notice::ERROR, $e->getMessage());
					}
          
          break;
        }
        case 2:
        {
          $admins = ORM::factory('admin')
            ->find_all();
          
					// Start transaction
					Database::instance()->begin();
					
					try
					{
						// Insert into email templates
						$template_id = DB::insert('email_templates')
							->columns(array('subject', 'template'))
							->values(array($this->request->post('subject'), $this->request->post('message')))
							->execute();

						foreach ($admins as $admin)
						{
							// Insert into email queue
							DB::insert('email_queues')
								->columns(array('to', 'first_name', 'template_id'))
								->values(array($admin->email, $admin->username, $template_id[0]))
								->execute();
						}

						// Commit transaction
						Database::instance()->commit();
						// Add success notice
						Notice::add(Notice::SUCCESS, 'Successfully queued emails and 50 emails will be sent every minute.');
					}
					catch (Exception $e)
					{
						// Rollback transaction
						Database::instance()->rollback();
						// Add error notice
						Notice::add(Notice::ERROR, $e->getMessage());
					}
          
          break;
        }
        case 3: 
        {
          $email = $this->request->post('email');

          // Start transaction
					Database::instance()->begin();
					
					try
					{
						$user = ORM::factory('user')
	            ->where('email', '=', $email)
	            ->find();
          
	          if ($user->loaded())
	          {
	            $subject = strtr($this->request->post('subject'), array(
	              '{first_name}' => ucfirst($user->first_name),
	              '{last_name}' => ucfirst($user->last_name),
	              '{point}' => $user->point,
	              ));
	            
	            $message = strtr($this->request->post('message'), array(
	              '{first_name}' => ucfirst($user->first_name),
	              '{last_name}' => ucfirst($user->last_name),
	              '{point}' => $user->point,
	              ));
	            
	            Email::factory($subject, $message, 'text/html')
	              ->from('support@hoterip.com', 'Hoterip')
	              ->to($email)
	              ->send();          
	            
	            Notice::add(Notice::SUCCESS, 'Email has been sent.');
	          }
	          else
	          {
	            Notice::add(Notice::ERROR, 'User email not found.');
	          }
					}
					catch (Exception $e)
					{
						// Rollback transaction
						Database::instance()->rollback();
						// Add error notice
						Notice::add(Notice::ERROR, $e->getMessage());
					}

          break;
        }
      }
    }   
    $languages = ORM::factory('language')
      ->find_all();
		
		$this->template->main = Kostache::factory('email/send')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('languages', $languages);
	}
	/*
  public function email_send($subscribers, $post_subject, $post_message)
  {
		$subject[] = strtr($post_subject, array(
			'{first_name}' => $subscribers['first_name'],
			'{last_name}' => $subscribers['last_name'],
			'{point}' => $subscribers['point'],
			));

		$message[] = strtr($post_message, array(
			'{first_name}' => $subscribers['first_name'],
			'{last_name}' => $subscribers['last_name'],
			'{point}' => $subscribers['point'],
			));
		
		// Insert into email queue
		DB::insert('emails')
			->columns(array('to', 'subject', 'message'))
			->values(array($subscribers['email'], $subject, $message))
			->execute();

  }
  */
}