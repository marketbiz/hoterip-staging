<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Admin extends Controller_Layout_Admin {

    public function before() {
        if ($this->request->action() == 'delete') {
            $this->auto_render = FALSE;
        }

        parent::before();
    }

    public function action_manage() {
        // Is Authorized ?
        if (!A2::instance()->allowed('admin', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        if ($this->request->post('delete_checked')) {
            if (A2::instance()->allowed('admin', 'delete')) {
                try {
                    $admin_ids = $this->request->post('ids');

                    if ($admin_ids) {
                        // Delete admins
                        DB::delete('admins')
                                ->where('id', 'IN', $admin_ids)
                                ->execute();
                    }

                    // Add success notice
                    Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
                } catch (Exception $e) {
                    // Add error notice
                    Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
                }
            } else {
                Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            }
        }

        // Get admins
        $admins = ORM::factory('admin')
                ->select(
                        array(DB::expr('COUNT(admins_hotels.hotel_id)'), 'total_hotels')
                )
                ->join('admins_hotels', 'left')->on('admins_hotels.admin_id', '=', 'admins.id')
                ->group_by('admins.id')
                ->find_all();

        $this->template->main = Kostache::factory('admin/manage')
                ->set('notice', Notice::render())
                ->set('admins', $admins);
    }

    public function action_add() {
        // Is Authorized ?
        if (!A2::instance()->allowed('admin', 'add')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage admins
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'admin', 'action' => 'manage')));
        }

        if ($this->request->post('submit')) {
            try {
                $values = $this->request->post();

                // Extra validation
                $extra_validation = Validation::factory($values)
                        ->rule('username', 'Model_Admin::username_available', array(':value'))
                        ->rule('password', 'min_length', array(':value', 4))
                        ->rule('password_confirm', 'matches', array(':validation', ':field', 'password'))
                        ->rule('email', 'Model_Admin::email_available', array(':value'))
                        ->label('username', 'Username')
                        ->label('password', 'Password')
                        ->label('password_confirm', 'Password Confirmation')
                        ->label('email', 'Email');

                // Create admin
                $admin = ORM::factory('admin')
                        ->values($values, array(
                            'username',
                            'password',
                            'email',
                            'name',
                            'role',
                            'timezone',
                        ))
                        ->set('is_receive_email', Arr::get($values, 'is_receive_email', 0))
                        ->create($extra_validation);

                /// Assign Roles to New Admin
                if ($values['role'] == 'admin') {
                    // Get admin
                    $admin_id = $admin->id;

                    // Get all hotels
                    $hotels = ORM::factory('hotel')
                            ->find_all();

                    foreach ($hotels as $hotel) {
                        // If admin not assigned
                        if (!$admin->has('hotels', $hotel)) {
                            // Assign hotel to admin
                            $admin->add('hotels', $hotel);
                        }
                    }
                }

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
                // Redirect to edit
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'admin', 'action' => 'edit', 'id' => $admin->id)));
            } catch (ORM_Validation_Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('')));
            } catch (Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        // Get roles
        $roles = ORM::factory('role')
                ->find_all();

        // Get timezones
        $timezones = ORM::factory('timezone')
                ->find_all();

        $this->template->main = Kostache::factory('admin/add')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('roles', $roles)
                ->set('timezones', $timezones);
    }

    public function action_edit() {
        // Get admin id
        $admin_id = (int) $this->request->param('id');

        // Is Authorized ?
        if (!A2::instance()->allowed('admin', 'edit')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage rooms
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'admin', 'action' => 'manage')));
        }

        if ($this->request->post('submit')) {
            try {
                $values = $this->request->post();

                // Is there any password submitted	
                if (isset($values['password']) AND strlen($values['password']) > 0) {
                    // Extra validation
                    $extra_validation = Validation::factory($values)
                            ->rule('password', 'min_length', array(':value', 4))
                            ->rule('password_confirm', 'matches', array(':validation', ':field', 'password'))
                            ->label('password', 'Password')
                            ->label('password_confirm', 'Password Confirmation');

                    // Update admin
                    ORM::factory('admin')
                            ->where('id', '=', $admin_id)
                            ->find()
                            ->values($values, array(
                                'username',
                                'password',
                                'email',
                                'name',
                                'role',
                            ))
                            ->set('is_receive_email', Arr::get($values, 'is_receive_email', 0))
                            ->update($extra_validation);
                } else {
                    // Update admin
                    ORM::factory('admin')
                            ->where('id', '=', $admin_id)
                            ->find()
                            ->values($values, array(
                                'username',
                                'email',
                                'role',
                                'name',
                            ))
                            ->set('is_receive_email', Arr::get($values, 'is_receive_email', 0))
                            ->update();
                }

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
            } catch (ORM_Validation_Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('admin')));
            } catch (Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        // Get admin
        $admin = ORM::factory('admin')
                ->where('id', '=', $admin_id)
                ->find();

        // Get roles
        $roles = ORM::factory('role')
                ->find_all();

        // Get timezones
        $timezones = ORM::factory('timezone')
                ->find_all();

        $this->template->main = Kostache::factory('admin/edit')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('admin', $admin)
                ->set('roles', $roles)
                ->set('timezones', $timezones);
    }

    public function action_delete() {
        // Get admin id
        $admin_id = $this->request->param('id');

        // Get admin
        $admin = ORM::factory('admin')
                ->where('id', '=', $admin_id)
                ->find();

        if (A2::instance()->allowed('admin', 'delete')) {
            try {
                // Delete admin
                $admin->delete();

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
            } catch (Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
            }
        } else {
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
        }

        // Redirect back
        $this->request->redirect($this->request->referrer());
    }

}
