<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Deal_Hotel extends Controller_Layout_Admin {
  
  public function before()
	{
		if (in_array($this->request->action(), array('delete', 'reorder')))
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
  
  public function action_add()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('deal', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    $deal_id = $this->request->param('id');
    
    if ($this->request->post('submit'))
    {
      try
      {
        $values = Arr::extract($this->request->post(), array(
          'hotel_id',
          'currency_id',
          'price',
        ));
        
        $values['deal_id'] = $deal_id;
        $values['order'] = 0;
        
        $validation = Validation::factory($values)
          ->rule('deal_id', 'not_empty')
          ->rule('hotel_id', 'not_empty')
          ->rule('currency_id', 'not_empty')
          ->rule('price', 'not_empty')
          ->label('price', 'Price');
        
        if ($validation->check())
        {
          // Insert data
          DB::insert('deals_hotels', array_keys($values))
            ->values($values)
            ->execute();
          
          // Add success notice
          Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
          // Redirect to edit
          $this->request->redirect(Route::get('default')->uri(array('controller' => 'deal', 'action' => 'manage')));
        }
        else
        {
          // Add error notice
          Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $validation->errors('deal_hotel'));
        }
      }
      catch (Exception $e)
      {
        // Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, array($e->getMessage()));
      }
    }
    
    // Get deal
    $deal = ORM::factory('deal')
      ->where('id', '=', $deal_id)
      ->find();
    
    // Get all hotels
    $hotels = ORM::factory('hotel')
      ->select(
        array('hotel_texts.name', 'name'),
        array('city_texts.name', 'city_name'),
        array('country_texts.name', 'country_name')
      )
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('cities')->on('cities.id', '=', 'hotels.city_id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('hotels.active', '=', 1)
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name', 'ASC')
      ->find_all();

    $currencies = ORM::factory('currency')
      ->find_all();
    
    $this->template->main = Kostache::factory('deal/hotel/add')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
			->set('deal', $deal)
			->set('hotels', $hotels)
			->set('currencies', $currencies);
  }
  
  public function action_delete()
  {
    // Get deal id
    list($deal_id, $hotel_id) = array_filter(explode('-', $this->request->param('id')), 'strlen');
		
		if (A2::instance()->allowed('deal_hotel', 'delete'))
		{
			try
			{
				// Delete deal hotel
				DB::delete('deals_hotels')
					->where('deal_id', '=', $deal_id)
          ->where('hotel_id', '=', $hotel_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
    
    $this->request->redirect($this->request->referrer());
  }
  
  public function action_reorder()
  {
    // If there is post
    if ($this->request->post())
    {
      // Get deals hotels id
      $deal_hotel_ids = $this->request->post('deal_hotel_ids');
      
      try
      {
        foreach ($deal_hotel_ids as $key => $deal_hotel_id)
        {
          list($deal_id, $hotel_id) = array_filter(explode('|', $deal_hotel_id), 'strlen');
          
          // Update order
          DB::update('deals_hotels')
            ->where('deal_id', '=', $deal_id)
            ->where('hotel_id', '=', $hotel_id)
            ->set(array(
              'order' => $key + 1,
            ))
            ->execute();
        }

        $return = array(
          'success' => TRUE,
          'message' => Kohana::message('general', 'save_success'),
        );
      }
      catch (Exception $e)
      {
        $return = array(
          'success' => FALSE,
          'message' => $e->getMessage(),
        );
      }

      $this->response->body(json_encode($return));
    }
  }
  
}