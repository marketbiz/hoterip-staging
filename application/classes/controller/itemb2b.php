<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Itemb2b extends Controller_Layout_Admin {

    public function action_manage() {
        // Get selected hotel currency
        $currency = ORM::factory('currency')
                ->where('id', '=', $this->selected_hotel->currency_id)
                ->find();

        // If ajax request
        if ($this->request->is_ajax()) {
            $campaigns_data = array();

            $room_id = $_GET['id'];
            if(isset($_GET['get_stock'])){
                $room_id = $_GET['id'];
                $tanggal = $_GET['tanggal'];
                
                $item_ajax = DB::select()
                    ->from('itemb2bs')
                    ->where('room_id', '=', $room_id)
                    ->where('date', '=', $tanggal)
                    ->execute();
                
                $data = array();
                $data['jumlah'] = count($item_ajax);
                $data['result'] = array();
                if(count($item_ajax) > 0){
                    foreach($item_ajax as $k=>$v){
                        $data['result'][$k]['id']     = $v['id'];
                        $data['result'][$k]['stock']  = $v['stock'];
                        $data['result'][$k]['date']   = $v['date'];
                    }
                }
                
                /*$data = array();
                $data['jumlah'] = count($item_ajax);
                $data['result'] = array();
                if(count($item_ajax) > 0){
                    foreach($item_ajax as $k=>$v){
                        $data['result'][$k]['id']     = $v['share_stock'];
                        if(!empty($v['share_stock'])){
                            $item_stock = DB::select('items.stock')
                                ->from('items')
                                ->where('id', '=', $v['share_stock'])
                                ->execute();
                            $data['result'][$k]['stock'] = $item_stock[0]['stock'];
                        }else{
                            $data['result'][$k]['stock']  = $v['stock'];
                        }
                        $data['result'][$k]['date']   = $v['date'];
                    }
                }*/
                echo json_encode($data);
                exit();
            }else{
                $campaigns = ORM::factory('campaign')
                        ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                        ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                        ->where('rooms.id', '=', $room_id)
                        ->where('campaigns.is_show', '=', 1)
                        ->where('campaigns.status', '=', 'b')
                        ->order_by('campaigns.is_default', 'DESC')
                        ->find_all();
                foreach ($campaigns as $campaign) {
                    $benefit = '';

                    /// Additional Benefit promotion name
                    $room_names = $campaign->rooms
                            ->select(array('room_texts.name', 'name'))
                            ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                            ->where('room_texts.language_id', '=', $this->selected_language->id)
                            ->find_all()
                            ->as_array('id', 'name');

                    if ($campaign->discount_rate > 0) {
                        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
                            ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                            ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                            ':discount_rate' => $campaign->discount_rate,
                        ));
                    } elseif ($campaign->discount_amount > 0) {
                        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
                            ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                            ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                            ':currency_code' => $currency->code,
                            ':discount_amount' => $campaign->discount_amount,
                        ));
                    } elseif ($campaign->number_of_free_nights) {
                        $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
                            ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                            ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                            ':number_of_free_nights' => $campaign->number_of_free_nights,
                        ));
                    }
                    /// Additional Benefit promotion name
                    else {
                        $other_benefits = DB::select(
                                        'campaigns_benefits.*', array('benefit_texts.name', 'name')
                                )
                                ->from('campaigns_benefits')
                                ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                                ->where('benefit_texts.language_id', '=', $this->selected_language->id)
                                ->where('campaigns_benefits.campaign_id', '=', $campaign->id)
                                ->execute();

                        if (count($other_benefits)) {
                            $room_campaigns = implode(', ', $room_names);

                            $benefit = $room_campaigns;
                            $benefit .= __(' get ');

                            foreach ($other_benefits as $count => $other_benefit) {
                                $benefit .= $other_benefit['name'];
                                if ($other_benefit['value']) {
                                    $benefit .= ' for ' . $other_benefit['value'];
                                }

                                $benefit .= count($other_benefits) == ($count + 1) ? '.' : ', ';
                            }
                        }
                    }

                    if ($campaign->type == 2) {
                        $benefit .= ' ' . __('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
                    } elseif ($campaign->type == 3) {
                        $benefit .= ' ' . __('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
                    } elseif ($campaign->type == 4) {
                        $benefit .= ' ' . __('Non refundable.');
                    } elseif ($campaign->type == 5) {
                        $benefit .= ' ' . __('Flash Deal.');
                    } elseif ($campaign->is_default) {
                        $benefit .= ' ' . __('BAR.');
                        $campaign->id = 0;
                    }

                    $campaigns_data[] = array(
                        'id' => $campaign->id,
                        'name' => $benefit,
                    );
                }
                echo json_encode($campaigns_data);
                exit();
            }
        }

        // Is Authorized ?
        if (!A2::instance()->allowed('itemb2b', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        /**
         * Get values from post
         */
        $room_id = (int) $this->request->post('room_id');
        $campaign_id = $this->request->post('campaign_id') ? (int) $this->request->post('campaign_id') : 0;

        $start_date = $this->request->post('start_date');
        $end_date = $this->request->post('end_date');

        /**
         * Filter 
         */
        if (!$room_id) {
            $room = ORM::factory('room')
                    ->select(
                            'rooms.id', 'room_texts.name'
                    )
                    ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                    ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                    ->where('room_texts.language_id', '=', $this->selected_language->id)
                    ->order_by('rooms.publish_price')
                    ->find();

            $room_id = $room->id;
        }

        if (!$start_date) {
            $start_date = date('Y-m-d');
        }

        if (!$end_date) {
            $end_date = date('Y-m-d', strtotime('+7 days'));
        }

        $start_date_timestamp = strtotime($start_date);
        $end_date_timestamp = strtotime($end_date);

        // If end date before start date
        if ($end_date_timestamp < $start_date_timestamp) {
            // Swap start date with end date
            $temp = $start_date;
            $start_date = $end_date;
            $end_date = $temp;

            $start_date_timestamp = strtotime($start_date);
            $end_date_timestamp = strtotime($end_date);
        }

        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            // Is Authorized ?
            if(!$this->request->post('stb2c')){
                $stb2c = 0;
            }else{
                $stb2c = $this->request->post('stb2c');
            }
            
            if ($this->request->post('campaign_id')) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
                // Redirect to home dashboard
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'itemb2b', 'action' => 'manage')));
            }
            
            // Get room id
            $room_id = $this->request->post('room_id');
            
            // Get room ORM
            $room = ORM::factory('room')
                    ->where('id', '=', $room_id)
                    ->find();

            // Is Authorized ?
            if (!A2::instance()->allowed($room, 'read')) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
                // Redirect to manage rooms
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
            }

            $dates = Arr::get($this->request->post(), 'dates', array());
            $values = $this->request->post();
            
            try {
                // Validation
                
                foreach ($dates as $date) {
                    $validation = Validation::factory(array(
                                'date' => Arr::get($dates, $date),
                                'stock' => Arr::get($this->request->post('stocks'), $date, 0),
                                'price' => Arr::get($this->request->post('prices'), $date),
                                'minimum_night' => Arr::get($this->request->post('minimum_nights'), $date),
                                'additional_pax_price' => Arr::get($this->request->post('additional_pax_price'), $date)
                                //'extrabed_item_price' => Arr::get($this->request->post('extrabed_item_price'), $date)
                            ))
                            ->rule('date', 'not_empty', array(':value'))
                            ->rule('date', 'Model_Cancellation::valid_date', array(':value', $room_id))
                            ->rule('stock', 'not_empty', array(':value'))
                            ->rule('stock', 'digit', array(':value'))
                            ->rule('price', 'digit', array(':value'))
                    // ->rule('price', 'Model_Item::valid_price', array(':value', $date, $room_id))
                    ;

                    /*if (A1::instance()->get_user()->role != 'extranet') {
                        $validation = $validation->rule('price', 'not_empty', array(':value'))
                                ->rule('extrabed_item_price', 'digit', array(':value'))
                                ->rule('extrabed_item_price', 'Model_Item::valid_price', array(':value', $date, $room_id));
                    } else {*/
                        if (!$room->hoterip_campaign) {
                            $validation = $validation->rule('price', 'not_empty', array(':value'))
                                    ->rule('price', 'digit', array(':value'))
                                    ->rule('price', 'Model_Itemb2b::valid_price', array(':value', $date, $room_id))
                                    ->rule('additional_pax_price', 'digit', array(':value'))
                                    ->rule('additional_pax_price', 'Model_Itemb2b::valid_price', array(':value', $date, $room_id))
                            ;
                        }
                    //}
                    $validation = $validation->rule('minimum_night', 'not_empty', array(':value'))
                            ->rule('minimum_night', 'digit', array(':value'))
                            ->label('stock', 'Stock')
                            ->label('price', 'Price')
                            //->label('extrabed_item_price', 'Extrabed Item Price')
                            ->label('additional_pax_price', 'Additional Pax Price')
                            ->label('minimum_night', 'Minimum Night');

                    if (Arr::get($this->request->post('minimum_nights'), $date) == 0) {
                        // Add error notice
                        Notice::add(Notice::ERROR, Kohana::message('itemb2b', 'minimum_night'));
                        // Redirect to manage rooms
                        $this->request->redirect(Route::get('default')->uri(array('controller' => 'itemb2b', 'action' => 'manage')));
                    }

                    /// Hoterip campaign
                    // Check net price restrictions if exist

                    if (!empty($date['net_price'][$date])) {
                        if ($values['net_price'][$date] >= $values['prices'][$date]) {
                            // Add error notice
                            Notice::add(Notice::ERROR, Kohana::message('itemb2b', 'net_price'));
                            // Redirect to manage rooms
                            $this->request->redirect(Route::get('default')->uri(array('controller' => 'itemb2b', 'action' => 'manage')));
                        }
                    }

                    // If validation failed
                    if (!$validation->check()) {
                        throw new Validation_Exception($validation);
                    }
                }
                /// Vacation avail notif. Saving status
                $save_status = !$room->hoterip_campaign ? TRUE : FALSE;
                $post = $this->request->post();
                
                try {
                    // Changed items 
                    $changed_prev_items = array();
                    $changed_items = array();
                    
                    // Start transaction
                    Database::instance()->begin();

                    foreach ($dates as $date) {
                        if($stb2c){
                            $values = array(
                                'room_id' => $room_id,
                                'date' => $date,
                                'stock' => '0',
                                'minimum_night' => (int) Arr::get($this->request->post('minimum_nights'), $date),
                                'is_blackout' => (int) Arr::get($this->request->post('is_blackouts'), $date, 0),
                                'additional_pax_price' => (int) Arr::get($this->request->post('additional_pax_price'), $date, 0),
                                //extrabed do not update to database
                                //'extrabed_item_price' => (int) Arr::get($this->request->post('extrabed_item_price'), $date, 0),
                                'is_campaign_blackout' => (int) Arr::get($this->request->post('is_campaign_blackouts'), $date, 0)
                            );
                        }else{
                            $values = array(
                                'room_id' => $room_id,
                                'date' => $date,
                                'stock' => (int) Arr::get($this->request->post('stocks'), $date, 0),
                                'minimum_night' => (int) Arr::get($this->request->post('minimum_nights'), $date),
                                'is_blackout' => (int) Arr::get($this->request->post('is_blackouts'), $date, 0),
                                'additional_pax_price' => (int) Arr::get($this->request->post('additional_pax_price'), $date, 0),
                                //extrabed do not update to database
                                //'extrabed_item_price' => (int) Arr::get($this->request->post('extrabed_item_price'), $date, 0),
                                'is_campaign_blackout' => (int) Arr::get($this->request->post('is_campaign_blackouts'), $date, 0)
                            );
                        }
                        /*if (A1::instance()->get_user()->role != 'extranet') {
                            $values['price'] = (int) Arr::get($this->request->post('prices'), $date);
                            $values['net_price'] = (int) Arr::get($this->request->post('net_price'), $date, 0);
                        } else {*/
                            if (!$room->hoterip_campaign) {
                                $values['price'] = (int) Arr::get($this->request->post('prices'), $date);
                                $values['net_price'] = (int) Arr::get($this->request->post('net_price'), $date, 0);
                            }
                        //}
                            
                        // Get the item
                        $item = DB::select()
                                ->from('itemb2bs')
                                ->where('room_id', '=', $room_id)
                                ->where('campaign_id', '=', $campaign_id)
                                ->where('date', '=', $date)
                                ->as_object()
                                ->execute()
                                ->current();

                        // If item already exist
                        if ($item) {
                            // Clone item to write log
                            $prev_item = clone $item;

                            if (!$this->checkPriceHasInRule($values)) {
                                $warningItemPrice = true;
                                $restrictionsVacation[$date] = 1;
                                $values['price'] = 0;
                                $post['prices'][$date] = 0;
                            }
                            
                            // Update
                            DB::update('itemb2bs')
                                    ->set($values)
                                    ->where('room_id', '=', $room_id)
                                    ->where('campaign_id', '=', $campaign_id)
                                    ->where('date', '=', $date)
                                    ->execute();
                        } else {
                            $prev_item = new stdClass;
                            $prev_item->stock = NULL;
                            $prev_item->price = NULL;
                            $prev_item->net_price = NULL;
                            $prev_item->minimum_night = NULL;
                            $prev_item->is_blackout = NULL;
                            $prev_item->is_campaign_blackout = NULL;

                            if (!$this->checkPriceHasInRule($values)) {
                                $warningItemPrice = true;
                                $restrictionsVacation[$date] = 1;
                                $values['price'] = 0;
                                $post['prices'][$date] = 0;
                            }

                            // Create new
                            DB::insert('itemb2bs')
                                    ->columns(array_keys($values))
                                    ->values(array_values($values))
                                    ->execute();
                        }

                        // Get the item
                        $item = DB::select()
                                ->from('itemb2bs')
                                ->where('room_id', '=', $room_id)
                                ->where('campaign_id', '=', $campaign_id)
                                ->where('date', '=', $date)
                                ->as_object()
                                ->execute()
                                ->current();

                        // Get room
                        $room = DB::select()
                                ->from('rooms')
                                ->where('id', '=', $room_id)
                                ->as_object()
                                ->execute()
                                ->current();

                        // Set log values
                        $log_values = array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $room->hotel_id,
                            'action' => 'Changed Item',
                            'room_texts' => serialize(DB::select()
                                            ->from('room_texts')
                                            ->where('room_id', '=', $room->id)
                                            ->execute()
                                            ->as_array('language_id')),
                            'text' => 'Date: :date. '
                            . 'Stock: :prev_stock -> :stock. '
                            . 'Price: :prev_price -> :price. '
                            . 'Minimum Night: :prev_minimum_night -> :minimum_night. '
                            . 'Stop Sell: :prev_is_blackout -> :is_blackout. '
                            . 'Stop Promotion: :prev_is_campaign_blackout -> :is_campaign_blackout.',
                            'data' => serialize(array(
                                ':prev_stock' => $prev_item->stock ? $prev_item->stock : 'None',
                                ':prev_price' => $prev_item->price ? Num::display($prev_item->price) : 'None',
                                ':prev_minimum_night' => $prev_item->minimum_night ? $prev_item->minimum_night : 'None',
                                ':prev_is_blackout' => $prev_item->is_blackout ? 'Yes' : 'No',
                                ':prev_is_campaign_blackout' => $prev_item->is_campaign_blackout ? 'Yes' : 'No',
                                ':date' => date('M j, Y', strtotime($item->date)),
                                ':stock' => $item->stock,
                                ':price' => Num::display($item->price),
                                ':minimum_night' => $item->minimum_night,
                                ':is_blackout' => $item->is_blackout ? 'Yes' : 'No',
                                ':is_campaign_blackout' => $item->is_campaign_blackout ? 'Yes' : 'No',
                            )),
                            'created' => time(),
                        );

                        // Write log
                        DB::insert('logs')
                                ->columns(array_keys($log_values))
                                ->values(array_values($log_values))
                                ->execute();

                        // Track changed items
                        if ($prev_item->stock !== $item->stock OR
                                $prev_item->price !== $item->price OR
                                $prev_item->minimum_night !== $item->minimum_night OR
                                $prev_item->is_blackout !== $item->is_blackout OR
                                $prev_item->is_campaign_blackout !== $item->is_campaign_blackout) {
                            $changed_prev_items[$date] = $prev_item;
                            $changed_items[$date] = $item;
                        }
                    }
                    // Get QA admins emails
                    $qa_emails = ORM::factory('admin')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->where('admins.role', '=', 'quality-assurance')
                            ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                            ->find_all()
                            ->as_array('email', 'name');
                    // If there is QA admins
                    if (count($qa_emails) > 0) {
                        ksort($changed_prev_items);
                        ksort($changed_items);

                        // Create email message
                        $message = Kostache::factory('itemb2b/qa/manage')
                                ->set('admin_username', A1::instance()->get_user()->username)
                                ->set('admin_name', A1::instance()->get_user()->name)
                                ->set('room_name', ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name)
                                ->set('changed_prev_items', $changed_prev_items)
                                ->set('changed_items', $changed_items)
                                ->render();

                        // Send email
                        Email::factory(strtr(':hotel_name - Changed Item', array(
                                    ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                        )), $message, 'text/html')
                                ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                ->to($qa_emails)
                                ->send();
                    }
                    
                    if ($warningItemPrice) {
                        Notice::add(Notice::WARNING, Kohana::message('itemb2b', 'price_rule'));
                    }

                    // Commit transaction
                    Database::instance()->commit();
                    // Add success notice
                    Notice::add(Notice::SUCCESS, Kohana::message('notice', 'save_success'));
                } catch (ORM_Validation_Exception $e) {
                    // Rollback transaction
                    Database::instance()->rollback();
                    /// Vacation avail notif.  Save status changed
                    $save_status = FALSE;
                    // Add error notice
                    Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('itemb2b'));
                } catch (Exception $e) {
                    // Rollback transaction
                    Database::instance()->rollback();
                    /// Vacation avail notif.  Save status changed
                    $save_status = FALSE;
                    // Add error notice
                    Notice::add(Notice::ERROR, $e->getMessage() . $e->getLine());
                }
                /*
                 *  Item Notification for API
                 */
                
                //get all campaigns
                $all_campaigns = DB::select(
                                array('campaigns_rooms.*'), array('campaigns.booking_end_date'), array('campaigns.is_default')
                        )
                        ->from('campaigns_rooms')
                        ->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
                        ->where('room_id', '=', $room_id)
                        ->execute()
                        ->as_array();
                
                //unset campaign if expired
                //foreach ($all_campaigns as $key => $value) {
                //    if ((date('Y-m-d') > $value['booking_end_date']) && !$value['is_default']) {
                //        unset($all_campaigns[$key]);
                //    }
                //}
                
                if ($save_status) {
                    
                    //log old
                    // Add logs
                    // Log::instance()->add(Log::INFO, 'API Vacation Items');
                    // Log::instance()->add(Log::INFO, 'Admin ID: '.A1::instance()->get_user());
                    // Log::instance()->add(Log::INFO, 'Hotel ID: '.$this->selected_hotel->id);
                    // Log::instance()->add(Log::INFO, 'Room ID: '.$room_id);
                    //new log
                    //echo '<pre>';print_r($all_campaigns);echo '</pre>';
                    CustomLog::factory()->add(2, 'DEBUG', 'API Vacation Items');
                    CustomLog::factory()->add(2, 'DEBUG', 'Admin ID: ' . A1::instance()->get_user());
                    CustomLog::factory()->add(2, 'DEBUG', 'Hotel ID: ' . $this->selected_hotel->id);
                    CustomLog::factory()->add(2, 'DEBUG', 'Room ID: ' . $room_id);
                    
                    DB::update('rooms')
                                    ->set(array('share_stock'=>$stb2c))
                                    ->where('id', '=', $room_id)
                                    ->where('hotel_id', '=', $this->selected_hotel->id)
                                    ->execute();
                    
                    /*$postB2C = array();
                    $postB2C['room_id']             = $post['room_id'];
                    $postB2C['start_date']          = $post['start_date'];
                    $postB2C['end_date']            = $post['end_date'];
                    $postB2C['auto_stock']          = $post['auto_stock'];
                    $postB2C['auto_price']          = $post['auto_price'];
                    $postB2C['auto_minimum_night']  = $post['auto_minimum_night'];
                    
                    $postB2B = array();
                    $postB2B['room_id']             = $post['room_id'];
                    $postB2B['start_date']          = $post['start_date'];
                    $postB2B['end_date']            = $post['end_date'];
                    $postB2B['auto_stock']          = $post['auto_stock'];
                    $postB2B['auto_price']          = $post['auto_price'];
                    $postB2B['auto_minimum_night']  = $post['auto_minimum_night'];
                    
                    $val = $post;*/
                    //$i=0;
                    //$b=0;
                    /*foreach($post['item_id'] as $k=>$v) {
                        if(!empty($v)){
                            $postB2C['dates'][$k] = $post['dates'][$k];
                            $postB2C['stocks'][$k] = $post['stocks'][$k];
                            $postB2C['item_id'][$k] = $v;
                            $postB2C['prices'][$k] = $post['prices'][$k];
                            $postB2C['minimum_nights'][$k] = $post['minimum_nights'][$k]; 
                            $dateC[$i] = $k;
                            $i++;
                        }else{
                            $postB2B['dates'][$k] = $post['dates'][$k];
                            $postB2B['stocks'][$k] = $post['stocks'][$k];
                            $postB2B['item_id'][$k] = $v;
                            $postB2B['prices'][$k] = $post['prices'][$k];
                            $postB2B['minimum_nights'][$k] = $post['minimum_nights'][$k];
                            $dateB[$b] = $k;
                            $b++;
                        }
                    }
                    $postB2C['submit'] = 'Submit';
                    $postB2B['submit'] = 'Submit';
                    
                    if(is_array($dateC)){
                        $postB2C['start_date']          = $dateC[0];
                        $postB2C['end_date']            = $dateC[count($dateC)-1];
                    }
                    if(is_array($dateB)){
                        $postB2B['start_date']          = $dateB[0];
                        $postB2B['end_date']            = $dateB[count($dateB)-1];
                    }*/
                    
                    /*foreach ($all_campaigns as $key => $value) {
                        $valuesB = $postB2B;
                        $valuesC = $postB2C;

                        if (!empty($restrictionsVacation)) {
                            $valuesB['is_blackouts'] = $restrictionsVacation;
                            $valuesC['is_blackouts'] = $restrictionsVacation;
                        }
                        
                        $valuesB['hotel_id'] = $this->selected_hotel->id;
                        $valuesB['room_id'] = $room_id;
                        $valuesB['campaign_id'] = $value['campaign_id'];
                        $valuesB['end_date'] = date("Y-m-d", strtotime("$end_date +1 day"));
                        
                        $valuesC['hotel_id'] = $this->selected_hotel->id;
                        $valuesC['room_id'] = $room_id;
                        $valuesC['campaign_id'] = $value['campaign_id'];
                        $valuesC['end_date'] = date("Y-m-d", strtotime("$end_date +1 day"));
                        
                        // Vacation notifictions
                        if(count($valuesC) > 0){
                            $availability = Controller_Vacation_Notifications_Availability::item($valuesC) ? 'success' : 'failed';
                            $rateplan = Controller_Vacation_Notifications_Rateplan::item($valuesC) ? 'success' : 'failed';
                            $amount = Controller_Vacation_Notifications_Amount::item($valuesC) ? 'success' : 'failed';
                        }
                        //
                        //
                    }*/
                    
                    
                    Notice::add(Notice::SUCCESS, Kohana::message('notice', 'save_success'));
                    
                    /*CustomLog::factory()->add(2, 'DEBUG', 'API Vacation Items');
                    CustomLog::factory()->add(2, 'DEBUG', 'Admin ID: ' . A1::instance()->get_user());
                    CustomLog::factory()->add(2, 'DEBUG', 'Hotel ID: ' . $this->selected_hotel->id);
                    CustomLog::factory()->add(2, 'DEBUG', 'Room ID: ' . $room_id);
                    //exit();
                    //send RQ API for al campaign in active
                    foreach ($all_campaigns as $key => $value) {
                        // Add Hotel
                        $values = $post;

                        if (!empty($restrictionsVacation)) {
                            $values['is_blackouts'] = $restrictionsVacation;
                        }

                        $values['hotel_id'] = $this->selected_hotel->id;
                        $values['room_id'] = $room_id;
                        $values['campaign_id'] = $value['campaign_id'];
                        $values['end_date'] = date("Y-m-d", strtotime("$end_date +1 day"));
                        
                        // Vacation notifictions
                        $availability = Controller_Vacation_Notifications_Availability::item($values) ?
                                'success' : 'failed';
                        $rateplan = Controller_Vacation_Notifications_Rateplan::item($values) ?
                                'success' : 'failed';

                        $amount = Controller_Vacation_Notifications_Amount::item($values) ?
                                'success' : 'failed';

                        //Sucess or error message vacation
                        if ($rateplan == 'success') {
                            Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_notif_save_success'));
                        } elseif ($rateplan == 'failed') {
                            Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_notif_save_failure'));
                        }

                        if ($amount == 'success') {
                            Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_rateplan_amount_save_success'));
                        } elseif ($amount == 'failed') {
                            Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_rateplan_amount_notif_save_failure'));
                        }

                        if ($availability == 'success') {
                            Notice::add(Notice::SUCCESS, Kohana::message('api', 'vacation_availability_vacation_save_success'));
                        } elseif ($availability == 'failed') {
                            Notice::add(Notice::ERROR, Kohana::message('api', 'vacation_availability_vacation_save_failure'));
                        }
                    }*/
                }
            } catch (Validation_Exception $e) {
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->array->errors('itemb2b'));
            }
        }
/*end submit*/
        /**
         * Build variables for view
         */
        //build subquery for $range_cancel_date
        $sub_campaign_id = DB::select(
                        'id'
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->where('is_default', '=', 1)
                ->where('campaigns_rooms.room_id', '=', $room_id)
                ->execute()
                ->current()
        ;
        //get range_cancelation_date
        $range_cancel_date = DB::select(
                        array('cancellations.start_date', 'cancelation_start_date'), array('cancellations.end_date', 'cancelation_end_date')
                )
                ->from('campaigns_cancellations')
                ->join('cancellations')->on('cancellations.id', '=', 'campaigns_cancellations.cancellation_id')
                ->where('campaigns_cancellations.campaign_id', '=', $sub_campaign_id)
                ->order_by('cancelation_end_date', 'ASC')
                ->execute()
                ->as_array()
        ;
        if (!empty($range_cancel_date)) {
            foreach ($range_cancel_date as $key => $value) {
                if (strtotime('now') + Date::offset($this->selected_hotel->timezone) <= strtotime($value['cancelation_end_date'])) {
                    unset($range_cancel_date);
                    $range_cancel_date = $value;
                } else {
                    unset($range_cancel_date);
                    $range_cancel_date = $value;
                }
            }
        } else {
            $range_cancel_date['cancelation_start_date'] = '0000-00-00';
            $range_cancel_date['cancelation_end_date'] = '0000-00-00';
        }


        $range_stay_date['stay_start_date'] = '0000-00-00';
        $range_stay_date['stay_end_date'] = '0000-00-00';
        //if campaigns is not default, get stay_date and stay_end_date campaign
        if ($campaign_id != 0) {
            $range_stay_date = DB::select(
                            array('campaigns.stay_start_date', 'stay_start_date'), array('campaigns.stay_end_date', 'stay_end_date')
                    )
                    ->from('campaigns')
                    ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                    ->where('campaigns.id', '=', $campaign_id)
                    ->where('campaigns_rooms.room_id', '=', $room_id)
                    ->execute()
                    ->current()
            ;
        }

        $room = ORM::factory('room')
                ->select(
                        'rooms.*', 'room_texts.name'
                )
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('rooms.id', '=', $room_id)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
                ->order_by('rooms.publish_price')
                ->find();
        
        $campaigns = ORM::factory('campaign')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->where('rooms.id', '=', $room_id)
                ->where('campaigns.is_show', '=', 1)
                ->where('campaigns.status', '=', 'b')
                ->order_by('campaigns.is_default', 'DESC')
                ->find_all();
        
        $items = ORM::factory('itemb2b')
                ->select(
                        'itemb2bs.*', array('room_texts.name', 'room_name')
                        //array('items.stock', 'stockB2C')
                        //array('(SELECT stock FROM items WHERE id = itemb2bs.share_stock)', 'stockB2C')
                        /// Additional pax price
                        //array('rooms.extrabed_price', 'extrabed_price'),
                )
                ->join('rooms')->on('itemb2bs.room_id', '=', 'rooms.id')
                //->join('items')->on('itemb2bs.share_stock', '=', 'items.id')
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('itemb2bs.room_id', '=', $room_id)
                ->where('itemb2bs.campaign_id', '=', 0)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
        ;
        $items_share = ORM::factory('item')
                ->select(
                        'items.*', array('room_texts.name', 'room_name')
                        //array('items.stock', 'stockB2C')
                        //array('(SELECT stock FROM items WHERE id = itemb2bs.share_stock)', 'stockB2C')
                        /// Additional pax price
                        //array('rooms.extrabed_price', 'extrabed_price'),
                )
                ->join('rooms')->on('items.room_id', '=', 'rooms.id')
                //->join('items')->on('itemb2bs.share_stock', '=', 'items.id')
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('items.room_id', '=', $room_id)
                ->where('items.campaign_id', '=', 0)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
        ;
        
        if ($end_date <= $range_cancel_date['cancelation_end_date'] && $end_date <= $range_stay_date['stay_end_date']) {
            $items = $items
                    ->where('itemb2bs.date', '>=', $start_date)
                    ->where('itemb2bs.date', '<=', $end_date);
            
            $items_share = $items_share
                           ->where('items.date', '>=', $start_date)
                           ->where('items.date', '<=', $end_date);
                    
        } elseif ($campaign_id != 0 && $end_date >= $range_stay_date['stay_end_date'] && $end_date <= $range_cancel_date['cancelation_end_date']
        ) {
            $items = $items
                    ->where('itemb2bs.date', '>=', $start_date)
                    ->where('itemb2bs.date', '<=', $range_stay_date['stay_end_date']);
            
            $items_share = $items_share
                    ->where('items.date', '>=', $start_date)
                    ->where('items.date', '<=', $range_stay_date['stay_end_date']);
        } elseif ($end_date <= $range_stay_date['stay_end_date'] && $end_date >= $range_cancel_date['cancelation_end_date']
        ) {
            $items = $items
                    ->where('itemb2bs.date', '>=', $start_date)
                    ->where('itemb2bs.date', '<=', $range_cancel_date['cancelation_end_date']);
            
            $items_share = $items_share
                    ->where('items.date', '>=', $start_date)
                    ->where('items.date', '<=', $range_cancel_date['cancelation_end_date']);
        } elseif ($end_date >= $range_stay_date['stay_end_date'] && $end_date >= $range_cancel_date['cancelation_end_date']
        ) {
            $items = $items
                    ->where('itemb2bs.date', '>=', $start_date)
                    ->where('itemb2bs.date', '<=', $range_cancel_date['cancelation_end_date']);
            
            $items_share = $items_share
                    ->where('items.date', '>=', $start_date)
                    ->where('items.date', '<=', $range_cancel_date['cancelation_end_date']);
        } else {
            $items = $items
                    ->where('itemb2bs.date', '>=', $start_date)
                    ->where('itemb2bs.date', '<=', $end_date);
            
            $items_share = $items_share
                    ->where('items.date', '>=', $start_date)
                    ->where('items.date', '<=', $end_date);
        }
        
        $items = $items
                ->find_all()
                ->as_array('date');
        
        $items_share = $items_share
                ->find_all()
                ->as_array('date');

        
        if ($end_date <= $range_cancel_date['cancelation_end_date'] && $end_date <= $range_stay_date['stay_end_date']) {
            //build date array and get warning
            $dates = array();
            for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
            }
        } elseif ($campaign_id != 0 && $end_date >= $range_stay_date['stay_end_date'] && $end_date <= $range_cancel_date['cancelation_end_date']
        ) {
            //build date array and get warning
            Notice::add(Notice::WARNING, Kohana::message('itemb2b', 'promotion_date') . $range_stay_date['stay_end_date']);

            $end_date_timestamp = strtotime($range_stay_date['stay_end_date']);
            $dates = array();
            for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
            }
        } elseif ($end_date <= $range_stay_date['stay_end_date'] && $end_date >= $range_cancel_date['cancelation_end_date']
        ) {
            //build date array and get warning
            Notice::add(Notice::WARNING, Kohana::message('itemb2b', 'cancelation_date') . $range_cancel_date['cancelation_end_date']);

            $end_date_timestamp = strtotime($range_cancel_date['cancelation_end_date']);
            $dates = array();
            for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
            }
        } elseif ($end_date >= $range_stay_date['stay_end_date'] && $end_date >= $range_cancel_date['cancelation_end_date']
        ) {
            //build date array and get warning
            Notice::add(Notice::WARNING, Kohana::message('itemb2b', 'cancelation_date') . $range_cancel_date['cancelation_end_date']);

            $end_date_timestamp = strtotime($range_cancel_date['cancelation_end_date']);
            $dates = array();
            for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
            }
        } else {
            //build date array and get warning
            $dates = array();
            for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
            }
        }
        
        if(!$room->share_stock){
            if ($this->request->post('is_no_stock')) {
                foreach ($items as $item) {
                    if ($item->stock > 0) {
                        if (Arr::get($dates, $item->date)) {
                            unset($dates[$item->date]);
                        }
                    }
                }
            }
        }else{
            if ($this->request->post('is_no_stock')) {
                foreach ($items_share as $item) {
                    if ($item->stock > 0) {
                        if (Arr::get($dates, $item->date)) {
                            unset($dates[$item->date]);
                        }
                    }
                }
            }
        }
        

        $rooms = ORM::factory('room')
                ->select(
                        'rooms.id', 'room_texts.name'
                )
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
                ->where('room_texts.language_id', '=', $this->selected_language->id)
                ->order_by('rooms.publish_price', 'ASC')
                ->find_all();
        //$share_stock = $this->selected_hotel->share_stock;
        // If there is no rooms
        if (count($rooms) <= 0) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('itemb2b', 'no_room'));
            // Redirect to manage campaign
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
        }
        
        $cancellation_available = ORM::factory('cancellation')
                        ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
                        ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
                        ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                        ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                        ->where('campaigns.is_default', '=', 1)
                        ->where('rooms.id', '=', $room_id)
                        ->count_all() > 0;

        // If there is no cancellation policy for this room
        if (!$cancellation_available) {
            // Add error notice
            Notice::add(Notice::WARNING, Kohana::message('itemb2b', 'no_cancellation_policy'));
//      $dates = array();
//      $items = array();
        }
        //echo $room->hoterip_campaign;
        $this->template->main = Kostache::factory('itemb2b/manage')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('logged_in_admin', A1::instance()->get_user())
                ->set('is_update_campaign', $campaign_id ? TRUE : FALSE)
                /// Hoterip campaign
                ->set('hoterip', A1::instance()->get_user()->role != 'extranet' ? TRUE : FALSE)
                ->set('selected_hotel', $this->selected_hotel)
                ->set('selected_language', $this->selected_language)
                ->set('room_id', $room_id)
                ->set('campaign_id', $campaign_id)
                ->set('start_date', $start_date)
                ->set('end_date', $end_date)
                ->set('dates', $dates)
                ->set('campaigns', $campaigns)
                ->set('room', $room)
                ->set('items', $items)
                ->set('items_share', $items_share)
                ->set('rooms', $rooms)
                ->set('currency', $currency);
    }

    // For H.I.S Vacation
    public function action_export() {
        // Is Authorized ?
        if (!A2::instance()->allowed('item', 'export')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        if ($this->request->post('submit')) {
            $values = $this->request->post();

            // Get selected hotel
            $hotel = ORM::factory('hotel')
                    ->where('id', '=', Arr::get($values, 'hotel_id'))
                    ->find();

            // Get selected year
            $year = Arr::get($values, 'year', date('Y'));

            if ($hotel->loaded()) {
                // Create csv
                $csv = CSV::factory('Item - ' . date('YmdHis') . '.csv')
                        ->titles(array_merge(array('Room Name', 'Month'), Arr::range(1, 31)));

                // Get hotel rooms
                $rooms = ORM::factory('room')
                        ->select(
                                array('room_texts.name', 'name')
                        )
                        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                        ->where('rooms.hotel_id', '=', $hotel->id)
                        ->where('room_texts.language_id', '=', $this->selected_language->id)
                        ->find_all();

                // Get months
                $months = Date::months();

                foreach ($rooms as $room) {
                    foreach ($months as $month) {
                        $last_day = count(Date::days($month, $year));

                        $items = ORM::factory('item')
                                ->where('room_id', '=', $room->id)
                                ->where('date', '>=', date('Y-m-d', strtotime("$year-$month-1")))
                                ->where('date', '<=', date('Y-m-d', strtotime("$year-$month-$last_day")))
                                ->order_by('date')
                                ->find_all();

                        // Fill everyday stock with blank first
                        $stocks = array_fill(0, $last_day, NULL);

                        foreach ($items as $item) {
                            // Get the day
                            $day = date('j', strtotime($item->date));

                            // If not blackout
                            if (!$item->is_blackout) {
                                // Fill the stock day
                                $stocks[$day - 1] = $item->stock;
                            }
                        }

                        // Add csv row
                        $csv->values(array_merge(array($room->name, $month), $stocks));
                    }
                }

                // Send file to browser so client can download it
                $csv->send_file();
            } else {
                Notice::add(Notice::ERROR, 'Hotel is not valid');
            }
        }

        $hotels = ORM::factory('hotel')
                ->select(
                        array('hotel_texts.name', 'name'), array('city_texts.name', 'city_name')
                )
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->order_by('hotel_texts.name')
                ->find_all();

        $this->template->main = Kostache::factory('item/export')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('hotels', $hotels)
        ;
    }

    private function checkPriceHasInRule($item) {
        //load config price rule
        if (is_array($item))
            $item = (object) $item;

        $rulePrice = Kohana::config('application.rule_price');

        if ($this->selected_hotel->currency_id != 1) {
            $price = $item->price * Model::factory('exchange')->to($this->selected_hotel->currency_id, 1);
        } else {
            $price = $item->price;
        }

        if ($price < $rulePrice['min_price_item']) {
            return false;
        } elseif ($price > $rulePrice['max_price_item']) {
            return false;
        }

        return true;
    }

}
