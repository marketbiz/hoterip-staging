<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Api extends Controller_Layout_Admin {

  public function action_manage()
  {
    // Is Authorized ?
    if ( ! A2::instance()->allowed('api', 'manage'))
    {
      // Add error notice
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      // Redirect to home dashboard
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }

    if ($this->request->post('delete_checked'))
    {
      if (A2::instance()->allowed('admin', 'delete'))
      {
        try
        {
          $admin_ids = $this->request->post('ids');

          if ($admin_ids)
          {
            // Delete API Access
            DB::delete('api_access')
              ->where('admin_id', 'IN', $admin_ids)
              ->execute();

            // Delete admins
            DB::delete('admins')
              ->where('id', 'IN', $admin_ids)
              ->execute();
          }

          // Add success notice
          Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
        }
        catch (Exception $e)
        {
          // Add error notice
          Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
        }
      }
      else
      {
        Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      }
    }
    
    // Get admins
    $api_access = DB::select(
        array('admins.id', 'id'),
        array('admins.username', 'username'),
        array('admins.name', 'name'),
        array('admins.email', 'email'),
        array('api_access.api_key', 'api_key'),
        array('api_access.ip', 'ip')     
      )
      ->from('admins')
      ->join('roles')->on('roles.value', '=', 'admins.role')
      ->join('api_access')->on('api_access.admin_id', '=', 'admins.id')
      ->where('admins.role', '=', 'api')
      ->execute();

    $this->template->main = Kostache::factory('api/access')
      ->set('notice', Notice::render())
      ->set('api_access', $api_access);

  }

  public function action_add()
  {
    // Is Authorized ?
    if ( ! A2::instance()->allowed('api', 'add'))
    {
      // Add error notice
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      // Redirect to manage admins
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }
    
    if ($this->request->post('submit'))
    {
      // Start transaction
      Database::instance()->begin();

      try
      {
        $values = $this->request->post();
        $values['role'] = 'api';

        // Extra validation
        $extra_validation = Validation::factory($values)
          ->rule('name', 'not_empty', array(':value'))
          ->rule('username', 'not_empty', array(':value'))
          ->rule('password', 'not_empty', array(':value'))
          ->rule('password_confirm', 'not_empty', array(':value'))
          ->rule('email', 'not_empty', array(':value'))
          ->rule('name', 'not_empty', array(':value'))
          ->rule('name', 'Model_Admin::username_available', array(':value'))
          ->rule('username', 'Model_Admin::username_available', array(':value'))
          ->rule('password', 'min_length', array(':value', 4))
          ->rule('password_confirm', 'matches', array(':validation', ':field', 'password'))
          ->rule('email', 'Model_Admin::email_available', array(':value'))
          ->label('name', 'Name')
          ->label('username', 'Username')
          ->label('password', 'Password')
          ->label('password_confirm', 'Password Confirmation')
          ->label('email', 'Email');

        // Create admin
        $admin = ORM::factory('admin')
          ->values($values, array(
            'username',
            'password',
            'email',
            'name',
            'role',
            'timezone',
          ))
          ->set('is_receive_email', Arr::get($values, 'is_receive_email', 0))
          ->create($extra_validation);

        $api_key = md5($values['name'].strtotime("now").$values['email']);

        // Get admin
        $admin_id = $admin->id;

        DB::insert('api_access')
          ->columns(array('admin_id', 'name_segment','api_key', 'ip'))
          ->values(array($admin_id, $values['username'], $api_key, $values['ip']))
          ->execute();

        // Get dummy hotels
        /// API asignne
        $hotels = ORM::factory('hotel')
          ->where('hotels.url_segment', '=', 'api-hotel')
          ->find_all();
        
        foreach ($hotels as $hotel)
        {
          // If admin not assigned
          if ( ! $admin->has('hotels', $hotel))
          {
            // Assign hotel to admin
            $admin->add('hotels', $hotel);
          }
        }

        // Commit transaction
        Database::instance()->commit();

        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        // Redirect to edit
        $this->request->redirect(Route::get('default')->uri(array('controller' => 'api', 'action' => 'edit', 'id' => $admin->id)));
      }
      catch (ORM_Validation_Exception $e)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('')));
      }
      catch (Exception $e)
      {
        // Add error notice
        Notice::add(Notice::ERROR, $e->getMessage());
      }
    }

    // Get timezones
    $timezones = ORM::factory('timezone')
      ->find_all();

    $this->template->main = Kostache::factory('api/add')
      ->set('notice', Notice::render())
      ->set('timezones', $timezones)
      ->set('values', $this->request->post());
  }

  public function action_edit()
  {
    // Get admin id
    $admin_id = (int) $this->request->param('id');

    // Is Authorized ?
    if ( ! A2::instance()->allowed('api', 'edit'))
    {
      // Add error notice
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      // Redirect to manage admins
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }

    $values = $this->request->post();

    if ($this->request->post('api_key'))
    {
      // Start transaction
      Database::instance()->begin();

      try
      {
        $api_key = md5($values['name'].strtotime("now").$values['email']);

        DB::update('api_access')
          ->set(array('api_key' => $api_key))
          ->where('admin_id', '=', $admin_id)
          ->execute();

        // Commit transaction
        Database::instance()->commit();
      
        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        // Redirect to edit
        $this->request->redirect(Route::get('default')->uri(array('controller' => 'api', 'action' => 'edit', 'id' => $admin_id)));
      }
      catch (ORM_Validation_Exception $e)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('')));
      }
      catch (Exception $e)
      {
        // Add error notice
        Notice::add(Notice::ERROR, $e->getMessage());
      }
    }

    if ($this->request->post('submit'))
    {
      
      // Start transaction
      Database::instance()->begin();

      try
      {
        $values['role'] = 'api';

        if (isset($values['password']) AND strlen($values['password']) > 0)
        {
          // Extra validation
          $extra_validation = Validation::factory($values)
            ->rule('password', 'min_length', array(':value', 4))
            ->rule('password_confirm', 'matches', array(':validation', ':field', 'password'))
            ->label('password', 'Password')
            ->label('password_confirm', 'Password Confirmation');

          // Update admin
          ORM::factory('admin')
            ->where('id', '=', $admin_id)
            ->find()
            ->values($values, array(
              'username',
              'password',
              'email',
              'name',
              'role',
              'timezone',
            ))
            ->set('is_receive_email', Arr::get($values, 'is_receive_email', 0))
            ->update($extra_validation);
        }
        else
        {
          // Update admin
          ORM::factory('admin')
            ->where('id', '=', $admin_id)
            ->find()
            ->values($values, array(
              'username',
              'email',
              'role',
              'name',
              'timezone',
            ))
            ->set('is_receive_email', Arr::get($values, 'is_receive_email', 0))
            ->update();
        }

        DB::update('api_access')
          ->set(array('name_segment' => $values['username']))
          ->set(array('ip' => $values['ip']))
          ->where('admin_id', '=', $admin_id)
          ->execute();

        // Commit transaction
        Database::instance()->commit();

        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        // Redirect to edit
        $this->request->redirect(Route::get('default')->uri(array('controller' => 'api', 'action' => 'edit', 'id' => $admin_id)));
      }
      catch (ORM_Validation_Exception $e)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('')));
      }
      catch (Exception $e)
      {
        // Add error notice
        Notice::add(Notice::ERROR, $e->getMessage());
      }
    }

    // Get admins
    $api_access = DB::select(
        array('admins.id', 'id'),
        array('admins.username', 'username'),
        array('admins.name', 'name'),
        array('admins.email', 'email'),
        array('admins.timezone', 'timezone'),
        array('api_access.api_key', 'api_key'),
        array('api_access.ip', 'ip')     
      )
      ->from('admins')
      ->join('roles')->on('roles.value', '=', 'admins.role')
      ->join('api_access')->on('api_access.admin_id', '=', 'admins.id')
      ->where('admins.role', '=', 'api')
      ->where('admins.id', '=', $admin_id)
      ->execute()
      ->current();

    // Get timezones
    $timezones = ORM::factory('timezone')
      ->find_all();

    $this->template->main = Kostache::factory('api/edit')
      ->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('timezones', $timezones)
      ->set('api_access', $api_access);
  }

  public function action_delete()
  {
    // Get admin id
    $admin_id = $this->request->param('id');
    
    // Get admin
    $admin = ORM::factory('admin')
      ->where('id', '=', $admin_id)
      ->find();
    
    if (A2::instance()->allowed('admin', 'delete'))
    {
      try
      {
        // Delete API Access
        DB::delete('api_access')
          ->where('admin_id', '=', $admin_id)
          ->execute();

        // Delete admin
        $admin->delete();

        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
      }
      catch (Exception $e)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
      }
    }
    else
    {
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
    }
    
    // Redirect back
    $this->request->redirect($this->request->referrer());
  }
}