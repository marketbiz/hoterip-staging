<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Language extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'change')
		{
			$this->auto_render = FALSE;
		}
		
		return parent::before();
	}
	
	public function action_change() 
	{
		// Get language id
		$language_id = $this->request->param('id');
		
		// Set selected language id in cookie
		Cookie::set('selected_language_id', $language_id);
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}