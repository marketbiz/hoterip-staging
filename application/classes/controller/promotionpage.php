<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_PromotionPage extends Controller_Layout_Admin {

    private $view;

    public function action_manage() {
        //get from query string
        $type = $this->request->query('type');
        //process delete checked earlybird
        if ($this->request->post('check_all_earlybird')) {
            $ids = $this->request->post('ids');
            foreach ($ids as $id_k => $id) {
                $this->action_delete($id, 2, 'action_manage');
            }
        }

        //process delete checked earlybird lastminutes
        if ($this->request->post('check_all_lastminutes')) {
            foreach ($this->request->post('ids') as $id_k => $id) {
                $this->action_delete($id, 3, 'action_manage');
            }
        }

        //process delete checked earlybird lastminutes
        if ($this->request->post('check_all_superdeal')) {
            die('1');
            foreach ($this->request->post('ids') as $id_k => $id) {
                $this->action_delete($id, 1, 'action_manage');
            }
        }

        $datas['earlybirds'] = $this->earlybird();
        $datas['lastminutes'] = $this->lastMinutes();
        $datas['superdeal'] = $this->superdeal();

        // If ajax request
        if ($this->request->is_ajax()) {
            $this->ajax();
        }

        // $this->setTemplate($datas,$page,'manage', $type);
        $this->template->main = Kostache::factory('promotionpage/manage')
                ->set('notice', Notice::render())
                ->set('datas', $datas)
                ->set('page', $page)
                ->set('type', $type)
                ->set('hotels', $datas['hotels']);
    }

    public function action_add() {
        //selecttion page
        try {
            $type = $this->request->query('type');
            //get logged admin
            $logged_in_admin = A1::instance()->get_user();
            //get hotel manage by get logged admin 
            $datas['hotels'] = json_encode(DB::select(
                                    array('hotel_texts.name', 'label'), array('hotel_texts.hotel_id', 'value')
                            )
                            ->from('hotel_texts')
                            ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotel_texts.hotel_id')
                            ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
                            ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                            ->order_by('name')
                            ->execute()
                            ->as_array()
            );

            //if submit
            if ($this->request->post('submit')) {
                Database::instance()->begin();

                $dataToInsert = $this->request->post();

                $values = array(
                    'hotel_id' => $dataToInsert['hotel_code'],
                    'campaign_id' => $dataToInsert['campaign_id'],
                    'room_id' => $dataToInsert['room_id'],
                    'type' => $type
                );

                $promotionpage = ORM::factory('PromotionPage')
                        ->where('campaign_id', '=', $values['campaign_id'])
                        ->where('room_id', '=', $values['room_id'])
                        ->where('hotel_id', '=', $values['hotel_id'])
                        ->where('type', '=', $values['type'])
                        ->find();

                //check if data is exist
                if ($promotionpage->loaded()) {
                    throw new Kohana_Exception('Promotoin Page has already exists');
                }

                $promotionpage = ORM::factory('PromotionPage')
                        ->create_promotionpage($values);

                if ($promotionpage->id) {
                    //commit database
                    Database::instance()->commit();

                    //add notice
                    Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));

                    // Redirect to edit
                    $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotionpage', 'action' => 'manage')));
                }
            }

            //set tamplate
            $page = '';
        } catch (Kohana_Exception $e) {
            //roleback datasabaes
            Database::instance()->rollback();
            //add notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed', NULL, $e->getMessage()));
        }

        $this->template->main = Kostache::factory('promotionpage/add')
                ->set('notice', Notice::render())
                ->set('datas', $datas)
                ->set('page', $page)
                ->set('type', $type)
                ->set('hotels', $datas['hotels']);
    }

    public function action_edit() {
        $promotionpage_id = (int) $this->request->param('id');
        $type = $this->request->query('type');
        $datapromotion = ORM::factory('promotionpage')
                ->where('id', '=', $promotionpage_id)
                ->find();

        //get campaigns 
        $campaign = ORM::factory('campaign', $datapromotion->campaign_id);

        //add name campaign
        $campaign = $this->nameCampaign($campaign);
        $campaign['type'] = $type;
        //get hotel
        $hotel_promotion = ORM::factory('hotel')
                ->select('hotel_texts.*')
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('hotels.id', '=', $datapromotion->hotel_id)
                ->find();
        //get room
        $room_promotion = ORM::factory('room')
                ->select('room_texts.*')
                ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                ->where('rooms.id', '=', $datapromotion->room_id)
                ->find();

        //get logged admin
        $logged_in_admin = A1::instance()->get_user();
        //get hotel manage by get logged admin 
        $hotels = json_encode(DB::select(
                                array('hotel_texts.name', 'label'), array('hotel_texts.hotel_id', 'value')
                        )
                        ->from('hotel_texts')
                        ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotel_texts.hotel_id')
                        ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
                        ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                        ->order_by('name')
                        ->execute()
                        ->as_array()
        );

        //if submit
        if ($this->request->post('submit')) {
            $dataToInsert = $this->request->post();

            $values = array(
                'id' => $promotionpage_id,
                'hotel_id' => $dataToInsert['hotel_code'],
                'campaign_id' => $dataToInsert['campaign_id'],
                'room_id' => $dataToInsert['room_id'],
                'type' => $type
            );

            //get promotion exists
            $promotionpage = ORM::factory('PromotionPage')
                    ->where('campaign_id', '=', $values['campaign_id'])
                    ->where('room_id', '=', $values['room_id'])
                    ->where('hotel_id', '=', $values['hotel_id'])
                    ->where('type', '=', $values['type'])
                    ->find();


            $promotionpage = ORM::factory('promotionpage')
                    ->where('id', '=', $promotionpage_id)
                    ->find();

            $promotionpage->update_promotionpage($values);

            if ($promotionpage) {
                //commmit databse
                Database::instance()->commit();

                //add notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));

                //redirect
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotionpage', 'action' => 'edit', 'id' => $promotionpage->id)) . '?type=' . $type);
            }
        }



        // $this->setTemplate($datas,$page,'edit');
        $this->template->main = Kostache::factory('promotionpage/edit')
                ->set('notice', Notice::render())
                ->set('campaign', $campaign)
                ->set('hotel_promotion', $hotel_promotion)
                ->set('room_promotion', $room_promotion)
                ->set('type', $type)
                ->set('hotels', $hotels);
    }

    //call in method action_manage()
    // for process delete
    public function action_delete($promotionpage_id = NULL, $type = NULL, $opt_param = NULL) {

        if ($promotionpage_id == NULL) {
            $promotionpage_id = (int) $this->request->param('id');
        }

        if ($type == NULL) {
            $type = $this->request->query('type');
        }

        $promotionpage = ORM::factory('promotionpage', $promotionpage_id);

        switch ($opt_param) {
            case 'action_manage':
                if ($promotionpage->delete()) {
                    //add notice
                    Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
                } else {
                    //add notice
                    Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_failed'));
                }
                break;

            default:
                if ($promotionpage->delete()) {
                    //add notice
                    Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));

                    // Redirect to manage
                    $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotionpage', 'action' => 'manage')) . '?type=' . $type);
                } else {
                    //add notice
                    Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_failed'));

                    //redirect to manage
                    $this->request->redirect(Route::get('default')->uri(array('controller' => 'promotionpage', 'action' => 'manage')) . '?type=' . $type);
                }
                break;
        }
    }

    //proceess page lastMenutes
    //call in method aciton_manage(), aciton_add(), aciton_edit() 
    private function lastMinutes($id = NULL) {
        $promotions = DB::select()
                ->from('promotionpages')
                ->where('type', '=', 3);

        if ($id > 0) {
            $promotions = $promotions
                    ->where('id', '=', $id);
        }

        $promotions = $promotions
                ->order_by('number_list')
                ->execute()
                ->as_array()
        ;

        foreach ($promotions as $promotionk => $promotion) {
            //getAllData
            $datas[$promotionk] = $this->getAllData($promotion);
        }

        if (empty($datas)) {
            $datas = array();
        }

        return $datas;
    }

    //proceess page lastMenutes
    //call in method aciton_manage(), aciton_add(), aciton_edit() 
    private function superdeal($id = NULL) {
        $promotions = DB::select()
                ->from('promotionpages')
                ->where('type', '=', 1);

        if ($id > 0) {
            $promotions = $promotions
                    ->where('id', '=', $id);
        }

        $promotions = $promotions
                ->order_by('number_list')
                ->execute()
                ->as_array()
        ;

        foreach ($promotions as $promotionk => $promotion) {
            //getAllData
            $datas[$promotionk] = $this->getAllData($promotion);
        }

        if (empty($datas)) {
            $datas = array();
        }

        return $datas;
    }

    //proceess page earlybird
    //call in method aciton_manage(), aciton_add(), aciton_edit() 
    private function earlybird($id = NULL) {
        $promotions = DB::select()
                ->from('promotionpages')
                ->where('type', '=', 2);

        if ($id > 0) {
            $promotions = $promotions
                    ->where('id', '=', $id);
        }

        $promotions = $promotions
                ->order_by('number_list')
                ->execute()
                ->as_array();

        foreach ($promotions as $promotionk => $promotion) {
            //getAllData
            $datas[$promotionk] = $this->getAllData($promotion);
        }

        if (empty($datas)) {
            $datas = array();
        }

        return $datas;
    }

    //call in method elarlyBird(),lastMinutes()
    //get all data for prosessing
    private function getAllData($promotion) {
        $hotel = Model::factory('hotel')->get_hotel_by_id($this->selected_language->id, $promotion['hotel_id']);
        $room = Model::factory('room')->get_by_id($this->selected_language->id, $promotion['room_id']);
        $campaign = DB::select('campaigns.*')
                ->from('campaigns')
                ->where('campaigns.id', '=', $promotion['campaign_id'])
                ->execute()
                ->current();

        return array(
            'hotel' => $hotel,
            'room' => $room,
            'campaign' => $campaign,
            'promotion' => $promotion
        );
    }

    //for proccess name of camapaign
    //call in method action_manage()
    private function nameCampaign($campaign) {
        $benefit = '';

        if ($campaign->discount_rate > 0) {
            $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
                ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                ':discount_rate' => $campaign->discount_rate,
            ));
        } elseif ($campaign->discount_amount > 0) {
            $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
                ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                ':currency_code' => $currency->code,
                ':discount_amount' => $campaign->discount_amount,
            ));
        } elseif ($campaig->number_of_free_nights) {
            $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
                ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                ':number_of_free_nights' => $campaign->number_of_free_nights,
            ));
        }

        if ($campaign->type == 2) {
            $benefit .= ' ' . __('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
        } elseif ($campaign->type == 3) {
            $benefit .= ' ' . __('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
        } elseif ($campaign->type == 5) {
            $benefit .= ' ' . __('Flash Deal.');
        }

        return array(
            'id' => $campaign->id,
            'name' => $benefit,
        );
    }

    //process for request ajax
    //call in method action_manage().
    private function ajax() {
        $type = $_GET['type_request'];
        $id = $_GET['id'];
        $type_campaign = $_GET['type_campaign'];

        if ($type_campaign == 1) {
            $type_campaign = array(1, 5);
        } else {
            $type_campaign = array($type_campaign);
        }

        if ($type == 'campaign') {
            $campaigns = Model::factory('campaign')->getAllCampaignsHotel($id, array('call' => 'promotion_page', 'type' => $type_campaign));

            foreach ($campaigns as $campaignk => $campaign) {
                $data[$campaignk] = $this->nameCampaign((object) $campaign);
            }
        } elseif ($type == 'room') {

            $rooms = DB::select()
                    ->from('rooms')
                    ->join('campaigns_rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
                    ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                    ->where('campaigns_rooms.campaign_id', '=', $id)
                    ->where('room_texts.language_id', '=', $this->selected_language->id)
                    ->group_by('rooms.id')
                    ->execute()
                    ->as_array()
            ;

            foreach ($rooms as $roomk => $room) {
                $data[$roomk] = array(
                    'id' => $room['room_id'],
                    'name' => $room['name']
                );
            }
        }

        echo json_encode($data);
        exit();
    }

}
