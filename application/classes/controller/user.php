<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Layout_Admin {

    public function before() {
        if ($this->request->action() == 'delete') {
            $this->auto_render = FALSE;
        }

        parent::before();
    }

    public function action_manage() {
        // Is Authorized ?
        if (!A2::instance()->allowed('user', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        $field = $this->request->query('field');
        $keyword = $this->request->query('keyword');

        $page = Arr::get($this->request->query(), 'page', 1);
        $items_per_page = 30;
        $offset = ($page - 1) * $items_per_page;

        // Get users
        $users = ORM::factory('user')
                        ->select(
                                array('languages.name', 'language_name')
                        )
                        ->join('languages')->on('languages.id', '=', 'users.language_id');

        if ($keyword) {
            if ($field == 1) {
                $users->where('email', '=', $keyword);
            } elseif ($field == 2) {
                $users->where('first_name', 'LIKE', "%$keyword%");
            } elseif ($field == 3) {
                $users->where('last_name', 'LIKE', "%$keyword%");
            }
        }

        // Get total users
        $total_users = $users
                ->reset(FALSE)
                ->count_all();

        // Get users
        $users = $users
                ->offset($offset)
                ->limit($items_per_page)
                ->find_all();

        // Create pagination
        $pagination = Pagination::factory(array(
                    'items_per_page' => $items_per_page,
                    'total_items' => $total_users,
        ));

        $this->template->main = Kostache::factory('user/manage')
                ->set('notice', Notice::render())
                ->set('filters', $this->request->query())
                ->set('users', $users)
                ->set('pagination', $pagination);
    }

    public function action_delete() {
        $user_id = $this->request->param('id');

        if (A2::instance()->allowed('user', 'delete')) {
            try {
                // Delete user
                DB::delete('users')
                        ->where('id', '=', $user_id)
                        ->execute();

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
            } catch (Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
            }
        } else {
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
        }

        // Redirect back
        $this->request->redirect($this->request->referrer());
    }

    public function action_unsubscribe() {
        // Is Authorized ?
        if (!A2::instance()->allowed('user', 'unsubscribe')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        $user_id = $this->request->param('id');

        try {
            // Unsubscribe newsletter
            ORM::factory('user')
                    ->where('id', '=', $user_id)
                    ->find()
                    ->values(array(
                        'is_subscribe_newsletter' => 0,
                    ))
                    ->update();

            Notice::add(Notice::SUCCESS, Kohana::message('user', 'unsubscribe_success'));
        } catch (Exception $e) {
            Notice::add(Notice::ERROR, $e->getMessage());
        }

        $this->request->redirect($this->request->referrer());
    }

}
