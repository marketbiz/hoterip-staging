<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Trivago_Availability extends Controller 
{

  /*
  *List Frontend Model
  * Hotel :search_hotel, search_campaign
  * Room capacity :gerroom, checkroom, extrabed
  * Country/City  : getbysegment
  * Timezone  : timezone identifier
  * Setting  : get
  * Exchane  : To
  */
  public function before()
  {
    parent::before();
  }

  public function errors($errors)
  {
    $error_list = array();
  
    foreach ($errors as $key_array => $error) {
      $error_list['error'] = $error;
    }

    $error_list = json_encode($error_list);

    echo $error_list;
    die();
  }

  public function get_xml_single($hotel_query)
  {
    //Server Name
    $server_name = Controller_Api_Request::base_url(URL::base(true));
    // Factory exchange
    $exchange = Model::factory('exchange');

    $stay_night = Date::span(strtotime($hotel_query['check_out']), strtotime($hotel_query['check_in']), 'days');

    // Get hotel ids
    $hotel_ids = array($hotel_query['single_hotel'] => $hotel_query['single_hotel']);
    
    $response = array(
      'key' => $hotel_query['key'],
      // 'CheckIn' => $hotel_query['check_in'],
      // 'CheckOut' => $hotel_query['check_out'],
      // 'Adult' => $hotel_query['capacities'],
      // 'Children' => $hotel_query['children'],
      // 'NumberRooms' => $hotel_query['number_of_rooms'],
      // 'Language' => 'en',
      // 'Currency' => $hotel_query['CurrencyCode'],
      );

    foreach ($hotel_query['rooms'] as $room_id) 
    {
      $hotel_query['InvTypeCode'] = $room_id;
      // Get campaigns in the hotel_ids
      $campaigns = Model::factory('hotel')->search_campaigns($hotel_ids, $hotel_query);

      //Check Campaign exist
      if(count($campaigns))
      {
        // Set cheapest campaign values
        $hotel_campaigns =array();

        // First value hotel
        $campaign_start = reset($campaigns);
        $hotel_campaigns[$campaign_start['hotel_id']] = $campaign_start;

        foreach ($campaigns as $value)
        {
          // Get hotel id
          $key = $value['hotel_id'];

          // Set first appear hotel
          if (!isset($hotel_campaigns[$key]['campaign_average_price']))
          {
            $hotel_campaigns[$key] = $value;
          }
          else
          {
            // Compare price
            if ($hotel_campaigns[$key]['campaign_average_price'] > $value['campaign_average_price'])
            {
              $hotel_campaigns[$key] = $value;
            }
          }
        }

        // counter
        $i=0;

        foreach($hotel_campaigns as $key => $room)
        {
          //Generate XML Rooms
          $roomID = $room['room_id'];

          $promotion_exist = FALSE;

          //Determine room available
          $hotel_no_arrival = DB::select()
              ->from('hotel_no_arrivals')
              ->where('hotel_id', '=', $room['hotel_id'])
              ->where('date', '=', $hotel_query['check_in'])
              ->execute()
              ->get('date');

          // Get no departure
          $hotel_no_departure = DB::select()
            ->from('hotel_no_departures')
            ->where('hotel_id', '=', $room['hotel_id'])
            ->where('date', '=', $hotel_query['check_out'])
            ->execute()
            ->get('date');

          $Onrequest = TRUE;

          // Check campaign prices exist
          $room['campaign_average_price'] = isset($room['campaign_average_price']) ? $room['campaign_average_price'] : 0;

          // Determine if guest stay night more than minimum night
          $minimum_night_available = $stay_night >= $room['max_minimum_night'];
          if ($room['campaign_average_price'] AND ! $hotel_no_arrival AND ! $hotel_no_departure AND $minimum_night_available)
          {
            if(($stay_night >= $minimum_night_available) && ($stay_night >= $room['campaign_min_nights']) &&($hotel_query['number_of_rooms'] >= $room['campaign_min_rooms']))
            {
              $Onrequest = FALSE;
            }

            if(($stay_night <= $minimum_night_available) && ($stay_night >= $room['campaign_min_nights']) &&($hotel_query['number_of_rooms'] >= $room['campaign_min_rooms']))
            {
              $promotion_exist = TRUE;
            }
          }

          //Determine Promo
          $stay = '';
          $promo = '';

          if($promotion_exist AND $room['campaign_default'] == 0)
          {
            $stay = '';

            if($room['last_days'] > 0)
            {
              $room['last_days_date'] = Date::formatted_time("+{$room['last_days']} days", __('M j, Y'), $room['hotel_timezone']);
              $last_days =$room['last_days_date'];
              $last_days = Date::formatted_time("$last_days", 'm-d-Y', $room['hotel_timezone']);

              if ($hotel_query['check_in'] <= $last_days AND $hotel_query['check_out'] <= $last_days) {
                $Onrequest = FALSE;
              }
              elseif ($hotel_query['check_in']  < $last_days AND $hotel_query['check_out'] >= $last_days) {
                $Onrequest = FALSE;
              }
              else
              {
                $Onrequest = TRUE;
              }

              $stay .= 'Book before '.Date::formatted_time("+{$room['last_days']} days", __('M j, Y'), $room['hotel_timezone']).', ';
            }

            elseif($room['early_days'] > 0)
            {
              $early_days = $room['early_days'] - 1;
              $room['early_days_date'] = Date::formatted_time("+$early_days days", __('M j, Y'), $room['hotel_timezone']);

              $early_days =$room['early_days_date'];
              $early_days = Date::formatted_time("$early_days", 'm-d-Y', $room['hotel_timezone']);

              if($hotel_query['check_in'] <= $early_days AND $hotel_query['check_out'] <= $early_days)
              {
                $Onrequest = TRUE;
              }
              elseif ($hotel_query['check_in'] < $early_days AND $hotel_query['check_out'] >= $early_days) {
                 $Onrequest = FALSE;
              }
              elseif ($hotel_query['check_in'] > $early_days AND $hotel_query['check_out'] >= $early_days) {
                 $Onrequest = FALSE;
              }

              $stay .= 'Book after '.Date::formatted_time("+$early_days days", __('M j, Y'), $room['hotel_timezone']).', ';
            }

            elseif($room['rate'] > 0)
            {
              $stay .= 'Discount '.$room['rate'].' %, ';
            }
            elseif($room['amount'] > 0)
            {
              $stay .= 'Discount '.$room['rate'].self::selected_currency($hotel_query['currency_code'])->code.' '.$room['amount'].', ';
            }

            $stay .= 'Stay at least '.$room['campaign_min_nights'].' nights and book '.$room['campaign_min_rooms'].' or more rooms';
          }
          else
          {
            $promo = 'Default Promotions';
          }

          $promotion_exist = $promotion_exist ? $promo.' '.$stay : NULL;

          $service_tax_rate = (($room['hotel_service_charge_rate'] + $room['hotel_tax_rate']) +100) /100;
          
          $RatePerNight_amount = $room['campaign_average_price'] != 0 ?($room['campaign_average_price'])
              * ($exchange->to
          ($room['currency_id'], $hotel_query['currency_code'])) : 0 ;
          $RatePerNight_net_amount = $RatePerNight_amount / $service_tax_rate;

        $url_key = base64_encode($room['campaign_id'].','.$room['room_id']
          .','.$room['hotel_id'].','.$hotel_query['check_in'].','.$hotel_query['check_out']
          .','.$hotel_query['number_of_rooms'].','.$hotel_query['capacities'].','.$hotel_query['language_id'].','.$hotel_query['currency_code']);        

          $url = $server_name.$room['country_segment'].'/'.$room['city_segment'].'/'.$room['hotel_segment'].'?vendorkey='.$url_key;

          // Cancellations
          $cancellations = Controller_Vacation_Static::cancellations($room['campaign_id'], $room['hotel_id']);

          // Rate
          if(!empty($url) AND ($promotion_exist) AND ($room['campaign_average_price']))
          {
            $response['link'] = $url;
            $response['HotelName'] = $room['hotel_name'];
            $response['Rooms'][$i]['ID'] = $roomID;
            $response['Rooms'][$i]['RoomDescription'] = $room['room_name'];
            $response['Rooms'][$i]['RateDescription'] = $promotion_exist;
            $response['Rooms'][$i]['Rate'] = number_format(round($RatePerNight_amount));
            $response['Rooms'][$i]['NetRate'] = number_format(round($RatePerNight_net_amount));
            $response['Rooms'][$i]['Tax'] = number_format(round($RatePerNight_amount - $RatePerNight_net_amount));
            $response['Rooms'][$i]['Breakfast'] = $room['breakfast'];
            $response['Rooms'][$i]['BreakfastPrice'] = $room['breakfast_price'];
            $response['Rooms'][$i]['FreeCancellation'] = $cancellations['first']['within_days'] == 1000 ? 1 : 0 ;
            $response['Rooms'][$i]['CancellationDeadline'] = $cancellations['first']['within_days'] == 1000 ? 0 : 1;
          }
          else
          {
            $response['Rooms'][$room['room_name']] = $room['Empty Data'];
          }
          $i++;
          $response['Check-in'] = $hotel_query['check_in'];
          $response['Check-out'] = $hotel_query['check_out'];
          $response['Adult'] = $hotel_query['capacities'];
          $response['Children'] = $hotel_query['children'];
          $response['NumberRooms'] = $hotel_query['number_of_rooms'];
          $response['Language'] = $hotel_query['language_code'];
          $response['Currency'] = $hotel_query['CurrencyCode'];
        }
      }
      // else
      // {
      //   $this->errors($errors = array('321'=>array('Code' => 2,'Type'=>425,'Desc' => 'No data found')));
      // }
    }

    $response = json_encode($response);

    return $response;
  }

  public function get_xml($hotel_query)
  {
    //Server Name
    $server_name = Controller_Api_Request::base_url(URL::base(true));
    // Factory exchange
    $exchange = Model::factory('exchange');    

    $stay_night = Date::span(strtotime($hotel_query['check_out']), strtotime($hotel_query['check_in']), 'days');

    if(!empty($hotel_query['hotel_ids']))
    {
      // Get hotel ids
      $hotel_ids = $hotel_query['hotel_ids'];

      $hotels = Model::factory('hotel')->search_hotels_limit($hotel_ids, $hotel_query);
    }
    else
    {
      $hotel_ids = NULL;

      $hotels = Model::factory('hotel')->search_hotels_limit($hotel_ids, $hotel_query);

      // Get hotel ids
      $hotel_ids = array_keys($hotels);
    }
    
    // Get campaigns in the hotel_ids
    $campaigns = Model::factory('hotel')->search_campaigns($hotel_ids, $hotel_query);

    //Check Campaign exist
    if(count($campaigns))
    {
      // Set cheapest campaign values
      $hotel_campaigns =array();

      // First value hotel
      $campaign_start = reset($campaigns);
      $hotel_campaigns[$campaign_start['hotel_id']] = $campaign_start;

      foreach ($campaigns as $value)
      {
        // Get hotel id
        $key = $value['hotel_id'];
       
        // Set first appear hotel
        if (!isset($hotel_campaigns[$key]['campaign_average_price']))
        {
          $hotel_campaigns[$key] = $value;          
        }
        else
        {
          // Compare price
          if ($hotel_campaigns[$key]['campaign_average_price'] > $value['campaign_average_price'])
          {
            $hotel_campaigns[$key] = $value;
          }
        }
      }

      $response = array(
        'key' => $hotel_query['key'],
        // 'CheckIn' => $hotel_query['check_in'],
        // 'CheckOut' => $hotel_query['check_out'],
        // 'Adult' => $hotel_query['capacities'],
        // 'Children' => $hotel_query['children'],
        // 'NumberRooms' => $hotel_query['number_of_rooms'],
        // 'Language' => 'en',
        // 'Currency' => $hotel_query['CurrencyCode'],
        );
      
      $hotelDatas = array();
      foreach($hotel_campaigns as $key => $room)
      {
        //Generate XML Rooms
        $roomID = $room['room_id'];

        $promotion_exist = FALSE;

        //Determine room available
        $hotel_no_arrival = DB::select()
            ->from('hotel_no_arrivals')
            ->where('hotel_id', '=', $room['hotel_id'])
            ->where('date', '=', $hotel_query['check_in'])
            ->execute()
            ->get('date');

        // Get no departure
        $hotel_no_departure = DB::select()
          ->from('hotel_no_departures')
          ->where('hotel_id', '=', $room['hotel_id'])
          ->where('date', '=', $hotel_query['check_out'])
          ->execute()
          ->get('date');

        $Onrequest = TRUE;

        // Check campaign prices exist
        $room['campaign_average_price'] = isset($room['campaign_average_price']) ? $room['campaign_average_price'] : 0;

        // Determine if guest stay night more than minimum night
        $minimum_night_available = $stay_night >= $room['max_minimum_night'];
        if ($room['campaign_average_price'] AND ! $hotel_no_arrival AND ! $hotel_no_departure AND $minimum_night_available)
        {
          if(($stay_night >= $minimum_night_available) && ($stay_night >= $room['campaign_min_nights']) &&($hotel_query['number_of_rooms'] >= $room['campaign_min_rooms']))
          {
            $Onrequest = FALSE;
          }

          if(($stay_night <= $minimum_night_available) && ($stay_night >= $room['campaign_min_nights']) &&($hotel_query['number_of_rooms'] >= $room['campaign_min_rooms']))
          {
            $promotion_exist = TRUE;
          }
        }

        $service_tax_rate = (($room['hotel_service_charge_rate'] + $room['hotel_tax_rate']) +100) /100;

        $RatePerNight_amount = $room['campaign_average_price'] != 0 ?($room['campaign_average_price'])
              * ($exchange->to
          ($room['currency_id'], $hotel_query['currency_code'])) : 0 ;
        $RatePerNight_net_amount = $RatePerNight_amount / $service_tax_rate;

        $url_key = base64_encode($room['campaign_id'].','.$room['room_id']
          .','.$room['hotel_id'].','.$hotel_query['check_in'].','.$hotel_query['check_out']
          .','.$hotel_query['number_of_rooms'].','.$hotel_query['capacities'].','.$hotel_query['language_id'].','.$hotel_query['currency_code']);

        $url = $server_name.$room['country_segment'].'/'.$room['city_segment'].'/'.$room['hotel_segment'].'?vendorkey='.$url_key;
        //$url = $server_name.$room['country_segment'].'/'.$room['city_segment'].'/'.$room['hotel_segment'].'?language_id='.$hotel_query['language_id'].'&currency_id='.$hotel_query['currency_code'];

        // Cancellations
        $cancellations = Controller_Vacation_Static::cancellations($room['campaign_id'], $room['hotel_id']);
        // Rate belum
        if(!empty($url) AND ($promotion_exist) AND ($room['campaign_average_price']))
        {
          $hotelData = array();

          $hotelData['ID'] = $room['hotel_id'];
          $hotelData['HotelURL'] = $url;
          $hotelData['Rate'] = number_format(round($RatePerNight_amount));
          $hotelData['NetRate'] = number_format(round($RatePerNight_net_amount));
          $hotelData['Tax'] = number_format(round($RatePerNight_amount - $RatePerNight_net_amount));

          array_push($hotelDatas, $hotelData);
        }
      }
        $response['Hotels'] = $hotelDatas;
        $response['CheckIn'] = $hotel_query['check_in'];
        $response['CheckOut'] = $hotel_query['check_out'];
        $response['Adult'] = $hotel_query['capacities'];
        $response['Children'] = $hotel_query['children'];
        $response['NumberRooms'] = $hotel_query['number_of_rooms'];
        $response['Language'] = $hotel_query['language_code'];
        $response['Currency'] = $hotel_query['CurrencyCode'];

      $response = json_encode($response);

      return $response;
    }
    else
    {
      $this->errors($errors = array('321'=>array('Code' => 2,'Type'=>425,'Desc' => 'No data found')));
    }
  }

  public static function time_format($hotel_query)
  {
    if (Arr::get($hotel_query, 'check_in'))
    {
      try
      {
        $check_in = Date::formatted_time(Arr::get($hotel_query, 'check_in'), 'Y-m-d');
      }
      catch (Exception $e)
      {
        $error = TRUE;
      }
    }

    if (Arr::get($hotel_query, 'check_out'))
    {
      try
      {
        $check_out = Date::formatted_time(Arr::get($hotel_query, 'check_out'), 'Y-m-d');
      }
      catch (Exception $e)
      {
        $error = TRUE;
      }
    }

    if(empty($error))
    {
      $time = array(
        'check_in'=>$check_in,
        'check_out'=>$check_out,
        );
      $hotel_query = array_merge($hotel_query, $time);
    }
    else
    {
      $hotel_query = NULL;

    }
    return $hotel_query;
  }

    public function process($hotel_query)
  {
    try
    {
      // reformat time
      $hotel_query = $this->time_format($hotel_query);

      //Check Country 
      if(strtolower($hotel_query['country']) != 'indonesia'
        && strtolower($hotel_query['country']) != 'id')
      {
        $this->errors($errors = array('321'=>array('Code' => 2,'Type'=>427,'Desc' => 'Data Not Available')));
      }
      //Check Null request
      elseif($hotel_query['city'] == Null)
      {
        $this->errors($errors = array('321'=>array('Code' => 2,'Type'=>321,'Desc' => 'Required field missing')));
      }
      else
      {
        //Determine Country  
        if(strtolower($hotel_query['country']) == 'id')
          $hotel_query['country'] = 'indonesia';

        //Determine Currency
        if($hotel_query['currency'] == 'USD'
          ||$hotel_query['currency'] == 'JPY'
          ||$hotel_query['currency'] == 'IDR')
        {
          $hotel_query['currency_code'] = $hotel_query['currency'];
        }
        else
        {
          $hotel_query['currency_code'] = 'IDR';
        }

        //Determine currency id
        $currency_code = ORM::factory('currency')
          ->where('code', '=', $hotel_query['currency_code'])
          ->find()->as_array();

        $hotel_query['currency_code'] = $currency_code['id'];

        $country = Model::factory('country')->get_by_segment(Controller_Api_Request::default_language(), $hotel_query['country']);

        $city = Model::factory('city')->get_by_segment(Controller_Api_Request::default_language(), $hotel_query['city']);

        switch (true) {
          case (empty($country)):
            $this->errors($errors = array('321'=>array('Code' => 2,'Type'=>321,'Desc' => 'Invalid Country')));
            break;

          case (empty($city)):
            $this->errors($errors = array('321'=>array('Code' => 2,'Type'=>321,'Desc' => 'Invalid City')));
            break;

          default:
            $place = array(
              'country_id' => $country->id, 
              'city_id' => $city->id,
              );

            $hotel_query = array_merge($place, $hotel_query);

            if(!empty($hotel_query['single_hotel']))
            {
              $response = $this->get_xml_single($hotel_query);
            }
            else
            {
              $response = $this->get_xml($hotel_query);
            }
            
            return $response;
            break;
        }
      }
    }
    catch (Kohana_Exception $e)
    {
      $this->errors($errors = array('321'=>array('Code' => 450,'Type'=>321,'Desc' => 'Unable to process')));
    }
  }

  public function action_request()
  {
    try
    {
      $xml_data = trim(file_get_contents('php://input'));

      Log::instance()->add(Log::INFO, 'Trivago Request: '.$xml_data);

      header('Content-type: text/json');

      if ( ! $xml_data)
      {
        $this->errors($errors = array('448'=>array('Code' => 3,'Type'=>427,'Desc' => 'Data is empty')));
      }
      else
      {
        // Check Auth API KEY or IP

        if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
        $ip = getenv("HTTP_CLIENT_IP");
        else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
        $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
        $ip = getenv("REMOTE_ADDR");
        else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
        $ip = $_SERVER['REMOTE_ADDR'];
        else
        $ip = "unknown";

        $ip = str_replace(' ', '', $ip);

        // Log Request
        $request_time = date("Y-m-d H:i:s");
        Log::instance()->add(Log::INFO, 'Trivago - Request '.$request_time.' - From: '.$ip);
        Log::instance()->add(Log::INFO, 'Trivago - Request '.$request_time.' - Data : '.$xml_data);

        $ip = explode(',', $ip);

        $ip_preg_match = reset($ip);

        preg_match('/194.153.186.[0-2][0-9]?[0-9]?/', $ip_preg_match, $matches1);
        preg_match('/8.35.179.[0-2][0-9]?[0-9]/', $ip_preg_match, $matches2);

        // Check IP
        if (in_array("175.41.150.124", $ip)
          || in_array("122.248.251.172", $ip)
          || in_array("122.248.235.26", $ip)
          || in_array("202.138.247.73", $ip)
          || in_array("192.168.20.63", $ip)
          || in_array("119.81.12.85", $ip)
          || in_array("87.139.113.39", $ip)
          || in_array("127.0.0.1", $ip)
          || in_array("192.168.91.1", $ip) // local
          || $matches1
          || $matches2
          ) 
        {
          $doc = (array)json_decode($xml_data);
          
          $timezone = 'Asia/Kuala_Lumpur';

          $hotel_query = array(
            'key' => $doc['Key'],
            'language_code' => $doc['Language'],
            'country' => !empty($doc['CountryCode']) ? $doc['CountryCode'] : 'id',
            'city' => !empty($doc['City']) ? $doc['City'] : '',
            'check_in' => !empty($doc['CheckIn']) ? $doc['CheckIn'] : Date::formatted_time('now', 'Y-m-d', Controller_Api_Request::selected_timezone($timezone)),
            'check_out' => !empty($doc['CheckOut']) ? $doc['CheckOut'] : Date::formatted_time("now +1 day", 'Y-m-d'),
            'number_of_rooms' => !empty($doc['NumberRooms']) ? $doc['NumberRooms'] : 1,
            'capacities' => !empty($doc['Adult']) ? $doc['Adult'] : 1,
            'children' => !empty($doc['Children']) ? $doc['Children'] : 1,
            'currency' => !empty($doc['Currency']) ? $doc['Currency'] : 'IDR',
            'CurrencyCode' => !empty($doc['Currency']) ? $doc['Currency'] : 'IDR',
            'timezone' => $timezone,
            'now' => Date::formatted_time('now', 'Y-m-d', Controller_Api_Request::selected_timezone($timezone))
            );

          if($hotel_query['language_code']=='en')
            $hotel_query['language_id'] = 1;
          elseif($hotel_query['language_code']=='id')
            $hotel_query['language_id']= 3;
          elseif ($hotel_query['language_code']=='jp')
            $hotel_query['language_id']= 2;
          
          // For Hotel ID request
          if(!empty($doc['Hotels']))
          {
            $hotel_id = $doc['Hotels'];

            if(count($hotel_id) > 1)
            {
              $hotel_query['hotel_ids'] = $hotel_id;
            }
            else
            {
              $hotel_query['hotel_ids'] = $hotel_id;
            }

            // Add Default City to pass city validation
            $hotel_query['city'] = 'Bali';
          }
          elseif (!empty($doc['Hotel_ID']))
          {
            $hotel_query['single_hotel'] = $doc['Hotel_ID'];
            $rooms = $doc['Rooms'];

            if(count($rooms) > 1)
            {
              $hotel_query['rooms'] = $rooms;
            }
            else
            {
              $hotel_query['rooms'] = $rooms;
            }

            // Add Default City to pass city balidation
            $hotel_query['city'] = 'Bali';
          }

          // Procces Function
          $hotel_query = $this->process($hotel_query);
          
          echo $hotel_query;
          Log::instance()->add(Log::INFO, 'Trivago - Response '.$request_time.' : '.$hotel_query);

          die();
        }
        else
        {
          $this->errors($errors = array('4'=>array('Code' => 497,'Type'=>4,'Desc' => 'Authorization errors')));
          Log::instance()->add(Log::INFO, 'Trivago - Response '.$request_time.' : Authorization errors');
        }
      }
    }
    catch (Kohana_Exception $e)
    {
      $this->errors($errors = array('321'=>array('Code' => 450,'Type'=>321,'Desc' => 'Unable to process')));
    }
  }
}