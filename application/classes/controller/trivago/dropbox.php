<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Trivago_Dropbox extends Controller {

  public function action_csv()
  {
    // Check Auth API KEY or IP

    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
    $ip = getenv("HTTP_CLIENT_IP");
    else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
    $ip = getenv("HTTP_X_FORWARDED_FOR");
    else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
    $ip = getenv("REMOTE_ADDR");
    else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
    $ip = $_SERVER['REMOTE_ADDR'];
    else
    $ip = "unknown";

    $ip = str_replace(' ', '', $ip);
    $ip = explode(',', $ip);

    $ip_preg_match = reset($ip);

    preg_match('/194.153.186.[0-2][0-9]?[0-9]?/', $ip_preg_match, $matches1);
    preg_match('/8.35.179.[0-2][0-9]?[0-9]/', $ip_preg_match, $matches2);

    // Check IP
    if (in_array("217.79.217.198", $ip)
      // || in_array("84.254.126.65", $ip)
      // || in_array("192.168.20.63", $ip)
      // || in_array("119.81.12.85", $ip)
      // || in_array("87.139.113.39", $ip)
      // || in_array("202.138.247.73", $ip)
      // || in_array("127.0.0.1", $ip)
      || $matches1
      || $matches2
      )
    {
      $datas = array();
      $directory = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/dropbox/csv/';

      if ($handle = opendir($directory)) {

        while (false !== ($entry = readdir($handle))) {
          if ($entry != "." && $entry != "..") {
            $datas[] = "$entry\n";
          }
        }

        closedir($handle);
      }

      $view = Kostache::factory('api/csv')
        ->set('notice', Notice::render())
        ->set('datas', $datas);
      $this->response->body($view);
    }
    else
    {
      if ($this->request->post('submit'))
      {
        if (A1::instance()->login($this->request->post('username'), $this->request->post('password'), (bool) $this->request->post('remember_me')))
        {
          Log::instance()->add(Log::INFO, "Trivago request");
          Log::instance()->add(Log::INFO, 'user: '.$this->request->post('username'));
          Log::instance()->add(Log::INFO, 'ip: '.$ip_preg_match);

          $datas = array(); 
          $directory = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/dropbox/csv/';  

          if ($handle = opendir($directory)) 
          {  
            while (false !== ($entry = readdir($handle))) 
            {
              if ($entry != "." && $entry != "..") 
              {  
                $datas[] = "$entry\n";  
              }
            } 
            closedir($handle);  
          } 

          // auto logout
          A1::instance()->logout();

          $view = Kostache::factory('api/csv')  
            ->set('notice', Notice::render()) 
            ->set('datas', $datas); 
            $this->response->body($view);
        }
        else
        {
          echo "Forbidden";
          die();
        }
      }
      else
      { 
        $view = Kostache::factory('auth/login') 
          ->set('notice', Notice::render()) 
          ->set('values', $this->request->post());

        $this->response->body($view);
      }
    }
  }

  public function action_download()
  {
    $directory = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/dropbox/csv/';
    $file = $directory .''. $_GET['name'];
    
    $file_name = urlencode($file);
    $file_name = explode("%252F", urlencode($file_name));
    $file_name = end($file_name);

    if (file_exists($file))
    {
      if (FALSE!== ($handler = fopen($file, 'r')))
      {
        header("Content-Disposition: attachment; filename=" . $file_name);    
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");             
        header("Content-Length: " . filesize($file));
        flush(); // this doesn't really matter.

        $fp = fopen($file, "r"); 
        while (!feof($fp))
        {
            echo fread($fp, 65536); 
            flush(); // this is essential for large downloads
        }  
        fclose($fp); 
        exit();
      }
    }
      echo "<h1>Content error</h1><p>The file does not exist!</p>";
    }
}