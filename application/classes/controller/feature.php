<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Feature extends Controller_Layout_Admin {
  
  public function before()
	{
		if (in_array($this->request->action(), array('delete')))
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
  
  public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('feature', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		$features = ORM::factory('feature')
      ->select(
        array('hotel_texts.name', 'hotel_name'),
        array('hotels.url_segment', 'hotel_url_segment'),
        array('hotels.basename', 'hotel_basename'),
        array('languages.code', 'language_code'),
        array('languages.name', 'language_name'),
        array('currencies.code', 'currency_code')
      )
      ->join('hotels')->on('hotels.id', '=', 'features.hotel_id')
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('languages')->on('languages.id', '=', 'features.language_id')
      ->join('currencies')->on('currencies.id', '=', 'features.currency_id')
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
			->find_all();
		
		$this->template->main = Kostache::factory('feature/manage')
			->set('notice', Notice::render())
			->set('features', $features);
	}
  
  public function action_add()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('feature', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    if ($this->request->post('submit'))
    {
      try
      {
        // Create feature
        $feature = ORM::factory('feature')
          ->values($this->request->post(), array(
            'language_id',
            'hotel_id',
            'currency_id',
            'description',
            'price',
          ))
          ->create();
        
        // Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'feature', 'action' => 'edit', 'id' => $feature->id)));
      }
      catch (Exception $e)
      {
        // Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, array($e->getMessage()));
      }
    }
    
    // Get all hotels
    $hotels = ORM::factory('hotel')
      ->select(
        array('hotel_texts.name', 'name'),
        array('city_texts.name', 'city_name'),
        array('country_texts.name', 'country_name')
      )
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('cities')->on('cities.id', '=', 'hotels.city_id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('hotels.active', '=', 1)
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name', 'ASC')
      ->find_all();
    
    $languages = ORM::factory('language')
      ->find_all();
    
    $currencies = ORM::factory('currency')
      ->find_all();
    
    $this->template->main = Kostache::factory('feature/add')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
			->set('hotels', $hotels)
			->set('languages', $languages)
			->set('currencies', $currencies);
  }
  
  public function action_edit()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('feature', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    // Get feature id
    $feature_id = $this->request->param('id');
    
    if ($this->request->post('submit'))
    {
      try
      {
        // Find feature
        $feature = ORM::factory('feature')
          ->where('id', '=', $feature_id)
          ->find();
        
        if ($feature->loaded())
        {
          // Update feature
          $feature->values($this->request->post(), array(
            'language_id',
            'hotel_id',
            'currency_id',
            'description',
            'price',
          ))
          ->update();
        
          // Add success notice
          Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        }
        else
        {
          throw new Exception('Feature not found');
        }
      }
      catch (Exception $e)
      {
        // Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->getMessage());
      }
    }
    
    $feature = ORM::factory('feature')
      ->where('id', '=', $feature_id)
      ->find();
    
    // Get all hotels
    $hotels = ORM::factory('hotel')
      ->select(
        array('hotel_texts.name', 'name'),
        array('city_texts.name', 'city_name'),
        array('country_texts.name', 'country_name')
      )
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('cities')->on('cities.id', '=', 'hotels.city_id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('hotels.active', '=', 1)
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name', 'ASC')
      ->find_all();
    
    $languages = ORM::factory('language')
      ->find_all();
    
    $currencies = ORM::factory('currency')
      ->find_all();
    
    $this->template->main = Kostache::factory('feature/edit')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('feature', $feature)
      ->set('hotels', $hotels)
      ->set('languages', $languages)
      ->set('currencies', $currencies)
			->set('languages', $languages);
  }
  
  public function action_delete()
  {
    // Get feature id
		$feature_id = $this->request->param('id');
		
		if (A2::instance()->allowed('feature', 'delete'))
		{
			try
			{
				// Delete feature
				DB::delete('features')
					->where('id', '=', $feature_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
    
    $this->request->redirect($this->request->referrer());
  }
  
}