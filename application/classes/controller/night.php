<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Night extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
    
		parent::before();
    
    if ($this->auto_render)
    {
      // Calculate number of rooms
      $number_of_rooms = ORM::factory('room')
        ->where('hotel_id', '=', $this->selected_hotel->id)
        ->count_all();

      // If there is no rooms
      if ($number_of_rooms <= 0)
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('item', 'no_room'));
        // Redirect to manage campaign
        $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
      }
    }
	}
  
  public function action_manage()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('night', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    if ($this->request->post('delete_checked'))
		{
			$night_ids = Arr::get($this->request->post(), 'ids', array());
			
			foreach ($night_ids as $night_id)
			{
				$night = ORM::factory('cancellation')
					->where('id', '=', $night_id)
					->find();
				
				if (A2::instance()->allowed($night, 'delete'))
				{
          // Get all night rooms
          $rooms = $night->rooms->find_all();
          // Delete night
					$night->delete();
          
          foreach ($rooms as $room)
          {
            $night->restucture_default_campaigns($room);
          }
				}
			}
		}
    
    $nights = ORM::factory('night')
      ->join('nights_rooms')->on('nights_rooms.night_id', '=', 'nights.id')
      ->join('rooms')->on('rooms.id', '=', 'nights_rooms.room_id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->order_by('nights.start_date', 'ASC')
      ->group_by('nights.id')
      ->find_all();
    
    $this->template->main = Kostache::factory('night/manage')
			->set('notice', Notice::render())
      ->set('selected_hotel', $this->selected_hotel)
      ->set('selected_language', $this->selected_language)
      ->set('nights', $nights);
  }
  
  public function action_add()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('night', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    $start_date = $this->request->post('start_date');
    $end_date = $this->request->post('end_date');
    $minimum_number_of_nights = (int) $this->request->post('minimum_number_of_nights');
    
    if ( ! $start_date)
    {
      $start_date = date('Y-m-d');
    }
    
    if ( ! $end_date)
    {
      $end_date = date('Y-m-d');
    }
    
    if ( ! $minimum_number_of_nights)
    {
      $minimum_number_of_nights = 3;
    }
    
    if ($this->request->post('submit'))
    {
      // Start transaction
      Database::instance()->begin();
      
      try 
      {
        // Create minimum nights
        $night = ORM::factory('night')
          ->values(array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'minimum_number_of_nights' => $minimum_number_of_nights,
          ))
          ->create();

        // Get room ids
        $room_ids = Arr::get($this->request->post(), 'room_ids', array());
        // If there is no room selected
        if (count($room_ids) == 0)
        {
          throw new Kohana_Exception(Kohana::message('campaign', 'no_room_selected'));
        }

        foreach ($room_ids as $room_id)
        {
          // Get room ORM
          $room = ORM::factory('room')
            ->where('id', '=', $room_id)
            ->find();

          // Is Authorized ?
          if ( ! A2::instance()->allowed($room, 'read'))
          {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage rooms
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
          }

          // Link minimum nights with room
          $night->add('rooms', $room);
          // Restructure default campaign
          $night->restucture_default_campaigns($room);
          
          // Write log
          ORM::factory('log')
            ->values(array(
              'admin_id' => A1::instance()->get_user()->id,
              'hotel_id' => $room->hotel_id,
              'action' => 'Added Minimum Room Night',
              'room_texts' => serialize(DB::select()
                ->from('room_texts')
                ->where('room_id', '=', $room->id)
                ->execute()
                ->as_array('language_id')),
              'text' => 'Stay Date: :start_date - :end_date. Minimum Number of Night: :minimum_number_of_nights.',
              'data' => serialize(array(
                ':start_date' => date(__('M j, Y'), strtotime($night->start_date)),
                ':end_date' => date(__('M j, Y'), strtotime($night->end_date)),
                ':minimum_number_of_nights' => $night->minimum_number_of_nights,
              )),
              'created' => time(),
            ))
            ->create();
        }

        // Commit transaction
        Database::instance()->commit();
        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        // Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'night', 'action' => 'edit', 'id' => $night->id)));
      }
      catch (Exception $e)
      {
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
      }
    }
    
    $rooms = ORM::factory('room')
      ->select(
        array('room_texts.name', 'name')
      )
      ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->order_by('rooms.publish_price')
      ->find_all();
    
    $this->template->main = Kostache::factory('night/add')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('rooms', $rooms);
  }
  
  public function action_edit()
  {
    // Get night id
		$night_id = (int) $this->request->param('id');
		
		// Get night
		$night = ORM::factory('night')
			->where('id', '=', $night_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed($night, 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage rooms
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
		}
    
    $start_date = $this->request->post('start_date');
    $end_date = $this->request->post('end_date');
    $minimum_number_of_nights = (int) $this->request->post('minimum_number_of_nights');
    
    if ( ! $start_date)
    {
      $start_date = date('Y-m-d');
    }
    
    if ( ! $end_date)
    {
      $end_date = date('Y-m-d');
    }
    
    if ( ! $minimum_number_of_nights)
    {
      $minimum_number_of_nights = 3;
    }
    
    if ($this->request->post('submit'))
    {
      // Start transaction
      Database::instance()->begin();
      
      try 
      {
        // Create minimum nights
        $night = ORM::factory('night')
          ->values(array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'minimum_number_of_nights' => $minimum_number_of_nights,
          ))
          ->create();

        // Get room ids
        $room_ids = Arr::get($this->request->post(), 'room_ids', array());
        // If there is no room selected
        if (count($room_ids) == 0)
        {
          throw new Kohana_Exception(Kohana::message('campaign', 'no_room_selected'));
        }

        foreach ($room_ids as $room_id)
        {
          // Get room ORM
          $room = ORM::factory('room')
            ->where('id', '=', $room_id)
            ->find();

          // Is Authorized ?
          if ( ! A2::instance()->allowed($room, 'read'))
          {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage rooms
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'room', 'action' => 'manage')));
          }

          // Link minimum nights with room
          $night->add('rooms', $room);
          // Restructure default campaigns
          $night->restructure_default_campaigns($room);
          
          // Write log
          ORM::factory('log')
            ->values(array(
              'admin_id' => A1::instance()->get_user()->id,
              'hotel_id' => $room->hotel_id,
              'action' => 'Edited Minimum Room Night',
              'room_texts' => serialize(DB::select()
                ->from('room_texts')
                ->where('room_id', '=', $room->id)
                ->execute()
                ->as_array('language_id')),
              'text' => 'Stay Date: :start_date - :end_date. Minimum Number of Night: :minimum_number_of_nights.',
              'data' => serialize(array(
                ':start_date' => date(__('M j, Y'), strtotime($night->start_date)),
                ':end_date' => date(__('M j, Y'), strtotime($night->end_date)),
                ':minimum_number_of_nights' => $night->minimum_number_of_nights,
              )),
              'created' => time(),
            ))
            ->create();
        }

        // Commit transaction
        Database::instance()->commit();
        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        // Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'night', 'action' => 'edit', 'id' => $night->id)));
      }
      catch (Exception $e)
      {
        // Rollback transaction
        Database::instance()->rollback();
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
      }
    }
    
    $rooms = ORM::factory('room')
      ->select(
        array('room_texts.name', 'name')
      )
      ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
      ->where('rooms.hotel_id', '=', $this->selected_hotel->id)
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->order_by('rooms.publish_price')
      ->find_all();
    
    $this->template->main = Kostache::factory('night/edit')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('night', $night)
      ->set('rooms', $rooms);
  }
  
  public function action_delete()
	{
		// Get night id
		$night_id = $this->request->param('id');
		
		$night = ORM::factory('night')
			->where('id', '=', $night_id)
			->find();
		
		if (A2::instance()->allowed($night, 'delete'))
		{
			try
			{
				// Get all night rooms
        $rooms = $night->rooms->find_all();
        
        // Delete night
        $night->delete();

        foreach ($rooms as $room)
        {
          // Restructure default campaign
          $night->restucture_default_campaigns($room);
          
          // Write log
          ORM::factory('log')
            ->values(array(
              'admin_id' => A1::instance()->get_user()->id,
              'hotel_id' => $room->hotel_id,
              'action' => 'Deleted Minimum Room Night',
              'room_texts' => serialize(DB::select()
                ->from('room_texts')
                ->where('room_id', '=', $room->id)
                ->execute()
                ->as_array('language_id')),
              'text' => NULL,
              'data' => NULL,
              'created' => time(),
            ))
            ->create();
        }
        
				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
  
}