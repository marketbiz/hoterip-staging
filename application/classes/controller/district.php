<?php defined('SYSPATH') or die('No direct script access.');

class Controller_district extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('district', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			if (A2::instance()->allowed('district', 'delete'))
			{
				try
				{
					// Get districts id
					$district_ids = $this->request->post('ids');
			
					// Delete districts
					DB::delete('districts')
						->where('id', 'IN', $district_ids)
						->execute();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			}
		}
		
		$districts = ORM::factory('district')
			->select(
				array('district_texts.name', 'name'),
				array('city_texts.name', 'city_name'),
        array('country_texts.name', 'country_name'),
				array(DB::expr('COUNT(hotels.id)'), 'total_hotels')
			)
			->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
			->join('cities')->on('cities.id', '=', 'districts.city_id')
			->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
			->join('hotels', 'left')->on('hotels.district_id', '=', 'districts.id')
			->where('district_texts.language_id', '=', $this->selected_language->id)
			->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
			->group_by('districts.id')
      ->order_by('city_texts.name', 'ASC')
			->order_by('district_texts.name', 'ASC')
			->find_all();
		
		$this->template->main = Kostache::factory('district/manage')
			->set('notice', Notice::render())
			->set('districts', $districts);
	}
	
	public function action_add()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('district', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage districts
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'district', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Create district
				$district = ORM::factory('district')
					->create_district($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'district', 'action' => 'add')), __('Add another district'))));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'district', 'action' => 'edit', 'id' => $district->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('district'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$languages = ORM::factory('language')
			->find_all();
		
		$cities = ORM::factory('city')
			->select(
				array('city_texts.name', 'name'),
        array('country_texts.name', 'country_name')
			)
			->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
			->order_by('city_texts.name')
			->find_all();
		
		$this->template->main = Kostache::factory('district/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('languages', $languages)
			->set('cities', $cities);
	}
	
	public function action_edit()
	{
		// Get district id
		$district_id = (int) $this->request->param('id');
		
		// Get district
		$district = ORM::factory('district')
			->where('id', '=', $district_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed('district', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage districts
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'district', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Update district
				ORM::factory('district')
					->where('id', '=', $district_id)
					->find()
					->update_district($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('district'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$district = ORM::factory('district')
			->where('id', '=', $district_id)
			->find();
		
		// Get district texts
		$district_texts = ORM::factory('district_text')
			->where('district_id', '=', $district->id)
			->find_all()
			->as_array('language_id');
		
		$languages = ORM::factory('language')
			->find_all();
		
		$cities = ORM::factory('city')
			->select(
				array('city_texts.name', 'name'),
        array('country_texts.name', 'country_name')
			)
			->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
			->order_by('city_texts.name')
			->find_all();
		
		$this->template->main = Kostache::factory('district/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('district', $district)
			->set('district_texts', $district_texts)
			->set('languages', $languages)
			->set('cities', $cities);
	}

	public function action_delete()
	{
		// Get district id
		$district_id = $this->request->param('id');
		
		if (A2::instance()->allowed('district', 'delete'))
		{
			try
			{
				// Delete district
				DB::delete('districts')
					->where('id', '=', $district_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}