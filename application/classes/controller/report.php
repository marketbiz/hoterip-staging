<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Report extends Controller_Layout_Admin {
  
  public function action_finance()
  {
    // Is Authorized ?
    if ( ! A2::instance()->allowed('report', 'finance'))
    {
      // Add error notice
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      // Redirect to home dashboard
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }
    
    $booking_start_date = $this->request->query('booking_start_date');
    $booking_end_date = $this->request->query('booking_end_date');
    $check_in_start_date = $this->request->query('check_in_start_date');
    $check_in_end_date = $this->request->query('check_in_end_date');
    $check_out_start_date = $this->request->query('check_out_start_date');
    $check_out_end_date = $this->request->query('check_out_end_date');
    
    // Get logged in admin
    $logged_in_admin = A1::instance()->get_user();
    
    // Get selected hotel currency
    $currency = ORM::factory('currency')
      ->where('id', '=', $this->selected_hotel->currency_id)
      ->find();
    
    // Get bookings
    $bookings = ORM::factory('booking')
      ->select(
        array('room_texts.name', 'room_name'),
        array('nationalities.name', 'nationality_name')
      )
      ->from('bookings')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
      ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
      ->join('nationalities')->on('nationalities.id', '=', 'bookings.nationality_id')
      ->where('hotels.id', '=', $this->selected_hotel->id)
      ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
      ->where('room_texts.language_id', '=', $this->selected_language->id);
    
    if ($booking_start_date AND $booking_end_date)
    {
      $offset = Date::offset($this->selected_hotel->timezone);
      
      $bookings->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '>=', $booking_start_date);
      $bookings->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '<=', $booking_end_date);
    }

    if ($check_in_start_date AND $check_in_end_date)
    {
      $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
      $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

      $bookings->where('bookings.check_in_date', '>=', $check_in_start_date);
      $bookings->where('bookings.check_in_date', '<=', $check_in_end_date);
    }

    if ($check_out_start_date AND $check_out_end_date)
    {
      $check_out_start_date = date('Y-m-d', strtotime($check_out_start_date));
      $check_out_end_date = date('Y-m-d', strtotime($check_out_end_date));

      $bookings->where('bookings.check_out_date', '>=', $check_out_start_date);
      $bookings->where('bookings.check_out_date', '<=', $check_out_end_date);
    }
    
    $bookings = $bookings
      ->order_by('bookings.id', 'ASC')
      ->find_all();

    /// Manage booking. add data for deleted room
    $bookings_values = ORM::factory('booking')
      ->select(
        array('room_deleted.name', 'room_name'),
        array('nationalities.name', 'nationality_name')
      )
      ->from('bookings')
      ->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
      ->join('hotels')->on('hotels.id', '=', 'room_deleted.hotel_id')
      ->join('admins_hotels')->on('admins_hotels.hotel_id', '=', 'hotels.id')
      ->join('nationalities')->on('nationalities.id', '=', 'bookings.nationality_id')
      ->where('hotels.id', '=', $this->selected_hotel->id)
      ->where('admins_hotels.admin_id', '=', $logged_in_admin->id)
      ->where('room_deleted.language_id', '=', $this->selected_language->id);


    if ($booking_start_date AND $booking_end_date)
    {
      $offset = Date::offset($this->selected_hotel->timezone);
      
      $bookings_values->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '>=', $booking_start_date);
      $bookings_values->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '<=', $booking_end_date);
    }

    if ($check_in_start_date AND $check_in_end_date)
    {
      $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
      $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

      $bookings_values->where('bookings.check_in_date', '>=', $check_in_start_date);
      $bookings_values->where('bookings.check_in_date', '<=', $check_in_end_date);
    }

    if ($check_out_start_date AND $check_out_end_date)
    {
      $check_out_start_date = date('Y-m-d', strtotime($check_out_start_date));
      $check_out_end_date = date('Y-m-d', strtotime($check_out_end_date));

      $bookings_values->where('bookings.check_out_date', '>=', $check_out_start_date);
      $bookings_values->where('bookings.check_out_date', '<=', $check_out_end_date);
    }

    $bookings_values = $bookings_values
      ->order_by('bookings.id', 'ASC')
      ->find_all();

    $this->template->main = Kostache::factory('report/finance')
      ->set('notice', Notice::render())
      ->set('filters', $this->request->query())
      ->set('selected_hotel', $this->selected_hotel)
      ->set('selected_language', $this->selected_language)
      ->set('logged_in_admin', $logged_in_admin)
      ->set('currency', $currency)
      ->set('bookings', $bookings)
      ->set('bookings_values', $bookings_values)
      ->set('total_bookings', count($bookings));
  }
  
  public function action_finance_all()
  {
    // Is Authorized ?
    if ( ! A2::instance()->allowed('report', 'finance_all'))
    {
      // Add error notice
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      // Redirect to home dashboard
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }

    // Factory exchange
    $exchange = Model::factory('exchange');

    if ($this->request->post('send_refund'))
    {
      // Get booking id
      $booking_id = $this->request->post('booking_id');

      // Get the booking
      $booking = ORM::factory('booking')
        ->select(
          'booking_datas.*'
        )
        ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
        ->where('bookings.id', '=', $booking_id)
        ->find();
      
      // If booking already canceled and not refunded yet
      if ($booking->is_canceled AND ! $booking->is_refunded)
      {
        // Get prices
        $prices = Model_Booking::calculate_prices($booking_id);

        // Get the user
        $user = ORM::factory('user')
          ->where('id', '=', $booking->user_id)
          ->find();

        // Start transaction
        Database::instance()->begin();

        try
        {
          /// empty model
          if(!empty($user->id))
          {
            // Refund the point
            $user->point = $user->point + $prices['amount_point_refunded_to_guest'];
            $user->update();
          }
          else
          {
            $user->email = $booking->email;
          }

          // Set booking as refunded
          $booking->is_refunded = 1;
          $booking->update();

          // Get booking language
          $language = ORM::factory('language')
            ->where('id', '=', $booking->language_id)
            ->find();

          // Create message
          $message = Kostache::factory('email/refund')
            ->set('booking', $booking)
            ->set('language', $language)
            ->set('prices', $prices);

          // Send email to user
          Email::factory(I18n::get('Refund Notification', $language->code), $message)
            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
            ->to($user->email)
            ->send();

          // Commit transaction
          Database::instance()->commit();
          // Add success notice
          Notice::add(Notice::SUCCESS, Kohana::message('report', 'refund_success'));
        }
        catch (Exception $e)
        {
          // Rollback transaction
          Database::instance()->rollback();
          // Add error notice
          Notice::add(Notice::ERROR, $e->getMessage());
        }
      }
      else
      {
        // Add error notice
          Notice::add(Notice::ERROR, Kohana::message('report', 'refund_failed'));
      }
    }
    
    /*
     * Get filters
     */
    
    $hotel_id = $this->request->query('hotel_id');
    $invoice_id = $this->request->query('invoice_id');
    $tracking_type = $this->request->query('tracking_type');
    $booking_start_date = $this->request->query('booking_start_date');
    $booking_end_date = $this->request->query('booking_end_date');
    $check_in_start_date = $this->request->query('check_in_start_date');
    $check_in_end_date = $this->request->query('check_in_end_date');
    $check_out_start_date = $this->request->query('check_out_start_date');
    $check_out_end_date = $this->request->query('check_out_end_date');
    
    // Get logged in admin
    $logged_in_admin = A1::instance()->get_user();
    
    // Get all hotels
    $hotels = ORM::factory('hotel')
      ->select(
        array('hotel_texts.name', 'name'),
        array('city_texts.name', 'city_name'),
        array('country_texts.name', 'country_name')
      )
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('cities')->on('cities.id', '=', 'hotels.city_id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name', 'ASC')
      ->find_all();

    $bookings = DB::select(
        'bookings.*',
        'booking_datas.*',
        array('bookings.id', 'id'),
        array('bookings.campaign_id', 'campaign_id'),
        array('booking_datas.booking_pending_id', 'invoice_id'),
        array('hotels.id', 'hotel_id'),
        array('hotel_texts.name', 'hotel_name'),
        array('room_texts.name', 'room_name'),
        array('currencies.code', 'hotel_currency_code')
      )
      ->from('bookings')
      ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
      ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
      ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
      ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('currencies')->on('currencies.id', '=', 'hotels.currency_id')
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->order_by('bookings.id', 'ASC');
    
    if ($hotel_id)
    {
      $bookings->where('hotels.id', '=', $hotel_id);
    }

   //determine tracking query
    if(!empty($invoice_id))
    {
      if ($tracking_type == 0) 
      {
        $bookings->where('booking_datas.booking_pending_id', '=', $invoice_id);
      }
      elseif($tracking_type == 1)
      {
        $bookings->where('bookings.finpay_order_id', '=', $invoice_id);
      }
      elseif($tracking_type == 2)
      {
        $bookings->where('bookings.wego_tracking_id', '=', $invoice_id);
      }
      else
      {
        $bookings->where('booking_datas.booking_pending_id', '=', $invoice_id);
      }
    }
    elseif(empty($invoice_id))
    {
      if($tracking_type == 0)
      {
        $bookings->where('booking_datas.booking_pending_id', 'IS NOT', NULL);
      }
      elseif($tracking_type == 1)
      {
        $bookings->where('bookings.finpay_order_id', 'IS NOT', NULL);
      }
      elseif($tracking_type == 2)
      {
        $bookings->where('bookings.wego_tracking_id', 'IS NOT', NULL);
      }
      else
      {
        $bookings->where('booking_datas.booking_pending_id', '=', $invoice_id);
      }
    }


    if ($booking_start_date AND $booking_end_date)
    {
      $offset = Date::offset($logged_in_admin->timezone);
      
      $booking_start_date = date('Y-m-d', strtotime($booking_start_date));
      $booking_end_date = date('Y-m-d', strtotime($booking_end_date));
      
      $bookings->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '>=', $booking_start_date);
      $bookings->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '<=', $booking_end_date);
    }

    if ($check_in_start_date AND $check_in_end_date)
    {
      $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
      $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

      $bookings->where('bookings.check_in_date', '>=', $check_in_start_date);
      $bookings->where('bookings.check_in_date', '<=', $check_in_end_date);
    }

    if ($check_out_start_date AND $check_out_end_date)
    {
      $check_out_start_date = date('Y-m-d', strtotime($check_out_start_date));
      $check_out_end_date = date('Y-m-d', strtotime($check_out_end_date));

      $bookings->where('bookings.check_out_date', '>=', $check_out_start_date);
      $bookings->where('bookings.check_out_date', '<=', $check_out_end_date);
    }
    
    $bookings = $bookings->as_object()
      ->execute()
      ->as_array();

    /// Manage booking
    $bookings_values = DB::select(
        'bookings.*',
        'booking_datas.*',
        array('bookings.id', 'id'),
        array('bookings.campaign_id', 'campaign_id'),
        array('booking_datas.booking_pending_id', 'invoice_id'),
        array('hotels.id', 'hotel_id'),
        array('hotel_texts.name', 'hotel_name'),
        array('room_deleted.name', 'room_name'),
        array('currencies.code', 'hotel_currency_code')
      )
      ->from('bookings')
      ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
      ->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
      ->join('hotels')->on('hotels.id', '=', 'room_deleted.hotel_id')
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('currencies')->on('currencies.id', '=', 'hotels.currency_id')
      ->where('room_deleted.language_id', '=', $this->selected_language->id)
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->order_by('bookings.id', 'ASC');
    
    if ($hotel_id)
    {
      $bookings_values->where('hotels.id', '=', $hotel_id);
    }

    if ($invoice_id)
    {
      $bookings_values->where('booking_datas.booking_pending_id', '=', $invoice_id);
    }

    if ($booking_start_date AND $booking_end_date)
    {
      $offset = Date::offset($logged_in_admin->timezone);
      
      $booking_start_date = date('Y-m-d', strtotime($booking_start_date));
      $booking_end_date = date('Y-m-d', strtotime($booking_end_date));
      
      $bookings_values->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '>=', $booking_start_date);
      $bookings_values->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '<=', $booking_end_date);
    }

    if ($check_in_start_date AND $check_in_end_date)
    {
      $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
      $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

      $bookings_values->where('bookings.check_in_date', '>=', $check_in_start_date);
      $bookings_values->where('bookings.check_in_date', '<=', $check_in_end_date);
    }

    if ($check_out_start_date AND $check_out_end_date)
    {
      $check_out_start_date = date('Y-m-d', strtotime($check_out_start_date));
      $check_out_end_date = date('Y-m-d', strtotime($check_out_end_date));

      $bookings_values->where('bookings.check_out_date', '>=', $check_out_start_date);
      $bookings_values->where('bookings.check_out_date', '<=', $check_out_end_date);
    }
    
    $bookings_values = $bookings_values->as_object()
      ->execute()
      ->as_array();
    
    $filtered_hotels = array();
    foreach ($bookings as $booking)
    {
      if ( ! isset($filtered_hotels[$booking->hotel_id]))
      {
        $filtered_hotels[$booking->hotel_id] = new stdClass;
        $filtered_hotels[$booking->hotel_id]->id = $booking->hotel_id;
        $filtered_hotels[$booking->hotel_id]->name = $booking->hotel_name;
        $filtered_hotels[$booking->hotel_id]->currency_code = $booking->hotel_currency_code;
      }
      
      // Benefit load data
      $additional_benefit = $booking->additional_benefit ? unserialize($booking->additional_benefit) : array();

      $booking->benefit_price = 0;
      $booking->benefit_price_currency = $booking->currency_id;

      $booking->benefit_check = FALSE;

      // Benefit cost count price
      if(count($additional_benefit))
      {
        foreach ($additional_benefit as $additional_benefit_key => $benefit)
        {
          if(isset($benefit))
          {
            $booking->benefit_check = TRUE;
            $booking->benefit_currency = $benefit['benefit_currency'] ? $benefit['benefit_currency'] : 1 ;
            $benefit_count = $benefit['value'] ? $benefit['value'] : 1;

            $booking->benefit_price += $benefit['additional_price'] * $benefit_count;
          }
        }
      }
      
      $booking->benefit_price = $booking->number_of_rooms * $booking->benefit_price;
      
      $filtered_hotels[$booking->hotel_id]->bookings[] = $booking;
    }
    /// Manage booking
    foreach ($bookings_values as $bookings_value)
    {
      // Check room from deleted room is already restored
      $rooms = DB::select(
        array(DB::expr('COUNT(room_texts.id)'), 'count')
      )
      ->from('room_texts')
      ->where('room_texts.room_id', '!=', $bookings_value->room_id)
      ->where('room_texts.name', '=', $bookings_value->room_name)
      ->where('room_texts.language_id', '=', $this->selected_language->id)
      ->execute();
      if(count($rooms)){continue;}
      
      if (count($filtered_hotels) && isset($filtered_hotels[$bookings_value->hotel_id]))
      {
        // Determine last index in bookings data
        $count = count($filtered_hotels[$bookings_value->hotel_id]->bookings) - 1;

        // Benefit load data
        $additional_benefit = $bookings_value->additional_benefit ? unserialize($bookings_value->additional_benefit) : array();

        $bookings_value->benefit_price = 0;
        $bookings_value->benefit_price_currency = $bookings_value->currency_id;

        $bookings_value->benefit_check = FALSE;

        // Benefit cost count price
        if(count($additional_benefit))
        {
          foreach ($additional_benefit as $additional_benefit_key => $benefit)
          {
            if(isset($benefit))
            {
              $bookings_value->benefit_check = TRUE;
              $bookings_value->benefit_currency = $benefit['benefit_currency'] ? $benefit['benefit_currency'] : 1 ;
              $benefit_count = $benefit['value'] ? $benefit['value'] : 1;

              $bookings_value->benefit_price += ($benefit['additional_price'] * $benefit_count);
            }
          }
        }
        
        $bookings_value->benefit_price = $bookings_value->number_of_rooms * $bookings_value->benefit_price;

        $filtered_hotels[$bookings_value->hotel_id]->bookings[] = $bookings_value;
        $count++;
      }
    }

    if ($this->request->post('download_csv'))
    {
      $csv = CSV::factory('finance-report.csv')
        ->titles(array(
          __('Booking ID'),
          __('Hotel Name'), 
          __('Room Name'), 
          __('Booking Date'), 
          __('Check-in Date'), 
          __('Check-out Date'), 
          __('Number of Rooms'), 
          __('Hotel Revenue'), 
          __('Benefit Cost (Hotel Currency)'), 
          __('OTA Revenue'),
          __('Paid (Hotel Currency)'), 
          __('Paid (Guest Currency)'), 
          __('Paid (Transaction Currency)'), 
          __('Number of Points Used'), 
          __('Money Refunded'), 
          __('Point Refunded'), 
          __('Canceled')));
      
      foreach ($filtered_hotels as $filtered_hotel)
      {
        foreach ($filtered_hotel->bookings as $booking)
        {
          $prices = Model_Booking::calculate_prices($booking->id);
          
          $hotel_to_guest_exchange = unserialize($booking->hotel_to_guest_exchange);
          $hotel_to_transaction_exchange = unserialize($booking->hotel_to_transaction_exchange);

          $guest_currency = unserialize($booking->guest_currency);
          $transaction_currency = unserialize($booking->transaction_currency);

          // Benefit Cost
          $benefit_cost = $booking->benefit_check ? 
            Num::format_decimal(round($booking->benefit_price),2) : 0;

          // OTA revenue
          $ota_revenue = $prices['ota_revenue'] - $booking->benefit_price;

          $csv->values(array(
            $booking->id,
            $booking->hotel_name,
            $booking->room_name,
            date(__('M j, Y'), $booking->created + Date::offset($logged_in_admin->timezone)),
            date(__('M j, Y'), strtotime($booking->check_in_date)),
            date(__('M j, Y'), strtotime($booking->check_out_date)),
            $booking->number_of_rooms,
            Num::format_decimal($prices['hotel_revenue']).' '.$booking->hotel_currency_code,
            $benefit_cost.' '.$booking->hotel_currency_code,
            Num::format_decimal(round($ota_revenue),2).' '.$booking->hotel_currency_code,
            Num::format_decimal($prices['amount_paid_by_guest']).' '.$booking->hotel_currency_code,
            Num::format_decimal(round($prices['amount_paid_by_guest'] / $hotel_to_guest_exchange->amount * $hotel_to_guest_exchange->rate)).' '.$guest_currency->code,
            Num::format_decimal(round($prices['amount_paid_by_guest'] / $hotel_to_transaction_exchange->amount * $hotel_to_transaction_exchange->rate)).' '.$transaction_currency->code,
            $booking->number_of_points_used,
            Num::format_decimal($prices['amount_money_refunded_to_guest']),
            $prices['amount_point_refunded_to_guest'],
            $booking->is_canceled ? __('Yes') : __('No'),
          ));
        }
      }
      
      $csv->send_file();
    }
    
    $this->template->main = Kostache::factory('report/finance_all')
      ->set('notice', Notice::render())
      ->set('filters', $this->request->query())
      ->set('logged_in_admin', $logged_in_admin)
      ->set('selected_language', $this->selected_language)
      ->set('hotels', $hotels)
      ->set('filtered_hotels', $filtered_hotels)
      ->set('total_bookings', count($bookings));
  }
  
  public function action_booking_all()
  {
    // Is Authorized ?
    if ( ! A2::instance()->allowed('report', 'booking_all'))
    {
      // Add error notice
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      // Redirect to home dashboard
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }
    /*
     * Get filters
     */
    $booking_id = $this->request->query('id');
    $hotel_id = $this->request->query('hotel_id');
    $guest_name = $this->request->query('guest_name');
    $user_email = $this->request->query('user_email');
    $roles = $this->request->query('roles');
    $booking_start_date = $this->request->query('booking_start_date');
    $booking_end_date = $this->request->query('booking_end_date');
    $check_in_start_date = $this->request->query('check_in_start_date');
    $check_in_end_date = $this->request->query('check_in_end_date');
    $check_out_start_date = $this->request->query('check_out_start_date');
    $check_out_end_date = $this->request->query('check_out_end_date');
    $total_bookings = 0;
    /// Manage Booking
    $total_bookings_values = 0;

    // Get logged in admin
    $logged_in_admin = A1::instance()->get_user();
    
    // Get all hotels
    $hotels = ORM::factory('hotel')
      ->select(
        array('hotel_texts.name', 'name'),
        array('city_texts.name', 'city_name'),
        array('country_texts.name', 'country_name')
      )
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('cities')->on('cities.id', '=', 'hotels.city_id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id)
      ->order_by('name', 'ASC')
      ->find_all();
    
    /*
     * Form submitted
     */
    if ($this->request->query('search'))
    {
      // Get bookings
      $bookings = ORM::factory('booking')
        ->select(
          array('hotel_texts.name', 'hotel_name'),
          array('currencies.code', 'hotel_currency_code'),
          array('room_texts.name', 'room_name'),
          array('bookings.user_id','user_id'),
          array(DB::expr('SUM(booking_rooms.number_of_adults)'), 'number_of_adults'),
          array(DB::expr('SUM(booking_rooms.number_of_children)'), 'number_of_children')
        )
        ->from('bookings')
        ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
        ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
        ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
        ->join('currencies')->on('currencies.id', '=', 'hotels.currency_id')
        ->join('booking_rooms')->on('booking_rooms.booking_id', '=', 'bookings.id');
        
        if ($roles)
        {
          $bookings->join('users')->on('users.id', '=', 'bookings.user_id');
        }

        $bookings->where('room_texts.language_id', '=', $this->selected_language->id)
          ->where('hotel_texts.language_id', '=', $this->selected_language->id)
          ->group_by('bookings.id')
          ->order_by('bookings.id', 'ASC');

      if ($booking_id)
      {
        $bookings->where('bookings.id', '=', $booking_id);
      }

      if ($hotel_id)
      {
        $bookings->where('hotels.id', '=', $hotel_id);
      }

      if ($guest_name)
      {
        $bookings->where(DB::expr('CONCAT(bookings.first_name, bookings.last_name)'), 'LIKE', "%$guest_name%");
      }

      if ($user_email)
      {
        $bookings->where(DB::expr('CONCAT(bookings.email)'), 'LIKE', "%$user_email%");
      }

      if ($roles)
      {
        $bookings->where('users.role', '=', $roles);
      }

      if ($booking_start_date AND $booking_end_date)
      {
        $booking_start_date = date('Y-m-d', strtotime($booking_start_date));
        $booking_end_date = date('Y-m-d', strtotime($booking_end_date));
        
        $offset = Date::offset($logged_in_admin->timezone);
        
        $bookings->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '>=', $booking_start_date);
        $bookings->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '<=', $booking_end_date);
      }

      if ($check_in_start_date AND $check_in_end_date)
      {
        $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
        $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

        $bookings->where('bookings.check_in_date', '>=', $check_in_start_date);
        $bookings->where('bookings.check_in_date', '<=', $check_in_end_date);
      }
      
      if ($check_out_start_date AND $check_out_end_date)
      {
        $check_out_start_date = date('Y-m-d', strtotime($check_out_start_date));
        $check_out_end_date = date('Y-m-d', strtotime($check_out_end_date));

        $bookings->where('bookings.check_out_date', '>=', $check_out_start_date);
        $bookings->where('bookings.check_out_date', '<=', $check_out_end_date);
      }

      $bookings = $bookings
        ->find_all();

      //Total Bookings
      $total_bookings = array();
      $bookingsIds = array();
      $total_cancel_bookings = 0;

      foreach ($bookings as $key => $booking) {
        
        array_push($bookingsIds, $booking->id);

        if($booking->is_canceled == 0){
          $total_bookings[] = (object)array(
            'number_of_rooms' => $booking->number_of_rooms,
            'check_in_date' => $booking->check_in_date,
            'check_out_date' => $booking->check_out_date,
            'number_of_adults' => $booking->number_of_adults,
            'number_of_children' => $booking->number_of_children,
          );
        }
        else
        {
          $total_cancel_bookings += Date::span(strtotime($booking->check_in_date), strtotime($booking->check_out_date), 'days');; 
        }
      }

      /// Manage booking. add data for deleted room
      $bookings_values = ORM::factory('booking')
        ->select(
          array('hotel_texts.name', 'hotel_name'),
          array('currencies.code', 'hotel_currency_code'),
          array('room_deleted.name', 'room_name'),
          array('bookings.user_id','user_id'),
          array(DB::expr('SUM(booking_rooms.number_of_adults)'), 'number_of_adults'),
          array(DB::expr('SUM(booking_rooms.number_of_children)'), 'number_of_children')
        )
        ->from('bookings')
        ->join('room_deleted')->on('room_deleted.room_id', '=', 'bookings.room_id')
        ->join('hotels')->on('hotels.id', '=', 'room_deleted.hotel_id')
        ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
        ->join('currencies')->on('currencies.id', '=', 'hotels.currency_id')
        ->join('booking_rooms')->on('booking_rooms.booking_id', '=', 'bookings.id');
        

        if ($roles)
        {
          $bookings_values->join('users')->on('users.id', '=', 'bookings.user_id');
        }

        $bookings_values->where('room_deleted.language_id', '=', $this->selected_language->id)
          ->where('hotel_texts.language_id', '=', $this->selected_language->id)
          ->group_by('bookings.id')
          ->order_by('bookings.id', 'ASC');

      if ($booking_id)
      {
        $bookings_values->where('bookings.id', '=', $booking_id);
      }

      if ($hotel_id)
      {
        $bookings_values->where('hotels.id', '=', $hotel_id);
      }

      if ($guest_name)
      {
        $bookings_values->where(DB::expr('CONCAT(bookings.first_name, bookings.last_name)'), 'LIKE', "%$guest_name%");
      }

      if ($user_email)
      {
        $bookings_values->where(DB::expr('CONCAT(bookings.email)'), 'LIKE', "%$user_email%");
      }

      if ($roles)
      {
        $bookings_values->where('users.role', '=', $roles);
      }

      if ($booking_start_date AND $booking_end_date)
      {
        $booking_start_date = date('Y-m-d', strtotime($booking_start_date));
        $booking_end_date = date('Y-m-d', strtotime($booking_end_date));
        
        $offset = Date::offset($logged_in_admin->timezone);
        
        $bookings_values->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '>=', $booking_start_date);
        $bookings_values->where(DB::expr("FROM_UNIXTIME(bookings.created + $offset, '%Y-%m-%d')"), '<=', $booking_end_date);
      }

      if ($check_in_start_date AND $check_in_end_date)
      {
        $check_in_start_date = date('Y-m-d', strtotime($check_in_start_date));
        $check_in_end_date = date('Y-m-d', strtotime($check_in_end_date));

        $bookings_values->where('bookings.check_in_date', '>=', $check_in_start_date);
        $bookings_values->where('bookings.check_in_date', '<=', $check_in_end_date);
      }
      
      if ($check_out_start_date AND $check_out_end_date)
      {
        $check_out_start_date = date('Y-m-d', strtotime($check_out_start_date));
        $check_out_end_date = date('Y-m-d', strtotime($check_out_end_date));

        $bookings_values->where('bookings.check_out_date', '>=', $check_out_start_date);
        $bookings_values->where('bookings.check_out_date', '<=', $check_out_end_date);
      }

      if(!empty($bookingsIds)){
        $bookings_values = $bookings_values
        ->where('bookings.id', 'NOT IN', $bookingsIds);
      }

      $bookings_values = $bookings_values
        ->find_all();

      //Total Bookings
      $total_bookings_values = array();
      $total_cancel_bookings_values = 0;
      foreach ($bookings_values as $key => $bookings_value) {
        if($bookings_value->is_canceled == 0){
          $total_bookings_values[] = (object)array(
            'number_of_rooms' => $bookings_value->number_of_rooms,
            'check_in_date' => $bookings_value->check_in_date,
            'check_out_date' => $bookings_value->check_out_date,
            'number_of_adults' => $bookings_value->number_of_adults,
            'number_of_children' => $bookings_value->number_of_children,
          );
        }
        else
        {
          $total_cancel_bookings_values += Date::span(strtotime($bookings_value->check_in_date), strtotime($bookings_value->check_out_date), 'days');; 
        }
      }
    }
    else
    {
      $bookings = array();
      /// Manage Booking
      $bookings_values = array();
      $total_cancel_bookings = 0;
      $total_cancel_bookings_values = 0;
    }

    /// Booking report statistic
    $booking_cancel = count($bookings) - count($total_bookings);
    $booking_values_cancel = count($bookings_values) - count($total_bookings_values);

    $booking_cancel = $booking_cancel + $booking_values_cancel;

    /// Cancel nights
    $booking_cancel_nights = $total_cancel_bookings + $total_cancel_bookings_values;

    if($this->request->post('download_csv') || $this->request->post('production_summary'))
    {
      $data = array();
      $data_row = array();
      $valuecsv = array();

      $data['download_csv'] = $this->request->post('download_csv');
      $data['production_summary'] = $this->request->post('production_summary');

      if(!empty($data['download_csv']))
      {
        $csv_name = 'Monthly_sales';
        //create head.
        $headcsv = array(
          'Booking ID', 'Guest Name', 'Hotel ID', 'Hotel Name', 'Category', 'Hotel City', 'Hotel District', 'Number of Guests',
          'Number of Rooms', 'Booking Date', 'C/IN Date', 'C/OUT Date', 'Number of Nights', 'Total Rooms Nights', 'Hotel Currency',
          'Net Selling Price', 'Hotel Commission %', 'Hotel Revenue', 'OTA Revenue', 'Used Point by Guest', 'Amount Paid by Guest',
          'Sales discount', 'Bank Fee', 'VAT', 'Commision to VCT', 'VCT Booking REF#', 'Reward Point to Guest', 'Net Profit',
          'Waive profit (BT)', 'Sales Issue Location', 'Sales / Business Trip', 'Hotel Bank Account', 'Payment Method to Hotel',
          'Payment Status', 'Remarks'
        );       
      }
      elseif(!empty($data['production_summary']))
      {
        $headcsv = array(
          'Hotel ID', 'Hotel Name', 'Area', 'District Name', 'Total Rooms Nights', 'Confrimed Booking'
        );
      }

      $hotels_bookings = array();
      $i=0;
      foreach ($bookings as $bookingk => $booking) 
      {
        if($booking->is_canceled)
        {
          continue;
        }
        //get table booking_datas
        $booking_datas = ORM::factory('booking')
          ->select(
            'booking_datas.*'
          )
          ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
          ->where('bookings.id', '=', $booking->id)
          ->find();

        //get booking_id_vacation
        $vacation = DB::select('booking_vacation_id')
        ->from('api_booking_vacations')
        ->where('booking_id', '=', $booking->id)
        ->execute()
        ->current()
        ;
        //unserialice booking_data
        $booking_data = unserialize($booking->data);
        
        //calculated price
        $prices = Model_Booking::calculate_prices($booking->id);

        //get hotel_to_guest_exchange
        $hotel_to_guest_exchange = unserialize($booking_datas->hotel_to_guest_exchange);

        // Get all hotels
        $hotel = ORM::factory('hotel')
          ->select(
            array('hotel_texts.name', 'name'),
            array('city_texts.name', 'city_name'),
            array('country_texts.name', 'country_name')
          )
          ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
          ->join('cities')->on('cities.id', '=', 'hotels.city_id')
          ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
          ->join('countries')->on('countries.id', '=', 'cities.country_id')
          ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
          ->where('hotel_texts.language_id', '=', $this->selected_language->id)
          ->where('city_texts.language_id', '=', $this->selected_language->id)
          ->where('country_texts.language_id', '=', $this->selected_language->id)
          ->where('hotels.id', '=', $booking_data['hotel_id'])
          ->order_by('name', 'ASC')
          ->find();

        //set data price an other
        $number_of_nights = Date::span(strtotime($booking->check_in_date), strtotime($booking->check_out_date), 'days');
        $data['number_of_guest'] = $booking->number_of_adults + $booking->number_of_children;
        $data['total_rooms_night'] = $booking->number_of_rooms * $number_of_nights;
        $data['ota_revanue'] = $prices['ota_revenue'];
        $data['hotel_revanue'] = $prices['hotel_revenue'];
        $data['amount_paid_by_guest'] = $prices['amount_paid_by_guest'];
        $data['vat'] = $prices['amount_paid_by_guest'] * 0.01;
        $data['points'] = $booking->number_of_points_used;
        $data['net_selling_price'] = $prices['amount_paid_by_guest'] + $data['points'] + $prices['bni_discount'];
        
        if($booking_data['point_get'] > 300)
        {
          $booking_data['point_get'] =  round($booking_data['point_get'] * Model::factory('exchange')->to($hotel->currency_id, 1));
}
        $old_hotel = unserialize($booking_datas->hotel);

        if(!empty($data['download_csv']))
        {
          //set data row
          $data_row = array(
            'booking_id' => $booking->id,
            'guest_name' => $booking->first_name.' '.$booking->last_name,
            'hotel_id' => $booking_data['hotel_id'],
            'hotel_name' => $booking_data['hotel_name'],
            'category' => $booking_data['room_name'],
            'hotel_city' => $booking_data['city_name'],
            'hotel_district' => $booking_data['district_name'],
            'number_of_guests' => $data['number_of_guest'],
            'number_of_rooms' => $booking->number_of_rooms,
            'booking_date' => date('Y-m-d', $booking->created),
            'check_in' => $booking_data['check_in'],
            'check_out' => $booking_data['check_out'],
            'number_of_nights' => $booking_data['nights'],
            'total_rooms_night' => $data['total_rooms_night'],
            'hotel_currency' => $booking_data['hotel_currency']->code,
            'net_selling_price' => $data['net_selling_price'],
            'hotel_commision' => $old_hotel->commission_rate.'%',
            'hotel_revanue' => $data['hotel_revanue'],
            'ota_revanue' =>$data['ota_revanue'],
            'used_point_by_guest' => $booking->number_of_points_used,
            'amount_paid_by_guest' => $data['amount_paid_by_guest'],
            'sales_discount' => !empty($booking_data['bni_discount']) ? $booking_data['bni_discount'] : 0,
            'bank_fee' => '', //leave blank
            'vat' => $data['vat'],
            'commision_to_vct' => '', //leave blank 
            'vct_ref' => $vacation['booking_vacation_id'],
            'reward_point' => $booking_data['point_get'],
            'net_profit' => '',//leave blank
            'waive_profit' => '', //leave blank
            'sales_issue_location' => '', //leave blank
            'hotel_bank_account' => '', //leave blank
            'payment_method' => '', //leave blank
            'payment_status' => '', //leave blank
            'remarks' => '' //leave blank
          );
        }
        elseif(!empty($data['production_summary']))
        {
          //group by hotel
          $hotels_bookings[$booking_data['hotel_id']][$booking->id] = $booking->as_array();
        }
        //push array
        array_push($valuecsv, $data_row);
        // $i++;
      }
      
      if(!empty($data['production_summary']))
      {
        $csv_name = 'Summary_Hotel';
        $valuecsv = array();
        $data_row = array();
        foreach ($hotels_bookings as $hotel_bookingsk => $hotel_bookings) 
        {
         
          foreach ($hotel_bookings as $hotel_bookingk => $hotel_booking) 
          {
            $booking_data = unserialize($hotel_booking['data']);
            if(!$booking->is_canceled)
            {
              $number_of_nights = Date::span(strtotime($hotel_booking['check_in_date']), strtotime($hotel_booking['check_out_date']), 'days');
              $total_rooms_night += $hotel_booking['number_of_rooms'] * $number_of_nights;
              $total_booking_not_canceled += 1 ;
            }
          }

          $data_row = array(
            'hotel_id' => $booking_data['hotel_id'],
            'hotel_name' => $booking_data['hotel_name'],
            'area' => $booking_data['city_name'],
            'hotel_district' => $booking_data['district_name'],
            'total_rooms_nights' => $total_rooms_night,
            'status' => $total_booking_not_canceled
          );

          array_push($valuecsv, $data_row);
          $total_rooms_night = 0;
          $total_booking_not_canceled = 0;
        }
      }
      Controller_CSV::csvReport($headcsv, $valuecsv, $csv_name);
    }
    $this->template->main = Kostache::factory('report/booking_all')
      ->set('notice', Notice::render())
      ->set('filters', $this->request->query())
      ->set('logged_in_admin', $logged_in_admin)
      ->set('hotels', $hotels)
      ->set('bookings', $bookings)
      ->set('total_bookings', $total_bookings)
      ->set('bookings_values', $bookings_values)
      ->set('total_bookings_values', $total_bookings_values)
      ->set('booking_cancel_nights', $booking_cancel_nights)
      ->set('booking_cancel', $booking_cancel);
  }
  
}