<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of agent
 *
 * @author husenef
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Agent extends Controller_Layout_Admin {

    public function action_manage() {

        // Is Authorized ?
        if (!A2::instance()->allowed('user', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        $field = $this->request->query('field');
        $keyword = $this->request->query('keyword');

        $page = Arr::get($this->request->query(), 'page', 1);
        $items_per_page = 30;
        $offset = ($page - 1) * $items_per_page;

        // Get users
        $agents = ORM::factory('user')
                ->select(array('languages.name', 'language_name'),array('agent_datas.hotel_assigned','hotel_assigned'))
                ->join('languages')->on('languages.id', '=', 'users.language_id')
                ->join('agent_datas')->on('agent_datas.user_id', '=', 'users.id')
                ->where('role', '=', 'agent');

        if ($keyword) {
            if ($field == 1) {
                $agents->where('email', '=', $keyword);
            } elseif ($field == 2) {
                $agents->where('first_name', 'LIKE', "%$keyword%");
            } elseif ($field == 3) {
                $agents->where('last_name', 'LIKE', "%$keyword%");
            }
        }

        // Get total users
        $total_users = $agents->reset(FALSE)->count_all();

        // Get users
        $agents = $agents
                ->offset($offset)
                ->limit($items_per_page)
                ->find_all();
//        echo "<pre>";
//        print_r($agents);
//        echo "</pre>";
        // Create pagination
        $pagination = Pagination::factory(array(
                    'items_per_page' => $items_per_page,
                    'total_items' => $total_users,
        ));

        $this->template->main = Kostache::factory('agent/manage')
                ->set('notice', Notice::render())
                ->set('filters', $this->request->query())
                ->set('agents', $agents)
                ->set('pagination', $pagination);
    }

    public function action_delete() {
        $user_id = $this->request->param('id');

        if (A2::instance()->allowed('user', 'delete')) {
            try {
                // Delete user
                DB::delete('users')
                        ->where('id', '=', $user_id)
                        ->execute();
                DB::delete('agent_datas')
                        ->where('user_id', '=', $user_id)
                        ->execute();

                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
            } catch (Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
            }
        } else {
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
        }

        // Redirect back
        $this->request->redirect($this->request->referrer());
    }

    public function action_active() {
        $user_id = $this->request->param('id');
        /* if (A2::instance()->allowed('user', 'update')) {

          } else {
          Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
          } */
        try {
            // updae user
            DB::update('users')->set(array('actived' => 11))->where('id', '=', $user_id)->execute();

            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'update_success'));
        } catch (Exception $e) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'update_failed'), array(':message' => $e->getMessage()));
        }
        // Redirect back
        $this->request->redirect($this->request->referrer());
    }
    
     public function action_edit() {
        // Get admin id
        $agent_id = (int) $this->request->param('id');

        // Is Authorized ?
        if (!A2::instance()->allowed('admin', 'edit')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage rooms
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }
        
        $total_hotel = DB::select()->from('agent_hotels')->where('user_id','=',$agent_id)->execute();
        $update_hotel_assigned = DB::update('agent_datas')->set(array('hotel_assigned' => count($total_hotel)))->where('user_id', '=', $agent_id)->execute();     
        if ($this->request->post('submit')) {
            try {
                $values = $this->request->post();
                
                $data_deposit = '';
                $cek_deposit = DB::select()->from('agent_datas')->where('user_id','=',$agent_id)->execute();
                foreach ($cek_deposit as $deposit){
                    $data_deposit = $deposit;
                }
                
                $name = explode(' ', trim($values['name']));
                $values['first_name'] = $name[0];
                $values['last_name'] = !empty($name[1]) ? $name[1] : '';

                // Is there any password submitted	
                if (isset($values['password']) AND strlen($values['password']) > 0) {
                    // Extra validation
                    $extra_validation = Validation::factory($values)
                            ->rule('password', 'min_length', array(':value', 4))
                            ->rule('password_confirm', 'matches', array(':validation', ':field', 'password'))
                            ->label('password', 'Password')
                            ->label('password_confirm', 'Password Confirmation');

                    // Update admin
                    ORM::factory('user')
                            ->where('id', '=', $agent_id)
                            ->find()
                            ->values($values, array(
                                'first_name',
                                'last_name',
                                'display_name',
                                'password',
                                'email',
                                'home_phone'
                            ))
                            ->set('is_subscribe_newsletter', Arr::get($values, 'is_subscribe_newsletter', 0))
                            ->update($extra_validation);
                } else {
                    // Update agent
                    ORM::factory('user')
                            ->where('id', '=', $agent_id)
                            ->find()
                            ->values($values, array(
                                'first_name',
                                'last_name',
                                'display_name',
                                'email',
                                'home_phone'
                            ))
                            ->set('is_subscribe_newsletter', Arr::get($values, 'is_subscribe_newsletter', 0))
                            ->update();
                }
                
                $addition_deposit   = floatval($values['addition_deposit']);
                $floating_deposit   = floatval($values['floating_deposit']);
                $balance_deposit    = floatval($values['balance_deposit']);
                $now_deposit        = floatval($values['now_deposit']);
                
                if($values['ubah_deposit']){
                    if(empty($values['revisi_deposit']) || $values['revisi_deposit'] <= 0){
                        $revisi_deposit = 0;
                    }else{
                        $revisi_deposit = floatval($values['revisi_deposit']);
                    }
                }else{
                    $revisi_deposit = 0;
                }
                
                $data_update = array();
                $data_update['address'] = $values['address'];
                if($revisi_deposit == 0){
                    if(!empty($addition_deposit) || $addition_deposit > 0){
                        $last_d     = $addition_deposit;
                        $float_d    = floatval($data_deposit['floating_deposit'] + $addition_deposit);
                        $balanc_d   = floatval($data_deposit['balance_deposit'] + $addition_deposit);
                        
                        $data_update['addition_deposit']    = $last_d;
                        $data_update['balance_deposit']     = $balanc_d;
                        $data_update['floating_deposit']    = $float_d;
                    }
                }else{
                    if($now_deposit > $revisi_deposit){
                        $selisih    = floatval($now_deposit - $revisi_deposit);
                        
                        $last_d     = floatval($data_deposit['addition_deposit'] - $selisih);
                        $float_d    = floatval($data_deposit['floating_deposit'] - $selisih);
                        $balanc_d   = floatval($data_deposit['balance_deposit'] - $selisih);
                        
                        $data_update['addition_deposit']    = $last_d;
                        $data_update['balance_deposit']     = $balanc_d;
                        $data_update['floating_deposit']    = $float_d;
                        
                    }else if($now_deposit <= $revisi_deposit){
                        $selisih    = floatval($revisi_deposit - $now_deposit);
                        
                        $last_d     = floatval($data_deposit['addition_deposit'] + $selisih);
                        $float_d    = floatval($data_deposit['floating_deposit'] + $selisih);
                        $balanc_d   = floatval($data_deposit['balance_deposit'] + $selisih);
                        
                        $data_update['addition_deposit']    = $last_d;
                        $data_update['balance_deposit']     = $balanc_d;
                        $data_update['floating_deposit']    = $float_d;
                    }
                }
                
                DB::update('agent_datas')
                ->set(
                        /*array(
                            'address' => $values['address'],
                            'floating_deposit' => $float_d,
                            'addition_deposit' => $last_d,
                            'balance_deposit' => $balanc_d,
                            'floating_deposit' => (intval($values['floating_deposit']) !== intval($data_deposit['floating_deposit'])) ? intval($values['floating_deposit']) : intval($data_deposit['floating_deposit']),
                            'balance_deposit' => (intval($values['floating_deposit']) !== intval($data_deposit['floating_deposit'])) ? intval($values['floating_deposit']) + intval($values['balance_deposit']) : intval($values['balance_deposit'])
                        )*/
                        $data_update
                    )
                ->where('user_id', '=', $agent_id)
                ->execute();
                
                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
            } catch (ORM_Validation_Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, Arr::flatten($e->errors('admin')));
            } catch (Exception $e) {
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        // Get agent
        $agent = ORM::factory('user')
                ->select(
                        array('agent_datas.addition_deposit', 'addition_deposit'),
                        array('agent_datas.floating_deposit', 'floating_deposit'), 
                        array('agent_datas.balance_deposit', 'balance_deposit'),
                        array('agent_datas.address','address')
                )
                ->join('agent_datas')->on('agent_datas.user_id', '=', 'users.id')
                ->where('agent_datas.user_id', '=', $agent_id)
                ->where('id', '=', $agent_id)
                ->find();
        // Get timezones
        $timezones = ORM::factory('timezone')
                ->find_all();
        $text_deposit = array();
        $text_deposit['Ldeposit']  = number_format($agent->addition_deposit);
        $text_deposit['Fdeposit']  = number_format($agent->floating_deposit);
        $text_deposit['Bdeposit']  = number_format($agent->balance_deposit);
        
        $this->template->main = Kostache::factory('agent/edit')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('agent', $agent)
                ->set('text_deposit', $text_deposit)
                ->set('timezones', $timezones);
    }

    public function action_assign() {
        
        // Is Authorized ?
        if (!A2::instance()->allowed('hotel', 'assign')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }


        /**
         * Get values from post
         */
        $agent_id = (int) $this->request->post('agent_id');
        $hotel_id = (int) $this->request->post('hotel_id');
        //echo '<pre>';print_r($this->request);echo '</pre>';
        /**
         * Filter 
         */
        if (!$agent_id) {
//            $admin = ORM::factory('admin')->find();
//            $admin_id = $admin->id;
            $agent_id = 0;
        }
        if ($this->request->post('assign')) {
            // Get admin
            $agent = ORM::factory('user')
                    ->where('id', '=', $agent_id)
                    ->find();
            
            if ($hotel_id > 0) {
                // Get hotel
                $hotel = ORM::factory('hotel')
                        ->where('id', '=', $hotel_id)
                        ->find();
                
                // If hotel found
                if ($hotel->loaded()) {
                    // If $agent not assigned

                    if (!$agent->has('hotels', $hotel)) {
                        $agent->add('hotels', $hotel);
                    }
                }
            } else {
                // Get all hotels
                $hotels = ORM::factory('hotel')->find_all();

                foreach ($hotels as $hotel) {
                    // If admin not assigned
                    if (!$agent->has('hotels', $hotel)) {
                        // Assign hotel to admin
                        $agent->add('hotels', $hotel);
                    }
                }
            }

            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        }


        //un assign
        if ($this->request->post('unassign')) {
            $admin = ORM::factory('user')
                    ->where('id', '=', $agent_id)
                    ->find();

            $hotel_ids = Arr::get($this->request->post(), 'ids', array());

            foreach ($hotel_ids as $hotel_id) {
                $hotel = ORM::factory('hotel')
                        ->where('id', '=', $hotel_id)
                        ->find();

                // If hotel assigned to admin
                if ($admin->has('hotels', $hotel)) {
                    // Unassign hotel from admin
                    $admin->remove('hotels', $hotel);
                }
            }

            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        }
        

        // Get users
//        $agents = ORM::factory('user')->select(array('languages.name', 'language_name'))->join('languages')->on('languages.id', '=', 'users.language_id')->where('role', '=', 'agent')->find_all();
        $agents = ORM::factory('user')->where('role', '=', 'agent')->find_all();

        $admin_hotels = ORM::factory('hotel')
                ->select(
                        array('hotel_texts.name', 'name'), array('country_texts.name', 'country_name'), array('city_texts.name', 'city_name'), array('agent_hotels.recommend', 'recommend'),array('agent_hotels.order','order')
                )
                ->join('agent_hotels')->on('agent_hotels.hotel_id', '=', 'hotels.id')
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->join('countries')->on('countries.id', '=', 'cities.country_id')
                ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
                ->where('agent_hotels.user_id', '=', $agent_id)
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->where('country_texts.language_id', '=', $this->selected_language->id)
                ->order_by('agent_hotels.order','desc')
                ->order_by('hotel_texts.name')
                ->order_by('country_texts.name')
                ->order_by('city_texts.name')
                ->find_all();
        
        if ($this->request->post('update')) {
            $admin = ORM::factory('user')
                    ->where('id', '=', $agent_id)
                    ->find();

            $values = $this->request->post();
            //echo '<pre>';print_r($values);echo '</pre>';
            //exit();
            //echo '<pre>';print_r($this->request);echo '</pre>';
            foreach ($admin_hotels as $k => $v) {
                
                /*---------------------update recommend------------------*/
                if(!is_null($values['recommend'][$v->id]) || !empty($values['recommend'][$v->id])){
                    if(!is_null($values['order'][$v->id]) || !empty($values['order'][$v->id])){
                        $recom  = $values['recommend'][$v->id];
                        $order  = ($values['order'][$v->id] <= 10) ? $values['order'][$v->id] : 0;
                    }else{
                        $recom  = 0;
                        $order  = 0;
                    }
                    $query = DB::update('agent_hotels')
                    ->set(array('recommend' => $recom ,'order' =>  $order ))
                    ->where('user_id', '=', $agent_id)
                    ->where('hotel_id', '=', $v->id);
                    $result = $query->execute();
                }
                /*---------------------end update recommend--------------*/
                /*---------------------update markup---------------------*/
                if(!is_null($values['persen'][$v->id]) || !empty($values['persen'][$v->id])){
                    if(!is_null($values['markup'][$v->id]) || !empty($values['markup'][$v->id])){
                        
                        $markup = ($values['markup'][$v->id] <= 100) ? $values['markup'][$v->id] : 0;
                        
                        $query = DB::update('agent_hotels')
                        ->set(array('persen' => $values['persen'][$v->id], 'markup' => $markup))
                        ->where('user_id', '=', $agent_id)
                        ->where('hotel_id', '=', $v->id);
                        $result = $query->execute();
                    }
                }
                if(!is_null($values['markup'][$v->id]) || !empty($values['markup'][$v->id])){
                    if(is_null($values['persen'][$v->id]) || empty($values['persen'][$v->id])){
                        $query = DB::update('agent_hotels')
                        ->set(array('markup' => $values['markup'][$v->id], 'persen' => '0'))
                        ->where('user_id', '=', $agent_id)
                        ->where('hotel_id', '=', $v->id);
                        $result = $query->execute();
                    }
                }
                /*---------------------end update markup-----------------*/
                
                /*if(is_null($values['recommend'][$v->id]) || empty($values['recommend'][$v->id])){
                    $values['order'][$v->id] = 0;
                }else{
                    $values['order'][$v->id] = ($values['order'][$v->id] < 11 ) ? $values['order'][$v->id] : 0;
                }
                
                // Update recommend hotel
                $query = DB::update('agent_hotels')
                ->set(array('recommend' => !empty($values['recommend'][$v->id]) ? $values['recommend'][$v->id] : 0 ,'order' =>  $values['order'][$v->id] ))
                ->where('user_id', '=', $agent_id)
                ->where('hotel_id', '=', $v->id);
//                $admin_hotels[$k]->recommend = !empty($values['recommend'][$v->id]) ? $values['recommend'][$v->id] : 0;
                //echo '<pre>';print_r(json_decode(json_encode($v),true));echo '</pre>';
                $result = $query->execute();*/
            }
            
            // Add success notice
            Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
            //$this->request->redirect(Route::get('default')->uri(array('controller' => 'agent', 'action' => 'assign','agent_id'=>$agent_id)));
        }
        
        $total_hotel = DB::select()->from('agent_hotels')->where('user_id','=',$agent_id)->execute();
        $update_hotel_assigned = DB::update('agent_datas')->set(array('hotel_assigned' => count($total_hotel)))->where('user_id', '=', $agent_id)->execute();
       
        $admin_hotels2 = ORM::factory('hotel')
                ->select(
                        array('hotel_texts.name', 'name'), array('country_texts.name', 'country_name'), array('city_texts.name', 'city_name'), array('agent_hotels.recommend', 'recommend'),array('agent_hotels.order','order')
                        ,array('agent_hotels.persen', 'persen'), array('agent_hotels.markup', 'markup')
                )
                ->join('agent_hotels')->on('agent_hotels.hotel_id', '=', 'hotels.id')
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->join('countries')->on('countries.id', '=', 'cities.country_id')
                ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
                ->where('agent_hotels.user_id', '=', $agent_id)
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->where('country_texts.language_id', '=', $this->selected_language->id)
                ->order_by('agent_hotels.order','desc')
                ->order_by('hotel_texts.name')
                ->order_by('country_texts.name')
                ->order_by('city_texts.name')
                ->find_all();
        
        #hotels
        $hotels = ORM::factory('hotel')
                ->select(
                        array('hotel_texts.name', 'name'), array('city_texts.name', 'city_name')
                )
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->where('hotel_texts.language_id', '=', $this->selected_language->id)
                ->where('city_texts.language_id', '=', $this->selected_language->id)
                ->order_by('hotel_texts.name')
                ->order_by('city_texts.name')
                ->find_all();
        
        $data_admin = DB::select(array('currencies.code','currency'))->from('users')
                ->join('currencies')->on('currencies.id','=','users.currency_id')
                ->where('users.id', '=', $agent_id)
                ->execute()
                ->as_array();
        
        $this->template->main = Kostache::factory('agent/assign')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('agents', $agents)
                ->set('admin_hotels', $admin_hotels)
                ->set('admin_hotels2', $admin_hotels2)
                ->set('hotels', $hotels)
                ->set('admin',$data_admin);
    }

}
