<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Deal extends Controller_Layout_Admin {
  
  public function before()
	{
		if (in_array($this->request->action(), array('delete', 'reorder')))
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
  
  public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('deal', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		$deals = ORM::factory('deal')
      ->select(
        array('languages.code', 'language_code'),
        array('languages.name', 'language_name')
      )
      ->join('languages')->on('languages.id', '=', 'deals.language_id')
      ->order_by('order')
      ->order_by('id')
			->find_all();
		
		$this->template->main = Kostache::factory('deal/manage')
			->set('notice', Notice::render())
			->set('deals', $deals)
			->set('selected_language', $this->selected_language);
	}
  
  public function action_add()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('deal', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    if ($this->request->post('submit'))
    {
      try
      {
        // Create deal
        $deal = ORM::factory('deal')
          ->values($this->request->post(), array(
            'language_id',
            'name',
          ))
          ->create();
        
        // Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'deal', 'action' => 'edit', 'id' => $deal->id)));
      }
      catch (Exception $e)
      {
        // Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, array($e->getMessage()));
      }
    }
    
    $languages = ORM::factory('language')
      ->find_all();
    
    $this->template->main = Kostache::factory('deal/add')
			->set('notice', Notice::render())
			->set('languages', $languages);
  }
  
  public function action_edit()
  {
    // Is Authorized ?
		if ( ! A2::instance()->allowed('deal', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
    // Get deal id
    $deal_id = $this->request->param('id');
    
    if ($this->request->post('submit'))
    {
      try
      {
        // Find deal
        $deal = ORM::factory('deal')
          ->where('id', '=', $deal_id)
          ->find();
        
        if ($deal->loaded())
        {
          // Update deal
          $deal->values($this->request->post(), array(
            'language_id',
            'name',
          ))
          ->update();
        
          // Add success notice
          Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
        }
        else
        {
          throw new Exception('Deal not found');
        }
      }
      catch (Exception $e)
      {
        // Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->getMessage());
      }
    }
    
    $deal = ORM::factory('deal')
      ->where('id', '=', $deal_id)
      ->find();
    
    $languages = ORM::factory('language')
      ->find_all();
    
    $this->template->main = Kostache::factory('deal/edit')
			->set('notice', Notice::render())
      ->set('values', $this->request->post())
      ->set('deal', $deal)
			->set('languages', $languages);
  }
  
  public function action_delete()
  {
    // Get deal id
		$deal_id = $this->request->param('id');
		
		if (A2::instance()->allowed('deal', 'delete'))
		{
			try
			{
				// Delete deal
				DB::delete('deals')
					->where('id', '=', $deal_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
    
    $this->request->redirect($this->request->referrer());
  }
  
  public function action_reorder()
  {
    // If there is post
    if ($this->request->post())
    {
      // Get deals id
      $deal_ids = $this->request->post('deal_ids');

      try
      {
        foreach ($deal_ids as $key => $deal_id)
        {
          // Update order
          $deal = ORM::factory('deal')
            ->where('id', '=', $deal_id)
            ->find();
          
          if ($deal->loaded())
          {
            $deal->set('order', $key + 1)
            ->update();
          }
        }

        $return = array(
          'success' => TRUE,
          'message' => Kohana::message('general', 'save_success'),
        );
      }
      catch (Exception $e)
      {
        $return = array(
          'success' => FALSE,
          'message' => $e->getMessage(),
        );
      }

      $this->response->body(json_encode($return));
    }
  }
  
}