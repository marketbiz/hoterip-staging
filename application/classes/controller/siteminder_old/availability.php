<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Siteminder_Availability extends Controller {

  public function checker($transaction, $head, $body, $xmlns)
  {
    $data = array();
    $error = array();

    $auth = array(
      'User' => (string)$head->Security->UsernameToken->Username,
      'Pwd' => (string)$head->Security->UsernameToken->Password,
      'source' => 'vacation',
      'key' => NULL,
      );

    switch ($transaction) {
      case 'OTA_HotelAvailRQ':

        $OTA_HotelAvailRQ = $body->$transaction->attributes();
        $node_head = array(
          'head' => $transaction,
          'xmlns' => $xmlns,
          'TimeStamp' => (string)$OTA_HotelAvailRQ->TimeStamp,
          'Version' => (string)$OTA_HotelAvailRQ->Version,
          'EchoToken' => (string)$body->$transaction->attributes()->EchoToken
        );

        // Check node head null
        if(empty($node_head['xmlns'])
          || empty($node_head['TimeStamp'])
          || empty($node_head['Version'])
          || empty($node_head['EchoToken']))
        {
          $error['Empty_Property']['type'] = '10';
          $error['Empty_Property']['code'] = '321';
        }
        // Check Auth null request
        elseif(empty($auth['User'])
          || empty($auth['Pwd']))
        {
          $error['Authentification']['type'] = '4';
          $error['Authentification']['code'] = '321';
        }
        else
        {
          // Check Auth
          if (Controller_Vacation_Static::auth($auth) == FALSE)
          {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '9900';
          }
          else
          {
            $HotelRef = $body->$transaction->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->HotelRef;

            $data = array(
              'hotel_id' => (string)$HotelRef->attributes()->HotelCode,
            );

            // Check Null request
            if(empty($data['hotel_id']))
            {
              $error['AvailStatusMessage']['type'] = '10';
              $error['AvailStatusMessage']['code'] = '321';
              break;
            }
          } 
        }
        break;
      
      case 'OTA_HotelAvailNotifRQ':
        $OTA_HotelAvailNotifRQ = $body->$transaction->attributes();
        $node_head = array(
          'head' => $transaction,
          'xmlns' => $xmlns,
          'TimeStamp' => (string)$OTA_HotelAvailNotifRQ->TimeStamp,
          'Version' => (string)$OTA_HotelAvailNotifRQ->Version,
          'EchoToken' => (string)$body->$transaction->attributes()->EchoToken,
          );

        // Check node head null
        if(empty($node_head['xmlns'])
          || empty($node_head['TimeStamp'])
          || empty($node_head['Version'])
          || empty($node_head['EchoToken']))
        {
          $error['Empty_Property']['type'] = '10';
          $error['Empty_Property']['code'] = '321';
        }
        // Check Auth null request
        elseif(empty($auth['User'])
          || empty($auth['Pwd']))
        {
          $error['Authentification']['type'] = '4';
          $error['Authentification']['code'] = '321';
        }
        else
        {
          // Check Auth
          if (Controller_Vacation_Static::auth($auth) == FALSE)
          {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '9900';
          }
          else
          {
            $AvailStatusMessages = $body->$transaction->AvailStatusMessages->AvailStatusMessage;

            $data = array(
              'hotel_id' => (string)$body->$transaction->AvailStatusMessages->attributes()->HotelCode,
            );

            // data quque
            $i=0;

            foreach ($AvailStatusMessages as $AvailStatusMessage_list)
            {
              $data['available'][$i] = array(
                'BookingLimit' => (string)$AvailStatusMessage_list->attributes()->BookingLimit,
                'Start' => (string)$AvailStatusMessage_list->StatusApplicationControl->attributes()->Start,
                'End' => (string)$AvailStatusMessage_list->StatusApplicationControl->attributes()->End,
                'InvTypeCode' => (string)$AvailStatusMessage_list->StatusApplicationControl->attributes()->InvTypeCode,
                'RatePlanCode' => (string)$AvailStatusMessage_list->StatusApplicationControl->attributes()->RatePlanCode
                );

              // Check Null request
              if(empty($data['available'][$i]['Start'])
                || empty($data['available'][$i]['End'])
                || empty($data['available'][$i]['InvTypeCode'])
                || empty($data['available'][$i]['RatePlanCode']))
              {
                $error['AvailStatusMessage']['type'] = '10';
                $error['AvailStatusMessage']['code'] = '321';
                break;
              }

              // Check Format request
              if(!is_numeric($data['available'][$i]['InvTypeCode'])
                )
              {
                $error['InvTypeCode']['type'] = '3';
                $error['InvTypeCode']['code'] = '402';
                break;
              }

              if(!is_numeric($data['available'][$i]['RatePlanCode'])
                )
              {
                $error['InvTypeCode']['type'] = '3';
                $error['RatePlanCode']['code'] = '249';
                break;
              }

              // Check start and end date format
              if ( ! preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['available'][$i]['Start'])
                || ! preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['available'][$i]['End'])
                )
              {
                $error['Start_or_End']['type'] = '3';
                $error['Start_or_End']['code'] = '400';
                break;
              }

              if(isset($AvailStatusMessage_list->RestrictionStatus))
              {
                $data['available'][$i]['Status'] = strtolower((string)$AvailStatusMessage_list->RestrictionStatus->attributes()->Status) == 'open' ? 0 : 1 ;
                $data['available'][$i]['Restriction'] = (string)$AvailStatusMessage_list->RestrictionStatus->attributes()->Restriction;
              }
              
              if(isset($AvailStatusMessage_list->LengthsOfStay))
              {
                $data['available'][$i]['MinMaxMessageType'] = (string)$AvailStatusMessage_list->LengthsOfStay->children()->attributes()->MinMaxMessageType;
                $data['available'][$i]['Time'] = (string)$AvailStatusMessage_list->LengthsOfStay->children()->attributes()->Time;

                // Check Null request
                if(empty($data['available'][$i]['MinMaxMessageType'])
                  || empty($data['available'][$i]['Time'])
                  )
                {
                  $error['LengthsOfStay']['type'] = '10';
                  $error['LengthsOfStay']['code'] = '321';
                }
              }
              // remove variable nodes
                unset($AvailStatusMessage_list);
              $i++;
            }
          } 
        }
        break;

      case 'OTA_HotelRateAmountNotifRQ':
        $OTA_HotelRateAmountNotifRQ = $body->$transaction->attributes();
        $node_head = array(
          'head' => $transaction,
          'xmlns' => $xmlns,
          'TimeStamp' => (string)$OTA_HotelRateAmountNotifRQ->TimeStamp,
          'Version' => (string)$OTA_HotelRateAmountNotifRQ->Version,
          'EchoToken' => (string)$body->$transaction->attributes()->EchoToken,
          );

        // Check node head null
        if(empty($node_head['xmlns'])
          || empty($node_head['TimeStamp'])
          || empty($node_head['Version'])
          || empty($node_head['EchoToken']))
        {
          $error['Empty_Property']['type'] = '10';
          $error['Empty_Property']['code'] = '321';
        }
        // Check Auth null request
        elseif(empty($auth['User'])
          || empty($auth['Pwd']))
        {
          $error['Authentification']['type'] = '4';
          $error['Authentification']['code'] = '321';
        }
        else
        {
          // Check Auth
          if (Controller_Vacation_Static::auth($auth) == FALSE)
          {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '9900';
          }
          else
          {
            $RateAmountMessages = $body->$transaction->RateAmountMessages->RateAmountMessage;

            $data = array(
              'hotel_id' => (string)$body->$transaction->RateAmountMessages->attributes()->HotelCode,
            );

            // data quque
            $i=0;$j=0;$k=0;

            foreach ($RateAmountMessages as $RateAmountMessage_list)
            {
              $data['rates'][$i] = array(
                'Start' => (string)$RateAmountMessage_list->StatusApplicationControl->attributes()->Start,
                'End' => (string)$RateAmountMessage_list->StatusApplicationControl->attributes()->End,
                'InvTypeCode' => (string)$RateAmountMessage_list->StatusApplicationControl->attributes()->InvTypeCode,
                'RatePlanCode' => (string)$RateAmountMessage_list->StatusApplicationControl->attributes()->RatePlanCode
                );

              // Check Null request
              if(empty($data['rates'][$i]['Start'])
                || empty($data['rates'][$i]['End'])
                || empty($data['rates'][$i]['InvTypeCode'])
                || empty($data['rates'][$i]['RatePlanCode']))
              {
                $error['rates']['type'] = '10';
                $error['rates']['code'] = '321';
                break;
              }

              // Check Format request
              if(!is_numeric($data['rates'][$i]['InvTypeCode'])
                )
              {
                $error['InvTypeCode']['type'] = '3';
                $error['InvTypeCode']['code'] = '402';
                break;
              }

              if(!is_numeric($data['rates'][$i]['RatePlanCode'])
                )
              {
                $error['InvTypeCode']['type'] = '3';
                $error['RatePlanCode']['code'] = '249';
                break;
              }

              // Check start and end date format
              if ( ! preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['rates'][$i]['Start'])
                || ! preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['rates'][$i]['End'])
                )
              {
                $error['Start_or_End']['type'] = '3';
                $error['Start_or_End']['code'] = '400';
                break;
              }

              $Rates = $RateAmountMessage_list->Rates->Rate;

              if(!empty($Rates))
              {
                $j = 0;
                foreach ($Rates as $Rate_count => $Rate) 
                {
                  $BaseByGuestAmts = $Rates->BaseByGuestAmts->BaseByGuestAmt;

                  foreach ($BaseByGuestAmts as $BaseByGuestAmt_count => $BaseByGuestAmt_list) {

                    $data['rates'][$i]['BaseByGuestAmts'][$j] = array(
                      'AgeQualifyingCode' => (string)$BaseByGuestAmt_list->attributes()->AgeQualifyingCode,
                      'NumberOfGuests' => (string)$BaseByGuestAmt_list->attributes()->NumberOfGuests,
                      'AmountAfterTax' => (string)$BaseByGuestAmt_list->attributes()->AmountAfterTax,
                      'CurrencyCode' => (string)$BaseByGuestAmt_list->attributes()->CurrencyCode,
                      );

                    if(!empty($data['rates'][$i]['BaseByGuestAmts'][$j]['NumberOfGuests'])
                      )
                    {
                      $error['BaseByGuestAmts']['type'] = '2';
                      $error['BaseByGuestAmts']['code'] = '9901';
                      break;
                    }
                    
                    if(!is_numeric($data['rates'][$i]['BaseByGuestAmts'][$j]['AgeQualifyingCode'])
                      || !is_numeric($data['rates'][$i]['BaseByGuestAmts'][$j]['AmountAfterTax'])
                      )
                    {
                      $error['BaseByGuestAmts']['type'] = '3';
                      $error['BaseByGuestAmts']['code'] = '459';
                      break;
                    }
                    // if(!empty($data['rates'][$i]['BaseByGuestAmts'][$j]['CurrencyCode'])
                    //   && ($data['rates'][$i]['BaseByGuestAmts'][$j]['CurrencyCode'] != 'USD')
                    //   )
                    // {
                    //   $error['BaseByGuestAmts']['type'] = '3';
                    //   $error['BaseByGuestAmts']['code'] = '400';
                    //   break;
                    // }
                    $j++;
                  }

                  $AdditionalGuestAmounts = $Rates->AdditionalGuestAmounts->AdditionalGuestAmount;

                  if(!empty($AdditionalGuestAmounts))
                  {
                    $k = 0;
                    foreach ($AdditionalGuestAmounts as $AdditionalGuestAmounts_count => $AdditionalGuestAmounts_list) {
                      
                      $data['rates'][$i]['AdditionalGuestAmounts'][$k] = array(
                        'AgeQualifyingCode' => (string)$AdditionalGuestAmounts_list->attributes()->AgeQualifyingCode,
                        'Amount' => (string)$AdditionalGuestAmounts_list->attributes()->Amount,
                        'CurrencyCode' => (string)$AdditionalGuestAmounts_list->attributes()->CurrencyCode,
                        );

                      if(empty($data['rates'][$i]['AdditionalGuestAmounts'][$k]['AgeQualifyingCode'])
                        || empty($data['rates'][$i]['AdditionalGuestAmounts'][$k]['Amount'])
                        )
                      {
                        $error['AdditionalGuestAmounts']['type'] = '10';
                        $error['AdditionalGuestAmounts']['code'] = '321';
                        break;
                      }

                      if(!is_numeric($data['rates'][$i]['AdditionalGuestAmounts'][$k]['AgeQualifyingCode'])
                      || !is_numeric($data['rates'][$i]['AdditionalGuestAmounts'][$k]['Amount'])
                      )
                      {
                        $error['AdditionalGuestAmounts']['type'] = '3';
                        $error['AdditionalGuestAmounts']['code'] = '459';
                        break;
                      }
                      $k++;
                    }
                  }
                }
              }
              $i++;
            }
          } 
        }
        break;

      default:
        $error['450'] = '2';
        break;
    }

    $data = array_merge($data, $node_head);

    // Check error exist
    if (!empty($error)) {
      $errors = Controller_Siteminder_Static::errors($error,$node_head);
      $this->output($errors);
    }
    else
    {
      return ($data);      
    }
  }

  public function OTA_HotelAvailRQ($data)
  {
    // Get Hotel Data
    $data['hotels'] = DB::select(
        array('hotels.id', 'hotel_id'),
        array('hotels.timezone', 'hotel_timezone'),
        array('hotels.child_age_until', 'child_age_until'),

        array('hotel_texts.name', 'hotel_name'),
        array('hotels.currency_id', 'currency_code'),
        array('rooms.id', 'room_id'),
        array('rooms.is_breakfast_included', 'is_breakfast_included'),
        array('room_texts.name', 'name'),
        array(DB::expr('MAX(room_capacities.number_of_adults)'), 'number_of_adults'),
        array(DB::expr('MAX(room_capacities.number_of_children)'), 'number_of_children'),
        array('campaigns.id', 'campaign_id'),
        array('campaigns.is_default', 'campaign_default'),
        array('campaigns.type', 'type'),
        array('campaigns.minimum_number_of_nights', 'min_nights'),
        array('campaigns.minimum_number_of_rooms', 'min_rooms'),
        array('campaigns.days_in_advance', 'early_days'),
        array('campaigns.within_days_of_arrival', 'last_days'),
        array('campaigns.number_of_free_nights', 'free_night'),
        array('campaigns.discount_rate', 'rate'),
        array('campaigns.discount_amount', 'amount'),

        array('room_facilities.is_air_conditioner_available', 'is_air_conditioner_available'),
        array('room_facilities.is_alarm_clock_available', 'is_alarm_clock_available'),
        array('room_facilities.is_balcony_available', 'is_balcony_available'),
        array('room_facilities.is_bathrobe_available', 'is_bathrobe_available'),
        array('room_facilities.is_bathtub_available', 'is_bathtub_available'),
        array('room_facilities.is_body_lotion_available', 'is_body_lotion_available'),
        array('room_facilities.is_cable_tv_available', 'is_cable_tv_available'),
        array('room_facilities.is_coffee_maker_available', 'is_coffee_maker_available'),
        array('room_facilities.is_complimentary_water_bottle_available', 'is_complimentary_water_bottle_available'),
        array('room_facilities.is_cotton_bud_available', 'is_cotton_bud_available'),
        array('room_facilities.is_dvd_player_available', 'is_dvd_player_available'),
        array('room_facilities.is_fan_available', 'is_fan_available'),
        array('room_facilities.is_hair_dryer_available', 'is_hair_dryer_available'),
        array('room_facilities.is_idd_telephone_available', 'is_idd_telephone_available'),
        array('room_facilities.is_independent_shower_room_available', 'is_independent_shower_room_available'),
        array('room_facilities.is_safety_box_available', 'is_safety_box_available'),
        array('room_facilities.is_internet_jack_available', 'is_internet_jack_available'),
        array('room_facilities.is_iron_available', 'is_iron_available'),
        array('room_facilities.is_jacuzzi_available', 'is_jacuzzi_available'),
        array('room_facilities.is_kitchen_set_available', 'is_kitchen_set_available'),
        array('room_facilities.is_living_room_available', 'is_living_room_available'),
        array('room_facilities.is_mini_bar_available', 'is_mini_bar_available'),
        array('room_facilities.is_mosquito_equipment_available', 'is_mosquito_equipment_available'),
        array('room_facilities.is_moveable_shower_head_available', 'is_moveable_shower_head_available'),
        array('room_facilities.is_radio_available', 'is_radio_available'),
        array('room_facilities.is_room_wear_available', 'is_room_wear_available'),
        array('room_facilities.is_shampoo_available', 'is_shampoo_available'),
        array('room_facilities.is_shaver_available', 'is_shaver_available'),
        array('room_facilities.is_soap_available', 'is_soap_available'),
        array('room_facilities.is_toothbrush_available', 'is_toothbrush_available'),
        array('room_facilities.is_towel_available', 'is_towel_available'),
        array('room_facilities.is_tv_available', 'is_tv_available')
        )
      ->from('hotels')
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('hotel_services')->on('hotels.id', '=', 'hotel_services.hotel_id')

      ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('room_facilities')->on('rooms.id', '=', 'room_facilities.room_id')
      ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
      ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('campaigns_languages')->on('campaigns.id', '=', 'campaigns_languages.campaign_id')
      ->join('currencies')->on('hotels.currency_id', '=', 'currencies.id')
      ->join('room_texts', 'LEFT')->on('room_texts.room_id', '=', 'rooms.id')

      ->where('hotel_texts.language_id', '=', Controller_Vacation_Static::default_language())
      ->where('campaigns_languages.language_id', '=', Controller_Vacation_Static::default_language())
      ->where('hotels.id', '=', $data['hotel_id'])
      ->where('rooms.hoterip_campaign', '=', 0)

      ->group_by('hotel_id')
      ->group_by('room_id')
      ->group_by('campaign_id')
      ->execute()
      ->as_array();

    // Check error exist
    if (!count($data['hotels']))
    {

      $error['Hotel_'.$data['hotel_id'].'']['type'] = '12';
      $error['Hotel_'.$data['hotel_id'].'']['code'] = '392';

      $errors = Controller_Siteminder_Static::errors($error,$data);
      $this->output($errors);
    }
    else
    {
      foreach ($data['hotels'] as $key => $hotel) 
      {
        $data['hotels'][$key]['description'] = '';

        // Room services for Descriptions
        if($hotel['is_air_conditioner_available'])
        {
          $data['hotels'][$key]['description'] .= 'Air Conditioner,';
        }
        if($hotel['is_alarm_clock_available'])
        {
          $data['hotels'][$key]['description'] .= 'Alarm Clock,';
        }
        if($hotel['is_balcony_available'])
        {
          $data['hotels'][$key]['description'] .= 'Balcony,';
        }
        if($hotel['is_bathrobe_available'])
        {
          $data['hotels'][$key]['description'] .= 'Bathrobe,';
        }
        if($hotel['is_body_lotion_available'])
        {
          $data['hotels'][$key]['description'] .= 'Body Lotion,';
        }
        if($hotel['is_cable_tv_available'])
        {
          $data['hotels'][$key]['description'] .= 'Cable Tv,';
        }
        if($hotel['is_coffee_maker_available'])
        {
          $data['hotels'][$key]['description'] .= 'Coffe Maker,';
        }
        if($hotel['is_complimentary_water_bottle_available'])
        {
          $data['hotels'][$key]['description'] .= 'Complimentary water bottle,';
        }
        if($hotel['is_cotton_bud_available'])
        {
          $data['hotels'][$key]['description'] .= 'Cotton Bud,';
        }
        if($hotel['is_dvd_player_available'])
        {
          $data['hotels'][$key]['description'] .= 'DVD player,';
        }
        if($hotel['is_fan_available'])
        {
          $data['hotels'][$key]['description'] .= 'Fan,';
        }
        if($hotel['is_hair_dryer_available'])
        {
          $data['hotels'][$key]['description'] .= 'Hair Dryer,';
        }
        if($hotel['is_idd_telephone_available'])
        {
          $data['hotels'][$key]['description'] .= 'IDD telephone,';
        }
        if($hotel['is_independent_shower_room_available'])
        {
          $data['hotels'][$key]['description'] .= 'Independent shower room,';
        }
        if($hotel['is_safety_box_available'])
        {
          $data['hotels'][$key]['description'] .= 'Safety Box,';
        }
        if($hotel['is_internet_jack_available'])
        {
          $data['hotels'][$key]['description'] .= 'Internet Jack,';
        }
        if($hotel['is_iron_available'])
        {
          $data['hotels'][$key]['description'] .= 'Iron,';
        }
        if($hotel['is_jacuzzi_available'])
        {
          $data['hotels'][$key]['description'] .= 'Jacuzzi,';
        }
        if($hotel['is_kitchen_set_available'])
        {
          $data['hotels'][$key]['description'] .= 'Kitchen Set ,';
        }
        if($hotel['is_living_room_available'])
        {
          $data['hotels'][$key]['description'] .= 'Living room,';
        }
        if($hotel['is_mini_bar_available'])
        {
          $data['hotels'][$key]['description'] .= 'Mini room,';
        }
        if($hotel['is_mosquito_equipment_available'])
        {
          $data['hotels'][$key]['description'] .= 'Mosquito equipment,';
        }
        if($hotel['is_moveable_shower_head_available'])
        {
          $data['hotels'][$key]['description'] .= 'Movable shower head,';
        }
        if($hotel['is_radio_available'])
        {
          $data['hotels'][$key]['description'] .= 'Radio,';
        }
        if($hotel['is_room_wear_available'])
        {
          $data['hotels'][$key]['description'] .= 'Room wear,';
        }
        if($hotel['is_shampoo_available'])
        {
          $data['hotels'][$key]['description'] .= 'Shampoo,';
        }
        if($hotel['is_shaver_available'])
        {
          $data['hotels'][$key]['description'] .= 'Shaver,';
        }
        if($hotel['is_soap_available'])
        {
          $data['hotels'][$key]['description'] .= 'Soap,';
        }
        if($hotel['is_toothbrush_available'])
        {
          $data['hotels'][$key]['description'] .= 'Toothbrush,';
        }
        if($hotel['is_towel_available'])
        {
          $data['hotels'][$key]['description'] .= 'Towel,';
        }
        $data['hotels'][$key]['description'] = 
        $data['hotels'][$key]['description'] ? 
        rtrim($data['hotels'][$key]['description'], ',').'.' : '';
      }
      // Build response
     $this->OTA_HotelAvailRS($data);    
    }
  }

  public function OTA_HotelAvailRS($data)
  {
    try
    {
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $envelope = $doc->appendChild($doc->createElement('SOAP-ENV:Envelope'));
      $envelope->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');
      $head = $envelope->appendChild($doc->createElement('SOAP-ENV:Header'));
      $body = $envelope->appendChild($doc->createElement('SOAP-ENV:Body'));
      $body->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

      $response_node = $body->appendChild($doc->createElement(substr($data['head'], 0, -2).'RS'));
      $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
      $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
      $response_node->setAttribute('EchoToken', $data['EchoToken']);
      $response_node->setAttribute('Version', $data['Version']);

      $success = $response_node->appendChild($doc->createElement('Success'));

      $RoomStays = $response_node->appendChild($doc->createElement('RoomStays'));
      foreach ($data['hotels'] as $hotel_count => $hotel) 
      {
        // Add check In and check out date date
        $hotel['check_in'] = date('Y-m-d');
        $hotel['check_out'] = date('Y-m-d');

        $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));
        $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
        $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
        $RoomType->setAttribute('RoomTypeCode', $hotel['room_id']);

        $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
        $RoomDescription->setAttribute('Name', $hotel['name']);

        if(!empty($hotel['description']))
        {
          $text = $RoomTypes->appendChild($doc->createElement('Text', $hotel['description']));
        }

        $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
        $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
        $RatePlan->setAttribute('RatePlanCode', $hotel['campaign_id']);

        // Rate plan name
        $rate_descriptions = Controller_Siteminder_Static::promotion_descriptions($hotel);

        // Rate plan Type
        switch ($hotel['type']) {
          case 2:
            $campaign_type = 'Early bird'.', '.$rate_descriptions;
            break;
          case 3:
            $campaign_type = 'Last Minute'.', '.$rate_descriptions;
            break;
          default:
            $campaign_type = 'Normal'.', '.$rate_descriptions;
            break;
        }

        $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription'));
        $RatePlanDescription->setAttribute('Name', $campaign_type);
        $text = $RatePlanDescription->appendChild($doc->createElement('Text', $rate_descriptions));
      }

      $response = $doc->saveXML();

      $this->output($response);
    }
    catch (Kohana_Exception $e)
    {
      $error['XML_build']['type'] = '2';
      $error['XML_build']['code'] = '450';

      $errors = Controller_Siteminder_Static::errors($error,$data);
      $this->output($errors);
    }
  }

  public function OTA_HotelAvailNotifRQ($data)
  {
    try
    {
      foreach ($data['available'] as $key => $single_data)
      {
        $hotel_id = $data['hotel_id'];
        $room_id = $single_data['InvTypeCode'];
        $campaign_id = $single_data['RatePlanCode'];

        // check campaign and room exist
        $campaign = DB::select()
          ->from('campaigns')
          ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
          ->where('campaigns_rooms.room_id', '=', $room_id)
          ->where('campaigns.id', '=', $campaign_id)
          ->execute()
          ->current();

        if(!$campaign){
          // Rollback transaction
          Database::instance()->rollback();

          // Add error notice
          $error['Exception']['type'] = '2';
          $error['Exception']['code'] = '783';
          break;
        }

        // Create dates array
        $start_date_timestamp = strtotime($single_data['Start']);
        $end_date_timestamp = strtotime($single_data['End']);

        $dates = array();
        for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY)
        {
          $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
        }

        // Load Currency Hotel
        $currency = DB::select('currency_id')->from('hotels')
          ->where('id', '=', $hotel_id)
          ->as_object()
          ->execute()
          ->current();

        $currency_id = $currency ? $currency->currency_id : 1;

        // Load id currency request
        $currency_request = ORM::factory('currency')
          ->where('code', '=', 'USD')
          ->find()->as_array();

        // Factory exchange
        $exchange = Model::factory('exchange');

        // Changed items 
        $changed_prev_items = array();
        $changed_items = array();

        // Item Price
        $item_price = 0;

        // Dates property process
        foreach ($dates as $date)
        {
          // Cancellation expired
          $cancellation_available = ORM::factory('cancellation')
            ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
            ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
            ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
            ->where('campaigns.is_default', '=', 1)
            ->where('rooms.id', '=', $room_id)
            ->where('cancellations.start_date', '<=', $date)
            ->where('cancellations.end_date', '>=', $date)
            ->count_all() > 0;

          if(!$cancellation_available)
          {
            // Rollback transaction
            Database::instance()->rollback();

            // Add error notice
            $error['Please_update_cancellation_policy_which_covered_duration_of_price_and_inventory_beforehand']['type'] = '2';
            $error['Please_update_cancellation_policy_which_covered_duration_of_price_and_inventory_beforehand']['code'] = '161';
            break;
          }
          
          /*
           * Update Item
           */

          $item_campaign_id = $campaign['is_default'] ? 0 : $campaign_id;

          // Get the item
          $item = DB::select()
            ->from('items')
            ->where('room_id', '=', $room_id)
            ->where('campaign_id', '=', $item_campaign_id)
            ->where('date', '=', $date)
            ->as_object()
            ->execute()
            ->current();

          // If item already exist
          if ($item)
          {
            $values = array(
              'room_id' => $room_id,
              'campaign_id' => $item_campaign_id,
              'date' => $date,
              'price' => $item->price,
              'minimum_night' => isset($single_data['MinMaxMessageType']) && ($single_data['MinMaxMessageType'] == 'SetMinLOS') ? $single_data['Time'] : (string) $item->minimum_night,
              'stock' => !empty($single_data['BookingLimit']) ? (string) $single_data['BookingLimit'] : (string) $item->stock,
              'is_blackout' => isset($single_data['Status']) ? (string) $single_data['Status'] : (string) $item->is_blackout,
              'is_campaign_prices' => $item_campaign_id ? 1 : 0,
            );

            // Clone item to write log
            $prev_item = clone $item;

            // Update
            DB::update('items')
              ->set($values)
              ->where('room_id', '=', $room_id)
              ->where('campaign_id', '=', $item_campaign_id)
              ->where('date', '=', $date)
              ->execute();
          }
          else
          {
            $values = array(
              'room_id' => $room_id,
              'campaign_id' => $item_campaign_id,
              'date' => $date,
              'price' => $item_price ? $item_price : 0,
              'minimum_night' => isset($single_data['MinMaxMessageType']) && ($single_data['MinMaxMessageType'] == 'SetMinLOS') ? $single_data['Time'] : 1,
              'stock' => !is_null($single_data['BookingLimit']) ? (string) $single_data['BookingLimit'] : 0,
              'is_blackout' => isset($single_data['Status']) ? (string) $single_data['Status'] : 1,
              'is_campaign_prices' => $item_campaign_id ? 1 : 0,
            );

            // Build prev item class
            $prev_item = new stdClass;
            $prev_item->stock = NULL;
            $prev_item->price = NULL;
            $prev_item->net_price = NULL;
            $prev_item->minimum_night = NULL;
            $prev_item->is_blackout = NULL;
            $prev_item->is_campaign_blackout = NULL;

            // Create new
            DB::insert('items')
              ->columns(array_keys($values))
              ->values(array_values($values))
              ->execute();
          }
          
          // Get the item
          $item = DB::select()
            ->from('items')
            ->where('room_id', '=', $room_id)
            ->where('campaign_id', '=', $item_campaign_id)
            ->where('date', '=', $date)
            ->as_object()
            ->execute()
            ->current();
          
          // Get room
          $room = DB::select()
            ->from('rooms')
            ->where('id', '=', $room_id)
            ->as_object()
            ->execute()
            ->current();
          
          // Set log values
          $log_values = array(
            'admin_id' => A1::instance()->get_user()->id,
            'hotel_id' => $room->hotel_id,
            'action' => 'Changed Item',
            'room_texts' => serialize(DB::select()
              ->from('room_texts')
              ->where('room_id', '=', $room->id)
              ->execute()
              ->as_array('language_id')),
            'text' => 'Date: :date. '
              .'Stock: :prev_stock -> :stock. '
              .'Price: :prev_price -> :price. '
              .'Minimum Night: :prev_minimum_night -> :minimum_night. '
              .'Stop Sell: :prev_is_blackout -> :is_blackout. '
              .'Stop Promotion: :prev_is_campaign_blackout -> :is_campaign_blackout.',
            'data' => serialize(array(
              ':prev_stock' => $prev_item->stock ? $prev_item->stock : 'None',
              ':prev_price' => $prev_item->price ? Num::display($prev_item->price) : 'None',
              ':prev_minimum_night' => $prev_item->minimum_night ? $prev_item->minimum_night : 'None',
              ':prev_is_blackout' => $prev_item->is_blackout ? 'Yes' : 'No',
              ':prev_is_campaign_blackout' => $prev_item->is_campaign_blackout ? 'Yes' : 'No',

              ':date' => date('M j, Y', strtotime($item->date)),
              ':stock' => $item->stock,
              ':price' => Num::display($item->price),
              ':minimum_night' => $item->minimum_night,
              ':is_blackout' => $item->is_blackout ? 'Yes' : 'No',
              ':is_campaign_blackout' => $item->is_campaign_blackout ? 'Yes' : 'No',
            )),
            'created' => time(),
          );
          
          // Write log
          DB::insert('logs')
            ->columns(array_keys($log_values))
            ->values(array_values($log_values))
            ->execute();
          
          // Track changed items
          if ($prev_item->stock !== $item->stock OR 
            $prev_item->price !== $item->price OR 
            $prev_item->minimum_night !== $item->minimum_night OR 
            $prev_item->is_blackout !== $item->is_blackout OR 
            $prev_item->is_campaign_blackout !== $item->is_campaign_blackout)
          {
            $changed_prev_items[$date] = $prev_item;
            $changed_items[$date] = $item;
          }
          
          // Get QA admins emails
          $qa_emails = ORM::factory('admin')
            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
            ->where('admins.role', '=', 'quality-assurance')
            ->where('admins_hotels.hotel_id', '=', $hotel_id)
            ->find_all()
            ->as_array('email', 'name');

          // If there is QA admins
          if (count($qa_emails) > 0)
          {
            ksort($changed_prev_items);
            ksort($changed_items);
            
            // Create email message
            $message = Kostache::factory('item/qa/manage')
              ->set('admin_username', A1::instance()->get_user()->username)
              ->set('admin_name', A1::instance()->get_user()->name)
              ->set('room_name', ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name)
              ->set('changed_prev_items', $changed_prev_items)
              ->set('changed_items', $changed_items)
              ->render();
            
            // Send email
            Email::factory(strtr(':hotel_name - Changed Item', array(
                ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
              )), $message, 'text/html')
              ->from(Kohana::config('application.email'), Kohana::config('application.name'))
              ->to($qa_emails)
              ;//->send();
          }
        }

        // Commit transaction
        Database::instance()->commit();
        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('notice', 'save_success'));
      }
    }
    catch (Kohana_Exception $e)
    {
      // Rollback transaction
      Database::instance()->rollback();
      // Add error notice
      $error['Exception']['type'] = '12';
      $error['Exception']['code'] = '450';
    }
    // Check error exist
    if (!empty($error)) {
      $errors = Controller_Siteminder_Static::errors($error,$data);
      $this->output($errors);
    }
    else
    {
      // Build response
      $this->OTA_HotelAvailNotifRS($data);     
    }
  }

  public function OTA_HotelAvailNotifRS($data)
  {
    try
    {
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $envelope = $doc->appendChild($doc->createElement('SOAP-ENV:Envelope'));
      $envelope->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');
      $head = $envelope->appendChild($doc->createElement('SOAP-ENV:Header'));
      $body = $envelope->appendChild($doc->createElement('SOAP-ENV:Body'));
      $body->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

      $response_node = $body->appendChild($doc->createElement(substr($data['head'], 0, -2).'RS'));
      $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
      $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
      $response_node->setAttribute('EchoToken', $data['EchoToken']);
      $response_node->setAttribute('Version', $data['Version']);

      $success = $response_node->appendChild($doc->createElement('Success'));
      
      $response = $doc->saveXML();

      $this->output($response);
    }
    catch (Kohana_Exception $e)
    {
      $error['XML_build']['type'] = '2';
      $error['XML_build']['code'] = '450';

      $errors = Controller_Siteminder_Static::errors($error,$data);
      $this->output($errors);
    }
  }

  public function OTA_HotelRateAmountNotifRQ($data)
  {
    try
    {
      foreach ($data['rates'] as $key => $single_data)
      {
        $hotel_id = $data['hotel_id'];
        $room_id = $single_data['InvTypeCode'];
        $campaign_id = $single_data['RatePlanCode'];

        // check campaign and room exist
        $campaign = DB::select()
          ->from('campaigns')
          ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
          ->where('campaigns_rooms.room_id', '=', $room_id)
          ->where('campaigns.id', '=', $campaign_id)
          ->execute()
          ->current();

        if(!$campaign){
          // Rollback transaction
          Database::instance()->rollback();

          // Add error notice
          $error['Exception']['type'] = '2';
          $error['Exception']['code'] = '783';
          break;
        }

        // Factory exchange
        $exchange = Model::factory('exchange');

        // Create dates array
        $start_date_timestamp = strtotime($single_data['Start']);
        $end_date_timestamp = strtotime($single_data['End']);

        $dates = array();
        for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY)
        {
          $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
        }

        // Load Currency Hotel
        $currency = DB::select()->from('hotels')
          ->where('id', '=', $hotel_id)
          ->as_object()
          ->execute()
          ->current();

        $currency_id = $currency->currency_id;

        $currency_request = ORM::factory('currency');

          if (!empty($single_data['BaseByGuestAmts'][0]['CurrencyCode'])) 
          {
            $currency_request = $currency_request->where('code', '=', $single_data['BaseByGuestAmts'][0]['CurrencyCode']);
          }
          else
          {
            $currency_request = $currency_request->where('id', '=', $currency_id);
          }

          $currency_request = $currency_request
          ->find()->as_array();

        if(!isset($currency_request['id'])){
          // Rollback transaction
          Database::instance()->rollback();

          // Add error notice
          $error['Exception']['type'] = '3';
          $error['Exception']['code'] = '61';
          break;
        }

        // Changed items 
        $changed_prev_items = array();
        $changed_items = array();

        // Dates property process
        foreach ($dates as $date)
        {
          // Cancellation expired
          $cancellation_available = ORM::factory('cancellation')
            ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
            ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
            ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
            ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
            ->where('campaigns.is_default', '=', 1)
            ->where('rooms.id', '=', $room_id)
            ->where('cancellations.start_date', '<=', $date)
            ->where('cancellations.end_date', '>=', $date)
            ->count_all() > 0;

          if(!$cancellation_available)
          {
            // Rollback transaction
            Database::instance()->rollback();

            // Add error notice
            $error['Please_update_cancellation_policy_which_covered_duration_of_price_and_inventory_beforehand']['type'] = '2';
            $error['Please_update_cancellation_policy_which_covered_duration_of_price_and_inventory_beforehand']['code'] = '161';
            break;
          }

          /*
           * Room
           */
          foreach ($single_data['BaseByGuestAmts'] as $key_BaseByGuestAmts => $BaseByGuestAmt) 
          {
            /*
             * Update Item
             */
            $item_price = round($BaseByGuestAmt['AmountAfterTax'] * ($exchange->to($currency_request['id'], $currency_id)));

            // extrabed item price
            $extrabed_price = 0;
            if(!empty($single_data['AdditionalGuestAmounts']))
            {
              foreach ($single_data['AdditionalGuestAmounts'] as $key_AdditionalGuestAmount => $AdditionalGuestAmount) {
                if ($AdditionalGuestAmount['AgeQualifyingCode'] == 10) 
                {
                  $currency_req_additional = !empty($AdditionalGuestAmount['CurrencyCode']) ? $AdditionalGuestAmount['CurrencyCode'] : $currency_request['code'];

                  $currency_request_additional = ORM::factory('currency')
                    ->where('code', '=', $currency_req_additional)
                    ->find()->as_array();

                    if(!isset($currency_request_additional['id']))
                    {
                      // Rollback transaction
                      Database::instance()->rollback();

                      // Add error notice
                      $error['Exception']['type'] = '3';
                      $error['Exception']['code'] = '61';
                      break 2;
                    }

                  $extrabed_price = round($AdditionalGuestAmount['Amount'] * ($exchange->to($currency_request_additional['id'], $currency_id)), 0);
                  break;
                }
              }
            }
           
            // usable campaign ID
            $item_campaign_id = $campaign['is_default'] ? (string)0 : $campaign_id;

            // Get the item
            $item = DB::select()
              ->from('items')
              ->where('room_id', '=', $room_id)
              ->where('campaign_id', '=', $item_campaign_id)
              ->where('date', '=', $date)
              ->as_object()
              ->execute()
              ->current();

            // If item already exist
            if ($item)
            {
              $values = array(
                'room_id' => $room_id,
                'campaign_id' => $item_campaign_id,
                'date' => $date,
                'price' => $item_price ? $item_price : (string) $item->price,
                'minimum_night' => $item->minimum_night,
                'stock' => $item->stock,
                'is_blackout' => $item->is_blackout,
                'extrabed_item_price'=> $extrabed_price ? $extrabed_price : $item->extrabed_item_price,
                'is_campaign_prices' => $item_campaign_id ? 1 : 0,
              );

              // Clone item to write log
              $prev_item = clone $item;

              // Update
              $update = DB::update('items')
                ->set($values)
                ->where('room_id', '=', $room_id)
                ->where('campaign_id', '=', $item_campaign_id)
                ->where('date', '=', $date)
                ->execute();
            }
            else
            {
              $values = array(
                'room_id' => $room_id,
                'campaign_id' => $item_campaign_id,
                'date' => $date,
                'price' => $item_price ? $item_price : 0,
                'minimum_night' => 1,
                'stock' => 0,
                'is_blackout' => 0,
                'extrabed_item_price'=> $extrabed_price,
                'is_campaign_prices' => $item_campaign_id ? 1 : 0,
              );

              // Build prev item class
              $prev_item = new stdClass;
              $prev_item->stock = NULL;
              $prev_item->price = NULL;
              $prev_item->net_price = NULL;
              $prev_item->minimum_night = NULL;
              $prev_item->is_blackout = NULL;
              $prev_item->is_campaign_blackout = NULL;

              // Create new
              DB::insert('items')
                ->columns(array_keys($values))
                ->values(array_values($values))
                ->execute();
            }
          }
        }
      }
    }
    catch (Kohana_Exception $e)
    {
      // Rollback transaction
      Database::instance()->rollback();
      // Add error notice
      $error['Exception']['type'] = '12';
      $error['Exception']['code'] = '450';
    }
    // Check error exist
    if (!empty($error)) {
      $errors = Controller_Siteminder_Static::errors($error,$data);
      $this->output($errors);
    }
    else
    {
      // Build response
      $this->OTA_HotelRateAmountNotifRS($data);     
    }
  }

  public function OTA_HotelRateAmountNotifRS($data)
  {
    try
    {
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $envelope = $doc->appendChild($doc->createElement('SOAP-ENV:Envelope'));
      $envelope->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');
      $head = $envelope->appendChild($doc->createElement('SOAP-ENV:Header'));
      $body = $envelope->appendChild($doc->createElement('SOAP-ENV:Body'));
      $body->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

      $response_node = $body->appendChild($doc->createElement(substr($data['head'], 0, -2).'RS'));
      $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
      $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
      $response_node->setAttribute('EchoToken', $data['EchoToken']);
      $response_node->setAttribute('Version', $data['Version']);

      $success = $response_node->appendChild($doc->createElement('Success'));

      $response = $doc->saveXML();

      // Success
      Log::instance()->add(Log::INFO, 'Siteminder output Success');
      
      $this->output($response);
    }
    catch (Kohana_Exception $e)
    {
      $error['XML_build']['type'] = '2';
      $error['XML_build']['code'] = '450';

      $errors = Controller_Siteminder_Static::errors($error,$data);
      $this->output($errors);
    }
  }

  public function action_request()
  {
    // Set time limit
    set_time_limit (60);

    $xml_location = file_get_contents('php://input');

    //$xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/list.xml';

    if(!$xml_location)
    {
      $node_head = array(
        'xmlns' => '',
        'TimeStamp' => '',
        'Target' => '',
        'Version' => '',
        'head' => 'Response',
        'EchoToken' => '000',
        );

      $error['Welcome']['type'] = '2';
      $error['Welcome']['code'] = '450';
      $errors = Controller_Siteminder_Static::errors($error,$node_head);
      $this->output($errors);
    }
    else
    {
      try
      {
        /*
        $data = file_get_contents($xml_location);
        $xml_location = trim( stripslashes( $data ));
        */
        $xml = new SimpleXMLElement($xml_location);

        $namespace = $xml->getDocNamespaces(TRUE);
        foreach($namespace as $html_element => $value)
        {
          $xmlns = $value;
        }

        $head = $xml->children('SOAP-ENV', true)->Header->children('wsse', true);
        $body = $xml->children('SOAP-ENV', true)->Body->children();

        $transaction = $body->getName();

        Log::instance()->add(Log::INFO, 'Siteminder Input'.$transaction);
        
        $data = $this->checker($transaction, $head, $body, $xmlns);

        if($data)
        {
          //Called function
          $response = call_user_func(array($this, $transaction), $data);
        }
      }
      catch (Kohana_Exception $e)
      {
        $response = $this->create_error($e->getMessage());
      }
    }
  }

  public function output($response)
  {
            Log::instance()->add(Log::INFO, 'Hoterip response for siteminder');
    Log::instance()->add(Log::INFO, $response);
    // Logout account
    A1::instance()->logout();
    
    $this->response
      ->headers('Content-Type', 'text/xml')
      ->body($response);
  }
}