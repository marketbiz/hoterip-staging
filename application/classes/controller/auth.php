<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller {
	
	public function action_login() 
	{
		if ($this->request->post('submit'))
		{
			if (A1::instance()->login($this->request->post('username'), $this->request->post('password'), (bool) $this->request->post('remember_me')))
			{
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('auth', 'login_failed'));
			}
		}
		
		$view = Kostache::factory('auth/login')
			->set('notice', Notice::render())
			->set('values', $this->request->post());
		
		$this->response->body($view);
	}
	
	public function action_logout()
	{
		A1::instance()->logout();
		
		$this->request->redirect(Route::get('auth')->uri(array('action' => 'login')));
	}
  
  public function action_forgot()
  {
    if ($this->request->post('submit'))
		{
      $email = $this->request->post('email');
      
      $admin = ORM::factory('admin')
        ->where('email', '=', $email)
        ->find();
      
      if ($admin->loaded())
      {
        $admin->reset_key = Text::random('alnum', 16);
        $admin->update();
        
        $message = Kostache::factory('email/forgot')
          ->set('reset_url', Route::url('auth', array('action' => 'reset'), 'http').URL::query(array('username' => $admin->username, 'reset_key' => $admin->reset_key)))
          ->render();
        
        // Send forgot email password
        Email::factory(__('Forgot Password'), $message, 'text/plain')
          ->from(Kohana::config('application.forgot_email_address'), 'Hoterip')
          ->to($email)
          ->send();
        
        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('auth', 'forgot_success'));
      }
      else
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('auth', 'email_not_found'));
      }
		}
		
		$view = Kostache::factory('auth/forgot')
			->set('notice', Notice::render())
			->set('values', $this->request->post());
		
		$this->response->body($view);
  }

  public function action_reset()
  {
    $username = $this->request->query('username');
    $reset_key = $this->request->query('reset_key');
    
    if ($this->request->post('submit'))
    {
      $admin = ORM::factory('admin')
        ->where('username', '=', $username)
        ->where('reset_key', '=', $reset_key)
        ->find();
    
      if ($admin->loaded())
      {
        $admin->reset_key = NULL;
        $admin->password = $this->request->post('password');
        $admin->update();
        
        // Add success notice
        Notice::add(Notice::SUCCESS, Kohana::message('auth', 'reset_success'));
        
        // Redirect to login
        $this->request->redirect(Route::get('auth')->uri(array('action' => 'login')));
      }
      else
      {
        // Add error notice
        Notice::add(Notice::ERROR, Kohana::message('auth', 'reset_failed'));
      }
    }
    
    $view = Kostache::factory('auth/reset')
			->set('notice', Notice::render())
			->set('values', $this->request->post());
		
		$this->response->body($view);
  }
  
}