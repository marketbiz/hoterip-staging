<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Announcement extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('announcement', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			if (A2::instance()->allowed('announcement', 'delete'))
			{
				try
				{
					// Get announcements id
					$announcement_ids = $this->request->post('ids');

			    // Delete hotel announcement
			    DB::delete('hotel_announcements')
			      ->where('announcement_id', 'IN', $announcement_ids)
			      ->execute();

					DB::delete('announcement_texts')
						->where('announcement_id', 'IN', $announcement_ids)
						->execute();
						
					// Delete announcements
					DB::delete('announcements')
						->where('id', 'IN', $announcement_ids)
						->execute();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			}
		}

		$announcements = ORM::factory('hotel_announcement')
			->select(
				array('announcements.title', 'title')
			)
			->join('announcements')->on('announcements.id', '=', 'hotel_announcements.announcement_id')
			->where('hotel_announcements.hotel_id', '=', $this->selected_hotel->id)
			->find_all();

		$this->template->main = Kostache::factory('announcement/manage')
			->set('notice', Notice::render())
			->set('announcements', $announcements);
	}
	
	public function action_add()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('announcement', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage announcements
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'announcement', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();
				$values['hotel_id'] = $this->selected_hotel->id;

				// Create announcement
				$announcement = ORM::factory('announcement')
					->create_announcement($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'announcement', 'action' => 'add')), __('Add another announcement'))));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'announcement', 'action' => 'edit', 'id' => $announcement->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('announcement'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('announcement/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('languages', $languages);
	}
	
	public function action_edit()
	{
		// Get announcement id
		$announcement_id = (int) $this->request->param('id');

		// Get announcement
		$announcement = ORM::factory('announcement')
			->where('id', '=', $announcement_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed('announcement', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage announcements
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'announcement', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();
				$values['hotel_id'] = $this->selected_hotel->id;

				// Update announcement
				ORM::factory('announcement')
					->where('id', '=', $announcement_id)
					->find()
					->update_announcement($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('announcement'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		// Get announcement texts
		$announcement_texts = ORM::factory('announcement_text')
			->where('announcement_id', '=', $announcement_id)
			->find_all()
			->as_array('language_id');

		// Get hotel announcements
		$announcement = ORM::factory('hotel_announcement')
			->select(
				array('announcements.title', 'title')
			)
			->join('announcements')->on('announcements.id', '=', 'hotel_announcements.announcement_id')
			->where('hotel_announcements.announcement_id', '=', $announcement_id)
			->find_all();
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('announcement/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('announcement', $announcement)
			->set('announcement_texts', $announcement_texts)
			->set('languages', $languages);
	}

	public function action_delete()
	{
		// Get announcement id
		$announcement_id = $this->request->param('id');
		
		if (A2::instance()->allowed('announcement', 'delete'))
		{
			try
			{

		    // Delete hotel announcement
		    DB::delete('hotel_announcements')
		      ->where('announcement_id', '=', $announcement_id)
		      ->execute();

		    // Delete announcement
				DB::delete('announcement_texts')
					->where('id', '=', $announcement_id)
					->execute();

				// Delete announcement
				DB::delete('announcements')
					->where('id', '=', $announcement_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}