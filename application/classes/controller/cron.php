<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Cron extends Controller {

    public function __construct() {
//        if (!Kohana::$is_cli) {
//            $request->response = Request::factory(Route::get('error_404')->uri())->execute();
//            die();
//        }
    }

    public function action_ziplog() {
        $list_must_ziped_log = array(
            'vacation'
        );

        ZipLog::factory($list_must_ziped_log)->createPerFileLog(Kohana::config('ziplog.date_interval'));
    }

    public function action_medium() {
        set_time_limit(0);

        $photos = DB::select(
                        'hotel_photos.*', array('hotels.url_segment', 'hotel_url_segment'))
                ->from('hotel_photos')
                ->join('hotels')->on('hotels.id', '=', 'hotel_photos.hotel_id')
                ->as_object()
                ->execute();

        $hotel_photo_directory = Kohana::config('application.hotel_images_directory');

        foreach ($photos as $photo) {
            if (!$photo->medium_basename) {
                // Get the path info
                $path_info = pathinfo($photo->basename);

                // Create new medium basename
                $medium_basename = $path_info['filename'] . '-medium.' . $path_info['extension'];

                // Get the source and destination file
                $source_file_path = $hotel_photo_directory . '/' . $photo->hotel_url_segment . '/' . $photo->large_basename;
                $destination_file_path = $hotel_photo_directory . '/' . $photo->hotel_url_segment . '/' . $medium_basename;

                Log::instance()->add(Log::INFO, 'Start Resizing');
                Log::instance()->add(Log::INFO, 'Source: ' . $source_file_path);
                Log::instance()->add(Log::INFO, 'Destination: ' . $destination_file_path);

                if (file_exists($source_file_path)) {
                    // Resize the image
                    Image::factory($source_file_path)
                            ->resize(320, 240, Image::INVERSE)
                            ->crop(320, 240)
                            ->save($destination_file_path);

                    Log::instance()->add(Log::INFO, 'Resized');

                    // Save the into database
                    DB::update('hotel_photos')
                            ->set(array(
                                'medium_basename' => $medium_basename,
                            ))
                            ->where('id', '=', $photo->id)
                            ->execute();

                    Log::instance()->add(Log::INFO, 'Added to database');
                } else {
                    Log::instance()->add(Log::ERROR, 'Source file not found');
                }
            } else {
                Log::instance()->add(Log::INFO, $photo->basename . ' medium file already exist. Skipped.');
            }
        }
    }

    public function action_mass_email() {
        $emails = DB::select(
                        'email_queues.*', array('email_templates.subject', 'subject'), array('email_templates.template', 'template')
                )
                ->from('email_queues')
                ->join('email_templates')->on('email_queues.template_id', '=', 'email_templates.id')
                ->where('email_queues.status', '=', 0)
                ->limit(50)
                ->order_by('id', 'ASC')
                ->execute();

        if (count($emails) > 0) {
            foreach ($emails as $email) {
                $subject = strtr($email['subject'], array(
                    '{first_name}' => $email['first_name'],
                    '{last_name}' => $email['last_name'],
                    '{point}' => $email['point'],
                ));

                $message = strtr($email['template'], array(
                    '{first_name}' => $email['first_name'],
                    '{last_name}' => $email['last_name'],
                    '{point}' => $email['point'],
                ));

                // Send Email
                Email::factory($subject, $message, 'text/html')
                        ->from('support@hoterip.com', 'Hoterip')
                        ->to($email['to'])
                        ->send();

                DB::update('email_queues')
                        ->set(array(
                            'status' => 1,
                        ))
                        ->where('id', '=', $email['id'])
                        ->execute();
            }
        }
    }

    public function action_delete_templates() {
        $emails = DB::select()
                ->from('email_templates')
                ->execute();

        if (count($emails)) {
            foreach ($emails as $key => $email) {
                $email_queues = DB::select()
                        ->from('email_queues')
                        ->where('email_queues.template_id', '=', $email['id'])
                        ->where('email_queues.status', '=', 0)
                        ->execute();

                if (count($email_queues) == 0) {
                    DB::delete('email_queues')
                            ->where('template_id', '=', $email['id'])
                            ->execute();

                    DB::delete('email_templates')
                            ->where('id', '=', $email['id'])
                            ->execute();
                }
            }
        }
    }

    public function action_update_hotel_rank() {
        // Set no time limit
        set_time_limit(0);

        // Get all hotels
        $hotels = ORM::factory('hotel')
                ->find_all();

        // Get setting
        $setting = (object) ORM::factory('setting')
                        ->find_all()
                        ->as_array('key', 'value');

        // Start transaction
        Database::instance()->begin();

        try {
            foreach ($hotels as $hotel) {
                // Reset rank value
                $rank = 0;
                // Add commission rate factor
                $rank += $hotel->commission_rate * $setting->rank_commission_rate_weight;

                // Get number of bookings
                $number_of_bookings = ORM::factory('booking')
                        ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
                        ->where('rooms.hotel_id', '=', $hotel->id)
                        ->where('created', '>=', strtotime("-$setting->rank_number_of_bookings_days DAY"))
                        ->count_all();

                // Add number of bookings factor
                $rank += $number_of_bookings * $setting->rank_number_of_bookings_weight;

                // Get number of visits
                $number_of_accesses = DB::select(
                                array(DB::expr('COUNT(*)'), 'count')
                        )
                        ->from('accesses')
                        ->where('hotel_id', '=', $hotel->id)
                        ->where('created', '>=', strtotime("-$setting->rank_number_of_accesses_days DAY"))
                        ->execute()
                        ->get('count');

                // Add number of accesses factor
                $rank += $number_of_accesses * $setting->rank_number_of_accesses_weight;

                $review_score = DB::select(
                                array(DB::expr('AVG(score)'), 'average_score')
                        )
                        ->from('reviews')
                        ->where('hotel_id', '=', $hotel->id)
                        ->execute()
                        ->get('average_score');

                // Add review score factor
                $rank += round($review_score, 1) * $setting->rank_review_score_weight;

                // Update hotel rank
                DB::update('hotels')
                        ->set(array(
                            'rank' => $rank
                        ))
                        ->where('id', '=', $hotel->id)
                        ->execute();
            }

            // Commit transaction
            Database::instance()->commit();
        } catch (Exception $e) {
            // Rollback transaction
            Database::instance()->rollback();
        }
    }

    public function action_update_user_point() {
        // Get bookings that have check out date equal today
        $bookings = ORM::factory('booking')
                ->where('check_out_date', '=', date('Y-m-d'))
                ->where('is_canceled', '=', 0)
                ->find_all();

        // Add user points
        foreach ($bookings as $booking) {
            // Find if booking already added
            $booking_point_checked = DB::select(
                                    array(DB::expr('COUNT(*)'), 'count')
                            )
                            ->from('booking_point_checks')
                            ->where('booking_id', '=', $booking->id)
                            ->where('date', '=', $booking->check_out_date)
                            ->execute()
                            ->get('count') > 0;

            // If booking point haven't checked
            if (!$booking_point_checked) {
                // Get prices
                $prices = Model_Booking::calculate_prices($booking->id);

                // Get reward point
                $reward_point = $prices['reward_point'];

                // Update user point
                DB::update('users')
                        ->set(array(
                            'point' => DB::expr('point + :reward_point')
                        ))
                        ->where('id', '=', $booking->user_id)
                        ->bind(':reward_point', $reward_point)
                        ->execute();

                // Insert booking point added
                DB::insert('booking_point_checks', array(
                            'booking_id',
                            'date',
                        ))
                        ->values(array(
                            $booking->id,
                            $booking->check_out_date,
                        ))
                        ->execute();
            }
        }
    }

    public function action_rates() {

        // Set no time limit
        set_time_limit(60);

        // Get from currencies
        $from_currencies = ORM::factory('currency')
                ->find_all();

        // Get to currencies
        $to_currencies = ORM::factory('currency')
                ->find_all();

        foreach ($from_currencies as $from_currency) {
            foreach ($to_currencies as $to_currency) {
                // Get the exchange
                $exchange = ORM::factory('exchange')
                        ->where('from_currency_id', '=', $from_currency->id)
                        ->where('to_currency_id', '=', $to_currency->id)
                        ->find();

                if ($exchange->loaded()) {
                    /// Set XE config
                    $xe = 'xe';
                    // Get the rate
                    $rate = CE::instance($xe)->convert($exchange->amount, $from_currency->code, $to_currency->code);

                    // Change the rate
                    $exchange->rate = $rate ? $rate : $exchange->rate;

                    // Saving Date and status
                    $exchange->date_created = date('Y-m-d');

                    if ($rate == 0) {
                        $exchange->status = 0;
                    } else {
                        $exchange->status = 1;
                    }

                    // Update the new exchange rate
                    $exchange->update();
                }
            }
        }
    }

    public function action_update_exchange_rates() {
        // Set no time limit
        set_time_limit(0);

        // Get from currencies
        $from_currencies = ORM::factory('currency')
                ->find_all();

        // Get to currencies
        $to_currencies = ORM::factory('currency')
                ->find_all();

        foreach ($from_currencies as $from_currency) {
            foreach ($to_currencies as $to_currency) {
                // Get the exchange
                $exchange = ORM::factory('exchange')
                        ->where('from_currency_id', '=', $from_currency->id)
                        ->where('to_currency_id', '=', $to_currency->id)
                        ->find();

                if ($exchange->loaded()) {
                    /// Set XE config
                    $xe = 'google';
                    // Get the rate
                    $rate = CE::instance($xe)->convert($exchange->amount, $from_currency->code, $to_currency->code);

                    // Change the rate
                    $exchange->rate = $rate ? $rate : $exchange->rate;

                    // Saving Date and status
                    $exchange->date_created = date('Y-m-d');

                    if ($rate == 0) {
                        $exchange->status = 0;
                    } else {
                        $exchange->status = 1;
                    }

                    // Update the new exchange rate
                    $exchange->update();
                }
            }
        }
    }

    public function action_rates_oer() {
        // Set no time limit
        set_time_limit(0);

        // Get from currencies
        $from_currencies = ORM::factory('currency')
                ->find_all();

        // Get to currencies
        $to_currencies = ORM::factory('currency')
                ->find_all();

        foreach ($from_currencies as $from_currency) {
            foreach ($to_currencies as $to_currency) {
                // Get the exchange
                $exchange = ORM::factory('exchange')
                        ->where('from_currency_id', '=', $from_currency->id)
                        ->where('to_currency_id', '=', $to_currency->id)
                        ->find();

                if ($exchange->loaded()) {
                    /// Set CE config
                    $driver = 'oerfree';
                    // Get the rate
                    $rate = CE::instance($driver)->convert($exchange->amount, $from_currency->code, $to_currency->code);

                    // Change the rate
                    $exchange->rate = $rate ? $rate : $exchange->rate;

                    // Saving Date and status
                    $exchange->date_created = date('Y-m-d');

                    if ($rate == 0) {
                        $exchange->status = 0;
                    } else {
                        $exchange->status = 1;
                    }

                    // Update the new exchange rate when rate has a true value.
                    if (floatval($rate)) {
                        $msg = "CEDriver: $driver | Conversion success for $exchange->amount $from_currency->code to $to_currency->code : $rate";

                        // Update database
                        $exchange->update();
                    } else {
                        $msg = "CEDriver: $driver | Conversion not allowed with current plan from $from_currency->code to $to_currency->code, will be using old rate: $exchange->rate";
                    }
                    // write to log
                    Log::instance()->add(Log::NOTICE, $msg);

                    // echo out
                    echo $msg . "<br/>";
                }
            }
        }
    }

    public function action_return_stocks() {
        $booking_pendings = DB::select()
                ->from('booking_pendings')
                ->where('created', '<', time() - Date::MINUTE * 120)
                ->as_object()
                ->execute();

        // Start transaction
        Database::instance()->begin();

        try {
            foreach ($booking_pendings as $booking_pending) {
                // Get item ids
                $item_ids = unserialize($booking_pending->item_ids);

                if ($item_ids AND is_array($item_ids)) {
                    
                    $room = DB::select()->
                            from('rooms')
                            ->join('itemb2bs')->on('itemb2bs.room_id','=','rooms.id')
                            ->where('itemb2bs.id','=',$item_ids[0])
                            ->execute()
                            ->as_array();
                    
                    if($room[0]['share_stock'] > 0){
                    
                    // Update items, return stock
                    DB::update('items')
                            ->set(array(
                                'stock' => DB::expr("stock + $booking_pending->number_of_rooms")
                            ))
                            ->where('room_id', '=', $room[0]['room_id'])
                            ->where('date','=',$room[0]['date'])
                            ->execute();
                    }else{
                        // Update items, return stock
                    DB::update('itemb2bs')
                            ->set(array(
                                'stock' => DB::expr("stock + $booking_pending->number_of_rooms")
                            ))
                            ->where('id', 'IN', $item_ids)
                            ->execute();
                    }
                }

                // Delete booking id
                DB::delete('booking_pendings')
                        ->where('id', '=', $booking_pending->id)
                        ->execute();
            }

            // Commit transaction
            Database::instance()->commit();

            foreach ($booking_pendings as $booking_pending) {
                //get booking_pending_data
                $booking_data = unserialize($booking_pending->data);

                $vacation_triger = array(
                    'hotel_id' => $booking_data['hotel_id'],
                    'room_id' => $booking_data['room_id'],
                    'campaign_id' => $booking_data['campaign_id'],
                    // 'start_date' =>$booking_data['check_in'], 
                    // 'end_date' =>$booking_data['check_out'],
                    'item_id' => $booking_data['item_ids']
                );

                Controller_Vacation_Trigger::trigger_Vacation($vacation_triger);
            }
        } catch (Exception $e) {
            // Rollback transaction
            Database::instance()->rollback();
            // Log
            Log::instance()->add(Log::ERROR, Kohana_Exception::text($e));
        }
    }

    // CSV
    public function action_csv() {
        // Set Default Language
        $default_language_id = ORM::factory('language')
                        ->where('code', '=', 'en-us')
                        ->find()
                ->id;

        $directory = DOCROOT . 'media/dropbox/csv/';

        $hotels = ORM::factory('hotel')
                ->select(
                        array('hotels.id', 'hotel_id'), array('hotel_texts.name', 'hotel_name'), array('hotels.star', 'hotel_star'), array('hotels.number_of_rooms', 'hotel_rooms'), array('hotels.address', 'hotel_address'), array('hotel_facilities.is_banquet_room_available', 'banquet_room'), array('hotel_facilities.is_bars_available', 'bars'), array('hotel_facilities.is_business_center_available', 'business_center'), array('hotel_facilities.is_car_parking_available', 'car_parking'), array('hotel_facilities.is_casino_available', 'casino'), array('hotel_facilities.is_clinic_available', 'clinic'), array('hotel_facilities.is_club_lounge_available', 'club_lounge'), array('hotel_facilities.is_coffee_shop_available', 'coffee_shop'), array('hotel_facilities.is_departure_lounge_available', 'departure_lounge'), array('hotel_facilities.is_disabled_facilities_available', 'disabled_facilities'), array('hotel_facilities.is_elevator_available', 'elevator'), array('hotel_facilities.is_garden_available', 'garden'), array('hotel_facilities.is_gym_available', 'gym'), array('hotel_facilities.is_gift_shop_available', 'gift_shop'), array('hotel_facilities.is_golf_course_available', 'golf_course'), array('hotel_facilities.is_hair_salon_available', 'hair_salon'), array('hotel_facilities.is_jacuzzi_available', 'jacuzzi'), array('hotel_facilities.is_karaoke_room_available', 'karaoke_room'), array('hotel_facilities.is_kids_club_available', 'kids_club'), array('hotel_facilities.is_kids_pool_available', 'kids_pool'), array('hotel_facilities.is_library_available', 'library'), array('hotel_facilities.is_luggage_room_available', 'luggage_room'), array('hotel_facilities.is_meeting_room_available', 'meeting_room'), array('hotel_facilities.is_night_club_available', 'night_club'), array('hotel_facilities.is_private_beach_available', 'private_beach'), array('hotel_facilities.is_poolside_bar_available', 'poolside_bar'), array('hotel_facilities.is_restaurant_available', 'restaurant'), array('hotel_facilities.is_safety_box_available', 'safety_box'), array('hotel_facilities.is_sauna_available', 'sauna'), array('hotel_facilities.is_spa_available', 'spa'), array('hotel_facilities.is_squash_court_available', 'squash_court'), array('hotel_facilities.is_steam_room_available', 'steam_room'), array('hotel_facilities.is_swimming_pool_available', 'swimming_pool'), array('hotel_facilities.is_tennis_court_available', 'tennis_court'), array('cities.url_segment', 'city_segment'), array('countries.url_segment', 'country_segment'), array('city_texts.name', 'city_name'), array('district_texts.name', 'district_name'), array('country_texts.name', 'country_name'), array('hotels.coordinate', 'hotel_coordinate'), array('hotels.url_segment', 'hotel_url'), array('hotels.telephone', 'hotel_phone'), array('hotels.fax', 'hotel_fax'), array('hotels.check_in_time', 'hotel_check_in'), array('hotels.check_out_time', 'hotel_check_out'), array('hotels.opening_year', 'hotel_builded_year'), array('hotels.renovation_year', 'hotel_renovation_year')
                )
                ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
                ->join('hotel_facilities')->on('hotel_facilities.hotel_id', '=', 'hotels.id')
                ->join('cities')->on('cities.id', '=', 'hotels.city_id')
                ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
                ->join('districts')->on('districts.id', '=', 'hotels.district_id')
                ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
                ->join('countries')->on('countries.id', '=', 'cities.country_id')
                ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
                ->where('hotel_texts.language_id', '=', $default_language_id)
                ->where('city_texts.language_id', '=', $default_language_id)
                ->where('district_texts.language_id', '=', $default_language_id)
                ->where('country_texts.language_id', '=', $default_language_id)
                ->order_by('hotel_texts.name', 'ASC')
                ->order_by('city_texts.name', 'ASC')
                ->find_all();

        $csv = CSV::factory($directory . 'hotels.csv')
                ->titles(array(
            __('propertyid'),
            __('propertyname'),
            __('starratings'),
            __('numberofrooms'),
            __('address'),
            __('city'),
            __('state'),
            __('country'),
            __('latitude'),
            __('longitude'),
            __('amenities'),
            __('url'),
            __('phone'),
            __('fax'),
            __('checkintime'),
            __('checkouttime'),
            __('builtyear'),
            __('renovated')));

        foreach ($hotels as $hotel) {
            //Latitude and longitude
            $location = explode(",", $hotel->hotel_coordinate);
            $latitude = reset($location);
            $longitude = end($location);

            //Amenties
            $facilities = array();
            if ($hotel->banquet_room == 1) {
                $facilities [] = 'Banquet Room';
            }
            if ($hotel->banquet_room == 1) {
                $facilities [] = 'Bars';
            }
            if ($hotel->banquet_room == 1) {
                $facilities [] = 'Business Center';
            }
            if ($hotel->car_parking == 1) {
                $facilities [] = 'Car Parking';
            }
            if ($hotel->casino == 1) {
                $facilities [] = 'Casino';
            }
            if ($hotel->clinic == 1) {
                $facilities [] = 'Clinic';
            }
            if ($hotel->club_lounge == 1) {
                $facilities [] = 'Club Lounge';
            }
            if ($hotel->coffee_shop == 1) {
                $facilities [] = 'Coffee Shop';
            }
            if ($hotel->departure_lounge == 1) {
                $facilities [] = 'Departure Lounge';
            }
            if ($hotel->disabled_facilities == 1) {
                $facilities [] = 'Disabled Facilities';
            }
            if ($hotel->elevator == 1) {
                $facilities [] = 'Elevator';
            }
            if ($hotel->banquet_room == 1) {
                $facilities [] = 'Garden';
            }
            if ($hotel->gym == 1) {
                $facilities [] = 'Gym';
            }
            if ($hotel->gift_shop == 1) {
                $facilities [] = 'Gift Shop';
            }
            if ($hotel->golf_course == 1) {
                $facilities [] = 'Golf Course';
            }
            if ($hotel->hair_salon == 1) {
                $facilities [] = 'Hair Salon';
            }
            if ($hotel->jacuzzi == 1) {
                $facilities [] = 'Jacuzzi';
            }
            if ($hotel->karaoke_room == 1) {
                $facilities [] = 'Karaoke Room';
            }
            if ($hotel->kids_club == 1) {
                $facilities [] = 'Kids Club';
            }
            if ($hotel->kids_pool == 1) {
                $facilities [] = 'Kids Pool';
            }
            if ($hotel->library == 1) {
                $facilities [] = 'Library';
            }
            if ($hotel->luggage_room == 1) {
                $facilities [] = 'Luggage Room';
            }
            if ($hotel->meeting_room == 1) {
                $facilities [] = 'Meeting Room';
            }
            if ($hotel->night_club == 1) {
                $facilities [] = 'Night Club';
            }
            if ($hotel->private_beach == 1) {
                $facilities [] = 'Private Beach';
            }
            if ($hotel->poolside_bar == 1) {
                $facilities [] = 'Poolside Bar';
            }
            if ($hotel->restaurant == 1) {
                $facilities [] = 'Restaurant';
            }
            if ($hotel->safety_box == 1) {
                $facilities [] = 'Safety Box';
            }
            if ($hotel->sauna == 1) {
                $facilities [] = 'Sauna';
            }
            if ($hotel->spa == 1) {
                $facilities [] = 'Spa';
            }
            if ($hotel->squash_court == 1) {
                $facilities [] = 'Squash Court';
            }
            if ($hotel->steam_room == 1) {
                $facilities [] = 'Steam Room';
            }
            if ($hotel->swimming_pool == 1) {
                $facilities [] = 'Swimming Pool';
            }
            if ($hotel->tennis_court == 1) {
                $facilities [] = 'Tennis Court';
            }

            if (!empty($facilities)) {
                $amenities = implode(";", $facilities);
            }

            //Hotel Url
            $url = Kohana::config('application.hotel_url.live') . $hotel->country_segment
                    . '/' . $hotel->city_segment . '/' . $hotel->hotel_url;

            $csv->values(array(
                $hotel->id,
                $hotel->hotel_name,
                $hotel->hotel_star,
                $hotel->hotel_rooms,
                $hotel->hotel_address,
                $hotel->city_name,
                $hotel->district_name,
                $hotel->country_name,
                $latitude,
                $longitude,
                $amenities,
                $url,
                $hotel->hotel_phone,
                $hotel->hotel_fax,
                $hotel->hotel_check_in,
                $hotel->hotel_check_out,
                $hotel->hotel_builded_year,
                $hotel->hotel_renovation_year
            ));
        }

        $csv->save();
    }

    public function action_VacationDailyActivation() {
        $hotels = DB::select('value')
                ->from('settings')
                ->where('key', '=', 'vacation_hotel_active_list')
                ->execute()
                ->current();

        if (empty($hotels)) {
            exit();
        }

        $hotels = json_decode($hotels['value']);

        foreach ($hotels as $hotel) {
            DB::insert('api_vacation_updates')
                    ->columns(array('hotel_id', 'type_trigger', 'status'))
                    ->values(array($hotel, 1, 0))
                    ->execute();
        }
    }

    public function action_PromotionValidToVacation() {
        $now = date('Y-m-d');

        $campaigns = DB::select(
                        array('rooms.hotel_id', 'hotel_id'), array('campaigns.id', 'campaign_id'), array('campaigns.booking_start_date'), array('campaigns.booking_end_date')
                )
                ->from('campaigns')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->where('is_default', '!=', 1)
                ->where('booking_start_date', '=', $now)
                ->or_where('booking_end_date', '=', Date::formatted_time("$now - 1 days", 'Y-m-d'))
                ->execute()
                ->as_array('campaign_id');

        if (empty($campaigns)) {
            exit();
        }

        foreach ($campaigns as $campaign) {

            DB::insert('api_vacation_updates')
                    ->columns(array('hotel_id', 'campaign_id', 'type_trigger', 'status'))
                    ->values(array($campaign['hotel_id'], $campaign['campaign_id'], 2, 0))
                    ->execute();
        }
    }

    public function action_lastminute_queues() {
        // Load Config
        if (Kohana::$environment == Kohana::PRODUCTION) {
            $config = Kohana::$config->load('api')->vacation;
        } else {
            $config = Kohana::$config->load('api')->vacation_test;
        }

        $now = date('Y-m-d');
        //Truncate Database
        DB::query(Database::DELETE, "TRUNCATE TABLE `api_vacation_earlybird_queues`")->execute();

        //get datas for api_vacation_earlybird_queues
        $lastminute_queues = DB::select(
                        array('campaigns.id', 'campaign_id'), array('rooms.id', 'room_id'), array('hotels.id', 'hotel_id'), array('campaigns.booking_start_date', 'booking_start_date'), array('campaigns.booking_end_date', 'booking_end_date')
                )
                ->from('campaigns_rooms')
                ->and_where_open()
                ->where('campaigns.type', '=', 3)
                ->where('hotels.id', 'IN', $config['allowed_hotels'])
                ->where('rooms.hoterip_campaign', '=', 0)
                ->where('rooms.is_breakfast_included', '=', 1)
                ->where('hotels.active', '=', 1)
                ->where('campaigns.booking_end_date', '>=', $now)
                ->and_where_close()
                ->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                ->order_by('room_id')
                ->order_by('hotel_id')
                ->execute()
                ->as_array()
        ;

        //insert to api_vacation_earlybird_queues
        foreach ($lastminute_queues as $last_minute) {
            DB::insert('api_vacation_lastminute_queues', array_keys($last_minute)
                    )
                    ->values(array_values($last_minute))
                    ->execute();
        }
    }

    public function action_lastminute() {
        $now = date('Y-m-d');

        //get datas from api_vacation_earlybird_queues
        $posts = DB::select(
                        array('api_vacation_lastminute_queues.id'), array('api_vacation_lastminute_queues.room_id'), array('api_vacation_lastminute_queues.hotel_id'), array('api_vacation_lastminute_queues.campaign_id'), array('api_vacation_lastminute_queues.booking_start_date', 'booking_start_date'), array('api_vacation_lastminute_queues.booking_end_date', 'booking_end_date')
                )
                ->from('api_vacation_lastminute_queues')
                ->where('api_vacation_lastminute_queues.booking_end_date', '>', $now)
                ->limit(10)
                ->execute()
                ->as_array();

        //delete api_vacation_earlybird_queues if booking_end_date < today
        DB::delete('api_vacation_lastminute_queues')
                ->where('api_vacation_lastminute_queues.booking_end_date', '<', $now)
                ->execute()
        ;

        if (empty($posts)) {
            exit();
        }

        foreach ($posts as $post) {
            //set time limit
            $limit_days = 180; //day
            $post['start_date'] = $now;
            $post['end_date'] = Date::formatted_time("$now + 180 days", 'Y-m-d');

            //check booking_start_date until booking_end_date >= 180 days ?
            $date_start = date_create($post['start_date']);
            $date_end = date_create($post['booking_end_date']);
            $diff = date_diff($date_start, $date_end)->days;


            //get datas items
            $datas = DB::select(
                            array('items.stock', 'stock'), array('items.date', 'date'), array('items.campaign_id', 'campaign_id'), array('items.minimum_night', 'minimum_night'), array('items.is_blackout', 'is_blackout'), array('items.is_campaign_blackout', 'is_campaign_blackout')
                    )
                    ->from('items')
                    ->where('campaign_id', '=', 0)
                    ->where('room_id', '=', $post['room_id']);

            if ($diff >= $limit_days) {
                $datas = $datas
                        ->and_where_open()
                        ->where('items.date', '>=', $post['start_date'])
                        ->where('items.date', '<=', $post['end_date'])
                        ->where_close();
            } else {
                $datas = $datas
                        ->and_where_open()
                        ->where('items.date', '>=', $post['start_date'])
                        ->where('items.date', '<=', $post['booking_end_date'])
                        ->where_close();
            }
            $datas = $datas
                    ->order_by('date')
                    ->execute()
                    ->as_array('date')
            ;

            //if datas item not empty
            if (!empty($datas)) {

                //+++++++++++++++++++++++++ Generate Date Post +++++++++++++++++++++++++
                if ($diff < $limit_days) {
                    $limit_days = count($datas);
                }

                for ($i = 0; $i < $limit_days; $i++) {
                    $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                    $post['dates'][$date] = $date;
                }

                for ($i = 0; $i < $limit_days; $i++) {
                    $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                    $post['stocks'][$date] = $datas[$date]['stock'];

                    if (empty($post['stocks'][$date])) {
                        $post['stocks'][$date] = 0;
                    }
                }

                for ($i = 0; $i < $limit_days; $i++) {
                    $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                    $post['minimum_nights'][$date] = $datas[$date]['minimum_night'];

                    if (empty($post['minimum_nights'][$date])) {
                        $post['minimum_nights'][$date] = 0;
                    }
                }

                foreach ($datas as $date => $value) {
                    if ($value['is_blackout'] == 1) {
                        $post['is_blackouts'][$date] = $value['is_blackout'];
                    }
                }

                foreach ($datas as $date => $value) {
                    if ($value['is_campaign_blackout'] == 1) {
                        $post['is_campaign_blackouts'][$date] = $value['is_blackout'];
                    }
                }

                $post['submit'] = 'Submit';
                //+++++++++++++++++++++++++ End Generate Date Post +++++++++++++++++++++++++
                //send to api vacation        
                Controller_Vacation_Notifications_Availability::item($post, TRUE);
            }
            //if success delete data from api_vacation_earlybird_queues
            DB::delete('api_vacation_lastminute_queues')
                    ->where('api_vacation_lastminute_queues.id', '=', $post['id'])
                    ->execute()
            ;
        }
    }

    public function action_early_bird_queues() {
        // Load Config
        if (Kohana::$environment == Kohana::PRODUCTION) {
            $config = Kohana::$config->load('api')->vacation;
        } else {
            $config = Kohana::$config->load('api')->vacation_test;
        }

        $now = date('Y-m-d');
        //Truncate Database
        DB::query(Database::DELETE, "TRUNCATE TABLE `api_vacation_earlybird_queues`")->execute();


        //get datas for api_vacation_earlybird_queues
        $early_birds_queues = DB::select(
                        array('campaigns.id', 'campaign_id'), array('rooms.id', 'room_id'), array('hotels.id', 'hotel_id'), array('campaigns.booking_start_date', 'booking_start_date'), array('campaigns.booking_end_date', 'booking_end_date')
                )
                ->from('campaigns_rooms')
                ->and_where_open()
                ->where('campaigns.type', '=', 2)
                ->where('hotels.id', 'IN', $config['allowed_hotels'])
                ->where('rooms.hoterip_campaign', '=', 0)
                ->where('rooms.is_breakfast_included', '=', 1)
                ->where('hotels.active', '=', 1)
                ->where('campaigns.booking_end_date', '>=', $now)
                ->and_where_close()
                ->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
                ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                ->order_by('room_id')
                ->order_by('hotel_id')
                ->execute()
                ->as_array()
        ;

        //insert to api_vacation_earlybird_queues
        foreach ($early_birds_queues as $early_bird) {
            DB::insert('api_vacation_earlybird_queues', array_keys($early_bird)
                    )
                    ->values(array_values($early_bird))
                    ->execute();
        }
    }

    public function action_Vacation_Update() {
        set_time_limit(0);

        // get hotel list
        $config = Kohana::$config->load('api')->vacation;
        $hotel_config = $config['allowed_hotels'];

        $api_vacation_updates = DB::select(
                        'api_vacation_updates.*'
                )
                ->from('api_vacation_updates')
                ->where('status', '=', 1)
                ->limit(5)
                ->execute()
                ->as_array();

        if (empty($api_vacation_updates)) {
            // DB::query(Database::DELETE, "TRUNCATE TABLE `api_vacation_updates`")->execute();
            exit();
        }

        foreach ($api_vacation_updates as $vacation) {
            $exectime = date('Y/m/d h:i:s');
            CustomLog::factory()->add(2, 'DEBUG', 'Vacation Update: [START|' . $exectime . '] Hotel ID: ' . $vacation['hotel_id']);

            //get room_not valid
            $room_not_valid = DB::select('rooms.id')
                    ->from('rooms')
                    ->where_open()
                    ->or_where('rooms.is_breakfast_included', '=', 0)
                    ->or_where('rooms.hoterip_campaign', '=', 1)
                    ->where_close()
                    ->where('rooms.id', '=', $vacation['room_id'])
                    ->execute()
                    ->as_array()
            ;

            //room not valid then delete and continue to next room
            if (!empty($room_not_valid) || !Controller_Vacation_Trigger::checkHotelVacation($vacation['hotel_id'])) {
                DB::delete('api_vacation_updates')
                        ->where('api_vacation_updates.id', '=', $vacation['id'])
                        ->execute();

                CustomLog::factory()->add(2, 'DEBUG', 'Vacation Update: [SKIP|' . $exectime . '|' . date('Y/m/d h:i:s') . '] Invalid Room ID or Hotel ID: ' . $vacation['hotel_id']);
                echo 'Vacation Update: [SKIP|' . $exectime . '|' . date('Y/m/d h:i:s') . '] Invalid Room ID or Hotel ID: ' . $vacation['hotel_id'] . '<br/>' . "\r\n";

                continue;
            }

            if ($vacation['type_trigger'] == NULL) {
                Controller_Vacation_Trigger::updatePerItem($vacation);
                CustomLog::factory()->add(2, 'DEBUG', 'Vacation Update: [TYPE:NULL] Hotel ID: ' . $vacation['hotel_id']);
            } elseif ($vacation['type_trigger'] == 1 //Reactive/Active hotel
                    || $vacation['type_trigger'] == 2 //plan edit
                    || $vacation['type_trigger'] == 3 //siteminder
                    || $vacation['type_trigger'] == 4 //plandelete
                    || $vacation['type_trigger'] == 5 //restrictions
                    || $vacation['type_trigger'] == 7 //rategain
            ) {
                Controller_Vacation_Trigger::updateDatePeriod($vacation);
                CustomLog::factory()->add(2, 'DEBUG', 'Vacation Update: [TYPE:1/2/3/4/5] Hotel ID: ' . $vacation['hotel_id']);
            } elseif ($vacation['type_trigger'] == 4) {
                Controller_Vacation_Trigger::promotionDelete($vacation);
                CustomLog::factory()->add(2, 'DEBUG', 'Vacation Update: [TYPE:4] Hotel ID: ' . $vacation['hotel_id']);
            } elseif ($vacation['type_trigger'] == 6) {
                Controller_Vacation_Trigger::roomDeleteV2($vacation);
                CustomLog::factory()->add(2, 'DEBUG', 'Vacation Update: [TYPE:6] Hotel ID: ' . $vacation['hotel_id']);
            }

            CustomLog::factory()->add(2, 'DEBUG', 'Vacation Update: [END|' . $exectime . '|' . date('Y/m/d h:i:s') . '] Hotel ID: ' . $vacation['hotel_id']);
            echo 'Vacation Update: [END|' . $exectime . '|' . date('Y/m/d h:i:s') . '] Hotel ID: ' . $vacation['hotel_id'] . '<br/>' . "\r\n";

            //if success delete data from api_vacation_updates
            DB::delete('api_vacation_updates')
                    ->where('api_vacation_updates.id', '=', $vacation['id'])
                    ->execute();
            CustomLog::factory()->add(2, 'DEBUG', 'Deleted Hotel ID ' . $vacation['hotel_id']);
        }
    }

    public function action_vacationCheckStat() {
        $limit = 20;
        $api_vacation = DB::select(
                        array('id', 'ids')
                )
                ->from('api_vacation_updates')
                ->where('status', '=', 1)
                ->limit($limit)
                ->execute()
                ->as_array();

        $lim_update = $limit - count($api_vacation);

        if ($lim_update != 0) {
            DB::update('api_vacation_updates')
                    ->set(array(
                        'status' => 1
                    ))
                    ->where('status', '=', 0)
                    ->limit($lim_update)
                    ->execute();
        }
    }

    public function action_early_bird() {
        $now = date('Y-m-d');

        //get datas from api_vacation_earlybird_queues
        $posts = DB::select(
                        array('api_vacation_earlybird_queues.id'), array('api_vacation_earlybird_queues.room_id'), array('api_vacation_earlybird_queues.hotel_id'), array('api_vacation_earlybird_queues.campaign_id'), array('api_vacation_earlybird_queues.booking_start_date', 'booking_start_date'), array('api_vacation_earlybird_queues.booking_end_date', 'booking_end_date')
                )
                ->from('api_vacation_earlybird_queues')
                ->where('api_vacation_earlybird_queues.booking_end_date', '>', $now)
                ->limit(10)
                ->execute()
                ->as_array();

        //delete api_vacation_earlybird_queues if booking_end_date < today
        DB::delete('api_vacation_earlybird_queues')
                ->where('api_vacation_earlybird_queues.booking_end_date', '<', $now)
                ->execute()
        ;

        if (empty($posts)) {
            exit();
        }

        foreach ($posts as $post) {
            //set time limit
            $limit_days = 180; //day
            $post['start_date'] = $now;
            $post['end_date'] = Date::formatted_time("$now + 180 days", 'Y-m-d');

            //check booking_start_date until booking_end_date >= 180 days ?
            $date_start = date_create($post['start_date']);
            $date_end = date_create($post['booking_end_date']);
            $diff = date_diff($date_start, $date_end)->days;


            //get datas items
            $datas = DB::select(
                            array('items.stock', 'stock'), array('items.date', 'date'), array('items.campaign_id', 'campaign_id'), array('items.minimum_night', 'minimum_night'), array('items.is_blackout', 'is_blackout'), array('items.is_campaign_blackout', 'is_campaign_blackout')
                    )
                    ->from('items')
                    ->where('campaign_id', '=', 0)
                    ->where('room_id', '=', $post['room_id']);

            if ($diff >= $limit_days) {
                $datas = $datas
                        ->and_where_open()
                        ->where('items.date', '>=', $post['start_date'])
                        ->where('items.date', '<=', $post['end_date'])
                        ->where_close();
            } else {
                $datas = $datas
                        ->and_where_open()
                        ->where('items.date', '>=', $post['start_date'])
                        ->where('items.date', '<=', $post['booking_end_date'])
                        ->where_close();
            }
            $datas = $datas
                    ->order_by('date')
                    ->execute()
                    ->as_array('date')
            ;

            //if datas item not empty
            if (!empty($datas)) {

                //+++++++++++++++++++++++++ Generate Date Post +++++++++++++++++++++++++
                if ($diff < $limit_days) {
                    $limit_days = count($datas);
                }

                for ($i = 0; $i < $limit_days; $i++) {
                    $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                    $post['dates'][$date] = $date;
                }

                for ($i = 0; $i < $limit_days; $i++) {
                    $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                    $post['stocks'][$date] = $datas[$date]['stock'];

                    if (empty($post['stocks'][$date])) {
                        $post['stocks'][$date] = 0;
                    }
                }

                for ($i = 0; $i < $limit_days; $i++) {
                    $date = Date::formatted_time("$now + $i days", 'Y-m-d');
                    $post['minimum_nights'][$date] = $datas[$date]['minimum_night'];

                    if (empty($post['minimum_nights'][$date])) {
                        $post['minimum_nights'][$date] = 0;
                    }
                }

                foreach ($datas as $date => $value) {
                    if ($value['is_blackout'] == 1) {
                        $post['is_blackouts'][$date] = $value['is_blackout'];
                    }
                }

                foreach ($datas as $date => $value) {
                    if ($value['is_campaign_blackout'] == 1) {
                        $post['is_campaign_blackouts'][$date] = $value['is_blackout'];
                    }
                }

                $post['submit'] = 'Submit';
                //+++++++++++++++++++++++++ End Generate Date Post +++++++++++++++++++++++++
                //send to api vacation        
                Controller_Vacation_Notifications_Availability::item($post, TRUE);
            }
            //if success delete data from api_vacation_earlybird_queues
            DB::delete('api_vacation_earlybird_queues')
                    ->where('api_vacation_earlybird_queues.id', '=', $post['id'])
                    ->execute()
            ;
        }
    }

    // Wego queque
    public function action_hotels_queues() {
        // Only allow Access from 0.0.0.0 IP
        // if (Request::$client_ip != '0.0.0.0')
        // Only allow Access from background (crontab)
        // CLI: php webapps/hoterip_ycs/index.php --uri=cron/hotels_queues -q
        if (!Kohana::$is_cli) {
            $request->response = Request::factory(Route::get('error_404')->uri())->execute();
            die();
        }

        // Truncate database
        DB::query(Database::DELETE, "TRUNCATE TABLE `api_wego_queues`")->execute();
        DB::query(Database::DELETE, "TRUNCATE TABLE `api_wego`")->execute();

        $data_set = array();
        $hotels_row = DB::select('id', 'city_id')
                ->from('hotels')
                ->where('hotels.active', '=', 1)
                ->execute()
                ->as_array();

        if (count($hotels_row) > 0) {
            // Insert to db
            $query = DB::insert('api_wego_queues', array('hotel_id', 'city_id', 'time'));

            // Count time for every 1 minutes
            $time = strtotime(date('Y/m/d h:i:s', strtotime("+2 hours")));

            // Assign insert value
            foreach ($hotels_row as $hotel_count) {
                $query->values(array($hotel_count['id'], $hotel_count['city_id'], $time));
            }

            // Save process
            try {
                $result = $query->execute();
            } catch (Database_Exception $e) {
                echo $e->getMessage();
            }

            $data_set = array();
        }
    }

    // WEGO lowest rate csv
    public function action_lowest_rate() {
        //select update hotels
        $hotels = DB::select('hotel_id')
                ->from('api_wego_queues')
                ->order_by('api_wego_queues.id')
                // ->limit(10)
                ->execute()
                ->as_array();

        foreach ($hotels as $hotel) {
            //get campaigns_rooms
            $campaigns = DB::select(
                            array('hotels.id', 'hotel_id'), array('hotels.city_id', 'city_id'), array('campaigns.id', 'campaign_id'), array('hotels.currency_id', 'currency_id'), array('rooms.id', 'room_id'), array('items.price', 'price'), array('items.date', 'date')
                    )
                    ->from('campaigns')
                    ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                    ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
                    ->join('hotels')->on('hotels.id', '=', 'rooms.hotel_id')
                    ->join('items')->on('items.room_id', '=', 'rooms.id')
                    ->and_where_open()
                    ->where('hotels.id', '=', $hotel['hotel_id'])
                    ->where('items.date', '=', date('Y-m-d'))
                    ->where('items.price', '>', 0)
                    ->and_where_close()
                    ->group_by('campaigns.id')
                    ->execute()
                    ->as_array('campaign_id')
            ;
            //if campaigns not empty
            if (!empty($campaigns)) {
                foreach ($campaigns as $campaign) {
                    //insert to tabel api_wego
                    DB::insert('api_wego', array_keys($campaign)
                            )
                            ->values(array_values($campaign))
                            ->execute();
                }
            }
            //delete data from wego_queues
            DB::delete('api_wego_queues')
                    ->where('api_wego_queues.hotel_id', '=', $hotel['hotel_id'])
                    ->execute()
            ;
        }
    }

    public function action_notifreservation() {
        set_time_limit(0);

        $booking_exist = DB::select(
                        'bookings.*', 'booking_datas.*', 'api_reservation_queues.api', 'api_reservation_queues.request_type', 'api_reservation_queues.retries'
                )
                ->from('api_reservation_queues')
                ->join('bookings')->on('bookings.id', '=', 'api_reservation_queues.booking_id')
                ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                // ->where_open()
                //   ->where('bookings.siteminder_notifications', '=', 0)
                //   ->or_where('bookings.rategain_notifications', '=', 0)
                // ->where_close()
                // ->where('bookings.created', '<=', strtotime("now -10 minutes"))
                // Siteminder empty response bypass temporary fix
                // ->where('bookings.id', '>', '12680')
                ->execute()
                ->current();

        echo "<pre>";
        print_r($booking_exist);
        echo "</pre>";
        die();
        if ($booking_exist['request_type'] == 2 || $booking_exist['request_type'] == 1) {
            $set_limit_retry = 5;
            if ($booking_exist['retries'] <= $set_limit_retry) {
                // continue;
            } elseif ($booking_exist['retries'] > $set_limit_retry) {
                DB::delete('api_reservation_queues')
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();
            }
        }

        if (!empty($booking_exist['room_id'])) {
            $price = Model_Booking::calculate_prices($booking_exist['booking_id']);

            $booking_exist['data'] = unserialize($booking_exist['data']);
            $booking_exist['room_texts'] = unserialize($booking_exist['room_texts']);
            $booking_exist['room'] = unserialize($booking_exist['room']);
            $booking_exist['items'] = $price['rooms'];
            $booking_exist['hotel'] = unserialize($booking_exist['hotel']);

            // Guest Comment
            $booking_exist['note'] = 'Requested';
            $booking_exist['note'] .= $booking_exist['request_note'] ? ', guest comment:' . $booking_exist['request_note'] : NULL;
            $booking_exist['note'] .= $booking_exist['is_early_check_in_request'] ? ', Early Check In' : NULL;
            $booking_exist['note'] .= $booking_exist['is_high_floor_request'] ? ', High flor' : NULL;
            $booking_exist['note'] .= $booking_exist['is_large_bed_request'] ? ', Large Bed' : NULL;
            $booking_exist['note'] .= $booking_exist['is_twin_beds_request'] ? ', Twin Bed' : NULL;
            $booking_exist['note'] .= $booking_exist['is_airport_transfer_request'] ? ', Airport Transfer' : NULL;

            // Guid
            $s_message = strtoupper(md5(uniqid(rand(), true)));
            $booking_exist['guid_message'] = 'urn:uuid' . '-' .
                    substr($s_message, 0, 8) . '-' .
                    substr($s_message, 8, 4) . '-' .
                    substr($s_message, 12, 4) . '-' .
                    substr($s_message, 16, 4) . '-' .
                    substr($s_message, 20, 1);

            $s_token = strtoupper(md5(uniqid(rand(), true)));
            $booking_exist['guid_token'] = 'uuid' . '-' .
                    substr($s_token, 0, 8) . '-' .
                    substr($s_token, 8, 4) . '-' .
                    substr($s_token, 12, 4) . '-' .
                    substr($s_token, 16, 4) . '-' .
                    substr($s_token, 20, 12) . '-' .
                    substr($s_token, 31, 1);

            $campaign = (object) unserialize($booking_exist['campaign']);

            /*
             * Benefit name
             */

            $benefit = '';

            if ($campaign->discount_rate > 0) {
                $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night.', array(
                    ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                    ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                    ':discount_rate' => $campaign->discount_rate,
                ));
            } elseif ($campaign->discount_amount > 0) {
                $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night.', array(
                    ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                    ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                    ':currency_code' => 'USD',
                    ':discount_amount' => $campaign->discount_amount,
                ));
            } elseif ($campaign->number_of_free_nights) {
                $benefit = __('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights.', array(
                    ':minimum_number_of_nights' => $campaign->minimum_number_of_nights,
                    ':minimum_number_of_rooms' => $campaign->minimum_number_of_rooms,
                    ':number_of_free_nights' => $campaign->number_of_free_nights,
                ));
            } elseif (!$campaign->is_default) {
                $benefit = __('Default Promotions');
            }
            /// Additional Benefit promotion name
            else {
                $other_benefits = DB::select(
                                'campaigns_benefits.*', array('benefit_texts.name', 'name')
                        )
                        ->from('campaigns_benefits')
                        ->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
                        ->where('benefit_texts.language_id', '=', 1)
                        ->where('campaigns_benefits.campaign_id', '=', $campaign->id)
                        ->execute();

                if (count($other_benefits)) {
                    $room_campaigns = count($this->rooms) == count($room_names) ? __('All Rooms') : implode(', ', $room_names);

                    $benefit = $room_campaigns;
                    $benefit .= __(' get ');

                    foreach ($other_benefits as $count => $other_benefit) {
                        $benefit .= $other_benefit['name'];
                        if ($other_benefit['value']) {
                            $benefit .= ' for ' . $other_benefit['value'];
                        }

                        $benefit .= count($other_benefits) == ($count + 1) ? '.' : ', ';
                    }
                }
            }

            if ($campaign->type == 2) {
                $benefit .= ' ' . __('Guest must book :days_in_advance days in advance.', array(':days_in_advance' => $campaign->days_in_advance));
            } elseif ($campaign->type == 3) {
                $benefit .= ' ' . __('Guest must book within :within_days_of_arrival days of arrival.', array(':within_days_of_arrival' => $campaign->within_days_of_arrival));
            } elseif ($campaign->type == 4) {
                $benefit .= ' ' . __('Non refundable.');
            } elseif ($campaign->type == 5) {
                $benefit .= ' ' . __('Flash Deal.');
            }

            $booking_exist['benefit'] = $benefit;


//            echo "<pre>";
//            print_r($booking_exist);
//            echo "</pre>";
//            die('husen');
            // xml process
            if ($booking_exist['api'] == 1 AND $booking_exist['request_type'] == 1) {
                Controller_api_Rategainreservation::HotelRes_rategain($booking_exist);
                CustomLog::factory()->add(4, 'DEBUG', '1Controller_api_Rategainreservation::HotelRes_rategain($booking_exist);');
            } elseif ($booking_exist['api'] == 1 AND $booking_exist['request_type'] == 2) {
                Controller_api_Rategainreservation::HotelRes_rategain_cancel($booking_exist);
                CustomLog::factory()->add(4, 'DEBUG', '2Controller_api_Rategainreservation::HotelRes_rategain_cancel($booking_exist);');
            }

            if ($booking_exist['api'] == 2 AND $booking_exist['request_type'] == 1) {
                $xml = Controller_Siteminder_Reservation::HotelRes_siteminder($booking_exist);

                CustomLog::factory()->add(7, 'DEBUG', '3Controller_Siteminder_Reservation::HotelRes_siteminder($booking_exist);');
            } elseif ($booking_exist['api'] == 2 AND $booking_exist['request_type'] == 2) {
                Controller_Siteminder_Reservation::HotelRes_siteminder_cancel($booking_exist);
                CustomLog::factory()->add(7, 'DEBUG', '4Controller_Siteminder_Reservation::HotelRes_siteminder_cancel($booking_exist);');
            }
        }
    }

    public function action_checkItemFor6Mhonths() {
        CustomLog::factory()->add(1, 'DEBUG', 'Start : Populate Hotel notificationstock 6 months');
        try {

            set_time_limit(0);

            //prepared date 
            $now = date('Y-m-d');
            $plus6months = Date::formatted_time("$now + 6 months ", 'Y-m-d');
            $plus1week = Date::formatted_time("$plus6months + 1 weeks ", 'Y-m-d');


            //get all hotel active
            $hotels = ORM::factory('hotel')
                    ->where('hotels.active', '=', 1);

            // query for environment Testing
            if (Kohana::$environment != Kohana::PRODUCTION) {
                $hotels = $hotels
                        ->where('id', 'IN', array(49, 103))
                ;
            }

            $hotels = $hotels
                    ->find_all();

            //loop per hotel
            foreach ($hotels as $hotel) {
                //initial counter email
                $counter_hotel_email = 0;

                //get all room by hotel id
                $rooms = ORM::factory('room')
                        ->where('hotel_id', '=', $hotel->id)
                        ->find_all();

                $roomNoHaveAvailable = array();

                // //loop per room
                foreach ($rooms as $room) {

                    $stock_less = ORM::factory('item')
                            ->join('rooms')->on('items.room_id', '=', 'rooms.id')
                            ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
                            ->where('rooms.hotel_id', '=', $hotel->id)
                            ->where('room_texts.language_id', '=', 1)
                            ->where('rooms.id', '=', $room->id)
                            ->where('items.date', 'BETWEEN', array($now, $plus6months))
                            ->count_all();

                    //check items is available or not
                    if ($stock_less <= 0) {
                        array_push($roomNoHaveAvailable, $room->id);
                    }
                }

                if (!empty($roomNoHaveAvailable)) {
                    $counter_hotel_email++;
                    DB::insert('notificationstock')
                            ->columns(array('hotel_id', 'room_ids'))
                            ->values(array($hotel->id, serialize($roomNoHaveAvailable)))
                            ->execute();
                }
            }

            CustomLog::factory()->add(1, 'DEBUG', 'Finished populate Hotel notificationstock 6 months | '
                    . date('Y/m/d h:i:s'));
            /**
             * Hendle Error The function
             */
        } catch (Exception $e) {
            //add log
            CustomLog::factory()->add(1, 'DEBUG', 'Error : Populate Hotel notificationstock 6 months');
            //add log last tarce
            $error = $e->getTrace();
            CustomLog::factory()->add(1, 'DEBUG', print_r($error[0], true));
        }
    }

    public function action_sendEmailNotifcationitems() {
        try {
            $datas = array();

            $datas = DB::select('notificationstock.*')
                    ->from('notificationstock')
                    ->execute()
                    ->as_array();

            if (!empty($datas)) {
                CustomLog::factory()->add(1, 'DEBUG', 'INFO : Start send email notification stock');
                CustomLog::factory()->add(1, 'DEBUG', 'IP ACCCESS HTTPS: ' . $_SERVER['HTTP_X_FORWARDED_FOR']);
                CustomLog::factory()->add(1, 'DEBUG', 'IP ACCCESS HTTP: ' . $_SERVER['REMOTE_ADDR']);

                foreach ($datas as $dts => $data) {

                    try {

                        $room_names = array();
                        $data['room_ids'] = unserialize($data['room_ids']);

                        foreach ($data['room_ids'] as $rk => $room_id) {
                            $room_en = Model::factory('room')->get_by_id(1, $room_id);

                            $room_names[$rk]['name'] = $room_en->name;
                        }

                        $hotel_en = Model::factory('hotel')->get_hotel_by_id(1, $data['hotel_id']);

                        //send mail notification to hotel/extranet
                        $subject_email_notification = 'Hoterip Room Stock Notification in 6 Months';

                        $mail_body = Kostache::factory('email/notificationsix')
                                ->set('hotel_en', $hotel_en)
                                ->set('rooms', $room_names);

                        // Get admin emails (ALL ADMINs OF HOTEL: Admin and Extranet Role)
                        // $admin_emails = Model::factory('admin')->get_email($hotel->id);
                        // Get admin only emails
                        // $admin_emails = Model::factory('admin')->get_email_admin($hotel->id);
                        // Get extranet only emails
                        $extranet_emails = Model::factory('admin')->get_email_extranet($data['hotel_id']);
                        set_time_limit(360);

                        // Send email
                        Email::factory($subject_email_notification, $mail_body, 'text/plain')
                                ->from(Kohana::config('application')->get('email'), 'Hoterip')
                                ->to($extranet_emails)
                                ->cc(Kohana::config('application')->get('email'), 'Hoterip')
                                ->send();

                        DB::delete('notificationstock')
                                ->where('id', '=', $data['id'])
                                ->execute();

                        CustomLog::factory()->add(1, 'DEBUG', 'Success : Send Email Notification 6 Months to hotel ' . $hotel_en->name);
                    }
                    /**
                     * Hendle Error Send Per email
                     */ catch (Exception $e) {
                        //delete record notificationstock
                        DB::delete('notificationstock')
                                ->where('id', '=', $data['id'])
                                ->execute();

                        //add log
                        CustomLog::factory()->add(1, 'DEBUG', 'ERROR : Send Email Notification 6 Months to hotel ' . $data['hotel_id'] . ' Failed');

                        //add log last tarce
                        $error = $e->getTrace();
                        CustomLog::factory()->add(1, 'DEBUG', print_r($error[0], true));
                    }
                }

                CustomLog::factory()->add(1, 'DEBUG', 'INFO : Finished send email notification stock');
            } else {
                CustomLog::factory()->add(1, 'DEBUG', 'INFO : Finished send email notification stock');
                CustomLog::factory()->add(1, 'DEBUG', 'INFO : Table Notifstock is empty');
            }
        }
        /**
         * Hendle error the function
         */ catch (Exception $e) {
            //add log
            CustomLog::factory()->add(1, 'DEBUG', 'Error : Send Email Notification 6 Months');

            //delete all record
            DB::query(Database::DELETE, "TRUNCATE TABLE `notificationstock`")->execute();

            //add log last tarce
            $error = $e->getTrace();
            CustomLog::factory()->add(1, 'DEBUG', print_r($error[0], true));
        }
    }

    public function action_index() {
        echo 'index_action cron - ' . __class__;
    }

}
