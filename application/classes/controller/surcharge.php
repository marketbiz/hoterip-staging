<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Surcharge extends Controller_Layout_Admin {
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('surcharge', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		/**
		 * Get values from post
		 */
		$start_date = $this->request->post('start_date');
		$end_date = $this->request->post('end_date');
		
		/**
		 * Filter 
		 */
		
		if ( ! $start_date)
		{
			$start_date = date('Y-m-d');
		}
		
		if ( ! $end_date)
		{
			$end_date = date('Y-m-d', strtotime('+6 days'));
		}
		
		$start_date_timestamp = strtotime($start_date);
		$end_date_timestamp = strtotime($end_date);
		
		// If end date before start date
		if ($end_date_timestamp < $start_date_timestamp)
		{
			// Swap start date with end date
			$temp = $start_date;
			$start_date = $end_date;
			$end_date = $temp;
			
			$start_date_timestamp = strtotime($start_date);
			$end_date_timestamp = strtotime($end_date);
		}
		
		/**
		 * Form submitted
		 */
		if ($this->request->post('submit'))
		{
			// If posted hotel id different with current selected hotel
			// We are doing this check to prevent user change selected hotel in new window
			// and then submit this page
			if ($this->request->post('hotel_id') != $this->selected_hotel->id)
			{
				Notice::add(Notice::WARNING, Kohana::message('general', 'selected_hotel_changed'));
			}
			else
			{
				$dates = $this->request->post('dates');
			
				try
				{
					// Validation
					foreach ($dates as $date)
					{
						$validation = Validation::factory(array(
								'adult_price' => Arr::get($this->request->post('adult_prices'), $date),
								'child_price' => Arr::get($this->request->post('child_prices'), $date),
							))
							->rule('adult_price', 'digit', array(':value'))
							->rule('child_price', 'digit', array(':value'))
							->label('adult_price', 'Adult Price')
							->label('child_price', 'Child Price');

						// If validation failed
						if ( ! $validation->check())
						{
							throw new Validation_Exception($validation);
						}
					}
					
					// Get QA admins emails
					$qa_emails = ORM::factory('admin')
						->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
						->where('admins.role', '=', 'quality-assurance')
						->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
						->find_all()
						->as_array('email', 'name');

          // Start transaction
          Database::instance()->begin();
            
					try
					{
						foreach ($dates as $date)
						{
							$adult_prices = array_filter($this->request->post('adult_prices'));
							$child_prices = array_filter($this->request->post('child_prices'));
              $surcharge_description_ids = array_filter($this->request->post('surcharge_description_ids'));

							// Prepare the values
							$values = array(
								'hotel_id' => $this->selected_hotel->id,
								'date' => $date,
								'adult_price' => Arr::get($adult_prices, $date, NULL),
								'child_price' => Arr::get($child_prices, $date, NULL),
                'surcharge_description_id' => Arr::get($surcharge_description_ids, $date, NULL),
							);

							// Check if we should delete or save to table
							if ($values['adult_price'] == NULL AND $values['child_price'] == NULL)
							{
                // Find surcharge
								$surcharge = ORM::factory('surcharge')
                  ->select(array('surcharge_descriptions.description', 'description'))
                  ->join('surcharge_descriptions')->on('surcharge_descriptions.id', '=', 'surcharges.surcharge_description_id')
									->where('hotel_id', '=', $this->selected_hotel->id)
									->where('date', '=', $date)
									->find();
                
                if ($surcharge->loaded())
                {
                  // Delete surcharge
                  DB::delete('surcharges')
                    ->where('hotel_id', '=', $this->selected_hotel->id)
                    ->where('date', '=', $date)
                    ->execute();
                  
                  // Write log
                  ORM::factory('log')
                    ->values(array(
                      'admin_id' => A1::instance()->get_user()->id,
                      'hotel_id' => $this->selected_hotel->id,
                      'action' => 'Deleted Surcharge',
                      'room_texts' => NULL,
                      'text' => 'Date: :date. '
                        .'Adult Price: :adult_price. '
                        .'Child Price: :child_price. '
                        .'Description: :description.',
                      'data' => serialize(array(
                        ':date' => date('M j, Y', strtotime($surcharge->date)),
                        ':adult_price' => $surcharge->adult_price,
                        ':child_price' => $surcharge->child_price,
                        ':description' => $surcharge->description,
                      )),
                      'created' => time(),
                  ))
                  ->create();
									
									// If there is QA admins
									if (count($qa_emails) > 0)
									{
										// Send email
										Email::factory(strtr(':hotel_name - Deleted Surcharge', array(
												':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
											)), strtr(
											"Action: Deleted Surcharge.\n"
											."By: :admin_username (:admin_name).\n"
											."Date: :date.\n"
											."Adult Price: :adult_price.\n"
											."Child Price: :child_price.\n"
											."Description: :description.",
											array(
												':admin_username' => A1::instance()->get_user()->username,
												':admin_name' => A1::instance()->get_user()->name,
												':date' => date('M j, Y', strtotime($surcharge->date)),
												':adult_price' => $surcharge->adult_price,
                        ':child_price' => $surcharge->child_price,
                        ':description' => $surcharge->description,
											)))
											->from(Kohana::config('application.email'), Kohana::config('application.name'))
											->to($qa_emails)
											->send();
									}
                }
							}
							else
							{
                if ( ! ORM::factory('surcharge_description')
                  ->where('id', '=', Arr::get($values, 'surcharge_description_id'))
                  ->find()
                  ->loaded()
                  )
                {
                  throw new Kohana_Exception('Description is not valid');
                }
                
								// Find surcharge
								$surcharge = ORM::factory('surcharge')
                  ->select(array('surcharge_descriptions.description', 'description'))
                  ->join('surcharge_descriptions')->on('surcharge_descriptions.id', '=', 'surcharges.surcharge_description_id')
									->where('hotel_id', '=', $this->selected_hotel->id)
									->where('date', '=', $date)
									->find();
                
                // Copy surcharge to write log
                $prev_surcharge = clone $surcharge;

                // Save surcharge
                $surcharge
                  ->values($values)
                  ->save();

                // Reload surcharge because we need to select description
                $surcharge = ORM::factory('surcharge')
                  ->select(array('surcharge_descriptions.description', 'description'))
                  ->join('surcharge_descriptions')->on('surcharge_descriptions.id', '=', 'surcharges.surcharge_description_id')
                  ->where('hotel_id', '=', $this->selected_hotel->id)
                  ->where('date', '=', $date)
                  ->find();

                // Write log
                ORM::factory('log')
                  ->values(array(
                    'admin_id' => A1::instance()->get_user()->id,
                    'hotel_id' => $this->selected_hotel->id,
                    'action' => 'Changed Surcharge',
                    'room_texts' => NULL,
                    'text' => 'Date: :date. '
                      .'Adult Price: :prev_adult_price -> :adult_price. '
                      .'Child Price: :prev_child_price -> :child_price. '
                      .'Description: :prev_description -> :description.',
                    'data' => serialize(array(
                      ':prev_adult_price' => $prev_surcharge->adult_price ? $prev_surcharge->adult_price : 'None',
                      ':prev_child_price' => $prev_surcharge->child_price ? $prev_surcharge->child_price : 'None',
                      ':prev_description' => isset($prev_surcharge->description) ? $prev_surcharge->description : 'None',

                      ':date' => date('M j, Y', strtotime($surcharge->date)),
                      ':adult_price' => $surcharge->adult_price,
                      ':child_price' => $surcharge->child_price,
                      ':description' => $surcharge->description,
                    )),
                    'created' => time(),
                  ))
                  ->create();
								
								// If there is QA admins
								if (count($qa_emails) > 0)
								{
									// Send email
									Email::factory(strtr(':hotel_name - Changed Surcharge', array(
											':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
										)), strtr(
										"Action: Changed Surcharge.\n"
										."By: :admin_username (:admin_name).\n"
										."Date: :date.\n"
										."Adult Price: :prev_adult_price -> :adult_price.\n"
										."Child Price: :prev_child_price -> :child_price.\n"
										."Description: :prev_description -> :description.",
										array(
											':admin_username' => A1::instance()->get_user()->username,
											':admin_name' => A1::instance()->get_user()->name,
											':date' => date('M j, Y', strtotime($surcharge->date)),
											':prev_adult_price' => $prev_surcharge->adult_price ? $prev_surcharge->adult_price : 'None',
                      ':prev_child_price' => $prev_surcharge->child_price ? $prev_surcharge->child_price : 'None',
                      ':prev_description' => isset($prev_surcharge->description) ? $prev_surcharge->description : 'None',
											':adult_price' => $surcharge->adult_price,
											':child_price' => $surcharge->child_price,
											':description' => $surcharge->description,
										)))
										->from(Kohana::config('application.email'), Kohana::config('application.name'))
										->to($qa_emails)
										->send();
								}
							}
						}

						// Commit transaction
						Database::instance()->commit();
						// Add success notice
						Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
					}
					catch (ORM_Validation_Exception $e)
					{
						// Rollback transaction
						Database::instance()->rollback();
						// Add error notice
						Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('surcharge'));
					}
					catch (Exception $e)
					{
            // Rollback transaction
						Database::instance()->rollback();
						// Add error notice
						Notice::add(Notice::ERROR, $e->getMessage());
					}
				}
				catch (Validation_Exception $e)
				{
          // Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->array->errors('surcharge'));
				}
			}
		}
		
		/**
		 * Build variables for view
		 */
    
    $currency = ORM::factory('currency')
      ->where('id', '=', $this->selected_hotel->currency_id)
      ->find();
		
		// Create dates array
		$dates = array();
		for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY)
		{
			$dates[] = date('Y-m-d', $i);
		}
		
		$surcharges = ORM::factory('surcharge')
			->where('hotel_id', '=', $this->selected_hotel->id)
			->where('date', '>=', $start_date)
			->where('date', '<=', $end_date)
			->find_all()
			->as_array('date');
    
    $surcharge_descriptions = ORM::factory('surcharge_description')
      ->find_all();
    
		$this->template->main = Kostache::factory('surcharge/manage')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('selected_hotel', $this->selected_hotel)
      ->set('currency', $currency)
			->set('start_date', $start_date)
			->set('end_date', $end_date)
			->set('dates', $dates)
			->set('surcharges', $surcharges)
      ->set('surcharge_descriptions', $surcharge_descriptions);
	}
	
}