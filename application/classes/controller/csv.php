<?php defined('SYSPATH') or die('No direct script access.');


class Controller_CSV extends Controller_Layout_Admin {

  public function action_manage()
  {
    $datas = array();
    // Is Authorized ?
    if ( ! A2::instance()->allowed('csv', 'manage'))
    {
      // Add error notice
      Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
      // Redirect to home dashboard
      $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
    }

    switch ($this->request->post('generate-csv')) 
    {
      case 'Generate CSV':
          $this->_csv('csv');
        break;
      
      case 'Vacation Hotel List':
          $this->_csv('vacation');
        break;

      case 'Generate CSV Trivago':
          Kohana::$config->load('csv')->set('delimiter', ';');
          $this->_csv('trivago');
        break;
    }

    $directory = DOCROOT.'media/dropbox/csv/';

    if ($handle = opendir($directory)) {

      while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
          $datas[] = "$entry\n";
        }
      }

      closedir($handle);
    }

    $this->template->main = Kostache::factory('csv/manage')
      ->set('notice', Notice::render())
      ->set('datas', $datas);
  }

  public function get_hotels($type)
  {
    $hotels = ORM::factory('hotel')
      ->select(
        array('hotels.id', 'hotel_id'),
        array('hotel_texts.name', 'hotel_name'),
        array('hotels.star', 'hotel_star'),
        array('hotels.child_age_until', 'child_until'),
        array('hotels.child_age_from', 'child_from'),
        array('hotels.number_of_rooms', 'hotel_rooms'),
        array('hotels.address', 'hotel_address'),
        array('hotels.timezone', 'hotel_timezone'),
        array('hotels.active', 'active'),
        array('hotel_facilities.is_banquet_room_available', 'banquet_room'),
        array('hotel_facilities.is_bars_available', 'bars'),
        array('hotel_facilities.is_business_center_available', 'business_center'),
        array('hotel_facilities.is_car_parking_available', 'car_parking'),
        array('hotel_facilities.is_casino_available', 'casino'),
        array('hotel_facilities.is_clinic_available', 'clinic'),
        array('hotel_facilities.is_club_lounge_available', 'club_lounge'),
        array('hotel_facilities.is_coffee_shop_available', 'coffee_shop'),
        array('hotel_facilities.is_departure_lounge_available', 'departure_lounge'),
        array('hotel_facilities.is_disabled_facilities_available', 'disabled_facilities'),
        array('hotel_facilities.is_elevator_available', 'elevator'),
        array('hotel_facilities.is_garden_available', 'garden'),
        array('hotel_facilities.is_gym_available', 'gym'),
        array('hotel_facilities.is_gift_shop_available', 'gift_shop'),
        array('hotel_facilities.is_golf_course_available', 'golf_course'),
        array('hotel_facilities.is_hair_salon_available', 'hair_salon'),
        array('hotel_facilities.is_jacuzzi_available', 'jacuzzi'),
        array('hotel_facilities.is_karaoke_room_available', 'karaoke_room'),
        array('hotel_facilities.is_kids_club_available', 'kids_club'),
        array('hotel_facilities.is_kids_pool_available', 'kids_pool'),
        array('hotel_facilities.is_library_available', 'library'),
        array('hotel_facilities.is_luggage_room_available', 'luggage_room'),
        array('hotel_facilities.is_meeting_room_available', 'meeting_room'),
        array('hotel_facilities.is_night_club_available', 'night_club'),
        array('hotel_facilities.is_private_beach_available', 'private_beach'),
        array('hotel_facilities.is_poolside_bar_available', 'poolside_bar'),
        array('hotel_facilities.is_restaurant_available', 'restaurant'),
        array('hotel_facilities.is_safety_box_available', 'safety_box'),
        array('hotel_facilities.is_sauna_available', 'sauna'),
        array('hotel_facilities.is_spa_available', 'spa'),
        array('hotel_facilities.is_squash_court_available', 'squash_court'),
        array('hotel_facilities.is_steam_room_available', 'steam_room'),
        array('hotel_facilities.is_swimming_pool_available', 'swimming_pool'),
        array('hotel_facilities.is_tennis_court_available', 'tennis_court'),
        array('cities.url_segment', 'city_segment'),
        array('cities.id', 'city_id'),
        array('cities.country_id', 'country_id'),
        array('countries.url_segment', 'country_segment'), 
        array('city_texts.name', 'city_name'),
        array('district_texts.name', 'district_name'),
        array('country_texts.name', 'country_name'),
        array('hotels.coordinate', 'hotel_coordinate'),
        array('hotels.url_segment', 'hotel_url'),
        array('hotels.telephone', 'hotel_phone'),
        array('hotels.fax', 'hotel_fax'),
        array('hotels.check_in_time', 'hotel_check_in'),
        array('hotels.check_out_time', 'hotel_check_out'),
        array('hotels.opening_year', 'hotel_builded_year'),
        array('hotels.renovation_year', 'hotel_renovation_year')
      )
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('hotel_facilities')->on('hotel_facilities.hotel_id', '=', 'hotels.id')
      ->join('cities')->on('cities.id', '=', 'hotels.city_id')
      ->join('city_texts')->on('city_texts.city_id', '=', 'cities.id')
      ->join('districts')->on('districts.id', '=', 'hotels.district_id')
      ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
      ->join('countries')->on('countries.id', '=', 'cities.country_id')
      ->join('country_texts')->on('country_texts.country_id', '=', 'countries.id');

      if($type == 'vacation' || $type = 'trivago')
      {
        $hotels = $hotels
        ->where('hotels.active', '=', 1);
      }

      $hotels = $hotels
      ->where('hotel_texts.language_id', '=', $this->selected_language->id)
      ->where('city_texts.language_id', '=', $this->selected_language->id)
      ->where('district_texts.language_id', '=', $this->selected_language->id)
      ->where('country_texts.language_id', '=', $this->selected_language->id);

      if($type == 'vacation' || $type = 'trivago')
      {
        $hotels = $hotels
          ->order_by('hotels.id');
      }
      else
      {
        $hotels = $hotels
          ->order_by('hotel_texts.name');
      }

      $hotels = $hotels
      ->order_by('city_texts.name', 'ASC')
      ->find_all();

      return $hotels;
  }

  function _csv($type) 
  { 
    $directory = DOCROOT.'media/dropbox/csv/';
    
    $hotels = $this->get_hotels($type);

    if($type == 'csv' || $type == 'trivago')
    {
      // set csv header
      if($type == 'trivago')
      {
        //CSV HEADER
        $csv = CSV::factory($directory.'hotel_trivago.csv', array('delimiter' => ';'))
          ->titles(array(
          __('hotel_name'),
          __('address'),
          __('city'),
          __('country'),
          __('hotel_code'),
          __('address2'),
          __('country_id'),
          __('longitude'),
          __('latitude'),
          __('phone'), 
          __('fax'), 
          __('web'),  
          __('city_id'),
          __('currency')
        ));
      }
      elseif($type == 'csv')
      {
        $csv = CSV::factory( $directory.'hotels.csv')
        ->titles(array(
          __('propertyid'),
          __('propertyname'), 
          __('starratings'), 
          __('numberofrooms'), 
          __('address'), 
          __('city'), 
          __('state'), 
          __('country'), 
          __('latitude'),
          __('longitude'),
          __('amenities'),
          __('url'), 
          __('phone'), 
          __('fax'), 
          __('checkintime'), 
          __('checkouttime'), 
          __('builtyear'), 
          __('renovated')));
      }

      foreach ($hotels as $hotel)
      {
        //Latitude and longitude
        $location = explode(",", $hotel->hotel_coordinate);
        $latitude = reset($location);
        $longitude = end($location);

        //Amenties
        $facilities = array();
        if($hotel->banquet_room == 1){$facilities [] = 'Banquet Room';}
        if($hotel->banquet_room == 1){$facilities [] = 'Bars';}
        if($hotel->banquet_room == 1){$facilities [] = 'Business Center';}
        if($hotel->car_parking == 1){$facilities [] = 'Car Parking';}
        if($hotel->casino == 1){$facilities [] = 'Casino';}
        if($hotel->clinic == 1){$facilities [] = 'Clinic';}
        if($hotel->club_lounge == 1){$facilities [] = 'Club Lounge';}
        if($hotel->coffee_shop == 1){ $facilities [] = 'Coffee Shop';}
        if($hotel->departure_lounge == 1){$facilities [] = 'Departure Lounge';}
        if($hotel->disabled_facilities == 1){$facilities [] = 'Disabled Facilities';}
        if($hotel->elevator == 1){$facilities [] = 'Elevator';}
        if($hotel->banquet_room == 1){$facilities [] = 'Garden';}
        if($hotel->gym == 1){$facilities [] = 'Gym';}
        if($hotel->gift_shop == 1){$facilities [] = 'Gift Shop';}
        if($hotel->golf_course == 1){$facilities [] = 'Golf Course';}
        if($hotel->hair_salon == 1){$facilities [] = 'Hair Salon';}
        if($hotel->jacuzzi == 1){$facilities [] = 'Jacuzzi';}
        if($hotel->karaoke_room == 1){$facilities [] = 'Karaoke Room';}
        if($hotel->kids_club == 1){$facilities [] = 'Kids Club';}
        if($hotel->kids_pool == 1){$facilities [] = 'Kids Pool';}
        if($hotel->library == 1){$facilities [] = 'Library';}
        if($hotel->luggage_room == 1){$facilities [] = 'Luggage Room';}
        if($hotel->meeting_room == 1){$facilities [] = 'Meeting Room';}
        if($hotel->night_club == 1){$facilities [] = 'Night Club';}
        if($hotel->private_beach == 1){$facilities [] = 'Private Beach';}
        if($hotel->poolside_bar == 1){$facilities [] = 'Poolside Bar';}
        if($hotel->restaurant == 1){$facilities [] = 'Restaurant';}
        if($hotel->safety_box == 1){$facilities [] = 'Safety Box';}
        if($hotel->sauna == 1){$facilities [] = 'Sauna';}
        if($hotel->spa == 1){$facilities [] = 'Spa';}
        if($hotel->squash_court == 1){$facilities [] = 'Squash Court';}
        if($hotel->steam_room == 1){$facilities [] = 'Steam Room';}
        if($hotel->swimming_pool == 1){$facilities [] = 'Swimming Pool';}
        if($hotel->tennis_court == 1){$facilities [] = 'Tennis Court';}

        if(!empty($facilities)){
          $amenities = implode(";", $facilities);
        }

        //Hotel Url
        if (Kohana::$environment == Kohana::PRODUCTION)
        {
          $url_config = Kohana::config('application.hotel_url.live');
        } 
        else
        {
          $url_config = Kohana::config('application.hotel_url.staging');
        }
        
        $url =  $url_config.$hotel->country_segment
        .'/'.$hotel->city_segment.'/'.$hotel->hotel_url;

        //determine currency 
        if($hotel->currency_id == 1)
          $currency = 'USD';
        elseif($hotel->currency_id == 2)
          $currency = 'JPY';
        else
          $currency = 'IDR';


        //set value
        if($type == 'trivago')
        {
          $csv->values(array(
            $hotel->hotel_name,
            $hotel->hotel_address,
            $hotel->city_name,
            $hotel->country_name,
            $hotel->id,
            $hotel->district_name,
            $hotel->country_id,
            $longitude,
            $latitude,
            $hotel->hotel_phone,
            $hotel->hotel_fax,
            $url,
            $hotel->city_id,
            $currency
          ));
        }
        elseif($type == 'csv')
        {
          $csv->values(array(
            $hotel->id,
            $hotel->hotel_name,
            $hotel->hotel_star,
            $hotel->hotel_rooms,
            $hotel->hotel_address,
            $hotel->city_name,
            $hotel->district_name,
            $hotel->country_name,
            $latitude,
            $longitude,
            $amenities,
            $url,
            $hotel->hotel_phone,
            $hotel->hotel_fax,
            $hotel->hotel_check_in,
            $hotel->hotel_check_out,
            $hotel->hotel_builded_year,
            $hotel->hotel_renovation_year
          ));
        }
      }

      $csv->save();
      return TRUE;
    }
    else
    {
      $hotels = $this->get_hotels('vacation');

      //CSV HEADER
      $csv = CSV::factory( $directory.'hotels_vacation.csv')
        ->titles(array(
        __('hotel_code'),
        __('hotel_name'),
        __('city'),
        __('address'),
        __('phone'), 
        __('fax'), 
        __('room_type_name'),  
        __('room_type_code'),
        __('room_description'),
        __('bf/room only'),
        __('min_occupancy'),
        __('max_occupancy'),
        __('child category(x-xx_yo)'),
        __('plan_code'),
        __('plan_name'),
        __('plan_description'),
        __('clx_policy'),
        __('promotion_type'),
        __('currency_hotel')
      ));


      foreach ($hotels as $hotel)
      {

        $rooms = DB::select(
          'room_facilities.*',
          array('room_texts.name', 'room_name'),
          array('rooms.id', 'room_id'),
          array('rooms.is_breakfast_included', 'breakfast'),
          array('rooms.size', 'room_size')
          )
        ->from('rooms')
        ->join('room_texts')->on('rooms.id', '=', 'room_texts.room_id')
        ->join('room_facilities')->on('room_facilities.room_id', '=', 'rooms.id')
        ->where('rooms.hotel_id', '=', $hotel->id)
        ->group_by('rooms.id')
        ->as_object()
        ->execute()
        // ->current()
        ;

        foreach ($rooms as $room)
        {
          
          $child_cat = $hotel->child_from.'-'.$hotel->child_until.' Years Old';
          //get campaign
          $campaigns = DB::select(
            'campaigns.*'
            ) 
          ->from('campaigns') 
          ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
          ->join('rooms')->on('rooms.id','=','campaigns_rooms.room_id')
          ->where('campaigns_rooms.room_id','=',$room->room_id)
          ->where('campaigns.booking_end_date', '>=', date('Y-m-d'))
          ->group_by('campaigns.id')
          ->as_object()
          ->execute()
          ;

          foreach ($campaigns as $campaign)
          {

            //get cancellations campaign
            $cancellation = Model::factory('cancellation')->get_cancellation($campaign->id, date('Y-m-d'), date('Y-m-d'));
            if(empty($cancellation))
            {
              $cancellation_default = DB::select(
                array('rule_1.name', 'rule_1_name'),
                array('rule_2.name', 'rule_2_name'),
                array('rule_3.name', 'rule_3_name'),
                array('start_date', 'start_date'),
                array('end_date', 'end_date')
                )
              ->from('cancellations')
              ->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'cancellations.level_1_cancellation_rule_id')
              ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'cancellations.level_2_cancellation_rule_id')
              ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'cancellations.level_3_cancellation_rule_id')
              ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
              ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
              ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
              ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
              ->where('rooms.hotel_id', '=', $hotel->id)
              ->where('start_date', '<=', date('Y-m-d'))
              ->where('end_date', '>=', date('Y-m-d'))
              ->where('campaigns.is_default', '=', 1)
              ->order_by('cancellations.start_date', 'ASC')
              ->group_by('cancellations.id')
              ->execute()
              ->current();

              $cancellation = $cancellation_default;

            }
            $cancellation['all'];
            if(!empty($cancellation['rule_1_name']))
              $cancellation['all'] = $cancellation['all'].$cancellation['rule_1_name'];
            if(!empty($cancellation['rule_2_name']))
              $cancellation['all'] = $cancellation['all'].'|'.$cancellation['rule_2_name'];
            if(!empty($cancellation['rule_3_name']))
              $cancellation['all'] = $cancellation['all'].'|'.$cancellation['rule_3_name'];

            if(empty($cancellation)){
              $cancellation['all'] = 'Non Refundable';
            }

            $items = DB::select(
              array('COUNT("id")', 'items')
              )
            ->from('items')
            ->where('room_id', '=', $room->room_id)
            ->where('date', '>=', date('Y-m-d'))
            ->limit(1)
            ->execute()
            ->as_array('items');

            if(count($items[0]['items']))
            {
              continue;
            }

            $promotion_data = array(
              'campaign_default' => $campaign->is_default,
              'last_days' => $campaign->within_days_of_arrival,
              'campaign_default' => $campaign->is_default,
              'hotel_timezone' => $hotel->hotel_timezone,
              'early_days' => $campaign->days_in_advance,
              'rate' => $campaign->discount_rate,
              'amount' => $campaign->discount_amount,
              'min_nights' => $campaign->minimum_number_of_nights,
              'min_rooms' => $campaign->minimum_number_of_rooms,
              'id' => $campaign->id,
              );

            $hotel_query = array(
              'check_in' => date('Y-m-d'),
              'check_out' => date('Y-m-d', strtotime(' +1 day')),
              'currency_code' => '3',
              'hotel_currency' => $hotel->currency_id,
              );

            $plan_description = Controller_Vacation_Static::promotion_descriptions($hotel_query, $promotion_data, $promotion_exis = TRUE);
            
            if($campaign->discount_rate != 0)
            {
              $promotion_name = $campaign->discount_rate.'% Discount';
            }
            elseif($campaign->discount_amount != 0)
            {
              // $promotion_name = Controller_Vacation_Static::selected_currency($hotel->currency_id)->symbol
              // .$campaign->discount_amount.' Discount';
            }
            elseif($campaign->within_days_of_arrival != 0)
            {
              $promotion_name = 'LAST MINUTE';
            }
            elseif ($campaign->days_in_advance != 0)
            {
              $promotion_name = 'EARLY BIRDS';
            }
            else
            {
             $promotion_name = 'Best Available Rate';
            }

            switch ($campaign->type)
            {
              case 2:
                $promotion_type = 'Early Bird';
                break;
              case 3:
                $promotion_type = 'Last Minute';
                break;
              case 4:
                $promotion_type = 'Non Refundable';
                break;
              case 5:
                $promotion_type = 'Flash Deal 50% Discount';
                break;

              default:
                $promotion_type = 'Standard Promotion';
                break;
            }

            $min_occupancy = 1;
            $occupancies = Model::factory('room_capacity')->get_room_capacities($room->room_id);

            //select max room capasities
            foreach ($occupancies as $key => $value)
            { 
              if($key==0)
              {
                $tmp = $value['number_of_children']+$value['number_of_adults'];
              }
              else
              {
                if(($value['number_of_adults']+$value['number_of_children'])
                >($tmp['number_of_children']+$tmp['number_of_adults']))
                {
                    $max_occupancy = $value['number_of_children']+$value['number_of_adults'];
                }
              }
            }

            $description_room = '';
            if($room->room_size > 0)
            {
              // Entity code of meter square
              // $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['description_size'] = $hotel->room_size.' &#13217;, ';
              // Special character of meter square
              // $hotel_data[$hotel->hotel_id]['room_type'][$hotel->room_id]['description_size'] = $hotel->room_size.' ㎡, ';
              // Square Meters as text..
              $description_room = $room->room_size.' square meters, ';
            }

            $hotel_data_room_description = array();

            // Room services for Descriptions
            if($room->is_air_conditioner_available)
            {
              array_push($hotel_data_room_description,'Air Conditioner');
            }
            if($room->is_alarm_clock_available)
            {
              array_push($hotel_data_room_description,'Alarm Clock');
            }
            if($room->is_balcony_available)
            {
              array_push($hotel_data_room_description,'Balcony');
            }
            if($room->is_bathrobe_available)
            {
              array_push($hotel_data_room_description,'Bath Robe');
            }
            if($room->is_bathtub_available)
            {
              array_push($hotel_data_room_description,'Bathtub');
            }
            if($room->is_body_lotion_available)
            {
              array_push($hotel_data_room_description,'Body Lotion');
            }
            if($room->is_cable_tv_available)
            {
              array_push($hotel_data_room_description,'Cable TV');
            }
            if($room->is_coffee_maker_available)
            {
              array_push($hotel_data_room_description,'Coffe Maker');
            }
            if($room->is_cotton_bud_available)
            {
              array_push($hotel_data_room_description,'Cotton Bud');
            }
            if($room->is_dvd_player_available)
            {
              array_push($hotel_data_room_description,'DVD player');
            }
            if($room->is_hair_dryer_available)
            {
              array_push($hotel_data_room_description,'Hair Dryer');
            }
            if($room->is_idd_telephone_available)
            {
              array_push($hotel_data_room_description,'IDD Telephone');
            }
            if($room->is_independent_shower_room_available)
            {
              array_push($hotel_data_room_description,'Independent Shower Room');
            }
            if($room->is_safety_box_available)
            {
              array_push($hotel_data_room_description,'Safety Box');
            }
            if($room->is_internet_jack_available)
            {
              array_push($hotel_data_room_description,'Internet Jack');
            }
            if($room->is_iron_available)
            {
              array_push($hotel_data_room_description,'Iron');
            }
            if($room->is_mini_bar_available)
            {
              array_push($hotel_data_room_description,'Mini Bar');
            }
            if($room->is_mosquito_equipment_available)
            {
              array_push($hotel_data_room_description,'Mosquito Equipment');
            }
            if($room->is_moveable_shower_head_available)
            {
              array_push($hotel_data_room_description,'Movable Shower Head');
            }
            if($room->is_radio_available)
            {
              array_push($hotel_data_room_description,'Radio');
            }
            if($room->is_room_wear_available)
            {
              array_push($hotel_data_room_description,'Room Wear');
            }
            if($room->is_shampoo_available)
            {
              array_push($hotel_data_room_description,'Shampoo');
            }
            if($room->is_shaver_available)
            {
              array_push($hotel_data_room_description,'Shaver');
            }
            if($room->is_soap_available)
            {
              array_push($hotel_data_room_description,'Soap');
            }
            if($room->is_toothbrush_available)
            {
              array_push($hotel_data_room_description,'Toothbrush');
            }
            if($room->is_towel_available)
            {
              array_push($hotel_data_room_description,'Towel');
            }
            if($room->is_tv_available)
            {
              array_push($hotel_data_room_description,'TV');
            }

            if($hotel->currency_id==1)
              $currency = 'USD';
            elseif($hotel->currency_id==1)
              $currency = 'JPY';
            else
              $currency = 'IDR';

            // implode facilities to text
            $hotel_data_room_facilities = implode(', ', $hotel_data_room_description);
            $description_room = $description_room.$hotel_data_room_facilities;

              $breakfast = $room->breakfast == TRUE ? 'Breakfast' : 'Room Only';

              $csv->values(array(
                $hotel->id,
                $hotel->hotel_name,
                $hotel->city_name,
                $hotel->hotel_address,
                $hotel->hotel_phone,
                $hotel->hotel_fax,
                $room->room_name,
                $room->room_id,
                $description_room,
                $breakfast,
                $min_occupancy,
                $max_occupancy,
                $child_cat,
                $campaign->id,
                $promotion_name,
                $plan_description,
                $cancellation['all'],
                $promotion_type,
                $currency
              ));
          }
        }
      }
      $csv->save();
      return TRUE;
    }
  }

  public static function csvReport($headcsv, $bookings, $csv_name)
  {
    //initial csv
    $csv = CSV::factory($csv_name.'.csv');

    //generate head
    $titles = array();
    foreach ($headcsv as $head) 
    {
      $title = __($head);
      array_push($titles, $title);
    }
    $csv->titles($titles);

    $value_row = array();
    foreach ($bookings as $bookingk => $booking) 
    {
      foreach ($booking as $valuek => $value) 
      {
        array_push($value_row, $value);
      }
      $csv->values($value_row);
      $value_row = array();
    }

    $csv->send_file();
  }
  public function action_delete()
  {
    if ($_GET['name'])
    {
      $directory = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/dropbox/csv/';

      unlink($directory .''. $_GET['name']);

      $this->request->redirect(Route::get('default')->uri(array('controller' => 'csv', 'action' => 'manage')));
    }
  }
}