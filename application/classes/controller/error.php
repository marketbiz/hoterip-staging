<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Error extends Controller_Layout_Admin {
  
  protected $_message;

  public function before()
  {
    parent::before();
    
    // If sub request
    if ( ! $this->request->is_initial())
    {
      if ($message = rawurldecode($this->request->param('message')))
      {
        $this->_message = $message;
      }
    }
    else
    {
      // This one was directly requested, don't allow
      $this->request->action(404);
    }
    
    $this->response->status((int) $this->request->action());
  }
  
  public function action_404()
  {
    $this->template->main = Kostache::factory('error/404')
      ->set('message', $this->_message);
  }
  
  public function action_500()
  {
    $this->template->main = Kostache::factory('error/500')
      ->set('message', $this->_message);
  }
  
}