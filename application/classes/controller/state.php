<?php defined('SYSPATH') or die('No direct script access.');

class Controller_State extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('state', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			if (A2::instance()->allowed('state', 'delete'))
			{
				try
				{
					// Get states id
					$state_ids = $this->request->post('ids');
			
					// Delete states
					DB::delete('states')
						->where('id', 'IN', $state_ids)
						->execute();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			}
		}
		
		$states = ORM::factory('state')
			->select(
				array('state_texts.name', 'name'),
				array('country_texts.name', 'country_name'),
				array(DB::expr('COUNT(hotels.id)'), 'total_hotels')
			)
			->join('state_texts')->on('state_texts.state_id', '=', 'states.id')
			->join('countries')->on('countries.id', '=', 'states.country_id')
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
			->join('cities', 'left')->on('cities.state_id', '=', 'states.id')
			->join('hotels', 'left')->on('hotels.city_id', '=', 'cities.id')
			->where('state_texts.language_id', '=', $this->selected_language->id)
			->where('country_texts.language_id', '=', $this->selected_language->id)
			->group_by('states.id')
			->order_by('state_texts.name', 'ASC')
			->find_all();
		
		$this->template->main = Kostache::factory('state/manage')
			->set('notice', Notice::render())
			->set('states', $states);
	}
	
	public function action_add()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('state', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage states
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'state', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Create state
				$state = ORM::factory('state')
					->create_state($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'state', 'action' => 'edit', 'id' => $state->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('state'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$languages = ORM::factory('language')
			->find_all();
		
		$country_options = ORM::factory('country')
			->select(
				array('country_texts.name', 'name')
			)
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
			->where('country_texts.language_id', '=', $this->selected_language->id)
			->order_by('country_texts.name')
			->find_all()
			->as_array('id', 'name');
		
		$this->template->main = Kostache::factory('state/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('languages', $languages)
			->set('country_options', $country_options);
	}
	
	public function action_edit()
	{
		// Get state id
		$state_id = (int) $this->request->param('id');
		
		// Get state
		$state = ORM::factory('state')
			->where('id', '=', $state_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed('state', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage states
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'state', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Update state
				ORM::factory('state')
					->where('id', '=', $state_id)
					->find()
					->update_state($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('state'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$state = ORM::factory('state')
			->where('id', '=', $state_id)
			->find();
		
		// Get state texts
		$state_texts = ORM::factory('state_text')
			->where('state_id', '=', $state->id)
			->find_all()
			->as_array('language_id');
		
		$languages = ORM::factory('language')
			->find_all();
		
		$country_options = ORM::factory('country')
			->select(
				array('country_texts.name', 'name')
			)
			->join('country_texts')->on('country_texts.country_id', '=', 'countries.id')
			->where('country_texts.language_id', '=', $this->selected_language->id)
			->order_by('country_texts.name')
			->find_all()
			->as_array('id', 'name');
		
		$this->template->main = Kostache::factory('state/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('state', $state)
			->set('state_texts', $state_texts)
			->set('languages', $languages)
			->set('country_options', $country_options);
	}


	public function action_delete()
	{
		// Get state id
		$state_id = $this->request->param('id');
		
		if (A2::instance()->allowed('state', 'delete'))
		{
			try
			{
				// Delete state
				DB::delete('states')
					->where('id', '=', $state_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}