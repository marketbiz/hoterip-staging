<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Benefit extends Controller_Layout_Admin {

	public function before()
	{	
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('benefit', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			if (A2::instance()->allowed('benefit', 'delete'))
			{
				try
				{
					// Get benefit_id id
					$benefit_ids = $this->request->post('ids');
			
					// Delete benefit_id
					DB::delete('benefit_texts')
						->where('benefit_id', 'IN', $benefit_ids)
						->execute();

					DB::delete('benefits')
						->where('id', 'IN', $benefit_ids)
						->execute();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			}
		}
		
		$benefits = ORM::factory('benefit')
			->select(
				array('benefit_texts.name', 'name'),
				array('campaigns_benefits.id', 'campaign_benefit'),
				array('hotels.id', 'hotel_id'),
				array('hotel_texts.name', 'hotel_name')
			)
			->join('benefit_texts')->on('benefit_texts.benefit_id', '=', 'benefits.id')
			->join('campaigns_benefits', 'left')->on('campaigns_benefits.benefit_id', '=', 'benefits.id')
			->join('campaigns_rooms', 'left')->on('campaigns_rooms.campaign_id', '=', 'campaigns_benefits.campaign_id')
			->join('rooms', 'left')->on('rooms.id', '=', 'campaigns_rooms.room_id')
			->join('hotels', 'left')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('hotel_texts', 'left')->on('hotel_texts.hotel_id', '=', 'hotels.id')
			->where('benefit_texts.language_id', '=', $this->selected_language->id)
			->group_by('benefits.id')
			->order_by('benefit_texts.name', 'ASC')
			->find_all();

		$this->template->main = Kostache::factory('benefit/manage')
			->set('notice', Notice::render())
			->set('benefits', $benefits);
	}
	
	public function action_add()
	{
		// Is Authorized ?
		if ( ! A2::instance()->allowed('benefit', 'add'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage benefit
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'benefit', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Create benefit
				$benefit = ORM::factory('benefit')
					->create_benefit($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'benefit', 'action' => 'add')), __('Add another benefit'))));
				// Redirect to edit
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'benefit', 'action' => 'edit', 'id' => $benefit->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('benefit'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		    // Get selected hotel currency
    $currencies = ORM::factory('currency')
      ->find_all();

		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('benefit/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			/// Hoterip campaign
      ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
			->set('currencies', $currencies)
			->set('languages', $languages);
	}
	
	public function action_edit()
	{
		// Get benefit id
		$benefit_id = (int) $this->request->param('id');
		
		// Get benefit
		$benefit = ORM::factory('benefit')
			->where('id', '=', $benefit_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed('benefit', 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage benefits
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'benefit', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();

				// Update benefit
				ORM::factory('benefit')
					->where('id', '=', $benefit_id)
					->find()
					->update_benefit($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('benefit'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$benefit = ORM::factory('benefit')
			->where('id', '=', $benefit_id)
			->find();
		
		// Get benefit texts
		$benefit_texts = ORM::factory('benefit_text')
			->where('benefit_id', '=', $benefit->id)
			->find_all()
			->as_array('language_id');
		
		$languages = ORM::factory('language')
			->find_all();
		
		 // Get selected hotel currency
    $currencies = ORM::factory('currency')
      ->find_all();

		$this->template->main = Kostache::factory('benefit/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('benefit', $benefit)
			->set('benefit_texts', $benefit_texts)
      /// Hoterip campaign
      ->set('hoterip', A2::instance()->allowed('all', 'hoterip'))
			->set('currencies', $currencies)
			->set('languages', $languages);
	}

	public function action_delete()
	{
		// Get benefit id
		$benefit_id = $this->request->param('id');
		
		if (A2::instance()->allowed('benefit', 'delete'))
		{
			try
			{
				// Delete benefit_id
				DB::delete('benefit_texts')
					->where('benefit_id', '=', $benefit_id)
					->execute();

				// Delete benefit
				DB::delete('benefits')
					->where('id', '=', $benefit_id)
					->execute();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	public function action_ajax()
	{
		$benefits = ORM::factory('benefit')
			->select(
				array('campaigns_benefits.id', 'campaign_benefit'),
				array('hotels.id', 'hotel_id'),
				array('hotel_texts.name', 'hotel_name')
			)
			->join('campaigns_benefits', 'left')->on('campaigns_benefits.benefit_id', '=', 'benefits.id')
			->join('campaigns_rooms', 'left')->on('campaigns_rooms.campaign_id', '=', 'campaigns_benefits.campaign_id')
			->join('rooms', 'left')->on('rooms.id', '=', 'campaigns_rooms.room_id')
			->join('hotels', 'left')->on('hotels.id', '=', 'rooms.hotel_id')
			->join('hotel_texts', 'left')->on('hotel_texts.hotel_id', '=', 'hotels.id')
			->where('hotel_texts.language_id', '=', $this->selected_language->id)
			->where('benefits.id', '=', $this->request->param('id'))
			->group_by('benefits.id')
			->order_by('hotel_texts.name', 'ASC')
			->find_all();

		echo '<table class="list-table">';
		echo '<thead>
					<tr>
						<th>Hotels Using This Benefit</th>
					</tr>
				</thead>
				<tbody>';
		foreach ($benefits as $key => $benefit) {
			echo '<tr>';
			echo '<td>';
			echo $benefit->hotel_name;
			echo '</td>';
			echo '</tr>';
		}
		echo '</tbody></table>';

		exit();
	}
}