<?php defined('SYSPATH') or die('No direct script access.');

class Controller_PR extends Controller {
  
  public function create_error($message)
  {
    $doc = new DOMDocument('1.0', 'UTF-8');
    $doc->formatOutput = TRUE;
    
    $response_node = $doc->createElement('Response');
    $response_node = $doc->appendChild($response_node);
    
    $error_node = $doc->createElement('error');
    $error_node = $response_node->appendChild($error_node);
    
    $error_node->setAttribute('message', $message);
    
    return $doc->saveXML();
  }
  
  public function get_hotels($admin, $node)
  {
    // Get admin hotels
    $hotels = $admin->hotels
      ->select(array('hotel_texts.name', 'name'))
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->where('hotel_texts.language_id', '=', 1)
      ->find_all();
    
    // Create xml document
    $doc = new DOMDocument('1.0', 'UTF-8');
    $doc->formatOutput = TRUE;
    
    $response_node = $doc->createElement('Response');
    $response_node = $doc->appendChild($response_node);
    
    foreach ($hotels as $hotel)
    {
      $hotel_node = $doc->createElement('hotel');
      $hotel_node->setAttribute('id', $hotel->id);
      $hotel_node->setAttribute('description', $hotel->name);
      
      $response_node->appendChild($hotel_node);
    }
    
    return $doc->saveXML();
  }
  
  public function get_rates($admin, $node)
  {
    $hotel_id = $node->getAttribute('hotelId');
    
    $hotel = ORM::factory('hotel')
      ->where('id', '=', $hotel_id)
      ->find();
    
    if ($admin->has('hotels', $hotel))
    {
      $campaigns = ORM::factory('campaign')
        ->select('rooms.id', 'room_id')
        ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
        ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
        ->where('rooms.hotel_id', '=', $hotel_id)
        ->group_by('campaigns.id')
        ->find_all();

        // Create xml document
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $response_node = $doc->createElement('Response');
        $response_node = $doc->appendChild($response_node);

        foreach ($campaigns as $key => $campaign) 
        {
          // Description
          $promotion = '';
          $stay = '';

          if($campaign->is_default)
          {
            $promotion = 'Default Promotion';
          }
          elseif ($campaign->type == 1) 
          {
            if($campaign->discount_rate)
            {
              $promotion = 'Discount ';
              $promotion .= $campaign->discount_rate.'%';
            }
            elseif ($campaign->discount_amount) 
            {
              $currency = ORM::factory('currency')
                ->where('id', '=', $hotel->currency_id)
                ->find();

              $promotion = 'Discount ';
              $promotion .= $currency->code.' '.$campaign->discount_amount;
            }
            elseif ($campaign->number_of_free_nights) 
            {
              $promotion = 'Free '.$campaign->number_of_free_nights.'nights';
            }
          }
          elseif ($campaign->type == 2) 
          {
            $promotion = 'Book After '.$campaign->days_in_advance.' days';
          }
          elseif ($campaign->type == 3) 
          {
            $promotion = 'Book Before '.$campaign->within_days_of_arrival.' days';
          }

          if(!$campaign->is_default AND $campaign->minimum_number_of_nights)
          {
            $stay = 'Stay for '.$campaign->minimum_number_of_nights.' nights';
          }
          elseif (!$campaign->is_default AND  $campaign->minimum_number_of_rooms) 
          {
            $stay = 'Book '.$campaign->minimum_number_of_rooms.' rooms';
          }
          // room name
          $room = DB::select('room_texts.name')
            ->from('room_texts')
            ->where('room_texts.room_id', '=', $campaign->room_id)
            ->where('room_texts.language_id', '=', 1)
            ->as_object()
            ->execute()
            ->current();

          $description = $room->name.', '.$promotion.' '.$stay;

          $room_node = $doc->createElement('rate');
          $room_node->setAttribute('id', $campaign->id);
          $room_node->setAttribute('description', $description);
          $room_node = $response_node->appendChild($room_node);
        }

        return $doc->saveXML();
    }
    else
    {
      throw new Kohana_Exception('You are not authorized');
    }
  }
  
  public function get_rooms($admin, $node)
  {
    $hotel_id = $node->getAttribute('hotelId');
    
    $hotel = ORM::factory('hotel')
      ->where('id', '=', $hotel_id)
      ->find();
    
    if ($admin->has('hotels', $hotel))
    {
      // get hotel ids
      $hotel_ids[0] = $hotel_id;

      // Room properties
      $room = array(
        'language_id' => 1,
        'InvTypeCode' => '*',
        'RatePlanCode' => '*',
        'RatePlanCandidateRPH' => 1,
        'Quantity' => 1,
        'GuestCounts' => array
        (
          0 => array(
            'AgeQualifyingCode' => 10,
            'Count' => 1
            )
        ),
        'hotel_id' => $hotel_id,
        'check_in' => Date::formatted_time('now', 'Y-m-d', $hotel->timezone),
        'check_out' => Date::formatted_time('now + 1 days', 'Y-m-d', $hotel->timezone),
        'number_of_rooms' => 2,
        'capacities' => array
        (
          0 => array(
            'child' => 0,
            'adult' => 1
            ),
        ),
        'now' => Date::formatted_time('now', 'Y-m-d', $hotel->timezone)
        );
      
      $campaigns = ORM::factory('hotel')->get_campaigns($hotel_ids, $room);

      $campaigns_datas = array();

      foreach ($campaigns as $campaign_key => $campaign) 
      {
        $campaigns_datas[$campaign['room_id']][] = $campaign;      }
      
      // Create xml document
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $response_node = $doc->createElement('Response');
      $response_node = $doc->appendChild($response_node);
      
      foreach ($campaigns_datas as $room_id => $campaigns_data)
      {
        $room_node = $doc->createElement('room');
        $room_node->setAttribute('roomId', $room_id);
        
        foreach ($campaigns_data as $campaigns_data_key => $room) 
        {
          // Description
          $promotion = '';
          $stay = '';

          if($room['campaign_default'])
          {
            $promotion = 'Default Promotion';
          }
          elseif ($room['type'] == 1) 
          {
            if($room['rate'])
            {
              $promotion = 'Discount ';
              $promotion .= $room['rate'].'%';
            }
            elseif ($room['amount']) 
            {
              $currency = ORM::factory('currency')
                ->where('id', '=', $hotel->currency_id)
                ->find();

              $promotion = 'Discount ';
              $promotion .= $currency->code.' '.$room['amount'];
            }
            elseif ($room['free_night']) 
            {
              $promotion = 'Free '.$room['free_night'].'nights';
            }
          }
          elseif ($room['type'] == 2) 
          {
            $promotion = 'Book After '.$room['early_days'].' days';
          }
          elseif ($room['type'] == 3) 
          {
            $promotion = 'Book Before '.$room['last_days'].' days';
          }

          if(!$room['campaign_default'] AND $room['min_nights'])
          {
            $stay = 'Stay for '.$room['min_nights'].' nights';
          }
          elseif (!$room['campaign_default'] AND  $room['min_rooms']) 
          {
            $stay = 'Book '.$room['min_rooms'].' rooms';
          }

          $room_node->setAttribute('description', $room['room_name']);
        
          $roomRate_node = $doc->createElement('roomRate');
          $roomRate_node->setAttribute('rateId', $room['campaign_id']);
          $roomRate_node->setAttribute('roomRateId', $room['campaign_id']);
          $roomRate_node->setAttribute('roomRateDescription', $room['room_name'].' - '.$promotion.' '.$stay);
          $roomRate_node = $room_node->appendChild($roomRate_node);
        }
        $room_node = $response_node->appendChild($room_node);
      }
      
      return $doc->saveXML();
    }
    else
    {
      throw new Kohana_Exception('You are not authorized');
    }
  }
  
  public function view_request($admin, $node)
  {
    $hotel_id = $node->getAttribute('hotelId');
    
    $hotel = ORM::factory('hotel')
      ->where('id', '=', $hotel_id)
      ->find();
    
    if ($admin->has('hotels', $hotel))
    {
      /// Campaign prices check ID promotion
      $rate_id = $node->getAttribute('rateId');

      $campaigns = ORM::factory('campaign')
        ->select('rooms.id', 'room_id')
        ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
        ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
        ;
        if ($rate_id) 
        {
          $campaigns = $campaigns->where('campaigns.id', '=', $rate_id);
        }
        
        $campaigns = $campaigns
        ->where('rooms.hotel_id', '=', $hotel_id)
        ->find_all();
      
      $start_date = $node->getAttribute('startDate');
      $end_date = $node->getAttribute('endDate');

      if ($campaigns) 
      {
        // Create xml document
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->formatOutput = TRUE;

        $response_node = $doc->createElement('Response');
        $response_node = $doc->appendChild($response_node);
        

        foreach ($campaigns as $key => $campaign) 
        {
          $items = DB::select()
          ->from('items')

          ->where('campaign_id', '=', 0)

          ->where('room_id', '=', $campaign->room_id)
          ->where('items.date', '>=', $start_date)
          ->where('items.date', '<=', $end_date)
          
          ->order_by('campaign_id', 'ASC')

          ->execute()
          ->as_array('date')
          ;
          
          // Get hotel currency
          $currency = ORM::factory('currency')
            ->where('id', '=', $hotel->currency_id)
            ->find();

          foreach ($items as $item)
          {
            $availability_node = $doc->createElement('availability');
            $availability_node->setAttribute('day', $item['date']);
            $availability_node->setAttribute('rateId', $campaign->id);
            $availability_node->setAttribute('roomId', $item['room_id']);
            $availability_node->setAttribute('roomRateId', $campaign->id);
            $availability_node->setAttribute('quantity', $item['stock']);
            $availability_node->setAttribute('price', $item['price']);
            $availability_node->setAttribute('currency', $currency->code);
            $availability_node->setAttribute('minimumStay', $item['minimum_night']);
            
            $response_node->appendChild($availability_node);
          }
        }
        return $doc->saveXML();
      }
      else
      {
        throw new Kohana_Exception('You are not authorized');
      }
    }
    else
    {
      throw new Kohana_Exception('You are not authorized');
    }
  }
  
  public function modify($admin, $node)
  {
    $hotel_id = $node->getAttribute('hotelId');
    
    $hotel = ORM::factory('hotel')
      ->where('id', '=', $hotel_id)
      ->find();
    
    if ($admin->has('hotels', $hotel))
    {
      $start_date = $node->getAttribute('startDate');
      $end_date = $node->getAttribute('endDate');
      
      // Get hotel currency
      $currency = ORM::factory('currency')
        ->where('id', '=', $hotel->currency_id)
        ->find();
      
      // Check the node children
      if ($node->hasChildNodes() AND $node->childNodes->length > 0)
      {
        // Start transaction
        Database::instance()->begin();
        
        try
        {
          foreach ($node->childNodes as $availability_node)
          {
            // Make sure it's availability node
            if ($availability_node->nodeName == 'availability')
            {
              $date = $availability_node->getAttribute('day');
              $room_id = $availability_node->getAttribute('roomId');
              $stock = $availability_node->getAttribute('quantity');
              $price = $availability_node->getAttribute('price');
              $currency_code = $availability_node->getAttribute('currency');
              $minimum_night = $availability_node->getAttribute('minimumStay');
              /// Campaign prices 
              $rate_id = $availability_node->getAttribute('rateId');

              // Get room
              $room = ORM::factory('room')
                ->where('id', '=', $room_id)
                ->find();

              /// Campaign prices 
              if ( ! $room->loaded() OR $room->hotel_id != $hotel->id)
                throw new Kohana_Exception('You are not authorized');

              if ($currency_code AND $currency_code != $currency->code)
                throw new Kohana_Exception("Failed to modify. Only $currency->code is supported for your hotel.");

              if ( ! Valid::date($date))
                throw new Kohana_Exception('Invalid date');
              
              // Find item
              $item = ORM::factory('item')
                ->where('room_id', '=', $room_id)
                ->where('items.campaign_id', '=', 0)
                ->where('date', '=', $date)
                ->group_by('date')
                ->group_by('is_campaign_prices')
                ->order_by('is_campaign_prices', 'DESC')
                ->find();

              if ($item->loaded())
              {
                if ($stock !==  '')
                {
                  if ($stock < 0)
                    throw new Kohana_Exception('Invalid quantity');
                  
                  $item->stock = (int) $stock;
                }
                else
                {
                  throw new Kohana_Exception('Invalid quantity');
                }
                
                if ($price !== '')
                {
                  if ($price < 1)
                    throw new Kohana_Exception('Invalid price');
                  
                  $item->price = (int) $price;
                }
                else
                {
                  throw new Kohana_Exception('Invalid price');
                }

                if ($minimum_night !== '')
                {
                  if ($minimum_night < 1)
                    throw new Kohana_Exception('Invalid minimum night');
                  
                  $item->minimum_night = (int) $minimum_night;
                }
                else
                {
                  $minimum_night = 1;
                }

                // Update item
                $item->update();
              }
              else
              {
                if ($price !== '' AND $stock !== '')
                {
                  if ($stock < 0)
                    throw new Kohana_Exception('Invalid quantity');
                  if ($price < 1)
                    throw new Kohana_Exception('Invalid price');
                  if ($minimum_night < 1)
                    throw new Kohana_Exception('Invalid minimum night');
                  else 
                    $minimum_night = 1;

                  $item->room_id = $room_id;
                  $item->campaign_id = $rate_id;
                  $item->date = $date;
                  $item->price = (int) $price;
                  $item->stock = (int) $stock;
                  $item->minimum_night = (int) $minimum_night;
                  $item->sold = 0;
                  $item->is_blackout = 0;
                  $item->is_campaign_blackout = 0;
                  
                  // Create item
                  $item->create();
                }
                else
                {
                  throw new Kohana_Exception('Price and quantity must be entered for new item');
                }
              }
            }
          }
        
          // Commit transaction
          $status_commit = Database::instance()->commit();
        }
        catch (Kohana_Exception $e)
        {
          // Rollback transaction
          Database::instance()->rollback();
          throw new Kohana_Exception($e->getMessage());
        }
        catch (Exception $e)
        {
          // Rollback transaction
          Database::instance()->rollback();
          Log::instance()->add(Log::INFO, $e->getMessage());
          throw new Kohana_Exception('Failed to modify');
        }
      }

      // Create success XML
      // Create xml document
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $response_node = $doc->createElement('Response');
      $response_node = $doc->appendChild($response_node);

      $ok_node = $doc->createElement('ok');
      $response_node->appendChild($ok_node);

      return $doc->saveXML();
    }
    else
    {
      throw new Kohana_Exception('You are not authorized');
    }
  }
  
  public function reservations($admin, $node)
  {
    $hotel_id = $node->getAttribute('hotelId');
    
    $hotel = ORM::factory('hotel')
      ->where('id', '=', $hotel_id)
      ->find();
    
    if ($admin->has('hotels', $hotel))
    {
      $start_date = $node->getAttribute('startDate');
      $end_date = $node->getAttribute('endDate');
      
      $bookings = ORM::factory('booking')
        ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
        ->where('bookings.check_in_date', '>=', $start_date)
        ->where('bookings.check_out_date', '<=', $end_date)
        ->where('rooms.hotel_id', '=', $hotel_id)
        ->find_all();
      
      // Create xml document
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $response_node = $doc->createElement('Response');
      $response_node = $doc->appendChild($response_node);
      
      foreach ($bookings as $booking)
      {
        // Get booking prices
        $booking_prices = Model_Booking::calculate_prices($booking->id);
        
        // Booking status
        $status = 1;
        if ($booking->is_done)
        {
          $status = 2;
        }
        if ($booking->is_canceled)
        {
          $status = 0;
        }
        
        $reservation_node = $doc->createElement('reservation');
        $reservation_node->setAttribute('id', $booking->id);
        $reservation_node->setAttribute('checkin', $booking->check_in_date);
        $reservation_node->setAttribute('checkout', $booking->check_out_date);
        $reservation_node->setAttribute('client', $booking->first_name.' '.$booking->last_name);
        $reservation_node->setAttribute('rooms', $booking->number_of_rooms);
        $reservation_node->setAttribute('price', $booking_prices['amount_paid_by_guest']);
        $reservation_node->setAttribute('status', $status);
        
        $response_node->appendChild($reservation_node);
      }
      
      return $doc->saveXML();
    }
    else
    {
      throw new Kohana_Exception('You are not authorized');
    }
  }
  
  public function download_reservations($admin, $node)
  {
    $hotel_id = $node->getAttribute('hotelId');
    
    $hotel = ORM::factory('hotel')
      ->where('id', '=', $hotel_id)
      ->find();
    
    if ($admin->has('hotels', $hotel))
    {
      $seven_days_ago = strtotime(Date::formatted_time('-7 day', 'Y-m-d', $hotel->timezone));
      
      $bookings = ORM::factory('booking')
        ->select(
          array('currencies.code', 'currency_code')
        )
        ->join('rooms')->on('rooms.id', '=', 'bookings.room_id')
        ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
        ->join('currencies')->on('currencies.id', '=', 'bookings.currency_id')
        ->where('bookings.created', '>=', $seven_days_ago)
        ->where('rooms.hotel_id', '=', $hotel_id)
        ->where('room_texts.language_id', '=', 1)
        ->find_all();
      
      // Create xml document
      $doc = new DOMDocument('1.0', 'UTF-8');
      $doc->formatOutput = TRUE;

      $response_node = $doc->createElement('Response');
      $response_node = $doc->appendChild($response_node);
      
      foreach ($bookings as $booking)
      {
        // Get booking prices
        $booking_prices = Model_Booking::calculate_prices($booking->id);
        
        // Booking status
        $status = 1;
        if ($booking->is_done)
        {
          $status = 2;
        }
        if ($booking->is_canceled)
        {
          $status = 0;
        }
        
        // Create reservation with detail
        $reservation_node = $doc->createElement('reservationWD');
        $reservation_node->setAttribute('id', $booking->id);
        $reservation_node->setAttribute('checkin', $booking->check_in_date);
        $reservation_node->setAttribute('checkout', $booking->check_out_date);
        $reservation_node->setAttribute('reservation_date', date('Y-m-d\TH:i:s', $booking->created + Date::offset($hotel->timezone)));
        $reservation_node->setAttribute('client', $booking->first_name.' '.$booking->last_name);
        $reservation_node->setAttribute('rooms', $booking->number_of_rooms);
        $reservation_node->setAttribute('price', $booking_prices['amount_paid_by_guest']);
        $reservation_node->setAttribute('status', $status);
        
        // Create reservation detail
        $reservation_detail_node = $doc->createElement('reservationDetail');
        $reservation_detail_node->setAttribute('id', $booking->room_id);
        $reservation_detail_node->setAttribute('currency', $booking->currency_code);
        $reservation_detail_node->setAttribute('checkin', $booking->check_in_date);
        $reservation_detail_node->setAttribute('checkout', $booking->check_out_date);
        $reservation_detail_node->setAttribute('roomId', $booking->room_id);
        $reservation_detail_node->setAttribute('quantity', $booking->number_of_rooms);
        $reservation_detail_node->setAttribute('price', $booking_prices['amount_paid_by_guest']);
        
        $reservation_node->appendChild($reservation_detail_node);
        
        foreach ($booking_prices['rooms'] as $room)
        {
          foreach ($room['items'] as $item)
          {
            // Create day price
            $day_price_node = $doc->createElement('dayPrice');
            
            $day_price_node->setAttribute('roomId', $item['room_id']);
            $day_price_node->setAttribute('day', $item['date']);
            $day_price_node->setAttribute('price', $item['price']);
            
            $reservation_node->appendChild($day_price_node);
          }
        }
        
        $response_node->appendChild($reservation_node);
      }
      
      return $doc->saveXML();
    }
    else
    {
      throw new Kohana_Exception('You are not authorized');
    }
  }
  
  public function action_api()
  {
    try
    {
      // Get xml data
      $xml_data = $this->request->body();
      
      if ( ! $xml_data)
        throw Kohana_Exception('XML can not be empty');
      
      // Log Request
      $request_time = date("Y-m-d H:i:s");
      Log::instance()->add(Log::INFO, 'Parity Rate - Request : '.$request_time);
      // Log::instance()->add(Log::INFO, $request_time.' data :'."\r\n".$xml_data);

      $doc = new DOMDocument();
      $doc->preserveWhiteSpace = false;
      $doc->loadXML($xml_data);
      
      // Get root element
      $root = $doc->documentElement;
      
      if ($root->nodeName != 'Request')
        throw new Kohana_Exception('Request not found');

      // Check the root children
      if ($root->hasChildNodes() AND $root->childNodes->length == 1)
      {
        // Get the username and password
        $username = $root->getAttribute('userName');
        $password = $root->getAttribute('password');
        
        // Get admin
        $admin = ORM::factory('admin')
          ->where('username', '=', $username)
          ->find();
        
        // Check user password
        if ( ! A1::instance()->check_password($admin, $password))
          throw new Kohana_Exception('Authentication failed');
      
        // Get the command
        $command = $root->firstChild->nodeName;
        
        switch ($command) {
          case 'getHotels':
            $response = $this->get_hotels($admin, $root->firstChild);
            break;
          case 'getRates': 
            $response = $this->get_rates($admin, $root->firstChild);
            break;
          case 'getRooms': 
            $response = $this->get_rooms($admin, $root->firstChild);
            break;
          case 'view': 
            $response = $this->view_request($admin, $root->firstChild);
            break;
          case 'modify':
            $response = $this->modify($admin, $root->firstChild);
            break;
          case 'reservations':
            $response = $this->reservations($admin, $root->firstChild);
            break;
          case 'downloadReservations':
            $response = $this->download_reservations($admin, $root->firstChild);
            break;
          default: 
            throw new Kohana_Exception('Command is not supported');
        }
      }
      else
      {
        throw new Kohana_Exception('Command not found');
      }
    }
    catch (Kohana_Exception $e)
    {
      $response = $this->create_error($e->getMessage());
    }
    
    $this->response
      ->headers('Content-Type', 'text/xml')
      ->body($response);
    // Log::instance()->add(Log::INFO, 'Parity Rate - Response : '.$request_time.':'."\r\n".$response);
    Log::instance()->add(Log::INFO, 'Parity Rate - Response : '.$request_time);

  }
  
}