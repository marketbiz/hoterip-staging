<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Package extends Controller_Layout_Admin {

    public function before() {
        if ($this->request->action() == 'delete') {
            $this->auto_render = FALSE;
        }

        parent::before();
    }

    public function action_manage() {
        // Is Authorized ?
        if (!A2::instance()->allowed('package', 'manage')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to home dashboard
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
        }

        if ($this->request->post('delete_checked')) {
            $package_ids = $this->request->post('ids');

            if (A2::instance()->allowed('package', 'delete')) {
                if ($package_ids) {
                    DB::delete('packages')
                            ->where('id', 'IN', $package_ids)
                            ->execute();
                }
            }
        }

        if ($this->request->post('show')) {
            $package_ids = $this->request->post('ids');

            if (A2::instance()->allowed('package', 'edit')) {
                if ($package_ids) {
                    DB::update('packages')
                            ->set(array('is_show' => 1))
                            ->where('id', 'IN', $package_ids)
                            ->execute();
                }
            }
        }

        if ($this->request->post('hide')) {
            $package_ids = $this->request->post('ids');

            if (A2::instance()->allowed('package', 'edit')) {
                DB::update('packages')
                        ->set(array('is_show' => 0))
                        ->where('id', 'IN', $package_ids)
                        ->execute();
            }
        }

        $packages = ORM::factory('package')
                ->select(
                        array('languages.code', 'language_code'), array('languages.name', 'language_name')
                )
                ->join('languages')->on('languages.id', '=', 'packages.language_id')
                ->order_by('order', 'DESC')
                ->find_all();

        $this->template->main = Kostache::factory('package/manage')
                ->set('notice', Notice::render())
                ->set('packages', $packages);
    }

    private function add1() {
        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            // Get post values
            $values = $this->request->post();
            // Filter post
            $values += Arr::extract($this->request->post(), array(
                        'is_adult_can_participate',
                        'is_child_can_participate',
                        'is_infant_can_participate',
                        'is_flight',
                        'is_hotel',
                        'is_activity',
                            ), 0);

            // Get package values from session and merge it
            $values += Session::instance()->get('package_values', array());

            // Set package values session
            Session::instance()->set('package_values', $values);

            // Redirect to step 2
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'add')) . URL::query(array('step' => 2)));
        }

        /**
         * Build variables for view
         */
        $languages = ORM::factory('language')
                ->find_all();

        $this->template->main = Kostache::factory('package/add1')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('languages', $languages);
    }

    private function add2() {
        /**
         * Get values and filter it
         */
        $values = Session::instance()->get('package_values', array());

        // Get duration days
        $duration_days = Arr::get($values, 'duration_days', 1);

        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            // Get session values
            $values = $this->request->post();

            // Filter
            $schedule_days = Arr::get($values, 'schedule_days', array());
            $schedule_is_breakfasts = Arr::get($values, 'schedule_is_breakfasts', array());
            $schedule_is_lunchs = Arr::get($values, 'schedule_is_lunchs', array());
            $schedule_is_dinners = Arr::get($values, 'schedule_is_dinners', array());

            // Loop schedule days
            foreach ($schedule_days as $key => $schedule_day) {
                if (!isset($schedule_is_breakfasts[$key])) {
                    $schedule_is_breakfasts[$key] = 0;
                }

                if (!isset($schedule_is_lunchs[$key])) {
                    $schedule_is_lunchs[$key] = 0;
                }

                if (!isset($schedule_is_dinners[$key])) {
                    $schedule_is_dinners[$key] = 0;
                }
            }

            $values['schedule_is_breakfasts'] = $schedule_is_breakfasts;
            $values['schedule_is_lunchs'] = $schedule_is_lunchs;
            $values['schedule_is_dinners'] = $schedule_is_dinners;

            // Get package values from session and merge it
            $values += Session::instance()->get('package_values', array());

            // Set package values session
            Session::instance()->set('package_values', $values);

            // Redirect to step 3
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'add')) . URL::query(array('step' => 3)));
        }

        $this->template->main = Kostache::factory('package/add2')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('duration_days', $duration_days);
    }

    private function add3() {
        /**
         * Get values and filter it
         */
        $values = Session::instance()->get('package_values', array());

        $stay_start_date = Arr::get($values, 'stay_start_date', date('Y-m-d'));
        $stay_end_date = Arr::get($values, 'stay_end_date', date('Y-m-d', strtotime('+2 days')));
        $language_id = Arr::get($values, 'language_id', 1);

        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            // Get values from post
            $values = $this->request->post();

            // Get dates
            $dates = Arr::get($values, 'item_dates', array());

            try {
                // Validation
                foreach ($dates as $date) {
                    $validation = Validation::factory(array(
                                'adult_price' => Arr::get($this->request->post('item_adult_prices'), $date, 0),
                                'child_price' => Arr::get($this->request->post('item_child_prices'), $date),
                                'infant_price' => Arr::get($this->request->post('item_infant_prices'), $date),
                            ))
                            ->rule('adult_price', 'not_empty', array(':value'))
                            ->rule('child_price', 'not_empty', array(':value'))
                            ->rule('infant_price', 'not_empty', array(':value'))
                            ->rule('adult_price', 'digit', array(':value'))
                            ->rule('child_price', 'digit', array(':value'))
                            ->rule('infant_price', 'digit', array(':value'))
                            ->label('adult_price', 'Adult Price')
                            ->label('child_price', 'Child Price')
                            ->label('infant_price', 'Infant Price');

                    // If validation failed
                    if (!$validation->check()) {
                        throw new Validation_Exception($validation);
                    }
                }

                $adult_prices = Arr::get($this->request->post(), 'item_adult_prices', array());

                if (count($adult_prices) > 0) {
                    sort($adult_prices, SORT_NUMERIC);
                    $price_from = $adult_prices[0];
                } else {
                    $price_from = 0;
                }

                $values['price_from'] = $price_from;

                // Get package values from session and merge it
                $values += Session::instance()->get('package_values', array());
                // Set package values session
                Session::instance()->set('package_values', $values);
                // Redirect to step 4
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'add')) . URL::query(array('step' => 4)));
            } catch (Validation_Exception $e) {
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->array->errors('package'));
            }
        }

        /**
         * Build variables for view
         */
        $stay_start_date_timestamp = strtotime($stay_start_date);
        $stay_end_date_timestamp = strtotime($stay_end_date);

        $dates = array();
        for ($i = $stay_start_date_timestamp; $i <= $stay_end_date_timestamp; $i += Date::DAY) {
            $date = date('Y-m-d', $i);
            $dates[] = $date;
        }

        $currency_name = DB::select('currencies.name')
                ->from('languages')
                ->join('currencies')->on('currencies.id', '=', 'languages.currency_id')
                ->where('languages.id', '=', $language_id)
                ->limit(1)
                ->execute()
                ->get('name');

        $this->template->main = Kostache::factory('package/add3')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('dates', $dates)
                ->set('currency_name', $currency_name);
    }

    private function add4() {
        if ($this->request->post('submit')) {
            $photos = Arr::rotate($_FILES['photos']);

            $validation = Validation::factory($photos);

            foreach ($photos as $key => $photo) {
                $validation->rule($key, 'Upload::type', array(':value', array('jpg')))
                        ->label($key, 'Photo');
            }

            if ($validation->check()) {
                // Get values from post
                $values = $this->request->post();

                $photo_filenames = array();
                foreach ($photos as $photo) {
                    // Save photo to temporary upload folder
                    $photo_filenames[] = basename(Upload::save($photo, NULL, 'media/upload'));
                }

                $values['photo_filenames'] = $photo_filenames;

                // Remove blank photos
                $values['photo_names'] = array_filter($values['photo_names']);
                $values['photo_filenames'] = array_filter($values['photo_filenames']);

                // Get package values from session and merge it
                $values += Session::instance()->get('package_values', array());
                // Set package values session
                Session::instance()->set('package_values', $values);

                // Redirect to step 5
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'add')) . URL::query(array('step' => 5)));
            } else {
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $validation->errors('package'));
            }
        }

        $this->template->main = Kostache::factory('package/add4')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post());
    }

    private function add5() {
        /**
         * Get values and filter it
         */
        $values = Session::instance()->get('package_values', array());

        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            $values += $this->request->post();

            // Start transaction
            Database::instance()->begin();

            try {
                /**
                 * Create package
                 */
                $package = ORM::factory('package')
                        ->values($values, array(
                            'language_id',
                            'name',
                            'description',
                            'is_adult_can_participate',
                            'is_child_can_participate',
                            'is_infant_can_participate',
                            'minimum_adults',
                            'duration_days',
                            'duration_nights',
                            'price_from',
                            'booking_start_date',
                            'booking_end_date',
                            'stay_start_date',
                            'stay_end_date',
                            'is_flight',
                            'flight_places',
                            'flight_airline',
                            'is_hotel',
                            'hotel_names',
                            'is_activity',
                            'calendar_caption',
                            'cancellation_policy',
                            'order',
                            'is_show',
                        ))
                        ->create();

                /**
                 * Create schedules
                 */
                $schedule_days = Arr::get($values, 'schedule_days', array());
                $schedule_descriptions = Arr::get($values, 'schedule_descriptions', array());
                $schedule_places = Arr::get($values, 'schedule_places', array());
                $schedule_is_breakfasts = Arr::get($values, 'schedule_is_breakfasts', array());
                $schedule_is_lunchs = Arr::get($values, 'schedule_is_lunchs', array());
                $schedule_is_dinners = Arr::get($values, 'schedule_is_dinners', array());

                foreach ($schedule_days as $schedule_day) {
                    ORM::factory('package_schedule')
                            ->values(array(
                                'package_id' => $package->id,
                                'day' => $schedule_day,
                                'description' => Arr::get($schedule_descriptions, $schedule_day),
                                'place' => Arr::get($schedule_places, $schedule_day),
                                'is_breakfast' => Arr::get($schedule_is_breakfasts, $schedule_day),
                                'is_lunch' => Arr::get($schedule_is_lunchs, $schedule_day),
                                'is_dinner' => Arr::get($schedule_is_dinners, $schedule_day),
                            ))
                            ->create();
                }

                /**
                 * Create items
                 */
                $item_dates = Arr::get($values, 'item_dates', array());
                $item_adult_prices = Arr::get($values, 'item_adult_prices', array());
                $item_child_prices = Arr::get($values, 'item_child_prices', array());
                $item_infant_prices = Arr::get($values, 'item_infant_prices', array());

                foreach ($item_dates as $item_date) {
                    ORM::factory('package_item')
                            ->values(array(
                                'package_id' => $package->id,
                                'date' => $item_date,
                                'adult_price' => Arr::get($item_adult_prices, $item_date),
                                'child_price' => Arr::get($item_child_prices, $item_date),
                                'infant_price' => Arr::get($item_infant_prices, $item_date),
                            ))
                            ->create();
                }

                /**
                 * Create photos
                 */
                $photo_names = Arr::get($values, 'photo_names', array());
                $photo_filenames = Arr::get($values, 'photo_filenames', array());

                // Create package directory
                $package_directory = Kohana::config('application.package_directory') . '/package_' . $package->id;

                // If directory not exist
                if (!is_dir($package_directory)) {
                    mkdir($package_directory);
                }

                foreach ($photo_filenames as $key => $photo_filename) {
                    $basename = URL::title(Arr::get($photo_names, $key));

                    if (!Valid::alpha_dash($basename)) {
                        $basename = Text::random('alnum', 30);
                    }

                    $extension = pathinfo($photo_filename, PATHINFO_EXTENSION);

                    // Save large filename
                    Image::factory('media/upload/' . $photo_filename)
                            ->resize(640, 480, IMAGE::INVERSE)
                            ->crop(640, 480)
                            ->save($package_directory . '/' . $basename . '_org.' . $extension);

                    // Save medium filename
                    Image::factory($package_directory . '/' . $basename . '_org.' . $extension)
                            ->resize(220, 165, IMAGE::INVERSE)
                            ->crop(220, 165)
                            ->save($package_directory . '/' . $basename . '.' . $extension);

                    // Save small filename
                    Image::factory($package_directory . '/' . $basename . '.' . $extension)
                            ->resize(56, 42, IMAGE::INVERSE)
                            ->crop(56, 42)
                            ->save($package_directory . '/' . $basename . '_thumb.' . $extension);

                    // Delete uploaded photo file
                    unlink('media/upload/' . $photo_filename);

                    // Insert to database
                    ORM::factory('package_photo')
                            ->values(array(
                                'package_id' => $package->id,
                                'name' => Arr::get($photo_names, $key),
                                'large_filename' => $basename . '_org.' . $extension,
                                'filename' => $basename . '.' . $extension,
                                'small_filename' => $basename . '_thumb.' . $extension,
                            ))
                            ->create();
                }

                // Commit database
                Database::instance()->commit();
                // Clear session
                Session::instance()->delete('package_values');
                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
                // Redirect to manage package
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'manage')));
            } catch (ORM_Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'));
            } catch (Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        /**
         * Build variables for view
         */
        $language = ORM::factory('language')
                ->where('id', '=', Arr::get($values, 'language_id', 1))
                ->find();

        $currency = ORM::factory('currency')
                ->where('id', '=', $language->currency_id)
                ->find();

        $this->template->main = Kostache::factory('package/add5')
                ->set('notice', Notice::render())
                ->set('values', $values)
                ->set('language', $language)
                ->set('currency', $currency);
    }

    public function action_add() {
        // Is Authorized ?
        if (!A2::instance()->allowed('package', 'add')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage campaigns
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'manage')));
        }

        // Get step
        $step = Arr::get($this->request->query(), 'step', 1);

        switch ($step) {
            case 1:
                $this->add1();
                break;
            case 2:
                $this->add2();
                break;
            case 3:
                $this->add3();
                break;
            case 4:
                $this->add4();
                break;
            case 5:
                $this->add5();
                break;
        }
    }

    private function edit1($package_id) {
        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            // Get post values
            $values = $this->request->post();
            // Filter post
            $values += Arr::extract($this->request->post(), array(
                        'is_adult_can_participate',
                        'is_child_can_participate',
                        'is_infant_can_participate',
                        'is_flight',
                        'is_hotel',
                        'is_activity',
                            ), 0);

            // Get package values from session and merge it
            $values += Session::instance()->get('package_values', array());

            // Set package values session
            Session::instance()->set('package_values', $values);

            // Redirect to step 2
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'edit', 'id' => $package_id)) . URL::query(array('step' => 2)));
        }

        /**
         * Build variables for view
         */
        $package = ORM::factory('package')
                ->where('id', '=', $package_id)
                ->find();

        $languages = ORM::factory('language')
                ->find_all();

        $this->template->main = Kostache::factory('package/edit1')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('package', $package)
                ->set('languages', $languages);
    }

    private function edit2($package_id) {
        /**
         * Get values and filter it
         */
        $values = Session::instance()->get('package_values', array());

        // Get duration days
        $duration_days = Arr::get($values, 'duration_days', 1);

        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            // Get session values
            $values = $this->request->post();

            // Filter
            $schedule_days = Arr::get($values, 'schedule_days', array());
            $schedule_is_breakfasts = Arr::get($values, 'schedule_is_breakfasts', array());
            $schedule_is_lunchs = Arr::get($values, 'schedule_is_lunchs', array());
            $schedule_is_dinners = Arr::get($values, 'schedule_is_dinners', array());

            // Loop schedule days
            foreach ($schedule_days as $key => $schedule_day) {
                if (!isset($schedule_is_breakfasts[$key])) {
                    $schedule_is_breakfasts[$key] = 0;
                }

                if (!isset($schedule_is_lunchs[$key])) {
                    $schedule_is_lunchs[$key] = 0;
                }

                if (!isset($schedule_is_dinners[$key])) {
                    $schedule_is_dinners[$key] = 0;
                }
            }

            $values['schedule_is_breakfasts'] = $schedule_is_breakfasts;
            $values['schedule_is_lunchs'] = $schedule_is_lunchs;
            $values['schedule_is_dinners'] = $schedule_is_dinners;

            // Get package values from session and merge it
            $values += Session::instance()->get('package_values', array());

            // Set package values session
            Session::instance()->set('package_values', $values);

            // Redirect to step 3
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'edit', 'id' => $package_id)) . URL::query(array('step' => 3)));
        }

        /**
         * Build variables for view
         */
        $package_schedules = ORM::factory('package_schedule')
                ->where('package_id', '=', $package_id)
                ->find_all()
                ->as_array('day');

        $this->template->main = Kostache::factory('package/edit2')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('duration_days', $duration_days)
                ->set('package_schedules', $package_schedules);
    }

    private function edit3($package_id) {
        /**
         * Get values and filter it
         */
        $values = Session::instance()->get('package_values', array());

        $stay_start_date = Arr::get($values, 'stay_start_date', date('Y-m-d'));
        $stay_end_date = Arr::get($values, 'stay_end_date', date('Y-m-d', strtotime('+2 days')));
        $language_id = Arr::get($values, 'language_id', 1);

        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            // Get values from post
            $values = $this->request->post();

            // Get dates
            $dates = Arr::get($values, 'item_dates', array());

            try {
                // Validation
                foreach ($dates as $date) {
                    $validation = Validation::factory(array(
                                'adult_price' => Arr::get($this->request->post('item_adult_prices'), $date, 0),
                                'child_price' => Arr::get($this->request->post('item_child_prices'), $date),
                                'infant_price' => Arr::get($this->request->post('item_infant_prices'), $date),
                            ))
                            ->rule('adult_price', 'not_empty', array(':value'))
                            ->rule('child_price', 'not_empty', array(':value'))
                            ->rule('infant_price', 'not_empty', array(':value'))
                            ->rule('adult_price', 'digit', array(':value'))
                            ->rule('child_price', 'digit', array(':value'))
                            ->rule('infant_price', 'digit', array(':value'))
                            ->label('adult_price', 'Adult Price')
                            ->label('child_price', 'Child Price')
                            ->label('infant_price', 'Infant Price');

                    // If validation failed
                    if (!$validation->check()) {
                        throw new Validation_Exception($validation);
                    }
                }

                $adult_prices = Arr::get($this->request->post(), 'item_adult_prices', array());

                if (count($adult_prices) > 0) {
                    sort($adult_prices, SORT_NUMERIC);
                    $price_from = $adult_prices[0];
                } else {
                    $price_from = 0;
                }

                $values['price_from'] = $price_from;

                // Get package values from session and merge it
                $values += Session::instance()->get('package_values', array());
                // Set package values session
                Session::instance()->set('package_values', $values);
                // Redirect to step 4
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'edit', 'id' => $package_id)) . URL::query(array('step' => 4)));
            } catch (Validation_Exception $e) {
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->array->errors('package'));
            }
        }

        /**
         * Build variables for view
         */
        $stay_start_date_timestamp = strtotime($stay_start_date);
        $stay_end_date_timestamp = strtotime($stay_end_date);

        $dates = array();
        for ($i = $stay_start_date_timestamp; $i <= $stay_end_date_timestamp; $i += Date::DAY) {
            $date = date('Y-m-d', $i);
            $dates[] = $date;
        }

        $package_items = ORM::factory('package_item')
                ->where('package_id', '=', $package_id)
                ->find_all()
                ->as_array('date');

        $currency_name = DB::select('currencies.name')
                ->from('languages')
                ->join('currencies')->on('currencies.id', '=', 'languages.currency_id')
                ->where('languages.id', '=', $language_id)
                ->limit(1)
                ->execute()
                ->get('name');

        $this->template->main = Kostache::factory('package/edit3')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('dates', $dates)
                ->set('package_items', $package_items)
                ->set('currency_name', $currency_name);
    }

    private function edit4($package_id) {
        if ($this->request->post('submit')) {
            $photos = Arr::rotate($_FILES['photos']);

            $validation = Validation::factory($photos);

            foreach ($photos as $key => $photo) {
                $validation->rule($key, 'Upload::type', array(':value', array('jpg')))
                        ->label($key, 'Photo');
            }

            if ($validation->check()) {
                // Get values from post
                $values = $this->request->post();

                $photo_filenames = array();
                foreach ($photos as $photo) {
                    // Save photo to temporary upload folder
                    $photo_filenames[] = basename(Upload::save($photo, NULL, 'media/upload'));
                }

                $package_photos = ORM::factory('package_photo')
                        ->where('package_id', '=', $package_id)
                        ->find_all()
                        ->as_array();

                foreach ($photo_filenames as $key => $photo_filename) {
                    if (!$photo_filename) {
                        if ($package_photo = Arr::get($package_photos, $key)) {
                            // Get package directory
                            $package_directory = Kohana::config('application.package_directory') . '/package_' . $package_photo->package_id;
                            // Create random filename
                            $new_filename = Text::random('alnum', 30) . '.jpg';
                            // Copy 
                            copy($package_directory . '/' . $package_photo->filename, 'media/upload/' . $new_filename);
                            // Insert to array
                            $photo_filenames[$key] = $new_filename;
                        }
                    }
                }

                $values['photo_filenames'] = $photo_filenames;

                // Remove blank photos
                $values['photo_names'] = array_filter($values['photo_names']);
                $values['photo_filenames'] = array_filter($values['photo_filenames']);

                // Get package values from session and merge it
                $values += Session::instance()->get('package_values', array());
                // Set package values session
                Session::instance()->set('package_values', $values);

                // Redirect to step 5
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'edit', 'id' => $package_id)) . URL::query(array('step' => 5)));
            } else {
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $validation->errors('package'));
            }
        }

        $package_photos = ORM::factory('package_photo')
                ->where('package_id', '=', $package_id)
                ->find_all()
                ->as_array();

        $this->template->main = Kostache::factory('package/edit4')
                ->set('notice', Notice::render())
                ->set('values', $this->request->post())
                ->set('package_photos', $package_photos);
    }

    private function edit5($package_id) {
        /**
         * Get values and filter it
         */
        $values = Session::instance()->get('package_values', array());

        /**
         * Form submitted
         */
        if ($this->request->post('submit')) {
            $values += $this->request->post();

            // Start transaction
            Database::instance()->begin();

            try {
                /**
                 * Update package
                 */
                $package = ORM::factory('package')
                        ->where('id', '=', $package_id)
                        ->find()
                        ->values($values, array(
                            'language_id',
                            'name',
                            'description',
                            'is_adult_can_participate',
                            'is_child_can_participate',
                            'is_infant_can_parcipate',
                            'minimum_adults',
                            'duration_days',
                            'duration_nights',
                            'price_from',
                            'booking_start_date',
                            'booking_end_date',
                            'stay_start_date',
                            'stay_end_date',
                            'is_flight',
                            'flight_places',
                            'flight_airline',
                            'is_hotel',
                            'hotel_names',
                            'is_activity',
                            'calendar_caption',
                            'cancellation_policy',
                            'order',
                            'is_show',
                        ))
                        ->update();

                /**
                 * Create and update schedules
                 */
                $schedule_days = Arr::get($values, 'schedule_days', array());
                $schedule_descriptions = Arr::get($values, 'schedule_descriptions', array());
                $schedule_places = Arr::get($values, 'schedule_places', array());
                $schedule_is_breakfasts = Arr::get($values, 'schedule_is_breakfasts', array());
                $schedule_is_lunchs = Arr::get($values, 'schedule_is_lunchs', array());
                $schedule_is_dinners = Arr::get($values, 'schedule_is_dinners', array());

                foreach ($schedule_days as $schedule_day) {
                    ORM::factory('package_schedule')
                            ->where('package_id', '=', $package->id)
                            ->where('day', '=', $schedule_day)
                            ->find()
                            ->values(array(
                                'package_id' => $package->id,
                                'day' => $schedule_day,
                                'description' => Arr::get($schedule_descriptions, $schedule_day),
                                'place' => Arr::get($schedule_places, $schedule_day),
                                'is_breakfast' => Arr::get($schedule_is_breakfasts, $schedule_day),
                                'is_lunch' => Arr::get($schedule_is_lunchs, $schedule_day),
                                'is_dinner' => Arr::get($schedule_is_dinners, $schedule_day),
                            ))
                            ->save();
                }

                /**
                 * Create and update items
                 */
                $item_dates = Arr::get($values, 'item_dates', array());
                $item_adult_prices = Arr::get($values, 'item_adult_prices', array());
                $item_child_prices = Arr::get($values, 'item_child_prices', array());
                $item_infant_prices = Arr::get($values, 'item_infant_prices', array());

                foreach ($item_dates as $item_date) {
                    ORM::factory('package_item')
                            ->where('package_id', '=', $package->id)
                            ->where('date', '=', $item_date)
                            ->find()
                            ->values(array(
                                'package_id' => $package->id,
                                'date' => $item_date,
                                'adult_price' => Arr::get($item_adult_prices, $item_date),
                                'child_price' => Arr::get($item_child_prices, $item_date),
                                'infant_price' => Arr::get($item_infant_prices, $item_date),
                            ))
                            ->save();
                }

                /**
                 * Update photos
                 */
                $photo_names = Arr::get($values, 'photo_names', array());
                $photo_filenames = Arr::get($values, 'photo_filenames', array());

                // Create package directory
                $package_directory = Kohana::config('application.package_directory') . '/package_' . $package->id;

                // If directory not exist
                if (!is_dir($package_directory)) {
                    mkdir($package_directory);
                }

                // Delete package photos
                DB::delete('package_photos')
                        ->where('package_id', '=', $package->id)
                        ->execute();

                foreach ($photo_filenames as $key => $photo_filename) {
                    $basename = URL::title(Arr::get($photo_names, $key));

                    if (!Valid::alpha_dash($basename)) {
                        $basename = Text::random('alnum', 30);
                    }

                    $extension = pathinfo($photo_filename, PATHINFO_EXTENSION);

                    // Save large filename
                    Image::factory('media/upload/' . $photo_filename)
                            ->resize(640, 480, IMAGE::INVERSE)
                            ->crop(640, 480)
                            ->save($package_directory . '/' . $basename . '_org.' . $extension);

                    // Save medium filename
                    Image::factory($package_directory . '/' . $basename . '_org.' . $extension)
                            ->resize(220, 165, IMAGE::INVERSE)
                            ->crop(220, 165)
                            ->save($package_directory . '/' . $basename . '.' . $extension);

                    // Save small filename
                    Image::factory($package_directory . '/' . $basename . '.' . $extension)
                            ->resize(56, 42, IMAGE::INVERSE)
                            ->crop(56, 42)
                            ->save($package_directory . '/' . $basename . '_thumb.' . $extension);

                    // Delete photo file
                    unlink('media/upload/' . $photo_filename);

                    // Insert to database
                    ORM::factory('package_photo')
                            ->values(array(
                                'package_id' => $package->id,
                                'name' => Arr::get($photo_names, $key),
                                'large_filename' => $basename . '_org.' . $extension,
                                'filename' => $basename . '.' . $extension,
                                'small_filename' => $basename . '_thumb.' . $extension,
                            ))
                            ->create();
                }

                // Commit database
                Database::instance()->commit();
                // Clear session
                Session::instance()->delete('package_values');
                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success'));
                // Redirect to manage package
                $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'manage')));
            } catch (ORM_Validation_Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'));
            } catch (Exception $e) {
                // Rollback transaction
                Database::instance()->rollback();
                // Add error notice
                Notice::add(Notice::ERROR, $e->getMessage());
            }
        }

        /**
         * Build variables for view
         */
        $package = ORM::factory('package')
                ->where('id', '=', $package_id)
                ->find();

        $language = ORM::factory('language')
                ->where('id', '=', Arr::get($values, 'language_id', 1))
                ->find();

        $currency = ORM::factory('currency')
                ->where('id', '=', $language->currency_id)
                ->find();

        $this->template->main = Kostache::factory('package/edit5')
                ->set('notice', Notice::render())
                ->set('values', $values)
                ->set('package', $package)
                ->set('language', $language)
                ->set('currency', $currency);
    }

    public function action_edit() {
        // Is Authorized ?
        if (!A2::instance()->allowed('package', 'edit')) {
            // Add error notice
            Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
            // Redirect to manage campaigns
            $this->request->redirect(Route::get('default')->uri(array('controller' => 'package', 'action' => 'manage')));
        }

        // Get step
        $step = Arr::get($this->request->query(), 'step', 1);

        // Get package id
        $package_id = $this->request->param('id');

        switch ($step) {
            case 1:
                $this->edit1($package_id);
                break;
            case 2:
                $this->edit2($package_id);
                break;
            case 3:
                $this->edit3($package_id);
                break;
            case 4:
                $this->edit4($package_id);
                break;
            case 5:
                $this->edit5($package_id);
                break;
        }
    }

    public function action_delete() {
        $package_id = $this->request->param('id');

        if (A2::instance()->allowed('package', 'delete')) {
            DB::delete('packages')
                    ->where('id', '=', $package_id)
                    ->execute();

            Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
        }

        $this->request->redirect($this->request->referrer());
    }

}
