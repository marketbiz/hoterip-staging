<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Video extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage()
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('hotel_video', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		$hotel_videos = ORM::factory('hotel_video')
			->select(
				array('hotel_video_texts.name', 'name')
			)
			->join('hotel_video_texts')->on('hotel_video_texts.hotel_video_id', '=', 'hotel_videos.id')
			->where('hotel_videos.hotel_id', '=', $this->selected_hotel->id)
			->where('hotel_video_texts.language_id', '=', $this->selected_language->id)
			->find_all();
		
		$this->template->main = Kostache::factory('video/manage')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('hotel_videos', $hotel_videos);
	}
	
	public function action_add()
	{
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();
				$values['hotel_id'] = $this->selected_hotel->id;
			
				// Create hotel video
				$hotel_video = ORM::factory('hotel_video')
					->create_hotel_video($values);

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'video', 'action' => 'manage')), Kohana::message('video', 'manage_videos'))));

				// Redirect to edit video
				$this->request->redirect(Route::get('default')->uri(array('controller' => 'video', 'action' => 'edit', 'id' => $hotel_video->id)));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->errors('video'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('video/add')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('languages', $languages);
	}
	
	public function action_edit()
	{
		// Get hotel video id
		$hotel_video_id = (int) $this->request->param('id');
		
		// Get hotel video
		$hotel_video = ORM::factory('hotel_video')
			->where('id', '=', $hotel_video_id)
			->find();
		
		// Is Authorized ?
		if ( ! A2::instance()->allowed($hotel_video, 'edit'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to manage rooms
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'video', 'action' => 'manage')));
		}
		
		if ($this->request->post('submit'))
		{
			try
			{
				$values = $this->request->post();
				$values['hotel_id'] = $this->selected_hotel->id;
				
				// Update hotel video
				$hotel_video = ORM::factory('hotel_video')
					->where('id', '=', $hotel_video_id)
					->find()
					->update_hotel_video($values);
					
				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'save_success_with_link'), array(':link' => HTML::anchor(Route::get('default')->uri(array('controller' => 'video', 'action' => 'manage')), Kohana::message('video', 'manage_videos'))));
			}
			catch (ORM_Validation_Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'save_failed'), NULL, $e->erros('video'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, $e->getMessage());
			}
		}
		
		// Get hotel video
		$hotel_video = ORM::factory('hotel_video')
			->where('id', '=', $hotel_video_id)
			->find();
		
		// Get hotel video texts
		$hotel_video_texts = ORM::factory('hotel_video_text')
			->where('hotel_video_id', '=', $hotel_video->id)
			->find_all()
			->as_array('language_id');
		
		// Get languages
		$languages = ORM::factory('language')
			->find_all();
		
		$this->template->main = Kostache::factory('video/edit')
			->set('notice', Notice::render())
			->set('values', $this->request->post())
			->set('hotel_video', $hotel_video)
			->set('hotel_video_texts', $hotel_video_texts)
			->set('languages', $languages);
	}
	
	public function action_delete()
	{
		// Get hotel_video id
		$hotel_video_id = $this->request->param('id');
		
		// Get hotel_video
		$hotel_video = ORM::factory('hotel_video')
			->where('id', '=', $hotel_video_id)
			->find();
		
		if (A2::instance()->allowed($hotel_video, 'delete'))
		{
			try
			{
				// Delete hotel_video
				$hotel_video->delete();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}