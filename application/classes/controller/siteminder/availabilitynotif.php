<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Siteminder_Availabilitynotif extends Controller {
    /*
     * List USED Model
     * Hotel :get_campaign
     * Campaign :get_items
     * Room capacity :getroom, checkroom, extrabed
     * Country/City  : getbysegment
     * Timezone  : timezone identifier
     * Setting  : get
     * Exchane  : To
     */

    public function checker($transaction, $head, $body, $xmlns) {
        $data = array();
        $error = array();

        $OTA_HotelAvailRQ = $body->$transaction->attributes();
        $node_head = array(
            'head' => $transaction,
            'xmlns' => $xmlns,
            'TimeStamp' => (string) $OTA_HotelAvailRQ->TimeStamp,
            'Version' => (string) $OTA_HotelAvailRQ->Version,
            'EchoToken' => (string) $body->$transaction->attributes()->EchoToken,
        );

        $auth = array(
            'User' => (string) $head->Security->UsernameToken->Username,
            'Pwd' => (string) $head->Security->UsernameToken->Password,
        );

        // Check node head null
        if (empty($node_head['xmlns']) || empty($node_head['TimeStamp']) || empty($node_head['Version']) || empty($node_head['EchoToken'])) {
            $error['Empty_Property']['type'] = '10';
            $error['Empty_Property']['code'] = '321';
        }
        // Check Auth null request
        elseif (empty($auth['User']) || empty($auth['Pwd'])) {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '321';
        } else {
            // Check Auth
            if (Controller_Vacation_Static::auth($auth) == FALSE) {
                $error['Authentification']['type'] = '4';
                $error['Authentification']['code'] = '9900';
            } else {
                $AvailStatusMessages = $body->$transaction->AvailStatusMessages->AvailStatusMessage;

                $data = array(
                    'hotel_id' => (string) $body->$transaction->AvailStatusMessages->attributes()->HotelCode,
                );

                // data quque
                $i = 0;

                foreach ($AvailStatusMessages as $AvailStatusMessage_list) {
                    $data['available'][$i] = array(
                        'BookingLimit' => (string) $AvailStatusMessage_list->attributes()->BookingLimit,
                        'Start' => (string) $AvailStatusMessage_list->StatusApplicationControl->attributes()->Start,
                        'End' => (string) $AvailStatusMessage_list->StatusApplicationControl->attributes()->End,
                        'InvTypeCode' => (string) $AvailStatusMessage_list->StatusApplicationControl->attributes()->InvTypeCode,
                        'RatePlanCode' => (string) $AvailStatusMessage_list->StatusApplicationControl->attributes()->RatePlanCode
                    );

                    // Check Null request
                    if (empty($data['available'][$i]['Start']) || empty($data['available'][$i]['End']) || empty($data['available'][$i]['InvTypeCode']) || empty($data['available'][$i]['RatePlanCode'])) {
                        $error['AvailStatusMessage']['type'] = '10';
                        $error['AvailStatusMessage']['code'] = '321';
                        break;
                    }

                    // Check Format request
                    if (!is_numeric($data['available'][$i]['InvTypeCode'])
                    ) {
                        $error['InvTypeCode']['type'] = '3';
                        $error['InvTypeCode']['code'] = '402';
                        break;
                    }

                    if (!is_numeric($data['available'][$i]['RatePlanCode'])
                    ) {
                        $error['InvTypeCode']['type'] = '3';
                        $error['RatePlanCode']['code'] = '249';
                        break;
                    }

                    // Check start and end date format
                    if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['available'][$i]['Start']) || !preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['available'][$i]['End'])
                    ) {
                        $error['Start_or_End']['type'] = '3';
                        $error['Start_or_End']['code'] = '400';
                        break;
                    }

                    if (isset($AvailStatusMessage_list->RestrictionStatus)) {
                        $data['available'][$i]['Status'] = strtolower((string) $AvailStatusMessage_list->RestrictionStatus->attributes()->Status) == 'open' ? 0 : 1;
                        $data['available'][$i]['Restriction'] = (string) $AvailStatusMessage_list->RestrictionStatus->attributes()->Restriction;
                    }

                    if (isset($AvailStatusMessage_list->LengthsOfStay)) {
                        $data['available'][$i]['MinMaxMessageType'] = (string) $AvailStatusMessage_list->LengthsOfStay->children()->attributes()->MinMaxMessageType;
                        $data['available'][$i]['Time'] = (string) $AvailStatusMessage_list->LengthsOfStay->children()->attributes()->Time;

                        // Check Null request
                        if (empty($data['available'][$i]['MinMaxMessageType']) || empty($data['available'][$i]['Time'])
                        ) {
                            $error['LengthsOfStay']['type'] = '10';
                            $error['LengthsOfStay']['code'] = '321';
                        }
                    }
                    // remove variable nodes
                    unset($AvailStatusMessage_list);
                    $i++;
                }
            }
        }

        $data = array_merge($data, $node_head);

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Siteminder_Static::errors($error, $node_head);
            $this->output($errors);
        } else {
            $this->process($data);
        }
    }

    public function process($data) {
        try {
            foreach ($data['available'] as $key => $single_data) {
                $hotel_id = $data['hotel_id'];
                $room_id = $single_data['InvTypeCode'];
                $campaign_id = $single_data['RatePlanCode'];

                // Create dates array
                $start_date_timestamp = strtotime($single_data['Start']);
                $end_date_timestamp = strtotime($single_data['End']);

                $dates = array();
                for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                    $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
                }

                // Load Currency Hotel
                $currency = DB::select('currency_id')->from('hotels')
                        ->where('id', '=', $hotel_id)
                        ->as_object()
                        ->execute()
                        ->current();

                $currency_id = $currency ? $currency->currency_id : 1;

                // Load id currency request
                $currency_request = ORM::factory('currency')
                                ->where('code', '=', 'USD')
                                ->find()->as_array();

                // Factory exchange
                $exchange = Model::factory('exchange');

                // Changed items 
                $changed_prev_items = array();
                $changed_items = array();

                // Item Price
                $base = $single_data['RateAmounts']['Base'];
                $item_price = $base['base_after_tax'] * ($exchange->to($currency_request['id'], $currency_id));

                // Dates property process
                foreach ($dates as $date) {
                    if (strtolower($single_data['Restriction']) == 'arrival' || strtolower($single_data['Restriction']) == 'departure') {
                        $values = array(
                            'date' => $date,
                            'is_no_arrival' => strtolower($single_data['Restriction']) == 'arrival' ? TRUE : FALSE,
                            'is_no_departure' => strtolower($single_data['Restriction']) == 'departure' ? TRUE : FALSE,
                        );

                        if ($values['is_no_arrival'] AND $single_data['Status']) {
                            // Create no arrival
                            ORM::factory('hotel_no_arrival')
                                    ->values(array(
                                        'hotel_id' => $hotel_id,
                                        'date' => $date,
                                    ))
                                    ->create();

                            // Write log
                            ORM::factory('log')
                                    ->values(array(
                                        'admin_id' => A1::instance()->get_user()->id,
                                        'hotel_id' => $hotel_id,
                                        'action' => 'Changed No Arrival',
                                        'room_texts' => NULL,
                                        'text' => 'Date: :date. No Arrival: :is_no_arrival',
                                        'data' => serialize(array(
                                            ':date' => date('M j, Y', strtotime($date)),
                                            ':is_no_arrival' => 'Yes',
                                        )),
                                        'created' => time(),
                                    ))
                                    ->create();

                            // If there is QA admins
                            if (count($qa_emails) > 0) {
                                // Send email
                                Email::factory(strtr(':hotel_name - Changed No Arrival', array(
                                            ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                                )), strtr(
                                                        "Action: Changed No Arrival.\n"
                                                        . "By: :admin_username (:admin_name).\n"
                                                        . "Date: :date.\n"
                                                        . "No Arrival: :is_no_arrival.", array(
                                            ':admin_username' => A1::instance()->get_user()->username,
                                            ':admin_name' => A1::instance()->get_user()->name,
                                            ':date' => date('M j, Y', strtotime($date)),
                                            ':is_no_arrival' => 'Yes',
                                        )))
                                        ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                        ->to($qa_emails)
                                ; //->send();
                            }
                        } else {
                            // Find no arrival
                            $no_arrival = ORM::factory('hotel_no_arrival')
                                    ->where('hotel_id', '=', $hotel_id)
                                    ->where('date', '=', $date)
                                    ->find();

                            // If exist
                            if ($no_arrival->loaded()) {
                                // Delete
                                DB::delete('hotel_no_arrivals')
                                        ->where('hotel_id', '=', $hotel_id)
                                        ->where('date', '=', $date)
                                        ->execute();

                                // Write log
                                ORM::factory('log')
                                        ->values(array(
                                            'admin_id' => A1::instance()->get_user()->id,
                                            'hotel_id' => $hotel_id,
                                            'action' => 'Changed No Arrival',
                                            'room_texts' => NULL,
                                            'text' => 'Date: :date. No Arrival: :is_no_arrival',
                                            'data' => serialize(array(
                                                ':date' => date('M j, Y', strtotime($date)),
                                                ':is_no_arrival' => 'No',
                                            )),
                                            'created' => time(),
                                        ))
                                        ->create();

                                // If there is QA admins
                                if (count($qa_emails) > 0) {
                                    // Send email
                                    Email::factory(strtr(':hotel_name - Changed No Arrival', array(
                                                ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                                    )), strtr(
                                                            "Action: Changed No Arrival.\n"
                                                            . "By: :admin_username (:admin_name).\n"
                                                            . "Date: :date.\n"
                                                            . "No Arrival: :is_no_arrival.", array(
                                                ':admin_username' => A1::instance()->get_user()->username,
                                                ':admin_name' => A1::instance()->get_user()->name,
                                                ':date' => date('M j, Y', strtotime($date)),
                                                ':is_no_arrival' => 'No',
                                            )))
                                            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                            ->to($qa_emails)
                                    ; //->send();
                                }
                            }
                        }

                        if ($values['is_no_departure'] AND $single_data['Status']) {
                            ORM::factory('hotel_no_departure')
                                    ->values(array(
                                        'hotel_id' => $hotel_id,
                                        'date' => $date,
                                    ))
                                    ->create();

                            // Write log
                            ORM::factory('log')
                                    ->values(array(
                                        'admin_id' => A1::instance()->get_user()->id,
                                        'hotel_id' => $hotel_id,
                                        'action' => 'Changed No Departure',
                                        'room_texts' => NULL,
                                        'text' => 'Date: :date. No Departure: :is_no_departure',
                                        'data' => serialize(array(
                                            ':date' => date('M j, Y', strtotime($date)),
                                            ':is_no_departure' => 'Yes',
                                        )),
                                        'created' => time(),
                                    ))
                                    ->create();

                            // If there is QA admins
                            if (count($qa_emails) > 0) {
                                // Send email
                                Email::factory(strtr(':hotel_name - Changed No Departure', array(
                                            ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                                )), strtr(
                                                        "Action: Changed No Departure.\n"
                                                        . "By: :admin_username (:admin_name).\n"
                                                        . "Date: :date.\n"
                                                        . "No Departure: :is_no_departure.", array(
                                            ':admin_username' => A1::instance()->get_user()->username,
                                            ':admin_name' => A1::instance()->get_user()->name,
                                            ':date' => date('M j, Y', strtotime($date)),
                                            ':is_no_departure' => 'Yes',
                                        )))
                                        ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                        ->to($qa_emails)
                                ; //->send();
                            }
                        } else {
                            // Find no departure
                            $no_departure = ORM::factory('hotel_no_departure')
                                    ->where('hotel_id', '=', $hotel_id)
                                    ->where('date', '=', $date)
                                    ->find();

                            // If exist
                            if ($no_departure->loaded()) {
                                // Delete
                                DB::delete('hotel_no_departures')
                                        ->where('hotel_id', '=', $hotel_id)
                                        ->where('date', '=', $date)
                                        ->execute();

                                // Write log
                                ORM::factory('log')
                                        ->values(array(
                                            'admin_id' => A1::instance()->get_user()->id,
                                            'hotel_id' => $hotel_id,
                                            'action' => 'Changed No Departure',
                                            'room_texts' => NULL,
                                            'text' => 'Date: :date. No Departure: :is_no_departure',
                                            'data' => serialize(array(
                                                ':date' => date('M j, Y', strtotime($date)),
                                                ':is_no_departure' => 'No',
                                            )),
                                            'created' => time(),
                                        ))
                                        ->create();

                                // If there is QA admins
                                if (count($qa_emails) > 0) {
                                    // Send email
                                    Email::factory(strtr(':hotel_name - Changed No Departure', array(
                                                ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                                    )), strtr(
                                                            "Action: Changed No Departure.\n"
                                                            . "By: :admin_username (:admin_name).\n"
                                                            . "Date: :date.\n"
                                                            . "No Departure: :is_no_departure.", array(
                                                ':admin_username' => A1::instance()->get_user()->username,
                                                ':admin_name' => A1::instance()->get_user()->name,
                                                ':date' => date('M j, Y', strtotime($date)),
                                                ':is_no_departure' => 'No',
                                            )))
                                            ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                            ->to($qa_emails)
                                    ; //->send();
                                }
                            }
                        }
                    } else {
                        /*
                         * Update Item
                         */
                        // Get the item
                        $item = DB::select()
                                ->from('items')
                                ->where('room_id', '=', $room_id)
                                ->where('campaign_id', '=', $campaign_id)
                                ->where('date', '=', $date)
                                ->as_object()
                                ->execute()
                                ->current();

                        // If item already exist
                        if ($item) {
                            $values = array(
                                'room_id' => $room_id,
                                'campaign_id' => $campaign_id,
                                'date' => $date,
                                'price' => $item_price ? $item_price : (string) $item->price,
                                'minimum_night' => isset($single_data['MinMaxMessageType']) && ($single_data['MinMaxMessageType'] == 'SetMinLOS') ? $single_data['Time'] : 1,
                                'stock' => isset($single_data['BookingLimit']) ? (string) $single_data['BookingLimit'] : (string) $item->stock,
                                'is_blackout' => isset($single_data['Status']) ? (string) $single_data['Status'] : (string) $item->is_blackout,
                            );

                            // Clone item to write log
                            $prev_item = clone $item;

                            // Update
                            DB::update('items')
                                    ->set($values)
                                    ->where('room_id', '=', $room_id)
                                    ->where('campaign_id', '=', $campaign_id)
                                    ->where('date', '=', $date)
                                    ->execute();
                        } else {
                            // Get item default
                            $item_default = DB::select()
                                    ->from('items')
                                    ->where('room_id', '=', $room_id)
                                    ->where('campaign_id', '=', 0)
                                    ->where('date', '=', $date)
                                    ->as_object()
                                    ->execute()
                                    ->current();

                            $values = array(
                                'room_id' => $room_id,
                                'campaign_id' => $campaign_id,
                                'date' => $date,
                                'price' => $item_price ? $item_price : 0,
                                'minimum_night' => isset($single_data['MinMaxMessageType']) && ($single_data['MinMaxMessageType'] == 'SetMinLOS') ? $single_data['Time'] : 1,
                                'stock' => isset($single_data['BookingLimit']) ? (string) $single_data['BookingLimit'] : (string) 0,
                                'is_blackout' => isset($single_data['Status']) ? (string) $single_data['Status'] : (string) $item_default->is_blackout,
                            );

                            // Build prev item class
                            $prev_item = new stdClass;
                            $prev_item->stock = NULL;
                            $prev_item->price = NULL;
                            $prev_item->net_price = NULL;
                            $prev_item->minimum_night = NULL;
                            $prev_item->is_blackout = NULL;
                            $prev_item->is_campaign_blackout = NULL;

                            // Create new
                            DB::insert('items')
                                    ->columns(array_keys($values))
                                    ->values(array_values($values))
                                    ->execute();
                        }

                        // Get the item
                        $item = DB::select()
                                ->from('items')
                                ->where('room_id', '=', $room_id)
                                ->where('campaign_id', '=', $campaign_id)
                                ->where('date', '=', $date)
                                ->as_object()
                                ->execute()
                                ->current();

                        // Get room
                        $room = DB::select()
                                ->from('rooms')
                                ->where('id', '=', $room_id)
                                ->as_object()
                                ->execute()
                                ->current();

                        // Set log values
                        $log_values = array(
                            'admin_id' => A1::instance()->get_user()->id,
                            'hotel_id' => $room->hotel_id,
                            'action' => 'Changed Item',
                            'room_texts' => serialize(DB::select()
                                            ->from('room_texts')
                                            ->where('room_id', '=', $room->id)
                                            ->execute()
                                            ->as_array('language_id')),
                            'text' => 'Date: :date. '
                            . 'Stock: :prev_stock -> :stock. '
                            . 'Price: :prev_price -> :price. '
                            . 'Minimum Night: :prev_minimum_night -> :minimum_night. '
                            . 'Stop Sell: :prev_is_blackout -> :is_blackout. '
                            . 'Stop Promotion: :prev_is_campaign_blackout -> :is_campaign_blackout.',
                            'data' => serialize(array(
                                ':prev_stock' => $prev_item->stock ? $prev_item->stock : 'None',
                                ':prev_price' => $prev_item->price ? Num::display($prev_item->price) : 'None',
                                ':prev_minimum_night' => $prev_item->minimum_night ? $prev_item->minimum_night : 'None',
                                ':prev_is_blackout' => $prev_item->is_blackout ? 'Yes' : 'No',
                                ':prev_is_campaign_blackout' => $prev_item->is_campaign_blackout ? 'Yes' : 'No',
                                ':date' => date('M j, Y', strtotime($item->date)),
                                ':stock' => $item->stock,
                                ':price' => Num::display($item->price),
                                ':minimum_night' => $item->minimum_night,
                                ':is_blackout' => $item->is_blackout ? 'Yes' : 'No',
                                ':is_campaign_blackout' => $item->is_campaign_blackout ? 'Yes' : 'No',
                            )),
                            'created' => time(),
                        );

                        // Write log
                        DB::insert('logs')
                                ->columns(array_keys($log_values))
                                ->values(array_values($log_values))
                                ->execute();

                        // Track changed items
                        if ($prev_item->stock !== $item->stock OR
                                $prev_item->price !== $item->price OR
                                $prev_item->minimum_night !== $item->minimum_night OR
                                $prev_item->is_blackout !== $item->is_blackout OR
                                $prev_item->is_campaign_blackout !== $item->is_campaign_blackout) {
                            $changed_prev_items[$date] = $prev_item;
                            $changed_items[$date] = $item;
                        }
                    }

                    // Get QA admins emails
                    $qa_emails = ORM::factory('admin')
                            ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                            ->where('admins.role', '=', 'quality-assurance')
                            ->where('admins_hotels.hotel_id', '=', $this->selected_hotel->id)
                            ->find_all()
                            ->as_array('email', 'name');

                    // If there is QA admins
                    if (count($qa_emails) > 0) {
                        ksort($changed_prev_items);
                        ksort($changed_items);

                        // Create email message
                        $message = Kostache::factory('item/qa/manage')
                                ->set('admin_username', A1::instance()->get_user()->username)
                                ->set('admin_name', A1::instance()->get_user()->name)
                                ->set('room_name', ORM::factory('room_text')->where('room_texts.room_id', '=', $room->id)->where('room_texts.language_id', '=', 1)->find()->name)
                                ->set('changed_prev_items', $changed_prev_items)
                                ->set('changed_items', $changed_items)
                                ->render();

                        // Send email
                        Email::factory(strtr(':hotel_name - Changed Item', array(
                                    ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                                        )), $message, 'text/html')
                                ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                                ->to($qa_emails)
                                ->send();
                    }
                }

                // Commit transaction
                Database::instance()->commit();
                // Add success notice
                Notice::add(Notice::SUCCESS, Kohana::message('notice', 'save_success'));
            }
        } catch (Kohana_Exception $e) {
            // Rollback transaction
            Database::instance()->rollback();
            // Add error notice
            $error['Exception']['type'] = '12';
            $error['Exception']['code'] = '450';
        }

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Siteminder_Static::errors($error, $data);
            $this->output($errors);
        } else {
            // Build response
            return $this->xml_build($data);
        }
    }

    public function xml_build($data) {
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('SOAP-ENV:Envelope'));
            $envelope->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');
            $head = $envelope->appendChild($doc->createElement('SOAP-ENV:Header'));
            $body = $envelope->appendChild($doc->createElement('SOAP-ENV:Body'));
            $body->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

            $response_node = $body->appendChild($doc->createElement(substr($data['head'], 0, -2) . 'RS'));
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
            $response_node->setAttribute('EchoToken', $data['EchoToken']);
            $response_node->setAttribute('Version', $data['Version']);

            $success = $response_node->appendChild($doc->createElement('Success'));

            $response = $doc->saveXML();

            $this->output($response);
        } catch (Kohana_Exception $e) {
            $error['XML_build']['type'] = '2';
            $error['XML_build']['code'] = '450';

            $errors = Controller_Siteminder_Static::errors($error, $data);
            $this->output($errors);
        }
    }

    public function output($response) {
        Log::instance()->add(Log::INFO, 'Hoterip response for siteminder');
        Log::instance()->add(Log::INFO, $response);
        // Logout account
        A1::instance()->logout();

        $this->response->headers('Content-Type', 'text/xml')->body($response);
    }

    public function action_request() {
        // Set time limit
        set_time_limit(60);

        $xml_location = file_get_contents('php://input');
        // $xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/vacation.xml';
        // $data = file_get_contents($xml_location);
        // $xml_location = trim( stripslashes( $data ));
        echo "<pre>";
        print_r($xml_location);
        die();
        if (!$xml_location) {
            $node_head = array(
                'head' => 'ResponseRQ',
                'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                'TimeStamp' => date('c'),
                'EchoToken' => '000',
                'Target' => 'Production',
                'Version' => '1.00',
                'PrimaryLangID' => 'En',
            );

            $error['Welcome']['type'] = '2';
            $error['Welcome']['code'] = '450';

            $errors = Controller_Siteminder_Static::errors($error, $node_head);

            $this->output($errors);
        } else {
            $xml = new SimpleXMLElement($xml_location);

            $namespace = $xml->getDocNamespaces(TRUE);
            foreach ($namespace as $html_element => $value) {
                $xmlns = $value;
            }

            $head = $xml->children('SOAP-ENV', true)->Header->children('wsse', true);
            $body = $xml->children('SOAP-ENV', true)->Body->children();

            $transaction = $body->getName();

            if ($transaction === 'OTA_HotelAvailNotifRQ') {
                $data = $this->checker($transaction, $head, $body, $xmlns);
            } else {
                $node_head = array(
                    'head' => 'ResponseRQ',
                    'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                    'TimeStamp' => date('c'),
                    'EchoToken' => '000',
                    'Target' => 'Production',
                    'Version' => '1.00',
                    'PrimaryLangID' => 'En',
                );

                $error['False_request']['type'] = '2';
                $error['False_request']['code'] = '450';
                $errors = Controller_Siteminder_Static::errors($error, $node_head);

                $this->output($errors);
            }
        }
    }

}
