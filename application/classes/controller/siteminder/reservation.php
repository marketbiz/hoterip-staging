<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Siteminder_Reservation extends Controller {

    public function action_res() {
        echo 'res';
    }

    public static function HotelRes_siteminder($booking_exist) {
        $result = FALSE;

        /// Hoterip campaign
        $exchange = Model::factory('exchange');
        // service_tax_rate
        $service_tax_rate = (($booking_exist['hotel']->service_charge_rate + $booking_exist['hotel']->tax_rate) + 100) / 100;
        // Total Amount
        $AmountAfterTax_total_all = 0;
        $AmountBeforeTax_total_all = 0;

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('SOAP-ENV:Envelope'));
            $envelope->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

            $head = $envelope->appendChild($doc->createElement('SOAP-ENV:Header'));
            $head->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

            $Security = $head->appendChild($doc->createElement('wsse:Security'));
            $Security->setAttribute('soap:mustUnderstand', 1);
            $Security->setAttribute('xmlns:wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');
            $Security->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $UsernameToken = $Security->appendChild($doc->createElement('wsse:UsernameToken'));
            $Username = $UsernameToken->appendChild($doc->createElement('wsse:Username', Kohana::config('api.siteminder.User')));
            $Password = $UsernameToken->appendChild($doc->createElement('wsse:Password', Kohana::config('api.siteminder.Pwd')));
            $Password->setAttribute('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');

            $body = $envelope->appendChild($doc->createElement('SOAP-ENV:Body'));
            $body->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');

            $response_node = $body->appendChild($doc->createElement('OTA_HotelResNotifRQ'));
            $response_node->setAttribute('ResStatus', 'Commit');
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('Version', '1.0');
            $response_node->setAttribute('EchoToken', $booking_exist['guid_token']);
            $response_node->setAttribute('ResStatus', 'Commit');
            $response_node->setAttribute('TimeStamp', date('c'));

            $pos = $response_node->appendChild($doc->createElement('POS'));
            $source = $pos->appendChild($doc->createElement('Source'));
            $RequestorID = $source->appendChild($doc->createElement('RequestorID'));
            $RequestorID->setAttribute('Type', 22);
            /* change */
            $RequestorID->setAttribute('ID', 'HTP'); //Kohana::config('api.siteminder.siteminder_code'));
            $BookingChannel = $source->appendChild($doc->createElement('BookingChannel'));
            $BookingChannel->setAttribute('Primary', 'true');
            $BookingChannel->setAttribute('Type', 7);
            $CompanyName = $BookingChannel->appendChild($doc->createElement('CompanyName', 'Hoterip'));
            $CompanyName->setAttribute('Code', Kohana::config('api.siteminder.Code'));
            /* change */

            $HotelReservations = $response_node->appendChild($doc->createElement('HotelReservations'));
            $HotelReservation = $HotelReservations->appendChild($doc->createElement('HotelReservation'));
            $HotelReservation->setAttribute('CreateDateTime', date('c'));

            $UniqueID = $HotelReservation->appendChild($doc->createElement('UniqueID'));
            $UniqueID->setAttribute('Type', 14);
            $UniqueID->setAttribute('ID', $booking_exist['booking_id']);

            $RoomStays = $HotelReservation->appendChild($doc->createElement('RoomStays'));

            foreach ($booking_exist['items'] as $num => $items) {
                $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));
                // $RoomStay->setAttribute('PromotionCode', $booking_exist['campaign_id']);

                $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                $RoomType->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomType->setAttribute('NumberOfUnits', 1);
                $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
                $RoomDescription->setAttribute('Name', $booking_exist['room_texts'][1]->name);

                // Rateplan descriptions
                $campaign = (array) unserialize($booking_exist['campaign']);
                $campaign['campaign_default'] = $campaign['is_default'];
                $campaign['rate'] = $campaign['discount_rate'];
                $campaign['amount'] = $campaign['discount_amount'];
                $campaign['currency_code'] = $booking_exist['data']['hotel_currency']->code;
                $campaign['free_night'] = $campaign['minimum_number_of_nights'];
                $campaign['last_days'] = $campaign['within_days_of_arrival'];
                $campaign['hotel_timezone'] = $booking_exist['hotel']->timezone;
                $campaign['check_in'] = $booking_exist['check_in_date'];
                $campaign['check_out'] = $booking_exist['check_out_date'];
                $campaign['early_days'] = $campaign['days_in_advance'];
                $campaign['min_nights'] = $campaign['minimum_number_of_nights'];
                $campaign['min_rooms'] = $campaign['minimum_number_of_rooms'];

                $benefit = Controller_Siteminder_Static::promotion_descriptions($campaign);

                $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription', $benefit));
                $RatePlan->setAttribute('RatePlanCode', $booking_exist['campaign_id']);

                // Supposed to be commisions

                $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));
                $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                $RoomRate->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $RoomRate->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomRate->setAttribute('NumberOfUnits', 1);

                // Items
                $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                $AmountAfterTax_total = 0;
                $AmountBeforeTax_total = 0;
                $price_after_tax = 0;
                $price_before_tax = 0;

                foreach ($items['items'] as $ord => $item) {
                    if ($item['stock'] > 0 && !$item['is_blackout'] && !$item['is_campaign_blackout']) {
                        // Add one day
                        $date = $item['date'];
                        $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');

                        $price_before_tax = round(($item['price'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1));
                        $price_after_tax = round($item['price'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1));

                        $Rate = $Rates->appendChild($doc->createElement('Rate'));
                        $Rate->setAttribute('EffectiveDate', $date);
                        $Rate->setAttribute('ExpireDate', $add_one_day);
                        $Rate->setAttribute('UnitMultiplier', 1);
                        $Rate->setAttribute('RateTimeUnit', 'Day');

                        $Base = $Rate->appendChild($doc->createElement('Base'));
                        $Base->setAttribute('AmountAfterTax', $price_after_tax);
                        $Base->setAttribute('AmountBeforeTax', $price_before_tax);
                        $Base->setAttribute('CurrencyCode', 'USD');
                    }

                    // Count for total prices
                    $AmountAfterTax_total += $price_after_tax;
                    $AmountBeforeTax_total += $price_before_tax;
                }

                $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                $GuestCounts->setAttribute('IsPerRoom', 1);
                $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                $GuestCount->setAttribute('AgeQualifyingCode', 10);
                $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['adult']);

                if ($booking_exist['data']['capacities'][$num]['child']) {
                    $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                    $GuestCount->setAttribute('AgeQualifyingCode', 8);
                    $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['child']);
                }

                // Night span
                $night_span = (strtotime($booking_exist['data']['check_in']) - strtotime($booking_exist['data']['check_out'])) / (60 * 60 * 24);

                $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
                $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);

                // $AmountAfterTax = $booking_exist['data']['invoice_amount'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // $AmountBeforeTax = ($booking_exist['data']['invoice_amount'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // //$tax = $AmountAfterTax - $AmountBeforeTax;
                // Total Amount all
                $AmountAfterTax_total_all += round($AmountAfterTax_total);
                $AmountBeforeTax_total_all += round($AmountBeforeTax_total);

                $Total = $RoomStay->appendChild($doc->createElement('Total'));
                $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total));
                $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total));
                $Total->setAttribute('CurrencyCode', 'USD');
                // $Taxes = $Rate->appendChild($doc->createElement('Taxes'));
                // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
                // $Tax->setAttribute('Amount', round($tax));
                // $Tax->setAttribute('CurrencyCode', 'USD');

                $BasicPropertyInfo = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                $BasicPropertyInfo->setAttribute('HotelCode', $booking_exist['data']['hotel_id']);
                $BasicPropertyInfo->setAttribute('HotelName', $booking_exist['data']['hotel_name']);

                $ResGuestRPHs = $RoomStay->appendChild($doc->createElement('ResGuestRPHs'));
                $ResGuestRPH = $ResGuestRPHs->appendChild($doc->createElement('ResGuestRPH'));
                $ResGuestRPH->setAttribute('RPH', 1);

                $Comments = $RoomStay->appendChild($doc->createElement('Comments'));
                $Comment = $Comments->appendChild($doc->createElement('Comment'));
                $Comment->setAttribute('GuestViewable', 'true');
                $Text = $Comment->appendChild($doc->createElement('Text', htmlspecialchars(utf8_encode($booking_exist['request_note']))));

                $booking_exist['note'] .= $booking_exist['request_note'] ? ', guest comment:' . $booking_exist['request_note'] : NULL;
                $booking_exist['note'] .= $booking_exist['is_early_check_in_request'] ? ', Early Check In' : NULL;
                $booking_exist['note'] .= $booking_exist['is_high_floor_request'] ? ', High flor' : NULL;
                $booking_exist['note'] .= $booking_exist['is_large_bed_request'] ? ', Large Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_twin_beds_request'] ? ', Twin Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_airport_transfer_request'] ? ', Airport Transfer' : NULL;

                $SpecialRequests = $RoomStay->appendChild($doc->createElement('SpecialRequests'));

                if ($booking_exist['is_early_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'early check in request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Early check in request'));
                }
                if ($booking_exist['is_high_floor_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'high floor');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'high floor request'));
                }
                if ($booking_exist['is_large_bed_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'large bed request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'large bed request'));
                }
                if ($booking_exist['is_twin_beds_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'twin beds');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'twin beds request'));
                }
                if ($booking_exist['is_airport_transfer_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'airport transfer');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'airport transfer request'));
                }
                if ($booking_exist['is_non_smoking_room_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'non smoking room request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'non smoking room request'));
                }
                if ($booking_exist['is_late_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'late check in');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'late check in request'));
                }
            }

            $ResGuests = $HotelReservation->appendChild($doc->createElement('ResGuests'));
            $ResGuest = $ResGuests->appendChild($doc->createElement('ResGuest'));
            $ResGuest->setAttribute('ResGuestRPH', 1);
            $ResGuest->setAttribute('PrimaryIndicator', 1);
            //$ResGuest->setAttribute('ArrivalTime', 'true');
            $Profiles = $ResGuest->appendChild($doc->createElement('Profiles'));
            $ProfileInfo = $Profiles->appendChild($doc->createElement('ProfileInfo'));
            $Profile = $ProfileInfo->appendChild($doc->createElement('Profile'));
            $Profile->setAttribute('ProfileType', 1);
            $Customer = $Profile->appendChild($doc->createElement('Customer'));
            $PersonName = $Customer->appendChild($doc->createElement('PersonName'));
            $GivenName = $PersonName->appendChild($doc->createElement('GivenName', $booking_exist['first_name']));
            $Surname = $PersonName->appendChild($doc->createElement('Surname', $booking_exist['last_name']));
            //$Telephone = $Customer->appendChild($doc->createElement('Telephone'));
            $Email = $Customer->appendChild($doc->createElement('Email', $booking_exist['email']));
            //$Address = $Customer->appendChild($doc->createElement('Address'));

            $ResGlobalInfo = $HotelReservation->appendChild($doc->createElement('ResGlobalInfo'));

            // Night span
            //$night_span = (strtotime($booking_exist['data']['check_in']) - strtotime($booking_exist['data']['check_out'])) / (60 * 60 * 24);
            // $TimeSpan = $ResGlobalInfo->appendChild($doc->createElement('TimeSpan'));
            // $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
            // $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
            // $TimeSpan->setAttribute('Duration', 'P1D');

            $Total = $ResGlobalInfo->appendChild($doc->createElement('Total'));
            $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total_all));
            $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total_all));
            $Total->setAttribute('CurrencyCode', 'USD');
            $Taxes = $Total->appendChild($doc->createElement('Taxes'));
            // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
            // $Tax->setAttribute('Amount', round($tax));
            // $Tax->setAttribute('CurrencyCode', 'USD');

            $HotelReservationIDs = $ResGlobalInfo->appendChild($doc->createElement('HotelReservationIDs'));
            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $booking_exist['booking_id']);

            $doc->appendChild($envelope);

            $request = $doc->saveXML();


            CustomLog::factory()->add(7, 'DEBUG', 'Siteminder reservation notification request');
            CustomLog::factory()->add(7, 'DEBUG', $request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, Kohana::config('api.siteminder.url_connect'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Content-length: " . strlen($request)));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $response = curl_exec($ch);
            echo "<pre>";
            print_r((string) $response);
            echo "</pre>";
            die();
            // Close the handle
            curl_close($ch);
            /*
             * XML process
             */
            try {
                $xml = new SimpleXMLElement($response);
                $body = $xml->children('SOAP-ENV', true)->Body->children();
                $OTA = $body->OTA_HotelResNotifRS;

                // Check response Success/ Errors
                if ($OTA->Success != null) {
                    DB::delete('api_reservation_queues')->where('api', '=', 2)->where('request_type', '=', 1)->where('booking_id', '=', $booking_exist['booking_id'])->execute();

                    CustomLog::factory()->add(7, 'DEBUG', 'Siteminder reservation notification Success');
                    CustomLog::factory()->add(7, 'DEBUG', $response);
                }
            } catch (Exception $e) {
                Database::instance()->rollback();

                $booking_exist['retries'] = $booking_exist['retries'] + 1;

                DB::update('api_reservation_queues')
                        ->set(
                                array('retries' => $booking_exist['retries'])
                        )
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();
                CustomLog::factory()->add(7, 'DEBUG', 'Siteminder reservation notification Failed');
                CustomLog::factory()->add(7, 'DEBUG', $response);
            }
        } catch (Kohana_Exception $e) {
            $booking_exist['retries'] = $booking_exist['retries'] + 1;

            DB::update('api_reservation_queues')
                    ->set(
                            array('retries' => $booking_exist['retries'])
                    )
                    ->where('booking_id', '=', $booking_exist['booking_id'])
                    ->where('request_type', '=', $booking_exist['request_type'])
                    ->where('api', '=', $booking_exist['api'])
                    ->execute();

            CustomLog::factory()->add(7, 'DEBUG', 'Siteminder reservation notification failed');

            $values = $this->request->post();
            foreach ($values as $key => $value) {
                CustomLog::factory()->add(7, 'DEBUG', "$key: $value");
            }
        }
    }

    public static function HotelRes_siteminder_cancel($booking_exist) {

        $success = TRUE;

        /// Hoterip campaign
        $exchange = Model::factory('exchange');
        // service_tax_rate
        $service_tax_rate = (($booking_exist['hotel']->service_charge_rate + $booking_exist['hotel']->tax_rate) + 100) / 100;

        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
            $envelope->setAttribute('xmlns:soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');

            $head = $envelope->appendChild($doc->createElement('soap-env:Header'));
            $Security = $head->appendChild($doc->createElement('wsse:Security'));
            $Security->setAttribute('soap:mustUnderstand', 1);
            $Security->setAttribute('xmlns:wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');
            $Security->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $UsernameToken = $Security->appendChild($doc->createElement('wsse:UsernameToken'));
            $Username = $UsernameToken->appendChild($doc->createElement('wsse:Username', Kohana::config('api.siteminder.User')));
            $Password = $UsernameToken->appendChild($doc->createElement('wsse:Password', Kohana::config('api.siteminder.Pwd')));
            $Password->setAttribute('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');

            $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
            $body->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');

            $response_node = $body->appendChild($doc->createElement('OTA_HotelResNotifRQ'));
            $response_node->setAttribute('ResStatus', 'Cancel');
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('Version', '1.0');
            $response_node->setAttribute('EchoToken', $booking_exist['guid_token']);
            $response_node->setAttribute('ResStatus', 'Cancel');
            $response_node->setAttribute('TimeStamp', date('c'));

            $pos = $response_node->appendChild($doc->createElement('POS'));
            $source = $pos->appendChild($doc->createElement('Source'));
            $RequestorID = $source->appendChild($doc->createElement('RequestorID'));
            $RequestorID->setAttribute('Type', 14);
            /* change */
            $RequestorID->setAttribute('ID', Kohana::config('api.siteminder.id'));
            $BookingChannel = $source->appendChild($doc->createElement('BookingChannel'));
            $BookingChannel->setAttribute('Primary', 'true');
            $BookingChannel->setAttribute('Type', 7);
            $CompanyName = $BookingChannel->appendChild($doc->createElement('CompanyName', 'Hoterip'));
            $CompanyName->setAttribute('Code', Kohana::config('api.siteminder.code'));
            /* change */

            $HotelReservations = $response_node->appendChild($doc->createElement('HotelReservations'));
            $HotelReservation = $HotelReservations->appendChild($doc->createElement('HotelReservation'));
            $HotelReservation->setAttribute('LastModifyDateTime', date('c'));

            $UniqueID = $HotelReservation->appendChild($doc->createElement('UniqueID'));
            $UniqueID->setAttribute('Type', $booking_exist['room_id']);
            $UniqueID->setAttribute('ID', $booking_exist['booking_id']);

            $RoomStays = $HotelReservation->appendChild($doc->createElement('RoomStays'));

            foreach ($booking_exist['items'] as $num => $items) {

                // echo "<pre>";
                // print_r($booking_exist);
                // die();
                // $price_before_tax =  ($item['price'] / $service_tax_rate)  * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // $price_after_tax =  $item['price']  * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // // Extrabed Price
                // $extrabed_price = $item['extrabed_item_price'] != 0.1 ? $item['extrabed_item_price'] : $booking_exist['data']['extrabed_price'];
                // $extrabed_price = ($extrabed_price * $items['number_of_extrabeds']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // // Surcharge Price
                // $surcharge_price = ($booking_exist['data']['total_surcharge']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // // Total Price Final Calculations
                // $price_before_tax = $price_before_tax + $extrabed_price + $surcharge_price;
                // $price_after_tax = $price_after_tax + $extrabed_price + $surcharge_price;

                $RoomStay = $RoomStays->appendChild($doc->createElement('RoomStay'));
                $RoomStay->setAttribute('PromotionCode', $booking_exist['campaign_id']);

                $RoomTypes = $RoomStay->appendChild($doc->createElement('RoomTypes'));
                $RoomType = $RoomTypes->appendChild($doc->createElement('RoomType'));
                $RoomType->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomType->setAttribute('NumberOfUnits', 1);
                $RoomDescription = $RoomType->appendChild($doc->createElement('RoomDescription'));
                $RoomDescription->setAttribute('Name', $booking_exist['room_texts'][1]->name);

                // Rateplan descriptions
                $campaign = (array) unserialize($booking_exist['campaign']);
                $campaign['campaign_default'] = $campaign['is_default'];
                $campaign['rate'] = $campaign['discount_rate'];
                $campaign['amount'] = $campaign['discount_amount'];
                $campaign['currency_code'] = $booking_exist['data']['hotel_currency']->code;
                $campaign['free_night'] = $campaign['minimum_number_of_nights'];
                $campaign['last_days'] = $campaign['within_days_of_arrival'];
                $campaign['hotel_timezone'] = $booking_exist['hotel']->timezone;
                $campaign['check_in'] = !empty($campaign['check_in_date']) ? $campaign['check_in_date'] : '0000-00-00';
                $campaign['check_out'] = !empty($campaign['check_out_date']) ? $campaign['check_out_date'] : '0000-00-00';
                $campaign['early_days'] = $campaign['days_in_advance'];
                $campaign['min_nights'] = $campaign['minimum_number_of_nights'];
                $campaign['min_rooms'] = $campaign['minimum_number_of_rooms'];

                $benefit = Controller_Siteminder_Static::promotion_descriptions($campaign);

                $RatePlans = $RoomStay->appendChild($doc->createElement('RatePlans'));
                $RatePlan = $RatePlans->appendChild($doc->createElement('RatePlan'));
                $RatePlanDescription = $RatePlan->appendChild($doc->createElement('RatePlanDescription', $benefit));
                $RatePlan->setAttribute('RatePlanCode', $booking_exist['campaign_id']);

                // Supposed to be commisions

                $RoomRates = $RoomStay->appendChild($doc->createElement('RoomRates'));
                $RoomRate = $RoomRates->appendChild($doc->createElement('RoomRate'));
                $RoomRate->setAttribute('RatePlanCode', $booking_exist['campaign_id']);
                $RoomRate->setAttribute('RoomTypeCode', $booking_exist['room_id']);
                $RoomRate->setAttribute('NumberOfUnits', 1);

                // Items
                $Rates = $RoomRate->appendChild($doc->createElement('Rates'));
                $AmountAfterTax_total = 0;
                $AmountBeforeTax_total = 0;

                foreach ($items['items'] as $ord => $item) {
                    // if($item['stock'] > 0 && !$item['is_blackout'] && !$item['is_campaign_blackout'])
                    // {
                    // Add one day
                    $date = $item['date'];
                    $add_one_day = Date::formatted_time("$date + 1 days", 'Y-m-d');

                    $price_before_tax = ($item['price'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    $price_after_tax = $item['price'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Extrabed Price
                    $extrabed_price = $item['extrabed_item_price'] != 0.1 ? $item['extrabed_item_price'] : $booking_exist['data']['extrabed_price'];
                    $extrabed_price = ($extrabed_price * $items['number_of_extrabeds']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);

                    // Surcharge Price
                    $surcharge_price = ($booking_exist['data']['total_surcharge']) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                    // Total Price Final Calculations
                    $price_before_tax = $price_before_tax + $extrabed_price + $surcharge_price;
                    $price_after_tax = $price_after_tax + $extrabed_price + $surcharge_price;

                    $Rate = $Rates->appendChild($doc->createElement('Rate'));
                    $Rate->setAttribute('EffectiveDate', $date);
                    $Rate->setAttribute('ExpireDate', $add_one_day);
                    $Rate->setAttribute('UnitMultiplier', 1);
                    $Rate->setAttribute('RateTimeUnit', 'Day');

                    $Base = $Rate->appendChild($doc->createElement('Base'));
                    $Base->setAttribute('AmountAfterTax', round($price_after_tax, 2));
                    $Base->setAttribute('AmountBeforeTax', round($price_before_tax, 2));
                    $Base->setAttribute('CurrencyCode', 'USD');
                    // }
                    // Count for total prices
                    $AmountAfterTax_total += $price_after_tax;
                    $AmountBeforeTax_total += $price_before_tax;
                }

                $GuestCounts = $RoomStay->appendChild($doc->createElement('GuestCounts'));
                $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                $GuestCount->setAttribute('AgeQualifyingCode', 10);
                $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['adult']);

                if ($booking_exist['data']['capacities'][$num]['child']) {
                    $GuestCount = $GuestCounts->appendChild($doc->createElement('GuestCount'));
                    $GuestCount->setAttribute('AgeQualifyingCode', 8);
                    $GuestCount->setAttribute('Count', $booking_exist['data']['capacities'][$num]['child']);
                }

                // Night span
                $night_span = (strtotime($booking_exist['data']['check_in']) - strtotime($booking_exist['data']['check_out'])) / (60 * 60 * 24);

                $TimeSpan = $RoomStay->appendChild($doc->createElement('TimeSpan'));
                $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
                $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);

                // $AmountAfterTax = $booking_exist['data']['invoice_amount'] * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // $AmountBeforeTax = ($booking_exist['data']['invoice_amount'] / $service_tax_rate) * Model::factory('exchange')->to($booking_exist['data']['hotel_currency']->id, 1);
                // //$tax = $AmountAfterTax - $AmountBeforeTax;
                // Total Amount all
                $AmountAfterTax_total_all = 0;
                $AmountBeforeTax_total_all = 0;
                $AmountAfterTax_total_all += round($AmountAfterTax_total);
                $AmountBeforeTax_total_all += round($AmountBeforeTax_total);

                $Total = $RoomStay->appendChild($doc->createElement('Total'));
                $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total, 2));
                $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total, 2));
                $Total->setAttribute('CurrencyCode', 'USD');
                // $Taxes = $Rate->appendChild($doc->createElement('Taxes'));
                // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
                // $Tax->setAttribute('Amount', round($tax));
                // $Tax->setAttribute('CurrencyCode', 'USD');

                $BasicPropertyInfo = $RoomStay->appendChild($doc->createElement('BasicPropertyInfo'));
                $BasicPropertyInfo->setAttribute('HotelCode', $booking_exist['data']['hotel_id']);
                $BasicPropertyInfo->setAttribute('HotelName', $booking_exist['data']['hotel_name']);

                // $ResGuestRPHs = $RoomStay->appendChild($doc->createElement('ResGuestRPHs'));
                // $ResGuestRPH = $ResGuestRPHs->appendChild($doc->createElement('ResGuestRPH'));
                // $ResGuestRPH->setAttribute('RPH', 1);

                $Comments = $RoomStay->appendChild($doc->createElement('Comments'));
                $Comment = $Comments->appendChild($doc->createElement('Comment'));
                $Comment->setAttribute('GuestViewable', 'true');
                $Text = $Comment->appendChild($doc->createElement('Text', $booking_exist['request_note']));

                $booking_exist['note'] .= $booking_exist['request_note'] ? ', guest comment:' . $booking_exist['request_note'] : NULL;
                $booking_exist['note'] .= $booking_exist['is_early_check_in_request'] ? ', Early Check In' : NULL;
                $booking_exist['note'] .= $booking_exist['is_high_floor_request'] ? ', High flor' : NULL;
                $booking_exist['note'] .= $booking_exist['is_large_bed_request'] ? ', Large Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_twin_beds_request'] ? ', Twin Bed' : NULL;
                $booking_exist['note'] .= $booking_exist['is_airport_transfer_request'] ? ', Airport Transfer' : NULL;

                $SpecialRequests = $RoomStay->appendChild($doc->createElement('SpecialRequests'));

                if ($booking_exist['is_early_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'early check in request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'Early check in request'));
                }
                if ($booking_exist['is_high_floor_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'high floor');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'high floor request'));
                }
                if ($booking_exist['is_large_bed_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'large bed request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'large bed request'));
                }
                if ($booking_exist['is_twin_beds_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'twin beds');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'twin beds request'));
                }
                if ($booking_exist['is_airport_transfer_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'airport transfer');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'airport transfer request'));
                }
                if ($booking_exist['is_non_smoking_room_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'non smoking room request');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'non smoking room request'));
                }
                if ($booking_exist['is_late_check_in_request']) {
                    $SpecialRequest = $SpecialRequests->appendChild($doc->createElement('SpecialRequest'));
                    $SpecialRequest->setAttribute('Name', 'late check in');
                    $Text = $SpecialRequest->appendChild($doc->createElement('Text', 'late check in request'));
                }
            }

            $ResGuests = $HotelReservation->appendChild($doc->createElement('ResGuests'));
            $ResGuest = $ResGuests->appendChild($doc->createElement('ResGuest'));
            $ResGuest->setAttribute('ResGuestRPH', 1);
            $ResGuest->setAttribute('PrimaryIndicator', 1);
            //$ResGuest->setAttribute('ArrivalTime', 'true');
            $Profiles = $ResGuest->appendChild($doc->createElement('Profiles'));
            $ProfileInfo = $Profiles->appendChild($doc->createElement('ProfileInfo'));
            $Profile = $ProfileInfo->appendChild($doc->createElement('Profile'));
            $Profile->setAttribute('ProfileType', 1);
            $Customer = $Profile->appendChild($doc->createElement('Customer'));
            $PersonName = $Customer->appendChild($doc->createElement('PersonName'));
            $GivenName = $PersonName->appendChild($doc->createElement('GivenName', $booking_exist['first_name']));
            $Surname = $PersonName->appendChild($doc->createElement('Surname', $booking_exist['last_name']));
            //$Telephone = $Customer->appendChild($doc->createElement('Telephone'));
            $Email = $Customer->appendChild($doc->createElement('Email', $booking_exist['email']));
            //$Address = $Customer->appendChild($doc->createElement('Address'));

            $ResGlobalInfo = $HotelReservation->appendChild($doc->createElement('ResGlobalInfo'));


            // $TimeSpan = $ResGlobalInfo->appendChild($doc->createElement('TimeSpan'));
            // $TimeSpan->setAttribute('Start', $booking_exist['data']['check_in']);
            // $TimeSpan->setAttribute('End', $booking_exist['data']['check_out']);
            // $TimeSpan->setAttribute('Duration', 'P1D');

            $Total = $ResGlobalInfo->appendChild($doc->createElement('Total'));
            $Total->setAttribute('AmountAfterTax', round($AmountAfterTax_total_all, 2));
            $Total->setAttribute('AmountBeforeTax', round($AmountBeforeTax_total_all, 2));
            $Total->setAttribute('CurrencyCode', 'USD');
            $Taxes = $Total->appendChild($doc->createElement('Taxes'));
            // $Tax = $Taxes->appendChild($doc->createElement('Tax'));
            // $Tax->setAttribute('Amount', round($tax));
            // $Tax->setAttribute('CurrencyCode', 'USD');

            $HotelReservationIDs = $ResGlobalInfo->appendChild($doc->createElement('HotelReservationIDs'));
            $HotelReservationID = $HotelReservationIDs->appendChild($doc->createElement('HotelReservationID'));
            $HotelReservationID->setAttribute('ResID_Type', 14);
            $HotelReservationID->setAttribute('ResID_Value', $booking_exist['booking_id']);

            $doc->appendChild($envelope);

            $request = $doc->saveXML();

            CustomLog::factory()->add(7, 'DEBUG', $request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, Kohana::config('api.siteminder.url_connect'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Content-length: " . strlen($request)));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $response = curl_exec($ch);
            // Close the handle
            curl_close($ch);

            /*
             * XML process
             */
            try {
                $xml = new SimpleXMLElement($response);
                $body = $xml->children('SOAP-ENV', true)->Body->children();

                $OTA = $body->OTA_HotelResNotifRS;



                // Check response Success/ Errors
                if (isset($OTA->Success)) {
                    DB::delete('api_reservation_queues')
                            ->where('api', '=', 2)
                            ->where('request_type', '=', 2)
                            ->where('booking_id', '=', $booking_exist['booking_id'])
                            ->execute();

                    CustomLog::factory()->add(7, 'DEBUG', 'Siteminder cancel reservation notification Success');
                    CustomLog::factory()->add(7, 'DEBUG', $response);
                } else {
                    $success = FALSE;
                }
            } catch (Exception $e) {
                Database::instance()->rollback();

                $booking_exist['retries'] = $booking_exist['retries'] + 1;

                DB::update('api_reservation_queues')
                        ->set(
                                array('retries' => $booking_exist['retries'])
                        )
                        ->where('booking_id', '=', $booking_exist['booking_id'])
                        ->where('request_type', '=', $booking_exist['request_type'])
                        ->where('api', '=', $booking_exist['api'])
                        ->execute();

                $success = FALSE;

                CustomLog::factory()->add(7, 'DEBUG', 'Siteminder cancel reservation notification Failed');
                CustomLog::factory()->add(7, 'DEBUG', $response);
            }
        } catch (Kohana_Exception $e) {
            $booking_exist['retries'] = $booking_exist['retries'] + 1;

            DB::update('api_reservation_queues')
                    ->set(
                            array('retries' => $booking_exist['retries'])
                    )
                    ->where('booking_id', '=', $booking_exist['booking_id'])
                    ->where('request_type', '=', $booking_exist['request_type'])
                    ->where('api', '=', $booking_exist['api'])
                    ->execute();

            CustomLog::factory()->add(7, 'DEBUG', 'Siteminder cancel reservation notification failed  Build response');

            $success = FALSE;
        }
        return $success;
    }

}
