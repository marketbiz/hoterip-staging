<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Siteminder_Static extends Controller {

  public static function errors($errors, $node_head)
  {
    // Logout account
    A1::instance()->logout();

    $list_error_type = array(
      '1' =>'Unknown',
      '2' =>'No implementation',
      '3' =>'Biz rule',
      '4' =>'Authentication',
      '5' =>'Authentication timeout',
      '6' =>'Authorization',
      '7' =>'Protocol violation',
      '8' =>'Transaction model',
      '9' =>'Authentication model',
      '10' =>'Required field missing',
      '12' =>'Processing exception',

      '00' =>'Welcome',
      );
    
    $list_error_code = array(
      '15' =>'Invalid Date',
      '61' =>'Invalid currency code',
      '69' =>'Minimum stay criteria not fulfilled',
      '70' =>'Maximum stay criteria not fulfilled',
      '87' =>'Booking reference invalid',
      '95' =>'Booking already cancelled',
      '97' =>'Booking reference not found',
      '111' =>'Booking invalid',
      '127' =>'Reservation already exists',
      '146' =>'Service requested incorrect',
      '147' =>'Taxes incorrect',
      '161' =>'Search criteria invalid',
      '187' =>'System currently unavailable',
      '245' =>'Invalid confirmation number',
      '249' =>'Invalid rate code',
      '251' =>'Last name and customer number do not match',
      '321' =>'Required field missing',
      '364' =>'Error rate range',
      '375' =>'Hotel not active',
      '392' =>'Invalid hotel code',
      '394' =>'Invalid item',
      '397' =>'Invalid number of adults',
      '400' =>'Invalid property code',
      '402' =>'Invalid room type',
      '436' =>'Rate does not exist',
      '448' =>' System error',
      '450' =>'Unable to process',
      '459' =>'Invalid request code',
      '783' =>'Room or rate not found',
      '842' =>'Rate not loaded',

      '00' =>'Welcome',
      '9900' =>'Invalid Username or Password',
      '9901' =>'Base By Guest Amounts not supported',
      );

    //Generate Error XML
    $doc = new DOMDocument('1.0', 'UTF-8');
    $doc->formatOutput = TRUE;

    $envelope = $doc->appendChild($doc->createElement('soap-env:Envelope'));
    $envelope->setAttribute('xmlns:soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
    $head = $envelope->appendChild($doc->createElement('soap-env:Header'));
    $body = $envelope->appendChild($doc->createElement('soap-env:Body'));
    $body->setAttribute('xmlns:soap', 'http://schemas.xmlsoap.org/soap/envelope/');

    $RS = $body->appendChild($doc->createElement(substr($node_head['head'], 0, -2).'RS'));
    $RS->setAttribute('xmlns', $node_head['xmlns']);
    $RS->setAttribute('TimeStamp', $node_head['TimeStamp']);
    $RS->setAttribute('EchoToken', $node_head['EchoToken']);
    $RS->setAttribute('Version', $node_head['Version']);

    $errors_node = $RS->appendChild($doc->createElement('Errors'));

    foreach ($errors as $key =>$error)
    {
      $error_descriptions = str_replace("_", " ", $key).', '.$list_error_code[$error['code']].'.';

      $error_node = $errors_node->appendChild($doc->createElement('Error',$error_descriptions));
      $error_node->setAttribute('Type', $error['type']);
      $error_node->setAttribute('Code', $error['code']);
    }

    $response = $doc->saveXML();

    // error log
    CustomLog::factory()->add(7,'DEBUG', 'Siteminder output Failed');
    
    return $response;
  }

  public static function selected_currency($id)
  {
    // Set default currency
    $selected_currency = ORM::factory('currency')->get_currency_by_id($id);

    return $selected_currency;
  }
  
  public static function promotion_descriptions($hotel)
  {
    //Determine Promo
    $stay = '';

    if($hotel AND $hotel['campaign_default'] == 0)
    {
      if($hotel['rate'] > 0)
      {
        $stay .= 'Discount '.$hotel['rate'].' %, ';
      }
      elseif($hotel['amount'] > 0)
      {
        $stay .= 'Discount '.$hotel['amount'].' '.self::selected_currency($hotel['currency_code'])->code.', ';
      }
      elseif($hotel['free_night'] > 0)
      {
        $stay .= 'Free '.$hotel['free_night'].' Night, ';
      }

      if($hotel['last_days'] > 0)
      {
        $hotel['last_days_date'] = Date::formatted_time("+{$hotel['last_days']} days", __('M j, Y'), $hotel['hotel_timezone']);
        $last_days =$hotel['last_days_date'];
        $last_days = Date::formatted_time("$last_days", 'm-d-Y', $hotel['hotel_timezone']);

        if ($hotel['check_in'] <= $last_days AND $hotel['check_out'] <= $last_days) {
          $Onrequest = 'FALSE';
        }
        elseif ($hotel['check_in']  < $last_days AND $hotel['check_out'] >= $last_days) {
          $Onrequest = 'FALSE';
        }
        else
        {
          $Onrequest = 'TRUE';
        }

        $stay .= 'Book before '.Date::formatted_time("+{$hotel['last_days']} days", __('M j, Y'), $hotel['hotel_timezone']).', ';
      }

      elseif($hotel['early_days'] > 0)
      {
        $early_days = $hotel['early_days'] - 1;
        $hotel['early_days_date'] = Date::formatted_time("+$early_days days", __('M j, Y'), $hotel['hotel_timezone']);

        $early_days =$hotel['early_days_date'];
        $early_days = Date::formatted_time("$early_days", 'm-d-Y', $hotel['hotel_timezone']);

        if($hotel['check_in'] <= $early_days AND $hotel['check_out'] <= $early_days)
        {
          $Onrequest = 'TRUE';
        }
        elseif ($hotel['check_in'] < $early_days AND $hotel['check_out'] >= $early_days) {
           $Onrequest = 'FALSE';
        }
        elseif ($hotel['check_in'] > $early_days AND $hotel['check_out'] >= $early_days) {
           $Onrequest = 'FALSE';
        }

        $stay .= 'Book after '.Date::formatted_time("+$early_days days", __('M j, Y'), $hotel['hotel_timezone']).', ';
      }

      $stay .= 'Stay at least '.$hotel['min_nights'].' nights and book '.$hotel['min_rooms'].' or more rooms';
    }

    if($hotel['campaign_default'])
    {
      $stay ='Default Promotions';
    }

    $promotion_exist = $stay.'.';

    $promotion_exist = htmlspecialchars(utf8_encode($promotion_exist), ENT_QUOTES);

    return $promotion_exist;
  }
}