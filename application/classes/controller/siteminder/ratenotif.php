<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Siteminder_Ratenotif extends Controller {
    /*
     * List USED Model
     * Hotel :get_campaign
     * Campaign :get_items
     * Room capacity :getroom, checkroom, extrabed
     * Country/City  : getbysegment
     * Timezone  : timezone identifier
     * Setting  : get
     * Exchane  : To
     */

    public function checker($transaction, $head, $body, $xmlns) {
        $data = array();
        $error = array();

        $OTA_HotelAvailRQ = $body->$transaction->attributes();
        $node_head = array(
            'head' => $transaction,
            'xmlns' => $xmlns,
            'TimeStamp' => (string) $OTA_HotelAvailRQ->TimeStamp,
            'Version' => (string) $OTA_HotelAvailRQ->Version,
            'EchoToken' => (string) $body->$transaction->attributes()->EchoToken,
        );

        $auth = array(
            'User' => (string) $head->Security->UsernameToken->Username,
            'Pwd' => (string) $head->Security->UsernameToken->Password,
        );

        // Check node head null
        if (empty($node_head['xmlns']) || empty($node_head['TimeStamp']) || empty($node_head['Version']) || empty($node_head['EchoToken'])) {
            $error['Empty_Property']['type'] = '10';
            $error['Empty_Property']['code'] = '321';
        }
        // Check Auth null request
        elseif (empty($auth['User']) || empty($auth['Pwd'])) {
            $error['Authentification']['type'] = '4';
            $error['Authentification']['code'] = '321';
        } else {
            // Check Auth
            if (Controller_Vacation_Static::auth($auth) == FALSE) {
                $error['Authentification']['type'] = '4';
                $error['Authentification']['code'] = '9900';
            } else {
                $RateAmountMessages = $body->$transaction->RateAmountMessages->RateAmountMessage;

                $data = array(
                    'hotel_id' => (string) $body->$transaction->RateAmountMessages->attributes()->HotelCode,
                );

                // data quque
                $i = 0;
                $j = 0;
                $k = 0;

                foreach ($RateAmountMessages as $RateAmountMessage_list) {
                    $data['rates'][$i] = array(
                        'Start' => (string) $RateAmountMessage_list->StatusApplicationControl->attributes()->Start,
                        'End' => (string) $RateAmountMessage_list->StatusApplicationControl->attributes()->End,
                        'InvTypeCode' => (string) $RateAmountMessage_list->StatusApplicationControl->attributes()->InvTypeCode,
                        'RatePlanCode' => (string) $RateAmountMessage_list->StatusApplicationControl->attributes()->RatePlanCode
                    );

                    // Check Null request
                    if (empty($data['rates'][$i]['Start']) || empty($data['rates'][$i]['End']) || empty($data['rates'][$i]['InvTypeCode']) || empty($data['rates'][$i]['RatePlanCode'])) {
                        $error['rates']['type'] = '10';
                        $error['rates']['code'] = '321';
                        break;
                    }

                    // Check Format request
                    if (!is_numeric($data['rates'][$i]['InvTypeCode'])
                    ) {
                        $error['InvTypeCode']['type'] = '3';
                        $error['InvTypeCode']['code'] = '402';
                        break;
                    }

                    if (!is_numeric($data['rates'][$i]['RatePlanCode'])
                    ) {
                        $error['InvTypeCode']['type'] = '3';
                        $error['RatePlanCode']['code'] = '249';
                        break;
                    }

                    // Check start and end date format
                    if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['rates'][$i]['Start']) || !preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $data['rates'][$i]['End'])
                    ) {
                        $error['Start_or_End']['type'] = '3';
                        $error['Start_or_End']['code'] = '400';
                        break;
                    }

                    $Rates = $body->$transaction->RateAmountMessages->RateAmountMessage->Rates->Rate;

                    if (!empty($Rates)) {
                        foreach ($Rates as $Rate_count => $Rate) {
                            $BaseByGuestAmts = $Rates->BaseByGuestAmts->BaseByGuestAmt;

                            foreach ($BaseByGuestAmts as $BaseByGuestAmt_count => $BaseByGuestAmt_list) {

                                $data['rates'][$i]['BaseByGuestAmts'][$j] = array(
                                    'AgeQualifyingCode' => (string) $BaseByGuestAmt_list->attributes()->AgeQualifyingCode,
                                    'NumberOfGuests' => (string) $BaseByGuestAmt_list->attributes()->NumberOfGuests,
                                    'AmountAfterTax' => (string) $BaseByGuestAmt_list->attributes()->AmountAfterTax,
                                    'CurrencyCode' => (string) $BaseByGuestAmt_list->attributes()->CurrencyCode,
                                );

                                if (!empty($data['rates'][$i]['BaseByGuestAmts'][$j]['NumberOfGuests'])
                                ) {
                                    $error['BaseByGuestAmts']['type'] = '2';
                                    $error['BaseByGuestAmts']['code'] = '9901';
                                    break;
                                }

                                if (!is_numeric($data['rates'][$i]['BaseByGuestAmts'][$j]['AgeQualifyingCode']) || !is_numeric($data['rates'][$i]['BaseByGuestAmts'][$j]['AmountAfterTax'])
                                ) {
                                    $error['BaseByGuestAmts']['type'] = '3';
                                    $error['BaseByGuestAmts']['code'] = '459';
                                    break;
                                }
                                if (!empty($data['rates'][$i]['BaseByGuestAmts'][$j]['CurrencyCode']) && ($data['rates'][$i]['BaseByGuestAmts'][$j]['CurrencyCode'] != 'USD')
                                ) {
                                    $error['BaseByGuestAmts']['type'] = '3';
                                    $error['BaseByGuestAmts']['code'] = '400';
                                    break;
                                }
                            }

                            $AdditionalGuestAmounts = $Rates->AdditionalGuestAmounts->AdditionalGuestAmount;

                            if (!empty($AdditionalGuestAmounts)) {
                                foreach ($AdditionalGuestAmounts as $AdditionalGuestAmounts_count => $AdditionalGuestAmounts_list) {

                                    $data['rates'][$i]['AdditionalGuestAmounts'][$k] = array(
                                        'AgeQualifyingCode' => (string) $AdditionalGuestAmounts_list->attributes()->AgeQualifyingCode,
                                        'Amount' => (string) $AdditionalGuestAmounts_list->attributes()->Amount,
                                    );

                                    if (empty($data['rates'][$i]['AdditionalGuestAmounts'][$k]['AgeQualifyingCode']) || empty($data['rates'][$i]['AdditionalGuestAmounts'][$k]['Amount'])
                                    ) {
                                        $error['AdditionalGuestAmounts']['type'] = '10';
                                        $error['AdditionalGuestAmounts']['code'] = '321';
                                        break;
                                    }

                                    if (!is_numeric($data['rates'][$i]['AdditionalGuestAmounts'][$k]['AgeQualifyingCode']) || !is_numeric($data['rates'][$i]['AdditionalGuestAmounts'][$k]['Amount'])
                                    ) {
                                        $error['AdditionalGuestAmounts']['type'] = '3';
                                        $error['AdditionalGuestAmounts']['code'] = '459';
                                        break;
                                    }
                                    $k++;
                                }
                            }
                            $j++;
                        }
                    }
                    $i++;
                }
            }
        }

        $data = array_merge($data, $node_head);

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Siteminder_Static::errors($error, $node_head);
            $this->output($errors);
        } else {
            $this->process($data);
        }
    }

    public function process($data) {
        try {
            foreach ($data['rates'] as $key => $single_data) {
                $hotel_id = $data['hotel_id'];
                $room_id = $single_data['InvTypeCode'];
                $campaign_id = $single_data['RatePlanCode'];

                // Factory exchange
                $exchange = Model::factory('exchange');

                // Create dates array
                $start_date_timestamp = strtotime($single_data['Start']);
                $end_date_timestamp = strtotime($single_data['End']);

                $dates = array();
                for ($i = $start_date_timestamp; $i <= $end_date_timestamp; $i += Date::DAY) {
                    $dates[date('Y-m-d', $i)] = date('Y-m-d', $i);
                }

                // Load Currency Hotel
                $currency = DB::select('currency_id')->from('hotels')
                        ->where('id', '=', $hotel_id)
                        ->as_object()
                        ->execute()
                        ->current();

                $currency_id = $currency ? $currency->currency_id : 1;
                $currency_req = $single_data['RateAmounts']['Currency'] ? $single_data['RateAmounts']['Currency'] : 'USD';

                $currency_request = ORM::factory('currency')
                                ->where('code', '=', $currency_req)
                                ->find()->as_array();

                // Changed items 
                $changed_prev_items = array();
                $changed_items = array();

                // Dates property process
                foreach ($dates as $date) {
                    /*
                     * Room
                     */

                    // if(!empty($single_data['AdditionalGuestAmounts']))
                    // {
                    //   // Prepare the values
                    //   $values = array(
                    //     'hotel_id' => $data['hotel_id'],
                    //     'date' => $date,
                    //     'surcharge_description_id' => 1,
                    //   );
                    //   foreach ($single_data['AdditionalGuestAmounts'] as $key_AdditionalGuestAmounts => $AdditionalGuestAmounts) 
                    //   {
                    //     if($AdditionalGuestAmounts['AgeQualifyingCode'] == 10)
                    //     {
                    //       $values['adult_price'] = $AdditionalGuestAmounts['Amount']  * ($exchange->to($currency_request['id'], $currency_id));
                    //     }
                    //     if($AdditionalGuestAmounts['AgeQualifyingCode'] == 8)
                    //     {
                    //       $values['child_price'] = $AdditionalGuestAmounts['Amount'] * ($exchange->to($currency_request['id'], $currency_id));
                    //     }
                    //   }
                    //   // Find surcharge
                    //   $surcharge = ORM::factory('surcharge')
                    //     ->select(array('surcharge_descriptions.description', 'description'))
                    //     ->join('surcharge_descriptions')->on('surcharge_descriptions.id', '=', 'surcharges.surcharge_description_id')
                    //     ->where('hotel_id', '=', $data['hotel_id'])
                    //     ->where('date', '=', $date)
                    //     ->find();
                    //   // Copy surcharge to write log
                    //   $prev_surcharge = clone $surcharge;
                    //   // Save surcharge
                    //   $surcharge
                    //     ->values($values)
                    //     ->save();
                    //   // Reload surcharge because we need to select description
                    //   $surcharge = ORM::factory('surcharge')
                    //     ->select(array('surcharge_descriptions.description', 'description'))
                    //     ->join('surcharge_descriptions')->on('surcharge_descriptions.id', '=', 'surcharges.surcharge_description_id')
                    //     ->where('hotel_id', '=', $data['hotel_id'])
                    //     ->where('date', '=', $date)
                    //     ->find();
                    //   // Write log
                    //   ORM::factory('log')
                    //     ->values(array(
                    //       'admin_id' => A1::instance()->get_user()->id,
                    //       'hotel_id' => $this->selected_hotel->id,
                    //       'action' => 'Changed Surcharge',
                    //       'room_texts' => NULL,
                    //       'text' => 'Date: :date. '
                    //         .'Adult Price: :prev_adult_price -> :adult_price. '
                    //         .'Child Price: :prev_child_price -> :child_price. '
                    //         .'Description: :prev_description -> :description.',
                    //       'data' => serialize(array(
                    //         ':prev_adult_price' => $prev_surcharge->adult_price ? $prev_surcharge->adult_price : 'None',
                    //         ':prev_child_price' => $prev_surcharge->child_price ? $prev_surcharge->child_price : 'None',
                    //         ':prev_description' => isset($prev_surcharge->description) ? $prev_surcharge->description : 'None',
                    //         ':date' => date('M j, Y', strtotime($surcharge->date)),
                    //         ':adult_price' => $surcharge->adult_price,
                    //         ':child_price' => $surcharge->child_price,
                    //         ':description' => $surcharge->description,
                    //       )),
                    //       'created' => time(),
                    //     ))
                    //     ->create();
                    //   // If there is QA admins
                    //   if (count($qa_emails) > 0)
                    //   {
                    //     // Send email
                    //     Email::factory(strtr(':hotel_name - Changed Surcharge', array(
                    //         ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $this->selected_hotel->id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                    //       )), strtr(
                    //       "Action: Changed Surcharge.\n"
                    //       ."By: :admin_username (:admin_name).\n"
                    //       ."Date: :date.\n"
                    //       ."Adult Price: :prev_adult_price -> :adult_price.\n"
                    //       ."Child Price: :prev_child_price -> :child_price.\n"
                    //       ."Description: :prev_description -> :description.",
                    //       array(
                    //         ':admin_username' => A1::instance()->get_user()->username,
                    //         ':admin_name' => A1::instance()->get_user()->name,
                    //         ':date' => date('M j, Y', strtotime($surcharge->date)),
                    //         ':prev_adult_price' => $prev_surcharge->adult_price ? $prev_surcharge->adult_price : 'None',
                    //         ':prev_child_price' => $prev_surcharge->child_price ? $prev_surcharge->child_price : 'None',
                    //         ':prev_description' => isset($prev_surcharge->description) ? $prev_surcharge->description : 'None',
                    //         ':adult_price' => $surcharge->adult_price,
                    //         ':child_price' => $surcharge->child_price,
                    //         ':description' => $surcharge->description,
                    //       )))
                    //       ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                    //       ->to($qa_emails)
                    //       ;//->send();
                    //   }
                    // }

                    foreach ($single_data['BaseByGuestAmts'] as $key_BaseByGuestAmts => $BaseByGuestAmt) {
                        /*
                         * Update Item
                         */
                        $item_price = round($BaseByGuestAmt['AmountAfterTax'] * ($exchange->to($currency_request['id'], $currency_id)));

                        $values = array(
                            'room_id' => $room_id,
                            'campaign_id' => $campaign_id,
                            'date' => $date,
                            'price' => $item_price ? $item_price : (string) $item_default->price,
                            'minimum_night' => !empty($single_data['MinMaxMessageType']) && ($single_data['MinMaxMessageType'] == 'SetMinLOS') ? $single_data['Time'] : 1,
                            'stock' => !empty($single_data['BookingLimit']) ? (string) $item_default->stock : (string) $single_data['BookingLimit'],
                            'is_blackout' => !empty($single_data['Status']) ? (string) $item_default->is_blackout : (string) $single_data['Status'],
                            'room_id' => $room_id,
                            'date' => $date,
                            'price' => $item_price,
                        );

                        // Get the item
                        $item = DB::select()
                                ->from('items');

                        if (!$campaign->is_default) {
                            $item = $item->where('campaign_id', '=', $campaign_id);
                        } else {
                            $item = $item->where('campaign_id', '=', 0);
                        }

                        $item = $item->where('date', '=', $date)
                                ->as_object()
                                ->execute()
                                ->current();

                        // If item already exist
                        if ($item) {
                            $values = array(
                                'room_id' => $room_id,
                                'campaign_id' => $campaign_id,
                                'date' => $date,
                                'price' => $item_price ? $item_price : (string) $item->price,
                                'minimum_night' => !empty($single_data['MinMaxMessageType']) && ($single_data['MinMaxMessageType'] == 'SetMinLOS') ? $single_data['Time'] : 1,
                                'stock' => !empty($single_data['BookingLimit']) ? (string) $single_data['BookingLimit'] : (string) $item->stock,
                                'is_blackout' => !empty($single_data['Status']) ? (string) $single_data['Status'] : (string) $item->is_blackout,
                            );

                            // Clone item to write log
                            $prev_item = clone $item;

                            // Update
                            DB::update('items')
                                    ->set($values)
                                    ->where('room_id', '=', $room_id)
                                    ->where('campaign_id', '=', $campaign_id)
                                    ->where('date', '=', $date)
                                    ->execute();
                        } else {
                            // Get item default
                            $item_default = DB::select()
                                    ->from('items')
                                    ->where('room_id', '=', $room_id)
                                    ->where('campaign_id', '=', 0)
                                    ->where('date', '=', $date)
                                    ->as_object()
                                    ->execute()
                                    ->current();

                            $values = array(
                                'room_id' => $room_id,
                                'campaign_id' => $campaign_id,
                                'date' => $date,
                                'price' => $item_price ? $item_price : (string) $item_default->price,
                                'minimum_night' => !empty($single_data['MinMaxMessageType']) && ($single_data['MinMaxMessageType'] == 'SetMinLOS') ? $single_data['Time'] : 1,
                                'stock' => !empty($single_data['BookingLimit']) ? (string) $single_data['BookingLimit'] : (string) $item_default->stock,
                                'is_blackout' => !empty($single_data['Status']) ? (string) $single_data['Status'] : (string) $item_default->is_blackout,
                            );

                            // Build prev item class
                            $prev_item = new stdClass;
                            $prev_item->stock = NULL;
                            $prev_item->price = NULL;
                            $prev_item->net_price = NULL;
                            $prev_item->minimum_night = NULL;
                            $prev_item->is_blackout = NULL;
                            $prev_item->is_campaign_blackout = NULL;

                            // Create new
                            DB::insert('items')
                                    ->columns(array_keys($values))
                                    ->values(array_values($values))
                                    ->execute();
                        }
                    }
                }

                if (!empty($single_data['AdditionalGuestAmounts'])) {
                    foreach ($single_data['AdditionalGuestAmounts'] as $key_AdditionalGuestAmount => $AdditionalGuestAmount) {
                        if ($AdditionalGuestAmount['AgeQualifyingCode'] == 10) {
                            $room_update['extrabed_price'] = round($AdditionalGuestAmount['Amount'] * ($exchange->to($currency_request['id'], $currency_id)), 0);

                            DB::update('rooms')
                                    ->set($room_update)
                                    ->where('id', '=', $room_id)
                                    ->execute();
                        }
                    }
                }

                // if(empty($single_data['BaseByGuestAmts']))
                // {
                //   /*
                //    *  Email for item changed
                //    */
                //   // Get QA admins emails
                //   $qa_emails = ORM::factory('admin')
                //     ->join('admins_hotels')->on('admins_hotels.admin_id', '=', 'admins.id')
                //     ->where('admins.role', '=', 'quality-assurance')
                //     ->where('admins_hotels.hotel_id', '=', $hotel_id)
                //     ->find_all()
                //     ->as_array('email', 'name');
                //   // If there is QA admins
                //   if (count($qa_emails) > 0)
                //   {
                //     ksort($changed_prev_items);
                //     ksort($changed_items);
                //     // Create email message
                //     $message = Kostache::factory('item/qa/manage')
                //       ->set('admin_username', A1::instance()->get_user()->username)
                //       ->set('admin_name', A1::instance()->get_user()->name)
                //       ->set('room_name', ORM::factory('room_text')->where('room_texts.room_id', '=', $room_id)->where('room_texts.language_id', '=', 1)->find()->name)
                //       ->set('changed_prev_items', $changed_prev_items)
                //       ->set('changed_items', $changed_items)
                //       ->render();
                //     // Send email
                //     Email::factory(strtr(':hotel_name - Changed Item', array(
                //         ':hotel_name' => ORM::factory('hotel_text')->where('hotel_texts.hotel_id', '=', $hotel_id)->where('hotel_texts.language_id', '=', 1)->find()->name,
                //       )), $message, 'text/html')
                //       ->from(Kohana::config('application.email'), Kohana::config('application.name'))
                //       ->to($qa_emails)
                //       ;//->send();
                //   }
                //    // Commit transaction
                //   Database::instance()->commit();
                // }
            }
        } catch (Kohana_Exception $e) {
            // Rollback transaction
            Database::instance()->rollback();
            // Add error notice
            $error['Exception']['type'] = '12';
            $error['Exception']['code'] = '450';
        }

        // Check error exist
        if (!empty($error)) {
            $errors = Controller_Siteminder_Static::errors($error, $data);
            $this->output($errors);
        } else {
            // Build response
            return $this->xml_build($data);
        }
    }

    public function xml_build($data) {
        try {
            $doc = new DOMDocument('1.0', 'UTF-8');
            $doc->formatOutput = TRUE;

            $envelope = $doc->appendChild($doc->createElement('SOAP-ENV:Envelope'));
            $envelope->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');
            $head = $envelope->appendChild($doc->createElement('SOAP-ENV:Header'));
            $body = $envelope->appendChild($doc->createElement('SOAP-ENV:Body'));
            $body->setAttribute('xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/');

            $response_node = $body->appendChild($doc->createElement(substr($data['head'], 0, -2) . 'RS'));
            $response_node->setAttribute('xmlns', 'http://www.opentravel.org/OTA/2003/05');
            $response_node->setAttribute('TimeStamp', $data['TimeStamp']);
            $response_node->setAttribute('EchoToken', $data['EchoToken']);
            $response_node->setAttribute('Version', $data['Version']);

            $success = $response_node->appendChild($doc->createElement('Success'));

            $response = $doc->saveXML();

            $this->output($response);
        } catch (Kohana_Exception $e) {
            $error['XML_build']['type'] = '2';
            $error['XML_build']['code'] = '450';

            $errors = Controller_Siteminder_Static::errors($error, $data);
            $this->output($errors);
        }
    }

    public function output($response) {
        Log::instance()->add(Log::INFO, 'Hoterip response for siteminder');
        Log::instance()->add(Log::INFO, $response);
        // Logout account
        A1::instance()->logout();

        $this->response
                ->headers('Content-Type', 'text/xml')
                ->body($response);
    }

    public function action_request() {
        // Set time limit
        set_time_limit(60);

        $xml_location = file_get_contents('php://input');
        // $xml_location = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'media/vacation.xml';
        // $data = file_get_contents($xml_location);
        // $xml_location = trim( stripslashes( $data ));
        echo "<pre>";
        print_r($xml_location);
        die();

        if (!$xml_location) {
            $node_head = array(
                'head' => 'ResponseRQ',
                'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                'TimeStamp' => date('c'),
                'EchoToken' => '000',
                'Target' => 'Production',
                'Version' => '1.00',
                'PrimaryLangID' => 'En',
            );

            $error['Welcome']['type'] = '2';
            $error['Welcome']['code'] = '450';

            $errors = Controller_Siteminder_Static::errors($error, $node_head);

            $this->output($errors);
        } else {
            $xml = new SimpleXMLElement($xml_location);

            $namespace = $xml->getDocNamespaces(TRUE);
            foreach ($namespace as $html_element => $value) {
                $xmlns = $value;
            }

            $head = $xml->children('SOAP-ENV', true)->Header->children('wsse', true);
            $body = $xml->children('SOAP-ENV', true)->Body->children();

            $transaction = $body->getName();

            if ($transaction === 'OTA_HotelRateAmountNotifRQ') {
                $data = $this->checker($transaction, $head, $body, $xmlns);
            } else {
                $node_head = array(
                    'head' => 'ResponseRQ',
                    'xmlns' => 'http://www.opentravel.org/OTA/2003/05',
                    'TimeStamp' => date('c'),
                    'EchoToken' => '000',
                    'Target' => 'Production',
                    'Version' => '1.00',
                    'PrimaryLangID' => 'En',
                );

                $error['False_request']['type'] = '2';
                $error['False_request']['code'] = '450';
                $errors = Controller_Siteminder_Static::errors($error, $node_head);

                $this->output($errors);
            }
        }
    }

}
