<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Review extends Controller_Layout_Admin {
	
	public function before()
	{
		if ($this->request->action() == 'delete')
		{
			$this->auto_render = FALSE;
		}
		
		parent::before();
	}
	
	public function action_manage() 
	{
    // Is Authorized ?
		if ( ! A2::instance()->allowed('review', 'manage'))
		{
			// Add error notice
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			// Redirect to home dashboard
			$this->request->redirect(Route::get('default')->uri(array('controller' => 'home', 'action' => 'dashboard')));
		}
    
		if ($this->request->post('delete_checked'))
		{
			if (A2::instance()->allowed('review', 'delete'))
			{
				try
				{
					// Get reviews id
					$review_ids = $this->request->post('ids');

					// Delete reviews
					DB::delete('reviews')
						->where('id', 'IN', $review_ids)
						->execute();

					// Add success notice
					Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
				}
				catch (Exception $e)
				{
					// Add error notice
					Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
				}
			}
			else
			{
				Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
			}
		}
		
		if ($this->request->post('approve'))
		{
			$review_ids = $this->request->post('ids');
			
			if ($review_ids)
			{
				DB::update('reviews')
					->set(array(
						'is_approved' => 1,
					))
					->where('id', 'IN', $review_ids)
					->execute();
				Request::current()->redirect('review/manage');
			}
		}
		
		if ($this->request->post('disapprove'))
		{
			$review_ids = $this->request->post('ids');
      
			if ($review_ids)
			{
				DB::update('reviews')
					->set(array(
						'is_approved' => 0,
					))
					->where('id', 'IN', $review_ids)
					->execute();
				Request::current()->redirect('review/manage');
			}
		}
		
		$page = Arr::get($this->request->query(), 'page', 1);
		$items_per_page = 30;
		$offset = ($page - 1) * $items_per_page;
		
		$reviews = ORM::factory('review')
			->select(
				array('hotel_texts.name', 'hotel_name'),
				array('languages.name', 'language_name')
			)
			->join('hotels')->on('hotels.id', '=', 'reviews.hotel_id')
			->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
			->join('languages', 'left')->on('languages.id', '=', 'reviews.language_id')
			->where('hotel_texts.language_id', '=', $this->selected_language->id)
			->order_by('created', 'DESC');
		
		$total_reviews = $reviews
      ->reset(FALSE)
			->count_all();
    
    $reviews = $reviews
      ->offset($offset)
			->limit($items_per_page)
      ->find_all();
		
		// Create pagination
		$pagination = Pagination::factory(array(
			'items_per_page' => $items_per_page,
			'total_items' => $total_reviews,
		));
		
		$this->template->main = Kostache::factory('review/manage')
			->set('notice', Notice::render())
			->set('reviews', $reviews)
			->set('pagination', $pagination);
	}
	
	public function action_delete()
	{
		// Get review id
		$review_id = $this->request->param('id');
		
		// Get review
		$review = ORM::factory('review')
			->where('id', '=', $review_id)
			->find();
		
		if (A2::instance()->allowed('review', 'delete'))
		{
			try
			{
				// Delete review
				$review->delete();

				// Add success notice
				Notice::add(Notice::SUCCESS, Kohana::message('general', 'delete_success'));
			}
			catch (Exception $e)
			{
				// Add error notice
				Notice::add(Notice::ERROR, Kohana::message('general', 'delete_failed'), array(':message' => $e->getMessage()));
			}
		}
		else
		{
			Notice::add(Notice::ERROR, Kohana::message('general', 'not_authorized'));
		}
		
		// Redirect back
		$this->request->redirect($this->request->referrer());
	}
	
}