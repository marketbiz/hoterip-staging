<?php defined('SYSPATH') or die('No direct script access.');

class File {
	
  /**
   * Remove directory with all files in it
   * @param string directory to delete
   */
	public static function remove_directory($dir) 
	{
		if (is_dir($dir))
		{
			$objects = scandir($dir);
			foreach ($objects as $object)
			{ 
				if ($object != "." && $object != "..")
				{
					if (filetype($dir."/".$object) == "dir")
					{
						rrmdir($dir."/".$object);
					}
					else 
					{
						unlink($dir."/".$object);
					}
       }
     }
     reset($objects);
     rmdir($dir);
   } 
 } 
	
}