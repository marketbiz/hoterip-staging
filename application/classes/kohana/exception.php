<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Exception extends Kohana_Kohana_Exception {
  
  public static function handler(Exception $e)
  {
    // If in development environment
    if (Kohana::$environment === Kohana::DEVELOPMENT)
    {
      // let the parent handle the exception
      parent::handler($e);
    }
    else
    {
      try
      {
        // Log the exception
        Kohana::$log->add(Log::ERROR, parent::text($e));
 
        $attributes = array
        (
          'action'  => 500,
          'message' => rawurlencode($e->getMessage())
        );

        // If the exception is HTTP exception
        if ($e instanceof HTTP_Exception)
        {
          // Overwrite action from HTTP exception code
          $attributes['action'] = $e->getCode();
        }
        
        // Do sub request
        echo Request::factory(Route::get('error')->uri($attributes))
          ->execute()
          ->send_headers()
          ->body();
      }
      catch (Exception $e)
      {
        // Clean the output buffer if one exists
        ob_get_level() and ob_clean();

        // Display the exception text
        echo parent::text($e);

        // Exit with an error status
        exit(1);
      }
    }
  }
}