<?php defined('SYSPATH') or die('No direct script access.');

class Model_Benefit extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'benefit';
	}
	
	public function create_benefit($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('benefit')
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Create benefit
			$this->values($values, array(
					'url_segment',
					'additional_price',
					'currency',
				))
				->create();
			
			// Create benefit texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$benefit_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$benefit_texts[] = ORM::factory('benefit_text')
					->set('benefit_id', $this->id)
					->values(array(
						'language_id' => $language->id,
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->create();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
	public function update_benefit($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('benefit')
					->where('id', '<>', $this->id)
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Update benefit
			$this->values($values, array(
					'url_segment',
					'additional_price',
					'currency',
				))
				->update();
			
			// Update benefit texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$benefit_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$benefit_texts[] = ORM::factory('benefit_text')
					->where('benefit_id', '=', $this->id)
					->where('language_id', '=', $language->id)
					->find()
					->values(array(
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->update();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
}