<?php defined('SYSPATH') or die('No direct script access.');

class Model_Country extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'country';
	}
	
	public function create_country($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('country')
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Create country
			$this->values($values, array(
					'url_segment',
				))
				->create();
			
			// Create country texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$country_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$country_texts[] = ORM::factory('country_text')
					->set('country_id', $this->id)
					->values(array(
						'language_id' => $language->id,
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->create();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
	public function update_country($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('country')
					->where('id', '<>', $this->id)
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Update country
			$this->values($values, array(
					'url_segment',
				))
				->update();
			
			// Update country texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$country_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$country_texts[] = ORM::factory('country_text')
					->where('country_id', '=', $this->id)
					->where('language_id', '=', $language->id)
					->find()
					->values(array(
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->update();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}

	public function get_by_segment($language_id, $segment)
	{
		return DB::select(
					'countries.*', 'country_texts.name'
				)
				->from('countries')
				->join('country_texts')->on('countries.id', '=', 'country_texts.country_id')
				->where('countries.url_segment', '=', $segment)
				->where('country_texts.language_id', '=', $language_id)
				->as_object()
				->execute()
				->current();
	}

}