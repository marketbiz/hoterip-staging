<?php defined('SYSPATH') or die('No direct script access.');

class Model_Hotel extends ORM implements Acl_Resource_Interface {
  
  protected $_has_many = array(
    'rooms' => array(),
  );

  public function get_resource_id()
  {
    return 'hotel';
  }

  public function get_hotel_by_id($language_id, $hotel_id)
  {
    $hotel = DB::select(
        array('hotels.id', 'id'),
        array('hotels.currency_id', 'currency_id'),
        array('hotels.url_segment', 'segment'),
        array('hotel_texts.name', 'name'),
        array('hotel_types.name', 'type'),
        'star',
        'address',
        'telephone',
        'fax',
        'opening_year',
        'renovation_year',
        'number_of_floors',
        'number_of_rooms',
        'distance_from_airport',
        'time_from_airport',
        'distance_from_city_center',
        'distance_from_station',
        'room_voltage',
        'is_pet_allowed',
        'check_in_time',
        'check_out_time',
        'child_age_from',
        'child_age_until',
        'coordinate',
        'commission_rate',
        'point_rate',
        'service_charge_rate',
        'tax_rate',
        array('hotel_photos.basename', 'basename'),
        array('hotel_photos.small_basename', 'small_basename'),
        array('hotel_photos.medium_basename', 'medium_basename'),
        'timezone',

        array('countries.id', 'country_id'),
        array('countries.url_segment', 'country_segment'),
        array('country_texts.name', 'country_name'),
        array('cities.id', 'city_id'),
        array('cities.url_segment', 'city_segment'),
        array('city_texts.name', 'city_name'),

        array('hotels.district_id', 'district_id'),
        array(DB::expr("
            IF(hotels.district_id IS NOT NULL,
              (SELECT url_segment FROM districts WHERE id = hotels.district_id),
              NULL
            )
        "), 'district_segment'),
        array(DB::expr("
          IF(hotels.district_id IS NOT NULL,
            (SELECT name FROM district_texts WHERE district_id = hotels.district_id AND language_id = $language_id),
            NULL
          )
        "), 'district_name')
      )
      ->from('hotels')
      ->join('hotel_texts')->on('hotels.id', '=', 'hotel_texts.hotel_id')

      ->join('hotel_types')->on('hotels.hotel_type_id', '=', 'hotel_types.id')
      
      ->join('cities')->on('hotels.city_id', '=', 'cities.id')
      ->join('city_texts')->on('hotels.city_id', '=', 'city_texts.city_id')
      ->join('countries')->on('hotels.city_id', '=', 'cities.id')
      ->join('country_texts')->on('cities.country_id', '=', 'country_texts.country_id')
      ->join('hotel_photos', 'left')->on('hotels.hotel_photo_id', '=', 'hotel_photos.id')

      ->where('hotels.id', '=', $hotel_id)
      /// Hotel active
      ->where('hotels.active', '=', 1)
      ->where('hotel_texts.language_id', '=', $language_id)
      ->where('city_texts.language_id', '=', $language_id)
      ->where('country_texts.language_id', '=', $language_id)

      ->as_object()
      ->execute()
      ->current();

    return $hotel;
  }

  public function url_segment_available($model, $url_segment)
  {
    if ( ! $model->loaded())
    {
      return ORM::factory('hotel')
        ->where('url_segment', '=', $url_segment)
        ->count_all() == 0;
    }
    else
    {
      return ORM::factory('hotel')
        ->where('id', '<>', $model->id)
        ->where('url_segment', '=', $url_segment)
        ->count_all() == 0;
    }
  }
  
  public function filters()
  {
    return array(
      'url_segment' => array(
        array('strtolower'),
      ),
    );
  }
  
  public function rules()
  {
    return array(
      'url_segment' => array(
        array('not_empty'),
        array(array($this, 'url_segment_available'), array(':model', ':value')),
        array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
      ),
      'address' => array(
        array('not_empty'),
      ),
      'telephone' => array(
        array('not_empty'),
        array('max_length', array(':value', 20))
      ),
      'fax' => array(
        array('max_length', array(':value', 20))
      ),
      'url' => array(
        array('max_length', array(':value', 50))
      ),
      'opening_year' => array(
        array('digit'),
      ),
      'renovation_year' => array(
        array('digit'),
      ),
      'number_of_rooms' => array(
        array('digit'),
      ),
      'number_of_floors' => array(
        array('digit'),
      ),
    );
  }
  
  public function labels()
  {
    return array(
      'city_id' => 'City ID',
      'hotel_type_id' => 'Hotel Type ID',
      'url_segment' => 'URL Segment',
      'star' => 'Star',
      'address' => 'Address',
      'telephone' => 'Telephone',
      'fax' => 'Fax',
      'url' => 'URL',
      'opening_year' => 'Opening Year',
      'renovation_year' => 'Renovation Year',
      'number_of_floors' => 'Number of Floors',
      'number_of_rooms' => 'Number of Rooms',
      'distance_from_airport' => 'Distance from Airport',
      'time_from_airport' => 'Time from Airport',
      'distance_from_city_center' => 'Distance from City Center',
      'distance_from_station' => 'Distance from Station',
      'room_voltage' => 'Room Voltage',
      'check_in_time' => 'Check-in Time',
      'check_out_time' => 'Check-out Time',
      'is_pets_allowed' => 'Pet Allowed',
      'coordinate' => 'Coordinate',
    );
  }
  
  public function create_hotel($values)
  {
    // Create hotel
    $this->values($values, array(
      'city_id',
      'district_id',
      'currency_id',
      'hotel_type_id',
      'url_segment',
      'star',
      'address',
      'telephone',
      'fax',
      'opening_year',
      'renovation_year',
      'number_of_floors',
      'number_of_rooms',
      'distance_from_airport',
      'time_from_airport',
      'distance_from_city_center',
      'distance_from_station',
      'room_voltage',
      'is_pet_allowed',
      'check_in_time',
      'check_out_time',
      'child_age_from',
      'child_age_until',
      'coordinate',
      'commission_rate',
      'commission_rate_minimum',
      'tax_rate',
      'service_charge_rate',
      'point_rate',
      'rank',
      'basename',
      'small_basename',
      'timezone',
      ))
      ->create();

    // Create hotel texts

    // Get languages
    $languages = ORM::factory('language')->find_all();

    // Get field value from post array
    $names = array_filter(Arr::get($values, 'names', array()));
    $descriptions = array_filter(Arr::get($values, 'descriptions', array()));

    // Set default language id using english
    $default_language_id = ORM::factory('language')
      ->where('code', '=', 'en-us')
      ->find()
      ->id;

    // Set default text value
    $default_name = Arr::get($names, $default_language_id, '');
    $default_description = NULL;

    $hotel_texts = array();

    // Loop each language
    foreach ($languages as $language)
    {
      // Get text and set it's values
      $hotel_texts[] = ORM::factory('hotel_text')
        ->values(array(
          'hotel_id' => $this->id,
          'language_id' => $language->id,
          'name' => Arr::get($names, $language->id, $default_name),
          'description' => Arr::get($descriptions, $language->id, $default_description),
        ))
        ->create();
    }

    // Create hotel facility
    $hotel_facility = ORM::factory('hotel_facility')
      ->set('hotel_id', $this->id)
      ->values(Arr::extract($values, array(
        'is_banquet_room_available',
        'is_bars_available',
        'is_business_center_available',
        'is_car_parking_available',
        'is_casino_available',
        'is_clinic_available',
        'is_club_lounge_available',
        'is_coffee_shop_available',
        'is_disabled_facilities_available',
        'is_departure_lounge_available',
        'is_elevator_available',
        'is_garden_available',
        'is_golf_course_available',
        'is_gift_shop_available',
        'is_gym_available',
        'is_hair_salon_available',
        'is_jacuzzi_available',
        'is_karaoke_room_available',
        'is_kids_club_available',
        'is_kids_pool_available',
        'is_library_available',
        'is_luggage_room_available',
        'is_meeting_room_available',
        'is_night_club_available',
        'is_poolside_bar_available',
        'is_private_beach_available',
        'is_restaurant_available',
        'is_safety_box_available',
        'is_sauna_available',
        'is_spa_available',
        'is_squash_court_available',
        'is_steam_room_available',
        'is_swimming_pool_available',
        'is_tennis_court_available',
      ), 0))
      ->create();

    // Create hotel service
    $hotel_service = ORM::factory('hotel_service')
      ->set('hotel_id', $this->id)
      ->values(Arr::extract($values, array(
        'is_room_service_24hour_available',
        'is_reception_24hour_available',
        'is_airport_transfer_available',
        'is_baby_sitter_available',
        'is_butler_available',
        'is_concierge_available',
        'is_laundry_available',
        'is_massage_available',
        'is_money_changer_available',
        'is_rental_baby_car_available',
        'is_rental_bicycle_available',
        'is_rental_multi_plug_available',
        'is_rental_wheel_chair_available',
        'is_room_service_available',
        'is_shuttle_available',
        'is_valet_parking_available',
        'is_free_wifi_available',
        'is_paid_wifi_available',
      ), 0))
      ->create();
    
    return $this;
  }
  
  public function update_hotel($values)
  {
    // Update hotel
    $this->values($values, array(
      'city_id',
      'district_id',
      'currency_id',
      'hotel_type_id',
      'url_segment',
      'active',
      'star',
      'address',
      'telephone',
      'fax',
      'opening_year',
      'renovation_year',
      'number_of_floors',
      'number_of_rooms',
      'distance_from_airport',
      'time_from_airport',
      'distance_from_city_center',
      'distance_from_station',
      'room_voltage',
      'is_pet_allowed',
      'check_in_time',
      'check_out_time',
      'child_age_from',
      'child_age_until',
      'coordinate',
      'commission_rate',
      'commission_rate_minimum',
      'tax_rate',
      'service_charge_rate',
      'point_rate',
      'rank',
      'basename',
      'small_basename',
      'timezone',
      ))
      ->update();

    // Update hotel texts

    // Get languages
    $languages = ORM::factory('language')
      ->find_all();

    // Get field value from post array
    $names = array_filter(Arr::get($values, 'names', array()));
    $descriptions = array_filter(Arr::get($values, 'descriptions', array()));

    // Set default language id using english
    $default_language_id = ORM::factory('language')
      ->where('code', '=', 'en-us')
      ->find()
      ->id;

    // Set default text value
    $default_name = Arr::get($names, $default_language_id, '');
    $default_description = NULL;

    $hotel_texts = array();

    // Loop each language
    foreach ($languages as $language)
    {
      // Get text and set it's values
      $hotel_texts[] = ORM::factory('hotel_text')
        ->where('hotel_id', '=', $this->id)
        ->where('language_id', '=', $language->id)
        ->find()
        ->values(array(
          'hotel_id' => $this->id,
          'language_id' => $language->id,
          'name' => Arr::get($names, $language->id, $default_name),
          'description' => Arr::get($descriptions, $language->id, $default_description),
        ))
        ->save();
    }

    // Update hotel facility
    $hotel_facility = ORM::factory('hotel_facility')
      ->where('hotel_id', '=', $this->id)
      ->find()
      ->values(Arr::extract($values, array(
        'is_banquet_room_available',
        'is_bars_available',
        'is_business_center_available',
        'is_car_parking_available',
        'is_casino_available',
        'is_clinic_available',
        'is_club_lounge_available',
        'is_coffee_shop_available',
        'is_disabled_facilities_available',
        'is_departure_lounge_available',
        'is_elevator_available',
        'is_garden_available',
        'is_golf_course_available',
        'is_gift_shop_available',
        'is_gym_available',
        'is_hair_salon_available',
        'is_jacuzzi_available',
        'is_karaoke_room_available',
        'is_kids_club_available',
        'is_kids_pool_available',
        'is_library_available',
        'is_luggage_room_available',
        'is_meeting_room_available',
        'is_night_club_available',
        'is_poolside_bar_available',
        'is_private_beach_available',
        'is_restaurant_available',
        'is_safety_box_available',
        'is_sauna_available',
        'is_spa_available',
        'is_squash_court_available',
        'is_steam_room_available',
        'is_swimming_pool_available',
        'is_tennis_court_available',
      ), 0))
      ->update();

    // Update hotel service
    $hotel_service = ORM::factory('hotel_service')
      ->where('hotel_id', '=', $this->id)
      ->find()
      ->values(Arr::extract($values, array(
        'is_room_service_24hour_available',
        'is_reception_24hour_available',
        'is_airport_transfer_available',
        'is_baby_sitter_available',
        'is_butler_available',
        'is_concierge_available',
        'is_laundry_available',
        'is_massage_available',
        'is_money_changer_available',
        'is_rental_baby_car_available',
        'is_rental_bicycle_available',
        'is_rental_multi_plug_available',
        'is_rental_wheel_chair_available',
        'is_room_service_available',
        'is_shuttle_available',
        'is_valet_parking_available',
        'is_free_wifi_available',
        'is_paid_wifi_available',
      ), 0))
      ->update();
    
    return $this;
  }
  
  /**
   *  Search_hotels
   */

  public function search_hotels($query)
  {
    // Expand the array
    $language_id = Arr::get($query, 'language_id');
    $country_id = Arr::get($query, 'country_id');
    $city_id = Arr::get($query, 'city_id');
    $hotel_name = Arr::get($query, 'hotel_name');
    $star_ratings = Arr::get($query, 'star_ratings');

    $hotel_ids = Arr::get($query, 'hotel_ids');

    // get hotels from table
    $sql = DB::select(
        array('hotels.id', 'hotel_id'),
        array('hotels.currency_id', 'currency_id'),
        array('hotels.url_segment', 'hotel_segment'),
        array('hotel_types.name', 'type'),
        array('hotel_texts.name', 'hotel_name'),
        array('hotel_photos.basename', 'hotel_basename'),
        array('hotel_photos.small_basename', 'hotel_small_basename'),
        array('hotel_photos.medium_basename', 'hotel_medium_basename'),
        array('hotels.star', 'hotel_star'),
        array('hotels.address', 'hotel_address'),
        array('hotels.telephone', 'telephone'),
        array('hotels.fax', 'fax'),
        array('hotels.coordinate', 'hotel_coordinate'),
        array('hotels.commission_rate', 'hotel_commission_rate'),
        array('hotels.rank', 'hotel_rank'),
        array('hotels.service_charge_rate', 'hotel_service_charge_rate'),
        array('hotels.tax_rate', 'hotel_tax_rate'),
        array('hotels.timezone', 'hotel_timezone'),
        array('hotels.point_rate', 'point_rate'),
        array('hotels.coordinate', 'coordinate'),
        array('hotels.check_in_time', 'check_in_time'),
        array('hotels.check_out_time', 'check_out_time'),
        array('countries.id', 'country_id'),
        array('countries.url_segment', 'country_segment'),
        array('country_texts.name', 'country_name'),
        array('cities.id', 'city_id'),
        array('cities.url_segment', 'city_segment'),
        array('city_texts.name', 'city_name'),
        array('hotels.district_id', 'district_id'),
        array('rooms.id', 'room_id'),
        array('room_texts.name', 'room_name'),
        array('rooms.publish_price', 'max_min_publish_price'),
        array('districts.url_segment', 'district_segment'),
        array('district_texts.name', 'district_name'),
        'hotel_services.is_free_wifi_available',
        'hotel_services.is_paid_wifi_available'
        )
      ->from('hotels')

      // Join table
      ->join('hotel_types')->on('hotels.hotel_type_id', '=', 'hotel_types.id')
      ->join('hotel_texts')->on('hotels.id', '=', 'hotel_texts.hotel_id')
      ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
      ->join('districts')->on('districts.id', '=', 'hotels.district_id')
      ->join('district_texts')->on('district_texts.district_id', '=', 'districts.id')
      ->join('cities')->on('hotels.city_id', '=', 'cities.id')
      ->join('city_texts')->on('hotels.city_id', '=', 'city_texts.city_id')
      ->join('countries')->on('cities.country_id', '=', 'countries.id')
      ->join('country_texts')->on('cities.country_id', '=', 'country_texts.country_id')
      ->join('reviews', 'left')->on('hotels.id', '=', 'reviews.hotel_id')
      ->join('hotel_facilities')->on('hotels.id', '=', 'hotel_facilities.hotel_id')
      ->join('hotel_services')->on('hotels.id', '=', 'hotel_services.hotel_id')
      ->join('hotel_photos', 'left')->on('hotels.hotel_photo_id', '=', 'hotel_photos.id');

      if ($hotel_ids == NULL) {
      // Filter county and city
        $sql = $sql
          ->where('countries.id', '=', $country_id)
          ->where('cities.id', '=', $city_id);
      }else{
        $sql = $sql
          ->where('hotels.id', '=', $hotel_ids);
      }

      // Filter text
      $sql = $sql
      ->where('hotels.active', '=', 1)
      ->where('room_texts.language_id', '=', $language_id)
      ->where('hotel_texts.language_id', '=', $language_id)
      ->where('city_texts.language_id', '=', $language_id)
      ->where('country_texts.language_id', '=', $language_id);


    // Group by hotel id
    $sql = $sql->group_by('hotel_id');

    // Ececute and as array key use hotel id
    $hotels = $sql
      ->as_object()
      ->execute()
      ->as_array('hotel_id');

    return $hotels;
  }

  /**
   *  search_hotels_limit
   */
  public function search_hotels_limit($hotel_ids, $query)
  {
    // Expand the array
    $language_id = Arr::get($query, 'language_id');
    $country_id = Arr::get($query, 'country_id');
    $city_id = Arr::get($query, 'city_id');
    $district_ids = Arr::get($query, 'district_ids');
    $hotel_name = Arr::get($query, 'hotel_name');
    $star_ratings = Arr::get($query, 'star_ratings');
    $review_ratings = Arr::get($query, 'review_ratings');
    $hotel_facilities = Arr::get($query, 'hotel_facilities');
    $hotel_type_ids = Arr::get($query, 'hotel_type_ids');

    $limit = Arr::get($query, 'limit') ? Arr::get($query, 'limit') : 30;
    $offset = Arr::get($query, 'offset') ? Arr::get($query, 'offset') : 0;

    // If country or city id is null return null
    if ($country_id == NULL || $city_id == NULL)
    {
      return NULL;
    }
    else
    {
      // get hotels from table
      $sql = DB::select(
          array('hotels.id', 'hotel_id'),
          array('hotels.url_segment', 'hotel_segment'),
          array('hotel_photos.basename', 'hotel_basename'),
          array('hotel_photos.small_basename', 'hotel_small_basename'),
          array('hotels.rank', 'hotel_rank'),
          array('hotels.tax_rate', 'hotel_tax_rate'),
          array('hotels.timezone', 'hotel_timezone'),

          array('countries.id', 'country_id'),
          array('countries.url_segment', 'country_segment'),
          array('cities.id', 'city_id'),
          array('cities.url_segment', 'city_segment'),
          'hotel_facilities.is_banquet_room_available',
          'hotel_facilities.is_bars_available',
          'hotel_facilities.is_business_center_available',
          'hotel_facilities.is_car_parking_available',
          'hotel_facilities.is_casino_available',
          'hotel_facilities.is_clinic_available',
          'hotel_facilities.is_club_lounge_available',
          'hotel_facilities.is_coffee_shop_available',
          'hotel_facilities.is_departure_lounge_available',
          'hotel_facilities.is_disabled_facilities_available',
          'hotel_facilities.is_elevator_available',
          'hotel_facilities.is_garden_available',
          'hotel_facilities.is_gift_shop_available',
          'hotel_facilities.is_gym_available',
          'hotel_facilities.is_golf_course_available',
          'hotel_facilities.is_hair_salon_available',
          'hotel_facilities.is_jacuzzi_available',
          'hotel_facilities.is_karaoke_room_available',
          'hotel_facilities.is_kids_club_available',
          'hotel_facilities.is_kids_pool_available',
          'hotel_facilities.is_library_available',
          'hotel_facilities.is_luggage_room_available',
          'hotel_facilities.is_meeting_room_available',
          'hotel_facilities.is_night_club_available',
          'hotel_facilities.is_private_beach_available',
          'hotel_facilities.is_poolside_bar_available',
          'hotel_facilities.is_restaurant_available',
          'hotel_facilities.is_safety_box_available',
          'hotel_facilities.is_sauna_available',
          'hotel_facilities.is_spa_available',
          'hotel_facilities.is_squash_court_available',
          'hotel_facilities.is_steam_room_available',
          'hotel_facilities.is_swimming_pool_available',
          'hotel_facilities.is_tennis_court_available',
          
          'hotel_services.is_free_wifi_available',
          'hotel_services.is_paid_wifi_available'
          )
        ->from('hotels')

        // Join table
        ->join('cities')->on('hotels.city_id', '=', 'cities.id')
        ->join('countries')->on('cities.country_id', '=', 'countries.id')
        ->join('reviews', 'left')->on('hotels.id', '=', 'reviews.hotel_id')
        ->join('hotel_facilities')->on('hotels.id', '=', 'hotel_facilities.hotel_id')
        ->join('hotel_services')->on('hotels.id', '=', 'hotel_services.hotel_id')
        ->join('hotel_photos', 'left')->on('hotels.hotel_photo_id', '=', 'hotel_photos.id');

        // Filter county and city
        // echo '<pre>';
        // print_r($hotel_ids);
        if (empty($hotel_ids)) {
          // Filter county and city
          $sql = $sql
            ->where('countries.id', '=', $country_id)
            ->where('cities.id', '=', $city_id);
        }
        else
        {
          $sql = $sql
          ->where('hotels.id', 'in', array_values($hotel_ids));
        }

      // Group by hotel id
      $sql = $sql->where('hotels.active', '=', 1)->group_by('hotel_id');

      // Ececute and as array key use hotel id
      $hotels = $sql
        ->order_by('hotels.rank', 'DESC')
        // ->limit(30)
        // ->offset(0)
        ->as_object()
        ->execute()
        ->as_array('hotel_id');

      return $hotels;
    }
  }

  /**
   *  search_campaigns
   */
  public function search_campaigns($ids, $query, $from_cache=false, $api_wego=array())
  {
    $language_id = Arr::get($query, 'language_id');
    $check_in = Arr::get($query, 'check_in');
    $check_out = Arr::get($query, 'check_out');
    $number_of_rooms = Arr::get($query, 'number_of_rooms');
    $capacities = Arr::get($query, 'capacities');
    $now = Arr::get($query, 'now');

    $final_date = Date::formatted_time("$check_out -1 day", 'Y-m-d');

    $room_id = Arr::get($query, 'InvTypeCode');

    // Query from cached hotel
    if($from_cache)
    {
      $sql = DB::select(
          array('hotels.id', 'hotel_id'),
          array('hotels.timezone', 'hotel_timezone'),
          array('hotels.star', 'hotel_star'),
          array('hotels.address', 'hotel_address'),
          array('hotels.currency_id', 'currency_id'),
          array('hotel_texts.name', 'hotel_name'),
          array('hotels.url_segment', 'hotel_segment'),
          array('hotel_photos.basename', 'hotel_basename'),
          array('hotel_photos.small_basename', 'hotel_small_basename'),
          array('hotels.service_charge_rate', 'hotel_service_charge_rate'),
          array('hotels.tax_rate', 'hotel_tax_rate'),
          array('rooms.id', 'room_id'),
          array('rooms.publish_price', 'room_publish_price'),
          array('room_texts.name', 'room_name'),
          array('countries.id', 'country_id'),
          array('countries.url_segment', 'country_segment'),
          array('country_texts.name', 'country_name'),
          array('cities.id', 'city_id'),
          array('cities.url_segment', 'city_segment'),
          array('city_texts.name', 'city_name'),
          array('campaigns.id', 'campaign_id'),
          array('campaigns.is_default', 'campaign_default'),
          array('campaigns.type', 'campaign_type'),
          array('campaigns.days_in_advance', 'early_days'),
          array('campaigns.within_days_of_arrival', 'last_days'),
          array('campaigns.number_of_free_nights', 'free_night'),
          array('campaigns.discount_rate', 'rate'),
          array('campaigns.discount_amount', 'amount'),
          array('campaigns.minimum_number_of_nights', 'campaign_min_nights'),
          array('campaigns.minimum_number_of_rooms', 'campaign_min_rooms'),
          array('campaigns.days_in_advance', 'campaign_early_days'),
          array('campaigns.within_days_of_arrival', 'campaign_last_days'),
          array('campaigns.number_of_free_nights', 'campaign_free_night'),
          array('campaigns.booking_start_date', 'booking_start_date'),
          array('campaigns.booking_end_date', 'booking_end_date'),
          array('campaigns.discount_rate', 'campaign_rate'),
          array('items.minimum_night', 'max_minimum_night')
          )
        ->from('api_wego')
        ->join('hotels')->on('hotels.id', '=', 'api_wego.hotel_id')
        ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
        ->join('hotel_photos')->on('hotels.hotel_photo_id', '=', 'hotel_photos.id')
        ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
        ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
        ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
        ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
        ->join('items')->on('rooms.id', '=', 'items.room_id')
        ->join('timezones')->on('hotels.timezone', '=', 'timezones.identifier')
        ->join('campaigns_languages')->on('campaigns.id', '=', 'campaigns_languages.campaign_id')
        ->join('room_texts', 'LEFT')->on('room_texts.room_id', '=', 'rooms.id')
        ->join('cities')->on('hotels.city_id', '=', 'cities.id')
        ->join('city_texts')->on('hotels.city_id', '=', 'city_texts.city_id')
        ->join('country_texts')->on('cities.country_id', '=', 'country_texts.country_id') 
        ->join('countries')->on('cities.country_id', '=', 'countries.id') 

          ->and_where_open()
          ->where('api_wego.hotel_id', '=', $api_wego['api_wego_hotel_id'])
          ->where('api_wego.campaign_id', '=', $api_wego['api_wego_campaign_id'])
          ->where('hotels.active', '=', 1)
          ->where('hotel_texts.language_id', '=', $language_id)
          ->where('campaigns.id', '=', $api_wego['api_wego_campaign_id'])
          ->where('rooms.id','=',$api_wego['api_wego_room_id'])
          ->where('items.date','=',$api_wego['api_wego_date'])
          ->and_where_close();

        $sql = $sql
        ->group_by('hotel_id')
        ->group_by('room_id')
        ->group_by('campaign_id')
        ;
        $campaigns = $sql
          ->execute()
          ->as_array()
        ;
    }
    // Query dynamically
    else if(!$from_cache)
    {
      $sql = DB::select(
        array('hotels.id', 'hotel_id'),
        array('hotels.timezone', 'hotel_timezone'),
        array('hotels.star', 'hotel_star'),
        array('hotels.address', 'hotel_address'),
        array('hotels.currency_id', 'currency_id'),
        array('hotel_texts.name', 'hotel_name'),
        array('hotels.url_segment', 'hotel_segment'),
        array('hotel_photos.basename', 'hotel_basename'),
        array('hotel_photos.small_basename', 'hotel_small_basename'),
        array('hotels.service_charge_rate', 'hotel_service_charge_rate'),
        array('hotels.tax_rate', 'hotel_tax_rate'),
        array('rooms.id', 'room_id'),
        array('rooms.publish_price', 'room_publish_price'),
        array('rooms.is_breakfast_included', 'breakfast'),
        array('rooms.adult_breakfast_price', 'breakfast_price'),
        array('room_texts.name', 'room_name'),
        array('countries.id', 'country_id'),
        array('countries.url_segment', 'country_segment'),
        array('country_texts.name', 'country_name'),
        array('cities.id', 'city_id'),
        array('cities.url_segment', 'city_segment'),
        array('city_texts.name', 'city_name'),
        array('campaigns.id', 'campaign_id'),
        array('campaigns.is_default', 'campaign_default'),
        array('campaigns.type', 'campaign_type'),
        array('campaigns.days_in_advance', 'early_days'),
        array('campaigns.within_days_of_arrival', 'last_days'),
        array('campaigns.number_of_free_nights', 'free_night'),
        array('campaigns.discount_rate', 'rate'),
        array('campaigns.discount_amount', 'amount'),
        array('campaigns.minimum_number_of_nights', 'campaign_min_nights'),
        array('campaigns.minimum_number_of_rooms', 'campaign_min_rooms'),
        array('campaigns.days_in_advance', 'campaign_early_days'),
        array('campaigns.within_days_of_arrival', 'campaign_last_days'),
        array('campaigns.number_of_free_nights', 'campaign_free_night'),
        array('campaigns.booking_start_date', 'booking_start_date'),
        array('campaigns.booking_end_date', 'booking_end_date'),
        array('campaigns.discount_rate', 'campaign_rate'),
        array(DB::expr('MAX(items.minimum_night)'), 'max_minimum_night'),
        array(DB::expr("
          IF(
            SUM(items.is_blackout)+
            SUM(IF(items.stock < $number_of_rooms, 1, 0))+
            SUM(
              CASE
                WHEN DAYOFWEEK(date) = 1 THEN IF(campaigns.is_stay_sunday_active, 0, 1)
                WHEN DAYOFWEEK(date) = 2 THEN IF(campaigns.is_stay_monday_active, 0, 1)
                WHEN DAYOFWEEK(date) = 3 THEN IF(campaigns.is_stay_tuesday_active, 0, 1)
                WHEN DAYOFWEEK(date) = 4 THEN IF(campaigns.is_stay_wednesday_active, 0, 1)
                WHEN DAYOFWEEK(date) = 5 THEN IF(campaigns.is_stay_thursday_active, 0, 1)
                WHEN DAYOFWEEK(date) = 6 THEN IF(campaigns.is_stay_friday_active, 0, 1)
                WHEN DAYOFWEEK(date) = 7 THEN IF(campaigns.is_stay_saturday_active, 0, 1)
                ELSE 0
              END
            ) > 0,

            #true
            #-----------------------------------
            '',
            #-----------------------------------

            #false
            #-----------------------------------
              AVG(
                CASE
                #----------------------------------------
                  WHEN items.is_campaign_blackout THEN
                      items.price
                  WHEN items.is_campaign_prices THEN
                      NULL
                  ELSE
                    CASE
                    #----------------------------------------
                      #early bird
                      WHEN campaigns.days_in_advance THEN
                        IF(DATEDIFF(items.date, CONVERT_TZ('$now','+00:00',timezones.offset)) >= campaigns.days_in_advance,
                          CASE
                          #-----------------------------------
                            WHEN campaigns.discount_rate THEN
                              items.price * ((100 - campaigns.discount_rate) / 100)

                            WHEN campaigns.discount_amount THEN
                              items.price - campaigns.discount_amount

                            WHEN campaigns.number_of_free_nights THEN
                              IF(items.date <= DATE_ADD('$final_date',INTERVAL -campaigns.number_of_free_nights DAY),
                                items.price,
                                0
                              )

                            ELSE
                              items.price
                          #-----------------------------------
                          END,
                          items.price
                        )

                      #last minute
                      WHEN campaigns.within_days_of_arrival THEN
                        IF(DATEDIFF(items.date, CONVERT_TZ('$now','+00:00',timezones.offset)) < campaigns.within_days_of_arrival,
                          CASE
                          #-----------------------------------
                            WHEN campaigns.discount_rate THEN
                              items.price * ((100 - campaigns.discount_rate) / 100)

                            WHEN campaigns.discount_amount THEN
                              items.price - campaigns.discount_amount

                            WHEN campaigns.number_of_free_nights THEN
                              IF(items.date <= DATE_ADD('$final_date',INTERVAL -campaigns.number_of_free_nights DAY),
                                items.price,
                                0
                              )

                            ELSE
                              items.price
                          #-----------------------------------
                          END,
                          items.price
                        )

                      #nomal
                      ELSE
                        CASE
                        #-----------------------------------
                          WHEN campaigns.discount_rate THEN
                            items.price * ((100 - campaigns.discount_rate) / 100)

                          WHEN campaigns.discount_amount THEN
                            items.price - campaigns.discount_amount

                          WHEN campaigns.number_of_free_nights THEN
                            IF(items.date <= DATE_ADD('$final_date',INTERVAL -campaigns.number_of_free_nights DAY),
                              items.price,
                              0
                            )

                          ELSE
                            items.price
                        #-----------------------------------
                        END
                    #----------------------------------------
                    END
                #----------------------------------------
                END
              )
            #-----------------------------------
          )
        "), 'campaign_average_price')
        )
        ->from('hotels')
        ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
        ->join('hotel_photos')->on('hotels.hotel_photo_id', '=', 'hotel_photos.id')
        ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
        ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
        ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
        ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
        ->join('items')->on('rooms.id', '=', 'items.room_id')
        ->join('timezones')->on('hotels.timezone', '=', 'timezones.identifier')
        ->join('campaigns_languages')->on('campaigns.id', '=', 'campaigns_languages.campaign_id')
        ->join('room_texts', 'LEFT')->on('room_texts.room_id', '=', 'rooms.id')
        ->join('cities')->on('hotels.city_id', '=', 'cities.id')
        ->join('city_texts')->on('hotels.city_id', '=', 'city_texts.city_id')
        ->join('countries')->on('cities.country_id', '=', 'countries.id')
        ->join('country_texts')->on('cities.country_id', '=', 'country_texts.country_id')
        ->where('hotels.active', '=', 1)
        ->where('hotel_texts.language_id', '=', $language_id)
        ->where('campaigns.is_show', '=', 1)
        ->where('campaigns_languages.language_id', '=', $language_id)
        ->where('hotels.id', 'in', $ids);

        if($room_id)
        {
          $sql->where('rooms.id', '=', $room_id);
        }

        // Filter booking date
        $sql = $sql
        ->where_open()
        ->and_where('campaigns.booking_start_date', '<=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
        ->and_where('campaigns.booking_end_date', '>=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
        ->where_close()

        // Filter stay date
        ->where_open()
        ->where('campaigns.stay_start_date', '<=', $check_in)
        ->and_where('campaigns.stay_end_date', '>=', $final_date)
        ->where_close()

        // Filter check in out
        ->where('items.date', '>=', $check_in)
        ->where('items.date', '<=', $final_date)

        // Filter number of guest
        ->where('room_capacities.number_of_adults', '>=', $capacities)

        // Filter ninimum rooms
        ->where('campaigns.minimum_number_of_rooms', '<=', $number_of_rooms)

        // Filter ninimum nights
        ->where('campaigns.minimum_number_of_nights', '<=', DB::expr("DATEDIFF('$check_out', '$check_in')"))

        ->group_by('hotel_id')
        ->group_by('room_id')
        ->group_by('campaign_id');
        
        $campaigns = $sql
          ->execute()
          ->as_array()
        ;
    }
    return $campaigns;
  }

  // Search campaign with campaign prices disable because of heavy load
  // public function search_campaigns($ids, $query)
  // {
  //   $language_id = Arr::get($query, 'language_id');
  //   $check_in = Arr::get($query, 'check_in');
  //   $check_out = Arr::get($query, 'check_out');
  //   $country_id = Arr::get($query, 'country_id');
  //   $city_id = Arr::get($query, 'city_id');
  //   $number_of_rooms = Arr::get($query, 'number_of_rooms');
  //   $capacities = Arr::get($query, 'capacities');
  //   $now = Arr::get($query, 'now');
  //   $campaigns = array();

  //   $final_date = Date::formatted_time("$check_out -1 day", 'Y-m-d');

  //   $nights = Date::span(strtotime($query['check_in']), strtotime($query['check_out']), 'days');

  //   $room_id = Arr::get($query, 'InvTypeCode');

  //   $sql = DB::select(
  //     array('hotels.id', 'hotel_id'),
  //     array('hotels.timezone', 'hotel_timezone'),
  //     array('hotels.star', 'hotel_star'),
  //     array('hotels.address', 'hotel_address'),
  //     array('hotels.currency_id', 'currency_id'),
  //     array('hotel_texts.name', 'hotel_name'),
  //     array('hotels.url_segment', 'hotel_segment'),
  //     array('hotel_photos.basename', 'hotel_basename'),
  //     array('hotel_photos.small_basename', 'hotel_small_basename'),
  //     array('hotels.service_charge_rate', 'hotel_service_charge_rate'),
  //     array('hotels.tax_rate', 'hotel_tax_rate'),
  //     array('rooms.id', 'room_id'),
  //     array('rooms.publish_price', 'room_publish_price'),
  //     array('room_texts.name', 'room_name'),
  //     array('countries.id', 'country_id'),
  //     array('countries.url_segment', 'country_segment'),
  //     array('country_texts.name', 'country_name'),
  //     array('cities.id', 'city_id'),
  //     array('cities.url_segment', 'city_segment'),
  //     array('city_texts.name', 'city_name'),
  //     array('campaigns.id', 'campaign_id'),
  //     array('campaigns.is_default', 'campaign_default'),
  //     array('campaigns.type', 'campaign_type'),
  //     array('campaigns.days_in_advance', 'early_days'),
  //     array('campaigns.within_days_of_arrival', 'last_days'),
  //     array('campaigns.number_of_free_nights', 'free_night'),
  //     array('campaigns.discount_rate', 'campaign_rate'),
  //     array('campaigns.discount_amount', 'campaign_amount'),
  //     array('campaigns.minimum_number_of_nights', 'campaign_min_nights'),
  //     array('campaigns.minimum_number_of_rooms', 'campaign_min_rooms'),
  //     array('campaigns.days_in_advance', 'campaign_early_days'),
  //     array('campaigns.within_days_of_arrival', 'campaign_last_days'),
  //     array('campaigns.number_of_free_nights', 'campaign_free_night'),
  //     array('campaigns.booking_start_date', 'booking_start_date'),
  //     array('campaigns.booking_end_date', 'booking_end_date'),
  //     array('campaigns.discount_rate', 'campaign_rate'),

  //     array('campaigns.is_stay_sunday_active', 'is_stay_sunday_active'),
  //     array('campaigns.is_stay_monday_active', 'is_stay_monday_active'),
  //     array('campaigns.is_stay_tuesday_active', 'is_stay_tuesday_active'),
  //     array('campaigns.is_stay_wednesday_active', 'is_stay_wednesday_active'),
  //     array('campaigns.is_stay_thursday_active', 'is_stay_thursday_active'),
  //     array('campaigns.is_stay_friday_active', 'is_stay_friday_active'),
  //     array('campaigns.is_stay_saturday_active', 'is_stay_saturday_active'),

  //     array('timezones.offset', 'timezone_offset'),


  //     array(DB::expr('MAX(items.minimum_night)'), 'max_minimum_night')
  //     )
  //     ->from('hotels')
  //     ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
  //     ->join('hotel_photos')->on('hotels.hotel_photo_id', '=', 'hotel_photos.id')
  //     ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
  //     ->join('room_capacities', 'left')->on('room_capacities.room_id', '=', 'rooms.id')
  //     ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
  //     ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
  //     ->join('items')->on('rooms.id', '=', 'items.room_id')
  //     ->join('timezones')->on('hotels.timezone', '=', 'timezones.identifier')
  //     ->join('campaigns_languages')->on('campaigns.id', '=', 'campaigns_languages.campaign_id')
  //     ->join('room_texts', 'LEFT')->on('room_texts.room_id', '=', 'rooms.id')
  //     ->join('cities')->on('hotels.city_id', '=', 'cities.id')
  //     ->join('city_texts')->on('hotels.city_id', '=', 'city_texts.city_id')
  //     ->join('countries')->on('cities.country_id', '=', 'countries.id')
  //     ->join('country_texts')->on('cities.country_id', '=', 'country_texts.country_id')
  //     ->where('hotels.active', '=', 1)
  //     ->where('hotel_texts.language_id', '=', $language_id)
  //     ->where('campaigns.is_show', '=', 1)
  //     ->where('campaigns_languages.language_id', '=', $language_id)
  //     ;//->where('hotels.id', 'in', $ids);

  //     if($room_id)
  //     {
  //       $sql->where('rooms.id', '=', $room_id);
  //     }

  //     if ($ids == NULL) {
  //     // Filter county and city
  //       $sql = $sql
  //         ->where('countries.id', '=', $country_id)
  //         ->where('cities.id', '=', $city_id);
  //     }else{
  //       $sql = $sql
  //         ->where('hotels.id', 'in', $ids);
  //     }

  //     // Filter booking date
  //     $sql = $sql
  //     ->where_open()
  //     ->and_where('campaigns.booking_start_date', '<=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
  //     ->and_where('campaigns.booking_end_date', '>=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
  //     ->where_close()

  //     // Filter stay date
  //     ->where_open()
  //     ->where('campaigns.stay_start_date', '<=', $check_in)
  //     ->and_where('campaigns.stay_end_date', '>=', $final_date)
  //     ->where_close()

  //     // Filter check in out
  //     ->where('items.date', '>=', $check_in)
  //     ->where('items.date', '<=', $final_date)

  //     ->where('items.stock', '>', 0)

  //     // Filter number of guest
  //     ->where('room_capacities.number_of_adults', '>=', $capacities)

  //     // Filter ninimum rooms
  //     ->where('campaigns.minimum_number_of_rooms', '<=', $number_of_rooms)

  //     // Filter ninimum nights
  //     ->where('campaigns.minimum_number_of_nights', '<=', DB::expr("DATEDIFF('$check_out', '$check_in')"))

  //     ->group_by('hotel_id')
  //     ->group_by('room_id')
  //     ->group_by('campaign_id');
      
  //     $temporary_selects = $sql
  //       ->execute()
  //       ->as_array()
  //     ;
  //   if(count($temporary_selects) > 0)
  //   {
  //     $i=0;
  //     foreach ($temporary_selects as $key => $temporary_select) 
  //     {
  //       // ReDeclare variable
  //       $campaign_id = $temporary_select['campaign_id'];
  //       $room_id = $temporary_select['room_id'];
  //       $night_counts = 0;
  //       $averege_prices = NULL;

  //       $is_stay_sunday_active = $temporary_select['is_stay_sunday_active'];
  //       $is_stay_monday_active = $temporary_select['is_stay_monday_active'];
  //       $is_stay_tuesday_active = $temporary_select['is_stay_tuesday_active'];
  //       $is_stay_wednesday_active = $temporary_select['is_stay_wednesday_active'];
  //       $is_stay_thursday_active = $temporary_select['is_stay_thursday_active'];
  //       $is_stay_friday_active = $temporary_select['is_stay_friday_active'];
  //       $is_stay_saturday_active = $temporary_select['is_stay_saturday_active'];
  //       $discount_rate = $temporary_select['campaign_rate'];
  //       $discount_amount = $temporary_select['campaign_amount'];
  //       $free_night = $temporary_select['campaign_free_night'];
  //       $within_days_of_arrival = $temporary_select['campaign_last_days'];
  //       $number_of_free_nights = $temporary_select['campaign_free_night'];
  //       $campaign_early_days = $temporary_select['campaign_early_days'];
  //       $timezone_offset = $temporary_select['timezone_offset'];

  //       // Set cheapest campaign values
  //       $hotel_campaigns =array();

  //       // First value hotel
  //       $campaign_start = reset($temporary_selects);
  //       $hotel_campaigns[$campaign_start['hotel_id']] = $campaign_start;

  //       // load items
  //       $items = DB::select()
  //         ->from('items')

  //         ->where('campaign_id', '=', DB::expr("IF (items.campaign_id != 0, $campaign_id, 0)"))

  //         ->where('room_id', '=', $room_id)
  //         ->where('items.date', '>=', $check_in)
  //         ->where('items.date', '<=', $final_date)
          
  //         ->order_by('campaign_id', 'ASC')

  //         ->execute()
  //         ->as_array('date')
  //         ;

  //       foreach ($items as $key_items => $item) 
  //       {
  //         // Check stock
  //         $stock_availability = ($item['stock'] < $number_of_rooms) ? 1:0; 

  //         // Check inactive items
  //         if($item['is_blackout'] + $stock_availability > 0){
  //           continue;
  //         }
  //         else
  //         {
  //           $night_counts ++;

  //           // Check day active
  //           $date = date('w', strtotime($item['date']));
            
  //           switch ($date) {
  //             case 1:
  //               if($temporary_select['is_stay_sunday_active']) $campaigns_price = $item['price'];
  //               break;
  //             case 2:
  //               if($temporary_select['is_stay_monday_active']) $campaigns_price = $item['price'];
  //               break;
  //             case 3:
  //               if($temporary_select['is_stay_tuesday_active']) $campaigns_price = $item['price'];
  //               break;
  //             case 4:
  //               if($temporary_select['is_stay_wednesday_active']) $campaigns_price = $item['price'];
  //                 break;
  //             case 5:
  //               if($temporary_select['is_stay_thursday_active']) $campaigns_price = $item['price'];
  //                 break;
  //             case 6:
  //               if($temporary_select['is_stay_friday_active']) $campaigns_price = $item['price'];
  //                 break;
  //             case 7:
  //               if($temporary_select['is_stay_saturday_active']) $campaigns_price = $item['price'];
  //                 break;

  //             default:
  //               $campaigns_price = 0;
  //               break;
  //           }
  //           if ($item['is_campaign_blackout']) {
  //             $campaigns_price = 0;
  //           }
  //           elseif ($item['is_campaign_prices']) {
  //             $campaigns_price = $item['price'];
  //           }
  //           else
  //           {
  //             $timezone_date = str_replace($temporary_select['timezone_offset'], 'Z', gmdate('Y-m-d', strtotime($now)));

  //             // If early days
  //             if($campaign_early_days > 0 AND (floor(strtotime($item['date'])  - strtotime($timezone_date))/(60*60*24) >= $campaign_early_days))
  //             {
  //               if($discount_rate > 0 ) {
  //                 $campaigns_price = $item['price'] * ((100 - $discount_rate) / 100);
  //               }elseif($discount_amount > 0 ) {
  //                 $campaigns_price = $item['price'] -$discount_amount;
  //               }elseif($number_of_free_nights > 0 AND (strtotime($item['date'] <= strtotime($final_date) - strtotime('1 day')))){
  //                 $campaigns_price = 0;
  //               }else
  //               {
  //                 $campaigns_price = $item['price'];
  //               }
  //             }
  //             elseif($within_days_of_arrival > 0 AND (floor(strtotime($item['date'])  - strtotime($timezone_date))/(60*60*24) < $within_days_of_arrival))
  //             {
  //               if($discount_rate > 0 ) {
  //                 $campaigns_price = $item['price'] * ((100 - $discount_rate) / 100);
  //               }elseif($discount_amount > 0 ) {
  //                 $campaigns_price = $item['price'] -$discount_amount;
  //               }elseif($number_of_free_nights > 0 AND (strtotime($item['date'] <= strtotime($final_date) - strtotime('1 day')))){
  //                 $campaigns_price = 0;
  //               }else
  //               {
  //                 $campaigns_price = $item['price'];
  //               }
  //             }
  //             else
  //             {
  //               if($discount_rate > 0 ) {
  //                 $campaigns_price = $item['price'] * ((100 - $discount_rate) / 100);
  //               }elseif($discount_amount > 0 ) {
  //                 $campaigns_price = $item['price'] -$discount_amount;
  //               }elseif($number_of_free_nights > 0 AND (strtotime($item['date'] <= strtotime($final_date) - strtotime('1 day')))){
  //                 $campaigns_price = 0;
  //               }else
  //               {
  //                 $campaigns_price = $item['price'];
  //               }
  //             }
  //           }
  //         }
  //         // Price selections
  //         $averege_prices[$item['date']] = isset($averege_prices[$item['date']]) ? $averege_prices[$item['date']] : 0;
  //         if($item['campaign_id'] == $campaign_id)
  //         {
  //           // decrease price if room default is already exist
  //           if(!empty($averege_prices_calculation[$item['date']][0]))
  //           {
  //             // decrease price if room default is already exist
  //             if(!isset($averege_prices_calculation[$item['date']][0]))
  //             {
  //               $averege_prices[$item['date']] = $averege_prices[$item['date']] - $averege_prices_calculation[$item['date']][0];

  //               $averege_prices[$item['date']] += $campaigns_price;
  //               unset($averege_prices_calculation[$item['date']][$item['campaign_id']]);
  //             }
  //             elseif(!empty($averege_prices_calculation[$item['date']][0]))
  //             {
  //               $averege_prices_calculation[$item['date']][$item['campaign_id']] = $campaigns_price;

  //               $averege_prices[$item['date']] += $campaigns_price;
  //             }
  //             /*
  //             * Application controls if available
  //             */
  //             // $application_controls =  unserialize($item['application_control']);

  //             // if (count($application_controls) > 0) 
  //             // {
  //             //   foreach ($application_controls as $key_application_control => $application_control) {
  //             //     if (date('w', strtotime($item['date'])) == $key_application_control AND $application_control) 
  //             //     {
  //             //       $averege_prices[$item['date']] = $averege_prices[$item['date']] - $averege_prices_calculation[$item['date']][0];
  //             //       $averege_prices[$item['date']] += $campaigns_price;unset($averege_prices_calculation[$item['date']][$item['campaign_id']]);
  //             //     }
  //             //   }
  //             // }
  //           }
  //         }
  //         elseif(!$item['campaign_id'])
  //         {
  //           $averege_prices_calculation[$item['date']][$item['campaign_id']] = $campaigns_price;

  //           $averege_prices[$item['date']] += $campaigns_price;
  //         }
  //       }

  //       if($nights == $night_counts AND $averege_prices != 0)
  //       {
  //         // Set to variable
  //         $campaigns[$i]['hotel_id'] = $temporary_select['hotel_id'];
  //         $campaigns[$i]['room_id'] = $temporary_select['room_id'];
  //         $campaigns[$i]['campaign_id'] = $temporary_select['campaign_id'];
  //         $campaigns[$i]['room_publish_price'] = $temporary_select['room_publish_price'];
  //         $campaigns[$i]['hotel_timezone'] = $temporary_select['hotel_timezone'];
  //         $campaigns[$i]['hotel_star'] = $temporary_select['hotel_star'];
  //         $campaigns[$i]['hotel_address'] = $temporary_select['hotel_address'];
  //         $campaigns[$i]['currency_id'] = $temporary_select['currency_id'];
  //         $campaigns[$i]['hotel_name'] = $temporary_select['hotel_name'];
  //         $campaigns[$i]['hotel_segment'] = $temporary_select['hotel_segment'];
  //         $campaigns[$i]['hotel_basename'] = $temporary_select['hotel_basename'];
  //         $campaigns[$i]['hotel_small_basename'] = $temporary_select['hotel_small_basename'];
  //         $campaigns[$i]['hotel_service_charge_rate'] = $temporary_select['hotel_service_charge_rate'];
  //         $campaigns[$i]['hotel_tax_rate'] = $temporary_select['hotel_tax_rate'];
  //         $campaigns[$i]['room_name'] = $temporary_select['room_name'];
  //         $campaigns[$i]['country_id'] = $temporary_select['country_id'];
  //         $campaigns[$i]['country_segment'] = $temporary_select['country_segment'];
  //         $campaigns[$i]['country_name'] = $temporary_select['country_name'];
  //         $campaigns[$i]['city_id'] = $temporary_select['city_id'];
  //         $campaigns[$i]['city_segment'] = $temporary_select['city_segment'];
  //         $campaigns[$i]['city_name'] = $temporary_select['city_name'];
  //         $campaigns[$i]['campaign_default'] = $temporary_select['campaign_default'];
  //         $campaigns[$i]['campaign_type'] = $temporary_select['campaign_type'];
  //         $campaigns[$i]['early_days'] = $temporary_select['early_days'];
  //         $campaigns[$i]['last_days'] = $temporary_select['last_days'];
  //         $campaigns[$i]['free_night'] = $temporary_select['free_night'];
  //         $campaigns[$i]['rate'] = $temporary_select['campaign_rate'];
  //         $campaigns[$i]['amount'] = $temporary_select['campaign_amount'];
  //         $campaigns[$i]['campaign_min_nights'] = $temporary_select['campaign_min_nights'];
  //         $campaigns[$i]['campaign_min_rooms'] = $temporary_select['campaign_min_rooms'];
  //         $campaigns[$i]['campaign_early_days'] = $temporary_select['campaign_early_days'];
  //         $campaigns[$i]['campaign_last_days'] = $temporary_select['campaign_last_days'];
  //         $campaigns[$i]['campaign_free_night'] = $temporary_select['campaign_free_night'];
  //         $campaigns[$i]['booking_start_date'] = $temporary_select['booking_start_date'];
  //         $campaigns[$i]['booking_end_date'] = $temporary_select['booking_end_date'];
  //         $campaigns[$i]['campaign_rate'] = $temporary_select['campaign_rate'];
  //         $campaigns[$i]['max_minimum_night'] = $temporary_select['max_minimum_night'];

  //         // Count averege
  //         $averege_prices_total = array_sum($averege_prices) / count($averege_prices);
  //         $campaigns[$i]['campaign_average_price'] = $averege_prices_total < 0 ? 0 : $averege_prices_total;
  //       }
        
  //       unset($averege_prices);
  //       $i++;
  //     }
  //   }
    
  //   return $campaigns;
  // }

  /**
   *  get_campaigns
   */
  
  public function get_campaigns($hotel_id, $query, $predifined = FALSE)
  {
    $language_id = Arr::get($query, 'language_id');
    $check_in = Arr::get($query, 'check_in');
    $check_out = Arr::get($query, 'check_out');
    $number_of_rooms = Arr::get($query, 'number_of_rooms');

    $capacities = Arr::get($query, 'capacities');
    $child = Arr::get($query, 'child');

    $now = Arr::get($query, 'now');

    $final_date = Date::formatted_time("$check_out -1 day", 'Y-m-d');

    $room_id = Arr::get($query, 'InvTypeCode');
    $campaign_id = Arr::get($query, 'RatePlanCode');

    $sql = DB::select(
      array('campaigns.id', 'campaign_id'),
      array('campaigns.is_default', 'campaign_default'),
      array('campaigns.type', 'type'),
      array('campaigns.minimum_number_of_nights', 'min_nights'),
      array('campaigns.minimum_number_of_rooms', 'min_rooms'),
      array('campaigns.days_in_advance', 'early_days'),
      array('campaigns.within_days_of_arrival', 'last_days'),
      array('campaigns.number_of_free_nights', 'free_night'),
      array('campaigns.discount_rate', 'rate'),
      array('campaigns.discount_amount', 'amount'),
      array('campaigns.stay_start_date', 'stay_start_date'),
      array('campaigns.stay_end_date', 'stay_end_date'),
      array('campaigns.booking_start_date', 'booking_start_date'),
      array('campaigns.booking_end_date', 'booking_end_date'),
      array('hotels.id', 'hotel_id'),
      array('hotels.url_segment', 'hotel_segment'),
      array('hotels.timezone', 'hotel_timezone'),
      array('hotels.service_charge_rate', 'hotel_service_charge_rate'),
      array('hotels.tax_rate', 'hotel_tax_rate'),
      array('hotels.currency_id', 'currency_id'),
      array('rooms.id', 'room_id'),
      array('room_texts.name', 'room_name'),
      array('rooms.publish_price', 'room_publish_price'),
      array('rooms.hotel_photo_id', 'room_photo_id'),
      array('rooms.is_breakfast_included', 'is_breakfast_included'),
      array('rooms.extrabed_price', 'extrabed_price'),
      array(DB::expr('MAX(items.minimum_night)'), 'max_minimum_night'),
      array('items.stock', 'stock'),
      'hotels.hotel_photo_id',
      'adult_breakfast_price',
      'child_breakfast_price',
      'is_breakfast_included',
      'number_of_persons_breakfast_included',
      'extrabed_price',
      'maximum_number_of_extrabeds',
      'is_extrabed_get_breakfast',
      'number_of_beds',
      'size'
    )
      ->from('hotels')
      ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('room_texts')->on('rooms.id', '=', 'room_texts.room_id')
      ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('items')->on('rooms.id', '=', 'items.room_id')
      ->join('timezones')->on('hotels.timezone', '=', 'timezones.identifier')
      ->join('campaigns_languages')->on('campaigns.id', '=', 'campaigns_languages.campaign_id')
      ->where('hotels.active', '=', 1)
      ->where('campaigns.is_show', '=', 1)
      ->where('room_texts.language_id', '=', $language_id)
      ->where('hotels.id', '=', $hotel_id)
      ;

      if($room_id)
      {
        if($room_id != '*')
        {
          $sql = $sql->where('rooms.id', '=', $room_id);
        }
      }

      if($campaign_id)
      {
        if($campaign_id != '*')
        {
          $sql = $sql->where('campaigns.id', '=', $campaign_id);
        }
      }

      if($predifined==TRUE)
      {
        $sql = $sql->and_where('rooms.hoterip_campaign', '=', 0);
        $sql = $sql->and_where('rooms.is_breakfast_included', '=', 1);
        //campaigns early bird enable to update
        // $sql = $sql->and_where('campaigns.days_in_advance', '=', 0);
      }
      else
      {
        $sql = $sql->and_where('campaigns_languages.language_id', '=', $language_id);
      }
      $sql = $sql
      // Filter booking date
      ->where_open()
      ->and_where('campaigns.booking_start_date', '<=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
      ->and_where('campaigns.booking_end_date', '>=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
      ->where_close()

      // Filter stay date
      ->where_open()
      ->and_where('campaigns.stay_start_date', '<=', $check_in)
      ->and_where('campaigns.stay_end_date', '>=', $final_date)
      ->where_close()

      // Filter check in out
      ->where('items.date', '>=', $check_in)
      ->where('items.date', '<=', $final_date)
      ->where('items.campaign_id', '=', 0)

      ->where_open()
      // Filter mininimum nights
      ->where('campaigns.minimum_number_of_nights', '<=', DB::expr("DATEDIFF('$check_out', '$check_in')"))
      ->or_where('items.minimum_night', '<=', DB::expr("DATEDIFF('$check_out', '$check_in')"))
      
      // Filter mininimum rooms
      ->or_where('campaigns.minimum_number_of_rooms', '<=', $number_of_rooms)
      ->where_close()
      
      ->group_by('campaign_id')
      ->group_by('room_id')

      ->group_by('items.date')
      ->order_by('campaign_id', 'ASC')

      ->order_by('rate', 'DESC')
      ->order_by('amount', 'DESC')
      ->order_by('free_night', 'DESC')
      ;

    $campaigns = $sql
      ->execute()
      ->as_array()
      ;

    return $campaigns;
  }
}