<?php defined('SYSPATH') or die('No direct script access.');

class Model_Campaign extends ORM implements Acl_Resource_Interface {
	
  protected $_has_many = array(
    'cancellations' => array(
      'through' => 'campaigns_cancellations',
    ),
    'rooms' => array(
      'through' => 'campaigns_rooms',
    ),
    'languages' => array(
      'through' => 'campaigns_languages',
    ),
  );
  
  public function get_resource_id()
	{
		return 'campaign';
	}
  
  public function rules()
	{
		return array(
			'booking_start_date' => array(
				array('not_empty'),
			),
      'booking_end_date' => array(
				array('not_empty'),
			),
      'stay_start_date' => array(
				array('not_empty'),
			),
      'stay_end_date' => array(
				array('not_empty'),
			),
      'discount_rate' => array(
        array('digit'),
      ),
      'minimum_number_of_nights' => array(
        array('not_empty'),
        array('digit'),
        array('range', array(':value', 1, 120)),
      ),
      'minimum_number_of_rooms' => array(
        array('not_empty'),
        array('digit'),
        array('range', array(':value', 1, 10)),
      ),
      'number_of_free_nights' => array(
        array('digit'),
      ),
      'days_in_advance' => array(
        array('digit'),
      ),
      'within_days_of_arrival' => array(
        array('digit'),
      ),
		);
	}
	
	public function labels()
	{
		return array(
			'booking_start_date' => 'Booking Start Date',
			'booking_end_date' => 'Booking End Date',
      'stay_start_date' => 'Stay Start Date',
			'stay_end_date' => 'Stay End Date',
      'discount_rate' => 'Discount Rate',
      'minimum_number_of_nights' => 'Minimum Number of Nights',
      'minimum_number_of_rooms' => 'Minimum Number of Rooms',
      'number_of_free_nights' > 'Number of Free Nights',
      'days_in_advance' => 'Days in Advance',
      'within_days_of_arrival' => 'Within Days of Arrival',
		);
	}
	
	public function create_campaign($values)
	{
    // Create campaign
    $this->values($values, array(
      'is_show',
      'is_default',
      'type',
      'booking_start_date',
      'booking_end_date',
      'stay_start_date',
      'stay_end_date',
      'is_stay_sunday_active',
      'is_stay_monday_active',
      'is_stay_tuesday_active',
      'is_stay_wednesday_active',
      'is_stay_thursday_active',
      'is_stay_friday_active',
      'is_stay_saturday_active',
      'discount_rate',
      'discount_amount',
      'minimum_number_of_nights',
      'minimum_number_of_rooms',
      'number_of_free_nights',
      'days_in_advance',
      'within_days_of_arrival',
      'stock_limit',
      'status',  
      ))
      ->create();

    if(!empty($values['additional_benefits']))
    {
	    	foreach ($values['additional_benefits'] as $key => $additional_benefit) {
	      //is value exist
	      $additional_benefit['value'] = !empty($additional_benefit['value']) ? $additional_benefit['value'] : '0';

	      // Insert into email queue
	      DB::insert('campaigns_benefits')
	        ->columns(array('campaign_id', 'benefit_id', 'value'))
	        ->values(array($this->id, $additional_benefit['benefit_id'], $additional_benefit['value']))
	        ->execute();
	    	}
     }
     
		return $this;
	}
	
	public function update_campaign($values)
	{
		// Update campaign
    $this->values($values, array(
      'is_show',
      'is_default',
      'type',
      'booking_start_date',
      'booking_end_date',
      'stay_start_date',
      'stay_end_date',
      'is_stay_sunday_active',
      'is_stay_monday_active',
      'is_stay_tuesday_active',
      'is_stay_wednesday_active',
      'is_stay_thursday_active',
      'is_stay_friday_active',
      'is_stay_saturday_active',
      'discount_rate',
      'discount_amount',
      'minimum_number_of_nights',
      'minimum_number_of_rooms',
      'number_of_free_nights',
      'days_in_advance',
      'within_days_of_arrival',
      'stock_limit',
      'status',
      ))
      ->update();

    // Delete and create announcement
    DB::delete('campaigns_benefits')
      ->where('campaign_id', '=', $this->id)
      ->execute();

    if(!empty($values['additional_benefits']))
    {
    	foreach ($values['additional_benefits'] as $key => $additional_benefit) {

      //is value exist
      $additional_benefit['value'] = !empty($additional_benefit['value']) ? $additional_benefit['value'] : '0';

      // Insert into email queue
      DB::insert('campaigns_benefits')
        ->columns(array('campaign_id', 'benefit_id', 'value'))
        ->values(array($this->id, $additional_benefit['benefit_id'], $additional_benefit['value']))
        ->execute();
    	}
    }

		return $this;
	}
  
  public static function valid_discount_amount($discount_amount, $room_ids, $stay_start_date, $stay_end_date)
  {
    $result = TRUE;
    
    $items = ORM::factory('item')
      ->where('room_id', 'IN', $room_ids)
      ->where('date', '>=', $stay_start_date)
      ->where('date', '<=', $stay_end_date)
      ->find_all();
    
    foreach ($items as $item)
    {
      if ($item->price - $discount_amount <= 0)
      {
        $result = FALSE;
        break;
      }
    }
      
    return $result;
  }
	
	public function as_log($prev_campaign = NULL, $prev_room_names = NULL, $prev_cancellation = NULL, $prev_other_benefits = NULL)
	{
		// Get hotel
		$hotel = ORM::factory('hotel')
			->select(
				array('campaigns.discount_rate', 'discount_rate'),
				array('campaigns.discount_amount', 'discount_amount'),
				array('campaigns.days_in_advance', 'days_in_advance'),
				array('campaigns.minimum_number_of_nights', 'minimum_number_of_nights')
				)
			->join('rooms')->on('rooms.hotel_id', '=', 'hotels.id')
			->join('campaigns_rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
			->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
			->where('campaigns.id', '=', $this->id)
			->group_by('hotels.id')
			->find();

		// Get hotel rooms
		$rooms = ORM::factory('room')
			->where('hotel_id', '=', $hotel->id)
			->find_all();
		
		// Get currency
		$currency = ORM::factory('currency')
			->where('id', '=', $hotel->currency_id)
			->find();
		
		// Benefit
		$benefit = '';
		
		if ($this->discount_rate > 0)
		{
			$benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night', array(
				':minimum_number_of_nights' => $this->minimum_number_of_nights,
				':minimum_number_of_rooms' => $this->minimum_number_of_rooms,
				':discount_rate' => $this->discount_rate,
			));
		}
		elseif ($this->discount_amount > 0)
		{
			$benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night', array(
				':minimum_number_of_nights' => $this->minimum_number_of_nights,
				':minimum_number_of_rooms' => $this->minimum_number_of_rooms,
				':currency_code' => $currency->code,
				':discount_amount' => $this->discount_amount,
			));
		}
		elseif ($this->number_of_free_nights)
		{
			$benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights', array(
				':minimum_number_of_nights' => $this->minimum_number_of_nights,
				':minimum_number_of_rooms' => $this->minimum_number_of_rooms,
				':number_of_free_nights' => $this->number_of_free_nights,
			));
		}

		if ($this->type == 2)
		{
			$benefit .= ' '.strtr('Guest must book :days_in_advance days in advance', array(':days_in_advance' => $this->days_in_advance));
		}
		elseif ($this->type == 3)
		{
			$benefit .= ' '.strtr('Guest must book within :within_days_of_arrival days of arrival', array(':within_days_of_arrival' => $this->within_days_of_arrival));
		}
		elseif ($this->type == 4)
		{
			$benefit .= ' Non refundable';
		}
		elseif ($this->type == 5)
		{
			$benefit .= ' Flash Deal';
		}

		$other_benefits = DB::select(
				'campaigns_benefits.*',
				array('benefit_texts.name', 'name')
			)
			->from('campaigns_benefits')
			->join('benefit_texts')->on('campaigns_benefits.benefit_id', '=', 'benefit_texts.benefit_id')
			->where('benefit_texts.language_id', '=', 1)
			->where('campaigns_benefits.campaign_id', '=', $this->id)
			->execute();
		
		$other_benefit_text = '';
		if(count($other_benefits))
		{
			$c = 1;
			foreach ($other_benefits as $key => $other_benefit)
			{
				if (count($other_benefits) != $c)
				{
					$other_benefit_text .= $other_benefit['name'].', ';
				}
				else
				{
					$other_benefit_text .= $other_benefit['name'];
				}
				$c++;
			}
		}

		$room_names = $this->rooms
			->select(array('room_texts.name', 'name'))
			->join('room_texts')->on('room_texts.room_id', '=', 'rooms.id')
			->where('room_texts.language_id', '=', 1)
			->find_all()
			->as_array('id', 'name');

		$cancellation = $this->cancellations
			->select(
				array('rule_1.name', 'rule_1_name'),
				array('rule_2.name', 'rule_2_name'),
				array('rule_3.name', 'rule_3_name')
			)
			->join(array('cancellation_rules', 'rule_1'), 'left')->on('rule_1.id', '=', 'level_1_cancellation_rule_id')
			->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'level_2_cancellation_rule_id')
			->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'level_3_cancellation_rule_id')
			->find();

		$result = array(
			'benefit' => $benefit,
			'other_benefit' => $other_benefit_text,
			'room_names' => count($rooms) == count($room_names) ? 'All Rooms' : implode(', ', $room_names),
			'booking_start_date' => date('M j, Y', strtotime($this->booking_start_date)),
			'booking_end_date' => date('M j, Y', strtotime($this->booking_end_date)),
			'stay_start_date' => date('M j, Y', strtotime($this->stay_start_date)),
			'stay_end_date' => date('M j, Y', strtotime($this->stay_end_date)),
			'cancellation' => $cancellation->loaded() ? implode(', ', array_filter(array($cancellation->rule_1_name, $cancellation->rule_2_name, $cancellation->rule_3_name))) : 'Default',
			'discount_rate' => $hotel->discount_rate,
			'discount_amount' => $hotel->discount_amount,
			'days_in_advance' => $hotel->days_in_advance,
			'minimum_number_of_nights' => $hotel->minimum_number_of_nights,
		);
		
		if ($prev_campaign)
		{
			$prev_benefit = '';
      
			if ($prev_campaign->discount_rate > 0)
			{
				$prev_benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_rate% discount per night', array(
					':minimum_number_of_nights' => $prev_campaign->minimum_number_of_nights,
					':minimum_number_of_rooms' => $prev_campaign->minimum_number_of_rooms,
					':discount_rate' => $prev_campaign->discount_rate,
				));
			}
			elseif ($prev_campaign->discount_amount > 0)
			{
				$prev_benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get :discount_amount :currency_code discount per night', array(
					':minimum_number_of_nights' => $prev_campaign->minimum_number_of_nights,
					':minimum_number_of_rooms' => $prev_campaign->minimum_number_of_rooms,
					':currency_code' => $currency->code,
					':discount_amount' => $prev_campaign->discount_amount,
				));
			}
			elseif ($prev_campaign->number_of_free_nights)
			{
				$prev_benefit = strtr('Stay at least :minimum_number_of_nights nights and book :minimum_number_of_rooms or more rooms get free :number_of_free_nights nights', array(
					':minimum_number_of_nights' => $prev_campaign->minimum_number_of_nights,
					':minimum_number_of_rooms' => $prev_campaign->minimum_number_of_rooms,
					':number_of_free_nights' => $prev_campaign->number_of_free_nights,
				));
			}

			if ($prev_campaign->type == 2)
			{
				$prev_benefit .= ' '.strtr('Guest must book :days_in_advance days in advance', array(':days_in_advance' => $prev_campaign->days_in_advance));
			}
			elseif ($prev_campaign->type == 3)
			{
				$prev_benefit .= ' '.strtr('Guest must book within :within_days_of_arrival days of arrival', array(':within_days_of_arrival' => $prev_campaign->within_days_of_arrival));
			}
			elseif ($prev_campaign->type == 4)
			{
				$prev_benefit .= ' Non refundable';
			}
			elseif ($prev_campaign->type == 5)
			{
				$prev_benefit .= ' Flash Deal';
			}

			$result = array_merge($result, array(
				'prev_benefit' => $prev_benefit,
				'prev_other_benefit' => $prev_other_benefits,
				'prev_room_names' => count($rooms) == count($prev_room_names) ? 'All Rooms' : implode(', ', $prev_room_names),
				'prev_booking_start_date' => date('M j, Y', strtotime($prev_campaign->booking_start_date)),
				'prev_booking_end_date' => date('M j, Y', strtotime($prev_campaign->booking_end_date)),
				'prev_stay_start_date' => date('M j, Y', strtotime($prev_campaign->stay_start_date)),
				'prev_stay_end_date' => date('M j, Y', strtotime($prev_campaign->stay_end_date)),
				'prev_cancellation' => $prev_cancellation->loaded() ? implode(', ', array_filter(array($prev_cancellation->rule_1_name, $prev_cancellation->rule_2_name, $prev_cancellation->rule_3_name))) : 'Default',
				'prev_discount_rate' => $prev_campaign->discount_rate,
				'prev_discount_amount' => $prev_campaign->discount_amount,
				'prev_days_in_advance' => $prev_campaign->days_in_advance,
				'prev_minimum_number_of_nights' => $prev_campaign->minimum_number_of_nights,
			));
		}
		
		return $result;
	}

	public function get_default_campaign_id($room_id)
	{
		return DB::select('campaigns.id')
				->from('campaigns_rooms')
				->join('campaigns')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
				->where('campaigns_rooms.room_id', '=', $room_id)
				->where('campaigns.is_default', '=', 1)
				->as_object()
				->execute()
				->get('id');
	}
	
	public function get_campaign_items($query)
	{
		$language_id = Arr::get($query, 'language_id');
		$check_in = Arr::get($query, 'check_in');
		$check_out = Arr::get($query, 'check_out');
		$room_id = Arr::get($query, 'InvTypeCode');
		$campaign_id = Arr::get($query, 'RatePlanCode');
		$now = Arr::get($query, 'now');

		$final_date = Date::formatted_time("$check_out -1 day", 'Y-m-d');

		$number_of_rooms = 1;

		// Get campaign
		$campaign = ORM::factory('campaign')
            ->where('id', '=', $campaign_id)
            ->find();

		$campaign_items = DB::select(
      	array('hotels.currency_id', 'currency_id'),
	      array('hotels.service_charge_rate', 'hotel_service_charge_rate'),
	      array('hotels.tax_rate', 'hotel_tax_rate'),
      	array('hotels.timezone', 'hotel_timezone'),
				array('rooms.id', 'InvTypeCode'),
				array('rooms.publish_price', 'publish_price'),
	      array('rooms.adult_breakfast_price', 'adult_breakfast_price'),
	      array('rooms.publish_price', 'room_publish_price'),
	      array('rooms.is_breakfast_included', 'is_breakfast_included'),
	      array('rooms.number_of_persons_breakfast_included', 'number_of_persons_breakfast_included'),
	      array('rooms.is_extrabed_get_breakfast', 'is_extrabed_get_breakfast'),
      	array('room_texts.name', 'room_name'),
      	array('room_capacities.number_of_adults', 'number_of_adults'),
      	array('room_capacities.id', 'room_capacities_id'),
      	array('room_capacities.number_of_children', 'number_of_children'),
      	array('room_capacities.number_of_extrabeds', 'number_of_extrabeds'),
      	array('campaigns.id', 'RatePlanCode'),
      	array('campaigns.minimum_number_of_nights', 'minimum_number_of_nights'),
      	array('campaigns.minimum_number_of_rooms', 'minimum_number_of_rooms'),
      	array('campaigns.days_in_advance', 'days_in_advance'),
      	array('campaigns.within_days_of_arrival', 'within_days_of_arrival'),
      	array('campaigns.booking_start_date', 'booking_start_date'),
      	array('campaigns.booking_end_date', 'booking_end_date'),
      	array('campaigns.is_stay_sunday_active', 'sun'),
      	array('campaigns.is_stay_monday_active', 'mon'),
      	array('campaigns.is_stay_tuesday_active', 'tue'),
      	array('campaigns.is_stay_wednesday_active', 'wed'),
      	array('campaigns.is_stay_thursday_active', 'thu'),
      	array('campaigns.is_stay_friday_active', 'fri'),
      	array('campaigns.is_stay_saturday_active', 'sat'),
	      array('campaigns.discount_rate', 'rate'),
	      array('campaigns.discount_amount', 'amount'),
	      /// extrabed item price
      	array(DB::expr("IF (items.extrabed_item_price = 0, rooms.extrabed_price, items.extrabed_item_price)"), 'extrabed_price'),
      	array('rooms.maximum_number_of_extrabeds', 'maximum_number_of_extrabeds'),
				array('items.id', 'item_id'),
				array('items.date', 'date'),
				array('items.stock', 'stock'),
				array('items.price', 'price'),
				array('items.is_blackout', 'blackout'),
				array(DB::expr("
					CASE
						WHEN DAYOFWEEK(date) = 1 THEN IF(campaigns.is_stay_sunday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 2 THEN IF(campaigns.is_stay_monday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 3 THEN IF(campaigns.is_stay_tuesday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 4 THEN IF(campaigns.is_stay_wednesday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 5 THEN IF(campaigns.is_stay_thursday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 6 THEN IF(campaigns.is_stay_friday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 7 THEN IF(campaigns.is_stay_saturday_active, 0, 1)
						ELSE 0
					END
				"), 'weekout'),
				array('items.is_campaign_blackout', 'campaign_blackout'),
				array(DB::expr("
					CASE
						WHEN campaigns.days_in_advance THEN
							IF(
								DATEDIFF(items.date, CONVERT_TZ('$now','+00:00',timezones.offset)) >= campaigns.days_in_advance,
								'1',
								'0'
							)
						WHEN campaigns.within_days_of_arrival THEN
							IF(
								DATEDIFF(items.date, CONVERT_TZ('$now','+00:00',timezones.offset)) < campaigns.within_days_of_arrival,
								'1',
								'0'
							)
						ELSE
							'1'
					END
				"), 'promotion'),
				'campaigns.discount_rate',
				'campaigns.discount_amount',
				array(DB::expr("
					IF(items.date > DATE_ADD('$final_date',INTERVAL -campaigns.number_of_free_nights DAY),
						'1',
						'0'
					)
				"), 'free_night'),
				array('campaigns.id', 'campaign_id')
			)
      ->from('hotels')
      ->join('hotel_texts')->on('hotel_texts.hotel_id', '=', 'hotels.id')
      ->join('hotel_photos')->on('hotels.hotel_photo_id', '=', 'hotel_photos.id')
      ->join('rooms')->on('hotels.id', '=', 'rooms.hotel_id')
      ->join('campaigns_rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->join('campaigns')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('items')->on('rooms.id', '=', 'items.room_id')
      ->join('timezones')->on('hotels.timezone', '=', 'timezones.identifier')
      ->join('campaigns_languages')->on('campaigns.id', '=', 'campaigns_languages.campaign_id')
			->join('room_capacities')->on('room_capacities.room_id', '=', 'rooms.id')
			->join('room_texts', 'LEFT')->on('room_texts.room_id', '=', 'rooms.id')
			->join('currencies')->on('currencies.id', '=', 'hotels.currency_id')

			->where('hotels.active', '=', 1)
			->where('campaigns.id', '=', $campaign_id)
			->where('rooms.id', '=', $room_id)

			//->where('items.campaign_id', '=', DB::expr("IF (items.campaign_id != 0, $campaign_id, 0)"))
			->where('items.campaign_id', '=', 0)
			->where('items.date', '=', $check_in)
			//->where('items.date', '<=', Date::formatted_time("$check_out -1days", 'Y-m-d'))
			->order_by('campaign_id', 'ASC')
			->execute()
			->as_array('date');
      
		return $campaign_items;
	}

	public function get_items($campaign_id, $query, $ignore_date = FALSE, $from_vacation = FALSE)
	{
		$language_id = Arr::get($query, 'language_id');
		$check_in = Arr::get($query, 'check_in');
		$check_out = Arr::get($query, 'check_in') == Arr::get($query, 'check_out') ? Date::formatted_time("$check_in + 1 days", 'Y-m-d') : Arr::get($query, 'check_out');
		$room_id = Arr::get($query, 'InvTypeCode');
		$number_of_rooms = Arr::get($query, 'number_of_rooms');
		$now = Arr::get($query, 'now');
		$hotel_active_vacation = Arr::get($query, 'hotel_active_vacation');
		$now_advanced = Date::formatted_time("$now -1 day", 'Y-m-d');
		$final_date = Date::formatted_time("$check_out -1 day", 'Y-m-d');

		$campaign_items = DB::select(
				array('campaigns.id', 'campaign_id'),
				array('rooms.id', 'room_id'),
				array('items.id', 'item_id'),
				array('campaigns.minimum_number_of_nights', 'campaigns_minimum_night'),
				array('items.date', 'date'),
				array('items.stock', 'stock'),
				array('items.price', 'price'),
				array('items.extrabed_item_price', 'extrabed_item_price'),
				array('items.is_blackout', 'blackout'),
				array('items.is_campaign_prices', 'is_campaign_prices'),
				array('items.minimum_night', 'minimum_night'),
				array(DB::expr("
					CASE
						WHEN DAYOFWEEK(date) = 1 THEN IF(campaigns.is_stay_sunday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 2 THEN IF(campaigns.is_stay_monday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 3 THEN IF(campaigns.is_stay_tuesday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 4 THEN IF(campaigns.is_stay_wednesday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 5 THEN IF(campaigns.is_stay_thursday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 6 THEN IF(campaigns.is_stay_friday_active, 0, 1)
						WHEN DAYOFWEEK(date) = 7 THEN IF(campaigns.is_stay_saturday_active, 0, 1)
						ELSE 0
					END
				"), 'weekout'),
				array('items.is_campaign_blackout', 'campaign_blackout'),
				array(DB::expr("
					CASE
						WHEN campaigns.days_in_advance THEN
							IF(
								DATEDIFF(items.date, CONVERT_TZ('$now_advanced','+00:00',timezones.offset)) >= campaigns.days_in_advance,
								'1',
								'0'
							)
						WHEN campaigns.within_days_of_arrival THEN
							IF(
								DATEDIFF(items.date, CONVERT_TZ('$now','+00:00',timezones.offset)) < campaigns.within_days_of_arrival,
								'1',
								'0'
							)
						ELSE
							'1'
					END
				"), 'promotion'),
				'campaigns.discount_rate',
				'campaigns.discount_amount',
				array(DB::expr("
					IF(items.date > DATE_ADD('$final_date',INTERVAL -campaigns.number_of_free_nights DAY),
						'1',
						'0'
					)
				"), 'free_night')
			)
			->from('campaigns')
			->join('campaigns_rooms')->on('campaigns.id', '=', 'campaigns_rooms.campaign_id')
			->join('rooms')->on('campaigns_rooms.room_id', '=', 'rooms.id')
			->join('hotels')->on('rooms.hotel_id', '=', 'hotels.id')
			->join('timezones')->on('hotels.timezone', '=', 'timezones.identifier')
			->join('items')->on('campaigns_rooms.room_id', '=', 'items.room_id');

			if($hotel_active_vacation)
			{
				$campaign_items = $campaign_items
				->where('hotels.active', '=', 0);		
			}
			else
			{
				$campaign_items = $campaign_items
				->where('hotels.active', '=', 1);
			}

			$campaign_items = $campaign_items
				->where('campaigns.id', '=', $campaign_id)
			->where('rooms.id', '=', $room_id);
			
			/// Campaign prices
			// ->where('items.campaign_id', '=', DB::expr("IF (items.campaign_id = $campaign_id, $campaign_id, 0)"))

			//if campaign not valid dont query by booking_end_date
			if($query['campaign_valid'])
			{
				$campaign_items = $campaign_items
	      // Filter booking date
	      ->where_open()
	      // ->and_where('campaigns.booking_start_date', '<=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
	      ->and_where('campaigns.booking_end_date', '>=', DB::expr("DATE_FORMAT(CONVERT_TZ('$now','+00:00',timezones.offset), '%Y-%m-%d')"))
	      ->where_close();
			}


        // Filter stay date
        // Can be ignored for Vacation Rate Amount Notification
        if (!$ignore_date)
        {
            $campaign_items = $campaign_items
                ->where_open()
                ->and_where('campaigns.stay_start_date', '<=', $check_in)
                ->and_where('campaigns.stay_end_date', '>=', $final_date)
                ->where_close();
        }

        $campaign_items = $campaign_items

            ->where('items.campaign_id', '=', 0)

			->where('items.date', '>=', $check_in)
			->where('items.date', '<=', Date::formatted_time("$check_out -1days", 'Y-m-d'))

			->order_by('campaign_id', ASC);

		if($from_vacation==TRUE)
		{
			$campaign_items = $campaign_items
				->group_by('campaign_id')
				->group_by('room_id');
		}
		else
		{
			$campaign_items = $campaign_items;
		}

		$campaign_items = $campaign_items
			->execute()
			->as_array('date');

		return $campaign_items;
	}


	public function getAllCampaignsHotel($hotel_id, $optParam = array())
	{
		$campaigns = DB::select('campaigns.*')
		->from('campaigns')
		->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
		->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
		->where('rooms.hotel_id', '=', $hotel_id);

		if($optParam['call'] == 'promotion_page'){
			$campaigns = $campaigns
			->where('campaigns.type', 'IN', $optParam['type'])
			->where('campaigns.booking_end_date','>', date('Y-m-d'))
			->where('campaigns.is_default', '=', 0)
			;
		}


		if($optParam['call'] == 'promotion_page'){
			$campaigns = $campaigns
			->group_by('campaigns.id');
		}

		$campaigns = $campaigns
		->execute()
		->as_array()

		;
		
		return $campaigns;
	}

	public function ConvertCurrencyCampaignBenifit($campaigns, $hotel, $values)
	{
		CustomLog::factory()->add(3, 'DEBUG', 'ConvertCurrencyCampaignBenifit start');
		foreach ($campaigns as $ck => $campaign) 
		{

			//convert discount amount
			$discount_amount = round($campaign->discount_amount * Model::factory('exchange')->to($hotel->currency_id, $values['currency_id']));

			//set new discount_amount
			$campaign->discount_amount = $discount_amount;
			//save campaign
			$campaign->save();
		}
		CustomLog::factory()->add(3, 'DEBUG', 'ConvertCurrencyCampaignBenifit finished');
	}
}