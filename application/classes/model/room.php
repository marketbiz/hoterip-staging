<?php defined('SYSPATH') or die('No direct script access.');

class Model_Room extends ORM implements Acl_Resource_Interface {
  
  protected $_has_many = array(
    'campaigns' => array(
      'through' => 'campaigns_rooms',
    ),
    'texts' => array(
      'model' => 'room_text',
    ),
  );
  
  public function get_resource_id()
	{
		return 'room';
	}
	
	public function rules()
	{
		return array(
			'publish_price' => array(
				array('not_empty'),
				array('digit'),
			),
			'adult_breakfast_price' => array(
				array('not_empty'),
				array('numeric'),
			),
			'child_breakfast_price' => array(
				array('not_empty'),
				array('numeric'),
			),
			'extrabed_price' => array(
				array('not_empty'),
				array('numeric'),
			),
      'maximum_number_of_extrabeds' => array(
				array('digit'),
			),
		);
	}
	
	public function labels()
	{
		return array(
			'publish_price' => 'Publish Price',
			'adult_breakfast_price' => 'Adult Breakfast Price',
			'child_breakfast_price' => 'Child Breakfast Price',
			'extrabed_price' => 'Extra Bed Price',
			'maximum_number_of_extrabeds' => 'Maximum Number of Extra Beds',
		);
	}
	
	public function create_room($values)
	{
    // Create room
    $this->values($values, array(
      'hotel_id',
      'hotel_photo_id',
      'publish_price',
      'adult_breakfast_price',
      'child_breakfast_price',
      'is_breakfast_included',
      'number_of_persons_breakfast_included',
      'extrabed_price',
      'maximum_number_of_extrabeds',
      'is_extrabed_get_breakfast',
      'number_of_beds',
      'size',
      'cutoff_days',
      'hoterip_campaign',
      ))
      ->create();

    // Create room texts

    // Get languages
    $languages = ORM::factory('language')->find_all();

    // Get field value from post array
    $names = array_filter(Arr::get($values, 'names', array()));

    // Set default language id using english
    $default_language_id = ORM::factory('language')
      ->where('code', '=', 'en-us')
      ->find()
      ->id;

    // Set default text value
    $default_name = Arr::get($names, $default_language_id, '');

    $room_texts = array();

    // Loop each language
    foreach ($languages as $language)
    {
      // Get text and set it's values
      $room_texts[] = ORM::factory('room_text')
        ->set('room_id', $this->id)
        ->values(array(
          'language_id' => $language->id,
          'name' => Arr::get($names, $language->id, $default_name),
        ))
        ->create();
    }

    // Create room facility
    ORM::factory('room_facility')
      ->set('room_id', $this->id)
      ->values(Arr::extract($values, array(
        'is_air_conditioner_available',
        'is_alarm_clock_available',
        'is_balcony_available',
        'is_bathrobe_available',
        'is_bathtub_available',
        'is_body_lotion_available',
        'is_cable_tv_available',
        'is_coffee_maker_available',
				'is_complimentary_water_bottle_available',
        'is_cotton_bud_available',
        'is_dvd_player_available',
				'is_fan_available',
        'is_hair_dryer_available',
        'is_idd_telephone_available',
        'is_independent_shower_room_available',
        'is_safety_box_available',
        'is_internet_jack_available',
        'is_iron_available',
        'is_jacuzzi_available',
        'is_kitchen_set_available',
        'is_living_room_available',
        'is_mini_bar_available',
        'is_mosquito_equipment_available',
        'is_moveable_shower_head_available',
        'is_radio_available',
        'is_room_wear_available',
        'is_shampoo_available',
        'is_shaver_available',
        'is_soap_available',
        'is_toothbrush_available',
        'is_towel_available',
        'is_tv_available',
      ), 0))
      ->create();

    // Get room capacities data
    $indexs = array_keys(Arr::get($values, 'indexs', array()));
    $number_of_adults = Arr::get($values, 'number_of_adults', array());
    $number_of_children = Arr::get($values, 'number_of_children', array());
    $number_of_extrabeds = Arr::get($values, 'number_of_extrabeds', array());

    foreach ($indexs as $index)
    {
      // Create room capacity
      ORM::factory('room_capacity')
        ->set('room_id', $this->id)
        ->values(array(
          'number_of_adults' => Arr::get($number_of_adults, $index, 1),
          'number_of_children' => Arr::get($number_of_children, $index, 0),
          'number_of_extrabeds' => Arr::get($number_of_extrabeds, $index, 0),
        ))
        ->create();
    }
      
		return $this;
	}
	
	public function update_room($values)
	{
    // Update room
    $this->values($values, array(
      'hotel_id',
      'hotel_photo_id',
      'publish_price',
      'adult_breakfast_price',
      'child_breakfast_price',
      'is_breakfast_included',
      'number_of_persons_breakfast_included',
      'extrabed_price',
      'maximum_number_of_extrabeds',
      'is_extrabed_get_breakfast',
      'number_of_beds',
      'size',
      'cutoff_days',
      'hoterip_campaign',
      ))
      ->update();

    // Update room texts

    // Get languages
    $languages = ORM::factory('language')->find_all();

    // Get field value from post array
    $names = array_filter(Arr::get($values, 'names', array()));

    // Set default language id using english
    $default_language_id = ORM::factory('language')
      ->where('code', '=', 'en-us')
      ->find()
      ->id;

    // Set default text value
    $default_name = Arr::get($names, $default_language_id, '');

    $room_texts = array();

    // Loop each language
    foreach ($languages as $language)
    {
      // Get text and set it's values
      $room_texts[] = ORM::factory('room_text')
        ->where('room_id', '=', $this->id)
        ->where('language_id', '=', $language->id)
        ->find()
        ->values(array(
          'room_id' => $this->id,
          'language_id' => $language->id,
          'name' => Arr::get($names, $language->id, $default_name),
        ))
        ->save();
    }

    // Update room facility
    ORM::factory('room_facility')
      ->where('room_id', '=', $this->id)
      ->find()
      ->values(Arr::extract($values, array(
        'is_air_conditioner_available',
        'is_alarm_clock_available',
        'is_balcony_available',
        'is_bathrobe_available',
        'is_bathtub_available',
        'is_body_lotion_available',
        'is_cable_tv_available',
        'is_coffee_maker_available',
        'is_cotton_bud_available',
        'is_dvd_player_available',
        'is_hair_dryer_available',
        'is_idd_telephone_available',
        'is_independent_shower_room_available',
        'is_safety_box_available',
        'is_internet_jack_available',
        'is_iron_available',
        'is_jacuzzi_available',
        'is_kitchen_set_available',
        'is_living_room_available',
        'is_mini_bar_available',
        'is_mosquito_equipment_available',
        'is_moveable_shower_head_available',
        'is_radio_available',
        'is_room_wear_available',
        'is_shampoo_available',
        'is_shaver_available',
        'is_soap_available',
        'is_toothbrush_available',
        'is_towel_available',
        'is_tv_available',
      ), 0))
      ->update();

    // Get room capacities data
    $indexs = array_keys(Arr::get($values, 'indexs', array()));
    $number_of_adults = Arr::get($values, 'number_of_adults', array());
    $number_of_children = Arr::get($values, 'number_of_children', array());
    $number_of_extrabeds = Arr::get($values, 'number_of_extrabeds', array());

    // Delete room capacities first
    DB::delete('room_capacities')
      ->where('room_id', '=', $this->id)
      ->execute();

    foreach ($indexs as $index)
    {
      // Create capacity
      $capacity = ORM::factory('room_capacity')
        ->values(array(
          'room_id' => $this->id,
          'number_of_adults' => Arr::get($number_of_adults, $index, 1),
          'number_of_children' => Arr::get($number_of_children, $index, 0),
          'number_of_extrabeds' => Arr::get($number_of_extrabeds, $index, 0),
        ))
        ->create();
    }
    
    return $this;
	}

  public function get_by_id($language_id, $room_id)
  {
    return DB::select()
      ->from('rooms')
      ->join('room_texts')->on('rooms.id', '=', 'room_texts.room_id')
      ->where('rooms.id', '=', $room_id)
      ->where('room_texts.language_id', '=', $language_id)
      ->as_object()
      ->execute()
      ->current();
  }

  public function get_by_hotel_id($language_id, $hotel_id)
  {
    return DB::select()
      ->from('rooms')
      ->join('room_texts')->on('rooms.id', '=', 'room_texts.room_id')
      ->where('rooms.hotel_id', '=', $hotel_id)
      ->where('room_texts.language_id', '=', $language_id)
      ->as_object()
      ->execute();
      ;
  }

  public function resetCurrencyRooms($rooms)
  {
    CustomLog::factory()->add(3, 'DEBUG', 'resetCurrencyRooms start');
    //reset price room one by one
    foreach ($rooms as $room_k => $room) {
      $room->publish_price = 0;
      $room->adult_breakfast_price = 0;
      $room->child_breakfast_price = 0;
      $room->extrabed_price = 0;
      $room->save();
    }
    CustomLog::factory()->add(3, 'DEBUG', 'resetCurrencyRooms Finished');
  }
}