<?php defined('SYSPATH') or die('No direct script access.');

class Model_Exchange extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'exchange';
	}

	public function to($from, $to)
	{

		$exchange = DB::select()
			->from('exchanges')
			->where('from_currency_id', '=', $from)
			->where('to_currency_id', '=', $to)
			->as_object()
			->execute()
			->current();

		$result = $exchange->rate / $exchange->amount;

		return $result;
	}
	
}