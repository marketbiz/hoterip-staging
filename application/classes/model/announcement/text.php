<?php defined('SYSPATH') or die('No direct script access.');

class Model_Announcement_Text extends ORM {

	public function rules()
	{
		return array(
			'description' => array(
				array('not_empty'),
				array('max_length', array(':value', 255)),
			),
		);
	}
	
	public function labels()
	{
		return array(
			'description' => 'Announcement Description',
		);
	}
	
}