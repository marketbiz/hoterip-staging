<?php defined('SYSPATH') or die('No direct script access.');

class Model_State extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'state';
	}
	
	public function create_state($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('state')
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Create state
			$this->values($values, array(
					'country_id',
					'url_segment',
				))
				->create();
			
			// Create state texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$state_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$state_texts[] = ORM::factory('state_text')
					->set('state_id', $this->id)
					->values(array(
						'language_id' => $language->id,
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->create();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
	public function update_state($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('state')
					->where('id', '<>', $this->id)
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Update state
			$this->values($values, array(
					'country_id',
					'url_segment',
				))
				->update();
			
			// Update state texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$state_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$state_texts[] = ORM::factory('state_text')
					->where('state_id', '=', $this->id)
					->where('language_id', '=', $language->id)
					->find()
					->values(array(
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->update();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
}