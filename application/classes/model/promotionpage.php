<?php defined('SYSPATH') or die('No direct script access.');

class Model_PromotionPage extends ORM implements Acl_Resource_Interface{

	public function rules()
	{
		return array(
			'hotel_id' => array(
				array('not_empty'),
				array('digit'),
			),
			'campaign_id' => array(
				array('not_empty'),
				array('digit'),
			),
			'room_id' => array(
				array('not_empty'),
				array('digit'),
			),
      'type' => array(
				array('digit'),
			),
		);
	}

	public function labels()
	{
		return array(
			'number_list' => 'Number List',
			'hotel_id' => 'Hotel ID',
			'campaign_id' => 'Promotion ID',
			'room_id' => 'Room ID',
			'type' => 'Type Campaign',
		);
	}

  public function get_resource_id()
	{
		return 'promotionpage';
	}


	public function create_promotionpage($values)
	{
    // Create promotionpage
    $this->values($values, array(
    	'hotel_id',
    	'campaign_id',
    	'room_id',
    	'type'
      ))
      ->create();

     return $this;
	}

	public function update_promotionpage($values)
	{
    // Create promotionpage
    $this->values($values, array(
    	'hotel_id',
    	'campaign_id',
    	'room_id',
    	'type'
      ))
      ->update();

     return $this;
	}

}