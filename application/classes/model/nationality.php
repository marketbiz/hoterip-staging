<?php defined('SYSPATH') or die('No direct script access.');

class Model_Nationality extends Model {

	public function get_nationalities() {

		return DB::select()
			->from('nationalities')
      ->order_by('name')
			->as_object()
			->execute();

	}

	public function get_nationality_name($nationality_id) {

		return DB::select()
			->from('nationalities')
			->where('id', '=', $nationality_id)
			->execute()
			->get('name');
	}

	public static function validate_nationality_exist($nationality_id)
	{
		return (bool) DB::select()
			->from('nationalities')
			->where('id', '=', $nationality_id)
			->execute()
			->count();
	}

}