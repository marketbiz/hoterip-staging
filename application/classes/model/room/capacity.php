<?php defined('SYSPATH') or die('No direct script access.');

class Model_Room_Capacity extends ORM {
	public function get_room_capacities($room_id)
	{
		return DB::select()
			->from('room_capacities')
			->where('room_capacities.room_id', '=', $room_id)
			->execute()
			->as_array();
	}

	public function check_room_capacity($room_id, $capacities)
	{
		$count = 0;

		foreach ($capacities as $capacity)
		{
			$count += (bool) DB::select()
			->from('room_capacities')
			->where('room_capacities.room_id', '=', $room_id)
			->where_open()
				->where('number_of_adults', '=', $capacity['adult'])
				->where('number_of_children', '=', $capacity['child'])
			->where_close()
			->execute()
			->count();
		}

		return ($count == count($capacities));
	}

	public function get_extrabed($room_id, $capacity)
	{
		return DB::select()
			->from('room_capacities')
			->where('room_capacities.room_id', '=', $room_id)
			->where_open()
				->where('number_of_adults', '=', $capacity['adult'])
				->where('number_of_children', '=', $capacity['child'])
			->where_close()
			->execute()
			->get('number_of_extrabeds');
	}
}