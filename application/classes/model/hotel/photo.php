<?php defined('SYSPATH') or die('No direct script access.');

class Model_Hotel_Photo extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'hotel_photo';
	}
	
	public function create_hotel_photo($values)
	{
    // Create hotel photo
    $this->values($values, array(
      'hotel_id',
      'basename',
      'small_basename',
      'medium_basename',
      'large_basename',
      ))
      ->create();

    // Create hotel photo texts

    // Get languages
    $languages = ORM::factory('language')->find_all();

    // Get field value from post array
    $names = array_filter(Arr::get($values, 'names', array()));

    // Set default language id using english
    $default_language_id = ORM::factory('language')
      ->where('code', '=', 'en-us')
      ->find()
      ->id;

    // Set default text value
    $default_name = Arr::get($names, $default_language_id, '');

    // Loop each language
    foreach ($languages as $language)
    {
      // Get text and set it's values
      ORM::factory('hotel_photo_text')
        ->values(array(
          'hotel_photo_id' => $this->id,
          'language_id' => $language->id,
          'name' => Arr::get($names, $language->id, $default_name),
        ))
        ->create();
    }
		
		return $this;
	}
	
	public function update_hotel_photo($values)
	{
    // Update hotel
    $this->values($values, array(
      'hotel_id',
      'basename',
      'small_basename',
			'medium_basename',
      'large_basename',
      ))
      ->update();

    // Update hotel texts

    // Get languages
    $languages = ORM::factory('language')->find_all();

    // Get field value from post array
    $names = array_filter(Arr::get($values, 'names', array()));

    // Set default language id using english
    $default_language_id = ORM::factory('language')
      ->where('code', '=', 'en-us')
      ->find()
      ->id;

    // Set default text value
    $default_name = Arr::get($names, $default_language_id, '');

    // Loop each language
    foreach ($languages as $language)
    {
      // Get text and set it's values
      ORM::factory('hotel_photo_text')
        ->where('hotel_photo_id', '=', $this->id)
        ->where('language_id', '=', $language->id)
        ->find()
        ->values(array(
          'name' => Arr::get($names, $language->id, $default_name),
        ))
        ->update();
    }
		
		return $this;
	}
	
}