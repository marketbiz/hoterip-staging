<?php

defined('SYSPATH') or die('No direct access allowed.');

class Model_User extends ORM implements Acl_Resource_Interface {

    protected $_has_many = array(
        'hotels' => array(
            'through' => 'agent_hotels'
        ),
    );

    public function get_resource_id() {
        return 'user';
    }

    private function table_agent() {
        /*
         * untuk backup script install tabel agen hotel
         * copy paste di phpmyadmin 
         */
        $query = "CREATE TABLE IF NOT EXISTS `agent_hotels` (
  `user_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`hotel_id`),
  KEY `fk_admin_id` (`user_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    }

}
