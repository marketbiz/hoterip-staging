<?php

defined('SYSPATH') or die('No direct access allowed.');

class Model_Agent extends Model_A1_User_ORM implements Acl_Role_Interface {

    protected $_has_many = array(
        'hotels' => array(
            'through' => 'agent_hotels'
        ),
    );

    public function get_role_id() {
        return $this->role;
    }

}
