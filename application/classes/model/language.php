<?php defined('SYSPATH') or die('No direct script access.');

class Model_Language extends ORM {

	public function get_language_by_id($id)
	{
		return DB::select()
			->from('languages')
			->where('id', '=', $id)
			->as_object()
			->execute()
			->current();
	}
}