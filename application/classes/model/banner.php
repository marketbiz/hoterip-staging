<?php defined('SYSPATH') or die('No direct script access.');

class Model_Banner extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'banner';
	}
  
  public function create_banner($values)
  {
    $this->values($values, array(
      'language_id',
      'title',
      'url',
      'order',
    ))->create();
    
    return $this;
  }
  
  public function update_banner($values)
  {
    $this->values($values, array(
      'language_id',
      'title',
      'url',
      'order',
    ))->update();
    
    return $this;
  }
}