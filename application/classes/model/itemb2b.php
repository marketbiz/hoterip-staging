<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Itemb2b extends ORM implements Acl_Resource_Interface {
    public function get_resource_id() {
        return 'itemb2b';
    }

    public static function valid_price($price, $date, $room_id) {
        $result = TRUE;

        $promotions = ORM::factory('campaign')
                ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
                ->where('campaigns_rooms.room_id', '=', $room_id)
                ->where('campaigns.stay_start_date', '<=', $date)
                ->where('campaigns.stay_end_date', '>=', $date)
                ->where('campaigns.is_default', '=', 0)
                ->find_all();

        foreach ($promotions as $promotion) {
            if ($price - $promotion->discount_amount <= 0) {
                $result = FALSE;
                break;
            }
        }

        return $result;
    }

    public function decrease_item_stock($item_id, $amount_stock) {
        $current_stock = DB::select()
                ->from('itemb2bs')
                ->where('id', '=', $item_id)
                ->execute()
                ->get('stock');

        $stock = $current_stock - $amount_stock;

        return DB::update('itemb2bs')
                        ->set(
                                array('stock' => $stock)
                        )
                        ->where('id', '=', $item_id)
                        ->execute();
    }

    public function increase_item_stock($item_id, $amount_stock) {
        $current_stock = DB::select()
                ->from('itemb2bs')
                ->where('id', '=', $item_id)
                ->execute()
                ->get('stock');

        $stock = $current_stock + $amount_stock;

        return DB::update('itemb2bs')
                        ->set(
                                array('stock' => $stock)
                        )
                        ->where('id', '=', $item_id)
                        ->execute();
    }

    public function increase_item_sold(array $item_ids, $amount_sold) {
        return DB::update('itemb2bs')
                        ->set(
                                array('sold' => DB::expr("sold + $amount_sold"))
                        )
                        ->where('id', 'IN', $item_ids)
                        ->execute();
    }

    public function get_items_by_item_id($item_ids) {
        return DB::select()
                        ->from('itemb2bs')
                        ->where('id', 'in', $item_ids)
                        ->as_object()
                        ->execute()
                        ->as_array();
    }

}
