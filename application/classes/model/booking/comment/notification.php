<?php defined('SYSPATH') or die('No direct script access.');

class Model_Booking_Comment_Notification extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'booking_comment_notification';
	}
	
}