<?php defined('SYSPATH') or die('No direct script access.');

class Model_Booking_Room extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'booking_room';
	}

	public function create_booking_room($room_array)
	{
		return DB::insert(
				'booking_rooms',
				array_keys($room_array)
			)
			->values(array_values($room_array))
			->execute();
	}
}