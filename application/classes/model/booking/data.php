<?php defined('SYSPATH') or die('No direct script access.');

class Model_Booking_Data extends Model {

	public function create_booking_data($booking_array)
	{
		return DB::insert(
				'booking_datas',
				array_keys($booking_array)
			)
			->values(array_values($booking_array))
			->execute();
	}

	public function get_booking_data($booking_id)
	{
		return DB::select()
			->from('booking_datas')
			->where('booking_id', '=', $booking_id)
			->as_object()
			->execute()
			->current();
	}

}