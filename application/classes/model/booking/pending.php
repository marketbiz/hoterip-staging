<?php defined('SYSPATH') or die('No direct script access.');

class Model_Booking_Pending extends Model {

	public function get_booking_pending_by_key($booking_key)
	{
		return DB::select()
			->from('booking_pendings')
			->where('key', '=', $booking_key)
			->as_object()
			->execute()
			->current();
	}

	public function get_booking_pending_by_id($booking_id)
	{
		return DB::select()
			->from('booking_pendings')
			->where('id', '=', $booking_id)
			->as_object()
			->execute()
			->current();
	}

	public function create_booking_pending($booking_array)
	{
		return DB::insert(
				'booking_pendings',
				array_keys($booking_array)
			)
			->values(array_values($booking_array))
			->execute();
	}

	public function delete_booking_pending($booking_id)
	{
		return DB::delete('booking_pendings')
			->where('id', '=', $booking_id)
			->execute();
	}

	public function delete_booking_pending_by_key($key)
	{
		return DB::delete('booking_pendings')
			->where('key', '=', $key)
			->execute();
	}

	public function update_booking_pending($booking_pending_key, $booking_pending_array)
	{
		return DB::update('booking_pendings')
			->set($booking_pending_array)
			->where('key', '=', $booking_pending_key)
			->execute();
	}

}