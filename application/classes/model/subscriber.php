<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Subscriber extends ORM implements Acl_Resource_Interface {
	
  public function get_resource_id()
	{
		return 'email_subscribe';
	}
  
} 