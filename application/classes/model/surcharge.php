<?php defined('SYSPATH') or die('No direct script access.');

class Model_Surcharge extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'surcharge';
	}
	
	public function get_surcharges($hotel_id, $check_in, $check_out)
	{
		return DB::select()
			->from('surcharges')
			->join('surcharge_descriptions')->on('surcharges.surcharge_description_id', '=', 'surcharge_descriptions.id')
			->where('hotel_id', '=',  $hotel_id)
			->where('date', '>=',  $check_in)
			->where('date', '<',  $check_out)
			->execute()
			->as_array();
	}

	public function resetCurrencySurcharges($surcharges)
	{
		CustomLog::factory()->add(3, 'DEBUG', 'resetCurrencySurcharges start');
		foreach ($surcharges as $surcharge_k => $surcharge) {
			$surcharge->delete();
		}
		CustomLog::factory()->add(3, 'DEBUG', 'resetCurrencySurcharges finished');
	}
}