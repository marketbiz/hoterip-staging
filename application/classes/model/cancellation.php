<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cancellation extends ORM implements Acl_Resource_Interface {
  
  protected $_has_many = array(
    'campaigns' => array(
      'through' => 'campaigns_cancellations',
    ),
  );
  
  public function rules()
	{
		return array(
			'start_date' => array(
				array('not_empty'),
//				array('date'),
			),
			'end_date' => array(
				array('not_empty'),
//        array('date'),
			),
		);
	}
	
	public function get_resource_id()
	{
		return 'cancellation';
	}
	
  public function create_cancellation($values, $extra_validation = NULL)
  {
    $this->values($values, array(
      'level_1_cancellation_rule_id',
      'level_2_cancellation_rule_id',
      'level_3_cancellation_rule_id',
      'start_date',		 	 	 	 	 	 	
      'end_date',
    ))->create($extra_validation);
    
    return $this;
  }
  
  public function update_cancellation($values, $extra_validation = NULL)
  {
    $this->values($values, array(
      'level_1_cancellation_rule_id',
      'level_2_cancellation_rule_id',
      'level_3_cancellation_rule_id',
      'start_date',		 	 	 	 	 	 	
      'end_date',
    ))->update($extra_validation);
    
    return $this;
  }
  
  public static function valid_date($date, $room_id)
  {
    return ORM::factory('cancellation')
      ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->where('campaigns_rooms.room_id', '=', $room_id)
      ->where('cancellations.start_date', '<=', $date)
      ->where('cancellations.end_date', '>=', $date)
      ->where('campaigns.is_default', '=', 1)
      ->count_all() > 0;
  }
  
  public static function valid_date_range($start_date, $end_date, $room_id)
  {
    return ORM::factory('cancellation')
      ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->where('campaigns_rooms.room_id', '=', $room_id)
      ->where('cancellations.start_date', '<=', $start_date)
      ->where('cancellations.end_date', '>=', $end_date)
      ->where('campaigns.is_default', '=', 1)
      ->count_all() > 0;
  }
  
  public static function valid_cancellation_date($start_date, $end_date, $room_ids, $cancellation_id = NULL)
  {
    $cancellations = ORM::factory('cancellation')
      ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->where('campaigns_rooms.room_id', 'IN', $room_ids)
      ->where('cancellations.end_date', '>=', $start_date)
      ->where('cancellations.start_date', '<=', $end_date)
      ->where('campaigns.is_default', '=', 1);
    
    if ($cancellation_id)
    {
      $cancellations = $cancellations->where('cancellations.id', '<>', $cancellation_id);
    }
    
    return $cancellations->count_all() == 0;
  }

  public function get_cancellation($campaign_id, $check_in, $check_out)
  {
    return DB::select(
        'cancellations.*',
        array('rule_1.name', 'rule_1_name'),
        array('rule_2.name', 'rule_2_name'),
        array('rule_3.name', 'rule_3_name')
      )
      ->from('campaigns_cancellations')
      ->join('cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->join(array('cancellation_rules', 'rule_1'))->on('rule_1.id', '=', 'cancellations.level_1_cancellation_rule_id')
      ->join(array('cancellation_rules', 'rule_2'), 'left')->on('rule_2.id', '=', 'cancellations.level_2_cancellation_rule_id')
      ->join(array('cancellation_rules', 'rule_3'), 'left')->on('rule_3.id', '=', 'cancellations.level_3_cancellation_rule_id')
      ->where('campaigns_cancellations.campaign_id', '=', $campaign_id)
      ->where('cancellations.start_date', '<=', $check_in)
      ->where('cancellations.end_date', '>=', $check_out)
      ->execute()
      ->current();
  }
}