<?php defined('SYSPATH') or die('No direct script access.');

class Model_Package_Photo extends ORM implements Acl_Resource_Interface {
  
  public function get_resource_id() 
  {
    return 'package_photo';
  }
  
}