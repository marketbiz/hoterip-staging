<?php defined('SYSPATH') or die('No direct script access.');

class Model_Package_Booking_Unread_Comment extends ORM implements Acl_Resource_Interface {
  
  public function get_resource_id() 
  {
    return 'package_booking_unread_comment';
  }
  
}