<?php defined('SYSPATH') or die('No direct script access.');

class Model_Benefit_Text extends ORM {

	public function rules()
	{
		return array(
			'name' => array(
				array('not_empty'),
			),
		);
	}
	
	public function labels()
	{
		return array(
			'name' => 'Benefit Name',
		);
	}
	
}