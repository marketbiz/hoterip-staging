<?php defined('SYSPATH') or die('No direct script access.');

class Model_Night extends ORM implements Acl_Resource_Interface {
  
  protected $_has_many = array(
    'rooms' => array(
      'through' => 'nights_rooms',
    ),
  );
  
  public function get_resource_id()
	{
		return 'night';
	}
  
  public function rules()
	{
		return array(
			'start_date' => array(
				array('not_empty'),
        array('date'),
			),
      'end_date' => array(
				array('not_empty'),
        array('date'),
			),
      'minimum_number_of_nights' => array(
				array('not_empty'),
        array('digit'),
			),
		);
	}
	
  public function restucture_default_campaigns($room)
  {
    // Get default campaigns
    $default_campaigns = ORM::factory('campaign')
      ->join('campaigns_rooms')->on('campaigns_rooms.campaign_id', '=', 'campaigns.id')
      ->join('rooms')->on('rooms.id', '=', 'campaigns_rooms.room_id')
      ->where('campaigns.is_default', '=', 1)
      ->where('rooms.id', '=', $room->id)
      ->find_all();

    // Get one of them as role model
    $campaign_values = $default_campaigns
      ->current()
      ->as_array();
    // Remove the id because we want to use the values to create new campaign
    unset($campaign_values['id']);

    // Get default campaign ids
    $default_campaign_ids = array_keys($default_campaigns->as_array('id'));

    // Get default campaigns cancellation policies
    $cancellations = ORM::factory('cancellation')
      ->join('campaigns_cancellations')->on('campaigns_cancellations.cancellation_id', '=', 'cancellations.id')
      ->join('campaigns')->on('campaigns.id', '=', 'campaigns_cancellations.campaign_id')
      ->where('campaigns.id', 'IN', $default_campaign_ids)
      ->group_by('cancellations.id')
      ->find_all();

    // Get all languages (since default campaign will be published in all languages)
    $languages = ORM::factory('language')
      ->find_all();

    // Delete all default campaigns
    foreach ($default_campaigns as $campaign)
    {
      $campaign->delete();
    }

    // Get minimum nights for this room
    $nights = ORM::factory('night')
      ->join('nights_rooms')->on('nights_rooms.night_id', '=', 'nights.id')
      ->where('nights_rooms.room_id', '=', $room->id)
      ->order_by('nights.start_date')
      ->find_all()
      ->as_array();

    $stay_start_date = '0000-01-01';

    // Create new default campaigns based on minimum room nights
    foreach ($nights as $night)
    {
      $stay_end_date = date('Y-m-d', strtotime("$night->start_date -1 DAYS"));

      if (strtotime($stay_end_date) >= strtotime($stay_start_date))
      {
        $campaign_values['stay_start_date'] = $stay_start_date;
        $campaign_values['stay_end_date'] = $stay_end_date;

        // Create default campaign
        $campaign = ORM::factory('campaign')
          ->values($campaign_values)
          ->create();

        // Link campaign with room
        $campaign->add('rooms', $room);

        // Link campaign with cancellations
        foreach ($cancellations as $cancellation)
        {
          $campaign->add('cancellations', $cancellation);
        }

        // Link campaign with languages
        foreach ($languages as $language)
        {
          $campaign->add('languages', $language);
        }
      }

      $stay_start_date = $night->start_date;
      $stay_end_date = $night->end_date;

      $campaign_values['stay_start_date'] = $stay_start_date;
      $campaign_values['stay_end_date'] = $stay_end_date;

      // Create default campaign
      $campaign = ORM::factory('campaign')
        ->values($campaign_values)
        ->create();

      // Link campaign with room
      $campaign->add('rooms', $room);

      // Link campaign with cancellations
      foreach ($cancellations as $cancellation)
      {
        $campaign->add('cancellations', $cancellation);
      }

      // Link campaign with languages
      foreach ($languages as $language)
      {
        $campaign->add('languages', $language);
      }

      $stay_start_date = date('Y-m-d', strtotime("$night->end_date +1 DAYS"));
    }

    $stay_end_date = '9999-12-31';

    $campaign_values['stay_start_date'] = $stay_start_date;
    $campaign_values['stay_end_date'] = $stay_end_date;

    // Create default campaign
    $campaign = ORM::factory('campaign')
      ->values($campaign_values)
      ->create();

    // Link campaign with room
    $campaign->add('rooms', $room);

    // Link campaign with cancellations
    foreach ($cancellations as $cancellation)
    {
      $campaign->add('cancellations', $cancellation);
    }

    // Link campaign with languages
    foreach ($languages as $language)
    {
      $campaign->add('languages', $language);
    }
  }
  
}