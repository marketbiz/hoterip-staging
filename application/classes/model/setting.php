<?php defined('SYSPATH') or die('No direct script access.');

class Model_Setting extends ORM implements Acl_Resource_Interface {
  
  public function get_resource_id()
	{
		return 'setting';
	}
  
}