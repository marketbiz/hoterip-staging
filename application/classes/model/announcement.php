<?php defined('SYSPATH') or die('No direct script access.');

class Model_Announcement extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'announcement';
	}
	
	public function create_announcement($values)
	{
		// Start transaction
		Database::instance()->begin();

		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$descriptions = array_filter(Arr::get($values, 'descriptions', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_description = Arr::get($descriptions, $default_language_id, '');

			// Create announcement
			$announcement = $this->values($values, array(
					'title',
				))
				->create();

			// Create announcement texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$descriptions = array_filter(Arr::get($values, 'descriptions', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_description = Arr::get($descriptions, $default_language_id, '');

			$announcement_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$announcement_texts[] = ORM::factory('announcement_text')
					->set('announcement_id', $this->id)
					->values(array(
						'language_id' => $language->id,
						'description' => Arr::get($descriptions, $language->id, $default_description),
					))
					->create();
			}
			
			//is announcment exist
      $values['announcement_start_date'] = !empty($values['announcement_start_date']) ? $values['announcement_start_date'] : NULL;
      $values['announcement_end_date'] = !empty($values['announcement_end_date']) ? $values['announcement_end_date'] : NULL;
      $values['announcement_start_show'] = !empty($values['announcement_start_show']) ? $values['announcement_start_show'] : NULL;
      $values['announcement_end_show'] = !empty($values['announcement_end_show']) ? $values['announcement_end_show'] : NULL;
      $values['announcement'] = !empty($values['announcement']) ? $values['announcement'] : 0;

      // Insert into hotel announcements
       DB::insert('hotel_announcements')
      ->columns(array('hotel_id', 'announcement_id', 'announcement_start_date', 'announcement_end_date', 'announcement_start_show', 'announcement_end_show', 'announcement'))
      ->values(array($values['hotel_id'], $this->id, $values['announcement_start_date'], $values['announcement_end_date'], $values['announcement_start_show'], $values['announcement_end_show'], $values['announcement']))
      ->execute();

			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
	public function update_announcement($values)
	{

		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$descriptions = array_filter(Arr::get($values, 'descriptions', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_description = Arr::get($descriptions, $default_language_id, '');

			// Update announcement
			$this->values($values, array(
					'title',
				))
				->update();
			
			// Update announcement texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$descriptions = array_filter(Arr::get($values, 'descriptions', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_description = Arr::get($descriptions, $default_language_id, '');

			$announcement_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$announcement_texts[] = ORM::factory('announcement_text')
					->where('announcement_id', '=', $this->id)
					->where('language_id', '=', $language->id)
					->find()
					->values(array(
						'description' => Arr::get($descriptions, $language->id, $default_description),
					))
					->update();
			}

	    // Delete and create announcement
	    DB::delete('hotel_announcements')
	      ->where('announcement_id', '=', $this->id)
	      ->execute();

      //is announcement exist
      $values['announcement_start_date'] = !empty($values['announcement_start_date']) ? $values['announcement_start_date'] : NULL;
      $values['announcement_end_date'] = !empty($values['announcement_end_date']) ? $values['announcement_end_date'] : NULL;
      $values['announcement_start_show'] = !empty($values['announcement_start_show']) ? $values['announcement_start_show'] : NULL;
      $values['announcement_end_show'] = !empty($values['announcement_end_show']) ? $values['announcement_end_show'] : NULL;
      $values['announcement'] = !empty($values['announcement']) ? $values['announcement'] : 0;

      // Insert into hotel announcements
       DB::insert('hotel_announcements')
      ->columns(array('hotel_id', 'announcement_id', 'announcement_start_date', 'announcement_end_date', 'announcement_start_show', 'announcement_end_show', 'announcement'))
      ->values(array($values['hotel_id'], $this->id, $values['announcement_start_date'], $values['announcement_end_date'], $values['announcement_start_show'], $values['announcement_end_show'], $values['announcement']))
      ->execute();

			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}

	public function get_by_segment($language_id, $segment)
	{
		return DB::select(
					'announcements.*', 'announcement_texts.name'
				)
				->from('announcements')
				->join('cannouncement_texts')->on('announcements.id', '=', 'announcement_texts.announcement_id')
				->where('announcements.title', '=', $segment)
				->where('announcement_texts.language_id', '=', $language_id)
				->as_object()
				->execute()
				->current();
	}
}