<?php defined('SYSPATH') or die('No direct script access.');

class Model_City extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'city';
	}
	
	public function create_city($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('city')
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Create city
			$this->values($values, array(
					'country_id',
					'url_segment',
				))
				->create();
			
			// Create city texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$city_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$city_texts[] = ORM::factory('city_text')
					->set('city_id', $this->id)
					->values(array(
						'language_id' => $language->id,
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->create();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
	public function update_city($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('city')
					->where('id', '<>', $this->id)
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Update city
			$this->values($values, array(
					'country_id',
					'url_segment',
				))
				->update();
			
			// Update city texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$city_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$city_texts[] = ORM::factory('city_text')
					->where('city_id', '=', $this->id)
					->where('language_id', '=', $language->id)
					->find()
					->values(array(
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->update();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}

	public function get_by_segment($language_id, $segment)
	{
		return DB::select(
					'cities.*', 'city_texts.name'
				)
				->from('cities')
				->join('city_texts')->on('cities.id', '=', 'city_texts.city_id')
				->where('cities.url_segment', '=', $segment)
				->where('city_texts.language_id', '=', $language_id)
				->as_object()
				->execute()
				->current();
	}
	
}