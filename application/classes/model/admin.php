<?php

defined('SYSPATH') or die('No direct access allowed.');

class Model_Admin extends Model_A1_User_ORM implements Acl_Role_Interface {

    protected $_has_many = array(
        'hotels' => array(
            'through' => 'admins_hotels'
        ),
    );

    /**
     * Rules for the user model. Because the password is _always_ a hash
     * when it's set,you need to run an additional not_empty rule in your controller
     * to make sure you didn't hash an empty string. The password rules
     * should be enforced outside the model or with a model helper method.
     *
     * @return array Rules
     */
    public function rules() {
        return array(
            'username' => array(
                array('not_empty'),
                array('min_length', array(':value', 4)),
                array('max_length', array(':value', 30)),
                array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
            ),
            'password' => array(
                array('not_empty'),
            ),
            'email' => array(
                array('not_empty'),
                array('min_length', array(':value', 4)),
                array('max_length', array(':value', 127)),
                array('email'),
            ),
            'role' => array(
                array('not_empty'),
            ),
        );
    }

    /**
     * Filters to run when data is set in this model. The password filter
     * automatically hashes the password when it's set in the model.
     *
     * @return array Filters
     */
    public function filters() {
        return array(
            'username' => array(
                array('strtolower')
            ),
            'password' => array(
                array(array(A1::instance(), 'hash'))
            ),
        );
    }

    public function get_role_id() {
        return $this->role;
    }

    public static function username_available($username) {
        return DB::select(
                                array(DB::expr('COUNT(*)'), 'count')
                        )
                        ->from('admins')
                        ->where('username', '=', $username)
                        ->limit(1)
                        ->execute()
                        ->get('count') == 0;
    }

    public static function email_available($email) {
        return DB::select(
                                array(DB::expr('COUNT(*)'), 'count')
                        )
                        ->from('admins')
                        ->where('email', '=', $email)
                        ->limit(1)
                        ->execute()
                        ->get('count') == 0;
    }

    public function get_email($hotel_id) {
        return DB::select(
                                'admins.id', 'admins.email'
                        )
                        ->from('hotels')
                        ->join('admins_hotels')->on('hotels.id', '=', 'admins_hotels.hotel_id')
                        ->join('admins')->on('admins_hotels.admin_id', '=', 'admins.id')
                        ->where('hotels.id', '=', $hotel_id)
                        ->execute()
                        ->as_array('email', 'email');
    }

    public function get_email_extranet($hotel_id) {
        return DB::select(
                                'admins.id', 'admins.email'
                        )
                        ->from('hotels')
                        ->join('admins_hotels')->on('hotels.id', '=', 'admins_hotels.hotel_id')
                        ->join('admins')->on('admins_hotels.admin_id', '=', 'admins.id')
                        ->where('hotels.id', '=', $hotel_id)
                        ->where('admins.role', '=', 'extranet')
                        ->execute()
                        ->as_array('email', 'email');
    }

}
