<?php defined('SYSPATH') or die('No direct script access.');

class Model_Deal extends ORM implements Acl_Resource_Interface {
  
  protected $_has_many = array(
    'hotels' => array(
      'through' => 'deals_hotels',
    ),
  );
	
	public function get_resource_id()
	{
		return 'deal';
	}
	
}