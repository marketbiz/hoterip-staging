<?php defined('SYSPATH') or die('No direct script access.');

class Model_District extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'district';
	}
	
	public function create_district($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('district')
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Create district
			$this->values($values, array(
					'city_id',
					'url_segment',
				))
				->create();
			
			// Create district texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$district_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$district_texts[] = ORM::factory('district_text')
					->set('district_id', $this->id)
					->values(array(
						'language_id' => $language->id,
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->create();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
	public function update_district($values)
	{
		// Start transaction
		Database::instance()->begin();
				
		try
		{
			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			// Create url segment
			$url_segment = URL::title($default_name);
			
			$counter = 1;
			
			do 
			{
				$found = ORM::factory('district')
					->where('id', '<>', $this->id)
					->where('url_segment', '=', $url_segment)
					->count_all() > 0;
				
				if ($found)
				{
					// Increment counter
					$counter++;
					// Try another name
					$url_segment = URL::title($default_name).$counter;
				}
			} 
			while ($found);
			
			$values['url_segment'] = $url_segment;
			
			// Update district
			$this->values($values, array(
					'city_id',
					'url_segment',
				))
				->update();
			
			// Update district texts

			// Get languages
			$languages = ORM::factory('language')->find_all();

			// Get field value from post array
			$names = array_filter(Arr::get($values, 'names', array()));

			// Set default language id using english
			$default_language_id = ORM::factory('language')
				->where('code', '=', 'en-us')
				->find()
				->id;

			// Set default text value
			$default_name = Arr::get($names, $default_language_id, '');

			$district_texts = array();

			// Loop each language
			foreach ($languages as $language)
			{
				// Get text and set it's values
				$district_texts[] = ORM::factory('district_text')
					->where('district_id', '=', $this->id)
					->where('language_id', '=', $language->id)
					->find()
					->values(array(
						'name' => Arr::get($names, $language->id, $default_name),
					))
					->update();
			}
			
			// Commit transaction
			Database::instance()->commit();
		}
		catch (Exception $e)
		{
			// Rollback transaction
			Database::instance()->rollback();
			// Rethrow exception
			throw $e;
		}
		
		return $this;
	}
	
}