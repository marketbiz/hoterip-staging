<?php defined('SYSPATH') or die('No direct script access.');

class Model_Currency extends ORM implements Acl_Resource_Interface {
	
	public function get_resource_id()
	{
		return 'currency';
	}
	
	public function get_currency_by_id($id)
	{
		return DB::select()
			->from('currencies')
			->where('id', '=', $id)
			->as_object()
			->execute()
			->current();
	}

	public function get_currency_by_code($code)
	{
		return DB::select()
			->from('currencies')
			->where('code', '=', $code)
			->as_object()
			->execute()
			->current();
	}
}