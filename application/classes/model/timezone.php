<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Timezone extends ORM {

	public function get_timezone_by_identifier($timezone)
	{
		return DB::select()
			->from('timezones')
			->where('identifier', '=', $timezone)
			->as_object()
			->execute()
			->current();
	}
} 