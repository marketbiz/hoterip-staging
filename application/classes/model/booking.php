<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Booking extends ORM implements Acl_Resource_Interface {

    public function get_resource_id() {
        return 'booking';
    }

    public static function calculate_prices($booking_id) {
        // Get booking
        $booking = ORM::factory('booking')
                ->select(
                        'booking_datas.*'
                )
                ->join('booking_datas')->on('booking_datas.booking_id', '=', 'bookings.id')
                ->where('bookings.id', '=', $booking_id)
                ->find();

        // Get hotel data
        $hotel = unserialize($booking->hotel);

        // Get room data
        $room = unserialize($booking->room);

        // Get campaign data
        $campaign = unserialize($booking->campaign);

        // Get items data
        $booking_items = unserialize($booking->items);

        // Get Hotel data
        $booking->data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $booking->data);
        $booking_data = unserialize($booking->data);

        // Check if promotion campaign
        if (!$campaign->is_default) {
            // Get free dates
            $free_dates = array_keys(array_slice($booking_items, -$campaign->number_of_free_nights, $campaign->number_of_free_nights, TRUE));

            $temp = array();
            foreach ($free_dates as $free_date) {
                $temp[$free_date] = $free_date;
            }
            $free_dates = $temp;

            foreach ($booking_items as $booking_item) {
                // Get days span
                $number_of_days_span = Date::span(strtotime(date('Y-m-d', $booking->created + Date::offset($hotel->timezone))), strtotime($booking_item->date), 'days');

                // If not stop promotion
                if (!$booking_item->is_campaign_blackout) {
                    // Get day of week
                    $dow = date("w", strtotime($booking_item->date));

                    if (($dow == 0 AND $campaign->is_stay_sunday_active) OR ( $dow == 1 AND $campaign->is_stay_monday_active) OR ( $dow == 2 AND $campaign->is_stay_tuesday_active) OR ( $dow == 3 AND $campaign->is_stay_wednesday_active) OR ( $dow == 4 AND $campaign->is_stay_thursday_active) OR ( $dow == 5 AND $campaign->is_stay_friday_active) OR ( $dow == 6 AND $campaign->is_stay_saturday_active)) {
                        // Set default as do promotion
                        $do_promotion = TRUE;
                        // If earlybird promotion we need to check the span
                        if ($campaign->type == 2) {
                            $do_promotion = $number_of_days_span >= $campaign->days_in_advance;
                        }
                        // If lastminute promotion
                        elseif ($campaign->type == 3) {
                            $do_promotion = $number_of_days_span < $campaign->within_days_of_arrival;
                        }

                        if ($do_promotion) {
                            // If free dates
                            if (Arr::get($free_dates, $booking_item->date)) {
                                /// Hoterip campaign
                                // net price discount
                                $booking_item->net_price = 0;
                                $booking_item->price = 0;
                            } else {
                                /// Hoterip campaign
                                // net price discount
                                if (!empty($booking_item->net_price)) {
                                    if ($booking_item->net_price) {
                                        $booking_item->net_price = $booking_item->net_price - round($booking_item->net_price * $campaign->discount_rate / 100, 2) - $campaign->discount_amount;
                                    }
                                }
                                // elseif (!empty($booking_item->is_campaign_prices)) 
                                // {
                                //   if ($booking_item->is_campaign_prices)
                                //   {
                                //     $booking_item->price = $booking_item->price;
                                //   }
                                // }
                                else {
                                    /// Hoterip campaign
                                    // net price discount
                                    $booking_item->net_price = 0;
                                    $booking_item->price = $booking_item->price - round($booking_item->price * $campaign->discount_rate / 100, 2) - $campaign->discount_amount;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Get surcharges data
        $booking_surcharges = unserialize($booking->surcharges);

        // Get cancellation data
        $cancellation = unserialize($booking->cancellation);

        $level_1_cancellation_rule = ORM::factory('cancellation_rule')
                ->where('id', '=', $cancellation->level_1_cancellation_rule_id)
                ->find();

        $level_2_cancellation_rule = ORM::factory('cancellation_rule')
                ->where('id', '=', $cancellation->level_2_cancellation_rule_id)
                ->find();

        $level_3_cancellation_rule = ORM::factory('cancellation_rule')
                ->where('id', '=', $cancellation->level_3_cancellation_rule_id)
                ->find();

        // If booking is canceled
        if ($booking->is_canceled) {
            $cancellation_timestamp = $booking->canceled_timestamp + Date::offset($hotel->timezone);
            $check_in_date_timestamp = strtotime($booking->check_in_date);

            // Get the within days
            $within_days = (int) ceil(($check_in_date_timestamp - $cancellation_timestamp) / Date::DAY);

            // Check cancellation rules
            if ($within_days <= $level_1_cancellation_rule->within_days) {
                $cancellation_rule = $level_1_cancellation_rule;
            } elseif ($within_days <= $level_2_cancellation_rule->within_days) {
                $cancellation_rule = $level_2_cancellation_rule;
            } elseif ($within_days <= $level_3_cancellation_rule->within_days) {
                $cancellation_rule = $level_3_cancellation_rule;
            } else {
                $cancellation_rule = NULL;
            }
        } else {
            $cancellation_rule = NULL;
        }

        // Get booking rooms
        $booking_rooms = ORM::factory('booking_room')
                ->where('booking_id', '=', $booking->id)
                ->find_all();

        // Amount of money received by OTA + Hotel for this booking
        $total_received = 0;
        // Amount of hotel revenues
        $total_hotel_revenues = 0;
        // This booking price
        $total_invoice = 0;
        // Total item prices
        $total_prices = 0;

        $rooms = array();
        foreach ($booking_rooms as $key => $booking_room) {
            // Set night index
            $night_index = 1;
            $items = array();
            foreach ($booking_items as $booking_item) {
                $surcharge = Arr::get($booking_surcharges, $booking_item->date);

                if (!$surcharge) {
                    $surcharge = new stdClass;
                    $surcharge->date = $booking_item->date;
                    $surcharge->adult_price = 0;
                    $surcharge->child_price = 0;
                }

                $item_price = $booking_item->price;
                $item_net_price = !empty($booking_item->net_price) ? $booking_item->net_price : 0;
                $room_extrabed_price = $room->extrabed_price;
                $surcharge_adult_price = $surcharge->adult_price;
                $surcharge_child_price = $surcharge->child_price;

                // If there is cancellation policy
                if ($cancellation_rule) {
                    if ($cancellation_rule->percent_charged) {
                        $item_price = round($item_price * $cancellation_rule->percent_charged / 100, 2);
                        $item_net_price = round($item_net_price * $cancellation_rule->percent_charged / 100, 2);
                        $room_extrabed_price = round($room_extrabed_price * $cancellation_rule->percent_charged / 100, 2);
                        $surcharge_adult_price = round($surcharge_adult_price * $cancellation_rule->percent_charged / 100, 2);
                        $surcharge_child_price = round($surcharge_child_price * $cancellation_rule->percent_charged / 100, 2);
                    } elseif ($cancellation_rule->night_charged) {
                        if ($night_index > $cancellation_rule->night_charged) {
                            $item_price = 0;
                            $room_extrabed_price = 0;
                            $item_net_price = 0;
                            $surcharge_adult_price = 0;
                            $surcharge_child_price = 0;
                        }
                    }
                } else {
                    // If booking canceled
                    if ($booking->is_canceled) {
                        // This means no charge to guest because the booking date hasn't made into within cancellation
                        // So we make revenue to zero
                        $item_price = 0;
                        $item_net_price = 0;
                        $room_extrabed_price = 0;
                        $surcharge_adult_price = 0;
                        $surcharge_child_price = 0;
                    }
                }

                $item = array(
                    'price' => $item_price,
                    /// Hoterip campaign
                    'net_price' => $item_net_price,
                    'hotel_revenue' => $item_net_price ? $item_net_price : $item_price * ( 1 - ($hotel->commission_rate / 100)),
                    'ota_revenue' => $item_net_price ? $item_net_price : $item_price * (($hotel->commission_rate / 100)),
                    //'hotel_revenue' => $item_net_price ? $item_net_price : $item_price ,
                    'extrabed_price' => ($booking_room->number_of_extrabeds * $room_extrabed_price) * ( 1 - ($hotel->commission_rate / 100)),
                    'surcharge_price' => ($booking_room->number_of_adults * $surcharge_adult_price + $booking_room->number_of_children * $surcharge_child_price) * ( 1 - ($hotel->commission_rate / 100)),
                );

                $item['subtotal'] = $item['price'] + $item['surcharge_price'] + $item['extrabed_price'];
                $item['subtotal_net_price'] = $item['net_price'] + $item['surcharge_price'] + $item['extrabed_price'];
                $item['subtotal_hotel_revenues'] = $item['hotel_revenue'] + $item['surcharge_price'] + $item['extrabed_price'];

                $total_received += $item['subtotal'];
                $total_hotel_revenues += $item['subtotal_hotel_revenues'];
                $total_invoice += $booking_item->price + ($booking_room->number_of_extrabeds * $room->extrabed_price) + ($booking_room->number_of_adults * $surcharge->adult_price + $booking_room->number_of_children * $surcharge->child_price);
                $total_prices += $item['price'];

                $items[] = Arr::merge((array) $booking_item, $item);

                $night_index++;
            }

            $rooms[] = Arr::merge($booking_room->as_array(), array(
                        'items' => $items,
                        'number' => $key + 1,
            ));
        }

        // Get hotel to point exchange rate
        $hotel_to_point_exchange = unserialize($booking->hotel_to_point_exchange);
        // Get point to hotel exchange rate
        $point_to_hotel_exchange = unserialize($booking->point_to_hotel_exchange);
        // Get number of points used in hotel currency (point value)
        $number_of_point_used_values = round($booking->number_of_points_used / $point_to_hotel_exchange->amount * $point_to_hotel_exchange->rate);

        /// Login discount
        if (!empty($booking_data['login_discount'])) {
            $login_discount = $booking_data['login_discount'];
        } else {
            $login_discount = 0;
        }

        // bni discount
        if (!empty($booking_data['bni_discount'])) {
            $bni_discount = $booking_data['bni_discount'];
        } else {
            $bni_discount = 0;
        }

        // Get amount paid by guest
        $amount_paid_by_guest = $total_invoice - $number_of_point_used_values - $login_discount - $bni_discount;

        // If booking is not canceled
        if (!$booking->is_canceled) {
            // If total room price is bigger than number of points used 
            if ($total_prices > $number_of_point_used_values) {
                // Reward is calculated by total rooms price minus number of point used by guest
                // Times hotel point rate
                $reward_point = round((($total_prices - $number_of_point_used_values) / $hotel_to_point_exchange->amount * $hotel_to_point_exchange->rate) * $hotel->point_rate / 100);
            } else {
                $reward_point = 0;
            }

//      $reward_point = round(($total_prices / $hotel_to_point_exchange->amount * $hotel_to_point_exchange->rate) * $hotel->point_rate / 100);
        } else {
            $reward_point = 0;
        }

        // Total amount that should be refunded to guest
        // but since some guest use point for his booking so we need to calculate
        // how many money we should refund, and how many point we should refund
        $amount_refunded_to_guest = $total_invoice - $total_received;

        // If guest paid below than amount that should be refunded
        // then this mean he use points, so we need to calculate the refund point
        if ($amount_paid_by_guest < $amount_refunded_to_guest) {
            $amount_money_refunded_to_guest = $amount_paid_by_guest;
            $amount_point_refunded_to_guest = round(($amount_refunded_to_guest - $amount_paid_by_guest) / $hotel_to_point_exchange->amount * $hotel_to_point_exchange->rate);
        } else {
            $amount_money_refunded_to_guest = $amount_refunded_to_guest;
            $amount_point_refunded_to_guest = 0;
        }

        // commision
        $commission_rate = !empty($booking_data['commission_rate']) ? $total_received * ($booking_data['commission_rate'] / 100) : 0;
        $total_hotel_revenues = !empty($booking_data['commission_rate']) ? $total_received - $commission_rate : $total_hotel_revenues;

        // $ota_revenue = $total_invoice - $total_hotel_revenues - $bni_discount;

        $ota_revenue = ($total_invoice * ($hotel->commission_rate / 100)) - $bni_discount;

        //$ota_revenue = $total_received - $total_hotel_revenues - $bni_discount;

        return array(
            'rooms' => $rooms, // Price in details
            'hotel_revenue' => $total_hotel_revenues, // Hotel revenue for this booking
            'ota_revenue' => $ota_revenue, // Online travel agent revenue for this booking
            'amount_paid_by_guest' => $amount_paid_by_guest, // The amount that guest paid when book
//      'amount_refunded_to_guest' => $total_invoice - $total_received, // The amount that should be refunded to guest when booking is canceled
            'amount_money_refunded_to_guest' => $amount_money_refunded_to_guest, // The amount of money that should be refunded to guest when booking is canceled
            'amount_point_refunded_to_guest' => $amount_point_refunded_to_guest, // The amount of points that should be refunded to guest when booking is canceled
            'reward_point' => (int) $reward_point, // The amount of reward point for this booking
            'bni_discount' => (int) $bni_discount, // The amount of login discount for this booking
            'total_invoice' => (int) $total_invoice, // The amount of login discount for this booking
            'point_to_hotel_exchange' => $point_to_hotel_exchange,
        );
    }

    public function create_booking($booking_array) {
        return DB::insert(
                                'bookings', array_keys($booking_array)
                        )
                        ->values(array_values($booking_array))
                        ->execute();
    }

    public function update_booking_pending($booking_id, $booking_array) {
        return DB::update('bookings')
                        ->set($booking_array)
                        ->where('id', '=', $booking_id)
                        ->execute();
    }
    
    private function table_update() {
        /*
         * copy paste di phpmyadmin untuk update table booking
         */
                
        $query = "ALTER TABLE  `bookings` ADD  `name_booking` VARCHAR( 1 ) NOT NULL COMMENT  'B : B2B' AFTER  `payment_id` ;";
    }

}
