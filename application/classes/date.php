<?php

class Date extends Kohana_Date {
  
  /**
	 * Check if date is between start and end date
	 *
	 * @param   int    date timestamp
   * @param   int    date timestamp
   * @param   int    date timestamp
	 * @return  boolean
	 */
	public static function in($date, $start, $end)
	{
    return $date >= $start AND $date <= $end;
	}
  
}