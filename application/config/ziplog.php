<?php

return array(

	//location vacation logs
	'dir_file' => array(
		'vacation' => 'C:\xampp\htdocs\public_html\wgs\hoteripapp\hoterip_ycs_app\application\logs\Vacation',
		),

	// location logs result zip
	'dir_result_zip' => array(
		'vacation' => 'C:\xampp\htdocs\public_html\wgs\hoteripapp\hoterip_ycs_app\application\logs\Vacation'
		),

	// date interval logs
	'date_interval' => 5,

	);