<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    // Application name
    'name' => 'Hoterip',
    'email' => 'robothoterip@gmail.com',
    // From address when sending newsletter / mass email
    'mass_email_address' => 'robothoterip@gmail.com',
    // From address when sending email to guest (on manage booking)
    'conversation_email_address' => 'robothoterip@gmail.com',
    // From address when sending forgot password email
    'forgot_email_address' => 'robothoterip@gmail.com',
    // To address when sending error email
    'error_email_address' => 'robothoterip@gmail.com',
    'hotel_images_directory' => '../hoterip/images/hotels',
    'hotel_images_url' => 'http://staging.hoterip.com/images/hotels',
    'banner_directory' => '../hoterip/images/banners',
    'banner_url' => 'http://staging.hoterip.com/images/banners',
    'unsubscribe_salt' => 'melonmamma',
    'unsubscribe_url' => 'http://staging.hoterip.com/unsubscribe',
    'rule_price' => array(
        'min_price_item' => 1,
        'max_price_item' => 100000,
    ),
    // Pdf directory
    'pdf_directory' => 'media/pdf/',
    'hotel_url' => array(
        'live' => 'https://hoterip.com/',
        'staging' => 'https://staging.hoterip.com/',
    )
);
