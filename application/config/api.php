<?php defined('SYSPATH') or die('No direct script access.');

return array(
  // Application name
  'vacation' => array(
    // set whether request is for specific hotel only
    'specific_hotel' => true,
    'allowed_hotels' => array(
        // Batch #1
           49,   103,   345,
        // Batch #2
           40,    43,    45,    47,    52,    56,    57,    59,   101,   106,
          107,   109,   110,   111,   162,   163,   209,   210,   211,   212,
          213,   215,   216,   218,   219,   220,   221,   223,   224,   240,
          241,   243,   267,   271,   273,   279,   280,   281,   315,   282,
          283,   285,   292,   297,   306,   312,   313,   314,   316,   317,
        // Batch #3
          429,   374,   375,   376,   379,   381,   383,   384,   386,   389,
          390,   398,   399,   404,   409,   411,   416,   420,   424,   425,
          426,   427,   435,   436,   441,   453,   454,   462,   463,   464,
          470,   471,   472,   473,   476,   477,   479,   480,   484,   485,
          499,   506,   508,   516,   517,   525,   981,   982,   1000,  1005,
        // Batch #4
          492,   526,   527,   543,   545,   561,   569,   570,   579,   584,
          609,   611,   615,   616,   617,   618,   619,   629,   633,   640,
          641,   643,   644,   649,   651,   698,   719,   720,   743,   752,
          756,   757,   760,   762,   763,   782,   786,   787,   798,   800,
          801,   802,   827,   845,   854,   855,   999,   1070,  1073,  1079,
        // Batch #5
           55,   168,   291,   308,   318,   324,   325,   326,   330,   331,
          333,   339,   341,   342,   343,   347,   348,   355,   358,   365,
          368,   372,   413,   414,   415,   430,   498,   515,   717,   885,
          886,   902,   903,   905,   919,   923,   931,   933,   934,   971,
          973,  1125,  1136,  1189,  1193,  1207,  1252,  1267,  1280,  1420
        ),

    'email'=>'vacation@hoterip.com',
    'nationality_id'=>'109',
    'credit_card_name'=>'HIS Vacation',

  	// URL connecting
    'url_connect'=>'https://api.hotels.his-vacation.com/api/VQCNotif.aspx?Channel=Hoterip',

  	// Head Property
    'soap_env'=>'http://schemas.xmlsoap.org/soap/envelope/',
    'Version'=>'1.0',
    'ChannelIdentifierId'=>'En',
    'Interface'=>'Productions',
    'xmlns'=>'http://www.opentravel.org/OTA/2003/05',
    'RequestId'=>'0001',
    'Transaction'=>'Productions',
    'ComponentType' => 'Hotel',

    // Body Property
    'Target' => 'Production',

    // Auth Property
    'User'=>'Hoterip',
    'Pwd'=>'xml4!Hrph1s',

    'phone' => '0361721064',
    ),

  'vacation_test' => array(
    // set whether request is for specific hotel only
    'specific_hotel' => true,
    'allowed_hotels' => array(239),

    'email'=>'sam.fajar@wgs.co.id',
    'nationality_id'=>'109',
    'credit_card_name'=>'HIS Vacation',

    // URL connecting
    'url_connect'=>'http://test-api.hotels.his-vacation.com/api_01/VQCNotif.aspx?Channel=Hoterip',
    //[3:49:07 PM] saburo his: test-api.hotels.his-vacation.com/api_01/VQCNotif.aspx
    
    // Head Property
    'soap_env'=>'http://schemas.xmlsoap.org/soap/envelope/',
    'Version'=>'2011B',
    'ChannelIdentifierId'=>'En',
    'Interface'=>'VACATION QUICK CONNECT XML 4 OTA',
    'xmlns'=>'http://www.opentravel.org/OTA/2003/05',
    'RequestId'=>'0001',
    'Transaction'=>'Development',
    'ComponentType' => 'Hotel',

    // Body Property
    'Target' => 'Test',

    // Auth Property
    'User'=>'Hoterip',
    'Pwd'=>'HoteripHIS',
    'Code'=>'HTRP',

    'phone' => '0361721064',
    ),



  // Application name
  'rategain' => array(
    'rategain_code' => 742,
    'code' => 'HTRP',
    'rategain_reservation_url' => 'https://t1.cgbeta.rategain.com/OTAPushResv/Reservation.svc',
    'username' => 'Hoterip',
    'password' => 'RG@Hoterip',
    ),

  // Application name
  'siteminder' => array(

    'email'=>'siteminder@hoterip.com',
    'nationality_id'=>'109',
    'credit_card_name'=>'HIS Vacation',
    'Code'=>'HOT',
    // URL connecting
    'url_connect'=>'https://smtpi.siteminder.com/siteconnect/services',
    // Auth Property
    'User'=>'Hoterip',
    'Pwd'=>'rqQX072v1hUB1ST',
    'id'=>'HTP',
    'code'=>'HOT',
    ),

  // Application name
  'staah' => array(

    'email'=>'staah@hoterip.com',
    'nationality_id'=>'109',
    'credit_card_name'=>'HIS Vacation',
    'Code'=>'HOT',
    // URL connecting
    'url_connect'=>'',
    // Auth Property
    'User'=>'',
    'Pwd'=>'',
    'id'=>'HTP',
    'code'=>'HOT',
    ),
);