<?php defined('SYSPATH') or die('No direct script access.');

return array(
  // Date
  'Y-m-d' => 'd-m-Y',
  'M j, Y' => 'j M Y',
  
  //Benefit
  'Promotion Benefit' => 'Manfaat Promosi',
  
  // Email - conversation
  'Message From :hotel_name' => 'Pesan Dari :hotel_name',
  'Hello :first_name :last_name,' => 'Halo :first_name :last_name,',
  ':hotel_name has sent you message regarding booking ID #:booking_id.' => ':hotel_name mengirim Anda pesan mengenai booking ID #:booking_id.',
  'The following is the contents of the message.' => 'Berikut adalah isi pesan yang dikirim.',
  'To reply this message, please click link below.' => 'Untuk membalas pesan ini, silahkan klik link dibawah ini.',
  'Note: Please write using English or language where the hotel is located.' => 'Catatan: Harap menulis menggunakan Bahasa Inggris atau bahasa dimana hotel berada.',
  'This email is an automated notification email. Do not reply to this email.' => 'Email ini merupakan notifikasi otomatis. Jangan membalas email ini.',
  
  // Email - cancel
  'Booking Canceled' => 'Pesanan Dibatalkan',
  'Hello :first_name :last_name,' => 'Halo :first_name :last_name,',
  'Your booking has been canceled.' => 'Pesanan Anda telah dibatalkan.',
  'The following are your canceled booking details.' => 'Berikut adalah detail pesanan Anda yang dibatalkan.',
  'Booking ID' => 'ID Pesanan',
  'Hotel Name' => 'Nama Hotel',
  'Room Name' => 'Nama Kamar',
  'Check-in Date' => 'Tanggal Cek-in',
  'Check-out Date' => 'Tanggal Cek-out',
  'Cancellation Policy' => 'Kebijakan Pembatalan',
  
  // Email - refund
  'Refund Notification' => 'Notifikasi Pengembalian Uang',
  'We have refunded money or/and points to your Hoterip account as below' => 'Kita telah mengembalikan uang Anda ke kartu kredit Anda dan/atau poin Anda ke akun Hoterip Anda seperti yang tertera dibawah',
  'Paid' => 'Di Bayarkan',
  'Used Point' => 'Poin Yang Digunakan',
  'Refunded' => 'Di Kembalikan',
  'Refunded Point' => 'Poin Yang Dikembalikan',
  'It may take up to 7 days to be done the process.' => 'Untuk proses ini dapat memakan waktu sampai 7 hari',
  'Please contact Hoterip Support if you do not receive the above amount after 7 days.' => 'Harap kontak Hoterip support jika Anda belum menerima seperti yang tertera di atas setelah 7 hari',
);