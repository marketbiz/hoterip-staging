<?php defined('SYSPATH') or die('No direct script access.');

return array(
  // Date
  'Y-m-d' => 'Y年m月d日',
  'M j, Y' => 'Y年m月d日',
  
  //Benefit
  'Promotion Benefit' => '特典',
  // Email - conversation
  'Message From :hotel_name' => ':hotel_nameからのお知らせ',
  'Hello :first_name :last_name,' => ':last_name:first_nameさま,',
  ':hotel_name has sent you message regarding booking ID #:booking_id.' => ':hotel_name から予約番号 #:booking_idの予約に関してメッセージが届いております。.',
  'The following is the contents of the message.' => '下記をご確認ください。',
  'To reply this message, please click link below.' => '返信される場合はリンクをクリックしてください。',
  'Note: Please write using English or language where the hotel is located.' => 'お願い: ホテルとの直接のやり取りとなりますので、英語または現地語でお書き頂けますようお願いします。',
  'This email is an automated notification email. Do not reply to this email.' => 'こちらは自動メールです。返信しないでください。',
  
  // Email - cancel
  'Booking Canceled' => 'キャンセル済み予約',
  'Hello :first_name :last_name,' => ':last_name:first_nameさま,',
  'Your booking has been canceled.' => 'ご予約はキャンセルされました。',
  'The following are your canceled booking details.' => '下記の予約詳細をご確認ください。',
  'Booking ID' => '予約番号',
  'Hotel Name' => 'ホテル名',
  'Room Name' => '部屋カテゴリー名',
  'Check-in Date' => 'チェックイン日',
  'Check-out Date' => 'チェックアウト日',
  'Cancellation Policy' => 'キャンセル規定',
  
  // Email - refund
  'Refund Notification' => '払い戻しのお知らせ',
  'We have refunded money or/and points to your Hoterip account as below' => 'キャンセル規定にのっとって払い戻ししました。',
  'Paid' => '支払い額',
  'Used Point' => '利用ポイント数',
  'Refunded' => '払い戻し額',
  'Refunded Point' => '払い戻しポイント数',
  'It may take up to 7 days to be done the process.' => '実際に口座に入るまで7日程かかる場合があります。',
  'Please contact Hoterip Support if you do not receive the above amount after 7 days.' => '7日以上経ってもお受け取りできない場合はホテリップサポートまでご連絡ください。',
);
